/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_package_map;
import static com.qspear.procurement.migration.Loader.teqip_package_techcriteria_map;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.repositories.method.RFPDetailRepository;
import com.qspear.procurement.repositories.method.RFPDocumentRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_rfpdocument)
public class teqip_package_rfpdocument implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    RFPDocumentRepository rFPDocumentRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT"
                    + "  pt.PTPackageID,"
                    + "  pt.PPPackageID,"
                    + "  pt.PassingScore,"
                    + "  pt.TechinalScorePercentage,"
                    + "  pt.FinancialScorePercentage,"
                    + "  pt.PTCode,"
                    + "  pt.ClientRepresentative,"
                    + "  pt.InputsFromClient,"
                    + "  pt.BidValidity,"
                    + "  pt.LastDateOfValidity,"
                    + "  pt.LastDateForClarifications,"
                    + "  pt.ClarificationIssuedPlace,"
                    + " pt.LocalTaxation,"
                    + " pt.NoofCopiedToBeSubmitted,"
                    + " pt.PurchaserNoticeAddress,"
                    + " pt.BidSubmissionLastDate,"
                    + " pt.BidSubmissionLastTime,"
                    + " pt.ExpectedContractNegotiationDate,"
                    + " pt.ContractNegotiationAddress,"
                    + " pt.WorkStartDate,"
                    + " pt.ConsultingServiceLocation,"
                    + " pt.CombinedProposal,"// paragraph1_2
                    + " pt.NoofStaffMonthsBudget," //paragraph3_3
                    + " pt.TrainingClause," //paragraph3_4Information
                    + "  pt.IsTrainingRequired,"//paragraph3_4IsTraining
                    + " pt.PerformanceSecurityInstrument" // paragraph3_4
                    + "  FROM tbl_PP_PMSS_PTPackageDetails pt");
            while (resultSet.next()) {

                TeqipPackageRFPDocument entity = new TeqipPackageRFPDocument();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(2)));
                entity.setMinTechnicalPassingScore(resultSet.getDouble(3));
                entity.setWeightageForTechnicalProposal(resultSet.getDouble(4));
                entity.setWeightageForFinancialProposal(resultSet.getDouble(5));
                entity.setReferenceNo(resultSet.getString(6));
                entity.setParagraph1_3(resultSet.getString(7));
                entity.setParagraph1_4(resultSet.getString(8));
                entity.setParagraph1_14(resultSet.getInt(9));
                entity.setParagraph1_14Date(resultSet.getDate(10));
                entity.setParagraph2_1(resultSet.getDate(11));
                entity.setParagraph2_1Address(resultSet.getString(12));

                entity.setParagraph3_6(resultSet.getInt(13));
                entity.setParagraph4_3(resultSet.getInt(14));
                entity.setParagraph4_5(resultSet.getString(15));
                entity.setParagraph4_5Date(Loader.setTimeInDate(resultSet.getDate(16), resultSet.getString(17)));
                entity.setParagraph4_5DateTime(Loader.setTimeInDate(resultSet.getDate(16), resultSet.getString(17)));
                entity.setParagraph6_1Date(resultSet.getDate(18));
                entity.setParagraph6_1Address(resultSet.getString(19));
                entity.setParagraph7_1(resultSet.getDate(20));
                entity.setParagraph7_1Address(resultSet.getString(21));
                entity.setParagraph1_2(resultSet.getInt(22));
                entity.setParagraph3_3(resultSet.getInt(23));

                entity.setParagraph3_4Information(resultSet.getString(24));
                entity.setParagraph3_4IsTraining(resultSet.getInt(25));
                if (resultSet.getInt(26) != 0) {
                    entity.setParagraph3_4(resultSet.getInt(26) == 2 ? "STP" : "FTP");
                }
                teqip_package_rfpdocument_map.put(resultSet.getInt(1), entity);
            }
            rFPDocumentRepository.save(teqip_package_rfpdocument_map.values());
            rFPDocumentRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
