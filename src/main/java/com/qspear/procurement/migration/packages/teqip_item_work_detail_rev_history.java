package com.qspear.procurement.migration.packages;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static com.qspear.procurement.migration.Loader.teqip_categorymaster_map;
import static com.qspear.procurement.migration.Loader.teqip_item_master_map;
import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_categorymaster_map;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipItemWorkDetailHistory;
import com.qspear.procurement.repositories.ItemWorkHistoryRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

@Repository
@Order(value = Loader.teqip_item_work_detail_rev_history)
public class teqip_item_work_detail_rev_history implements PackageLoader {

    @Autowired
    ItemWorkHistoryRepository itemWorkRepository;

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipItemWorkDetailHistory> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " ppitem.PPItemRevisionID,"
                    + " ppitem.item_name,"
                    + " ppitem.item_description,"
                    + " ppackage.ItemCategoryID,"
                    + " ppitem.item_procuringdepartmentid,"
                    + " ppitem.item_totalestimatedcost,"
                    + " ppitem.createdby,"
                    + " ppitem.createddate,"
                    + " ppitem.modifiedby,"
                    + " ppitem.modifieddate,"
                    + " ppackage.PPPackageID,"
                    + " ppitem.PPRevisionID,"
                    + " ppitem.PPItemID"
                    + " FROM tbl_PP_PMSS_PPPackageDetails ppackage"
                    + " INNER JOIN  tbl_PP_PMSS_PPItemDetails_Revision ppitem ON ppackage.PPPackageID=ppitem.PPPackageID"
            );
            while (resultSet.next()) {
                TeqipCategorymaster get = teqip_categorymaster_map.get(resultSet.getInt(4));

                if (get.getCategoryName().equalsIgnoreCase("Civil Works")) {

                    int id = resultSet.getInt(1);

                    TeqipItemWorkDetailHistory entity = new TeqipItemWorkDetailHistory();

                    entity.setWorkName(resultSet.getString(2));
                    entity.setWorkSpecification(resultSet.getString(3));
                    entity.setTeqipCategorymaster(teqip_categorymaster_map.get(resultSet.getInt(4)));
                    entity.setTeqipPmssDepartmentMaster(teqip_pmssdepartmentmaster_map.get(resultSet.getInt(5)));
                    entity.setWorkCost(resultSet.getDouble(6));
                    entity.setCreatedOn(new Timestamp(new Date().getTime()));
                    entity.setCreatedBy(1);
                    entity.setModifyOn(new Timestamp(new Date().getTime()));
                    entity.setModifyBy(1);
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(11)));
                    entity.setTeqipPackageHistory(teqip_package_rev_history_map.get(resultSet.getInt(12)));
                    entity.setTeqipItemWorkDetail(teqip_item_work_detail_map.get(resultSet.getInt(13)));

                    mapping.add(entity);
                }

            }

            itemWorkRepository.save(mapping);
            itemWorkRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
