package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsEvaluationItemDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import com.qspear.procurement.repositories.ItemRepository;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsEvaluationItemDetailRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_quotationevaluationitemdetails)
public class teqip_package_quotationevaluationitemdetails implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PackageQuotationsEvaluationItemDetailRepository packageQuotationsEvaluationItemDetailRepository;

    @Autowired
    TeqipPackageRepository packageRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    ItemRepository itemRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageQuotationsEvaluationItemDetail> mapping = new ArrayList<>();
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(" SELECT"
                    + "  pid.PPPackageID,"
                    + "  bdi.bidderdetailid,"
                    + "  pti.PPItemID,"
                    + "  pti.item_quantity,"
                    + "  bdi.i_rate,"
                    + "  bdi.i_rate * pti.item_quantity,"
                    + "  bdi.i_comments"
                    + "   FROM tbl_PP_PMSS_BidderitemDetails bdi"
                    + "	 INNER JOIN tbl_PP_PMSS_BidderDetails bd ON bd.BidderDetailID = bdi.BidderDetailID"
                    + "	 INNER JOIN  tbl_PP_PMSS_PTItemDetails pti ON bdi.PTItemID = pti.PTItemID"
                    + "	 INNER JOIN tbl_PP_PMSS_PTPackageDetails pid  ON  bdi.PTPackageID=pid.PTPackageID  ");
            while (resultSet.next()) {
                TeqipPackageQuotationsEvaluationItemDetail entity = new TeqipPackageQuotationsEvaluationItemDetail();

                TeqipPackage teqipPackage = teqip_package_map.get(resultSet.getInt(1));
                TeqipCategorymaster teqipCategorymaster = teqipPackage.getTeqipCategorymaster();
                if (teqipCategorymaster.getCategoryName().equalsIgnoreCase("Goods")) {

                    TeqipItemMaster get = teqip_item_master_map.get(resultSet.getInt(3));
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setTeqipPmssSupplierMaster(teqip_pmss_suppliermaster_map.get(resultSet.getInt(2)));
                    entity.setTeqipItemMaster(get);
                    entity.setItemQty(resultSet.getDouble(4));
                    entity.setBasicCost(resultSet.getDouble(5));
                    entity.setTotalPrice(resultSet.getDouble(6));
                    entity.setComment(resultSet.getString(7));
                    mapping.add(entity);
                }

            }

            packageQuotationsEvaluationItemDetailRepository.save(mapping);
            packageQuotationsEvaluationItemDetailRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
