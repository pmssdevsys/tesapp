package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_package_map;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsEvaluationItemDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsEvaluationWorkDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import com.qspear.procurement.repositories.ItemRepository;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsEvaluationItemDetailRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsEvaluationWorkDetailRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_quotationevaluationworkdetails)
public class teqip_package_quotationevaluationworkdetails implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PackageQuotationsEvaluationWorkDetailRepository detailRepository;

    @Autowired
    TeqipPackageRepository packageRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    ItemRepository itemRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageQuotationsEvaluationWorkDetail> mapping = new ArrayList<>();
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT  "
                    + " pid.PPPackageID,"
                    + " bdi.bidderdetailid, "
                    + " ppitem.item_name, "
                    + " ppitem.item_description, "
                    + " bdi.I_ReadOutPrice, "
                    + " bdi.I_ReadOutPrice, "
                    + " bdi.i_comments  "
                    + " FROM tbl_PP_PMSS_BidderitemDetails bdi	 "
                    + " INNER JOIN tbl_PP_PMSS_BidderDetails bd ON bd.BidderDetailID = bdi.BidderDetailID	 "
                    + " INNER JOIN  tbl_PP_PMSS_PTItemDetails pti ON bdi.PTItemID = pti.PTItemID	 "
                    + " INNER JOIN  tbl_PP_PMSS_PPItemDetails ppitem  ON pti.PPItemID =  ppitem.PPItemID"
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pid  ON  bdi.PTPackageID=pid.PTPackageID ");
            while (resultSet.next()) {
                
                TeqipPackage teqipPackage = teqip_package_map.get(resultSet.getInt(1));
                TeqipCategorymaster teqipCategorymaster = teqipPackage.getTeqipCategorymaster();
                if (teqipCategorymaster.getCategoryName().equalsIgnoreCase("Civil Works")) {

                    TeqipPackageQuotationsEvaluationWorkDetail entity = new TeqipPackageQuotationsEvaluationWorkDetail();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setTeqipPmssSupplierMaster(teqip_pmss_suppliermaster_map.get(resultSet.getInt(2)));
                    entity.setWorkName(resultSet.getString(3));
                    entity.setWorkSpecification(resultSet.getString(4));
                    entity.setWorkCost(resultSet.getDouble(5));
                    entity.setWorkEstimatedCost(resultSet.getDouble(6));
                    entity.setComment(resultSet.getString(7));
                    mapping.add(entity);
                }

            }

            detailRepository.save(mapping);
            detailRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
