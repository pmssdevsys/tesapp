/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.methods.TeqipPackageLetterAcceptance;
import com.qspear.procurement.repositories.method.LetterOfAcceptanceRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_letteracceptance)
public class teqip_package_letteracceptance implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    private LetterOfAcceptanceRepository letterOfAcceptanceRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageLetterAcceptance> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("  SELECT "
                    + "  PPPackageID,"
                    + "  MaintenancePeriodExpDate,"
                    + "  LOAGeneratedDate,"
                    + "  BidValidity,"
                    + "  PerformanceSecurityAmt"
                    + "  FROM tbl_PP_PMSS_PTPackageDetails");
            while (resultSet.next()) {

                TeqipPackageLetterAcceptance entity = new TeqipPackageLetterAcceptance();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                entity.setMaintancePeriodExpiryDate(resultSet.getDate(2));
                entity.setLoaGeneratedDate(resultSet.getDate(3));
                entity.setNewBidValidity(resultSet.getInt(4));
                entity.setSecurityAmount(resultSet.getDouble(5));

                mapping.add(entity);
            }

            letterOfAcceptanceRepository.save(mapping);
            letterOfAcceptanceRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
