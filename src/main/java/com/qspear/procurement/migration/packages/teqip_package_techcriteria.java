/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.repositories.method.TechCriteriaRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_techcriteria)
public class teqip_package_techcriteria implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    private TechCriteriaRepository techCriteriaRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT"
                    + "  a.PTPackgeTCriteriaID,"
                    + "  pd.PPPackageID,"
                    + "  a.TechnicalCriteria,"
                    + "  a.Marks"
                    + "  FROM tbl_PP_PMSS_PTPackageTechnicalCriteria a"
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON pd.PTPackageID = a.PTPackageID");
            while (resultSet.next()) {

                TeqipPackageTechCriteria entity = new TeqipPackageTechCriteria();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(2)));
                entity.setTechCriteriaDesc(resultSet.getString(3));
                entity.setMarks(resultSet.getDouble(4));

                teqip_package_techcriteria_map.put(resultSet.getInt(1), entity);
            }
            techCriteriaRepository.save(teqip_package_techcriteria_map.values());
            techCriteriaRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
