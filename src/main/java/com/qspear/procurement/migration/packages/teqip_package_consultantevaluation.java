package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageAward;
import com.qspear.procurement.persistence.methods.TeqipPackageConsultantEvaluation;
import com.qspear.procurement.persistence.methods.TeqipPackageTechSubCriteria;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.ConsultantEvaluationRepository;
import com.qspear.procurement.repositories.method.PackageAwardRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_consultantevaluation)
public class teqip_package_consultantevaluation implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    ConsultantEvaluationRepository consultantEvaluationRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageConsultantEvaluation> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " pt.PPPackageID,"
                    + " ssc.BidderDetailID,"
                    + " ssc.PTPackageSubCriteriaID,"
                    + " ssc.Marks"
                    + " FROM tbl_PP_PMSS_SupplierSubCriteria ssc "
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pt ON pt.PTPackageID = ssc.PTPackageID");
            while (resultSet.next()) {

                TeqipPackageTechSubCriteria get = teqip_package_techsubcriteria_map.get(resultSet.getInt(3));
                if (get != null) {
                    TeqipPackageConsultantEvaluation entity = new TeqipPackageConsultantEvaluation();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setTeqipPmssSupplierMaster(teqip_pmss_suppliermaster_map.get(resultSet.getInt(2)));
                    entity.setTeqipPackageTechSubCriteria(get);
                    entity.setMarks(resultSet.getDouble(4));

                    mapping.add(entity);
                }

            }

            consultantEvaluationRepository.save(mapping);
            consultantEvaluationRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
