package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageAmendmentdetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractGeneration;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageTermReference;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.AmendmentdetailRepository;
import com.qspear.procurement.repositories.method.PackageQuotationDetailRepository;
import com.qspear.procurement.repositories.method.QCBSContractGenerationRepository;
import com.qspear.procurement.repositories.method.TermReferenceRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_termreference)
public class teqip_package_termreference implements PackageLoader {
    
    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;
    
    @Autowired
    TermReferenceRepository repository;
    
    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageTermReference> mapping = new ArrayList<>();
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            
            resultSet = statement.executeQuery("SELECT "
                    + " PPPackageID,"
                    + " IsTORApproved,"
                    + " TORApprovalDate,"
                    + " EOIOpeningDate,"
                    + " TORDetails,"
                    + " MaintenancePeriodExpDate,"
                    + " EOIOpeningTime"
                    + " from  tbl_PP_PMSS_PTPackageDetails ");
            while (resultSet.next()) {
                
                TeqipPackageTermReference entity = new TeqipPackageTermReference();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                entity.setIsworldBankNoc(resultSet.getInt(2));
                entity.setWorldBankNocDate(resultSet.getDate(3));
                entity.setEoiOpeningDate(Loader.setTimeInDate(resultSet.getDate(4), resultSet.getString(7)));
                entity.setRemarks(resultSet.getString(5));
                entity.setDateOfShortlistingFirms(resultSet.getDate(6));
                mapping.add(entity);
            }
            repository.save(mapping);
            repository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
        
    }
    
}
