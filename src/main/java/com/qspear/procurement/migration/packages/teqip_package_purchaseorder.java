/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrder;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackagePurchaseOrderRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_purchaseorder)
public class teqip_package_purchaseorder implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    private PackagePurchaseOrderRepository packagePurchaseOrderRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackagePurchaseOrder> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("  SELECT "
                    + "  pd.PPPackageID,"
                    + "  d.BidderDetailID,"
                    + "  d.PONumber,"
                    + " d.PODate,"
                    + "  d.OctroiAndOthers,"
                    + "  d.POID,"
                    + " pd.IsPOFinalized,"
                    + " pd.BasicValue,"
                    + " pd.ContractValue"
                    + "  FROM tbl_PP_PMSS_PurchaseOrder d "
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON pd.PTPackageID = d.PTPackageID "
            );
            while (resultSet.next()) {
                int id = resultSet.getInt(6);

                TeqipPmssSupplierMaster get = teqip_pmss_suppliermaster_map.get(resultSet.getInt(2));
                if (get == null) {
                    missingData.add("Invalid BidderDetailID ::tbl_PP_PMSS_PurchaseOrder ::" + id);

                } else {
                    TeqipPackagePurchaseOrder entity = new TeqipPackagePurchaseOrder();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setTeqipPmssSupplierMaster(get);
                    entity.setPurchaseOrderNo(resultSet.getString(3));
                    entity.setPoDate(resultSet.getDate(4));
                    entity.setOctroiOther(resultSet.getDouble(5));
                    if (resultSet.getObject(7) != null) {
                        entity.setIsPoFinalised(resultSet.getInt(7));
                    }
                    entity.setEvaluatedCost(resultSet.getDouble(8));
                    entity.setTotalContractValue(resultSet.getDouble(9)); 
                    
                    Double totalBaseCost = entity.getTotalContractValue();
                    if(entity.getOctroiOther() != null && totalBaseCost != null){
                        totalBaseCost =  totalBaseCost - entity.getOctroiOther() ;
                    }
                    entity.setTotalBaseCost(totalBaseCost);
                    
                    mapping.add(entity);
                }
            }

            packagePurchaseOrderRepository.save(mapping);
            packagePurchaseOrderRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
