/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.persistence.methods.TeqipPackageTechEvalCommitee;
import com.qspear.procurement.persistence.methods.TeqipPackageTechSubCriteria;
import com.qspear.procurement.repositories.method.TechSubCriteriaRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_techsubcriteria)
public class teqip_package_techsubcriteria implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TechSubCriteriaRepository techSubCriteriaRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

//        ArrayList<TeqipPackageTechSubCriteria> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("  SELECT "
                    + "  a.PTPackageSubCriteriaID,"
                    + "  pd.PPPackageID,"
                    + "  ptc.PTPackgeTCriteriaID,"
                    + "  a.SubCriteria,"
                    + "  a.SubCriteria_Marks"
                    + "  FROM tbl_PP_PMSS_PTPackageSubCriteria a "
                    + "  INNER JOIN  tbl_PP_PMSS_PTPackageTechnicalCriteria ptc on ptc.PTPackgeTCriteriaID = a.PTPackgeTCriteriaID"
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON pd.PTPackageID = ptc.PTPackageID"
            );
            while (resultSet.next()) {

                TeqipPackageTechCriteria get = teqip_package_techcriteria_map.get(resultSet.getInt(3));
                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid PTPackgeTCriteriaID ::tbl_PP_PMSS_PTPackageSubCriteria ::" + id);

                } else {
                    TeqipPackageTechSubCriteria entity = new TeqipPackageTechSubCriteria();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(2)));
                    entity.setTeqipPackageTechCriteria(get);
                    entity.setTechCriteriaDesc(resultSet.getString(4));
                    entity.setMarks(resultSet.getDouble(5));

                    teqip_package_techsubcriteria_map.put(id,entity);
                }
            }

            techSubCriteriaRepository.save(teqip_package_techsubcriteria_map.values());
            techSubCriteriaRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
