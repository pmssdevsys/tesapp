package com.qspear.procurement.migration.packages;

import com.qspear.procurement.constants.MethodConstants;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipUsersDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsEvaluationItemDetail;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageDocumentRepository;
import java.io.File;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_documentdetails)
public class teqip_package_documentdetails implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PackageDocumentRepository packageDocumentRepository;

    @Autowired
    TeqipPackageRepository packageRepository;

    public String getFolderLocation(int TagId, int categoryID) {

        if ((TagId == 6021 || TagId == 6060) && (categoryID == 5 || categoryID == 6)) {
            return "Attachments" + File.separator + "Shopping" + File.separator + "GRN" + File.separator;
        }
        if (TagId == 6022 && categoryID == 4) {
            return "Attachments" + File.separator + "Shopping" + File.separator + "InvitationLetter" + File.separator;
        }
        if (TagId == 6022 && categoryID == 7) {
            return "Attachments" + File.separator + "Shopping" + File.separator + "Quotation" + File.separator;
        }
        if (TagId == 6067 && categoryID == 42) {
            return "Attachments" + File.separator + "LIB" + File.separator + "InvitationLetter" + File.separator;
        }
        if (TagId == 6079 && categoryID == 31) {
            return "Attachments" + File.separator + "QCBS" + File.separator + "InvitationLetter" + File.separator;
        }
        if (TagId == 6079 && categoryID == 7) {
            return "Attachments" + File.separator + "Shopping" + File.separator + "Quotation" + File.separator;
        }
        if (TagId == 6068 && categoryID == 39) {
            return "Attachments" + File.separator + "QCBS" + File.separator + "WCC" + File.separator;
        }
        if (TagId == 1111 && (categoryID == 10 || categoryID == 88)) {
            return "Attachments" + File.separator + "Documents" + File.separator;
        }
        return "Attachments" + File.separator + "PMSS Documents" + File.separator;
    }

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageDocument> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(" SELECT"
                    + "  pt.PPPackageID,"
                    + "  dc.category,"
                    + "  FileName,"
                    + "  Description,"
                    + "  originalFileName,"
                    + "  dd.TagID,"
                    + " dd.Categoryid,"
                    + " dd.createdDate,"
                    + " dd.UploadedBy"
                    + "  FROM tbl_PP_PMSS_Documents dd,"
                    + " tbl_PM_DocumentCategory dc ,"
                    + " tbl_PP_PMSS_PTPackageDetails pt"
                    + " where dd.categoryid=dc.Categoryid"
                    + " and  pt.PTPackageID =  uniqueId "
                    + " and  dd.UploadedBy !='Admin' and dd.categoryid NOT IN(5,6) ");
            while (resultSet.next()) {
                int aInt = resultSet.getInt(1);
                if (aInt > 0) {
                    TeqipPackageDocument entity = new TeqipPackageDocument();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setDocumentCategory(resultSet.getString(2));
                    entity.setDocumentfilename(this.getFolderLocation(resultSet.getInt(6), resultSet.getInt(7)) + resultSet.getString(3));
                    entity.setDescription(resultSet.getString(4));
                    entity.setOriginalFileName(resultSet.getString(5));
                    entity.setCreatedOn(resultSet.getDate(8));
                    TeqipUsersDetail get = teqip_users_detail_string_map.get(resultSet.getString(9));
                    if (get != null) {
                        entity.setCreatedBy(get.getUserid());
                    }
                    mapping.add(entity);
                }

            }

            resultSet = statement.executeQuery(" SELECT"
                    + " pt.PPPackageID,"
                    + " dc.category,"
                    + " FileName,"
                    + " dd.Description,"
                    + " originalFileName,"
                    + " BidderDetailID,"
                    + " pp.ItemCategoryID,"
                    + "  dd.TagID,"
                    + " dd.Categoryid,"
                    + " dd.createdDate,"
                    + " dd.UploadedBy"
                    + " FROM tbl_PP_PMSS_Documents dd, "
                    + " tbl_PM_DocumentCategory dc , "
                    + " tbl_PP_PMSS_BidderDetails b,"
                    + " tbl_PP_PMSS_PTPackageDetails pt,"
                    + " tbl_PP_PMSS_PPPackageDetails pp"
                    + " where dd.categoryid=dc.Categoryid "
                    + "   and  b.BidderDetailID =  dd.uniqueId "
                    + "   and  pt.PTPackageID =  b.PTPackageID "
                    + "   and pt.PPPackageID =  pp.PPPackageID"
                    + "   and dd.UploadedBy ='Admin' and dd.categoryid IN(4,31) ");
            while (resultSet.next()) {
                int PPPackageID = resultSet.getInt(1);
                if (PPPackageID > 0) {
                    TeqipPackageDocument entity = new TeqipPackageDocument();
                    String category = resultSet.getString(2);
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setDocumentCategory(category);
                    entity.setDocumentfilename(this.getFolderLocation(resultSet.getInt(8), resultSet.getInt(9)) + resultSet.getString(3));
                    entity.setDescription(resultSet.getString(4));
                    entity.setOriginalFileName(resultSet.getString(5));
                    entity.setCreatedOn(resultSet.getDate(10));
                    TeqipUsersDetail get = teqip_users_detail_string_map.get(resultSet.getString(11));
                    if (get != null) {
                        entity.setCreatedBy(get.getUserid());
                    }
                    int bidderID = resultSet.getInt(6);
                    int categoryID = resultSet.getInt(7);

                    if ("Invitation Letter".equalsIgnoreCase(category) && categoryID == 1) {
                        teqip_package_documentdetails_INV_map.put(PPPackageID + "_" + bidderID, entity);
                        entity.setDocumentCategory(MethodConstants.DOC_CAT_INVITATION_WORKS);
                        mapping.add(entity);
                    }
                    if ("Invitation Letter".equalsIgnoreCase(category) && categoryID == 2) {
                        teqip_package_documentdetails_INV_map.put(PPPackageID + "_" + bidderID, entity);
                        entity.setDocumentCategory(MethodConstants.DOC_CAT_INVITATION_GOODS);
                        mapping.add(entity);
                    }
                    if ("Invitation Letter".equalsIgnoreCase(category) && categoryID == 3) {
                        teqip_package_documentdetails_INV_map.put(PPPackageID + "_" + bidderID, entity);
                        entity.setDocumentCategory(MethodConstants.DOC_CAT_INVITATION_QCBS);
                        mapping.add(entity);
                    }

                }

            }

            resultSet = statement.executeQuery("SELECT  pt.PPPackageID, dc.category, FileName, dd.Description, "
                    + "originalFileName, grn.BidderDetailID, pp.ItemCategoryID,  dd.TagID, dd.Categoryid ,uniqueId,"
                    + " dd.createdDate,"
                    + " dd.UploadedBy"
                    + "      FROM tbl_PP_PMSS_Documents dd"
                    + "      INNER JOIN tbl_PM_DocumentCategory dc ON dd.categoryid=dc.Categoryid"
                    + "      INNER JOIN tbl_PP_PMSS_GoodsReceivedNote grn ON  grn.GRNID = dd.uniqueId "
                    + "      INNER JOIN tbl_PP_PMSS_BidderDetails b ON  b.BidderDetailID =   grn.BidderDetailID"
                    + "      INNER JOIN tbl_PP_PMSS_PTPackageDetails pt ON pt.PTPackageID =  grn.PTPackageID "
                    + "      INNER JOIN tbl_PP_PMSS_PPPackageDetails pp ON  pt.PPPackageID =  pp.PPPackageID"
                    + "      where   dd.categoryid IN(5,6)");
            while (resultSet.next()) {
                int PPPackageID = resultSet.getInt(1);
                if (PPPackageID > 0) {
                    TeqipPackageDocument entity = new TeqipPackageDocument();
                    String category = resultSet.getString(2);
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setDocumentCategory(category);
                    entity.setDocumentfilename(this.getFolderLocation(resultSet.getInt(8), resultSet.getInt(9)) + resultSet.getString(3));
                    entity.setDescription(resultSet.getString(4));
                    entity.setOriginalFileName(resultSet.getString(5));
                    entity.setCreatedOn(resultSet.getDate(11));
                    TeqipUsersDetail get = teqip_users_detail_string_map.get(resultSet.getString(12));
                    if (get != null) {
                        entity.setCreatedBy(get.getUserid());
                    }

                    int bidderID = resultSet.getInt(6);
                    int categoryID = resultSet.getInt(7);
                    int GRNID = resultSet.getInt(10);
                    if ("GRN".equalsIgnoreCase(category)) {
                        teqip_package_documentdetails_GRN_map.put(GRNID, entity);
                        entity.setDocumentCategory(MethodConstants.DOC_CAT_GRN_LETTER);
                        mapping.add(entity);

                    }
                    if ("Asset Register".equalsIgnoreCase(category)) {
                        teqip_package_documentdetails_ASSET_map.put(GRNID, entity);
                        entity.setDocumentCategory(MethodConstants.DOC_CAT_ASSET_LETTER);
                        mapping.add(entity);

                    }

                }

            }
            packageDocumentRepository.save(mapping);
            packageDocumentRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
