/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_package_map;
import static com.qspear.procurement.migration.Loader.teqip_package_techcriteria_map;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.repositories.method.RFPDetailRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_rfpdetail)
public class teqip_package_rfpdetail implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    RFPDetailRepository rFPDetailRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT"
                    + "  pt.PTPackageID,"
                    + "  pt.PPPackageID,"
                    + "  pt.TypeOfContract,"
                    + "  pt.BidSubmissionLastDate,"
                    + "  pt.BidSubmissionLastTime,"
                    + "  pt.PrebidDate,"
                    + "  pt.PrebidTime,"
                    + "  pt.PlannedBidopeningLocation,"
                    + "  pt.IsRFPApproved,"
                    + "  pt.RFPApprovalDate"
                    + "  FROM tbl_PP_PMSS_PTPackageDetails pt");
            while (resultSet.next()) {

                TeqipPackageRFPDetail entity = new TeqipPackageRFPDetail();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(2)));
                if (resultSet.getInt(3) != 0) {
                    entity.setTypeOfContract(resultSet.getInt(3) == 2 ? "Lumpsum" : "Timebased");
                }
                entity.setProposalSubmissionDate(Loader.setTimeInDate(resultSet.getDate(4), resultSet.getString(5)));
                entity.setProposalSubmissionDateTime(Loader.setTimeInDate(resultSet.getDate(4), resultSet.getString(5)));
                entity.setPreProposalMeetingDate(Loader.setTimeInDate(resultSet.getDate(6), resultSet.getString(7)));
                entity.setPreProposalMeetingDateTime(Loader.setTimeInDate(resultSet.getDate(6), resultSet.getString(7)));
                entity.setPreProposalVenue(resultSet.getString(8));
                entity.setIsWordWorkBankNOC(resultSet.getInt(9));
                entity.setWordBankNOCDate(resultSet.getDate(10));
                teqip_package_rfpdetail_map.put(resultSet.getInt(1), entity);
            }
            rFPDetailRepository.save(teqip_package_rfpdetail_map.values());
            rFPDetailRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
