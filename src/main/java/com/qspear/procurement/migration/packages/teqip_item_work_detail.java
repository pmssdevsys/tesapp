package com.qspear.procurement.migration.packages;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static com.qspear.procurement.migration.Loader.missingData;
import static com.qspear.procurement.migration.Loader.teqip_categorymaster_map;
import static com.qspear.procurement.migration.Loader.teqip_item_master_map;
import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_categorymaster_map;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipItemWorkDetail;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.repositories.ItemWorkRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

@Repository
@Order(value = Loader.teqip_item_works_detail)
public class teqip_item_work_detail implements PackageLoader {

    @Autowired
    ItemWorkRepository itemWorkRepository;

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " ppitem.PPItemID,"
                    + "  ppitem.item_name,"
                    + " item_description,"
                    + " ppackage.itemcategoryid,"
                    + " dm.DepartmentName,"
                    + " ppitem.item_totalestimatedcost,"
                    + " ppitem.createdby,"
                    + " ppitem.createddate,"
                    + " ppitem.modifiedby,"
                    + " ppitem.modifieddate,"
                    + " ppackage.PPPackageID"
                    + " FROM tbl_PP_PMSS_PPPackageDetails ppackage"
                    + " INNER JOIN  tbl_PP_PMSS_PPItemDetails ppitem ON ppackage.PPPackageID=ppitem.PPPackageID"
                    + " LEFT JOIN tbl_CNF_PMSS_InstitutionDepartmentMapping dm ON ppitem.Item_ProcuringDepartmentID = dm.InstitutionDepartmentMappingID"
            );
            while (resultSet.next()) {

                TeqipCategorymaster get = teqip_categorymaster_map.get(resultSet.getInt(4));

                if (get.getCategoryName().equalsIgnoreCase("Civil Works")) {
                    int id = resultSet.getInt(1);

                    TeqipItemWorkDetail entity = new TeqipItemWorkDetail();

                    entity.setWorkName(resultSet.getString(2));
                    entity.setWorkSpecification(resultSet.getString(3));
                    entity.setTeqipCategorymaster(teqip_categorymaster_map.get(resultSet.getInt(4)));
                    entity.setTeqipPmssDepartmentMaster(teqip_pmssdepartmentmaster_map.get(resultSet.getString(5)));
                    entity.setWorkCost(resultSet.getDouble(6));
                    entity.setCreatedOn(new Timestamp(new Date().getTime()));
                    entity.setCreatedBy(1);
                    entity.setModifyOn(new Timestamp(new Date().getTime()));
                    entity.setModifyBy(1);
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(11)));

                    teqip_item_work_detail_map.put(id, entity);
                }

            }

            itemWorkRepository.save(teqip_item_work_detail_map.values());
            itemWorkRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
