/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_package_map;
import static com.qspear.procurement.migration.Loader.teqip_package_techcriteria_map;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageIssueRFP;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.repositories.method.IssueRFPRepository;
import com.qspear.procurement.repositories.method.RFPDetailRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_issuerfp)
public class teqip_package_issuerfp implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    IssueRFPRepository issueRFPRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT"
                    + "  pt.PTPackageID,"
                    + "  pt.PPPackageID,"
                    + "  pt.ActualPrebidDate, "
                    + "  pt.PrebidMOM,"
                    + "  pt.IsClarificationIssuedToAll,"
                    + "  pt.DateClarificationIssued,"
                    + "  pt.ClarificationDetails,"
                    + "  pt.BidsOpeningDate,"
                    + "  pt.BidsOpeningTime,"
                    + " pt.BidDocumentSaleStartDate,"
                    + " pt.BidDocumentSaleStartTime,"
                    + " pt.LastDateofBidReceipt,"
                    + " pt.LastTimeofBidReceipt,"
                    + " pt.ContractDocSignDate"
                    //currentSubmissionDate
                    //reasonOfExtension
                    //extendedDate
                    + "  FROM tbl_PP_PMSS_PTPackageDetails pt");
            while (resultSet.next()) {

                TeqipPackageIssueRFP entity = new TeqipPackageIssueRFP();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(2)));

                entity.setActualPreProposalDate(resultSet.getDate(3));
                entity.setPreProposalMinutes(resultSet.getString(4));
                entity.setClarificationIssuedToConsultants(resultSet.getInt(5));
                entity.setClarificationIssuedDate(resultSet.getDate(6));
                entity.setClarificationDetails(resultSet.getString(7));
                entity.setTechProposalOpeningDate(Loader.setTimeInDate(resultSet.getDate(8), resultSet.getString(9)));
                entity.setPlannedFinancialOpeningDate(Loader.setTimeInDate(resultSet.getDate(10), resultSet.getString(11)));
                entity.setPlannedFinancialOpeningDateTime(Loader.setTimeInDate(resultSet.getDate(10), resultSet.getString(11)));
                entity.setFinancialOpeningDate(Loader.setTimeInDate(resultSet.getDate(12), resultSet.getString(13)));
                entity.setFinancialOpeningDateTime(Loader.setTimeInDate(resultSet.getDate(12), resultSet.getString(13)));
                entity.setContractSignDate(resultSet.getDate(14));

                teqip_package_issuerfp_map.put(resultSet.getInt(1), entity);
            }
            issueRFPRepository.save(teqip_package_issuerfp_map.values());
            issueRFPRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
