package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_package_map;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipUsersDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageExtension;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrderDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageStage;
import com.qspear.procurement.persistence.methods.TeqipPackageStageIndex;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrder;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.ExtensionRepository;
import com.qspear.procurement.repositories.method.PackageStageIndexRepository;
import com.qspear.procurement.repositories.method.PackageStageRepository;
import com.qspear.procurement.repositories.method.WorkOrderRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_stage_index)
public class teqip_package_stage_index implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PackageStageRepository packageStageRepository;

    @Autowired
    PackageStageIndexRepository packageStageIndexRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageStageIndex> mapping = new ArrayList<>();

        ArrayList<TeqipPackageStage> mapping2 = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("select  pt.PPPackageID,e.EventTime,e.Comments,e.UserID,"
                    + "  (select ProcurementStage from tbl_CNF_PMSS_ProcurementStages where StageID=e.fromstageid) as fromstagename,"
                    + "  (select ProcurementStage from tbl_CNF_PMSS_ProcurementStages where StageID=e.ToStageID) as tostagename"
                    + "  from  tbl_WF_Instance w "
                    + "  inner join tbl_WF_Event e   on e.InstanceID=w.InstanceID "
                    + "  inner join tbl_PP_PMSS_PTPackageDetails pt on pt.PTPackageID = primarykeyvalue"
                    + "  where e.ProcessID !=  '08B40518-E76E-4A0A-9202-45B0292C9920'"
                    + "  order by primarykeyvalue ");
            int packId = 0;
            while (resultSet.next()) {

                int packageID = resultSet.getInt(1);

                if (packId != packageID) {

                    TeqipPackageStageIndex entity = new TeqipPackageStageIndex();
                    entity.setTeqipPackage(teqip_package_map.get(packageID));
                    entity.setCategory("current");
                    
                    String val = resultSet.getString(5);
                    val = "Collect CV".equals(val) ? "Shortlisting" : val;
                    entity.setStageName(val);
                    
                    mapping.add(entity);
                }

                TeqipPackageStageIndex entity = new TeqipPackageStageIndex();
                entity.setTeqipPackage(teqip_package_map.get(packageID));
                entity.setCategory("current");
                
                String val = resultSet.getString(6);
                val = "Collect CV".equals(val) ? "Shortlisting" : val;
                entity.setStageName(val);

                mapping.add(entity);

                TeqipPackageStage entity2 = new TeqipPackageStage();
                entity2.setTeqipPackage(teqip_package_map.get(packageID));
                entity2.setTeqipPackageStageIndex(entity);

                entity2.setActionTime(resultSet.getDate(2));
                entity2.setActionString("Move To Next");
                entity2.setActionType("MOVE_TO_NEXT");
                entity2.setComments(resultSet.getString(3));

                TeqipUsersDetail get = teqip_users_detail_map.get(resultSet.getInt(4));
                if (get != null) {
                    entity2.setActorName((get.getFName() != null ? get.getFName() : "")
                            + " " + (get.getLname() != null ? get.getLname() : ""));
                }

                mapping2.add(entity2);

            }
            packageStageIndexRepository.save(mapping);
            packageStageIndexRepository.flush();

            packageStageRepository.save(mapping2);
            packageStageRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
