package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageAward;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageAwardRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_award)
public class teqip_package_award implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PackageAwardRepository packageAwardRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageAward> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " PPPackageID,"
                    + " L1BidderID,"
                    + " L1Comments"
                    + " FROM tbl_PP_PMSS_PTPackageDetails");
            while (resultSet.next()) {
                TeqipPmssSupplierMaster get = teqip_pmss_suppliermaster_map.get(resultSet.getInt(2));
                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid L1BidderID ::tbl_PP_PMSS_PTPackageDetails ::" + id);

                } else {
                    TeqipPackageAward entity = new TeqipPackageAward();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setTeqipPmssSupplierMaster(get);
                    entity.setAwardComments(resultSet.getString(3));

                    mapping.add(entity);
                }
            }

            packageAwardRepository.save(mapping);
            packageAwardRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
