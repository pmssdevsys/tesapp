package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackagePaymentDetail;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackagePaymentRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_paymentdetail)
public class teqip_package_paymentdetail implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PackagePaymentRepository packagePayementRepository;

    @Autowired
    TeqipPackageRepository packageRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackagePaymentDetail> mapping = new ArrayList<>();
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " m.MilestoneID,"
                    + " p.PPPackageID,"
                    + " m.MilestoneName,"
                    + " m.MilestonePercentage,"
                    + " m.ExpectedCompletionDate,"
                    + " m.ExpectedCompletionPeriod,"
                    + " m.ExpectedPaymentAmount,"
                    + " po.PONumber,"
                    + " po.PODate,"
                    + " m.IsWCCGenerated,"
                    + " m.MilestoneActualCompletionPeriod,"
                    + " m.PaymentDate,"
                    + " m.ChequeOrDraftNumber,"
                    + " m.ExpectedPaymentDate,"
                    + " m.IsWorkCompleted,"
                    + " m.ActualCompletionDate,"
                    + " m.ActualPaymentAmount,"
                    + " m.MilestoneDescription,"
                    + " m.Comments,"
                    + " m.TDS,"
                    + " m.IsLiquidatedDamagesWaived,"
                    + " m.LiquidatedDamagesAmount,"
                    + " m.MaxLiquidatedDamages,"
                    + " m.MilestoneActualCompletionPeriod,"
                    + " '',"
                    + " m.RetentionMoney,"
                    + " m.ActualCompletionPeriodComments,"
                    + " m.PaymentDateComments,"
                    + " m.ActualPaymentAmountComments"
                    + " FROM tbl_PP_PMSS_Milestones m"
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails p  ON p.PTPackageID =  m.PTPackageID "
                    + " LEFT  JOIN tbl_PP_PMSS_PurchaseOrder po ON m.POID =  po.POID");
            while (resultSet.next()) {
                TeqipPackagePaymentDetail entity = new TeqipPackagePaymentDetail();
                
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(2)));
                entity.setPaymentDescription(resultSet.getString(3));
                entity.setMilestonePercentage(resultSet.getDouble(4));
                entity.setExpectedDeliveryDate(resultSet.getDate(5));
                entity.setExpectedDeliveryPeriod(resultSet.getInt(6));
                entity.setExpectedPaymentAmount(resultSet.getDouble(7));
                entity.setPoNumber(resultSet.getString(8));
                entity.setPoDate(resultSet.getDate(9));
                entity.setIsWCCGenerated(resultSet.getInt(10));
                entity.setPaymentDate(resultSet.getString(12));
                entity.setChequeDraftNumber(resultSet.getString(13));
                entity.setExpectedPaymentdate(resultSet.getDate(14));
                entity.setIsWorkCompleted(resultSet.getInt(15));
                entity.setActualCompletionDate(resultSet.getDate(16));
                entity.setActualPaymentAmount(resultSet.getDouble(17));
                entity.setTotalPaymentTillNow(resultSet.getDouble(17));
                entity.setComments(resultSet.getString(18) != null ?resultSet.getString(18): resultSet.getString(19));
                entity.setGst(resultSet.getDouble(20));
                entity.setIsliquidateDamageWaived(resultSet.getInt(21));
                entity.setLiquidatedDamages(resultSet.getDouble(22));
                entity.setExpectedPaymentAmountLD(resultSet.getDouble(23));
                try{
                    
                entity.setMilestoneActualCompletionPeriod(Integer.parseInt(resultSet.getString(24)));
                }catch(Exception e){}
                entity.setRetentionMoney(resultSet.getDouble(26));
                entity.setComment1(resultSet.getString(27));
                entity.setComment2(resultSet.getString(28));
                entity.setComment3(resultSet.getString(29));
                entity.setPaymentMode(entity.getChequeDraftNumber() != null && !entity.getChequeDraftNumber().isEmpty()?"Cheque": null);
                entity.setActualPaymentDate(resultSet.getDate(12));
               
                entity.setActualCompletionPeriod(entity.getMilestoneActualCompletionPeriod());

                
                mapping.add(entity);

            }
            packagePayementRepository.save(mapping);
            packagePayementRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
