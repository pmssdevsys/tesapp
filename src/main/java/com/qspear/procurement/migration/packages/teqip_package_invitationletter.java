package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import static com.qspear.procurement.migration.Loader.missingData;
import static com.qspear.procurement.migration.Loader.teqip_categorymaster_map;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageInvitationLetter;
import com.qspear.procurement.repositories.method.InviationLetterRepository;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_invitationletter)
public class teqip_package_invitationletter implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    InviationLetterRepository inviationLetterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageInvitationLetter> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT pp.PPPackageID, "
                    + " pp.ItemCategoryID, "
                    + " pt.BidValidity,"
                    + " pt.TrainingClause,"
                    + " pt.Warranty,"
                    + " pt.workCompletionPeriod,"
                    + " pt.LiquidatedDamagesPerDay,"
                    + " pt.BidSubmissionLastDate,"
                    + " pt.BidSubmissionLastTime,"
                    + " pt.TestingInstallationClause,"
                    + " pt.AMC,"
                    + " pt.PlannedBidopeningDate,"
                    + " pt.PlannedBidopeningTime,"
                    + " pt.MaxLiquidatedDamages,"
                    + " pt.BasicValue"
                    + " FROM tbl_PP_PMSS_PTPackageDetails pt "
                    + " INNER JOIN tbl_PP_PMSS_PPPackageDetails pp on pp.PPPackageID =  pt.PPPackageID"
            );
            while (resultSet.next()) {
                TeqipCategorymaster get = teqip_categorymaster_map.get(resultSet.getInt(2));
                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid ItemCategoryID ::tbl_PP_PMSS_PTPackageDetails ::" + id);

                } else {
                    TeqipPackageInvitationLetter entity = new TeqipPackageInvitationLetter();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setTeqipCategorymaster(get);
                    entity.setQuotationValidityRequired(resultSet.getInt(3));
                    entity.setTrainingClause(resultSet.getString(4));
                    entity.setWarranty(resultSet.getString(5));
                    entity.setWorkCompletionPeriod(resultSet.getInt(6));
                    entity.setMinliquidatedDamages(resultSet.getDouble(7));
                    entity.setLastSubmissionDate(resultSet.getDate(8));
                    entity.setLastSubmissionTime(Loader.setTimeInDate(resultSet.getDate(8),resultSet.getString(9)));
                    entity.setInstallationClause(resultSet.getString(10));
                    entity.setAmc(resultSet.getString(11));
                    entity.setPlannedBidOpeningDate(resultSet.getDate(12));
                    entity.setPlannedBidOpeningTime(Loader.setTimeInDate(resultSet.getDate(12),resultSet.getString(13)));
                    entity.setMaxliquidatedDamages(resultSet.getDouble(14));
                    entity.setQuotedPrice(resultSet.getDouble(15));
                    mapping.add(entity);
                }
            }

            inviationLetterRepository.save(mapping);
            inviationLetterRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
