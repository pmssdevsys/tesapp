/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrderDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackagePurchaseOrderDetailRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_purchaseorderdetail)
public class teqip_package_purchaseorderdetail implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    private PackagePurchaseOrderDetailRepository packagePurchaseOrderDetailRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackagePurchaseOrderDetail> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(" SELECT "
                    + "     pd.PPPackageID,"
                    + "    po.BidderDetailID,"
                    + "   (SELECT  pi.PPItemID FROM tbl_PP_PMSS_PTItemDetails pi WHERE  pi.PTItemID=d.PTItemID),"
                    + "    d.PO_Quantity,"
                    + "    d.PO_EstimatedCostPerUnit,"
                    + "    d.PO_TotalEstimatedCost,"
                    + "     d.PODetailsID,"
                    + "     po.VATPercent"
                    + "    FROM tbl_PP_PMSS_PurchaseOrderDetails d"
                    + "    INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON  pd.PTPackageID = d.PTPackageID"
                    + "    INNER JOIN tbl_PP_PMSS_PurchaseOrder po  ON po.POID = d.POID");
            while (resultSet.next()) {

                TeqipPmssSupplierMaster get = teqip_pmss_suppliermaster_map.get(resultSet.getInt(2));
                int id = resultSet.getInt(7);

                TeqipItemMaster get1 = teqip_item_master_map.get(resultSet.getInt(3));
                if (get == null) {
                    missingData.add("Invalid BidderDetailID ::tbl_PP_PMSS_PurchaseOrderDetails ::" + id);

                } else if (get1 == null) {
                    missingData.add("Invalid PPItemID ::tbl_PP_PMSS_PurchaseOrderDetails ::" + id);

                } else {
                    TeqipPackagePurchaseOrderDetail entity = new TeqipPackagePurchaseOrderDetail();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setTeqipPmssSupplierMaster(get);
                    entity.setTeqipItemMaster(get1);
                    entity.setComment(get1.getDescription());
                    entity.setItemQty(resultSet.getDouble(4));
                    entity.setBasicCost(resultSet.getDouble(5));
                    entity.setTotalPrice(resultSet.getDouble(6));
                    entity.setGst(resultSet.getDouble(8));
                    mapping.add(entity);
                }
            }

            packagePurchaseOrderDetailRepository.save(mapping);
            packagePurchaseOrderDetailRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
