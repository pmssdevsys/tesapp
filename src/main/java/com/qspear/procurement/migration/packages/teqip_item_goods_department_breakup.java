package com.qspear.procurement.migration.packages;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipItemGoodsDepartmentBreakup;
import com.qspear.procurement.repositories.ItemGoodDepartmentBreakupRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;
import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import java.util.ArrayList;

@Repository
@Order(value = Loader.teqip_item_goods_department_breakup)
public class teqip_item_goods_department_breakup implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    ItemGoodDepartmentBreakupRepository itemGoodDepartmentBreakupRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipItemGoodsDepartmentBreakup> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT   id.PPItemID,  id.DepartmentID,  id.Quantity, d.DepartmentName"
                    + " FROM tbl_PP_PMSS_PPItemDepartmentMapping id"
                    + " LEFT JOIN tbl_CNF_PMSS_InstitutionDepartmentMapping d on id.DepartmentID = d.InstitutionDepartmentMappingID"
            );
            while (resultSet.next()) {

                TeqipItemGoodsDepartmentBreakup entity = new TeqipItemGoodsDepartmentBreakup();
                entity.setTeqipItemGoodsDetail(teqip_item_goods_detail_map.get(resultSet.getInt(1)));
                // 1 entry missing
                entity.setTeqipPmssDepartmentMaster(teqip_pmssdepartmentmaster_map.get(resultSet.getString(4)));
                entity.setQuantity(resultSet.getDouble(3));

                entity.setCreatedOn(new Timestamp(new Date().getTime()));
                entity.setCreatedBy(1);
                entity.setModifyOn(new Timestamp(new Date().getTime()));
                entity.setModifyBy(1);

                mapping.add(entity);

            }
            itemGoodDepartmentBreakupRepository.save(mapping);
            itemGoodDepartmentBreakupRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
