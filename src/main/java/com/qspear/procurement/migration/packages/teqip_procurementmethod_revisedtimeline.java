package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_categorymaster_map;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipUsersMaster;
import com.qspear.procurement.persistence.Teqip_procurement_revisedtimeline;
import com.qspear.procurement.repositories.CategoryRepository;
import com.qspear.procurement.repositories.ProcurementmethodRepository;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.Teqip_procurementmethod_revisedtimelineRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_procurementmethod_revisedtimeline)
public class teqip_procurementmethod_revisedtimeline implements PackageLoader {

    @Autowired
    Teqip_procurementmethod_revisedtimelineRepository teqip_procurementmethod_revisedtimelineRepository;

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<Teqip_procurement_revisedtimeline> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("select "
                    + " pt.ProcurementMethodID,   pp.ItemCategoryID,  rl.BidDocumentPreparationDate,  "
                    + " rl.BankNOCForBiddingDocuments,  rl.BidInvitationDate,  rl.BidOpeningDate,  rl.TORFinalizationDate, "
                    + " rl.AdvertisementDate,  rl.FinalDraftToBeForwardedToTheBankDate,  rl.NoObjectionFromBankForRFP,"
                    + " rl.RFPIssuedDate,  rl.LastDateToReceiveProposals,  rl.EvaluationDate,  rl.NoObjectionFromBankForEvaluation,"
                    + " rl.ContractCompletionDate,  rl.ContractAwardDate,  pp.PPPackageID "
                    + " from  tbl_PP_PMSS_ReviseTimeLine rl "
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pt  ON pt.PTPackageID = rl.PTPackageID  "
                    + " INNER JOIN tbl_PP_PMSS_PPPackageDetails pp ON pp.PPPackageID = pt.PPPackageID"
            );
            while (resultSet.next()) {
                TeqipCategorymaster get1 = teqip_categorymaster_map.get(resultSet.getInt(2));
                int procurementMethodId = resultSet.getInt(1);
                TeqipProcurementmethod get5 = null;
                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("GOODS") >= 0) {
                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_G");

                }
                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("WORKS") >= 0) {
                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_W");

                }
                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("SERVICES") >= 0) {
                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_S");

                }
                Teqip_procurement_revisedtimeline entity = new Teqip_procurement_revisedtimeline();
                entity.setProcurementmethod(get5);
                entity.setTeqipCategorymaster(get1);
                entity.setBidDocumentationPreprationDatedays(resultSet.getDate(3));
                entity.setBankNOCForBiddingDocumentsdays(resultSet.getDate(4));
                entity.setBidInvitationDatedays(resultSet.getDate(5));
                entity.setBidOpeningDatedays(resultSet.getDate(6));
                entity.settORFinalizationDatedays(resultSet.getDate(7));
                entity.setAdvertisementDatedays(resultSet.getDate(8));
                entity.setFinalDraftToBeForwardedToTheBankDatedays(resultSet.getDate(9));
                entity.setNoObjectionFromBankForRFPdays(resultSet.getDate(10));
                entity.setrFPIssuedDatedays(resultSet.getDate(11));
                entity.setLastDateToReceiveProposalsdays(resultSet.getDate(12));
                entity.setEvaluationDatedays(resultSet.getDate(13));
                entity.setNoObjectionFromBankForEvaluationdays(resultSet.getDate(14));
                entity.setContractCompletionDatedays(resultSet.getDate(15));
                entity.setContractAwardDatedays(resultSet.getDate(16));
                entity.settORFinalizationDate_WithoutBankNOCdays(null);
                entity.setAdvertisementDate_WithoutBankNOC_days(null);
                entity.setrFPIssuedDate_WithoutBankNOCdays(null);
                entity.setEvaluationDate_WithoutBankNOCdays(null);
                entity.setContractAwardDate_WithoutBankNOCdays(null);
                entity.setContractCompletionDate_WithoutBankNOC_days(null);
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(17)));
                entity.setLastdateofsubmissionofEOI(null);
                entity.setrFPApprovalDate(null);
                entity.setShortlistingOfEOI(null);
                entity.setFinancialEvaluationDate(null);
                entity.setCreatedOn(new Timestamp(new Date().getTime()));
                entity.setCreatedBy(1);
                entity.setModifiedOn(new Timestamp(new Date().getTime()));
                entity.setModifiedBy(1);
                mapping.add(entity);
            }
            teqip_procurementmethod_revisedtimelineRepository.save(mapping);
            teqip_procurementmethod_revisedtimelineRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
