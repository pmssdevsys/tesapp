package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageBidDetail;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageBidINSTAndSCC;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageBidINSTAndSCCRepository;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_bidinstandscc)
public class teqip_package_bidinstandscc implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipPackageRepository packageRepository;

    @Autowired
    PackageBidINSTAndSCCRepository packageBidINSTAndSCCRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageBidINSTAndSCC> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT "
                    + " pt.PPPackageID,"
                    + " pt.BidChqFavOf,"
                    + " pt.BidChqPayableAt,"
                    + " pt.BidDocumentSaleStartTime,"
                    + " pt.BidDocumentSaleEndTime,"
                    + " pt.BidPostalchargesI,"
                    + " pt.BidSecurity,"
                    + " pt.OfficerInvitingBids, "
                    + " pt.PrebidDate, "
                    + " pt.PrebidTime,"
                    + " pt.DefectCorrectionPeriod,"
                    + " pt.PlaceforArbitration,"
                    + " pt.PurchaserNoticeAddress,"
                    + " pt.LiquidatedDamagesPerDay,"
                    + " pt.MaxLiquidatedDamages "
                    + " FROM tbl_PP_PMSS_PTPackageDetails pt "
                    + " INNER JOIN tbl_PP_PMSS_PPPackageDetails pp on pp.PPPackageID =  pt.PPPackageID"
            );
            while (resultSet.next()) {
                TeqipPackageBidINSTAndSCC entity = new TeqipPackageBidINSTAndSCC();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                entity.setChequeFavour(resultSet.getString(2));
                entity.setPayableAt(resultSet.getString(3));
                entity.setBidsellStartTime(Loader.setTimeInDate(new Date(),resultSet.getString(4)));
                entity.setBidsellEndTime(Loader.setTimeInDate(new Date(),resultSet.getString(5)));
                entity.setPostalCharge(resultSet.getDouble(6));
                entity.setBidSecurity(resultSet.getDouble(7));
                entity.setBidOfficer(resultSet.getString(8));
                entity.setPrebidMeetingDate(resultSet.getDate(9));
                entity.setPrebidmeetingDateTime(Loader.setTimeInDate(resultSet.getDate(9),resultSet.getString(10)));
                try{
                entity.setWarranty(Double.valueOf(resultSet.getString(11)));
                }catch(Exception e){}
                entity.setArbitrationPlace(resultSet.getString(12));
                entity.setAddressofNotice(resultSet.getString(13));
                entity.setMinLiquidatedDamage(resultSet.getDouble(14));
                entity.setMaxLiquidatedDamage(resultSet.getDouble(15));
                mapping.add(entity);
            }
            packageBidINSTAndSCCRepository.save(mapping);
            packageBidINSTAndSCCRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
