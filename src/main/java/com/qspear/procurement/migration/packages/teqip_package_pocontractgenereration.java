package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import static com.qspear.procurement.migration.Loader.missingData;
import static com.qspear.procurement.migration.Loader.teqip_pmss_suppliermaster_map;
import com.qspear.procurement.persistence.methods.TeqipPackagePaymentDetail;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackagePoContractGenereration;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PoContractGenererationRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_pocontractgenereration)
public class teqip_package_pocontractgenereration implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PoContractGenererationRepository poContractGenererationRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackagePoContractGenereration> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT "
                    + " pt.PPPackageID, "
                    + " pt.L1BidderID, "
                    + " pt.LOAGeneratedDate,"
                    + " pt.PTCode,"
                    + " pt.PerfSecRcvdDate,"
                    + " pt.ContractDocSignDate, "
                    + " pt.ContractStartDate, "
                    + " pt.WorkCompletionDate, "
                    + " pt.PerformanceSecurityAmt,"
                    + " pt.IsArbitratorAgreed, "
                    + " pt.PerformanceSecurityInstrument, "
                    + " pt.PerformanceSecurityExpiryDate, "
                    + " 0,"
                    + " po.OctroiAndOthers,"
                     + " pt.BasicValue,"
                    + "  pt.ContractValue"
//                    + " 0,"
//                    + " pt.BasicValue "
                    + " FROM tbl_PP_PMSS_PTPackageDetails pt "
                    + " INNER JOIN tbl_PP_PMSS_PPPackageDetails pp on pp.PPPackageID =  pt.PPPackageID"
                    + " LEFT JOIN tbl_PP_PMSS_PurchaseOrder po ON  po.PTPackageID=pt.PTPackageID");
            while (resultSet.next()) {
                TeqipPmssSupplierMaster get = teqip_pmss_suppliermaster_map.get(resultSet.getInt(2));
                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid L1BidderID ::tbl_PP_PMSS_PTPackageDetails ::" + id);

                } else {
                    TeqipPackagePoContractGenereration entity = new TeqipPackagePoContractGenereration();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setTeqipPmssSupplierMaster(get);                    
                    entity.setLoageneratedDate(resultSet.getDate(3));
                    entity.setPoNumber(resultSet.getString(4));
                    entity.setPerformanceSecurityDate(resultSet.getDate(5));
                    entity.setContractDocsignedDate(resultSet.getDate(6));
                    entity.setContractStartDate(resultSet.getDate(7));
                    entity.setExpectedDeliveryDate(resultSet.getDate(8));
                    entity.setPerfSecurityAmount(resultSet.getDouble(9));
                    if (resultSet.getObject(10) != null) {
                        entity.setArbitratorAgreed(resultSet.getInt(10));
                    }
                   if (resultSet.getObject(11) != null) {
                    entity.setPerfSecurityInstrument(resultSet.getInt(11) == 1? "Bank Guarantee": "Other");
                   }
                    entity.setPerfSecurityExpiryDate(resultSet.getDate(12));
                    entity.setVatPercentage(resultSet.getDouble(13));
                    entity.setOtherOctroi(resultSet.getDouble(14));
                    
                    entity.setEvaluatedPrice(resultSet.getDouble(15));
                    entity.setTotalContractValue(resultSet.getDouble(16)); 
                    
                    Double totalBaseCost = entity.getTotalContractValue();
                    if(entity.getOtherOctroi()!= null && totalBaseCost != null){
                        totalBaseCost =  totalBaseCost - entity.getOtherOctroi() ;
                    }
                    entity.setTotalBaseCost(totalBaseCost);
                    mapping.add(entity);
                }
            }

            poContractGenererationRepository.save(mapping);
            poContractGenererationRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
