package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageQuotationDetailRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_quotationdetail)
public class teqip_package_quotationdetail  implements PackageLoader{
	
	@Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;
	
	@Autowired
	PackageQuotationDetailRepository packageQuotationDetailRepository;
	
	@Autowired
	TeqipPackageRepository packageRepository;

	@Override
	public void load() {
	System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
          ArrayList<TeqipPackageQuotationDetail> mapping = new ArrayList<>();
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(" SELECT"
                    + "  pt.PPPackageID,"
                    + "  pt.bidsopeningdate,"
                    + "  pt.bidsopeningtime,"
                    + "  pt.bidmom "
                    + "  FROM tbl_PP_PMSS_PTPackageDetails pt");
            while (resultSet.next()) {
                
            	TeqipPackageQuotationDetail entity = new TeqipPackageQuotationDetail();
            	entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
            	entity.setQuotationOpeningDate(resultSet.getDate(2));
                //make correction for time
            	entity.setQuotationOpeningTime(Loader.setTimeInDate(resultSet.getDate(2),resultSet.getString(3)));
            	entity.setQuotationMom(resultSet.getString(4));
                
            	 mapping.add(entity);
            }
            packageQuotationDetailRepository.save(mapping);
            packageQuotationDetailRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
		
	}

}
