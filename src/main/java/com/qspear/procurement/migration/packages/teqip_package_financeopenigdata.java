package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import static com.qspear.procurement.migration.Loader.missingData;
import static com.qspear.procurement.migration.Loader.teqip_pmss_suppliermaster_map;
import com.qspear.procurement.persistence.methods.TeqipPackageBidMeetingDetail;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageFinancialOpeningData;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.FinancialOpeningDataRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_financeopenigdata)
public class teqip_package_financeopenigdata implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    FinancialOpeningDataRepository financialOpeningDataRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageFinancialOpeningData> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT "
                    + " pt.PPPackageID, "
                    + " b.BidderDetailID, "
                    + " b.EvaluatedPrice,"
                    + " '',"
                    + " b.TechnicalScore ,"
                    + " b.FinancialScore,"
                    + " b.CombinedScore,"
                    + " b.CombinedRank,"
                    + " b.IsResponsive "
                    + " FROM tbl_PP_PMSS_BidderDetails b "
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pt on pt.PTPackageID =  b.PTPackageID"
            );
            while (resultSet.next()) {
                TeqipPmssSupplierMaster get = teqip_pmss_suppliermaster_map.get(resultSet.getInt(2));
                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid BidderDetailID ::tbl_PP_PMSS_BidderDetails ::" + id);

                } else {
                    TeqipPackageFinancialOpeningData entity = new TeqipPackageFinancialOpeningData();
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                    entity.setTeqipPmssSupplierMaster(get);
                    entity.setPrice(resultSet.getDouble(3));
//                    entity.setTotalScore(resultSet.getDouble(4));
                    entity.setTechnicalScore(resultSet.getDouble(5));
                    entity.setFinancialScore(resultSet.getDouble(6));
                    entity.setCombinedScore(resultSet.getDouble(7));
                    entity.setRank(resultSet.getInt(8));
                    entity.setIsTechnicalResponsive(resultSet.getInt(9));
                    mapping.add(entity);
                }
            }

            financialOpeningDataRepository.save(mapping);
            financialOpeningDataRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
