package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageBidMeetingDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDetail;
import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDistribution;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import com.qspear.procurement.repositories.method.PackageGRNItemDistributionRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_grnitemdistribution)
public class teqip_package_grnitemdistribution implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PackageGRNItemDistributionRepository packageGRNItemDistributionRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageGRNItemDistribution> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " Quantity,"
                    + " dm.DepartmentName,"
                    + " GRNDetailID,"
                    + " GRNItemDistributionID"
                    + " FROM tbl_PP_PMSS_GRNItemDistribution gr"
                    + " LEFT JOIN tbl_CNF_PMSS_InstitutionDepartmentMapping dm ON gr.DepartmentID = dm.InstitutionDepartmentMappingID");
            while (resultSet.next()) {
                TeqipPmssDepartmentMaster get = teqip_pmssdepartmentmaster_map.get(resultSet.getString(2));
                TeqipPackageGRNItemDetail get1 = teqip_package_grnitemdetails_map.get(resultSet.getInt(3));
                int id = resultSet.getInt(4);
                if (get == null) {
                    missingData.add("Invalid DepartmentID ::tbl_PP_PMSS_GRNItemDistribution ::" + id);

                } else if (get1 == null) {
                    missingData.add("Invalid GRNDetailID ::tbl_PP_PMSS_GRNItemDistribution ::" + id);

                } else {
                    TeqipPackageGRNItemDistribution entity = new TeqipPackageGRNItemDistribution();
                    entity.setQuantity(resultSet.getDouble(1));
                    entity.setTeqipPmssDepartmentMaster(get);
                    entity.setTeqipPackageGRNItemDetails(get1);
                    mapping.add(entity);

                }

            }

            packageGRNItemDistributionRepository.save(mapping);
            packageGRNItemDistributionRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
