package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNDetail;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDetail;
import com.qspear.procurement.repositories.method.PackageGRNItemDetailRepository;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_grnitemdetails)
public class teqip_package_grnitemdetails implements PackageLoader {

    @Autowired
    PackageGRNItemDetailRepository packageGRNItemDetailRepository;

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " d.GRNDetailsID,"
                    + " d.PTItemCode,"
                    + " d.Comments,"
                    + " d.ReceivedQuantity,"
                    + " d.StockRegisterName,"
                    + " d.PageNo,"
                    + " d.SerialNo,"
                    + " d.GRNID,"
                    + " (SELECT  pi.PPItemID FROM tbl_PP_PMSS_PTItemDetails pi WHERE  pi.PTItemID=d.PTItemID),"
                    + "  pd.PPPackageID "
                    + " FROM tbl_PP_PMSS_GoodsReceivedNoteDetails d"
                    + " INNER JOIN tbl_PP_PMSS_GoodsReceivedNote gn ON  gn.GRNID=d.GRNID "
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON pd.PTPackageID = gn.PTPackageID "
            );
            while (resultSet.next()) {
                TeqipPackageGRNDetail get = teqip_package_grndetail_map.get(resultSet.getInt(8));
                TeqipItemMaster get1 = teqip_item_master_map.get(resultSet.getInt(9));
                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid GRNID ::tbl_PP_PMSS_GoodsReceivedNoteDetails ::" + id);

                } else if (get1 == null) {
                    missingData.add("Invalid PPItemID ::tbl_PP_PMSS_GoodsReceivedNoteDetails ::" + id);

                } else {
                    TeqipPackageGRNItemDetail entity = new TeqipPackageGRNItemDetail();
                    entity.setItemCode(resultSet.getString(2));
                    entity.setItemComment(resultSet.getString(3));
                    entity.setReceivedQty(resultSet.getDouble(4));
                    entity.setStockRegistorName(resultSet.getString(5));
                    entity.setPageNumber(resultSet.getString(6));
                    entity.setSerialNumber(resultSet.getString(7));
                    entity.setTeqipPackageGRNDetails(get);
                    entity.setTeqipItemMaster(get1);
                    entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));

                    teqip_package_grnitemdetails_map.put(id, entity);
                }
            }

            packageGRNItemDetailRepository.save(teqip_package_grnitemdetails_map.values());
            packageGRNItemDetailRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
