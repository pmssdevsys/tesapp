package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageAmendmentdetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.AmendmentdetailRepository;
import com.qspear.procurement.repositories.method.PackageQuotationDetailRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_amendmentdetail)
public class teqip_package_amendmentdetail implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    AmendmentdetailRepository amendmentdetailRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageAmendmentdetail> mapping = new ArrayList<>();
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("select "
                    + " PPPackageID, "
                    + " IsPOAmmendment,"
                    + " IsPOAmmendmentG,"
                    + " MaintenancePeriodExpDate,"
                    + " PerformanceSecurityAmt,"
                    + " WorkCompletionDate,"
                    + " PerformanceSecurityExpiryDate"
                    + " from  tbl_PP_PMSS_PTPackageDetails ");
            while (resultSet.next()) {

                TeqipPackageAmendmentdetail entity = new TeqipPackageAmendmentdetail();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                if (resultSet.getObject(2) != null) {
                    entity.setIsAmendmentRequired(resultSet.getInt(2));
                }
                if (resultSet.getObject(3) != null) {
                    entity.setIsAmmendmentComplete(resultSet.getInt(3));
                }
                entity.setPeriodExpiryDate(resultSet.getDate(4));
                entity.setPerfSecurityAmount(resultSet.getDouble(5));
                entity.setWorkCompletionDate(resultSet.getDate(6));
                entity.setPerfSecurityExpiryDate(resultSet.getDate(7));

                mapping.add(entity);
            }
            amendmentdetailRepository.save(mapping);
            amendmentdetailRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
