package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageBidAdvertisement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageBidDetail;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageBidDetailRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_biddetail)
public class teqip_package_biddetail implements PackageLoader {

    @Autowired
    PackageBidDetailRepository packageBidDetailRepository;

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipPackageRepository packageRepository;

    @Override
    public void load() {

        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageBidDetail> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT "
                    + " pt.PPPackageID,"
                    + " pt.BidDocumentSaleStartDate,"
                    + " pt.BidSubmissionLastDate,"
                    + " pt.BidSubmissionLastTime,"
                    + " pt.BidDocumentSaleEndDate,"
                    + " pt.PlannedBidopeningDate,"
                    + " pt.PlannedBidopeningTime,"
                    + " pt.BidValidity,"
                    + " pt.BidSecurityValidity,"
                    + " pt.BidPrice "
                    + " FROM tbl_PP_PMSS_PTPackageDetails pt "
                    + " INNER JOIN tbl_PP_PMSS_PPPackageDetails pp on pp.PPPackageID =  pt.PPPackageID");
            while (resultSet.next()) {
                TeqipPackageBidDetail entity = new TeqipPackageBidDetail();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                entity.setSalebidDocument(resultSet.getDate(2));
                entity.setLastreceiptBidDate(Loader.setTimeInDate(resultSet.getDate(3),resultSet.getString(4)));
                entity.setLastSbdDate(resultSet.getDate(5));
                entity.setOpeningDate(Loader.setTimeInDate(resultSet.getDate(6),resultSet.getString(7)));
                entity.setValidityQuotation(resultSet.getInt(8));
                entity.setBidsecurityValidity(resultSet.getInt(9));
                entity.setCostbidDocument(resultSet.getDouble(10));

                mapping.add(entity);
            }

            packageBidDetailRepository.save(mapping);
            packageBidDetailRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
