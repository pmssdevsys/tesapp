package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_package_map;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageExtension;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrderDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrder;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.ExtensionRepository;
import com.qspear.procurement.repositories.method.WorkOrderRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_extension)
public class teqip_package_extension implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    ExtensionRepository extensionRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageExtension> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT pt.PPPackageID, be.ExtensionDate, be.Reason, be.CreatedDate"
                    + " FROM tbl_PP_PMSS_BidExtension be"
                    + " LEFT JOIN tbl_PP_PMSS_PTPackageDetails pt ON be.PTPackageID = pt.PTPackageID;");
            while (resultSet.next()) {
                TeqipPackageExtension entity = new TeqipPackageExtension();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                entity.setExtendedDate(resultSet.getDate(2));
                entity.setReasonforExtension(resultSet.getString(3));
                entity.setSubmissionDate(resultSet.getDate(4));
                mapping.add(entity);
            }
            extensionRepository.save(mapping);
            extensionRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
