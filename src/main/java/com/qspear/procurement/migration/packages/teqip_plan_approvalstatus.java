/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipActivitiymaster;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPlanApprovalstatus;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.TeqipProcurementmaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.TeqipPlanApprovalStatusRepository;
import com.qspear.procurement.repositories.TeqipPmssProcurementstageRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.core.annotation.Order;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_plan_approvalstatus)
public class teqip_plan_approvalstatus implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    protected TeqipPlanApprovalStatusRepository teqipPlanApprovalStatusRepository;

    @Autowired
    protected TeqipPmssProcurementstageRepository teqipPmssProcurementstageRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPlanApprovalstatus> list = new ArrayList<>();
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            String qry = "select i.PPPeriodID,"
                    + " i.InstitutionID,"
                    + " i.SPFUID,"
                    + " e.Comments,"
                    + " i.RevisionNo,"
                    + " (select ProcurementStageID from tbl_CNF_PMSS_ProcurementStages where StageID=e.fromstageid) as fromstagename,"
                    + " (select ProcurementStageID from tbl_CNF_PMSS_ProcurementStages where StageID=e.ToStageID) as tostagename, "
                    + " i.PlanApproved,"
                    + " e.UserID,"
                    + " e.EventTime"
                    + " from dbo.tbl_PP_PMSS_InstitutionPP i ,"
                    + " tbl_CNF_PMSS_ProcurementStages s ,"
                    + " tbl_WF_Instance w ,"
                    + " tbl_WF_Event e "
                    + " where i.CurrentStageID=s.ProcurementStageID "
                    + " and w.stageId=s.stageid "
                    + " and primarykeyvalue= i.PPID"
                    + " and e.InstanceID=w.InstanceID and  e.ProcessID =  '08B40518-E76E-4A0A-9202-45B0292C9920'"
                    + " order by i.InstitutionID,i.SPFUID,InstanceEventID ASC;";
            resultSet = statement.executeQuery(qry);
            int RevisionNo = 1;
            int instId = 0;
            int stateId = 0;
            int PlanApproved = 0;
            TeqipPmssProcurementstage prevStage = null;

            while (resultSet.next()) {

                int currInstId = resultSet.getInt(2);
                int currStateId = resultSet.getInt(3);

                if (!(instId == currInstId && stateId == currStateId) && !list.isEmpty()) {

                    if (PlanApproved == 1) {
                        TeqipPlanApprovalstatus get = list.get(list.size() - 1).clone();
                        int newProcurementstageid = 1;

                        if (PlanApproved == 1 && RevisionNo == 1) {
                            newProcurementstageid = 6;
                        } else {
                            newProcurementstageid = 12;
                        }
                        TeqipPmssProcurementstage stage = teqipPmssProcurementstageRepository.findFirstByProcurementstageidAndRoleIdAndType(
                                newProcurementstageid,
                                get.getTeqipInstitution() != null ? 5 : get.getTeqipStatemaster() != null ? 4 : 10,
                                get.getType());

                        get.setTeqipPmssProcurementstage(stage);
                        get.setCurrentStage(1);

                        list.add(get);

                    }

                    list.get(list.size() - 1).setCurrentStage(1);
                    RevisionNo = 1;
                    prevStage = null;
                }

                instId = currInstId;
                stateId = currStateId;

                TeqipPlanApprovalstatus entity = new TeqipPlanApprovalstatus();

                entity.setTeqipProcurementmaster(teqip_procurementmaster_map.get(resultSet.getInt(1)));
                entity.setTeqipInstitution(instId == 0 ? null : teqip_institutions_map.get(instId));
                entity.setTeqipStatemaster(instId > 0 || stateId == 0 ? null : teqip_statemaster_map.get(stateId));
                
                entity.setType(entity.getTeqipInstitution() != null && entity.getTeqipInstitution().getTeqipInstitutiontype() != null
                        ? entity.getTeqipInstitution().getTeqipInstitutiontype().getInstitutiontypeId() : 0);
                
                entity.setStatusComment(resultSet.getString(4));
                entity.setCurrentStage(0);
                if (teqip_users_detail_map.get(resultSet.getInt(9)) != null) {
                    entity.setCreatedBy(teqip_users_detail_map.get(resultSet.getInt(9)).getUserid());
                }
                entity.setCreatedOn(resultSet.getDate(10));

                int currentStageID = resultSet.getInt(7);
                PlanApproved = resultSet.getInt(8);

                int newProcurementstageid = teqip_procurementstages_map.get(currentStageID);

                // When current stage is Revision
                if (newProcurementstageid == 10) {
                    if (RevisionNo == 1) {
                        newProcurementstageid = 6;
                    } else {
                        newProcurementstageid = 12;
                    }
                    TeqipPmssProcurementstage stage = teqipPmssProcurementstageRepository.findFirstByProcurementstageidAndRoleIdAndType(
                            newProcurementstageid,
                            entity.getTeqipInstitution() != null ? 5 : entity.getTeqipStatemaster() != null ? 4 : 10,
                            entity.getType());

                    TeqipPlanApprovalstatus approvedEntry = entity.clone();
                    approvedEntry.setTeqipPmssProcurementstage(stage);
                    approvedEntry.setTeqipPmssProcurementstage_prev(prevStage);
                    approvedEntry.setReviseid(RevisionNo);

                    list.add(entity);
                    prevStage = stage;
                    RevisionNo++;
                }

                TeqipPmssProcurementstage stage = teqipPmssProcurementstageRepository.findFirstByProcurementstageidAndRoleIdAndType(
                        newProcurementstageid,
                        entity.getTeqipInstitution() != null ? 5 : entity.getTeqipStatemaster() != null ? 4 : 10,
                        entity.getType());

                entity.setTeqipPmssProcurementstage(stage);
                entity.setTeqipPmssProcurementstage_prev(prevStage);
                entity.setReviseid(RevisionNo);

                list.add(entity);
                prevStage = stage;

            }

            teqipPlanApprovalStatusRepository.save(list);
            teqipPlanApprovalStatusRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
