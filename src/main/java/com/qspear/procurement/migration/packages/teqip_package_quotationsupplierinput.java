/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import static com.qspear.procurement.migration.Loader.missingData;
import static com.qspear.procurement.migration.Loader.teqip_pmss_suppliermaster_map;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQuestionMaster;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageQuotationsSupplierInputRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_quotationsupplierinput)
public class teqip_package_quotationsupplierinput implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PackageQuotationsSupplierInputRepository supplierInputRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageQuotationsSupplierInput> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(" SELECT"
                    + "  sq.SupplierID,"
                    + "  pd.PPPackageID,"
                    + "  sq.PTQuestionID,"
                    + "  sq.UserResponse,"
                    + "  sq.IsResponsiveAnswer,"
                    + " sq.PTSupplierQuestionID"
                    + "  FROM tbl_PP_PMSS_PTPackage_SupplierQuestions sq"
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON pd.PTPackageID = sq.PTPackageID");
            while (resultSet.next()) {

                TeqipPmssSupplierMaster get = teqip_pmss_suppliermaster_map.get(resultSet.getInt(1));
                int id = resultSet.getInt(6);
                int packageId = resultSet.getInt(2);

                TeqipPackageQuestionMaster get1 = teqip_package_questionmaster_map.get(resultSet.getInt(3));
                if (get == null) {
                    missingData.add("Invalid SupplierID ::tbl_PP_PMSS_PTPackage_SupplierQuestions ::" + id);

                } else if (get1 == null) {
                    missingData.add("Invalid PTQuestionID ::tbl_PP_PMSS_PTPackage_SupplierQuestions ::" + id);

                } else {
                    TeqipPackageQuotationsSupplierInput entity = new TeqipPackageQuotationsSupplierInput();
                    entity.setTeqipPmssSupplierMaster(get);
                    entity.setTeqipPackage(teqip_package_map.get(packageId));
                    entity.setTeqipPackageQuestionMaster(get1);

                    String userInput = resultSet.getString(4);
                    if ("Date".equalsIgnoreCase(get1.getType())) {
                        try {
                            userInput = String.valueOf(new SimpleDateFormat("dd-MMM-yyyy").parse(userInput).getTime());
                        } catch (Exception e) {
                        }
                    }
                    entity.setUserResponse(userInput ==null || "".equals(userInput.trim())? null : userInput);

                    if ("Readout Price without taxes".equalsIgnoreCase(get1.getQuestionDesc())) {
                        TeqipPackageQuotationsSupplierInput entity1 = entity.clone();
                        entity1.setTeqipPackageQuestionMaster(teqipPackageEvaQuesMaster_map.get(packageId));
                        mapping.add(entity1);
                    }

                    mapping.add(entity);
                }
            }

            supplierInputRepository.save(mapping);
            supplierInputRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
