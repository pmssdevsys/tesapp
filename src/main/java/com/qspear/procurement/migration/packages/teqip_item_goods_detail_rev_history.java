package com.qspear.procurement.migration.packages;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.qspear.procurement.migration.PackageLoader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipItemGoodsDetail;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.repositories.ItemGoodRepository;
import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_categorymaster_map;
import static com.qspear.procurement.migration.Loader.teqip_package_map;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipItemGoodsDetailHistory;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.repositories.ItemGoodHistoryRepository;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.core.annotation.Order;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_item_goods_detail_rev_history)
public class teqip_item_goods_detail_rev_history implements PackageLoader {

    @Autowired
    ItemGoodHistoryRepository itemGoodHistoryRepository;

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " p.PPItemRevisionID,"
                    + "  p.Item_Name,"
                    + " p.Item_Description,"
                    + " pp.ItemCategoryID,"
                    + " p.Item_Quantity,"
                    + " p.Item_EstimatedCostPerUnit,"
                    + " pid.DeliveryPeriod,"
                    + " pid.Specifications,"
                    + " pid.IsTrainingRequired,"
                    + " pid.IsInstallationRequired,"
                    + " pid.PlaceOfDelivery,"
                    + " pid.InstallationRequirements,"
                    + " p.CreatedDate,"
                    + " p.CreatedBy,"
                    + " p.ModifiedDate,"
                    + " p.ModifiedBy,"
                    + " p.PPPackageID,"
                    + " p.PPRevisionID,"
                    + " p.PPItemID"
                    + " FROM tbl_PP_PMSS_PPItemDetails_Revision p"
                    + " INNER JOIN tbl_PP_PMSS_PPPackageDetails pp ON pp.PPPackageID =  p.PPPackageID"
                    + " LEFT JOIN  tbl_PP_PMSS_PTItemDetails pid ON pid.PPItemID = p.PPItemID"
            );
            while (resultSet.next()) {
                TeqipItemMaster item = teqip_item_master_map.get(resultSet.getInt(1));
                TeqipCategorymaster get = teqip_categorymaster_map.get(resultSet.getInt(4));
                TeqipPackage get1 = teqip_package_map.get(resultSet.getInt(17));

                if (get1 != null && get.getCategoryName().equalsIgnoreCase("Goods")) {
                    int id = resultSet.getInt(1);

                    TeqipItemGoodsDetailHistory entity = new TeqipItemGoodsDetailHistory();

                    entity.setTeqipItemMaster(item);
                    entity.setItemSpecification(resultSet.getString(3));
                    entity.setTeqipCategorymaster(teqip_categorymaster_map.get(resultSet.getInt(4)));
                    entity.setItemQnt(resultSet.getDouble(5));
                    entity.setItemCostUnit(resultSet.getDouble(6));
                    entity.setDeliveyPeriod(resultSet.getInt(7));
                    entity.setItemMainSpecification(resultSet.getString(8));
                    entity.setTrainingRequired(resultSet.getInt(9));
                    entity.setInstallationRequired(resultSet.getInt(10));
                    entity.setPlaceOfDelivery(resultSet.getString(11));
                    entity.setInstllationRequirement(resultSet.getString(12));
                    entity.setCreatedOn(new Timestamp(new Date().getTime()));
                    entity.setCreatedBy(1);
                    entity.setModifyOn(new Timestamp(new Date().getTime()));
                    entity.setModifyBy(1);
                    entity.setTeqipPackage(get1);
                    entity.setTeqipPackageHistory(teqip_package_rev_history_map.get(resultSet.getInt(18)));
                    entity.setTeqipItemGoodsDetail(teqip_item_goods_detail_map.get(resultSet.getInt(19)));

                    teqip_item_goods_detail_rev_history_map.put(id, entity);
                }
            }

            itemGoodHistoryRepository.save(teqip_item_goods_detail_rev_history_map.values());
            itemGoodHistoryRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
