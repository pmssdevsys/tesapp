/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import static com.qspear.procurement.migration.Loader.missingData;
import static com.qspear.procurement.migration.Loader.teqip_pmss_suppliermaster_map;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_supplierdetail)
public class teqip_package_supplierdetail implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageSupplierDetail> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("  SELECT"
                    + "  pd.PPPackageID ,"
                    + "  bd.BidderDetailID,"
                    + "  bd.GeneralComments,"
                    + "  bd.IsMailSent,"
                    + "  bd.IsCommerciallyResponsive"
                    + "  FROM tbl_PP_PMSS_BidderDetails bd"
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON pd.PTPackageID = bd.PTPackageID ");
            while (resultSet.next()) {

                 int packId = resultSet.getInt(1);
                int bidderid = resultSet.getInt(2);
                
                TeqipPmssSupplierMaster get = teqip_pmss_suppliermaster_map.get(bidderid);
                if (get == null) {
                    missingData.add("Invalid BidderDetailID ::tbl_PP_PMSS_BidderDetail ::" + bidderid);

                } else {
                    TeqipPackageSupplierDetail entity = new TeqipPackageSupplierDetail();
                  
                    entity.setTeqipPackage(teqip_package_map.get(packId));
                    entity.setTeqipPmssSupplierMaster(get);
                    entity.setComments(resultSet.getString(3));
                    if(resultSet.getObject(4) != null){
                      entity.setIsEmailSent(resultSet.getInt(4));
                    }
                    if(resultSet.getObject(5) != null){
                      entity.setIsShortListed(resultSet.getInt(5));
                    }
                    entity.setInvitationDocument(teqip_package_documentdetails_INV_map.get(packId+"_"+bidderid));
                    mapping.add(entity);
                }
            }
            packageSupplierRepository.save(mapping);
            packageSupplierRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
