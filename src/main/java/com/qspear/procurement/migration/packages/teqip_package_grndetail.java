package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageGRNDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageGRNDetailRepository;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_grndetail)
public class teqip_package_grndetail implements PackageLoader {

    @Autowired
    PackageGRNDetailRepository packageGRNDetailRepository;

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " grnNote.GRNID,"
                    + "  pd.PPPackageID ,"
                    + " grnNote.BidderDetailID,"
                    + " grnNote.ReceivedOn"
                    + " FROM tbl_PP_PMSS_GoodsReceivedNote grnNote"
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON  pd.PTPackageID = grnNote.PTPackageID "
            );
            while (resultSet.next()) {
                int BidderDetailID = resultSet.getInt(3);
                int GRNID = resultSet.getInt(1);
                int PPPackageID = resultSet.getInt(2);

                TeqipPmssSupplierMaster get = teqip_pmss_suppliermaster_map.get(BidderDetailID);
                if (get == null) {
                    missingData.add("Invalid BidderDetailID ::tbl_PP_PMSS_GoodsReceivedNote ::" + GRNID);

                } else {
                    TeqipPackageGRNDetail entity = new TeqipPackageGRNDetail();
                    entity.setTeqipPackage(teqip_package_map.get(PPPackageID));
                    entity.setTeqipPmssSupplierMaster(get);
                    entity.setSupplyDate(resultSet.getDate(4));

                    entity.setGrnDocument(teqip_package_documentdetails_GRN_map.get(GRNID));
                    entity.setAssetDocument(teqip_package_documentdetails_ASSET_map.get(GRNID));

                    teqip_package_grndetail_map.put(GRNID, entity);

                }
            }

            packageGRNDetailRepository.save(teqip_package_grndetail_map.values());
            packageGRNDetailRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
