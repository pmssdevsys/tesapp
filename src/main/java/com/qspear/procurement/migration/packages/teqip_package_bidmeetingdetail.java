package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageBidMeetingDetail;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageBidMeetingDetailRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_bidmeetingdetail)
public class teqip_package_bidmeetingdetail implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipPackageRepository packageRepository;

    @Autowired
    PackageBidMeetingDetailRepository packageBidMeetingDetailRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageBidMeetingDetail> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT "
                    + " pt.PPPackageID,"
                    + " pt.ActualPrebidDate, "
                    + " '', "
                    + " pt.IsClarificationIssuedToAll, "
                    + " pt.DateClarificationIssued,"
                    + " pt.PrebidTime,"
                    + " pt.PrebidMOM "
                    + " FROM tbl_PP_PMSS_PTPackageDetails pt "
                    + " INNER JOIN tbl_PP_PMSS_PPPackageDetails pp on pp.PPPackageID =  pt.PPPackageID"
            );
            while (resultSet.next()) {
                TeqipPackageBidMeetingDetail entity = new TeqipPackageBidMeetingDetail();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                entity.setActualprebidMeetingDate(resultSet.getDate(2));
//                entity.setAnyCorrigendum(resultSet.getInt(3));
                if(resultSet.getObject(4) != null){
                    entity.setIsclarificationIssues(resultSet.getInt(4));
                }
                entity.setClarificationprebidMeetingDate(resultSet.getDate(5));
                entity.setPrebidMinutes(resultSet.getDate(5));
                entity.setPrebidMom(resultSet.getString(7));
                mapping.add(entity);
            }
            packageBidMeetingDetailRepository.save(mapping);
            packageBidMeetingDetailRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
