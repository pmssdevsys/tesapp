package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrderDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrder;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.WorkOrderRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_workorder)
public class teqip_package_workorder implements PackageLoader{
	
	@Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;
	
	@Autowired
	WorkOrderRepository workOrderRepository;
	
	@Autowired
	TeqipPackageRepository packageRepository;

	@Override
	public void load() {
		System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
                ArrayList<TeqipPackageWorkOrder> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(" SELECT"
                    + "  pt.PPPackageID,"
                    + "  perfSecRcvdDate,"
                    + "  contractStartDate,"
                    + "  workCompletionDate,"
                    + "  PerformanceSecurityExpiryDate,"
                    + "  workStartDate,"
                    + "  BasicValue,"
                    + "  ApplicableTax,"
                    + "  POGeneratedDate,"
                    + "  ContractValue,"
                    + "  PONo,"
                    + " PerformanceSecurityAmt"
                    + "  FROM tbl_PP_PMSS_PTPackageDetails PT ");
            while (resultSet.next()) {
            	TeqipPackageWorkOrder entity = new TeqipPackageWorkOrder();
            	entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                //workorder
            	entity.setPerfsecurityrcvd(resultSet.getDate(2));
            	entity.setPerfsecurityexpdate(resultSet.getDate(5));
                entity.setContractstartdate(resultSet.getDate(3));
            	entity.setWorkstartdate(resultSet.getDate(6));
                entity.setWorkcompletiondate(resultSet.getDate(4));
            	entity.setBasicvalue(resultSet.getDouble(7));
            	entity.setSumapplicable(resultSet.getDouble(8));
            	entity.setContractvalue(resultSet.getDouble(10));
                entity.setWorkGeneratedDate(resultSet.getDate(9));
                entity.setWorkOrderNo(resultSet.getString(11));
                entity.setPerformanceSecurityAmount(resultSet.getDouble(12));
                //performance security Amount.
                    mapping.add(entity);
            }
            workOrderRepository.save(mapping);
            workOrderRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
		
	}

}
