package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageAmendmentdetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractGeneration;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.AmendmentdetailRepository;
import com.qspear.procurement.repositories.method.PackageQuotationDetailRepository;
import com.qspear.procurement.repositories.method.QCBSContractGenerationRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_qcbs_contractgeneration)
public class teqip_package_qcbs_contractgeneration implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    QCBSContractGenerationRepository repository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageQCBSContractGeneration> mapping = new ArrayList<>();
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("select "
                    + " pt.PPPackageID,"
                    + " pt.L1BidderID,"
                    + " pt.L1Comments,"
                    + " pt.ContractValue,"
                    + " pt.DateOfNegociation,"
                    + " pt.NegociationComments,"
                    + " pt.IsNegoApproved,"
                    + " l2.L2Bidder,"
                    + " l2.L2Comments"
                    + " from  tbl_PP_PMSS_PTPackageDetails pt"
                    + " LEFT JOIN tbl_PP_PMSS_L2Bidder l2 ON pt.PTPackageId = l2.PTPackageId ");
            while (resultSet.next()) {

                TeqipPackageQCBSContractGeneration entity = new TeqipPackageQCBSContractGeneration();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                
                if (resultSet.getObject(8) != null && resultSet.getInt(8) != 0) {
                    entity.setTeqipPmssSupplierMaster(teqip_pmss_suppliermaster_map.get(resultSet.getInt(8)));
                } else {
                    entity.setTeqipPmssSupplierMaster(teqip_pmss_suppliermaster_map.get(resultSet.getInt(2)));
                }
                if (resultSet.getObject(9) != null ) {
                    entity.setRecommendationComments(resultSet.getString(9));
                } else {
                    entity.setRecommendationComments(resultSet.getString(3));
                }
                entity.setContractValue(resultSet.getDouble(4));
                entity.setDateOfNegotiation(resultSet.getDate(5));
                entity.setNegotiationComments(resultSet.getString(6));
                entity.setIsNegotiationSuccessful(resultSet.getInt(7));

                mapping.add(entity);
            }
            repository.save(mapping);
            repository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
