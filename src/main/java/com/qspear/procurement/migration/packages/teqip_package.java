/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipActivitiymaster;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.TeqipProcurementmaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.TeqipPmssProcurementstageRepository;
import java.util.HashMap;
import java.util.List;
import org.springframework.core.annotation.Order;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package)
public class teqip_package implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    private TeqipPackageRepository teqipPackageRepository;

    @Autowired
    protected TeqipPmssProcurementstageRepository teqipPmssProcurementstageRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        /**
         * @key : ProcurmentMethodId_ItemCategoryID_ProcurementStageID
         * @value : TabIndex
         */
        HashMap<String, Integer> map = new HashMap<>();

        // NCB GOODS
        map.put("2_2_20", 1);
        map.put("2_2_21", 2);
        map.put("2_2_22", 3);
        map.put("2_2_23", 4);
        map.put("2_2_24", 5);
        map.put("2_2_25", 6);
        map.put("2_2_26", 7);
        map.put("2_2_29", 7);

        // NCB WORKS
        map.put("2_1_20", 1);
        map.put("2_1_21", 2);
        map.put("2_1_22", 3);
        map.put("2_1_23", 4);
        map.put("2_1_24", 5);
        map.put("2_1_27", 6);
        map.put("2_1_28", 7);
        map.put("2_1_29", 7);
        
        // SHOPPING GOODS
        map.put("3_2_10", 1);
        map.put("3_2_11", 2);
        map.put("3_2_12", 3);
        map.put("3_2_13", 4);
        map.put("3_2_14", 5);
        map.put("3_2_15", 6);
        map.put("3_2_19", 6);

        // SHOPPING WORKS
        map.put("3_1_10", 1);
        map.put("3_1_11", 2);
        map.put("3_1_12", 3);
        map.put("3_1_13", 4);
        map.put("3_1_16", 5);
        map.put("3_1_17", 6);
        map.put("3_1_18", 7);
        map.put("3_1_19", 7);

        // DC GOODS
        map.put("4_2_10", 1);
        map.put("4_2_14", 2);
        map.put("4_2_15", 3);
        map.put("4_2_19", 3);

        // DC WORKS
        map.put("4_1_10", 1);
        map.put("4_1_16", 2);
        map.put("4_1_17", 3);
        map.put("4_1_18", 4);
        map.put("4_1_19", 4);


        //QCBS Service
        map.put("7_3_30", 1);
        map.put("7_3_31", 2);
        map.put("7_3_32", 3);
        map.put("7_3_33", 3);
        map.put("7_3_34", 3);
        map.put("7_3_35", 4);
        map.put("7_3_36", 4);
        map.put("7_3_37", 5);
        map.put("7_3_38", 6);
        map.put("7_3_39", 6);
        map.put("7_3_40", 7);
        map.put("7_3_41", 7);
        map.put("7_3_42", 8);
        map.put("7_3_43", 8);
        map.put("7_3_44", 9);
        map.put("7_3_45", 9);

        //LCS Service
        map.put("9_3_30", 1);
        map.put("9_3_31", 2);
        map.put("9_3_32", 3);
        map.put("9_3_33", 3);
        map.put("9_3_34", 3);
        map.put("9_3_35", 4);
        map.put("9_3_36", 4);
        map.put("9_3_37", 5);
        map.put("9_3_38", 6);
        map.put("9_3_39", 6);
        map.put("9_3_40", 7);
        map.put("9_3_41", 7);
        map.put("9_3_42", 8);
        map.put("9_3_43", 8);
        map.put("9_3_44", 9);
        map.put("9_3_45", 9);

        //CQS Service
        map.put("10_3_30", 1);
        map.put("10_3_31", 2);
        map.put("10_3_32", 3);
        map.put("10_3_33", 3);
        map.put("10_3_34", 3);
        map.put("10_3_35", 4);
        map.put("10_3_36", 4);
        map.put("10_3_37", 5);
        map.put("10_3_38", 6);
        map.put("10_3_39", 6);
        map.put("10_3_40", 7);
        map.put("10_3_41", 7);
        map.put("10_3_42", 8);
        map.put("10_3_43", 8);
        map.put("10_3_44", 9);
        map.put("10_3_45", 9);
        
          //SSS Service
        map.put("11_3_30", 1);
        map.put("11_3_35", 2);
        map.put("11_3_36", 2);
        map.put("11_3_60", 2);
        map.put("11_3_37", 3);
        map.put("11_3_42", 4);
        map.put("11_3_43", 4);
        map.put("11_3_58", 4);
        map.put("11_3_44", 5);
        map.put("11_3_45", 5);
        
          //Indv Consultant Comp.
        map.put("13_3_76", 1);
        map.put("13_3_77", 2);
        map.put("13_3_78", 3);
        map.put("13_3_79", 4);
        map.put("13_3_80", 4);
        
         //Indv Consultant SSS
        map.put("14_3_76", 1);
        map.put("14_3_77", 2);
        map.put("14_3_78", 3);
        map.put("14_3_79", 4);
        map.put("14_3_80", 4);

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT "
                    + " p.PPPackageID,"
                    + " ipp.PPPeriodID,"
                    + " p.PackageCode,"
                    + " p.PackageName,"
                    + " p.Description,"
                    + " p.ItemCategoryID,"
                    + " p.ItemSubcategoryID,"
                    + " ipp.SubComponentID,"
                    + " p.ActivityID,"
                    + " p.EstimatedFinancialSanctionDate,"
                    + " p.IsProprietary,"
                    + " p.IsCOE,"
                    + " p.ProcurementMethodID,"
                    + " p.ServiceProvider,"
                    + " p.TotalEstimatedCost,"
                    + " p.RevisionComments,"
                    + " ipp.InstitutionID,"
                    + " ipp.SPFUID,"
                    + " ptp.InitiationDate,"
                    + " ipp.CurrentStageID,"
                    + " ptp.ProcurementStageID,"
                    + " ptp.PTPackageID,"
                    + "	(SELECT COUNT(*) FROM tbl_PP_PMSS_PPPackageDetails_Revision WHERE PPPackageID = p.PPPackageID) as rev,"
                    + " ipp.RevisionNo,"
                    + " ps.ProcurementStage,"
                    + " p.IsDGSG,"
                    + " ptp.ActualFinancialSanctionDate"
                    + " FROM tbl_PP_PMSS_PPPackageDetails p "
                    + " INNER JOIN tbl_PP_PMSS_InstitutionPP ipp ON ipp.PPID=p.PPID "
                    + " LEFT JOIN tbl_PP_PMSS_PTPackageDetails ptp ON   ptp.PPPackageID = p.PPPackageID"
                    + " LEFT JOIN tbl_CNF_PMSS_ProcurementStages ps ON ptp.ProcurementStageID= ps.ProcurementStageID"
            );
            while (resultSet.next()) {
                TeqipProcurementmaster get = teqip_procurementmaster_map.get(resultSet.getInt(2));
                int ItemCategoryID = resultSet.getInt(6);
                TeqipCategorymaster get1 = teqip_categorymaster_map.get(ItemCategoryID);
                TeqipSubcategorymaster get2 = teqip_subcategorymaster_map.get(resultSet.getInt(7));
                TeqipActivitiymaster get4 = teqip_activitiymaster_map.get(resultSet.getInt(9));
                int procurementMethodId = resultSet.getInt(13);
                TeqipProcurementmethod get5 = null;
                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("GOODS") >= 0) {
                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_G");

                }
                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("WORKS") >= 0) {
                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_W");

                }
                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("SERVICES") >= 0) {
                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_S");

                }
                int id = resultSet.getInt(1);

                TeqipPackage entity = new TeqipPackage();
                entity.setTeqipProcurementmaster(get);
                entity.setPackageCode(resultSet.getString(3));
                entity.setPackageName(resultSet.getString(4));
                entity.setJustification(resultSet.getString(5));
                entity.setTeqipCategorymaster(get1);
                entity.setTeqipSubcategorymaster(get2);
                entity.setTeqipSubcomponentmaster(teqip_subcomponentmaster_map.get(resultSet.getInt(8)));
                entity.setTeqipActivitiymaster(get4);
                entity.setFinancialSanctionDate(resultSet.getDate(10));
                entity.setIsprop(resultSet.getBoolean(11));
                entity.setIscoe(resultSet.getBoolean(12));
                entity.setIsgem(resultSet.getBoolean(26));
                entity.setTeqipProcurementmethod(get5);
                entity.setServiceProviderId(resultSet.getInt(14));
                entity.setEstimatedCost(resultSet.getDouble(15));
                entity.setRevisionComments(resultSet.getString(16));
                entity.setTeqipInstitution(resultSet.getInt(17) == 0 ? null : teqip_institutions_map.get(resultSet.getInt(17)));
                entity.setTeqipStatemaster(resultSet.getInt(17) > 0 || resultSet.getInt(18) == 0 ? null : teqip_statemaster_map.get(resultSet.getInt(18)));
                entity.setInitiateDate(resultSet.getDate(27));
                entity.setPackageInitiationDate(resultSet.getDate(19));
                entity.setIsOldPackage(1);
                        
                int CurrentStageID = resultSet.getInt(20);
                int ProcurementStageID = resultSet.getInt(21);
                int PTPackageID = resultSet.getInt(22);
                int RevisionNo = resultSet.getInt(23);
                int CurrRevisionNo = resultSet.getInt(24);
                String currentStage = resultSet.getString(25);
                currentStage = currentStage == null ? "" : currentStage;
                Integer pageIndex = map.get(procurementMethodId + "_" + ItemCategoryID + "_" + ProcurementStageID);
                pageIndex = pageIndex == null || pageIndex == 0 ? 1 : pageIndex;

                String stateCheckList = "\"stateCheckList\": {\"currentStageIndexString\": \"" + pageIndex + "\"},"
                        + "\"currentStage\": \"" + currentStage + "\","
                        + "\"currentStageIndex\": " + pageIndex + "";

                entity.setCurrentStage(currentStage);
                entity.setIsInitiated(ProcurementStageID > 0 ? 1 : 0);

                if (ProcurementStageID == 52
                        || ProcurementStageID == 53
                        || ProcurementStageID == 54
                        || ProcurementStageID == 81) {
                    entity.setIsPackageCancelled(1);
                    stateCheckList += ",\"isCanceled\":1";
                } else {
                    entity.setIsPackageCancelled(0);

                }

                if (ProcurementStageID == 19
                        || ProcurementStageID == 29
                        || ProcurementStageID == 45
                        || ProcurementStageID == 80) {
                    entity.setIsPackageCompleted(1);
                    stateCheckList += ",\"isCompleted\":1";
                } else {
                    entity.setIsPackageCompleted(0);

                }
                entity.setPackageMetaData("{" + stateCheckList + "}");

                int newProcurementstageid = 1;

                if (PTPackageID > 0 && RevisionNo == 0) {
                    newProcurementstageid = 6;

                } else if (PTPackageID > 0 && RevisionNo > 0) {
                    newProcurementstageid = 12;
                } else {
                    newProcurementstageid = teqip_procurementstages_map.get(CurrentStageID);
                }

                TeqipPmssProcurementstage stage = teqipPmssProcurementstageRepository.findFirstByProcurementstageidAndRoleIdAndType(
                        newProcurementstageid,
                        entity.getTeqipInstitution() != null ? 5 : entity.getTeqipStatemaster() != null ? 4 : 10,
                        entity.getTeqipInstitution() != null && entity.getTeqipInstitution().getTeqipInstitutiontype() != null
                        ? entity.getTeqipInstitution().getTeqipInstitutiontype().getInstitutiontypeId() : 0);
                
                entity.setTeqipPmssProcurementstage(stage);
                entity.setTypeofplanCreator(entity.getTeqipInstitution() != null && entity.getTeqipInstitution().getTeqipInstitutiontype() != null
                        ? entity.getTeqipInstitution().getTeqipInstitutiontype().getInstitutiontypeId() : 0);

                entity.setReviseId((CurrRevisionNo - RevisionNo) + 1);
                teqip_package_map.put(id, entity);

            }

            teqipPackageRepository.save(teqip_package_map.values());
            teqipPackageRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
