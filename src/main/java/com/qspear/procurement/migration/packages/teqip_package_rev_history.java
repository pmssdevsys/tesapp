/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_procurementstages_map;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipActivitiymaster;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPackageHistory;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.TeqipProcurementmaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import com.qspear.procurement.repositories.TeqipPackageHistoryRepository;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.TeqipPmssProcurementstageRepository;
import java.util.List;
import org.springframework.core.annotation.Order;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_rev_history)
public class teqip_package_rev_history implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;
    @Autowired
    protected TeqipPmssProcurementstageRepository teqipPmssProcurementstageRepository;

    @Autowired
    private TeqipPackageHistoryRepository packageHistoryRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT "
                    + " p.PPRevisionID,"
                    + " ipp.PPPeriodID,"
                    + " p.PackageCode,"
                    + " p.PackageName,"
                    + " p.Description,"
                    + " p.ItemCategoryID,"
                    + " p.ItemSubcategoryID,"
                    + " p.SubcomponentID,"
                    + " p.ActivityID,"
                    + " p.EstimatedFinancialSanctionDate,"
                    + " p.IsProprietary,"
                    + " '',"
                    + " p.ProcurementMethodID,"
                    + " p.ServiceProvider,"
                    + " p.TotalEstimatedCost,"
                    + " p.RevisionComments,"
                    + " ipp.InstitutionID,"
                    + " ipp.SPFUID,"
                    + " ptp.InitiationDate,"
                    + " p.PPPackageID,"
                    + " ipp.CurrentStageID,"
                    + " (select count(*) from tbl_PP_PMSS_InstitutionPP_Revision pr where pr.PPID = ipp.PPID  and pr.PPRevisionID < p.PPRevisionID)"
                    + " FROM tbl_PP_PMSS_PPPackageDetails_Revision p "
                    + " INNER JOIN tbl_PP_PMSS_InstitutionPP_Revision ippr ON ippr.PPRevisionID=p.PPRevisionID "
                    + " INNER JOIN tbl_PP_PMSS_InstitutionPP ipp ON ipp.PPID=ippr.PPID "
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails ptp ON   ptp.PPPackageID = p.PPPackageID");
            while (resultSet.next()) {
                TeqipProcurementmaster get = teqip_procurementmaster_map.get(resultSet.getInt(2));
                TeqipCategorymaster get1 = teqip_categorymaster_map.get(resultSet.getInt(6));
                TeqipSubcategorymaster get2 = teqip_subcategorymaster_map.get(resultSet.getInt(7));
                TeqipActivitiymaster get4 = teqip_activitiymaster_map.get(resultSet.getInt(9));
                int procurementMethodId = resultSet.getInt(13);
                TeqipProcurementmethod get5 = null;
                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("GOODS") >= 0) {
                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_G");

                }
                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("WORKS") >= 0) {
                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_W");

                }
                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("SERVICES") >= 0) {
                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_S");

                }
                int id = resultSet.getInt(1);

                TeqipPackageHistory entity = new TeqipPackageHistory();
                entity.setTeqipProcurementmaster(get);
                entity.setPackageCode(resultSet.getString(3));
                entity.setPackageName(resultSet.getString(4));
                entity.setJustification(resultSet.getString(5));
                entity.setTeqipCategorymaster(get1);
                entity.setTeqipSubcategorymaster(get2);
                entity.setTeqipSubcomponentmaster(teqip_subcomponentmaster_map.get(resultSet.getInt(8)));
                entity.setTeqipActivitiymaster(get4);
                entity.setFinancialSanctionDate(resultSet.getDate(10));
                entity.setIsprop(resultSet.getBoolean(11));
//                entity.setIscoe(resultSet.getBoolean(12));
                entity.setTeqipProcurementmethod(get5);
                entity.setServiceProviderId(resultSet.getInt(14));
                entity.setEstimatedCost(resultSet.getDouble(15));
                entity.setRevisionComments(resultSet.getString(16));
                entity.setTeqipInstitution(resultSet.getInt(17) == 0 ? null : teqip_institutions_map.get(resultSet.getInt(17)));
                entity.setTeqipStatemaster(resultSet.getInt(17) > 0 || resultSet.getInt(18) == 0 ? null : teqip_statemaster_map.get(resultSet.getInt(18)));
                entity.setInitiateDate(resultSet.getDate(19));
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(20)));
                TeqipPmssProcurementstage stage = teqipPmssProcurementstageRepository.findFirstByProcurementstageidAndRoleIdAndType(
                        teqip_procurementstages_map.get(resultSet.getInt(21)),
                        entity.getTeqipInstitution() != null ? 5 : entity.getTeqipStatemaster() != null ? 4 : 10,
                        entity.getTeqipInstitution() != null && entity.getTeqipInstitution().getTeqipInstitutiontype() != null
                        ? entity.getTeqipInstitution().getTeqipInstitutiontype().getInstitutiontypeId() : 1);
                entity.setTeqipPmssProcurementstage(stage);
                entity.setReviseId(resultSet.getInt(22));

                teqip_package_rev_history_map.put(id, entity);

            }

            packageHistoryRepository.save(teqip_package_rev_history_map.values());
            packageHistoryRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
