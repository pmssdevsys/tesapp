/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageAward;
import com.qspear.procurement.persistence.methods.TeqipPackageBidAdvertisement;
import com.qspear.procurement.repositories.method.PackageBidAdvertisementRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_bidadvertisement)
public class teqip_package_bidadvertisement implements PackageLoader {
    
    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;
    
    @Autowired
    private PackageBidAdvertisementRepository packageBidAdvertisementRepository;
    
    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipPackageBidAdvertisement> mapping = new ArrayList<>();
        
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            
            resultSet = statement.executeQuery("  SELECT "
                    + "  PPPackageID,"
                    + "  AdvertisementDetails,"
                    + "  LocalPaperDetails,"
                    + "  NationalPaperDetails,"
                    + "  AdvertisementDate,"
                    + "  ActualPublicationDate_Local,"
                    + "  ActualPublicationDate_National,"
                    + "  EOISubmissionLastDate"
                    + "  FROM tbl_PP_PMSS_PTPackageDetails");
            while (resultSet.next()) {
                
                TeqipPackageBidAdvertisement entity = new TeqipPackageBidAdvertisement();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                entity.setSelectPackage(resultSet.getString(2));
                entity.setLocalPaperName(resultSet.getString(3));
                entity.setNationalPaperName(resultSet.getString(4));
                entity.setAdvertisementDate(resultSet.getDate(5));
                entity.setActualpublicationLocaleDate(resultSet.getDate(6));
                entity.setActualpublicationNationalDate(resultSet.getDate(7));
                entity.setEoiSubmissionDate(resultSet.getDate(8));
                mapping.add(entity);
            }
            
            packageBidAdvertisementRepository.save(mapping);
            packageBidAdvertisementRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
