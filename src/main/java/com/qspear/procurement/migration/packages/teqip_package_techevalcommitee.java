/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageTechEvalCommitee;
import com.qspear.procurement.repositories.method.TechEvalCommiteeRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

@Repository
@Order(value = Loader.teqip_package_techevalcommitee)
public class teqip_package_techevalcommitee implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TechEvalCommiteeRepository commiteeRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageTechEvalCommitee> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT "
                    + "  pd.PPPackageID,"
                    + "  a.CommitteeMemberName"
                    + "  FROM tbl_PP_PMSS_TechnicalEvaluationCommittee a"
                    + "  INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON pd.PTPackageID = a.PTPackageID");
            while (resultSet.next()) {

                TeqipPackageTechEvalCommitee entity = new TeqipPackageTechEvalCommitee();

                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                entity.setMemberName(resultSet.getString(2));
                entity.setCreatedOn(new Timestamp(new Date().getTime()));
                entity.setCreatedBy(1);
                entity.setModifyOn(new Timestamp(new Date().getTime()));
                entity.setModifyBy(1);
                mapping.add(entity);

            }
            commiteeRepository.save(mapping);
            commiteeRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
