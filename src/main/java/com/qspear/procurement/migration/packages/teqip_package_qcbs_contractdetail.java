package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractDetail;
import com.qspear.procurement.repositories.method.QCBSContractDetailRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_qcbs_contractdetail)
public class teqip_package_qcbs_contractdetail implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    QCBSContractDetailRepository qCBSContractDetailRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPackageQCBSContractDetail> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " PPPackageID,"
                    + " MemberInCharge,"
                    + " ClientRepresentative,"
                    + " ContractStartDate,"
                    + " DeliveryPeriod,"
                    + " BasicValue,"
                    + " ContractValue,"
                    + " ApplicableTax,"
                    + " AdvPaymentReqd,"
                    + " PaymentMilestoneDefined,"
                    + " BankGuanteeSubmitted,"
                    + " IsArbitratorAgreed,"
                    + " ContractStartDate,"
                    + " PlaceforArbitration,"
                    + " InterestRate,"
                    + " IsContractApproved,"
                    + " ContractApprovalDate"
                    + " FROM tbl_PP_PMSS_PTPackageDetails");
            while (resultSet.next()) {
                TeqipPackageQCBSContractDetail entity = new TeqipPackageQCBSContractDetail();
                entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
                entity.setMemberInCharge(resultSet.getString(2));
                entity.setAuthorizedRepresentativeOfClient(resultSet.getString(3));
                entity.setContractStartDate(resultSet.getDate(4));
                try {
                    entity.setDeliveryPeriodInMonths(Integer.parseInt(resultSet.getString(5)));
                } catch (Exception e) {
                }
                entity.setBasicValue(resultSet.getDouble(6));
                entity.setContractValue(resultSet.getDouble(7));
                entity.setApplicableTax(resultSet.getDouble(8));
                entity.setIsAdvancePaymentRequired(resultSet.getInt(9));
                entity.setPaymentMilestonesDefined(resultSet.getInt(10));
                entity.setIsBankGuaranteeSubmitted(resultSet.getInt(11));
                entity.setIsArbitratorAgreed(resultSet.getInt(12));
                entity.setCommencementServiceDate(resultSet.getDate(13));
                entity.setArbitrationProceedingHeld(resultSet.getString(14));
                entity.setInterestPercentageOnDelayedPayments(resultSet.getDouble(15));
                entity.setIsWordWorkBankNOC(resultSet.getInt(16));
                entity.setWordBankNOCDate(resultSet.getDate(17));
                entity.setProposalAmount(resultSet.getDouble(6));

                mapping.add(entity);
            }

            qCBSContractDetailRepository.save(mapping);
            qCBSContractDetailRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
