/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.packages;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageQuestionMaster;
import com.qspear.procurement.repositories.method.PackageQuestionRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_package_questionmaster)
public class teqip_package_questionmaster implements PackageLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    private PackageQuestionRepository packageQuestionRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        HashedMap<Integer, String> categoryMap = new HashedMap<>();
        categoryMap.put(1, "opening");
        categoryMap.put(2, "evaluation");
        categoryMap.put(3, "post_evaluation");
        categoryMap.put(5, "instructionToConsultants");
        categoryMap.put(9, "extendValidity");

        HashedMap<Integer, String> datetype = new HashedMap<>();
        datetype.put(1, "string");
        datetype.put(2, "radio");
        datetype.put(3, "date");
        datetype.put(4, "string");
        datetype.put(5, "string");
        datetype.put(6, "string");
        /**
         * @key : Question
         * @Value : Sting [] { isDecisionMaking, isReponsive,
         * type, isReadout, isEvaluated, option}
         */
        HashedMap<String, String[]> openingQuestion = new HashedMap<>();
        openingQuestion.put("Is Bid Received?", new String[]{"Yes", "No", "radio_quotation_received", "0", "0", ""});
        openingQuestion.put("Is Bid Withdrawn?", new String[]{"No", "Yes", "radio", "0", "0", ""});
        openingQuestion.put("Is Bid Modified?", new String[]{"No", "No", "radio", "0", "0", ""});
        openingQuestion.put("Is Bid Duly Signed?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        openingQuestion.put("Bid Number", new String[]{"No", "No", "string", "0", "0", ""});
        openingQuestion.put("Bid Date", new String[]{"No", "No", "date", "0", "0", ""});
        openingQuestion.put("Validity of Bid (in days)", new String[]{"No", "No", "float_validity", "0", "0", ""});
        openingQuestion.put("Validity of Bid Security (in days) ", new String[]{"No", "No", "float_bid_security", "0", "0", ""});
        openingQuestion.put("Is Bid Security as specified?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        openingQuestion.put("Is the cost of Bid Document attached?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        openingQuestion.put("Unconditional Discount offered, if any(%)", new String[]{"No", "No", "float", "0", "0", ""});
        openingQuestion.put("Readout Price without taxes", new String[]{"No", "No", "float", "1", "0", ""});
        openingQuestion.put("Is Quotation Received?", new String[]{"Yes", "No", "radio_quotation_received", "0", "0", ""});
        openingQuestion.put("Is Quotation Duly Signed?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        openingQuestion.put("Quotation Number", new String[]{"No", "No", "string", "0", "0", ""});
        openingQuestion.put("Quotation Received Date", new String[]{"No", "No", "date", "0", "0", ""});
        openingQuestion.put("Quotation Validity (in days)", new String[]{"No", "No", "float_validity", "0", "0", ""});
        openingQuestion.put("Is the Signed BOQ submitted?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        openingQuestion.put("Is Estimated Time of Completion as specified?", new String[]{"Yes", "No", "radio", "0", "0", ""});

        HashedMap<String, String[]> evaluationQuestion = new HashedMap<>();
        evaluationQuestion.put("Is Cross Discount applicable?", new String[]{"No", "No", "radio", "0", "0", ""});
        evaluationQuestion.put("Is Technically Responsive?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        evaluationQuestion.put("Quantity as specified?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        evaluationQuestion.put("Delivery Period", new String[]{"No", "No", "float", "0", "0", ""});
        evaluationQuestion.put("Is the Place of Delivery as specified?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        evaluationQuestion.put("Are the Payment Terms Agreed?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        evaluationQuestion.put("Is Warranty as Specified?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        evaluationQuestion.put("Essential Accessories price included?", new String[]{"Yes", "No", "radio_na", "0", "0", ""});
        evaluationQuestion.put("Optional Spares Accessories included?", new String[]{"No", "No", "radio_na", "0", "0", ""});
        evaluationQuestion.put("Is Testing/ Commissioning/ Installation included?", new String[]{"Yes", "No", "radio_na", "0", "0", ""});
        evaluationQuestion.put("Is Training Included?", new String[]{"Yes", "No", "radio_na", "0", "0", ""});
        evaluationQuestion.put("Is there any Price Adjustment?", new String[]{"No", "No", "radio_price_correction", "0", "0", ""});
        evaluationQuestion.put("Reason(s) for Price Adjustment", new String[]{"No", "No", "dropdown", "0", "0", "Arithmatic Error,Discounts Offered,Evaluation Criterias,Delivery Schedule deviations,Payment Schedule deviations"});
        evaluationQuestion.put("Evaluated Price", new String[]{"No", "No", "float", "0", "1", ""});
        evaluationQuestion.put("Readout Price without taxes", new String[]{"No", "No", "float", "1", "0", ""});
        evaluationQuestion.put("Comments", new String[]{"No", "No", "string_comments", "0", "0", ""});
        evaluationQuestion.put("Reason(s) for Price Correction", new String[]{"No", "No", "dropdown", "0", "0", "Arithmatic Reason,Discount Offered"});
        evaluationQuestion.put("Is there any Price Correction?", new String[]{"No", "No", "radio_price_correction", "0", "0", ""});
        evaluationQuestion.put("Is the Quotation Technically Responsive?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        evaluationQuestion.put("Is the Quantity as specified?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        evaluationQuestion.put("Are the Payment Terms Agreed?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        evaluationQuestion.put("Are the Delivery terms as specified?", new String[]{"Yes", "No", "radio", "0", "0", ""});

        HashedMap<String, String[]> postQualQuestion = new HashedMap<>();

        postQualQuestion.put("GST Number", new String[]{"No", "No", "string_gst", "0", "0", ""});
        postQualQuestion.put("Satisfactory delivery of similar goods/items of value not less than 80% of estimated contract value in less than 3 years.", new String[]{"Yes", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Is Minimum Financial Turnover met in at least once of the last three years excluding current financial year?", new String[]{"Yes", "No", "radio", "0", "0", ""});

        postQualQuestion.put("Satisfactorily completion of at least 1 similar work of value not less than 80% of estimated contract value in less than 3 years", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Minimum Financial Turnover on at least 2 of the last five financial years", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Liquid Assets / Letter of Credit submitted for amount mentioned in bid?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Is the Sanitary work being done by the bidder?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Is the rates quoted as per BOQ un balanced?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Is the Electrical work being done by the bidder?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Is Assessed Available Bid Capacity more than contract size?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("If Yes, then does the bidder have valid Sanitary Work License?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("If Yes, then does the bidder have valid Electrical License?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("If Yes, then does bidder meet aggregate of the qualifying criteria?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("If sub-contractor has valid sanitary work license, has the bidder performed similar sanitary work in the past 1 year amounting to value, as specified in the bid document?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("IF sub-contractor has valid electrical license, has the bidder performed similar electrical work in the past 1 year amounting to value, as specified in the bid document?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("If sanitary work is being done by sub-contractor, then does the sub-contractor have valid sanitary work License?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("If electrical work is being done by sub-contractor, then does the sub-contractor have valid Electrical License?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("If Bidder has valid sanitary work license, has the bidder performed similar sanitary work in the past 1 year amounting to value, as specified in the bid document?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("If Bidder has valid electrical license, has the bidder performed similar electrical work in the past 1 year amounting to value, as specified in the bid document?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Executed in one year the minimum quantities of work as specified in the bid document", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Does the bid cover civil, electrical, sanitary works etc. as per the BOQ", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Does bidder qualify for a package of contracts made up of this and other contracts for which bids were invited in the IFB?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Bid in INR with all applicable Tax", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Availability of Project Manager?", new String[]{"No", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Are the Payment Terms Agreed?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Is the Total Monetary Value of construction in the last 3 years Received?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Is the Litigation Report Received?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Is the Income Tax Certificate Received?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Is the Financial Standing Report Received?", new String[]{"Yes", "No", "radio", "0", "0", ""});
        postQualQuestion.put("Has the Contractor Satisfactorily completed at least 1 similar work of value not less than 80% of estimated contract value in less than 3 years?", new String[]{"No", "Yes", "radio", "0", "0", ""});
        postQualQuestion.put("Does the Contractor have a Valid Sanitary Work License?", new String[]{"No", "Yes", "radio", "0", "0", ""});
        postQualQuestion.put("Does the Contractor have a Valid Electrical License?", new String[]{"No", "Yes", "radio", "0", "0", ""});
        try {
            List<TeqipPackageQuestionMaster> list = new ArrayList<>();
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("  SELECT "
                    + "  pq.PTQuestionID,"
                    + "  pd.PPPackageID ,"
                    + "  pq.QuestionCategoryID,"
                    + "  pq.Question,"
                    + "  pq.NonResponsiveValue,"
                    + "  pq.IsSystemQuestion,"
                    + "  pq.Datatype,"
                    + " pq.IsDecisionMakingQuestion"
                    + "  FROM tbl_PP_PMSS_PTPackageQuestions pq"
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON pd.PTPackageID = pq.PTPackageID");
            while (resultSet.next()) {

                int packageID = resultSet.getInt(2);
                TeqipPackageQuestionMaster entity = new TeqipPackageQuestionMaster();
                entity.setTeqipPackage(teqip_package_map.get(packageID));
                int questionCategoryID = resultSet.getInt(3);
                String question = resultSet.getString(4);
                entity.setQuestionCategory(categoryMap.get(questionCategoryID));
                entity.setQuestionDesc(question);
                entity.setQuestionNonResponsiveValue(resultSet.getString(5));
                entity.setIsStandardQuestion(0);
                entity.setType(datetype.get(resultSet.getInt(7)));
                if (resultSet.getObject(8) != null) {
                    entity.setDecision(resultSet.getInt(8));
                }

                String str[] = null;
                if (questionCategoryID == 1) {
                    str = openingQuestion.get(question);
                } else if (questionCategoryID == 2) {
                    str = evaluationQuestion.get(question);
                } else if (questionCategoryID == 3) {
                    str = postQualQuestion.get(question);
                }

                if (str != null) {
//                    entity.setDecision("YES".equalsIgnoreCase(str[0]) ? 1 : 0);
//                    entity.setQuestionNonResponsiveValue(str[1]);
                    entity.setType(str[2]);
                    entity.setIsreadOut(Integer.parseInt(str[3]));
                    entity.setIsevalatedPrice(Integer.parseInt(str[4]));
                    entity.setOptions(str[5]);
                    entity.setIsStandardQuestion(1);

                }
                if ("Readout Price without taxes".equalsIgnoreCase(question)) {
                    TeqipPackageQuestionMaster entity1 = entity.clone();
                    entity1.setQuestionCategory("evaluation");
                    teqipPackageEvaQuesMaster_map.put(packageID, entity1);
                    list.add(entity1);
                }

                teqip_package_questionmaster_map.put(resultSet.getInt(1), entity);
                list.add(entity);

            }

            packageQuestionRepository.save(list);
            packageQuestionRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
