package com.qspear.procurement.migration.packages;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.PackageLoader;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrder;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.WorkOrderCertificateRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_package_workordercertificate)
public class teqip_package_workordercertificate  implements PackageLoader{
	
	@Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;
	
	@Autowired
	WorkOrderCertificateRepository workOrderCertificateRepository;
	
	@Autowired
	TeqipPackageRepository packageRepository;

	@Override
	public void load() {
		System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
         ArrayList<TeqipPackageWorkOrderCertificate> mapping = new ArrayList<>();
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(" SELECT"
                    + "  pt.PPPackageID,"
                    + "  ActualWorkCompletionDate,"
                    + "  WorkDone,"
                    + "  BalanceWork,"
                    + "  Comments,"
                    + "  pt.PONo "
                    + "  FROM tbl_PP_PMSS_WorkCompletionStatus PW,"
                    + "  tbl_PP_PMSS_PTPackageDetails pt "
                    + " where  PW.PTPackageID=pt.PTPackageID ");
            while (resultSet.next()) {
            	TeqipPackageWorkOrderCertificate entity = new TeqipPackageWorkOrderCertificate();
            	entity.setTeqipPackage(teqip_package_map.get(resultSet.getInt(1)));
            	entity.setActualCompletionDate(resultSet.getDate(2));
            	entity.setWorkDone(resultSet.getString(3));
            	entity.setBalanceWork(resultSet.getString(4));
            	entity.setComment(resultSet.getString(5));
            	entity.setWoNumber(resultSet.getString(6));
                mapping.add(entity);
            	
            }
            workOrderCertificateRepository.save(mapping);
            workOrderCertificateRepository.flush();
            
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
		
	}

}
