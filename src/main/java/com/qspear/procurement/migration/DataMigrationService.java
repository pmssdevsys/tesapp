/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
//@Transactional
public class DataMigrationService {

    @Autowired
    List<MasterLoader> masterLoader;

    @Autowired
    List<PackageLoader> packageLoader;

    public void execute(Boolean onlyMaster) {

        for (MasterLoader loader : masterLoader) {
            loader.load();

        }
        if (!onlyMaster) {
            for (PackageLoader loader : packageLoader) {
                loader.load();

            }
        }

        List<String> missingData = Loader.missingData;
        for (String string : missingData) {
            System.out.println(string);
        }
    }
}
