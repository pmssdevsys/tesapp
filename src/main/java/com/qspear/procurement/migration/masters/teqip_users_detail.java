package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipRolemaster;
import com.qspear.procurement.persistence.TeqipUsersDetail;
import com.qspear.procurement.repositories.UserDetailRepository;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_users_detail)
public class teqip_users_detail implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    UserDetailRepository userRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("  SELECT l.loginid, e.EmployeeID, e.EmployeeCode , e.EmployeeName, PMSS_firstName, PMSS_middleNAme, "
                    + "PMSS_lastname, e.PP_RelativeName, e.Phone, e.BirthDate, e.Gender, e.EmailID, e.UserName, e.JoiningDate, e.DepartmentID, l.RoleID,"
                    + "e.address,e.city,e.state,e.country,e.pincode FROM tbl_PM_Login l INNER JOIN tbl_PM_Employee e ON e.EmployeeID =  l.EmployeeID");
            while (resultSet.next()) {

                TeqipUsersDetail entity = new TeqipUsersDetail();
                int id = resultSet.getInt(2);
                TeqipRolemaster get = teqip_rolemaster_map.get(resultSet.getInt(16));

                String userName = resultSet.getString(13);
                entity.setEmployeedId(resultSet.getInt(2));
                entity.setEmployeeCode(resultSet.getString(3));
                entity.setName(resultSet.getString(4));
                entity.setFName(resultSet.getString(5));
                entity.setMiddleName(resultSet.getString(6));
                entity.setLname(resultSet.getString(7));
                entity.setSpouseName(resultSet.getString(8));
                entity.setUserMobile(resultSet.getString(9));
                entity.setDob(resultSet.getDate(10));
                entity.setGender(resultSet.getString(11));
                entity.setEmail(resultSet.getString(12));
                entity.setPassword(userName);
                entity.setJoiningDate(resultSet.getDate(14));
                entity.setDeptId(resultSet.getInt(15));
                entity.setTeqipRolemaster(get);
                entity.setAddress(resultSet.getString(17));
                entity.setCity(resultSet.getString(18));
                entity.setState(resultSet.getString(19));
                entity.setCountry(resultSet.getString(20));
                entity.setPincode(resultSet.getString(21));
                entity.setCtc(0.0);
                entity.setIsBlocked(false);
                entity.setUserName(userName);
                entity.setIsactive(true);
                entity.setCreatedOn(new Timestamp(new Date().getTime()));
                entity.setCreatedBy(1);
                entity.setModifiedOn(new Timestamp(new Date().getTime()));
                entity.setModifiedBy(1);

                teqip_users_detail_map.put(id, entity);
                teqip_users_detail_string_map.put(userName, entity);

            }

            userRepository.save(teqip_users_detail_map.values());
            userRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
