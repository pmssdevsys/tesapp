package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipRolemaster;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.persistence.TeqipUsersDetail;
import com.qspear.procurement.persistence.TeqipUsersMaster;
import com.qspear.procurement.repositories.UserMasterRepository;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_users_master)
public class teqip_users_master implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    UserMasterRepository userMasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipUsersMaster> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT e.EmployeeCode, e.EmployeeID, e.PMSS_InstitutionID ,e.PMSS_SPFUID, l.RoleID, l.LoginID"
                    + " FROM tbl_PM_Login l INNER JOIN tbl_PM_Employee e ON e.EmployeeID =  l.EmployeeID");
            while (resultSet.next()) {

                TeqipUsersDetail get = teqip_users_detail_map.get(resultSet.getInt(2));

                TeqipInstitution get1 = teqip_institutions_map.get(resultSet.getInt(3));

                TeqipStatemaster get2 = teqip_statemaster_map.get(resultSet.getInt(4));

                TeqipRolemaster get3 = teqip_rolemaster_map.get(resultSet.getInt(5));

                int id = resultSet.getInt(6);
                if (get == null) {
                    missingData.add("Invalid EmployeeID ::tbl_PM_Login ::" + id);

                }  else if (get3 == null) {
                    missingData.add("Invalid RoleID ::tbl_PM_Login ::" + id);

                } else {
                    TeqipUsersMaster entity = new TeqipUsersMaster();

                    entity.setEmployeeCode(resultSet.getString(1));
                    entity.setTeqipUsersDetail(get);
                    entity.setTeqipInstitution(get1);
                    entity.setTeqipStatemaster(get2);
                    entity.setTeqipRolemaster(get3);
                    entity.setIsactive(true);
                    entity.setCreatedOn(new Timestamp(new Date().getTime()));
                    entity.setCreatedBy(1);
                    entity.setModifiedOn(new Timestamp(new Date().getTime()));
                    entity.setModifiedBy(1);

                    mapping.add(entity);
                }
            }

            userMasterRepository.save(mapping);
            userMasterRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
