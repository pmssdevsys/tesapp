package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipRolemaster;
import com.qspear.procurement.repositories.RoleMasterRepository;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_rolemaster)

public class teqip_rolemaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    RoleMasterRepository roleMasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT "
                    + "  RoleID,"
                    + "  RoleDescription,"
                    + "  PMSS_RoleCode,"
                    + "  Level,"
                    + "  CreatedBy,"
                    + "  CreatedDate"
                    + "  FROM tbl_PM_Role");
            while (resultSet.next()) {
                TeqipRolemaster entity = new TeqipRolemaster();
                int id = resultSet.getInt(1);

                entity.setRoleDescription(resultSet.getString(2));
                entity.setPmssRole(resultSet.getString(3));
                entity.setLevelId(resultSet.getInt(4));
                entity.setCreatedOn(new Timestamp(new Date().getTime()));
                entity.setCreatedBy(1);
                entity.setIsactive(true);

                teqip_rolemaster_map.put(id, entity);
            }

            roleMasterRepository.save(teqip_rolemaster_map.values());
            roleMasterRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
