/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipStatemaster;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;
import com.qspear.procurement.repositories.StateMasterRepository;
import org.springframework.util.StringUtils;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_statemaster)
public class teqip_statemaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    StateMasterRepository statemasteryRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT "
                    + "  SPFUID,"
                    + "  SPFUCode,"
                    + "  SPFUName,"
                    + "  EmailID,"
                    + "  TelephoneNumber,"
                    + "  FaxNumber,"
                    + "  Address,"
                    + "  WebSiteURL"
                    + "  FROM tbl_CNF_PMSS_SPFU");
            while (resultSet.next()) {

                String state = Loader.camelCase(resultSet.getString(3));
                if (state == null || "".equals(state)) {
                    continue;
                }
                TeqipStatemaster entity = new TeqipStatemaster();
                entity.setStateCode(resultSet.getString(2));
                entity.setStateName(state);
                entity.setEmailID(resultSet.getString(4));
                entity.setPhoneNumber(resultSet.getString(5));
                entity.setAddress(resultSet.getString(6));
                entity.setWebsiteURL(resultSet.getString(7));
                entity.setIsactive(true);
                teqip_statemaster_map.put(resultSet.getInt(1), entity);
//                teqip_statemaster_string_map.put(state, entity);
            }

//            resultSet = statement.executeQuery("SELECT "
//                    + "  DISTINCT state"
//                    + "  FROM tbl_PP_PMSS_BidderDetails");
//            while (resultSet.next()) {
//                String state = Loader.camelCase(resultSet.getString(1));
//                if (state == null || "".equals(state) || teqip_statemaster_string_map.containsKey(state)) {
//                    continue;
//                }
//                TeqipStatemaster entity = new TeqipStatemaster();
//
//                entity.setStateName(state);
//                teqip_statemaster_string_map.put(state, entity);
//
//            }

            statemasteryRepository.save(teqip_statemaster_map.values());
            statemasteryRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
