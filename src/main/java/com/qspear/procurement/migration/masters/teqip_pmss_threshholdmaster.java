package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipPmssInstitutionpurchasecommitte;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPmssThreshholdmaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.repositories.TeqipPmssThreshholdmasterRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_pmss_threshholdmaster)

public class teqip_pmss_threshholdmaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipPmssThreshholdmasterRepository teqipPmssThreshholdmasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
//        Connection connection = null;
//        Statement statement = null;
//        ResultSet resultSet = null;
//
//        ArrayList<TeqipPmssThreshholdmaster> mapping = new ArrayList<>();
//
//        try {
//            connection = secondaryDataSource.getConnection();
//            statement = connection.createStatement();
//
//            resultSet = statement.executeQuery("SELECT DISTINCT 1 as id,  temp.ItemCategoryID,  temp.MinThreshholdValue,  temp.MaxThreshholdValue,  temp.ProcurementMethodID,  temp.IsProprietaryItem ,temp.isgem "
//                    + " FROM ("
//                    + " SELECT    ItemCategoryID,  MinThreshholdValue,  MaxThreshholdValue,  ProcurementMethodID,  IsProprietaryItem ,0 as isgem"
//                    + " FROM tbl_CNF_PMSS_ItemCategory_ProcurementMethodThreshhold"
//                    + " UNION ALL "
//                    + " SELECT  	 ItemCategoryID,  MinThreshholdValue,  MaxThreshholdValue,  ProcurementMethodID,  IsProprietaryItem ,1 as isgem"
//                    + " FROM tbl_CNF_PMSS_ItemCategory_ProcurementMethodThreshhold"
//                    + " )as temp");
//            while (resultSet.next()) {
//
//                 TeqipCategorymaster get1 = teqip_categorymaster_map.get(resultSet.getInt(2));
//                int procurementMethodId = resultSet.getInt(5);
//                TeqipProcurementmethod get5 = null;
//                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("GOODS") >= 0) {
//                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_G");
//
//                }
//                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("WORKS") >= 0) {
//                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_W");
//
//                }
//                if (get1 != null && get1.getCategoryName() != null && get1.getCategoryName().toUpperCase().indexOf("SERVICES") >= 0) {
//                    get5 = teqip_procurementmethod_map_str.get(procurementMethodId + "_S");
//
//                }
//                
//                if (get1 == null) {
//                    missingData.add("Invalid ItemCategoryID ::tbl_CNF_PMSS_ItemCategory_ProcurementMethodThreshhold ::");
//
//                } else if (get5 == null) {
//                    missingData.add("Invalid ProcurementMethodID ::tbl_CNF_PMSS_ItemCategory_ProcurementMethodThreshhold ::");
//
//                } else {
//                    TeqipPmssThreshholdmaster entity = new TeqipPmssThreshholdmaster();
//                    entity.setTeqipCategorymaster(get1);
//                    entity.setThresholdMinValue(resultSet.getDouble(3));
//                    entity.setThresholdMaxValue(resultSet.getDouble(4));
//                    entity.setTeqipProcurementmethod(get5);
//                    entity.setIsproprietary(resultSet.getInt(6));
//                    entity.setIsactive(Boolean.TRUE);
//                    entity.setIsgem(resultSet.getInt(7));
//                    mapping.add(entity);
//
//                }
//            }
//
//            teqipPmssThreshholdmasterRepository.save(mapping);
//            teqipPmssThreshholdmasterRepository.flush();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            Loader.closeResources(resultSet, statement, connection);
//        }

    }

}
