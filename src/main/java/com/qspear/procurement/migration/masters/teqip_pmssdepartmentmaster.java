/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.missingData;
import static com.qspear.procurement.migration.Loader.teqip_institutiondepartmentmapping_map;
import static com.qspear.procurement.migration.Loader.teqip_institutions_map;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import com.qspear.procurement.repositories.method.DepartmentMasterRepository;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.core.annotation.Order;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_pmssdepartmentmaster)
public class teqip_pmssdepartmentmaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    DepartmentMasterRepository departmentMasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT DepartmentID,"
                    + " Department"
                    + " FROM tbl_PM_DepartmentMaster");
            while (resultSet.next()) {
                TeqipPmssDepartmentMaster entity = new TeqipPmssDepartmentMaster();

                entity.setDepartment(resultSet.getString(2));

                teqip_pmssdepartmentmaster_map.put(resultSet.getString(2), entity);

            }
            resultSet = statement.executeQuery(" SELECT "
                    + " m.InstitutionDepartmentMappingID,"
                    + " m.InstitutionID,"
                    + " m.DepartmentName,"
                    + " m.DepartmentHead"
                    + " FROM tbl_CNF_PMSS_InstitutionDepartmentMapping  m");
            while (resultSet.next()) {


                TeqipPmssDepartmentMaster get1 = new TeqipPmssDepartmentMaster();
                get1.setDepartment(resultSet.getString(3));

                teqip_pmssdepartmentmaster_map.put(resultSet.getString(3), get1);

            }
            departmentMasterRepository.save(teqip_pmssdepartmentmaster_map.values());
            departmentMasterRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
