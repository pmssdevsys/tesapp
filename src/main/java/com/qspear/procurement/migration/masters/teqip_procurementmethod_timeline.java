/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipPmssInstitutionpurchasecommitte;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipProcurementmethodTimeline;
import com.qspear.procurement.repositories.TeqipProcMethodTimelineRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_procurementmethod_timeline)
public class teqip_procurementmethod_timeline implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipProcMethodTimelineRepository methodTimelineRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipProcurementmethodTimeline> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("  SELECT "
                    + "  1 as temp, c.ProcurementMethodID,"
                    + "  c.ProcurementCategory,"
                    + "  c.BidDocumentPreparationDate_days,"
                    + "  c.BankNOCForBiddingDocuments_days,"
                    + "    c.BidInvitationDate_days ,"
                    + "    c.BidOpeningDate_days ,"
                    + "    c.TORFinalizationDate_days ,"
                    + "    c.AdvertisementDate_days ,"
                    + "    c.FinalDraftToBeForwardedToTheBankDate_days ,"
                    + "    c.NoObjectionFromBankForRFP_days ,"
                    + "    c.RFPIssuedDate_days ,"
                    + "    c.LastDateToReceiveProposals_days ,"
                    + "    c.EvaluationDate_days ,"
                    + "    c.NoObjectionFromBankForEvaluation_days ,"
                    + "    c.ContractAwardDate_days ,"
                    + "    c.ContractCompletionDate_days ,"
                    + "    c.TORFinalizationDate_WithoutBankNOC_days ,"
                    + "    c.AdvertisementDate_WithoutBankNOC_days ,"
                    + "    c.RFPIssuedDate_WithoutBankNOC_days ,"
                    + "    c.LastDateToReceiveProposals_WithoutBankNOC_days ,"
                    + "    c.EvaluationDate_WithoutBankNOC_days ,"
                    + "    c.ContractAwardDate_WithoutBankNOC_days ,"
                    + "    c.ContractCompletionDate_WithoutBankNOC_days"
                    + "  FROM tbl_CNF_PMSS_ProcurementMethod c");
            while (resultSet.next()) {

                if (resultSet.getString(4) != null && resultSet.getString(4).toUpperCase().indexOf("GOODS") >= 0) {
                    TeqipProcurementmethod get = teqip_procurementmethod_map_str.get(resultSet.getInt(2) + "_G");
                    TeqipCategorymaster value = teqip_categorymaster_map_str.get("Goods");
                    mapping.add(getEntity(resultSet, get, value));
                }
                if (resultSet.getString(4) != null && resultSet.getString(4).toUpperCase().indexOf("WORKS") >= 0) {
                    TeqipProcurementmethod get = teqip_procurementmethod_map_str.get(resultSet.getInt(2) + "_W");
                    TeqipCategorymaster value = teqip_categorymaster_map_str.get("Civil Works");
                    mapping.add(getEntity(resultSet, get, value));

                }
                if (resultSet.getString(4) != null && resultSet.getString(4).toUpperCase().indexOf("SERVICES") >= 0) {
                    TeqipProcurementmethod get = teqip_procurementmethod_map_str.get(resultSet.getInt(2) + "_S");
                    TeqipCategorymaster value = teqip_categorymaster_map_str.get("Services");
                    mapping.add(getEntity(resultSet, get, value));

                }
            }

            methodTimelineRepository.save(mapping);
            methodTimelineRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

    private TeqipProcurementmethodTimeline getEntity(ResultSet resultSet,
            TeqipProcurementmethod get, TeqipCategorymaster value) throws SQLException {

        TeqipProcurementmethodTimeline entity = new TeqipProcurementmethodTimeline();
        entity.setTeqipProcurementmethod(get);
        entity.setTeqipCategorymaster(value);
        entity.setBidDocumentPreparationDate_days(resultSet.getDouble(4));
        entity.setBankNOCForBiddingDocuments_days(resultSet.getDouble(5));
        entity.setBidInvitationDate_days(resultSet.getDouble(6));
        entity.setBidOpeningDate_days(resultSet.getDouble(7));
        entity.setTORFinalizationDate_days(resultSet.getDouble(8));
        entity.setAdvertisementDate_days(resultSet.getDouble(9));
        entity.setFinalDraftToBeForwardedToTheBankDate_days(resultSet.getDouble(10));
        entity.setNoObjectionFromBankForRFP_days(resultSet.getDouble(11));
        entity.setRFPIssuedDate_days(resultSet.getDouble(12));
        entity.setLastDateToReceiveProposals_days(resultSet.getDouble(13));
        entity.setEvaluationDate_days(resultSet.getDouble(14));
        entity.setNoObjectionFromBankForEvaluation_days(resultSet.getDouble(15));
        entity.setContractAwardDate_days(resultSet.getDouble(16));
        entity.setContractCompletionDate_days(resultSet.getDouble(17));
        entity.setTORFinalizationDate_WithoutBankNOC_days(resultSet.getDouble(18));
        entity.setAdvertisementDate_WithoutBankNOC_days(resultSet.getDouble(19));
        entity.setRFPIssuedDate_WithoutBankNOC_days(resultSet.getDouble(20));
        entity.setLastDateToReceiveProposals_WithoutBankNOC_days(resultSet.getDouble(21));
        entity.setEvaluationDate_WithoutBankNOC_days(resultSet.getDouble(22));
        entity.setContractAwardDate_WithoutBankNOC_days(resultSet.getDouble(23));
        entity.setContractCompletionDate_WithoutBankNOC_days(resultSet.getDouble(24));

        return entity;
    }

}
