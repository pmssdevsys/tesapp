/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipComponentmaster;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;
import com.qspear.procurement.repositories.TeqipSubcomponentmasterRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_subcomponentmaster)
public class teqip_subcomponentmaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipSubcomponentmasterRepository subcomponentmasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("  SELECT SubComponentID,ComponentID,SubComponentCode, SubComponentName FROM tbl_CNF_PMSS_SubComponent");
            while (resultSet.next()) {

                TeqipComponentmaster get = teqip_componentmaster_map.get(resultSet.getInt(2));
                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid ComponentID ::tbl_CNF_PMSS_SubComponent ::" + id);

                } else {
                    TeqipSubcomponentmaster entity = new TeqipSubcomponentmaster();
                    entity.setTeqipComponentmaster(get);
                    entity.setSubcomponentcode(resultSet.getString(3));
                    entity.setSubComponentName(resultSet.getString(4));
                    entity.setTeqipCategorymaster(teqip_categorymaster_map.get(1));
                    entity.setSubCategoryId(1);
                    entity.setIsactive(true);

                    teqip_subcomponentmaster_map.put(id, entity);
                }
            }

            subcomponentmasterRepository.save(teqip_subcomponentmaster_map.values());
            subcomponentmasterRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
