package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import com.qspear.procurement.persistence.TeqipPmssInstitutionlogo;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPmssInstitutionpurchasecommitte;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import com.qspear.procurement.repositories.TeqipPmssInstitutionpurchasecommitteRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_pmss_institutionpurchasecommitte)
public class teqip_pmss_institutionpurchasecommitte implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipPmssInstitutionpurchasecommitteRepository teqipPmssInstitutionpurchasecommitteRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPmssInstitutionpurchasecommitte> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT "
                    + "  InstitutionPCommitteMappingID,"
                    + "	 InstitutionID,"
                    + "  DepartmentID,"
                    + "  CommitteMemberName,"
                    + "  Designation,"
                    + "  PCommitteRole"
                    + "  FROM tbl_CNF_PMSS_InstitutionPurchaseCommitte");
            while (resultSet.next()) {

                TeqipInstitution get = teqip_institutions_map.get(resultSet.getInt(2));
                TeqipInstitutiondepartmentmapping get1 = teqip_institutiondepartmentmapping_map.get(resultSet.getInt(3));

                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid InstitutionID ::tbl_CNF_PMSS_InstitutionPurchaseCommitte ::" + id);

                } else if (get1 == null) {
                    missingData.add("Invalid DepartmentID ::tbl_CNF_PMSS_InstitutionPurchaseCommitte ::" + id);

                } else {

                    TeqipPmssInstitutionpurchasecommitte entity = new TeqipPmssInstitutionpurchasecommitte();
                    entity.setTeqipInstitution(get);
                    entity.setTeqipPmssdepartmentmaster(get1.getTeqipPmssdepartmentmaster());
                    entity.setCommitteMemberName(resultSet.getString(4));
                    entity.setDesignation(resultSet.getString(5));
                    entity.setPCommitteRole(resultSet.getString(6));
                    entity.setType(0);
                    mapping.add(entity);
                }

            }

            resultSet = statement.executeQuery("SELECT "
                    + "  SPFUPCommitteMappingID,"
                    + "	 SPFUID,"
                    + "  CommitteMemberName,"
                    + "  Designation,"
                    + "  PCommitteRole"
                    + "  FROM tbl_CNF_PMSS_SPFUPurchaseCommitte");
            while (resultSet.next()) {

                TeqipStatemaster get = teqip_statemaster_map.get(resultSet.getInt(2));
                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid SPFUID ::tbl_CNF_PMSS_SPFUPurchaseCommitte ::" + id);

                } else {

                    TeqipPmssInstitutionpurchasecommitte entity = new TeqipPmssInstitutionpurchasecommitte();
                    entity.setStatemaster(get);
                    entity.setCommitteMemberName(resultSet.getString(3));
                    entity.setDesignation(resultSet.getString(4));
                    entity.setPCommitteRole(resultSet.getString(5));
                    entity.setType(0);
                    mapping.add(entity);
                }

            }

            resultSet = statement.executeQuery("SELECT "
                    + "  NPIUPCommitteMappingID,"
                    + "	 0,"
                    + "  CommitteMemberName,"
                    + "  Designation,"
                    + "  PCommitteRole"
                    + "  FROM tbl_CNF_PMSS_NPIUPurchaseCommitte");
            while (resultSet.next()) {

                TeqipPmssInstitutionpurchasecommitte entity = new TeqipPmssInstitutionpurchasecommitte();
                entity.setCommitteMemberName(resultSet.getString(3));
                entity.setDesignation(resultSet.getString(4));
                entity.setPCommitteRole(resultSet.getString(5));
                entity.setType(0);
                mapping.add(entity);

            }

            teqipPmssInstitutionpurchasecommitteRepository.save(mapping);
            teqipPmssInstitutionpurchasecommitteRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
