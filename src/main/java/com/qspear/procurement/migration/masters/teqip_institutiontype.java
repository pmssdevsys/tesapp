/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipInstitutiontype;
import com.qspear.procurement.repositories.InstitutionTypeRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_institutiontype)
public class teqip_institutiontype implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    InstitutionTypeRepository institutionTypeRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(" SELECT "
                    + "  InstitutionTypeID,"
                    + "  InstitutionType"
                    + "  FROM tbl_CNF_PMSS_InstitutionType");
            while (resultSet.next()) {

                TeqipInstitutiontype entity = new TeqipInstitutiontype();
                entity.setInstitutiontypeName(resultSet.getString(2));
                entity.setIsactive(true);

                teqip_institutiontype_map.put(resultSet.getInt(1), entity);
            }
            institutionTypeRepository.save(teqip_institutiontype_map.values());
            institutionTypeRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
