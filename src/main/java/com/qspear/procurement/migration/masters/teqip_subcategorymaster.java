/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import com.qspear.procurement.repositories.TeqipSubcategorymasterRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_subcategorymaster)
public class teqip_subcategorymaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipSubcategorymasterRepository subcategorymasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT ItemSubCategoryID,ItemCategoryID,ItemSubCategory FROM tbl_CNF_PMSS_ItemSubCategory");
            while (resultSet.next()) {
                int ItemSubCategoryID = resultSet.getInt(1);
                TeqipCategorymaster name = teqip_categorymaster_map.get(resultSet.getInt(2));
                if (name == null) {
                    missingData.add("Invalid ItemCategoryID ::tbl_CNF_PMSS_ItemSubCategory ::" + ItemSubCategoryID);
                } else {
                    TeqipSubcategorymaster entity = new TeqipSubcategorymaster();
                    entity.setTeqipCategorymaster(name);
                    entity.setSubCategoryName(resultSet.getString(3));
                    entity.setIsactive(true);
                    teqip_subcategorymaster_map.put(ItemSubCategoryID, entity);
                }

            }
            subcategorymasterRepository.save(teqip_subcategorymaster_map.values());
            subcategorymasterRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
