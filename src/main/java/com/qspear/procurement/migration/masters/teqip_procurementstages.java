//package com.qspear.procurement.migration.masters;
//
//import com.qspear.procurement.migration.Loader;
//import com.qspear.procurement.migration.MasterLoader;
//import com.qspear.procurement.persistence.TeqipPmssInstitutionpurchasecommitte;
//import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
//import com.qspear.procurement.repositories.TeqipPmssProcurementstageRepository;
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.sql.DataSource;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Repository;
//
//@Repository
//@Order(value = Loader.teqip_procurementstages)
//public class teqip_procurementstages implements MasterLoader {
//
//    @Qualifier("secondary")
//    @Autowired
//    private DataSource secondaryDataSource;
//
//    @Autowired
//    TeqipPmssProcurementstageRepository pmssProcurementstageRepository;
//
//    @Override
//    public void load() {
//        System.out.println(this.getClass().getName());
//        Connection connection = null;
//        Statement statement = null;
//        ResultSet resultSet = null;
//
//
//        try {
//            connection = secondaryDataSource.getConnection();
//            statement = connection.createStatement();
//
//            resultSet = statement.executeQuery("SELECT "
//                    + "  [ProcurementStageID],"
//                    + "  [ProcurementStage],"
//                    + "  [Description]"
//                    + "  FROM tbl_CNF_PMSS_ProcurementStages");
//            while (resultSet.next()) {
//
//                TeqipPmssProcurementstage entity = new TeqipPmssProcurementstage();
//
//                entity.setProcurementstageid(resultSet.getInt(1));
//                entity.setProcurementstages(resultSet.getString(2));
//                entity.setStageDescription(resultSet.getString(3));
//
//                teqip_procurementstages_map.put(resultSet.getInt(1),entity);
//
//            }
//
//            pmssProcurementstageRepository.save(teqip_procurementstages_map.values());
//            pmssProcurementstageRepository.flush();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            Loader.closeResources(resultSet, statement, connection);
//        }
//    }
//
//}
