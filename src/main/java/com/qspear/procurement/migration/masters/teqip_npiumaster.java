package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipInstitutionsubcomponentmapping;
import com.qspear.procurement.persistence.TeqipNpiuMaster;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.repositories.NpiuMasterRepository;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_npiumaster)
public class teqip_npiumaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    NpiuMasterRepository npiuMasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipNpiuMaster> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT e.CompanyInfoID, e.LicenseCode, e.CompanyName ,e.Address,"
                    + " e.PMSS_ProjectName, e.PMSS_LoanAccountNumber,e.Email,e.PMSS_AllocatedBudget, e.PMSS_ProjectPrice,e.PMSS_FaxNumber"
                    + " FROM tbl_PM_CompanyInformation e ");
            while (resultSet.next()) {
                TeqipNpiuMaster entity = new TeqipNpiuMaster();

                entity.setCode(resultSet.getString(2));
                entity.setName(resultSet.getString(3));
                entity.setAddress(resultSet.getString(4));
                entity.setProjectName(resultSet.getString(5));
                entity.setCreditNumber(resultSet.getString(6));
                entity.setEmailID(resultSet.getString(7));
                entity.setAllocatedBudget(resultSet.getDouble(8));
                entity.setProjectCost(resultSet.getDouble(9));
                entity.setFaxNumber(resultSet.getString(10));
                entity.setCreatedOn(new Timestamp(new Date().getTime()));
                entity.setCreatedBy(1);
                entity.setModifyOn(new Timestamp(new Date().getTime()));
                entity.setModifyBy(1);
                
                mapping.add(entity);

            }
            npiuMasterRepository.save(mapping);
            npiuMasterRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
