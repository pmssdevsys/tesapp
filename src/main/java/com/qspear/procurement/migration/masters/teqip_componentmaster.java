/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipComponentmaster;
import com.qspear.procurement.repositories.ComponentMasterRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_componentmaster)
public class teqip_componentmaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    ComponentMasterRepository componentMasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT ComponentID,ComponentCode,ComponentName FROM tbl_CNF_PMSS_Component");
            while (resultSet.next()) {

                TeqipComponentmaster entity = new TeqipComponentmaster();
                entity.setComponentCode(resultSet.getString(2));
                entity.setComponentName(resultSet.getString(3));
                entity.setIsactive(true);

                teqip_componentmaster_map.put(resultSet.getInt(1), entity);

            }

            componentMasterRepository.save(teqip_componentmaster_map.values());
            componentMasterRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
