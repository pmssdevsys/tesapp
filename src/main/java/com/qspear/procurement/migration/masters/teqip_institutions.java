/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.repositories.TeqipInstitutionRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_institutions)
public class teqip_institutions implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipInstitutionRepository institutionRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT "
                    + "  InstitutionID,"
                    + "  InstitutionCode,"
                    + "  InstitutionName,"
                    + "  State,"
                    + "  Address,"
                    + "  EmailID,"
                    + "  TelephoneNumber,"
                    + "  FaxNumber,"
                    + "  InstitutionTypeID,"
                    + "  SPFUID,"
                    + "  WebSiteURL,"
                    + "  AllocatedBudget"
                    + "  FROM tbl_CNF_PMSS_Institution");
            while (resultSet.next()) {

                TeqipInstitution entity = new TeqipInstitution();

                entity.setInstitutionCode(resultSet.getString(2));
                entity.setInstitutionName(resultSet.getString(3));
                entity.setState(resultSet.getString(4));
                entity.setAddress(resultSet.getString(5));
                entity.setEmailid(resultSet.getString(6));
                entity.setTelephonenumber(resultSet.getString(7));
                entity.setFaxnumber(resultSet.getString(8));
                entity.setTeqipInstitutiontype(resultSet.getInt(9) == 0 ? null : teqip_institutiontype_map.get(resultSet.getInt(9)));
                entity.setTeqipStatemaster(resultSet.getInt(10) == 0 ? null : teqip_statemaster_map.get(resultSet.getInt(10)));
                entity.setWebsiteurl(resultSet.getString(11));
                entity.setAllocatedbuget(resultSet.getDouble(12));
                entity.setCreatedBy(1);
                entity.setCreatedOn(new Timestamp(new Date().getTime()));

                teqip_institutions_map.put(resultSet.getInt(1), entity);

            }
            institutionRepository.save(teqip_institutions_map.values());
            institutionRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
