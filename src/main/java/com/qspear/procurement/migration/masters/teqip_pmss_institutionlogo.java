package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipNpiuMaster;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPmssInstitutionlogo;
import com.qspear.procurement.repositories.TeqipPmssInstitutionlogoRepository;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_pmss_institutionlogo)
public class teqip_pmss_institutionlogo implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipPmssInstitutionlogoRepository teqipPmssInstitutionlogoRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ArrayList<TeqipPmssInstitutionlogo> mapping = new ArrayList<>();

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT   "
                    + " InstitutionID, "
                    + " 0 as SPFUID , "
                    + " OriginalFileName,"
                    + " SystemFileName,  "
                    + " Description  "
                    + " FROM tbl_CNF_PMSS_InstitutionLogo "
                    + " union "
                    + " SELECT    "
                    + " 0 as InstitutionID,"
                    + "	SPFUID, "
                    + " OriginalFileName, "
                    + " SystemFileName, "
                    + " Description  "
                    + " FROM tbl_CNF_PMSS_SPFULogo");
            while (resultSet.next()) {

                TeqipPmssInstitutionlogo entity = new TeqipPmssInstitutionlogo();
                entity.setTeqipInstitution(teqip_institutions_map.get(resultSet.getInt(1)));
                entity.setTeqipStatemaster(teqip_statemaster_map.get(resultSet.getInt(2)));
                entity.setTypeNpcEdcil(0);
                entity.setOriginalFileName(resultSet.getString(3));
                entity.setSystemFileName("Attachments" + File.separator + "Logo" + File.separator + resultSet.getString(4));
                entity.setDescription(resultSet.getString(5));
                entity.setCreatedBy(1);
                entity.setCreatedOn(new Timestamp(new Date().getTime()));

                mapping.add(entity);

            }
            teqipPmssInstitutionlogoRepository.save(mapping);
            teqipPmssInstitutionlogoRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }

    }

}
