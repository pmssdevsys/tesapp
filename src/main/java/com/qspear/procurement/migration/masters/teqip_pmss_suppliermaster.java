/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import static com.qspear.procurement.migration.Loader.teqip_categorymaster_map;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_pmss_suppliermaster)
public class teqip_pmss_suppliermaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    private SupplierRepository supplierRepository;

    @Override
    public void load() {
        ArrayList<String> sourceList = new ArrayList<>();
        sourceList.add("PMSS Database");
        sourceList.add("Website");
        sourceList.add("Yellow Pages");
        sourceList.add("Advertisement");
        sourceList.add("Notice Board");

        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery(" SELECT "
                    + "  BidderDetailID,"
                    + "  BidderName,"
                    + "  BidderAddress,"
                    + "  City,"
                    + "  State,"
                    + "  Country,"
                    + "  Nationality,"
                    + "  BidderSource,"
                    + "  NameOfRepresentative,"
                    + "  PinCode,"
                    + "  BidderPhone,"
                    + "  TANNumber,"
                    + "  FAXNumber,"
                    + "  TaxNumber,"
                    + "  PANNumber,"
                    + "  EmailID,"
                    + "  GeneralComments,"
                    + "  OtherSourceComments,"
                    + "  m.ProcurementMethodCode,"
                    + "  c.ItemCategory"
                    + "  FROM tbl_PP_PMSS_BidderDetails bd"
                    + " INNER JOIN tbl_PP_PMSS_PTPackageDetails pd ON pd.PTPackageID = bd.PTPackageID"
                    + " INNER JOIN tbl_PP_PMSS_PPPackageDetails p ON  pd.PPPackageID = p.PPPackageID"
                    + " INNER JOIN  tbl_CNF_PMSS_ItemCategory c ON c.ItemCategoryID= p.ItemCategoryID"
                    + " INNER JOIN tbl_CNF_PMSS_ProcurementMethod m on m.ProcurementMethodID =p.ProcurementMethodID"
            );
            while (resultSet.next()) {

                TeqipPmssSupplierMaster entity = new TeqipPmssSupplierMaster();
                entity.setSupplierName(resultSet.getString(2));
                entity.setSupllierAddress(resultSet.getString(3));
                entity.setSupplierCity(resultSet.getString(4));
                entity.setSupplierState(resultSet.getString(5));
                entity.setSupplierCountry(resultSet.getString(6));
                entity.setSupplierNationality(resultSet.getString(7));
                String source = resultSet.getString(8);
                entity.setSupplierSource(source);
                entity.setSupplierSpecificSource(resultSet.getString(18));
                entity.setSupplierRepresentativeName(resultSet.getString(9));
                entity.setSupplierPincode(resultSet.getString(10));
                entity.setSupplierPhoneno(resultSet.getString(11));
                entity.setSuplierTanNumber(resultSet.getString(12));
                entity.setSuplierFaxNumber(resultSet.getString(13));
                entity.setSuplierTaxNumber(resultSet.getString(14));
                entity.setSuplierPanNumber(resultSet.getString(15));
                entity.setSuplierEmailId(resultSet.getString(16));
                entity.setRemarks(resultSet.getString(17));
                entity.setPmssType(4);

                String procurementmethodCode = resultSet.getString(19);
                String itemCategory = resultSet.getString(20);

                if ("Shopping".equalsIgnoreCase(procurementmethodCode) && "Goods".equalsIgnoreCase(itemCategory)) {
                    entity.setPmssType(1);
                }

                if ("Shopping".equalsIgnoreCase(procurementmethodCode) && "Civil Works".equalsIgnoreCase(itemCategory)) {
                    entity.setPmssType(2);
                }
                if ("Direct Contract".equalsIgnoreCase(procurementmethodCode) && "Goods".equalsIgnoreCase(itemCategory)) {
                    entity.setPmssType(1);
                }

                if ("Direct Contract".equalsIgnoreCase(procurementmethodCode) && "Civil Works".equalsIgnoreCase(itemCategory)) {
                    entity.setPmssType(2);
                }

                 if ("NCB".equalsIgnoreCase(procurementmethodCode) && "Goods".equalsIgnoreCase(itemCategory)) {
                    entity.setPmssType(3);
                }

                if ("NCB".equalsIgnoreCase(procurementmethodCode) && "Civil Works".equalsIgnoreCase(itemCategory)) {
                    entity.setPmssType(2);
                }

    
                teqip_pmss_suppliermaster_map.put(resultSet.getInt(1), entity);
            }

            supplierRepository.save(teqip_pmss_suppliermaster_map.values());
            supplierRepository.flush();
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
