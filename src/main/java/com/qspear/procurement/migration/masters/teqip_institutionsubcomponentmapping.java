package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipInstitution;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipInstitutionsubcomponentmapping;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;
import com.qspear.procurement.repositories.TeqipInstitutionsubcomponentmappingRepository;
import java.util.ArrayList;
import org.springframework.core.annotation.Order;

@Repository
@Order(value = Loader.teqip_institutionsubcomponentmapping)
public class teqip_institutionsubcomponentmapping implements MasterLoader {
    
    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;
    
    @Autowired
    TeqipInstitutionsubcomponentmappingRepository teqipInstitutionsubcomponentmappingRepository;
    
    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<TeqipInstitutionsubcomponentmapping> mapping = new ArrayList<>();
        
        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " InstitutionSubComponentMappingID,"
                    + " InstitutionID,"
                    + " NULL,"
                    + " SubComponentID,"
                    + " AllocatedBudget,"
                    + " BudgetForProcurement,"
                    + " BudgetForCW,"
                    + " BudgetForServices"
                    + " FROM tbl_CNF_PMSS_InstitutionSubcomponentMapping");
            while (resultSet.next()) {
                
                TeqipInstitution get = teqip_institutions_map.get(resultSet.getInt(2));
                TeqipSubcomponentmaster get1 = teqip_subcomponentmaster_map.get(resultSet.getInt(4));
                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid InstitutionID ::tbl_CNF_PMSS_InstitutionSubcomponentMapping ::" + id);
                    
                } else if (get1 == null) {
                    missingData.add("Invalid SubComponentID ::tbl_CNF_PMSS_InstitutionSubcomponentMapping ::" + id);
                    
                } else {
                    TeqipInstitutionsubcomponentmapping entity = new TeqipInstitutionsubcomponentmapping();
                    
                    entity.setTeqipInstitution(get);
                    entity.setTeqipStatemaster(null);
                    entity.setTeqipSubcomponentmaster(get1);
                    entity.setAllocatedbudget(resultSet.getDouble(5));
                    entity.setBudgetforprocuremen(resultSet.getDouble(6));
                    entity.setBudgetforcw(resultSet.getDouble(7));
                    entity.setBudgetforservices(resultSet.getDouble(8));
                    entity.setIsactive(Boolean.TRUE);
                    mapping.add(entity);
                    
                }
            }
            teqipInstitutionsubcomponentmappingRepository.save(mapping);
            teqipInstitutionsubcomponentmappingRepository.flush();
            
        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
    
}
