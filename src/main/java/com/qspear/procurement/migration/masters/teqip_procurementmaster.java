/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipProcurementmaster;
import com.qspear.procurement.repositories.ProcurementPlanRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_procurementmaster)
public class teqip_procurementmaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    ProcurementPlanRepository procurementPlanRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT PPPeriodID,PPTitle,PPTitle,IsCurrentPlan,StartDate,EndDate "
                    + " FROM tbl_CNF_PMSS_ProcurementPlanPeriod");
            while (resultSet.next()) {

                TeqipProcurementmaster entity = new TeqipProcurementmaster();
                entity.setShortName(resultSet.getString(2));
                entity.setPlanName(resultSet.getString(3));
                entity.setIsactive(true);
                entity.setStartDate(resultSet.getDate(5));
                entity.setEndDate(resultSet.getDate(6));
                entity.setPlanAmount(null);

                teqip_procurementmaster_map.put(resultSet.getInt(1), entity);
            }
            procurementPlanRepository.save(teqip_procurementmaster_map.values());
            procurementPlanRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }
}
