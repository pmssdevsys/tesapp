/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipCitymaster;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.repositories.CityMasterRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;
import com.qspear.procurement.repositories.StateMasterRepository;
import java.util.ArrayList;
import org.springframework.util.StringUtils;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_citymaster)
public class teqip_citymaster implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    CityMasterRepository cityMasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
//        Connection connection = null;
//        Statement statement = null;
//        ResultSet resultSet = null;
//        ArrayList<TeqipCitymaster> list = new ArrayList<>();
//
//        try {
//            connection = secondaryDataSource.getConnection();
//            statement = connection.createStatement();
//
//            resultSet = statement.executeQuery(" select DISTINCT state , city, PinCode "
//                    + " from tbl_PP_PMSS_BidderDetails "
//                    + " order by State;");
//            while (resultSet.next()) {
//
//                String state = Loader.camelCase(resultSet.getString(1));
//                String city = Loader.camelCase(resultSet.getString(2));
//
//                if (state == null || "".equals(state) 
//                        || !teqip_statemaster_string_map.containsKey(state)
//                        || city == null || "".equals(city)) {
//                    continue;
//                }
//
//                TeqipCitymaster entity = new TeqipCitymaster();
//
//                entity.setStatemaster(teqip_statemaster_string_map.get(state));
//                entity.setCityName(city);
//                try {
//                    entity.setPincode(resultSet.getInt(3));
//                } catch (Exception ex) {
//                }
//                list.add(entity);
//            }
//
//            cityMasterRepository.save(list);
//            cityMasterRepository.flush();
//        } catch (SQLException ex) {
//            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            Loader.closeResources(resultSet, statement, connection);
//        }
    }

}
