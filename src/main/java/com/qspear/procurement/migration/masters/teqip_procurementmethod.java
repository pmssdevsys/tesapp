/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.repositories.ProcurementmethodRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_procurementmethod)
public class teqip_procurementmethod implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    ProcurementmethodRepository procurementmethodRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT "
                    + " m.ProcurementMethodID,"
                    + " m.ProcurementMethodCode,"
                    + " m.ProcurementMethod,"
                    + " m.ProcurementCategory"
                    + " FROM tbl_CNF_PMSS_ProcurementMethod m");

            while (resultSet.next()) {

                if (resultSet.getString(4) != null && resultSet.getString(4).toUpperCase().indexOf("GOODS")>=0) {
                    TeqipCategorymaster get = teqip_categorymaster_map_str.get("Goods");
                    TeqipProcurementmethod entity = new TeqipProcurementmethod();
                    entity.setProcurementmethodCode(resultSet.getString(2));
                    entity.setProcurementmethodName(resultSet.getString(3));
                    entity.setTeqipCategorymaster(get);
                    teqip_procurementmethod_map_str.put(resultSet.getInt(1)+"_G" , entity);
                }
                 if (resultSet.getString(4) != null && resultSet.getString(4).toUpperCase().indexOf("WORKS")>=0) {
                    TeqipCategorymaster get = teqip_categorymaster_map_str.get("Civil Works");
                    TeqipProcurementmethod entity = new TeqipProcurementmethod();
                    entity.setProcurementmethodCode(resultSet.getString(2));
                    entity.setProcurementmethodName(resultSet.getString(3));
                    entity.setTeqipCategorymaster(get);
                    teqip_procurementmethod_map_str.put(resultSet.getInt(1)+"_W", entity);
                }
                  if (resultSet.getString(4) != null && resultSet.getString(4).toUpperCase().indexOf("SERVICES")>=0) {
                    TeqipCategorymaster get = teqip_categorymaster_map_str.get("Services");
                    TeqipProcurementmethod entity = new TeqipProcurementmethod();
                    entity.setProcurementmethodCode(resultSet.getString(2));
                    entity.setProcurementmethodName(resultSet.getString(3));
                    entity.setTeqipCategorymaster(get);
                    teqip_procurementmethod_map_str.put(resultSet.getInt(1)+"_S", entity);
                }

            }

            procurementmethodRepository.save(teqip_procurementmethod_map_str.values());
            procurementmethodRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
