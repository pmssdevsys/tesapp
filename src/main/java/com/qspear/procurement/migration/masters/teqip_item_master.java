/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import com.qspear.procurement.repositories.ItemRepository;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.core.annotation.Order;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_item_master)
public class teqip_item_master implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    ItemRepository itemRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " t.Item_Name,"
                    + " t.Item_Description,"
                    + " pp.ItemCategoryID,"
                    + " pp.ItemSubcategoryID,"
                    + " t.CreatedDate,"
                    + " t.CreatedBy,"
                    + " t.ModifiedDate,"
                    + " t.ModifiedBy,"
                    + " PPItemID"
                    + "  FROM tbl_PP_PMSS_PPItemDetails t"
                    + " INNER JOIN tbl_PP_PMSS_PPPackageDetails pp ON pp.PPPackageID =  t.PPPackageID");
            while (resultSet.next()) {
                TeqipCategorymaster get = teqip_categorymaster_map.get(resultSet.getInt(3));
                TeqipSubcategorymaster get1 = teqip_subcategorymaster_map.get(resultSet.getInt(4));
                int id = resultSet.getInt(9);

                TeqipItemMaster entity = new TeqipItemMaster();

                entity.setItemName(resultSet.getString(1));
                entity.setDescription(resultSet.getString(2));
                entity.setTeqipCategorymaster(get);
                entity.setTeqipSubcategorymaster(get1);
                entity.setCreatedOn(new Timestamp(new Date().getTime()));
                entity.setCreatedBy(1);
                entity.setModifyDate(new Date());
                entity.setModifyBy(1);
                entity.setStatus(true);

                teqip_item_master_map.put(id, entity);
            }
            itemRepository.save(teqip_item_master_map.values());
            itemRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
