/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration.masters;

import com.qspear.procurement.migration.Loader;
import com.qspear.procurement.migration.MasterLoader;
import com.qspear.procurement.persistence.TeqipInstitution;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import com.qspear.procurement.repositories.TeqipInstitutiondepartmentmappingRepository;
import com.qspear.procurement.repositories.method.DepartmentMasterRepository;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.core.annotation.Order;

/**
 *
 * @author jaspreet
 */
@Repository
@Order(value = Loader.teqip_institutiondepartmentmapping)
public class teqip_institutiondepartmentmapping implements MasterLoader {

    @Qualifier("secondary")
    @Autowired
    private DataSource secondaryDataSource;

    @Autowired
    TeqipInstitutiondepartmentmappingRepository teqipInstitutiondepartmentmappingRepository;

    @Autowired
    DepartmentMasterRepository departmentMasterRepository;

    @Override
    public void load() {
        System.out.println(this.getClass().getName());
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            connection = secondaryDataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(" SELECT "
                    + " m.InstitutionDepartmentMappingID,"
                    + " m.InstitutionID,"
                    + " m.DepartmentName,"
                    + " m.DepartmentHead"
                    + " FROM tbl_CNF_PMSS_InstitutionDepartmentMapping  m");
            while (resultSet.next()) {

                TeqipInstitution get = teqip_institutions_map.get(resultSet.getInt(2));

                int id = resultSet.getInt(1);
                if (get == null) {
                    missingData.add("Invalid InstitutionID ::tbl_CNF_PMSS_InstitutionDepartmentMapping ::" + id);

                } else {
                    TeqipInstitutiondepartmentmapping entity = new TeqipInstitutiondepartmentmapping();

                   
                    entity.setTeqipInstitution(get);
                    entity.setTeqipPmssdepartmentmaster(teqip_pmssdepartmentmaster_map.get(resultSet.getString(3)));
                    entity.setDepartmentHead(resultSet.getString(4));
                    entity.setIsactive(true);
                    entity.setCreatedBy(1);
                    entity.setCreatedOn(new Timestamp(new Date().getTime()));
                    entity.setModifyOn(new Timestamp(new Date().getTime()));
                    entity.setModifyBy(1);

                    teqip_institutiondepartmentmapping_map.put(id, entity);
                }
            }
            teqipInstitutiondepartmentmappingRepository.save(teqip_institutiondepartmentmapping_map.values());
            teqipInstitutiondepartmentmappingRepository.flush();

        } catch (SQLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            Loader.closeResources(resultSet, statement, connection);
        }
    }

}
