/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.migration;

import com.qspear.procurement.persistence.TeqipActivitiymaster;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipComponentmaster;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import com.qspear.procurement.persistence.TeqipInstitutionsubcomponentmapping;
import com.qspear.procurement.persistence.TeqipInstitutiontype;
import com.qspear.procurement.persistence.TeqipItemGoodsDetail;
import com.qspear.procurement.persistence.TeqipItemGoodsDetailHistory;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipItemWorkDetail;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPackageHistory;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.TeqipProcurementmaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipRolemaster;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;
import com.qspear.procurement.persistence.TeqipUsersDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageIssueRFP;
import com.qspear.procurement.persistence.methods.TeqipPackageQuestionMaster;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.persistence.methods.TeqipPackageTechSubCriteria;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jaspreet
 */
public interface Loader {

    int teqip_procurementmaster = 1;
    int teqip_categorymaster = 2;
    int teqip_subcategorymaster = 3;
    int teqip_componentmaster = 4;
    int teqip_subcomponentmaster = 5;
    int teqip_activitiymaster = 6;
    int teqip_procurementmethod = 7;
    int teqip_procurementmethod_timeline = 8;
    int teqip_institutiontype = 9;
    int teqip_statemaster = 10;
    int teqip_citymaster =11;
    int teqip_institutions = 12;
    int teqip_pmss_suppliermaster = 13;
    int teqip_item_master = 14;
    int teqip_pmssdepartmentmaster = 15;
    int teqip_institutiondepartmentmapping = 16;
    int teqip_institutionsubcomponentmapping = 17;
    int teqip_pmss_threshholdmaster = 18;
    int teqip_rolemaster = 19;
    int teqip_pmss_institutionpurchasecommitte = 20;
    int teqip_pmss_institutionlogo = 21;
    int teqip_procurementstages = 22;
    int teqip_plan_approvalstatus = 23;
    int teqip_users_detail = 24;
    int teqip_users_master = 25;
    int teqip_npiumaster = 26;
    
    int teqip_package = 26;
    int teqip_item_goods_detail = 27;
    int teqip_package_bidadvertisement = 28;
    int teqip_package_documentdetails = 29;
    int teqip_package_supplierdetail = 30;
    int teqip_package_purchaseorder = 31;
    int teqip_package_letteracceptance = 32;
    int teqip_package_purchaseorderdetail = 33;
    int teqip_package_questionmaster = 34;
    int teqip_package_quotationsupplierinput = 35;
    int teqip_package_techcriteria = 36;
    int teqip_package_techsubcriteria = 37;
    int teqip_package_award = 38;
    int teqip_package_grndetail = 39;
    int teqip_package_grnitemdetails = 40;
    int teqip_package_grnitemdistribution = 41;
    int teqip_package_qcbs_contractdetail = 42;
    int teqip_package_techevalcommitee = 43;
    int teqip_item_goods_department_breakup = 44;
    int teqip_item_works_detail = 45;
    int teqip_package_biddetail = 46;
    int teqip_package_bidinstandscc = 47;
    int teqip_package_bidmeetingdetail = 48;
    int teqip_package_financeopenigdata = 49;
    int teqip_package_invitationletter = 50;
    int teqip_package_paymentdetail = 51;
    int teqip_package_pocontractgenereration = 52;
    int teqip_package_rev_history = 53;
    int teqip_item_goods_detail_rev_history = 54;
    int teqip_item_work_detail_rev_history = 55;
    int teqip_procurementmethod_user_timeline = 56;
    int teqip_procurementmethod_revisedtimeline = 57;
    int teqip_package_quotationdetail = 58;
    int teqip_package_quotationevaluationitemdetails = 59;
    int teqip_package_workorder = 60;
    int teqip_package_workordercertificate = 61;
    int teqip_package_consultantevaluation = 62;
    int teqip_package_quotationevaluationworkdetails = 63;
    int teqip_package_amendmentdetail = 64;
    int teqip_package_qcbs_contractgeneration = 65;
    int teqip_package_termreference = 66;
    int teqip_package_extension =67;
    int teqip_package_rfpdetail = 68;
    int teqip_package_rfpdocument =69;
    int teqip_package_issuerfp =70;
    int teqip_package_stage_index = 71;
    
    List<String> missingData = new ArrayList<>();

    Map<Integer, TeqipCategorymaster> teqip_categorymaster_map = new HashMap<>();
    Map<String, TeqipCategorymaster> teqip_categorymaster_map_str = new HashMap<>();

    Map<Integer, TeqipSubcategorymaster> teqip_subcategorymaster_map = new HashMap<>();
    Map<Integer, TeqipSubcomponentmaster> teqip_subcomponentmaster_map = new HashMap<>();
    Map<Integer, TeqipComponentmaster> teqip_componentmaster_map = new HashMap<>();
    Map<Integer, TeqipActivitiymaster> teqip_activitiymaster_map = new HashMap<>();
    Map<Integer, TeqipInstitutiondepartmentmapping> teqip_institutiondepartmentmapping_map = new HashMap<>();
    Map<Integer, TeqipInstitution> teqip_institutions_map = new HashMap<>();
    Map<Integer, TeqipInstitutionsubcomponentmapping> teqip_institutionsubcomponentmapping_map = new HashMap<>();
    Map<String, TeqipPmssDepartmentMaster> teqip_pmssdepartmentmaster_map = new HashMap<>();
    Map<Integer, TeqipInstitutiontype> teqip_institutiontype_map = new HashMap<>();
    Map<Integer, TeqipStatemaster> teqip_statemaster_map = new HashMap<>();
//    Map<String, TeqipStatemaster> teqip_statemaster_string_map = new HashMap<>();

    Map<Integer, TeqipItemMaster> teqip_item_master_map = new HashMap<>();
    Map<Integer, TeqipItemGoodsDetail> teqip_item_goods_detail_map = new HashMap<>();

    Map<Integer, TeqipPackage> teqip_package_map = new HashMap<>();
    Map<Integer, TeqipProcurementmaster> teqip_procurementmaster_map = new HashMap<>();
    Map<String, TeqipProcurementmethod> teqip_procurementmethod_map_str = new HashMap<>();
    Map<Integer, TeqipPmssSupplierMaster> teqip_pmss_suppliermaster_map = new HashMap<>();
    Map<Integer, TeqipPackageGRNDetail> teqip_package_grndetail_map = new HashMap<>();
    Map<Integer, TeqipPackageGRNItemDetail> teqip_package_grnitemdetails_map = new HashMap<>();
    Map<Integer, TeqipPackageQuestionMaster> teqip_package_questionmaster_map = new HashMap<>();
    Map<Integer, TeqipPackageQuestionMaster> teqipPackageEvaQuesMaster_map = new HashMap<>();

    Map<Integer, TeqipPackageTechCriteria> teqip_package_techcriteria_map = new HashMap<>();
    Map<Integer, TeqipUsersDetail> teqip_users_detail_map = new HashMap<>();
        Map<String, TeqipUsersDetail> teqip_users_detail_string_map = new HashMap<>();

    Map<Integer, TeqipRolemaster> teqip_rolemaster_map = new HashMap<>();
    Map<Integer, TeqipPmssProcurementstage> teqip_package_procurementstages_map = new HashMap<>();
    Map<String, TeqipPackageDocument> teqip_package_documentdetails_INV_map = new HashMap<>();
    Map<Integer, TeqipPackageDocument> teqip_package_documentdetails_GRN_map = new HashMap<>();
    Map<Integer, TeqipPackageDocument> teqip_package_documentdetails_ASSET_map = new HashMap<>();

    Map<Integer, TeqipPackageHistory> teqip_package_rev_history_map = new HashMap<>();
    Map<Integer, TeqipItemGoodsDetailHistory> teqip_item_goods_detail_rev_history_map = new HashMap<>();
    Map<Integer, TeqipItemWorkDetail> teqip_item_work_detail_map = new HashMap<>();
    Map<Integer, TeqipPackageTechSubCriteria> teqip_package_techsubcriteria_map = new HashMap<>();

    Map<Integer, TeqipPackageRFPDetail> teqip_package_rfpdetail_map = new HashMap<>();
    Map<Integer, TeqipPackageRFPDocument> teqip_package_rfpdocument_map = new HashMap<>();
    Map<Integer, TeqipPackageIssueRFP> teqip_package_issuerfp_map = new HashMap<>();

    
    Map<Integer, Integer> teqip_procurementstages_map = new HashMap() {
        {
            put(0, 1);
            put(1, 1);
            put(2, 3);
            put(3, 5);
            put(4, 6);
            put(5, 10);
            put(6, 11);
            put(7, 3);
            put(8, 13);

        }
    };

    void load();

    public static String camelCase(String str) {
        if (str == null) {
            return null;
        }
        if ("".equals(str.trim())) {
            return str.trim();
        }
        String[] strArr = str.trim().split(" ");
        if (strArr == null) {
            return str.trim();
        }
        String finalStr = "";
        for (String strTemp : strArr) {
            if (strTemp == null || "".equals(strTemp.trim())) {
                finalStr += "";
                continue;
            }
            strTemp = strTemp.trim();
            if (strTemp.length() > 1) {
                finalStr += strTemp.substring(0, 1).toUpperCase() + strTemp.substring(1).toLowerCase();
            } else {
                finalStr += strTemp.toUpperCase();
            }

        }
        return finalStr;
    }

    public static Date setTimeInDate(Date d, String time) {
        if (d == null) {
            return null;
        }
        int hour = 0;
        int min = 0;

        if (time != null) {
            String[] str = time.split(":");
            try {
                hour = Integer.parseInt(str[0]);
            } catch (Exception e) {
            }
            try {
                min = Integer.parseInt(str[1]);
            } catch (Exception e) {
            }
        }

        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, min);

        return c.getTime();
    }

    public static void closeResources(ResultSet resultSet, Statement statement, Connection connection) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException ex) {
                Logger.getLogger("Loader").log(Level.SEVERE, null, ex);
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                Logger.getLogger("Loader").log(Level.SEVERE, null, ex);
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger("Loader").log(Level.SEVERE, null, ex);
            }
        }
    }
}
