/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.security.util;

/**
 *
 * @author jaspreet
 */
public class LoggedUser {

    private static final ThreadLocal<LoginUser> userHolder
            = new ThreadLocal<>();

    public static void logIn(LoginUser user) {
        userHolder.set(user);
    }

    public static void logOut() {
        userHolder.remove();
    }

    public static LoginUser get() {
        return userHolder.get();
    }
}
