package com.qspear.procurement.security.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import static com.qspear.procurement.security.constant.SecurityConstants.EXPIRATION_TIME;
import static com.qspear.procurement.security.constant.SecurityConstants.SECRET;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.qspear.procurement.persistence.TeqipUsersMaster;
import com.qspear.procurement.persistence.User;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenGenerator {

    public static String token(User usr) {
        Map<String, Object> claim = new HashMap<>();

        if (null != usr.getTeqipRolemaster()) {
            claim.put("roleId", usr.getTeqipRolemaster().getRoleId());
            claim.put("pmssrole", usr.getTeqipRolemaster().getPmssRole());
            claim.put("roledescription", usr.getTeqipRolemaster().getRoleDescription());

        }

        if (null != usr.getTeqipUsersMasters() && usr.getTeqipUsersMasters().size() > 0) {
            TeqipUsersMaster teqipUsersMaster = usr.getTeqipUsersMasters().get(0);
            if (null != teqipUsersMaster && null != teqipUsersMaster.getTeqipInstitution()) {
                claim.put("instid", teqipUsersMaster.getTeqipInstitution().getInstitutionId());
                claim.put("instcode", teqipUsersMaster.getTeqipInstitution().getInstitutionCode());
                claim.put("instname", teqipUsersMaster.getTeqipInstitution().getInstitutionName());
                claim.put("instType", teqipUsersMaster.getTeqipInstitution().getTeqipInstitutiontype().getInstitutiontypeId());

            }
            if (null != teqipUsersMaster && null != teqipUsersMaster.getTeqipStatemaster()) {
                claim.put("statecode", teqipUsersMaster.getTeqipStatemaster().getStateCode());
                claim.put("stateid", teqipUsersMaster.getTeqipStatemaster().getStateId());
                claim.put("statname", teqipUsersMaster.getTeqipStatemaster().getStateName());

            }
        }
        
        claim.put("userid", usr.getUserid());
        claim.put("username", usr.getUserName());
        claim.put("name", usr.getName());
        claim.put("firstName", usr.getFname());
        claim.put("middleName", usr.getMiddleName());
        claim.put("lastName", usr.getLname());

        String token = Jwts.builder().setSubject(usr.getUserName()).setClaims(claim)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET.getBytes()).compact();

        return token;
    }

    public static LoginUser parseJWT(String jwt) {
        // This line will throw an exception if it is not a signed JWS (as expected)
        try {
            Claims claims = Jwts.parser().setSigningKey(SECRET.getBytes()).parseClaimsJws(jwt).getBody();
            if (null != claims) {
                ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
                LoginUser pojo = mapper.convertValue(claims, LoginUser.class);

                return pojo;
            }
        } catch (Exception ex) {
            System.out.println("Error while parsing calims" + ex.getMessage());
        }
        return null;
    }

}
