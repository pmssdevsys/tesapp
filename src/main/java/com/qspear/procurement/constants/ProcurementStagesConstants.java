package com.qspear.procurement.constants;

public interface ProcurementStagesConstants {
	Integer UNDER_PREPARATION = 1;
	Integer STATE_APPROVAL = 2;
	Integer STATE_REJECTION = 3;
	Integer NPIU_APPROVAL = 4;
	Integer NPIU_REJECTION = 5;
	Integer EDCIL_APPROVAL = 6;
	Integer EDCIL_REJECTION = 7;
}
