package com.qspear.procurement.constants;

public enum ProcurementStages {
	UNDERPREPARATION(1), SPFUAPPROVAL(2), NPIUAPPROVAL(3), SPFUREJECTED(4), NPIUREJECTED(5), NPIUDIRECTORAPPROVED(
			6), NPIUDIRECTORREJECTED(7), UNKNOWN(0);

	private Integer type;

	private ProcurementStages(Integer type) {
		this.type = type;
	}

	public Integer getType() {
		return this.type;
	}

	public static ProcurementStages valueByType(Integer type) {
		for (ProcurementStages typeOfPlanCreator : ProcurementStages.values()) {
			if (typeOfPlanCreator.type.equals(type)) {
				return typeOfPlanCreator;
			}
		}
		return UNKNOWN;
	}
}
