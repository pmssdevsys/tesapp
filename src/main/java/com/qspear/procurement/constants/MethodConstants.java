/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.constants;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author jaspreet
 */
public interface MethodConstants {

    static boolean isAppDocCategory(String documentCategory) {
        return getFieldnames().contains(documentCategory);
    }

    static ArrayList<String> getFieldnames() {
        ArrayList<String> value = new ArrayList<>();

        Field[] fields = MethodConstants.class.getFields();
        for (Field field : fields) {
            if (field.getType().equals(String.class)) {
                try {
                    value.add((String) field.get(null));
                } catch (IllegalAccessException ex) {
                }
            }
        }

        return value;
    }

    String UPLOAD_LOCATION = "D:\\upload";
//    String UPLOAD_LOCATION = "/opt/uploads";

    String SUBMIT = "SUBMIT";
    String REJECTED = "REJECTED";
    String APPROVED = "APPROVED";
    String REVISION = "REVISION";

    String DOC_CAT_INVITATION_GOODS = "INVITATION_GOODS";
    String DOC_CAT_INVITATION_GOODS_DC = "INVITATION_GOODS_DC";
    String DOC_CAT_INVITATION_GOODS_BLANK = "INVITATION_GOODS_BLANK";

    String DOC_CAT_INVITATION_WORKS = "INVITATION_WORKS";
    String DOC_CAT_INVITATION_WORKS_BLANK = "INVITATION_WORKS_BLANK";


    String DOC_CAT_QUOTATION_OPENING = "QUOTATION_OPENING";
    String DOC_CAT_NCB_BID_OPENING = "NCB_BID_OPENING";
    String DOC_CAT_QCBS_EOI_OPENING = "QCBS_EOI_OPENING";

    String DOC_CAT_QUOTATION_EVALUATION = "QUOTATION_EVALUATION";
    String DOC_CAT_NCB_BID_EVALUATION = "NCB_BID_EVALUATION";
    String DOC_CAT_QCBS_FINANCIAL_OPENING = "QCBS_FINANCIAL_OPENING";

    String DOC_CAT_PURCHASE_ORDER = "PURCHASE_ORDER";
    String DOC_CAT_PURCHASE_ORDER_DC = "PURCHASE_ORDER_DC";
    String DOC_CAT_PURCHASE_ORDER_NCB = "PURCHASE_ORDER_NCB";

    String DOC_CAT_WORK_ORDER = "WORK_ORDER";
    String DOC_CAT_WORK_ORDER_NCB = "WORK_ORDER_NCB";

    String DOC_CAT_LETTER_OF_ACCEPTANCE = "LETTER_OF_ACCEPTANCE";
    String DOC_CAT_LETTER_OF_ACCEPTANCE_DC = "LETTER_OF_ACCEPTANCE_DC";
    String DOC_CAT_LETTER_OF_ACCEPTANCE_NCB = "LETTER_OF_ACCEPTANCE_NCB";

    String DOC_CAT_GRN_LETTER = "GRN_LETTER";
    String DOC_CAT_GRN_LETTER_NCB = "GRN_LETTER_NCB";

    String DOC_CAT_ASSET_LETTER = "ASSET_LETTER";
    String DOC_CAT_ASSET_LETTER_NCB = "ASSET_LETTER_NCB";

    String DOC_CAT_NCB_SBD = "NCB_SBD";
    String DOC_CAT_NCB_SBD_WORK = "NCB_SBD_WORKS";
    String DOC_CAT_NCB_BID_AWARD = "NCB_BID_AWARD";
    String DOC_CAT_NCB_ADVERTISEMENT = "NCB_ADVERTISEMENT";
    String DOC_CAT_NCB_CONTRACT = "NCB_CONTRACT";

    String DOC_CAT_QCBS_ADVERTISEMENT = "QCBS_ADVERTISEMENT";
    String DOC_CAT_QCBS_RFP = "QCBS_RFP";
    String DOC_CAT_QCBS_CONTRACT = "QCBS_CONTRACT";
    String DOC_CAT_QCBS_PRIOR_REVIEW = "QCBS_PRIOR_REVIEW";
    String DOC_CAT_QCBS_TECH_OPENING = "QCBS_TECH_OPENING";
    String DOC_CAT_QCBS_TECH_EVAL = "QCBS_TECH_EVAL";
    String DOC_CAT_INVITATION_QCBS = "INVITATION_QCBS";
    String DOC_CAT_INVITATION_FINANCIAL_QCBS = "INVITATION_FINANCIAL_QCBS";
    String DOC_CAT_QCBS_CONTRACT_LUMSUM = "QCBS_CONTRACT_LUMSUM";
    String DOC_CAT_QCBS_CONTRACT_TIMEBASED = "QCBS_CONTRACT_TIMEBASED";
}
