package com.qspear.procurement.constants;

public enum TypeOfPlanCreator {
	INSTSTATEFUNDED(1), INSTCENTRALFUNDED(2), STATE(3), NPIU(4), EDCIL(5), UNKNOWN(0);

	private Integer type;

	private TypeOfPlanCreator(Integer type) {
		this.type = type;
	}

	public Integer getType() {
		return this.type;
	}

	public static TypeOfPlanCreator valueByType(Integer type) {
		for (TypeOfPlanCreator typeOfPlanCreator : TypeOfPlanCreator.values()) {
			if (typeOfPlanCreator.type.equals(type)) {
				return typeOfPlanCreator;
			}
		}
		return UNKNOWN;
	}
}
