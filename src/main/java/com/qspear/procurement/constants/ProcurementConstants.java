package com.qspear.procurement.constants;

public interface ProcurementConstants {

    String PMSSROLE = "pmssrole";

    String PACKAGE_CODE = "TEQIP-III/";

    String PACKAGE_CRITERIA = "ALL";

    String USER_ROLE_INST = "INST";
    String USER_ROLE_INST_DIR = "INST_DIR";

    String USER_ROLE_NPIU = "NPIU";
    String USER_ROLE_NPC = "NPC";
    String USER_ROLE_CPA = "CPA";
    String USER_ROLE_EDCIL = "EDCIL";

    String USER_ROLE_SPA = "SPA";
    String USER_ROLE_SPFU = "SPFU";

    String USD_CURRENCY_CODE = "USD";

    String INSTID = "instid";
    String TYPE = "type";
    String STATEID = "stateid";
    String ROLEID = "roleId";

    String USER_AUTH_REQ_OBJECT = "claim";

    static boolean isStateRole(String role) {
        return USER_ROLE_SPFU.equalsIgnoreCase(role)
                || USER_ROLE_SPA.equalsIgnoreCase(role);
    }

    static boolean isNationalRole(String role) {
        return USER_ROLE_NPIU.equalsIgnoreCase(role)
                || USER_ROLE_CPA.equalsIgnoreCase(role)
                || USER_ROLE_NPC.equalsIgnoreCase(role);
    }

    static boolean isEdcilRole(String role) {
        return USER_ROLE_EDCIL.equalsIgnoreCase(role);
    }

    static boolean isInstitutionalRole(String role) {
        return USER_ROLE_INST.equalsIgnoreCase(role)
                || USER_ROLE_INST_DIR.equalsIgnoreCase(role);
    }
}
