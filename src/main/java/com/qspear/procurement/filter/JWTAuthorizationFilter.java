package com.qspear.procurement.filter;

import static com.qspear.procurement.security.constant.SecurityConstants.HEADER_STRING;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSException;
import com.qspear.procurement.persistence.TrnAuditTrial;
import com.qspear.procurement.repositories.AuditRepository;
import com.qspear.procurement.security.util.LoggedUser;
import com.qspear.procurement.security.util.LoginUser;
import com.qspear.procurement.security.util.TokenGenerator;
@Component
public class JWTAuthorizationFilter implements Filter {
	@Autowired
	AuditRepository auditRepository;

	   
    @Override
    public  void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
    	 HttpServletRequest req = (HttpServletRequest) request;
         HttpServletResponse res = (HttpServletResponse) response;
        String header = req.getHeader(HEADER_STRING);
      try {
    	 // System.out.println(""+header);
            if (req.getRequestURI().equalsIgnoreCase("/pmss/login")
                    || req.getRequestURI().equalsIgnoreCase("/pmss/password/forgetPassword")
                    || req.getRequestURI().equalsIgnoreCase("/pmss/password/resetPassword")
                    || req.getRequestURI().equalsIgnoreCase("/pmss/signup/getRoles")
                    || req.getRequestURI().contains("/pmss/helpdesk/downloadAllfiles")
                    || req.getRequestURI().equalsIgnoreCase("/pmss/signup/getAllInstituteData")
                    || req.getRequestURI().equalsIgnoreCase("/swagger-ui.html")
                    || req.getRequestURI().contains("/webjars/")
                    || req.getRequestURI().contains("/download")
                    || req.getRequestURI().contains("/uploads")
                    || req.getRequestURI().contains("/configuration/security")
                    || req.getRequestURI().contains("/swagger-resources")
                    || req.getRequestURI().contains("/configuration/ui")
                    || req.getRequestURI().contains("/v2/api-docs")) {
                chain.doFilter(req, res);
            } else {
                if (header == null) {
                    //chain.doFilter(req, res);
                    ErrorCode errorCode = new ErrorCode("PMSS-101", "session timed out");
                    throw new PMSSException("Login session is not there.", errorCode);
                }
                
                
                else if(header!=null){
                	TrnAuditTrial trn = auditRepository.expireToken(header);
                int count = 0;
                	try{
                	count	=trn.getLogoutstatus();
                	}
                	catch(Exception e){
                		
                	}
                	if(count==1){
                        //    ErrorCode errorCode = new ErrorCode("PMSS-101", "session timed out");

                    		throw new PMSSException("Invalid Request",new ErrorCode("Invalid Header", "Authorization Key is Not Valid"));
                    	}
                }
                LoginUser tclaim = getAuthentication(req);

                if (null == tclaim) {
                    return;
                }
                
                
           
                          LoggedUser.logIn(tclaim);
                    req.setAttribute("claim", tclaim);
                chain.doFilter(req, res);
                              }
                
                
            
        } catch(Exception e){
        		
    		throw new PMSSException("Login Session is not there",new ErrorCode("Failure", e.getMessage()));
        }

    }

    private LoginUser getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            // parse the token.
            LoginUser parsedClaim = TokenGenerator.parseJWT(token);
            if (null != parsedClaim) {
                return parsedClaim;
            }
            return null;
        }
        return null;
    }

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

		@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}
