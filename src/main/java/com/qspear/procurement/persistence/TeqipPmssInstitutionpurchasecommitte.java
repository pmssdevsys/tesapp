package com.qspear.procurement.persistence;

import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import java.io.Serializable;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the teqip_pmss_institutionpurchasecommitte database
 * table.
 *
 */
@Entity
@Table(name = "teqip_pmss_institutionpurchasecommitte")
@NamedQuery(name = "TeqipPmssInstitutionpurchasecommitte.findAll", query = "SELECT t FROM TeqipPmssInstitutionpurchasecommitte t")
public class TeqipPmssInstitutionpurchasecommitte implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer institutionPurchaseCommitte_ID;

    private String committeMemberName;

    @Column(name = "DesignationName")
    private String designation;

    @Column(name = "PcCommitteName")
    private String PCommitteRole;

    //bi-directional many-to-one association to TeqipInstitution
    @ManyToOne
    @JoinColumn(name = "INSTITUTION_ID")
    private TeqipInstitution teqipInstitution;

    @ManyToOne
    @JoinColumn(name = "SPFUID")
    private TeqipStatemaster statemaster;

    //bi-directional many-to-one association to TeqipPmssdepartmentmaster
    @ManyToOne
    @JoinColumn(name = "DEPARTMENT_ID")
    private TeqipPmssDepartmentMaster teqipPmssdepartmentmaster;

    private String committeetype;
    @Column(name = "ISACTIVE")
    private Boolean isactive;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "MODIFY_BY")
    private Integer modifiedBy;

    private Integer type;

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getCommitteetype() {
        return committeetype;
    }

    public void setCommitteetype(String committeetype) {
        this.committeetype = committeetype;
    }

    /*          	@ManyToOne
	@JoinColumn(name = "SPFUID")
	private TeqipStatemaster teqipStatemaster;

	public TeqipStatemaster getTeqipStatemaster() {
		return teqipStatemaster;
	}

	public void setTeqipStatemaster(TeqipStatemaster stateMaster) {
		this.teqipStatemaster = stateMaster;
	}
     */
    public TeqipPmssInstitutionpurchasecommitte() {
    }

    public Integer getInstitutionPurchaseCommitte_ID() {
        return this.institutionPurchaseCommitte_ID;
    }

    public void setInstitutionPurchaseCommitte_ID(Integer institutionPurchaseCommitte_ID) {
        this.institutionPurchaseCommitte_ID = institutionPurchaseCommitte_ID;
    }

    public String getCommitteMemberName() {
        return this.committeMemberName;
    }

    public void setCommitteMemberName(String committeMemberName) {
        this.committeMemberName = committeMemberName;
    }

    public String getDesignation() {
        return this.designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getPCommitteRole() {
        return this.PCommitteRole;
    }

    public void setPCommitteRole(String PCommitteRole) {
        this.PCommitteRole = PCommitteRole;
    }

    public TeqipInstitution getTeqipInstitution() {
        return this.teqipInstitution;
    }

    public void setTeqipInstitution(TeqipInstitution teqipInstitution) {
        this.teqipInstitution = teqipInstitution;
    }

    public TeqipPmssDepartmentMaster getTeqipPmssdepartmentmaster() {
        return this.teqipPmssdepartmentmaster;
    }

    public void setTeqipPmssdepartmentmaster(TeqipPmssDepartmentMaster teqipPmssdepartmentmaster) {
        this.teqipPmssdepartmentmaster = teqipPmssdepartmentmaster;
    }

    public TeqipStatemaster getStatemaster() {
        return statemaster;
    }

    public void setStatemaster(TeqipStatemaster statemaster) {
        this.statemaster = statemaster;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
