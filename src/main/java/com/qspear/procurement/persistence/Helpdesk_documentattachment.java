package com.qspear.procurement.persistence;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="teqip_helpdesk_documentattachment")
public class Helpdesk_documentattachment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
private Integer 	HelpDesk_DocumentAttachmentID;
private  Integer TicketID;
private String AttachedDocName;
private String AttachedDocType;
private String AttachedDocument;
private Integer AttchedDocSize;
private Boolean IsActive;
private Integer ModifiedBy;
private Date ModifiedDate;
public Integer getHelpDesk_DocumentAttachmentID() {
	return HelpDesk_DocumentAttachmentID;
}
public void setHelpDesk_DocumentAttachmentID(Integer helpDesk_DocumentAttachmentID) {
	HelpDesk_DocumentAttachmentID = helpDesk_DocumentAttachmentID;
}
public Integer getTicketID() {
	return TicketID;
}
public void setTicketID(Integer ticketID) {
	TicketID = ticketID;
}
public String getAttachedDocName() {
	return AttachedDocName;
}
public void setAttachedDocName(String attachedDocName) {
	AttachedDocName = attachedDocName;
}
public String getAttachedDocType() {
	return AttachedDocType;
}
public void setAttachedDocType(String attachedDocType) {
	AttachedDocType = attachedDocType;
}
public String getAttachedDocument() {
	return AttachedDocument;
}
public void setAttachedDocument(String attachedDocument) {
	AttachedDocument = attachedDocument;
}
public Integer getAttchedDocSize() {
	return AttchedDocSize;
}
public void setAttchedDocSize(Integer attchedDocSize) {
	AttchedDocSize = attchedDocSize;
}
public Boolean getIsActive() {
	return IsActive;
}
public void setIsActive(Boolean isActive) {
	IsActive = isActive;
}
public Integer getModifiedBy() {
	return ModifiedBy;
}
public void setModifiedBy(Integer modifiedBy) {
	ModifiedBy = modifiedBy;
}
public Date getModifiedDate() {
	return ModifiedDate;
}
public void setModifiedDate(Date modifiedDate) {
	ModifiedDate = modifiedDate;
}

}
