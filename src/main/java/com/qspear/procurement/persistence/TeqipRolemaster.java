package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the teqip_rolemaster database table.
 * 
 */
@Entity
@Table(name="teqip_rolemaster")
@NamedQuery(name="TeqipRolemaster.findAll", query="SELECT t FROM TeqipRolemaster t")
public class TeqipRolemaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ROLE_ID")
	private Integer roleId;

	@Column(name="CREATED_BY")
	private Integer createdBy;

	@Column(name="CREATED_ON")
	private Timestamp createdOn;

	private Boolean isactive;

	@Column(name="LEVEL_ID")
	private Integer levelId;

	@Column(name="PMSS_ROLE")
	private String pmssRole;

	@Column(name="ROLE_DESCRIPTION")
	private String roleDescription;

	//bi-directional many-to-one association to TeqipUsersDetail
	@OneToMany(mappedBy="teqipRolemaster")
	private List<TeqipUsersDetail> teqipUsersDetails;

	//bi-directional many-to-one association to TeqipUsersMaster
	@OneToMany(mappedBy="teqipRolemaster")
	private List<TeqipUsersMaster> teqipUsersMasters;

	public TeqipRolemaster() {
	}

	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Integer getLevelId() {
		return this.levelId;
	}

	public void setLevelId(Integer levelId) {
		this.levelId = levelId;
	}

	public String getPmssRole() {
		return this.pmssRole;
	}

	public void setPmssRole(String pmssRole) {
		this.pmssRole = pmssRole;
	}

	public String getRoleDescription() {
		return this.roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public List<TeqipUsersDetail> getTeqipUsersDetails() {
		return this.teqipUsersDetails;
	}

	public void setTeqipUsersDetails(List<TeqipUsersDetail> teqipUsersDetails) {
		this.teqipUsersDetails = teqipUsersDetails;
	}

	public TeqipUsersDetail addTeqipUsersDetail(TeqipUsersDetail teqipUsersDetail) {
		getTeqipUsersDetails().add(teqipUsersDetail);
		teqipUsersDetail.setTeqipRolemaster(this);

		return teqipUsersDetail;
	}

	public TeqipUsersDetail removeTeqipUsersDetail(TeqipUsersDetail teqipUsersDetail) {
		getTeqipUsersDetails().remove(teqipUsersDetail);
		teqipUsersDetail.setTeqipRolemaster(null);

		return teqipUsersDetail;
	}

	public List<TeqipUsersMaster> getTeqipUsersMasters() {
		return this.teqipUsersMasters;
	}

	public void setTeqipUsersMasters(List<TeqipUsersMaster> teqipUsersMasters) {
		this.teqipUsersMasters = teqipUsersMasters;
	}

	public TeqipUsersMaster addTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
		getTeqipUsersMasters().add(teqipUsersMaster);
		teqipUsersMaster.setTeqipRolemaster(this);

		return teqipUsersMaster;
	}

	public TeqipUsersMaster removeTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
		getTeqipUsersMasters().remove(teqipUsersMaster);
		teqipUsersMaster.setTeqipRolemaster(null);

		return teqipUsersMaster;
	}

}