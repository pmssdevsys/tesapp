package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
@Entity
@Table(name="teqip_pmssdesignationmaster")
public class DesignationMasterModel implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long DesignationID;
	@NotNull
	private String Designation;
	@NotNull
	private Integer CREATED_BY;
	@NotNull
	private Timestamp CREATED_ON;
	private Integer ISACTIVE;
	private Date MODIFY_ON;
	private Integer MODIFY_BY;
	
	public Long getDesignationID() {
		return DesignationID;
	}
	public void setDesignationID(Long designationID) {
		DesignationID = designationID;
	}
	public String getDesignation() {
		return Designation;
	}
	public void setDesignation(String designation) {
		Designation = designation;
	}
	public Integer getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(Integer cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public Timestamp getCREATED_ON() {
		return CREATED_ON;
	}
	public void setCREATED_ON(Timestamp cREATED_ON) {
		CREATED_ON = cREATED_ON;
	}
	public Integer getISACTIVE() {
		return ISACTIVE;
	}
	public void setISACTIVE(Integer iSACTIVE) {
		ISACTIVE = iSACTIVE;
	}
	public Date getMODIFY_ON() {
		return MODIFY_ON;
	}
	public void setMODIFY_ON(Date mODIFY_ON) {
		MODIFY_ON = mODIFY_ON;
	}
	public Integer getMODIFY_BY() {
		return MODIFY_BY;
	}
	public void setMODIFY_BY(Integer mODIFY_BY) {
		MODIFY_BY = mODIFY_BY;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}



