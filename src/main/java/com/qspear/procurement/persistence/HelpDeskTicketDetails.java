package com.qspear.procurement.persistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="teqip_helpdesk_ticketdetails")
public class HelpDeskTicketDetails {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String ticketType;
private Integer tickettypeId;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public Integer getTickettypeId() {
		return tickettypeId;
	}

	public void setTickettypeId(Integer tickettypeId) {
		this.tickettypeId = tickettypeId;
	}


	

	
}
