package com.qspear.procurement.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TEQIP_INSTITUTIONTYPE")
public class InstitutionType implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long INSTITUTIONTYPE_ID;
	@Column
	private String INSTITUTIONTYPE_NAME;
	@Column
	private Integer ISACTIVE;
	public Long getINSTITUTIONTYPE_ID() {
		return INSTITUTIONTYPE_ID;
	}
	public void setINSTITUTIONTYPE_ID(Long iNSTITUTIONTYPE_ID) {
		INSTITUTIONTYPE_ID = iNSTITUTIONTYPE_ID;
	}
	public String getINSTITUTIONTYPE_NAME() {
		return INSTITUTIONTYPE_NAME;
	}
	public void setINSTITUTIONTYPE_NAME(String iNSTITUTIONTYPE_NAME) {
		INSTITUTIONTYPE_NAME = iNSTITUTIONTYPE_NAME;
	}
	public Integer getISACTIVE() {
		return ISACTIVE;
	}
	public void setISACTIVE(Integer iSACTIVE) {
		ISACTIVE = iSACTIVE;
	}

}
