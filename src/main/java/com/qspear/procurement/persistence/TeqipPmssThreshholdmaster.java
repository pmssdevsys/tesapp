package com.qspear.procurement.persistence;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the teqip_pmss_threshholdmaster database table.
 * 
 */
@Entity
@Table(name="teqip_pmss_threshholdmaster")
@NamedQuery(name="TeqipPmssThreshholdmaster.findAll", query="SELECT t FROM TeqipPmssThreshholdmaster t")
public class TeqipPmssThreshholdmaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
        @GeneratedValue(strategy = GenerationType.AUTO)
	private Integer thresholdid;

	private Boolean isactive;

	private Integer isproprietary;
        
        private Integer isgem;

	private String review;

	@Column(name="threshold_max_value")
	private Double thresholdMaxValue;

	@Column(name="threshold_min_value")
	private Double thresholdMinValue;

	//bi-directional many-to-one association to TeqipCategorymaster
	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name="CATEGORYID")
	private TeqipCategorymaster teqipCategorymaster;

	//bi-directional many-to-one association to TeqipProcurementmethod
	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name="procurementmethodid")
	private TeqipProcurementmethod teqipProcurementmethod;

	public TeqipPmssThreshholdmaster() {
	}

	public Integer getThresholdid() {
		return this.thresholdid;
	}

	public void setThresholdid(Integer thresholdid) {
		this.thresholdid = thresholdid;
	}

	public Boolean getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Integer getIsproprietary() {
		return this.isproprietary;
	}

	public void setIsproprietary(Integer isproprietary) {
		this.isproprietary = isproprietary;
	}

	public String getReview() {
		return this.review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public Double getThresholdMaxValue() {
		return this.thresholdMaxValue;
	}

	public void setThresholdMaxValue(Double thresholdMaxValue) {
		this.thresholdMaxValue = thresholdMaxValue;
	}

	public Double getThresholdMinValue() {
		return this.thresholdMinValue;
	}

	public void setThresholdMinValue(Double thresholdMinValue) {
		this.thresholdMinValue = thresholdMinValue;
	}

	public TeqipCategorymaster getTeqipCategorymaster() {
		return this.teqipCategorymaster;
	}

	public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
		this.teqipCategorymaster = teqipCategorymaster;
	}

	public TeqipProcurementmethod getTeqipProcurementmethod() {
		return this.teqipProcurementmethod;
	}

	public void setTeqipProcurementmethod(TeqipProcurementmethod teqipProcurementmethod) {
		this.teqipProcurementmethod = teqipProcurementmethod;
	}

        public Integer getIsgem() {
            return isgem;
        }

        public void setIsgem(Integer isgem) {
            this.isgem = isgem;
        }

}