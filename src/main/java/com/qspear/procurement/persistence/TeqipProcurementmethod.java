package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the TEQIP_PROCUREMENTMETHOD database table.
 * 
 */
@Entity
@Table(name="TEQIP_PROCUREMENTMETHOD")
public class TeqipProcurementmethod implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="PROCUREMENTMETHOD_ID")
	private Integer procurementmethodId;

	@Column(name="PROCUREMENTMETHOD_CODE")
	private String procurementmethodCode;

	@Column(name="PROCUREMENTMETHOD_NAME")
	private String procurementmethodName;

	//bi-directional many-to-one association to TeqipPackage
	@OneToMany(mappedBy="teqipProcurementmethod", cascade=CascadeType.ALL)
	private List<TeqipPackage> teqipPackages;

	//bi-directional many-to-one association to TeqipPmssThreshholdmaster
	@OneToMany(mappedBy="teqipProcurementmethod", cascade=CascadeType.ALL)
	private List<TeqipPmssThreshholdmaster> teqipPmssThreshholdmasters;

	//bi-directional many-to-one association to TeqipCategorymaster
	@ManyToOne
	@JoinColumn(name="PROCUREMENTMETHOD_CAT_ID")
	private TeqipCategorymaster teqipCategorymaster;

	//bi-directional many-to-one association to TeqipProcurementmethodTimeline
	@OneToMany(mappedBy="teqipProcurementmethod", cascade=CascadeType.ALL)
	private List<TeqipProcurementmethodTimeline> teqipProcurementmethodTimelines;

	public TeqipProcurementmethod() {
	}

	public Integer getProcurementmethodId() {
		return this.procurementmethodId;
	}

	public void setProcurementmethodId(Integer procurementmethodId) {
		this.procurementmethodId = procurementmethodId;
	}

	public String getProcurementmethodCode() {
		return this.procurementmethodCode;
	}

	public void setProcurementmethodCode(String procurementmethodCode) {
		this.procurementmethodCode = procurementmethodCode;
	}

	public String getProcurementmethodName() {
		return this.procurementmethodName;
	}

	public void setProcurementmethodName(String procurementmethodName) {
		this.procurementmethodName = procurementmethodName;
	}

	public List<TeqipPackage> getTeqipPackages() {
		return this.teqipPackages;
	}

	public void setTeqipPackages(List<TeqipPackage> teqipPackages) {
		this.teqipPackages = teqipPackages;
	}

	public TeqipPackage addTeqipPackage(TeqipPackage teqipPackage) {
		getTeqipPackages().add(teqipPackage);
		teqipPackage.setTeqipProcurementmethod(this);

		return teqipPackage;
	}

	public TeqipPackage removeTeqipPackage(TeqipPackage teqipPackage) {
		getTeqipPackages().remove(teqipPackage);
		teqipPackage.setTeqipProcurementmethod(null);

		return teqipPackage;
	}

	public List<TeqipPmssThreshholdmaster> getTeqipPmssThreshholdmasters() {
		return this.teqipPmssThreshholdmasters;
	}

	public void setTeqipPmssThreshholdmasters(List<TeqipPmssThreshholdmaster> teqipPmssThreshholdmasters) {
		this.teqipPmssThreshholdmasters = teqipPmssThreshholdmasters;
	}

	public TeqipPmssThreshholdmaster addTeqipPmssThreshholdmaster(TeqipPmssThreshholdmaster teqipPmssThreshholdmaster) {
		getTeqipPmssThreshholdmasters().add(teqipPmssThreshholdmaster);
		teqipPmssThreshholdmaster.setTeqipProcurementmethod(this);

		return teqipPmssThreshholdmaster;
	}

	public TeqipPmssThreshholdmaster removeTeqipPmssThreshholdmaster(TeqipPmssThreshholdmaster teqipPmssThreshholdmaster) {
		getTeqipPmssThreshholdmasters().remove(teqipPmssThreshholdmaster);
		teqipPmssThreshholdmaster.setTeqipProcurementmethod(null);

		return teqipPmssThreshholdmaster;
	}

	public TeqipCategorymaster getTeqipCategorymaster() {
		return this.teqipCategorymaster;
	}

	public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
		this.teqipCategorymaster = teqipCategorymaster;
	}

	public List<TeqipProcurementmethodTimeline> getTeqipProcurementmethodTimelines() {
		return this.teqipProcurementmethodTimelines;
	}

	public void setTeqipProcurementmethodTimelines(List<TeqipProcurementmethodTimeline> teqipProcurementmethodTimelines) {
		this.teqipProcurementmethodTimelines = teqipProcurementmethodTimelines;
	}

}