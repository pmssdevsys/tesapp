package com.qspear.procurement.persistence;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="teqip_helpdesk_tickethistory")
public class Helpdesk_tickethistory {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private Integer HdnTicketId;
	private String HdcComments;
	private Integer HdnTicketStatusId;
	private Integer ModifiedBy;
	private Date ModifiedOn;
	private Integer HdnTicketSeverityId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getHdnTicketId() {
		return HdnTicketId;
	}
	public void setHdnTicketId(Integer hdnTicketId) {
		HdnTicketId = hdnTicketId;
	}
	public String getHdcComments() {
		return HdcComments;
	}
	public void setHdcComments(String hdcComments) {
		HdcComments = hdcComments;
	}
	public Integer getHdnTicketStatusId() {
		return HdnTicketStatusId;
	}
	public void setHdnTicketStatusId(Integer hdnTicketStatusId) {
		HdnTicketStatusId = hdnTicketStatusId;
	}
	public Integer getModifiedBy() {
		return ModifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		ModifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return ModifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		ModifiedOn = modifiedOn;
	}
	public Integer getHdnTicketSeverityId() {
		return HdnTicketSeverityId;
	}
	public void setHdnTicketSeverityId(Integer hdnTicketSeverityId) {
		HdnTicketSeverityId = hdnTicketSeverityId;
	}
	public Helpdesk_tickethistory(Integer id, Integer hdnTicketId, String hdcComments, Integer hdnTicketStatusId,
			Integer modifiedBy, Date modifiedOn, Integer hdnTicketSeverityId) {
		super();
		this.id = id;
		HdnTicketId = hdnTicketId;
		HdcComments = hdcComments;
		HdnTicketStatusId = hdnTicketStatusId;
		ModifiedBy = modifiedBy;
		ModifiedOn = modifiedOn;
		HdnTicketSeverityId = hdnTicketSeverityId;
	}
	public Helpdesk_tickethistory() {
	
	}
	@Override
	public String toString() {
		return "Helpdesk_tickethistory [id=" + id + ", HdnTicketId=" + HdnTicketId + ", HdcComments=" + HdcComments
				+ ", HdnTicketStatusId=" + HdnTicketStatusId + ", ModifiedBy=" + ModifiedBy + ", ModifiedOn="
				+ ModifiedOn + ", HdnTicketSeverityId=" + HdnTicketSeverityId + "]";
	}
	
}
