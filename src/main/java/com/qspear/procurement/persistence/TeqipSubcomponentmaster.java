package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the TEQIP_SUBCOMPONENTMASTER database table.
 * 
 */
@Entity
@Table(name="TEQIP_SUBCOMPONENTMASTER")
public class TeqipSubcomponentmaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="COMPONENT_ID")
	private TeqipComponentmaster teqipComponentmaster;

	private Boolean isactive;

	@Column(name="SUB_COMPONENT_NAME")
	private String subComponentName;

	private String subcomponentcode;

	//bi-directional many-to-one association to TeqipInstitutionsubcomponentmapping
	@OneToMany(mappedBy="teqipSubcomponentmaster")
	private List<TeqipInstitutionsubcomponentmapping> teqipInstitutionsubcomponentmappings;

	//bi-directional many-to-one association to TeqipCategorymaster
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private TeqipCategorymaster teqipCategorymaster;

	@Column(name="SUB_CATEGORY_ID")
	private Integer subCategoryId;
	
	//bi-directional many-to-one association to TeqipPlanApprovalstatus
	@OneToMany(mappedBy="teqipSubcomponentmaster")
	private List<TeqipPackage> teqipPackages;

	public TeqipSubcomponentmaster() {
	}

	public Boolean getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public String getSubComponentName() {
		return this.subComponentName;
	}

	public void setSubComponentName(String subComponentName) {
		this.subComponentName = subComponentName;
	}

	public String getSubcomponentcode() {
		return this.subcomponentcode;
	}

	public void setSubcomponentcode(String subcomponentcode) {
		this.subcomponentcode = subcomponentcode;
	}

	public List<TeqipInstitutionsubcomponentmapping> getTeqipInstitutionsubcomponentmappings() {
		return this.teqipInstitutionsubcomponentmappings;
	}

	public void setTeqipInstitutionsubcomponentmappings(List<TeqipInstitutionsubcomponentmapping> teqipInstitutionsubcomponentmappings) {
		this.teqipInstitutionsubcomponentmappings = teqipInstitutionsubcomponentmappings;
	}

	public TeqipInstitutionsubcomponentmapping addTeqipInstitutionsubcomponentmapping(TeqipInstitutionsubcomponentmapping teqipInstitutionsubcomponentmapping) {
		getTeqipInstitutionsubcomponentmappings().add(teqipInstitutionsubcomponentmapping);
		teqipInstitutionsubcomponentmapping.setTeqipSubcomponentmaster(this);

		return teqipInstitutionsubcomponentmapping;
	}

	public TeqipInstitutionsubcomponentmapping removeTeqipInstitutionsubcomponentmapping(TeqipInstitutionsubcomponentmapping teqipInstitutionsubcomponentmapping) {
		getTeqipInstitutionsubcomponentmappings().remove(teqipInstitutionsubcomponentmapping);
		teqipInstitutionsubcomponentmapping.setTeqipSubcomponentmaster(null);

		return teqipInstitutionsubcomponentmapping;
	}

	public TeqipCategorymaster getTeqipCategorymaster() {
		return this.teqipCategorymaster;
	}

	public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
		this.teqipCategorymaster = teqipCategorymaster;
	}

	public List<TeqipPackage> getTeqipPackages() {
		return this.teqipPackages;
	}

	public void setTeqipPackages(List<TeqipPackage> teqipPackages) {
		this.teqipPackages = teqipPackages;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TeqipComponentmaster getTeqipComponentmaster() {
		return teqipComponentmaster;
	}

	public void setTeqipComponentmaster(TeqipComponentmaster teqipComponentmaster) {
		this.teqipComponentmaster = teqipComponentmaster;
	}

	public Integer getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(Integer subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
}