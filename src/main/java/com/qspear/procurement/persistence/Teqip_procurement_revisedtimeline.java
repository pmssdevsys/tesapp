/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "teqip_procurementmethod_revisedtimeline")
public class Teqip_procurement_revisedtimeline implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    @Column(name = "PROCUREMENTMETHOD_REVISEDTIMELINEID")
    private Integer procurementmethodrevisedtimelineId;
    @Column(name = "BidDocumentPreparationDate_days")
    private Date bidDocumentationPreprationDatedays;
    @Column(name = "BankNOCForBiddingDocuments_days")
    private Date bankNOCForBiddingDocumentsdays;
    @Column(name = "BidInvitationDate_days")
    private Date bidInvitationDatedays;
    @Column(name = "BidOpeningDate_days")
    private Date bidOpeningDatedays;
    @Column(name = "TORFinalizationDate_days")
    private Date tORFinalizationDatedays;
    @Column(name = "AdvertisementDate_days")
    private Date advertisementDatedays;
    @Column(name = "FinalDraftToBeForwardedToTheBankDate_days")
    private Date finalDraftToBeForwardedToTheBankDatedays;
    @Column(name = "NoObjectionFromBankForRFP_days")
    private Date noObjectionFromBankForRFPdays;
    @Column(name = "RFPIssuedDate_days")
    private Date rFPIssuedDatedays;
    @Column(name = "LastDateToReceiveProposals_days")
    private Date lastDateToReceiveProposalsdays;
    @Column(name = "AdvertisementDate_WithoutBankNOC_days")
    private Date AdvertisementDate_WithoutBankNOC_days;
    @Column(name = "EvaluationDate_days")
    private Date evaluationDatedays;
    @Column(name = "NoObjectionFromBankForEvaluation_days")
    private Date noObjectionFromBankForEvaluationdays;
    @Column(name = "ContractCompletionDate_days")
    private Date contractCompletionDatedays;
    @Column(name = "ContractAwardDate_days")
    private Date contractAwardDatedays;
    @Column(name = "TORFinalizationDate_WithoutBankNOC_days")
    private Date tORFinalizationDate_WithoutBankNOCdays;
    @Column(name = "RFPIssuedDate_WithoutBankNOC_days")
    private Date rFPIssuedDate_WithoutBankNOCdays;
    @Column(name = "EvaluationDate_WithoutBankNOC_days")
    private Date evaluationDate_WithoutBankNOCdays;
    @Column(name = "ContractAwardDate_WithoutBankNOC_days")
    private Date contractAwardDate_WithoutBankNOCdays;
    @Column(name = "ContractCompletionDate_WithoutBankNOC_days")
    private Date contractCompletionDate_WithoutBankNOC_days;
    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MODIFY_BY")
    private Integer modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "MODIFY_ON")
    private Date modifiedOn;
    @Column(name = "LastdateofsubmissionofEOI")
    private Date lastdateofsubmissionofEOI;
    @Column(name = "ShortlistingOfEOI")
    private Date shortlistingOfEOI;
    @Column(name = "RFPApprovalDate")
    private Date rFPApprovalDate;
    @Column(name = "FinancialEvaluationDate")
    private Date financialEvaluationDate;
    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID")
    @JsonIgnore
    private TeqipCategorymaster teqipCategorymaster;
    @ManyToOne
    @JoinColumn(name = "PROCUREMENTMETHOD_ID")
    @JsonIgnore
    private TeqipProcurementmethod procurementmethod;
    @ManyToOne
    @JoinColumn(name = "packageId")
    @JsonIgnore
    private TeqipPackage teqipPackage;

    private Date proposalInvitationDate;
    private Date proposalSubmissionDate;

    public Date getProposalInvitationDate() {
        return proposalInvitationDate;
    }

    public void setProposalInvitationDate(Date proposalInvitationDate) {
        this.proposalInvitationDate = proposalInvitationDate;
    }

    public Date getProposalSubmissionDate() {
        return proposalSubmissionDate;
    }

    public void setProposalSubmissionDate(Date proposalSubmissionDate) {
        this.proposalSubmissionDate = proposalSubmissionDate;
    }

    public TeqipProcurementmethod getProcurementmethod() {
        return procurementmethod;
    }

    public void setProcurementmethod(TeqipProcurementmethod procurementmethod) {
        this.procurementmethod = procurementmethod;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipCategorymaster getTeqipCategorymaster() {
        return teqipCategorymaster;
    }

    public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
        this.teqipCategorymaster = teqipCategorymaster;
    }

    public Integer getProcurementmethodrevisedtimelineId() {
        return procurementmethodrevisedtimelineId;
    }

    public void setProcurementmethodrevisedtimelineId(Integer procurementmethodrevisedtimelineId) {
        this.procurementmethodrevisedtimelineId = procurementmethodrevisedtimelineId;
    }

    public Date getBidDocumentationPreprationDatedays() {
        return bidDocumentationPreprationDatedays;
    }

    public void setBidDocumentationPreprationDatedays(Date bidDocumentationPreprationDatedays) {
        this.bidDocumentationPreprationDatedays = bidDocumentationPreprationDatedays;
    }

    public Date getBankNOCForBiddingDocumentsdays() {
        return bankNOCForBiddingDocumentsdays;
    }

    public void setBankNOCForBiddingDocumentsdays(Date bankNOCForBiddingDocumentsdays) {
        this.bankNOCForBiddingDocumentsdays = bankNOCForBiddingDocumentsdays;
    }

    public Date getBidInvitationDatedays() {
        return bidInvitationDatedays;
    }

    public void setBidInvitationDatedays(Date bidInvitationDatedays) {
        this.bidInvitationDatedays = bidInvitationDatedays;
    }

    public Date getBidOpeningDatedays() {
        return bidOpeningDatedays;
    }

    public void setBidOpeningDatedays(Date bidOpeningDatedays) {
        this.bidOpeningDatedays = bidOpeningDatedays;
    }

    public Date gettORFinalizationDatedays() {
        return tORFinalizationDatedays;
    }

    public void settORFinalizationDatedays(Date tORFinalizationDatedays) {
        this.tORFinalizationDatedays = tORFinalizationDatedays;
    }

    public Date getAdvertisementDatedays() {
        return advertisementDatedays;
    }

    public void setAdvertisementDatedays(Date advertisementDatedays) {
        this.advertisementDatedays = advertisementDatedays;
    }

    public Date getFinalDraftToBeForwardedToTheBankDatedays() {
        return finalDraftToBeForwardedToTheBankDatedays;
    }

    public void setFinalDraftToBeForwardedToTheBankDatedays(Date finalDraftToBeForwardedToTheBankDatedays) {
        this.finalDraftToBeForwardedToTheBankDatedays = finalDraftToBeForwardedToTheBankDatedays;
    }

    public Date getNoObjectionFromBankForRFPdays() {
        return noObjectionFromBankForRFPdays;
    }

    public void setNoObjectionFromBankForRFPdays(Date noObjectionFromBankForRFPdays) {
        this.noObjectionFromBankForRFPdays = noObjectionFromBankForRFPdays;
    }

    public Date getrFPIssuedDatedays() {
        return rFPIssuedDatedays;
    }

    public void setrFPIssuedDatedays(Date rFPIssuedDatedays) {
        this.rFPIssuedDatedays = rFPIssuedDatedays;
    }

    public Date getLastDateToReceiveProposalsdays() {
        return lastDateToReceiveProposalsdays;
    }

    public void setLastDateToReceiveProposalsdays(Date lastDateToReceiveProposalsdays) {
        this.lastDateToReceiveProposalsdays = lastDateToReceiveProposalsdays;
    }

    public Date getAdvertisementDate_WithoutBankNOC_days() {
        return AdvertisementDate_WithoutBankNOC_days;
    }

    public void setAdvertisementDate_WithoutBankNOC_days(Date advertisementDate_WithoutBankNOC_days) {
        AdvertisementDate_WithoutBankNOC_days = advertisementDate_WithoutBankNOC_days;
    }

    public Date getEvaluationDatedays() {
        return evaluationDatedays;
    }

    public void setEvaluationDatedays(Date evaluationDatedays) {
        this.evaluationDatedays = evaluationDatedays;
    }

    public Date getNoObjectionFromBankForEvaluationdays() {
        return noObjectionFromBankForEvaluationdays;
    }

    public void setNoObjectionFromBankForEvaluationdays(Date noObjectionFromBankForEvaluationdays) {
        this.noObjectionFromBankForEvaluationdays = noObjectionFromBankForEvaluationdays;
    }

    public Date getContractCompletionDatedays() {
        return contractCompletionDatedays;
    }

    public void setContractCompletionDatedays(Date contractCompletionDatedays) {
        this.contractCompletionDatedays = contractCompletionDatedays;
    }

    public Date getContractAwardDatedays() {
        return contractAwardDatedays;
    }

    public void setContractAwardDatedays(Date contractAwardDatedays) {
        this.contractAwardDatedays = contractAwardDatedays;
    }

    public Date gettORFinalizationDate_WithoutBankNOCdays() {
        return tORFinalizationDate_WithoutBankNOCdays;
    }

    public void settORFinalizationDate_WithoutBankNOCdays(Date tORFinalizationDate_WithoutBankNOCdays) {
        this.tORFinalizationDate_WithoutBankNOCdays = tORFinalizationDate_WithoutBankNOCdays;
    }

    public Date getrFPIssuedDate_WithoutBankNOCdays() {
        return rFPIssuedDate_WithoutBankNOCdays;
    }

    public void setrFPIssuedDate_WithoutBankNOCdays(Date rFPIssuedDate_WithoutBankNOCdays) {
        this.rFPIssuedDate_WithoutBankNOCdays = rFPIssuedDate_WithoutBankNOCdays;
    }

    public Date getEvaluationDate_WithoutBankNOCdays() {
        return evaluationDate_WithoutBankNOCdays;
    }

    public void setEvaluationDate_WithoutBankNOCdays(Date evaluationDate_WithoutBankNOCdays) {
        this.evaluationDate_WithoutBankNOCdays = evaluationDate_WithoutBankNOCdays;
    }

    public Date getContractAwardDate_WithoutBankNOCdays() {
        return contractAwardDate_WithoutBankNOCdays;
    }

    public void setContractAwardDate_WithoutBankNOCdays(Date contractAwardDate_WithoutBankNOCdays) {
        this.contractAwardDate_WithoutBankNOCdays = contractAwardDate_WithoutBankNOCdays;
    }

    public Date getContractCompletionDate_WithoutBankNOC_days() {
        return contractCompletionDate_WithoutBankNOC_days;
    }

    public void setContractCompletionDate_WithoutBankNOC_days(Date contractAwardDate_WithoutBankNOCdays) {
        this.contractCompletionDate_WithoutBankNOC_days = contractCompletionDate_WithoutBankNOC_days;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Date getLastdateofsubmissionofEOI() {
        return lastdateofsubmissionofEOI;
    }

    public void setLastdateofsubmissionofEOI(Date lastdateofsubmissionofEOI) {
        this.lastdateofsubmissionofEOI = lastdateofsubmissionofEOI;
    }

    public Date getShortlistingOfEOI() {
        return shortlistingOfEOI;
    }

    public void setShortlistingOfEOI(Date shortlistingOfEOI) {
        this.shortlistingOfEOI = shortlistingOfEOI;
    }

    public Date getrFPApprovalDate() {
        return rFPApprovalDate;
    }

    public void setrFPApprovalDate(Date rFPApprovalDate) {
        this.rFPApprovalDate = rFPApprovalDate;
    }

    public Date getFinancialEvaluationDate() {
        return financialEvaluationDate;
    }

    public void setFinancialEvaluationDate(Date financialEvaluationDate) {
        this.financialEvaluationDate = financialEvaluationDate;
    }

}
