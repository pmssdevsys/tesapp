package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the TEQIP_PROCUREMENTALLOCATION database table.
 * 
 */
@Entity
@Table(name="TEQIP_PROCUREMENTALLOCATION")
public class TeqipProcurementallocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="PROCUREMENTALLOCATION_ID")
	private Integer procurementallocationId;

	@Column(name="BUDGET_ID")
	private Integer budgetId;

	@Column(name="CREATED_ON")
	private Timestamp createdOn;

	@Column(name="GOODS_PERCENTAGE")
	private Double goodsPercentage;

	@Column(name="OTHER_PERCENTAGE")
	private Double otherPercentage;

	@Column(name="PLAN_AMOUNT")
	private Double planAmount;

	@Column(name="SERVICE_PERCENTAGE")
	private Double servicePercentage;

	@Column(name="WORKS_PERCENTAGE")
	private Double worksPercentage;

	//bi-directional many-to-one association to TeqipProcurementmaster
	@ManyToOne
	@JoinColumn(name="PROCUREMENT_PLAN_ID")
	private TeqipProcurementmaster teqipProcurementmaster;

	public TeqipProcurementallocation() {
	}

	public Integer getProcurementallocationId() {
		return this.procurementallocationId;
	}

	public void setProcurementallocationId(Integer procurementallocationId) {
		this.procurementallocationId = procurementallocationId;
	}

	public Integer getBudgetId() {
		return this.budgetId;
	}

	public void setBudgetId(Integer budgetId) {
		this.budgetId = budgetId;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Double getGoodsPercentage() {
		return this.goodsPercentage;
	}

	public void setGoodsPercentage(Double goodsPercentage) {
		this.goodsPercentage = goodsPercentage;
	}

	public Double getOtherPercentage() {
		return this.otherPercentage;
	}

	public void setOtherPercentage(Double otherPercentage) {
		this.otherPercentage = otherPercentage;
	}

	public Double getPlanAmount() {
		return this.planAmount;
	}

	public void setPlanAmount(Double planAmount) {
		this.planAmount = planAmount;
	}

	public Double getServicePercentage() {
		return this.servicePercentage;
	}

	public void setServicePercentage(Double servicePercentage) {
		this.servicePercentage = servicePercentage;
	}

	public Double getWorksPercentage() {
		return this.worksPercentage;
	}

	public void setWorksPercentage(Double worksPercentage) {
		this.worksPercentage = worksPercentage;
	}

	public TeqipProcurementmaster getTeqipProcurementmaster() {
		return this.teqipProcurementmaster;
	}

	public void setTeqipProcurementmaster(TeqipProcurementmaster teqipProcurementmaster) {
		this.teqipProcurementmaster = teqipProcurementmaster;
	}

}