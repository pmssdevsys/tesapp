package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the teqip_institutionsubcomponentmapping database table.
 * 
 */
@Entity
@Table(name="teqip_institutionsubcomponentmapping")
@NamedQuery(name="TeqipInstitutionsubcomponentmapping.findAll", query="SELECT t FROM TeqipInstitutionsubcomponentmapping t")
public class TeqipInstitutionsubcomponentmapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "INSTITUTIONSUBCOMPONENTMAPPING_ID")
	private Integer institutionSubcomponentMapping_ID;

	private Double allocatedbudget;

	private Double budgetforcw;

	private Double budgetforprocuremen;

	private Double budgetforservices;
        
        private Integer type;

	//bi-directional many-to-one association to TeqipInstitution
	@ManyToOne
	@JoinColumn(name="INSTITUTION_ID")
	private TeqipInstitution teqipInstitution;

	//bi-directional many-to-one association to TeqipSubcomponentmaster
	@ManyToOne
	@JoinColumn(name="SUBCOMPONENT_ID")
	private TeqipSubcomponentmaster teqipSubcomponentmaster;
	
	//bi-directional many-to-one association to TeqipStatemaster
	@ManyToOne
	@JoinColumn(name="SPFUID")
	private TeqipStatemaster teqipStatemaster;
	
	@Column(name="ALLOCATEDTYPE")
	private String allocatedtype;
	
	@Column(name="ISACTIVE")
	private Boolean isactive;
	
	@Column(name="MODIFY_BY")
	private Integer modifyBy;

	@Temporal(TemporalType.DATE)
	@Column(name="MODIFY_ON")
	private Date modifyOn;
	
	@Column(name="CREATED_ON")
	private Timestamp createdOn;
	
	@Column(name="CREATED_BY")
	private Integer createdBy;

	public TeqipInstitutionsubcomponentmapping() {
	}

	public Integer getInstitutionSubcomponentMapping_ID() {
		return this.institutionSubcomponentMapping_ID;
	}

	public void setInstitutionSubcomponentMapping_ID(Integer institutionSubcomponentMapping_ID) {
		this.institutionSubcomponentMapping_ID = institutionSubcomponentMapping_ID;
	}

	public Double getAllocatedbudget() {
		return this.allocatedbudget;
	}

	public void setAllocatedbudget(Double allocatedbudget) {
		this.allocatedbudget = allocatedbudget;
	}

	public Double getBudgetforcw() {
		return this.budgetforcw;
	}

	public void setBudgetforcw(Double budgetforcw) {
		this.budgetforcw = budgetforcw;
	}

	public Double getBudgetforprocuremen() {
		return this.budgetforprocuremen;
	}

	public void setBudgetforprocuremen(Double budgetforprocuremen) {
		this.budgetforprocuremen = budgetforprocuremen;
	}

	public Double getBudgetforservices() {
		return this.budgetforservices;
	}

	public void setBudgetforservices(Double budgetforservices) {
		this.budgetforservices = budgetforservices;
	}

	public TeqipInstitution getTeqipInstitution() {
		return this.teqipInstitution;
	}

	public void setTeqipInstitution(TeqipInstitution teqipInstitution) {
		this.teqipInstitution = teqipInstitution;
	}

	public TeqipSubcomponentmaster getTeqipSubcomponentmaster() {
		return this.teqipSubcomponentmaster;
	}

	public void setTeqipSubcomponentmaster(TeqipSubcomponentmaster teqipSubcomponentmaster) {
		this.teqipSubcomponentmaster = teqipSubcomponentmaster;
	}

	public TeqipStatemaster getTeqipStatemaster() {
		return teqipStatemaster;
	}

	public void setTeqipStatemaster(TeqipStatemaster teqipStatemaster) {
		this.teqipStatemaster = teqipStatemaster;
	}

	public String getAllocatedtype() {
		return allocatedtype;
	}

	public void setAllocatedtype(String allocatedtype) {
		this.allocatedtype = allocatedtype;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Integer getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(Integer modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyOn() {
		return modifyOn;
	}

	public void setModifyOn(Date modifyOn) {
		this.modifyOn = modifyOn;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
	
}