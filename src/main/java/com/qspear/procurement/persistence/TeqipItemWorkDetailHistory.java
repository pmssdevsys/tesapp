package com.qspear.procurement.persistence;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * The persistent class for the TEQIP_ITEM_WORK_DETAIL database table.
 *
 */
@Entity
@Table(name = "TEQIP_ITEM_WORK_DETAIL_rev_HISTORY")
public class TeqipItemWorkDetailHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "REVISEID")
    private Integer reviseId;

    private Boolean status;

    @Column(name = "CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "MODIFY_BY")
    private Integer modifyBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "MODIFY_ON")
    private Date modifyOn;

    @Column(name = "WORK_COST")
    private Double workCost;

    @Column(name = "WORK_NAME")
    private String workName;

    @Column(name = "WORK_SPECIFICATION")
    private String workSpecification;

    //bi-directional many-to-one association to TeqipPackage
    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "PACKAGE_ID")
    private TeqipPackage teqipPackage;

    @ManyToOne
    @JoinColumn(name = "PACKAGE_HISTORY_ID")
    private TeqipPackageHistory teqipPackageHistory;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "ITEM_WORK_DETAIL_ID")
    private TeqipItemWorkDetail teqipItemWorkDetail;

    //bi-directional many-to-one association to TeqipCategorymaster
    @ManyToOne
    @JoinColumn(name = "WORK_CATEGORY_ID")
    private TeqipCategorymaster teqipCategorymaster;

    @ManyToOne
    @JoinColumn(name = "ITEM_DEPARTMENT_ID")
    private TeqipPmssDepartmentMaster teqipPmssDepartmentMaster;

    @Column(name = "deliveyperiod")
    private Integer deliveyPeriod;

    @Column(name = "trainingrequired")
    private Integer trainingRequired;

    @Column(name = "installationrequired")
    private Integer installationRequired;

    @Column(name = "placeofdelivery")
    private String placeOfDelivery;

    @Column(name = "instllationrequirement")
    private String instllationRequirement;

    public TeqipItemWorkDetailHistory() {
    }

    public TeqipItemWorkDetailHistory(TeqipPackageHistory teqipPackageHistory, TeqipItemWorkDetail teqipItemWorkDetail) {
        this.teqipPackageHistory = teqipPackageHistory;
        this.teqipPackage = teqipPackageHistory.getTeqipPackage();
        this.teqipItemWorkDetail = teqipItemWorkDetail;

        this.createdBy = teqipItemWorkDetail.getCreatedBy();
        this.reviseId = teqipItemWorkDetail.getReviseId();
        this.status = teqipItemWorkDetail.getStatus();
        this.createdOn = teqipItemWorkDetail.getCreatedOn();
        this.modifyBy = teqipItemWorkDetail.getModifyBy();
        this.modifyOn = teqipItemWorkDetail.getModifyOn();
        this.workCost = teqipItemWorkDetail.getWorkCost();
        this.workName = teqipItemWorkDetail.getWorkName();
        this.workSpecification = teqipItemWorkDetail.getWorkSpecification();
        this.teqipCategorymaster = teqipItemWorkDetail.getTeqipCategorymaster();
        this.teqipPmssDepartmentMaster = teqipItemWorkDetail.getTeqipPmssDepartmentMaster();
        this.deliveyPeriod = teqipItemWorkDetail.getDeliveyPeriod();
        this.trainingRequired = teqipItemWorkDetail.getTrainingRequired();
        this.installationRequired = teqipItemWorkDetail.getInstallationRequired();
        this.placeOfDelivery = teqipItemWorkDetail.getPlaceOfDelivery();
        this.instllationRequirement = teqipItemWorkDetail.getInstllationRequirement();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getReviseId() {
        return reviseId;
    }

    public void setReviseId(Integer reviseId) {
        this.reviseId = reviseId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Date modifyOn) {
        this.modifyOn = modifyOn;
    }

    public Double getWorkCost() {
        return workCost;
    }

    public void setWorkCost(Double workCost) {
        this.workCost = workCost;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getWorkSpecification() {
        return workSpecification;
    }

    public void setWorkSpecification(String workSpecification) {
        this.workSpecification = workSpecification;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPackageHistory getTeqipPackageHistory() {
        return teqipPackageHistory;
    }

    public void setTeqipPackageHistory(TeqipPackageHistory teqipPackageHistory) {
        this.teqipPackageHistory = teqipPackageHistory;
    }

    public TeqipItemWorkDetail getTeqipItemWorkDetail() {
        return teqipItemWorkDetail;
    }

    public void setTeqipItemWorkDetail(TeqipItemWorkDetail teqipItemWorkDetail) {
        this.teqipItemWorkDetail = teqipItemWorkDetail;
    }

    public TeqipCategorymaster getTeqipCategorymaster() {
        return teqipCategorymaster;
    }

    public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
        this.teqipCategorymaster = teqipCategorymaster;
    }

    public TeqipPmssDepartmentMaster getTeqipPmssDepartmentMaster() {
        return teqipPmssDepartmentMaster;
    }

    public void setTeqipPmssDepartmentMaster(TeqipPmssDepartmentMaster teqipPmssDepartmentMaster) {
        this.teqipPmssDepartmentMaster = teqipPmssDepartmentMaster;
    }

    public Integer getDeliveyPeriod() {
        return deliveyPeriod;
    }

    public void setDeliveyPeriod(Integer deliveyPeriod) {
        this.deliveyPeriod = deliveyPeriod;
    }

    public Integer getTrainingRequired() {
        return trainingRequired;
    }

    public void setTrainingRequired(Integer trainingRequired) {
        this.trainingRequired = trainingRequired;
    }

    public Integer getInstallationRequired() {
        return installationRequired;
    }

    public void setInstallationRequired(Integer installationRequired) {
        this.installationRequired = installationRequired;
    }

    public String getPlaceOfDelivery() {
        return placeOfDelivery;
    }

    public void setPlaceOfDelivery(String placeOfDelivery) {
        this.placeOfDelivery = placeOfDelivery;
    }

    public String getInstllationRequirement() {
        return instllationRequirement;
    }

    public void setInstllationRequirement(String instllationRequirement) {
        this.instllationRequirement = instllationRequirement;
    }

}
