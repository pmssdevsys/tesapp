package com.qspear.procurement.persistence;

import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the teqip_institutiondepartmentmapping database
 * table.
 *
 */
@Entity
@Table(name = "teqip_institutiondepartmentmapping")
@NamedQuery(name = "TeqipInstitutiondepartmentmapping.findAll", query = "SELECT t FROM TeqipInstitutiondepartmentmapping t")
public class TeqipInstitutiondepartmentmapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "INSTITUTIONDEPARTMENTMAPPING_ID")
    private Integer institutionDepartmentMapping_ID;

    @ManyToOne
    @JoinColumn(name = "DEPARTMENT_ID")
    private TeqipPmssDepartmentMaster teqipPmssdepartmentmaster;

    @Column(name = "DEPARTMENTHEAD")
    private String departmentHead;

    @ManyToOne
    @JoinColumn(name = "SPFUID")
    private TeqipStatemaster teqipStatemaster;

   
    
    public TeqipStatemaster getTeqipStatemaster() {
		return teqipStatemaster;
	}

	public void setTeqipStatemaster(TeqipStatemaster teqipStatemaster) {
		this.teqipStatemaster = teqipStatemaster;
	}

	//bi-directional many-to-one association to TeqipInstitution
    @ManyToOne
    @JoinColumn(name = "INSTITUTION_ID")
    private TeqipInstitution teqipInstitution;

    @Column(name = "ISACTIVE")
    private Boolean isactive;

    @Column(name = "MODIFY_BY")
    private Integer modifyBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "MODIFY_ON")
    private Date modifyOn;

    @Column(name = "CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "DEPARTMENTCODE")
    private String departmentcode;
	@Column(name="type")
	private Integer type;
	
	
    public Integer getTypeNpcEdcil() {
		return type;
	}

	public void setTypeNpcEdcil(Integer typeNpcEdcil) {
		this.type = typeNpcEdcil;
	}

	public String getDepartmentcode() {
        return departmentcode;
    }

    public void setDepartmentcode(String departmentcode) {
        this.departmentcode = departmentcode;
    }

    public TeqipInstitutiondepartmentmapping() {
    }

    public Integer getInstitutionDepartmentMapping_ID() {
        return this.institutionDepartmentMapping_ID;
    }

    public void setInstitutionDepartmentMapping_ID(Integer institutionDepartmentMapping_ID) {
        this.institutionDepartmentMapping_ID = institutionDepartmentMapping_ID;
    }

    public TeqipInstitution getTeqipInstitution() {
        return this.teqipInstitution;
    }

    public void setTeqipInstitution(TeqipInstitution teqipInstitution) {
        this.teqipInstitution = teqipInstitution;
    }

    public TeqipPmssDepartmentMaster getTeqipPmssdepartmentmaster() {
        return teqipPmssdepartmentmaster;
    }

    public void setTeqipPmssdepartmentmaster(TeqipPmssDepartmentMaster teqipPmssdepartmentmaster) {
        this.teqipPmssdepartmentmaster = teqipPmssdepartmentmaster;
    }

    public String getDepartmentHead() {
        return departmentHead;
    }

    public void setDepartmentHead(String departmentHead) {
        this.departmentHead = departmentHead;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Date modifyOn) {
        this.modifyOn = modifyOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }


}
