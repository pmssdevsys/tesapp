package com.qspear.procurement.persistence;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the TEQIP_SUBCATEGORYMASTER database table.
 * 
 */
@Entity
@Table(name="TEQIP_SUBCATEGORYMASTER")
public class TeqipSubcategorymaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="SUBCAT_ID")
	private Integer subcatId;

	private Boolean isactive;

	@Column(name="SUB_CATEGORY_NAME")
	private String subCategoryName;

	//bi-directional many-to-one association to TeqipCategorymaster
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="CATEGORY_ID")
	private TeqipCategorymaster teqipCategorymaster;

	public TeqipSubcategorymaster() {
	}

	public Integer getSubcatId() {
		return this.subcatId;
	}

	public void setSubcatId(Integer subcatId) {
		this.subcatId = subcatId;
	}

	public Boolean getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public String getSubCategoryName() {
		return this.subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
        
	public TeqipCategorymaster getTeqipCategorymaster() {
		return this.teqipCategorymaster;
	}

	public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
		this.teqipCategorymaster = teqipCategorymaster;
	}
}