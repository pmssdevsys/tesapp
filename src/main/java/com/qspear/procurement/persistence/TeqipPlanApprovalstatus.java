package com.qspear.procurement.persistence;

import com.qspear.procurement.persistence.methods.AuditEntity;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the teqip_plan_approvalstatus database table.
 *
 */
@Entity
@Table(name = "teqip_plan_approvalstatus")
@NamedQuery(name = "TeqipPlanApprovalstatus.findAll", query = "SELECT t FROM TeqipPlanApprovalstatus t")
public class TeqipPlanApprovalstatus extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    //bi-directional many-to-one association to TeqipProcurementmaster
    @ManyToOne
    @JoinColumn(name = "PROCUREMENT_PLAN_ID")
    private TeqipProcurementmaster teqipProcurementmaster;

    //bi-directional many-to-one association to TeqipInstitution
    @ManyToOne
    @JoinColumn(name = "INSTITUTION_ID")
    private TeqipInstitution teqipInstitution;

    //bi-directional many-to-one association to TeqipStatemaster
    @ManyToOne
    @JoinColumn(name = "SPFUID")
    private TeqipStatemaster teqipStatemaster;
    
    @ManyToOne
    @JoinColumn(name = "PROCUREMENTSTAGEID_PREV")
    private TeqipPmssProcurementstage teqipPmssProcurementstage_prev;

    //bi-directional many-to-one association to TeqipPmssProcurementstage
    @ManyToOne
    @JoinColumn(name = "PROCUREMENTSTAGEID")
    private TeqipPmssProcurementstage teqipPmssProcurementstage;

    @Column(name = "STATUSCOMMENTS")
    private String statusComment;

    private Integer reviseid;

    private Integer currentStage;

    @Column(name = "CREATED_TYPE")
    private Integer type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TeqipProcurementmaster getTeqipProcurementmaster() {
        return teqipProcurementmaster;
    }

    public void setTeqipProcurementmaster(TeqipProcurementmaster teqipProcurementmaster) {
        this.teqipProcurementmaster = teqipProcurementmaster;
    }

    public TeqipInstitution getTeqipInstitution() {
        return teqipInstitution;
    }

    public void setTeqipInstitution(TeqipInstitution teqipInstitution) {
        this.teqipInstitution = teqipInstitution;
    }

    public TeqipStatemaster getTeqipStatemaster() {
        return teqipStatemaster;
    }

    public void setTeqipStatemaster(TeqipStatemaster teqipStatemaster) {
        this.teqipStatemaster = teqipStatemaster;
    }

 

    public TeqipPmssProcurementstage getTeqipPmssProcurementstage() {
        return teqipPmssProcurementstage;
    }

    public void setTeqipPmssProcurementstage(TeqipPmssProcurementstage teqipPmssProcurementstage) {
        this.teqipPmssProcurementstage = teqipPmssProcurementstage;
    }

    public String getStatusComment() {
        return statusComment;
    }

    public void setStatusComment(String statusComment) {
        this.statusComment = statusComment;
    }

    public Integer getReviseid() {
        return reviseid;
    }

    public void setReviseid(Integer reviseid) {
        this.reviseid = reviseid;
    }

    @Override
    public TeqipPlanApprovalstatus clone() {
        TeqipPlanApprovalstatus approvalStatus = new TeqipPlanApprovalstatus();

        approvalStatus.setTeqipProcurementmaster(this.teqipProcurementmaster);
        approvalStatus.setTeqipInstitution(this.teqipInstitution);
        approvalStatus.setTeqipStatemaster(this.teqipStatemaster);
        approvalStatus.setType(this.type);
        approvalStatus.setTeqipPmssProcurementstage_prev(this.teqipPmssProcurementstage);
        approvalStatus.setTeqipPmssProcurementstage(this.teqipPmssProcurementstage);
        approvalStatus.setReviseid(this.reviseid);

        return approvalStatus;
    }

    public Integer getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(Integer currentStage) {
        this.currentStage = currentStage;
    }

    public TeqipPmssProcurementstage getTeqipPmssProcurementstage_prev() {
        return teqipPmssProcurementstage_prev;
    }

    public void setTeqipPmssProcurementstage_prev(TeqipPmssProcurementstage teqipPmssProcurementstage_prev) {
        this.teqipPmssProcurementstage_prev = teqipPmssProcurementstage_prev;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
