package com.qspear.procurement.persistence;

import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * The persistent class for the TEQIP_ITEM_GOODS_DETAIL database table.
 *
 */
@Entity
@Table(name = "TEQIP_ITEM_GOODS_DETAIL_rev_HISTORY")
public class TeqipItemGoodsDetailHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "REVISEID")
    private Integer reviseId;

    private Boolean status;

    @Column(name = "CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "ITEM_COST_UNIT")
    private Double itemCostUnit;

    @Column(name = "ITEM_QNT")
    private Double itemQnt;

    @Column(name = "ITEM_SPECIFICATION")
    private String itemSpecification;

    @Column(name = "MODIFY_BY")
    private Integer modifyBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "MODIFY_ON")
    private Date modifyOn;

    @Column(name = "deliveyperiod")
    private Integer deliveyPeriod;

    @Column(name = "itemmainspecification")
    private String itemMainSpecification;

    @Column(name = "trainingrequired")
    private Integer trainingRequired;

    @Column(name = "installationrequired")
    private Integer installationRequired;

    @Column(name = "placeofdelivery")
    private String placeOfDelivery;

    @Column(name = "instllationrequirement")
    private String instllationRequirement;

    //bi-directional many-to-one association to TeqipItemMaster
    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private TeqipItemMaster teqipItemMaster;

    //bi-directional many-to-one association to TeqipPackage
    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "PACKAGE_ID")
    private TeqipPackage teqipPackage;

    @ManyToOne
    @JoinColumn(name = "PACKAGE_HISTORY_ID")
    private TeqipPackageHistory teqipPackageHistory;

    @ManyToOne
    @JoinColumn(name = "ITEM_GOOD_DETAIL_ID")
    @NotFound(action = NotFoundAction.IGNORE)
    private TeqipItemGoodsDetail teqipItemGoodsDetail;

    @ManyToOne
    @JoinColumn(name = "ITEM_CATEGORY_ID")
    private TeqipCategorymaster teqipCategorymaster;

    @ManyToOne
    @JoinColumn(name = "ITEM_DEPARTMENT_ID")
    private TeqipPmssDepartmentMaster teqipPmssDepartmentMaster;

    public TeqipItemGoodsDetailHistory() {
    }

    public TeqipItemGoodsDetailHistory(TeqipPackageHistory packageHistory, TeqipItemGoodsDetail teqipItemGoodsDetail) {
        this.teqipPackageHistory = packageHistory;
        this.teqipPackage = packageHistory.getTeqipPackage();
        this.teqipItemGoodsDetail = teqipItemGoodsDetail;
        this.createdBy = teqipItemGoodsDetail.getCreatedBy();
        this.reviseId = teqipItemGoodsDetail.getReviseId();
        this.status = teqipItemGoodsDetail.getStatus();
        this.createdOn = teqipItemGoodsDetail.getCreatedOn();
        this.itemCostUnit = teqipItemGoodsDetail.getItemCostUnit();
        this.itemQnt = teqipItemGoodsDetail.getItemQnt();
        this.itemSpecification = teqipItemGoodsDetail.getItemSpecification();
        this.modifyBy = teqipItemGoodsDetail.getModifyBy();
        this.modifyOn = teqipItemGoodsDetail.getModifyOn();
        this.deliveyPeriod = teqipItemGoodsDetail.getDeliveyPeriod();
        this.itemMainSpecification = teqipItemGoodsDetail.getItemMainSpecification();
        this.trainingRequired = teqipItemGoodsDetail.getTrainingRequired();
        this.installationRequired = teqipItemGoodsDetail.getInstallationRequired();
        this.placeOfDelivery = teqipItemGoodsDetail.getPlaceOfDelivery();
        this.instllationRequirement = teqipItemGoodsDetail.getInstllationRequirement();
        this.teqipItemMaster = teqipItemGoodsDetail.getTeqipItemMaster();
        this.teqipCategorymaster = teqipItemGoodsDetail.getTeqipCategorymaster();
        this.teqipPmssDepartmentMaster = teqipItemGoodsDetail.getTeqipPmssDepartmentMaster();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getReviseId() {
        return reviseId;
    }

    public void setReviseId(Integer reviseId) {
        this.reviseId = reviseId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Double getItemCostUnit() {
        return itemCostUnit;
    }

    public void setItemCostUnit(Double itemCostUnit) {
        this.itemCostUnit = itemCostUnit;
    }

    public Double getItemQnt() {
        return itemQnt;
    }

    public void setItemQnt(Double itemQnt) {
        this.itemQnt = itemQnt;
    }

    public String getItemSpecification() {
        return itemSpecification;
    }

    public void setItemSpecification(String itemSpecification) {
        this.itemSpecification = itemSpecification;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Date modifyOn) {
        this.modifyOn = modifyOn;
    }

    public Integer getDeliveyPeriod() {
        return deliveyPeriod;
    }

    public void setDeliveyPeriod(Integer deliveyPeriod) {
        this.deliveyPeriod = deliveyPeriod;
    }

    public String getItemMainSpecification() {
        return itemMainSpecification;
    }

    public void setItemMainSpecification(String itemMainSpecification) {
        this.itemMainSpecification = itemMainSpecification;
    }

    public Integer getTrainingRequired() {
        return trainingRequired;
    }

    public void setTrainingRequired(Integer trainingRequired) {
        this.trainingRequired = trainingRequired;
    }

    public Integer getInstallationRequired() {
        return installationRequired;
    }

    public void setInstallationRequired(Integer installationRequired) {
        this.installationRequired = installationRequired;
    }

    public String getPlaceOfDelivery() {
        return placeOfDelivery;
    }

    public void setPlaceOfDelivery(String placeOfDelivery) {
        this.placeOfDelivery = placeOfDelivery;
    }

    public String getInstllationRequirement() {
        return instllationRequirement;
    }

    public void setInstllationRequirement(String instllationRequirement) {
        this.instllationRequirement = instllationRequirement;
    }

    public TeqipItemMaster getTeqipItemMaster() {
        return teqipItemMaster;
    }

    public void setTeqipItemMaster(TeqipItemMaster teqipItemMaster) {
        this.teqipItemMaster = teqipItemMaster;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPackageHistory getTeqipPackageHistory() {
        return teqipPackageHistory;
    }

    public void setTeqipPackageHistory(TeqipPackageHistory teqipPackageHistory) {
        this.teqipPackageHistory = teqipPackageHistory;
    }

    public TeqipItemGoodsDetail getTeqipItemGoodsDetail() {
        return teqipItemGoodsDetail;
    }

    public void setTeqipItemGoodsDetail(TeqipItemGoodsDetail teqipItemGoodsDetail) {
        this.teqipItemGoodsDetail = teqipItemGoodsDetail;
    }

    public TeqipCategorymaster getTeqipCategorymaster() {
        return teqipCategorymaster;
    }

    public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
        this.teqipCategorymaster = teqipCategorymaster;
    }

    public TeqipPmssDepartmentMaster getTeqipPmssDepartmentMaster() {
        return teqipPmssDepartmentMaster;
    }

    public void setTeqipPmssDepartmentMaster(TeqipPmssDepartmentMaster teqipPmssDepartmentMaster) {
        this.teqipPmssDepartmentMaster = teqipPmssDepartmentMaster;
    }

}
