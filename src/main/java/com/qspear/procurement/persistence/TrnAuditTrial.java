package com.qspear.procurement.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="teqip_users_audit_trial")
public class TrnAuditTrial {
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
@Column(name="id")
private Integer id;
@Column(name="username")
private String username;
@Column(name="ip_address")
private String ipAddress;
@Column(name="process_id")
private Integer processId;
@Column(name="session")
private String sessionId;
@Column(name="Description")
private String Description;
@Column(name="URL")
private String url;
@Column(name="user_agent")
private String userAgent;
@UpdateTimestamp
@Temporal(TemporalType.TIMESTAMP)
@Column(name="updateTime")
private Date updateTime;
@UpdateTimestamp
@Temporal(TemporalType.TIMESTAMP)
@Column(name="logoutTime")
private Date logoutTime;
@Column(name="logoutstatus")
private Integer logoutstatus;
public Date getLogoutTime() {
	return logoutTime;
}
public void setLogoutTime(Date logoutTime) {
	this.logoutTime = logoutTime;
}
public Integer getLogoutstatus() {
	return logoutstatus;
}
public void setLogoutstatus(Integer logoutstatus) {
	this.logoutstatus = logoutstatus;
}
public Integer getId() {
return id;
}
public void setId(Integer id) {
this.id = id;
}
public String getUsername() {
return username;
}
public void setUsername(String username) {
this.username = username;
}
public String getIpAddress() {
return ipAddress;
}
public void setIpAddress(String ipAddress) {
this.ipAddress = ipAddress;
}
public Integer getProcessId() {
return processId;
}
public void setProcessId(Integer processId) {
this.processId = processId;
}

public String getSessionId() {
return sessionId;
}
public void setSessionId(String sessionId) {
this.sessionId = sessionId;
}
public String getDescription() {
return Description;
}
public void setDescription(String description) {
Description = description;
}
public String getUrl() {
return url;
}
public void setUrl(String url) {
this.url = url;
}
public String getUserAgent() {
return userAgent;
}
public void setUserAgent(String userAgent) {
this.userAgent = userAgent;
}
public Date getUpdateTime() {
return updateTime;
}
public void setUpdateTime(Date updateTime) {
this.updateTime = updateTime;
}


}