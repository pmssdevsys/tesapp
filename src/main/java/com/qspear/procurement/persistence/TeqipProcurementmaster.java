package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TEQIP_PROCUREMENTMASTER database table.
 * 
 */
@Entity
@Table(name="TEQIP_PROCUREMENTMASTER")
public class TeqipProcurementmaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="PROCUREMENTMASTER_ID")
	private Integer procurementmasterId;

	@Column(name="CREATED_ON")
	private Timestamp createdOn;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATE")
	private Date endDate;

	private Boolean isactive;

	@Column(name="PLAN_AMOUNT")
	private Double planAmount;

	@Column(name="PLAN_NAME")
	private String planName;

	@Column(name="SHORT_NAME")
	private String shortName;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATE")
	private Date startDate;

	//bi-directional many-to-one association to TeqipPackage
	@OneToMany(mappedBy="teqipProcurementmaster", cascade=CascadeType.ALL)
	private List<TeqipPackage> teqipPackages;

	//bi-directional many-to-one association to TeqipProcurementallocation
	@OneToMany(mappedBy="teqipProcurementmaster", cascade=CascadeType.ALL)
	private List<TeqipProcurementallocation> teqipProcurementallocations;

	public TeqipProcurementmaster() {
	}

	public Integer getProcurementmasterId() {
		return this.procurementmasterId;
	}

	public void setProcurementmasterId(Integer procurementmasterId) {
		this.procurementmasterId = procurementmasterId;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Double getPlanAmount() {
		return this.planAmount;
	}

	public void setPlanAmount(Double planAmount) {
		this.planAmount = planAmount;
	}

	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public List<TeqipPackage> getTeqipPackages() {
		return this.teqipPackages;
	}

	public void setTeqipPackages(List<TeqipPackage> teqipPackages) {
		this.teqipPackages = teqipPackages;
	}

	public TeqipPackage addTeqipPackage(TeqipPackage teqipPackage) {
		getTeqipPackages().add(teqipPackage);
		teqipPackage.setTeqipProcurementmaster(this);

		return teqipPackage;
	}

	public TeqipPackage removeTeqipPackage(TeqipPackage teqipPackage) {
		getTeqipPackages().remove(teqipPackage);
		teqipPackage.setTeqipProcurementmaster(null);

		return teqipPackage;
	}


	public List<TeqipProcurementallocation> getTeqipProcurementallocations() {
		return this.teqipProcurementallocations;
	}

	public void setTeqipProcurementallocations(List<TeqipProcurementallocation> teqipProcurementallocations) {
		this.teqipProcurementallocations = teqipProcurementallocations;
	}

	public TeqipProcurementallocation addTeqipProcurementallocation(TeqipProcurementallocation teqipProcurementallocation) {
		getTeqipProcurementallocations().add(teqipProcurementallocation);
		teqipProcurementallocation.setTeqipProcurementmaster(this);

		return teqipProcurementallocation;
	}

	public TeqipProcurementallocation removeTeqipProcurementallocation(TeqipProcurementallocation teqipProcurementallocation) {
		getTeqipProcurementallocations().remove(teqipProcurementallocation);
		teqipProcurementallocation.setTeqipProcurementmaster(null);

		return teqipProcurementallocation;
	}

}