package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "teqip_users_detail")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userid;

    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "CREATED_ON")
    private Timestamp createdOn;

    private Double ctc;

    @Column(name = "DEPT_ID")
    private Integer deptId;

    @Temporal(TemporalType.DATE)
    private Date dob;

    private String email;

    @Column(name = "EMPLOYEE_CODE")
    private String employeeCode;

    private String gender;

    private Boolean isactive;

    @Temporal(TemporalType.DATE)
    @Column(name = "JOINING_DATE")
    private Date joiningDate;

    @Column(name = "FName")
    private String fname;
    @Column(name = "MiddleName")
    private String middleName;
    @Column(name = "LNAME")
    private String lname;

    @Column(name = "MODIFIED_BY")
    private Integer modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "MODIFIED_ON")
    private Date modifiedOn;

    private String name;

    private String password;

    @Column(name = "SPOUSE_NAME")
    private String spouseName;

    @Column(name = "USER_MOBILE")
    private String userMobile;

    @Column(name = "USER_NAME")
    private String username;
    @Column(name="isBlocked")
    private Boolean isBlocked;
    private Timestamp blockedAt;

    public Boolean getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}


    public Timestamp getBlockedAt() {
		return blockedAt;
	}

	public void setBlockedAt(Timestamp blockedAt) {
		this.blockedAt = blockedAt;
	}


	//bi-directional many-to-one association to TeqipRolemaster
    @ManyToOne
    @JoinColumn(name = "ROLE_ID")
    private TeqipRolemaster teqipRolemaster;

    //bi-directional many-to-one association to TeqipUsersMaster
    @OneToMany(mappedBy = "teqipUsersDetail")
    private List<TeqipUsersMaster> teqipUsersMasters;

    public User() {
    }

    public Integer getUserid() {
        return this.userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Double getCtc() {
        return this.ctc;
    }

    public void setCtc(Double ctc) {
        this.ctc = ctc;
    }

    public Integer getDeptId() {
        return this.deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployeeCode() {
        return this.employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public Date getJoiningDate() {
        return this.joiningDate;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getLname() {
        return this.lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public Integer getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSpouseName() {
        return this.spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getUserMobile() {
        return this.userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserName() {
        return this.username;
    }

    public void setUserName(String userName) {
        this.username = userName;
    }

    public TeqipRolemaster getTeqipRolemaster() {
        return this.teqipRolemaster;
    }

    public void setTeqipRolemaster(TeqipRolemaster teqipRolemaster) {
        this.teqipRolemaster = teqipRolemaster;
    }

    public List<TeqipUsersMaster> getTeqipUsersMasters() {
        return this.teqipUsersMasters;
    }

    public void setTeqipUsersMasters(List<TeqipUsersMaster> teqipUsersMasters) {
        this.teqipUsersMasters = teqipUsersMasters;
    }

    /*	public TeqipUsersMaster addTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
		getTeqipUsersMasters().add(teqipUsersMaster);
		teqipUsersMaster.setTeqipUsersDetail(this);

		return teqipUsersMaster;
	}
     */
    public TeqipUsersMaster removeTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
        getTeqipUsersMasters().remove(teqipUsersMaster);
        teqipUsersMaster.setTeqipUsersDetail(null);

        return teqipUsersMaster;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
