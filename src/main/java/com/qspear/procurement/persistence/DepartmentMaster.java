
package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "teqip_pmssdepartmentmaster")
public class DepartmentMaster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DepartmentId")
	public Integer DepartmentId;
	@Column(name="Department")
	public String departmentname;
	@Column(name = "Department_Code")
	private String departmentcode;
	public Integer Created_By;
	public Timestamp Created_on;
	@Column(name = "ISACTIVE")
	public Integer isactive;
	public Date Modify_on;
	public Integer Modify_by;

	public String getDepartmentcode() {
		return departmentcode;
	}

	public void setDepartmentcode(String departmentcode) {
		this.departmentcode = departmentcode;
	}

	public Integer getDepartmentId() {
		return DepartmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		DepartmentId = departmentId;
	}

	public String getDepartmentname() {
		return departmentname;
	}

	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}

	public Timestamp getCreated_on() {
		return Created_on;
	}

	public void setCreated_on(Timestamp created_on) {
		Created_on = created_on;
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getModify_on() {
		return Modify_on;
	}

	public void setModify_on(Date modify_on) {
		Modify_on = modify_on;
	}

	public Integer getModify_by() {
		return Modify_by;
	}

	public void setModify_by(Integer modify_by) {
		Modify_by = modify_by;
	}

	public Integer getCreated_By() {
		return Created_By;
	}

	public void setCreated_By(Integer created_By) {
		Created_By = created_By;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
