package com.qspear.procurement.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the teqip_procurementmethod_timeline database table.
 * 
 */
@Entity
@Table(name="teqip_procurementmethod_timeline")
@NamedQuery(name="TeqipProcurementmethodTimeline.findAll", query="SELECT t FROM TeqipProcurementmethodTimeline t")
public class TeqipProcurementmethodTimeline implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="PROCUREMENTMETHOD_TIMELINEID")
	private Integer procurementmethodTimelineid;

	@Column(name = "advertisementdate_days")
	private Double advertisementDate_days;

	@Column(name = "advertisementdate_withoutbanknoc_days")
	private Double advertisementDate_WithoutBankNOC_days;

	@Column(name = "banknocforbiddingdocuments_days")
	private Double bankNOCForBiddingDocuments_days;

	@Column(name = "biddocumentpreparationdate_days")
	private Double bidDocumentPreparationDate_days;

	@Column(name = "bidinvitationdate_days")
	private Double bidInvitationDate_days;

	@Column(name = "bidopeningdate_days")
	private Double bidOpeningDate_days;

	@Column(name = "contractawarddate_days")
	private Double contractAwardDate_days;

	@Column(name = "contractawarddate_withoutbanknoc_days")
	private Double contractAwardDate_WithoutBankNOC_days;

	@Column(name = "contractcompletiondate_days")
	private Double contractCompletionDate_days;

	@Column(name = "contractcompletiondate_withoutbanknoc_days")
	private Double contractCompletionDate_WithoutBankNOC_days;

	@Column(name = "evaluationdate_days")
	private Double evaluationDate_days;

	@Column(name = "evaluationdate_withoutbanknoc_days")
	private Double evaluationDate_WithoutBankNOC_days;

	@Column(name = "finaldrafttobeforwardedtothebankdate_days")
	private Double finalDraftToBeForwardedToTheBankDate_days;

	@Column(name = "lastdatetoreceiveproposals_days")
	private Double lastDateToReceiveProposals_days;

	@Column(name = "lastdatetoreceiveproposals_withoutbanknoc_days")
	private Double lastDateToReceiveProposals_WithoutBankNOC_days;

	@Column(name = "noobjectionfrombankforevaluation_days")
	private Double noObjectionFromBankForEvaluation_days;

	@Column(name = "noobjectionfrombankforrfp_days")
	private Double noObjectionFromBankForRFP_days;

	@Column(name = "rfpissueddate_days")
	private Double RFPIssuedDate_days;

	@Column(name = "rfpissueddate_withoutbanknoc_days")
	private Double RFPIssuedDate_WithoutBankNOC_days;

	@Column(name = "torfinalizationdate_days")
	private Double TORFinalizationDate_days;

	@Column(name = "torfinalizationdate_withoutbanknoc_days")
	private Double TORFinalizationDate_WithoutBankNOC_days;

	//bi-directional many-to-one association to TeqipProcurementmethod
	@ManyToOne
	@JoinColumn(name="PROCUREMENTMETHOD_ID")
	private TeqipProcurementmethod teqipProcurementmethod;

	//bi-directional many-to-one association to TeqipCategorymaster
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID")
	private TeqipCategorymaster teqipCategorymaster;

	public TeqipProcurementmethodTimeline() {
	}

	public Integer getProcurementmethodTimelineid() {
		return this.procurementmethodTimelineid;
	}

	public void setProcurementmethodTimelineid(Integer procurementmethodTimelineid) {
		this.procurementmethodTimelineid = procurementmethodTimelineid;
	}

	public Double getAdvertisementDate_days() {
		return this.advertisementDate_days;
	}

	public void setAdvertisementDate_days(Double advertisementDate_days) {
		this.advertisementDate_days = advertisementDate_days;
	}

	public Double getAdvertisementDate_WithoutBankNOC_days() {
		return this.advertisementDate_WithoutBankNOC_days;
	}

	public void setAdvertisementDate_WithoutBankNOC_days(Double advertisementDate_WithoutBankNOC_days) {
		this.advertisementDate_WithoutBankNOC_days = advertisementDate_WithoutBankNOC_days;
	}

	public Double getBankNOCForBiddingDocuments_days() {
		return this.bankNOCForBiddingDocuments_days;
	}

	public void setBankNOCForBiddingDocuments_days(Double bankNOCForBiddingDocuments_days) {
		this.bankNOCForBiddingDocuments_days = bankNOCForBiddingDocuments_days;
	}

	public Double getBidDocumentPreparationDate_days() {
		return this.bidDocumentPreparationDate_days;
	}

	public void setBidDocumentPreparationDate_days(Double bidDocumentPreparationDate_days) {
		this.bidDocumentPreparationDate_days = bidDocumentPreparationDate_days;
	}

	public Double getBidInvitationDate_days() {
		return this.bidInvitationDate_days;
	}

	public void setBidInvitationDate_days(Double bidInvitationDate_days) {
		this.bidInvitationDate_days = bidInvitationDate_days;
	}

	public Double getBidOpeningDate_days() {
		return this.bidOpeningDate_days;
	}

	public void setBidOpeningDate_days(Double bidOpeningDate_days) {
		this.bidOpeningDate_days = bidOpeningDate_days;
	}

	public Double getContractAwardDate_days() {
		return this.contractAwardDate_days;
	}

	public void setContractAwardDate_days(Double contractAwardDate_days) {
		this.contractAwardDate_days = contractAwardDate_days;
	}

	public Double getContractAwardDate_WithoutBankNOC_days() {
		return this.contractAwardDate_WithoutBankNOC_days;
	}

	public void setContractAwardDate_WithoutBankNOC_days(Double contractAwardDate_WithoutBankNOC_days) {
		this.contractAwardDate_WithoutBankNOC_days = contractAwardDate_WithoutBankNOC_days;
	}

	public Double getContractCompletionDate_days() {
		return this.contractCompletionDate_days;
	}

	public void setContractCompletionDate_days(Double contractCompletionDate_days) {
		this.contractCompletionDate_days = contractCompletionDate_days;
	}

	public Double getContractCompletionDate_WithoutBankNOC_days() {
		return this.contractCompletionDate_WithoutBankNOC_days;
	}

	public void setContractCompletionDate_WithoutBankNOC_days(Double contractCompletionDate_WithoutBankNOC_days) {
		this.contractCompletionDate_WithoutBankNOC_days = contractCompletionDate_WithoutBankNOC_days;
	}

	public Double getEvaluationDate_days() {
		return this.evaluationDate_days;
	}

	public void setEvaluationDate_days(Double evaluationDate_days) {
		this.evaluationDate_days = evaluationDate_days;
	}

	public Double getEvaluationDate_WithoutBankNOC_days() {
		return this.evaluationDate_WithoutBankNOC_days;
	}

	public void setEvaluationDate_WithoutBankNOC_days(Double evaluationDate_WithoutBankNOC_days) {
		this.evaluationDate_WithoutBankNOC_days = evaluationDate_WithoutBankNOC_days;
	}

	public Double getFinalDraftToBeForwardedToTheBankDate_days() {
		return this.finalDraftToBeForwardedToTheBankDate_days;
	}

	public void setFinalDraftToBeForwardedToTheBankDate_days(Double finalDraftToBeForwardedToTheBankDate_days) {
		this.finalDraftToBeForwardedToTheBankDate_days = finalDraftToBeForwardedToTheBankDate_days;
	}

	public Double getLastDateToReceiveProposals_days() {
		return this.lastDateToReceiveProposals_days;
	}

	public void setLastDateToReceiveProposals_days(Double lastDateToReceiveProposals_days) {
		this.lastDateToReceiveProposals_days = lastDateToReceiveProposals_days;
	}

	public Double getLastDateToReceiveProposals_WithoutBankNOC_days() {
		return this.lastDateToReceiveProposals_WithoutBankNOC_days;
	}

	public void setLastDateToReceiveProposals_WithoutBankNOC_days(Double lastDateToReceiveProposals_WithoutBankNOC_days) {
		this.lastDateToReceiveProposals_WithoutBankNOC_days = lastDateToReceiveProposals_WithoutBankNOC_days;
	}

	public Double getNoObjectionFromBankForEvaluation_days() {
		return this.noObjectionFromBankForEvaluation_days;
	}

	public void setNoObjectionFromBankForEvaluation_days(Double noObjectionFromBankForEvaluation_days) {
		this.noObjectionFromBankForEvaluation_days = noObjectionFromBankForEvaluation_days;
	}

	public Double getNoObjectionFromBankForRFP_days() {
		return this.noObjectionFromBankForRFP_days;
	}

	public void setNoObjectionFromBankForRFP_days(Double noObjectionFromBankForRFP_days) {
		this.noObjectionFromBankForRFP_days = noObjectionFromBankForRFP_days;
	}

	public Double getRFPIssuedDate_days() {
		return this.RFPIssuedDate_days;
	}

	public void setRFPIssuedDate_days(Double RFPIssuedDate_days) {
		this.RFPIssuedDate_days = RFPIssuedDate_days;
	}

	public Double getRFPIssuedDate_WithoutBankNOC_days() {
		return this.RFPIssuedDate_WithoutBankNOC_days;
	}

	public void setRFPIssuedDate_WithoutBankNOC_days(Double RFPIssuedDate_WithoutBankNOC_days) {
		this.RFPIssuedDate_WithoutBankNOC_days = RFPIssuedDate_WithoutBankNOC_days;
	}

	public Double getTORFinalizationDate_days() {
		return this.TORFinalizationDate_days;
	}

	public void setTORFinalizationDate_days(Double TORFinalizationDate_days) {
		this.TORFinalizationDate_days = TORFinalizationDate_days;
	}

	public Double getTORFinalizationDate_WithoutBankNOC_days() {
		return this.TORFinalizationDate_WithoutBankNOC_days;
	}

	public void setTORFinalizationDate_WithoutBankNOC_days(Double TORFinalizationDate_WithoutBankNOC_days) {
		this.TORFinalizationDate_WithoutBankNOC_days = TORFinalizationDate_WithoutBankNOC_days;
	}

	public TeqipProcurementmethod getTeqipProcurementmethod() {
		return this.teqipProcurementmethod;
	}

	public void setTeqipProcurementmethod(TeqipProcurementmethod teqipProcurementmethod) {
		this.teqipProcurementmethod = teqipProcurementmethod;
	}

	public TeqipCategorymaster getTeqipCategorymaster() {
		return this.teqipCategorymaster;
	}

	public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
		this.teqipCategorymaster = teqipCategorymaster;
	}

}