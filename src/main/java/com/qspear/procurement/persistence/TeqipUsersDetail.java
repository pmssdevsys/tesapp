package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.poi.hwpf.usermodel.DateAndTime;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * The persistent class for the teqip_users_detail database table.
 *
 */
@Entity
@Table(name = "teqip_users_detail")
@NamedQuery(name = "TeqipUsersDetail.findAll", query = "SELECT t FROM TeqipUsersDetail t")
public class TeqipUsersDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userid;

    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "CREATED_ON")
    private Timestamp createdOn;

    private Double ctc;

    @Column(name = "DEPT_ID")
    private Integer deptId;

    @Temporal(TemporalType.DATE)
    private Date dob;

    private String email;

    @Column(name = "EMPLOYEE_CODE")
    private String employeeCode;

    private String gender;

    private Boolean isactive;

    @Temporal(TemporalType.DATE)
    @Column(name = "JOINING_DATE")
    private Date joiningDate;

    private String lname;

    @Column(name = "MODIFIED_BY")
    private Integer modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "MODIFIED_ON")
    private Date modifiedOn;

    private String name;

    private String password;

    @Column(name = "SPOUSE_NAME")
    private String spouseName;

    @Column(name = "USER_MOBILE")
    private String userMobile;

    @Column(name = "USER_NAME")
    private String userName;
    @Column(name="isBlocked")
    private Boolean isBlocked;
    private Timestamp blockedAt;
    @Column(name="email_attempt")
    private Integer emailAttempt;
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="email_attemptTime")
    private Date emailattemptTime;
    
	public Date getEmailattemptTime() {
		return emailattemptTime;
	}

	public void setEmailattemptTime(Date    emailattemptTime) {
		this.emailattemptTime = emailattemptTime;
	}

	public Integer getEmailAttempt() {
		return emailAttempt;
	}

	public void setEmailAttempt(Integer emailAttempt) {
		this.emailAttempt = emailAttempt;
	}

	public Boolean getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public Timestamp getBlockedAt() {
		return blockedAt;
	}

	public void setBlockedAt(Timestamp blockedAt) {
		this.blockedAt = blockedAt;
	}

	//bi-directional many-to-one association to TeqipRolemaster
    @ManyToOne
    @JoinColumn(name = "ROLE_ID")
    private TeqipRolemaster teqipRolemaster;

    //bi-directional many-to-one association to TeqipUsersMaster
    @OneToMany(mappedBy = "teqipUsersDetail")
    private List<TeqipUsersMaster> teqipUsersMasters;

    @Column(name = "employeed_id")
    private Integer employeedId;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "pincode")
    private String pincode;

    @Column(name = "FName")
    private String FName;

    @Column(name = "MiddleName")
    private String MiddleName;

    public TeqipUsersDetail() {
    }

    public Integer getEmployeedId() {
        return employeedId;
    }

    public void setEmployeedId(Integer employeedId) {
        this.employeedId = employeedId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String fName) {
        FName = fName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public Integer getUserid() {
        return this.userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Double getCtc() {
        return this.ctc;
    }

    public void setCtc(Double ctc) {
        this.ctc = ctc;
    }

    public Integer getDeptId() {
        return this.deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployeeCode() {
        return this.employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public Date getJoiningDate() {
        return this.joiningDate;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getLname() {
        return this.lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public Integer getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSpouseName() {
        return this.spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getUserMobile() {
        return this.userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TeqipRolemaster getTeqipRolemaster() {
        return this.teqipRolemaster;
    }

    public void setTeqipRolemaster(TeqipRolemaster teqipRolemaster) {
        this.teqipRolemaster = teqipRolemaster;
    }

    public List<TeqipUsersMaster> getTeqipUsersMasters() {
        return this.teqipUsersMasters;
    }

    public void setTeqipUsersMasters(List<TeqipUsersMaster> teqipUsersMasters) {
        this.teqipUsersMasters = teqipUsersMasters;
    }

    public TeqipUsersMaster addTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
        getTeqipUsersMasters().add(teqipUsersMaster);
        teqipUsersMaster.setTeqipUsersDetail(this);

        return teqipUsersMaster;
    }

    public TeqipUsersMaster removeTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
        getTeqipUsersMasters().remove(teqipUsersMaster);
        teqipUsersMaster.setTeqipUsersDetail(null);

        return teqipUsersMaster;
    }

	

}
