package com.qspear.procurement.persistence;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="teqip_helpdesk_ticket")
public class Helpdesk_ticket {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer HDnTicketId;
	private Integer HDnTicketTypeId;
	private Integer HdnTicketPriorityId;
	private Integer HDnLocationId;
	private Integer HDnTicketStatusId;
	private String HDcTitle;
	private String HDcDetails;
	private Integer ModifiedBy;
	
	private Date ModifiedOn;
	
	private String UserName;
	private String UserPhone;
	private String UserMobile;
	private String UserEmail;
	private Integer HdnTicketSeverityId;
	private String HdnTicketnumber;
	public Integer getHDnTicketId() {
		return HDnTicketId;
	}
	public void setHDnTicketId(Integer hDnTicketId) {
		HDnTicketId = hDnTicketId;
	}
	public Integer getHDnTicketTypeId() {
		return HDnTicketTypeId;
	}
	public void setHDnTicketTypeId(Integer hDnTicketTypeId) {
		HDnTicketTypeId = hDnTicketTypeId;
	}
	public Integer getHdnTicketPriorityId() {
		return HdnTicketPriorityId;
	}
	public void setHdnTicketPriorityId(Integer hdnTicketPriorityId) {
		HdnTicketPriorityId = hdnTicketPriorityId;
	}
	public Integer getHDnLocationId() {
		return HDnLocationId;
	}
	public void setHDnLocationId(Integer hDnLocationId) {
		HDnLocationId = hDnLocationId;
	}
	public Integer getHDnTicketStatusId() {
		return HDnTicketStatusId;
	}
	public void setHDnTicketStatusId(Integer hDnTicketStatusId) {
		HDnTicketStatusId = hDnTicketStatusId;
	}
	public String getHDcTitle() {
		return HDcTitle;
	}
	public void setHDcTitle(String hDcTitle) {
		HDcTitle = hDcTitle;
	}
	public String getHDcDetails() {
		return HDcDetails;
	}
	public void setHDcDetails(String hDcDetails) {
		HDcDetails = hDcDetails;
	}
	public Integer getModifiedBy() {
		return ModifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		ModifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return ModifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		ModifiedOn = modifiedOn;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getUserPhone() {
		return UserPhone;
	}
	public void setUserPhone(String userPhone) {
		UserPhone = userPhone;
	}
	public String getUserMobile() {
		return UserMobile;
	}
	public void setUserMobile(String userMobile) {
		UserMobile = userMobile;
	}
	public String getUserEmail() {
		return UserEmail;
	}
	public void setUserEmail(String userEmail) {
		UserEmail = userEmail;
	}
	public Integer getHdnTicketSeverityId() {
		return HdnTicketSeverityId;
	}
	public void setHdnTicketSeverityId(Integer hdnTicketSeverityId) {
		HdnTicketSeverityId = hdnTicketSeverityId;
	}
	@Override
	public String toString() {
		return "Helpdesk_ticket [HDnTicketId=" + HDnTicketId + ", HDnTicketTypeId=" + HDnTicketTypeId
				+ ", HdnTicketPriorityId=" + HdnTicketPriorityId + ", HDnLocationId=" + HDnLocationId
				+ ", HDnTicketStatusId=" + HDnTicketStatusId + ", HDcTitle=" + HDcTitle + ", HDcDetails=" + HDcDetails
				+ ", ModifiedBy=" + ModifiedBy + ", ModifiedOn=" + ModifiedOn + ", UserName=" + UserName
				+ ", UserPhone=" + UserPhone + ", UserMobile=" + UserMobile + ", UserEmail=" + UserEmail
				+ ", HdnTicketSeverityId=" + HdnTicketSeverityId + "]";
	}
	public String getHdnTicketnumber() {
		return HdnTicketnumber;
	}
	public void setHdnTicketnumber(String hdnTicketnumber) {
		HdnTicketnumber = hdnTicketnumber;
	}
	

}
