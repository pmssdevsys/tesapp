package com.qspear.procurement.persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the teqip_pmss_currencymaster database table.
 * 
 */
@Entity
@Table(name="teqip_pmss_currencymaster")
@NamedQuery(name="TeqipPmssCurrencymaster.findAll", query="SELECT t FROM TeqipPmssCurrencymaster t")
public class TeqipPmssCurrencymaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer currencyid;

	private String currencycountry;

	private String currencyname;

	private Double currencyvalue;

	public TeqipPmssCurrencymaster() {
	}

	public Integer getCurrencyid() {
		return this.currencyid;
	}

	public void setCurrencyid(Integer currencyid) {
		this.currencyid = currencyid;
	}

	public String getCurrencycountry() {
		return this.currencycountry;
	}

	public void setCurrencycountry(String currencycountry) {
		this.currencycountry = currencycountry;
	}

	public String getCurrencyname() {
		return this.currencyname;
	}

	public void setCurrencyname(String currencyname) {
		this.currencyname = currencyname;
	}

	public Double getCurrencyvalue() {
		return this.currencyvalue;
	}

	public void setCurrencyvalue(Double currencyvalue) {
		this.currencyvalue = currencyvalue;
	}

}