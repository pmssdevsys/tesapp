package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the TEQIP_INSTITUTIONS database table.
 * 
 */
@Entity
@Table(name="TEQIP_INSTITUTIONS")
public class TeqipInstitution implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="INSTITUTION_ID")
	private Integer institutionId;

	private String address;

	private Double allocatedbuget;

	@Column(name="CREATED_BY")
	private Integer createdBy;

	@Column(name="CREATED_ON")
	private Timestamp createdOn;

	private String emailid;

	private String faxnumber;

	@Column(name="INSTITUTION_CODE")
	private String institutionCode;

	@Column(name="INSTITUTION_NAME")
	private String institutionName;

	private String state;

	private String telephonenumber;

	private String websiteurl;

	//bi-directional many-to-one association to TeqipInstitutiondepartmentmapping
	@OneToMany(mappedBy="teqipInstitution")
	private List<TeqipInstitutiondepartmentmapping> teqipInstitutiondepartmentmappings;

	//bi-directional many-to-one association to TeqipInstitutiontype
	@ManyToOne
	@JoinColumn(name="INSTITUTIONTYPE_ID")
	private TeqipInstitutiontype teqipInstitutiontype;

	//bi-directional many-to-one association to TeqipStatemaster
	@ManyToOne
	@JoinColumn(name="SPFUID")
	private TeqipStatemaster teqipStatemaster;

	//bi-directional many-to-one association to TeqipInstitutionsubcomponentmapping
	@OneToMany(mappedBy="teqipInstitution")
	private List<TeqipInstitutionsubcomponentmapping> teqipInstitutionsubcomponentmappings;

	//bi-directional many-to-one association to TeqipPackage
	@OneToMany(mappedBy="teqipInstitution")
	private List<TeqipPackage> teqipPackages;
        
	//bi-directional many-to-one association to TeqipPmssInstitutionlogo
	@OneToMany(mappedBy="teqipInstitution")
	private List<TeqipPmssInstitutionlogo> teqipPmssInstitutionlogos;

	//bi-directional many-to-one association to TeqipPmssInstitutionpurchasecommitte
	@OneToMany(mappedBy="teqipInstitution")
	private List<TeqipPmssInstitutionpurchasecommitte> teqipPmssInstitutionpurchasecommittes;

	//bi-directional many-to-one association to TeqipUsersMaster
	@OneToMany(mappedBy="teqipInstitution")
	private List<TeqipUsersMaster> teqipUsersMasters;

	public TeqipInstitution() {
	}

	public Integer getInstitutionId() {
		return this.institutionId;
	}

	public void setInstitutionId(Integer institutionId) {
		this.institutionId = institutionId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getAllocatedbuget() {
		return this.allocatedbuget;
	}

	public void setAllocatedbuget(Double allocatedbuget) {
		this.allocatedbuget = allocatedbuget;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getEmailid() {
		return this.emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getFaxnumber() {
		return this.faxnumber;
	}

	public void setFaxnumber(String faxnumber) {
		this.faxnumber = faxnumber;
	}

	public String getInstitutionCode() {
		return this.institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getInstitutionName() {
		return this.institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTelephonenumber() {
		return this.telephonenumber;
	}

	public void setTelephonenumber(String telephonenumber) {
		this.telephonenumber = telephonenumber;
	}

	public String getWebsiteurl() {
		return this.websiteurl;
	}

	public void setWebsiteurl(String websiteurl) {
		this.websiteurl = websiteurl;
	}

	public List<TeqipInstitutiondepartmentmapping> getTeqipInstitutiondepartmentmappings() {
		return this.teqipInstitutiondepartmentmappings;
	}

	public void setTeqipInstitutiondepartmentmappings(List<TeqipInstitutiondepartmentmapping> teqipInstitutiondepartmentmappings) {
		this.teqipInstitutiondepartmentmappings = teqipInstitutiondepartmentmappings;
	}

	public TeqipInstitutiondepartmentmapping addTeqipInstitutiondepartmentmapping(TeqipInstitutiondepartmentmapping teqipInstitutiondepartmentmapping) {
		getTeqipInstitutiondepartmentmappings().add(teqipInstitutiondepartmentmapping);
		teqipInstitutiondepartmentmapping.setTeqipInstitution(this);

		return teqipInstitutiondepartmentmapping;
	}

	public TeqipInstitutiondepartmentmapping removeTeqipInstitutiondepartmentmapping(TeqipInstitutiondepartmentmapping teqipInstitutiondepartmentmapping) {
		getTeqipInstitutiondepartmentmappings().remove(teqipInstitutiondepartmentmapping);
		teqipInstitutiondepartmentmapping.setTeqipInstitution(null);

		return teqipInstitutiondepartmentmapping;
	}

	public TeqipInstitutiontype getTeqipInstitutiontype() {
		return this.teqipInstitutiontype;
	}

	public void setTeqipInstitutiontype(TeqipInstitutiontype teqipInstitutiontype) {
		this.teqipInstitutiontype = teqipInstitutiontype;
	}

	public TeqipStatemaster getTeqipStatemaster() {
		return this.teqipStatemaster;
	}

	public void setTeqipStatemaster(TeqipStatemaster teqipStatemaster) {
		this.teqipStatemaster = teqipStatemaster;
	}

	public List<TeqipInstitutionsubcomponentmapping> getTeqipInstitutionsubcomponentmappings() {
		return this.teqipInstitutionsubcomponentmappings;
	}

	public void setTeqipInstitutionsubcomponentmappings(List<TeqipInstitutionsubcomponentmapping> teqipInstitutionsubcomponentmappings) {
		this.teqipInstitutionsubcomponentmappings = teqipInstitutionsubcomponentmappings;
	}

	public TeqipInstitutionsubcomponentmapping addTeqipInstitutionsubcomponentmapping(TeqipInstitutionsubcomponentmapping teqipInstitutionsubcomponentmapping) {
		getTeqipInstitutionsubcomponentmappings().add(teqipInstitutionsubcomponentmapping);
		teqipInstitutionsubcomponentmapping.setTeqipInstitution(this);

		return teqipInstitutionsubcomponentmapping;
	}

	public TeqipInstitutionsubcomponentmapping removeTeqipInstitutionsubcomponentmapping(TeqipInstitutionsubcomponentmapping teqipInstitutionsubcomponentmapping) {
		getTeqipInstitutionsubcomponentmappings().remove(teqipInstitutionsubcomponentmapping);
		teqipInstitutionsubcomponentmapping.setTeqipInstitution(null);

		return teqipInstitutionsubcomponentmapping;
	}

	public List<TeqipPackage> getTeqipPackages() {
		return this.teqipPackages;
	}

	public void setTeqipPackages(List<TeqipPackage> teqipPackages) {
		this.teqipPackages = teqipPackages;
	}

	public TeqipPackage addTeqipPackage(TeqipPackage teqipPackage) {
		getTeqipPackages().add(teqipPackage);
		teqipPackage.setTeqipInstitution(this);

		return teqipPackage;
	}

	public TeqipPackage removeTeqipPackage(TeqipPackage teqipPackage) {
		getTeqipPackages().remove(teqipPackage);
		teqipPackage.setTeqipInstitution(null);

		return teqipPackage;
	}


	public List<TeqipPmssInstitutionlogo> getTeqipPmssInstitutionlogos() {
		return this.teqipPmssInstitutionlogos;
	}

	public void setTeqipPmssInstitutionlogos(List<TeqipPmssInstitutionlogo> teqipPmssInstitutionlogos) {
		this.teqipPmssInstitutionlogos = teqipPmssInstitutionlogos;
	}

	public TeqipPmssInstitutionlogo addTeqipPmssInstitutionlogo(TeqipPmssInstitutionlogo teqipPmssInstitutionlogo) {
		getTeqipPmssInstitutionlogos().add(teqipPmssInstitutionlogo);
		teqipPmssInstitutionlogo.setTeqipInstitution(this);

		return teqipPmssInstitutionlogo;
	}

	public TeqipPmssInstitutionlogo removeTeqipPmssInstitutionlogo(TeqipPmssInstitutionlogo teqipPmssInstitutionlogo) {
		getTeqipPmssInstitutionlogos().remove(teqipPmssInstitutionlogo);
		teqipPmssInstitutionlogo.setTeqipInstitution(null);

		return teqipPmssInstitutionlogo;
	}

	public List<TeqipPmssInstitutionpurchasecommitte> getTeqipPmssInstitutionpurchasecommittes() {
		return this.teqipPmssInstitutionpurchasecommittes;
	}

	public void setTeqipPmssInstitutionpurchasecommittes(List<TeqipPmssInstitutionpurchasecommitte> teqipPmssInstitutionpurchasecommittes) {
		this.teqipPmssInstitutionpurchasecommittes = teqipPmssInstitutionpurchasecommittes;
	}

	public TeqipPmssInstitutionpurchasecommitte addTeqipPmssInstitutionpurchasecommitte(TeqipPmssInstitutionpurchasecommitte teqipPmssInstitutionpurchasecommitte) {
		getTeqipPmssInstitutionpurchasecommittes().add(teqipPmssInstitutionpurchasecommitte);
		teqipPmssInstitutionpurchasecommitte.setTeqipInstitution(this);

		return teqipPmssInstitutionpurchasecommitte;
	}

	public TeqipPmssInstitutionpurchasecommitte removeTeqipPmssInstitutionpurchasecommitte(TeqipPmssInstitutionpurchasecommitte teqipPmssInstitutionpurchasecommitte) {
		getTeqipPmssInstitutionpurchasecommittes().remove(teqipPmssInstitutionpurchasecommitte);
		teqipPmssInstitutionpurchasecommitte.setTeqipInstitution(null);

		return teqipPmssInstitutionpurchasecommitte;
	}

	public List<TeqipUsersMaster> getTeqipUsersMasters() {
		return this.teqipUsersMasters;
	}

	public void setTeqipUsersMasters(List<TeqipUsersMaster> teqipUsersMasters) {
		this.teqipUsersMasters = teqipUsersMasters;
	}

	public TeqipUsersMaster addTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
		getTeqipUsersMasters().add(teqipUsersMaster);
		teqipUsersMaster.setTeqipInstitution(this);

		return teqipUsersMaster;
	}

	public TeqipUsersMaster removeTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
		getTeqipUsersMasters().remove(teqipUsersMaster);
		teqipUsersMaster.setTeqipInstitution(null);

		return teqipUsersMaster;
	}

}