package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the teqip_users_master database table.
 *
 */
@Entity
@Table(name = "teqip_users_master")
@NamedQuery(name = "TeqipUsersMaster.findAll", query = "SELECT t FROM TeqipUsersMaster t")
public class TeqipUsersMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "CREATED_ON")
    private Timestamp createdOn;

    @Column(name = "EMPLOYEE_CODE")
    private String employeeCode;

    private Boolean isactive;

    @Column(name = "MODIFIED_BY")
    private Integer modifiedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "MODIFIED_ON")
    private Date modifiedOn;

    //bi-directional many-to-one association to TeqipUsersDetail
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private TeqipUsersDetail teqipUsersDetail;

    //bi-directional many-to-one association to TeqipInstitution
    @ManyToOne
    @JoinColumn(name = "INSTITUTION_ID")
    private TeqipInstitution teqipInstitution;

    //bi-directional many-to-one association to TeqipRolemaster
    @ManyToOne
    @JoinColumn(name = "ROLE_ID")
    private TeqipRolemaster teqipRolemaster;

    //bi-directional many-to-one association to TeqipStatemaster
    @ManyToOne
    @JoinColumn(name = "SPFUID")
    private TeqipStatemaster teqipStatemaster;

    public TeqipUsersMaster() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getEmployeeCode() {
        return this.employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public Integer getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public TeqipUsersDetail getTeqipUsersDetail() {
        return this.teqipUsersDetail;
    }

    public void setTeqipUsersDetail(TeqipUsersDetail teqipUsersDetail) {
        this.teqipUsersDetail = teqipUsersDetail;
    }

    public TeqipInstitution getTeqipInstitution() {
        return this.teqipInstitution;
    }

    public void setTeqipInstitution(TeqipInstitution teqipInstitution) {
        this.teqipInstitution = teqipInstitution;
    }

    public TeqipRolemaster getTeqipRolemaster() {
        return this.teqipRolemaster;
    }

    public void setTeqipRolemaster(TeqipRolemaster teqipRolemaster) {
        this.teqipRolemaster = teqipRolemaster;
    }

    public TeqipStatemaster getTeqipStatemaster() {
        return this.teqipStatemaster;
    }

    public void setTeqipStatemaster(TeqipStatemaster teqipStatemaster) {
        this.teqipStatemaster = teqipStatemaster;
    }

}
