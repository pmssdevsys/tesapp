package com.qspear.procurement.persistence;

import com.qspear.procurement.persistence.methods.*;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the TEQIP_PACKAGE database table.
 *
 */
@Entity
@Table(name = "TEQIP_PACKAGE")
public class TeqipPackage extends AuditEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PACKAGE_ID")
    private Integer packageId;

    @Column(name = "REVISEID")
    private Integer reviseId;

    @Column(name = "ESTIMATED_COST")
    private Double estimatedCost;

    @Temporal(TemporalType.DATE)
    @Column(name = "FINANCIAL_SANCTION_DATE")
    private Date financialSanctionDate;

    private Boolean iscoe;

    private Boolean isgem;

    private Boolean isprop;

    private String justification;

    @Column(name = "PACKAGE_CODE")
    private String packageCode;

    @Column(name = "PACKAGE_NAME")
    private String packageName;

    @Column(name = "REVISION_COMMENTS")
    private String revisionComments;

    @Column(name = "SERVICE_PROVIDER_ID")
    private Integer serviceProviderId;

    @Column(name = "TYPEOFPLAN_CREATOR")
    private Integer typeofplanCreator;

    @Column(name = "PACKAGE_INITIATEDATE")
    private Date initiateDate;

    private String packageMetaData;

    private Integer isPackageCompleted;

    private Integer isPackageCancelled;
    private Integer isInitiated;
    private Double consumeFromWorks;
    private String currentStage;
    private Integer currentStageIndex;
    private Integer isgemdcbid;
    private String purchaseType;
    private Integer isOldPackage;
    private String roleOfNextActor;
    private Integer isCurrentlyInPriorReview;

    //bi-directional many-to-one association to TeqipCategorymaster
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CATEGORY_ID")
    private TeqipCategorymaster teqipCategorymaster;

    //bi-directional many-to-one association to TeqipSubcategorymaster
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SUB_CATEGORY_ID")
    private TeqipSubcategorymaster teqipSubcategorymaster;

    //bi-directional many-to-one association to TeqipActivitiymaster
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ACTIVITY_ID")
    private TeqipActivitiymaster teqipActivitiymaster;

    //bi-directional many-to-one association to TeqipProcurementmethod
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PROCUREMENT_METHOD_ID")
    private TeqipProcurementmethod teqipProcurementmethod;

    //bi-directional many-to-one association to TeqipProcurementmaster
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PROCUREMENT_PLAN_ID")
    private TeqipProcurementmaster teqipProcurementmaster;

    //bi-directional many-to-one association to TeqipInstitution
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INSTITUTION_ID")
    private TeqipInstitution teqipInstitution;

    //bi-directional many-to-one association to TeqipStatemaster
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SPFUID")
    private TeqipStatemaster teqipStatemaster;

    //bi-directional many-to-one association to TeqipPmssProcurementstage
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PROCUREMENTSTAGE_ID")
    private TeqipPmssProcurementstage teqipPmssProcurementstage;

    //bi-directional many-to-one association to TeqipSubcomponentmaster
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SUB_COMPONENT_ID")
    private TeqipSubcomponentmaster teqipSubcomponentmaster;

    //bi-directional many-to-one association to TeqipItemGoodsDetail
    @OneToMany(mappedBy = "teqipPackage")
    private List<TeqipItemGoodsDetail> teqipItemGoodsDetails;

    @OneToMany(mappedBy = "teqipPackage")
    private List<TeqipPackageQuestionMaster> teqipPackageQuestionMasters;

    @OneToMany(mappedBy = "teqipPackage")
    private List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails;

    @OneToMany(mappedBy = "teqipPackage")
    private List<TeqipPackagePaymentDetail> teqipPackagePaymentDetails;

    @OneToMany(mappedBy = "teqipPackage")
    private List<TeqipPackageQuotationsSupplierInput> teqipPackageQuotationsEvaluations;

    @OneToMany(mappedBy = "teqipPackage")
    private List<TeqipPackageDocument> teqipPackageDocuments;

    @OneToMany(mappedBy = "teqipPackage")
    private List<TeqipPackageGRNDetail> teqipPackageGRNDetails;

    //bi-directional many-to-one association to TeqipItemWorkDetail
    @OneToMany(mappedBy = "teqipPackage")
    private List<TeqipItemWorkDetail> teqipItemWorkDetails;

    @OneToMany(mappedBy = "teqipPackage")
    private List<TeqipPackagePriorReview> teqipPackagePriorReviews;

    private Date packageInitiationDate;

    public Date getPackageInitiationDate() {
        return packageInitiationDate;
    }

    public void setPackageInitiationDate(Date packageInitiationDate) {
        this.packageInitiationDate = packageInitiationDate;
    }

    public TeqipPackage() {
    }

    public Integer getPackageId() {
        return this.packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Double getEstimatedCost() {
        return this.estimatedCost;
    }

    public void setEstimatedCost(Double estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public Date getFinancialSanctionDate() {
        return this.financialSanctionDate;
    }

    public void setFinancialSanctionDate(Date financialSanctionDate) {
        this.financialSanctionDate = financialSanctionDate;
    }

    public Boolean getIscoe() {
        return this.iscoe;
    }

    public void setIscoe(Boolean iscoe) {
        this.iscoe = iscoe;
    }

    public Boolean getIsgem() {
        return this.isgem;
    }

    public void setIsgem(Boolean isgem) {
        this.isgem = isgem;
    }

    public Boolean getIsprop() {
        return this.isprop;
    }

    public void setIsprop(Boolean isprop) {
        this.isprop = isprop;
    }

    public String getJustification() {
        return this.justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }

    public String getPackageCode() {
        return this.packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Integer getReviseId() {
        return this.reviseId;
    }

    public void setReviseId(Integer reviseId) {
        this.reviseId = reviseId;
    }

    public String getRevisionComments() {
        return this.revisionComments;
    }

    public void setRevisionComments(String revisionComments) {
        this.revisionComments = revisionComments;
    }

    public Integer getServiceProviderId() {
        return this.serviceProviderId;
    }

    public void setServiceProviderId(Integer serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public Integer getTypeofplanCreator() {
        return this.typeofplanCreator;
    }

    public void setTypeofplanCreator(Integer typeofplanCreator) {
        this.typeofplanCreator = typeofplanCreator;
    }

    public TeqipCategorymaster getTeqipCategorymaster() {
        return this.teqipCategorymaster;
    }

    public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
        this.teqipCategorymaster = teqipCategorymaster;
    }

    public TeqipSubcategorymaster getTeqipSubcategorymaster() {
        return this.teqipSubcategorymaster;
    }

    public void setTeqipSubcategorymaster(TeqipSubcategorymaster teqipSubcategorymaster) {
        this.teqipSubcategorymaster = teqipSubcategorymaster;
    }

    public TeqipActivitiymaster getTeqipActivitiymaster() {
        return this.teqipActivitiymaster;
    }

    public void setTeqipActivitiymaster(TeqipActivitiymaster teqipActivitiymaster) {
        this.teqipActivitiymaster = teqipActivitiymaster;
    }

    public TeqipProcurementmethod getTeqipProcurementmethod() {
        return this.teqipProcurementmethod;
    }

    public void setTeqipProcurementmethod(TeqipProcurementmethod teqipProcurementmethod) {
        this.teqipProcurementmethod = teqipProcurementmethod;
    }

    public TeqipProcurementmaster getTeqipProcurementmaster() {
        return this.teqipProcurementmaster;
    }

    public void setTeqipProcurementmaster(TeqipProcurementmaster teqipProcurementmaster) {
        this.teqipProcurementmaster = teqipProcurementmaster;
    }

    public TeqipInstitution getTeqipInstitution() {
        return this.teqipInstitution;
    }

    public void setTeqipInstitution(TeqipInstitution teqipInstitution) {
        this.teqipInstitution = teqipInstitution;
    }

    public TeqipStatemaster getTeqipStatemaster() {
        return this.teqipStatemaster;
    }

    public void setTeqipStatemaster(TeqipStatemaster teqipStatemaster) {
        this.teqipStatemaster = teqipStatemaster;
    }

    public TeqipPmssProcurementstage getTeqipPmssProcurementstage() {
        return this.teqipPmssProcurementstage;
    }

    public void setTeqipPmssProcurementstage(TeqipPmssProcurementstage teqipPmssProcurementstage) {
        this.teqipPmssProcurementstage = teqipPmssProcurementstage;
    }

    public TeqipSubcomponentmaster getTeqipSubcomponentmaster() {
        return teqipSubcomponentmaster;
    }

    public void setTeqipSubcomponentmaster(TeqipSubcomponentmaster teqipSubcomponentmaster) {
        this.teqipSubcomponentmaster = teqipSubcomponentmaster;
    }

    public List<TeqipItemGoodsDetail> getTeqipItemGoodsDetails() {
        return teqipItemGoodsDetails;
    }

    public void setTeqipItemGoodsDetails(List<TeqipItemGoodsDetail> teqipItemGoodsDetails) {
        this.teqipItemGoodsDetails = teqipItemGoodsDetails;
    }

    public List<TeqipPackageSupplierDetail> getTeqipPackageSupplierDetails() {
        return teqipPackageSupplierDetails;
    }

    public void setTeqipPackageSupplierDetails(List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails) {
        this.teqipPackageSupplierDetails = teqipPackageSupplierDetails;
    }

    public List<TeqipPackagePaymentDetail> getTeqipPackagePaymentDetails() {
        return teqipPackagePaymentDetails;
    }

    public void setTeqipPackagePaymentDetails(List<TeqipPackagePaymentDetail> teqipPackagePaymentDetails) {
        this.teqipPackagePaymentDetails = teqipPackagePaymentDetails;
    }

    public List<TeqipPackageQuotationsSupplierInput> getTeqipPackageQuotationsEvaluations() {
        return teqipPackageQuotationsEvaluations;
    }

    public void setTeqipPackageQuotationsEvaluations(List<TeqipPackageQuotationsSupplierInput> teqipPackageQuotationsEvaluations) {
        this.teqipPackageQuotationsEvaluations = teqipPackageQuotationsEvaluations;
    }

    public List<TeqipPackageDocument> getTeqipPackageDocuments() {
        return teqipPackageDocuments;
    }

    public void setTeqipPackageDocuments(List<TeqipPackageDocument> teqipPackageDocuments) {
        this.teqipPackageDocuments = teqipPackageDocuments;
    }

    public List<TeqipPackageGRNDetail> getTeqipPackageGRNDetails() {
        return teqipPackageGRNDetails;
    }

    public void setTeqipPackageGRNDetails(List<TeqipPackageGRNDetail> teqipPackageGRNDetails) {
        this.teqipPackageGRNDetails = teqipPackageGRNDetails;
    }

    public Date getInitiateDate() {
        return initiateDate;
    }

    public void setInitiateDate(Date initiateDate) {
        this.initiateDate = initiateDate;
    }

    public List<TeqipItemWorkDetail> getTeqipItemWorkDetails() {
        return teqipItemWorkDetails;
    }

    public void setTeqipItemWorkDetails(List<TeqipItemWorkDetail> teqipItemWorkDetails) {
        this.teqipItemWorkDetails = teqipItemWorkDetails;
    }

    public List<TeqipPackageQuestionMaster> getTeqipPackageQuestionMasters() {
        return teqipPackageQuestionMasters;
    }

    public void setTeqipPackageQuestionMasters(List<TeqipPackageQuestionMaster> teqipPackageQuestionMasters) {
        this.teqipPackageQuestionMasters = teqipPackageQuestionMasters;
    }

    public String getPackageMetaData() {
        return packageMetaData;
    }

    public void setPackageMetaData(String packageMetaData) {
        this.packageMetaData = packageMetaData;
    }

    public Integer getIsPackageCompleted() {
        return isPackageCompleted;
    }

    public void setIsPackageCompleted(Integer isPackageCompleted) {
        this.isPackageCompleted = isPackageCompleted;
    }

    public Integer getIsPackageCancelled() {
        return isPackageCancelled;
    }

    public void setIsPackageCancelled(Integer isPackageCancelled) {
        this.isPackageCancelled = isPackageCancelled;
    }

    public Integer getIsInitiated() {
        return isInitiated;
    }

    public void setIsInitiated(Integer isInitiated) {
        this.isInitiated = isInitiated;
    }

    public boolean isPackageInitiated() {
        return this.initiateDate != null && this.isInitiated != null && this.isInitiated == 1;
    }

    public Double getConsumeFromWorks() {
        return consumeFromWorks;
    }

    public void setConsumeFromWorks(Double consumeFromWorks) {
        this.consumeFromWorks = consumeFromWorks;
    }

    public String getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(String currentStage) {
        this.currentStage = currentStage;
    }

    public Integer getCurrentStageIndex() {
        return currentStageIndex;
    }

    public void setCurrentStageIndex(Integer currentStageIndex) {
        this.currentStageIndex = currentStageIndex;
    }

    public List<TeqipPackagePriorReview> getTeqipPackagePriorReviews() {
        return teqipPackagePriorReviews;
    }

    public void setTeqipPackagePriorReviews(List<TeqipPackagePriorReview> teqipPackagePriorReviews) {
        this.teqipPackagePriorReviews = teqipPackagePriorReviews;
    }

    public Integer getIsgemdcbid() {
        return isgemdcbid;
    }

    public void setIsgemdcbid(Integer isgemdcbid) {
        this.isgemdcbid = isgemdcbid;
    }

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public Integer getIsOldPackage() {
        return isOldPackage;
    }

    public void setIsOldPackage(Integer isOldPackage) {
        this.isOldPackage = isOldPackage;
    }

    public String getRoleOfNextActor() {
        return roleOfNextActor;
    }

    public void setRoleOfNextActor(String roleOfNextActor) {
        this.roleOfNextActor = roleOfNextActor;
    }

    public Integer getIsCurrentlyInPriorReview() {
        return isCurrentlyInPriorReview;
    }

    public void setIsCurrentlyInPriorReview(Integer isCurrentlyInPriorReview) {
        this.isCurrentlyInPriorReview = isCurrentlyInPriorReview;
    }

}
