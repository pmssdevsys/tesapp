package com.qspear.procurement.persistence.methods;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.qspear.procurement.persistence.TeqipPackage;

@Entity
@Table(name = "teqip_package_pbgdetails")

public class TeqipPackagePBGDDetails extends AuditEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer pdgDetailId;
	String pbgDetails;
	String bankName;
	String branchName;
	Double pbgPercentage;

    public Double getPbgPercentage() {
        return pbgPercentage;
    }

    public void setPbgPercentage(Double pbgPercentage) {
        this.pbgPercentage = pbgPercentage;
    }

    public Double getPbgAmount() {
        return pbgAmount;
    }

    public void setPbgAmount(Double pbgAmount) {
        this.pbgAmount = pbgAmount;
    }
        Double pbgAmount;
	String remarks;
	Integer isPBGRequired;

	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "packageId")
	private TeqipPackage teqipPackage;
	

	public Integer getPdgDetailId() {
		return pdgDetailId;
	}


	public void setPdgDetailId(Integer pdgDetailId) {
		this.pdgDetailId = pdgDetailId;
	}


	public String getPbgDetails() {
		return pbgDetails;
	}


	public void setPbgDetails(String pbgDetails) {
		this.pbgDetails = pbgDetails;
	}


	public String getBankName() {
		return bankName;
	}


	public void setBankName(String bankName) {
		this.bankName = bankName;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public String getRemarks() {
		return remarks;
	}


	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	public Integer getIsPBGRequired() {
		return isPBGRequired;
	}


	public void setIsPBGRequired(Integer isPBGRequired) {
		this.isPBGRequired = isPBGRequired;
	}


	public TeqipPackage getTeqipPackage() {
		return teqipPackage;
	}


	public void setTeqipPackage(TeqipPackage teqipPackage) {
		this.teqipPackage = teqipPackage;
	}



}
