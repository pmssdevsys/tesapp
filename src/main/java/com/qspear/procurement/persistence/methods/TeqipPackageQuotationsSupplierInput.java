/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_quotationsupplierinput")
public class TeqipPackageQuotationsSupplierInput extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "package_quotationsupplierinputiD")
    private Integer packageQuotationSupplierInputId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "supplierid")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "questionId")
    private TeqipPackageQuestionMaster teqipPackageQuestionMaster;

    private String userResponse;
    private String reasonForNonResponsiveNess;

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public TeqipPackageQuestionMaster getTeqipPackageQuestionMaster() {
        return teqipPackageQuestionMaster;
    }

    public void setTeqipPackageQuestionMaster(TeqipPackageQuestionMaster teqipPackageQuestionMaster) {
        this.teqipPackageQuestionMaster = teqipPackageQuestionMaster;
    }

    public String getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(String userResponse) {
        this.userResponse = userResponse;
    }

    public String getReasonForNonResponsiveNess() {
        return reasonForNonResponsiveNess;
    }

    public void setReasonForNonResponsiveNess(String reasonForNonResponsiveNess) {
        this.reasonForNonResponsiveNess = reasonForNonResponsiveNess;
    }

    public Integer getPackageQuotationSupplierInputId() {
        return packageQuotationSupplierInputId;
    }

    public void setPackageQuotationSupplierInputId(Integer packageQuotationSupplierInputId) {
        this.packageQuotationSupplierInputId = packageQuotationSupplierInputId;
    }

    public TeqipPackageQuotationsSupplierInput clone() {
        TeqipPackageQuotationsSupplierInput input = new TeqipPackageQuotationsSupplierInput();
        input.setReasonForNonResponsiveNess(reasonForNonResponsiveNess);
        input.setTeqipPackage(teqipPackage);
        input.setTeqipPackageQuestionMaster(teqipPackageQuestionMaster);
        input.setTeqipPmssSupplierMaster(teqipPmssSupplierMaster);
        input.setUserResponse(userResponse);
        
        
        return input;

    }

}
