/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_bidopeningdata")
public class TeqipPackageBidOpeningData extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer bidopeningId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "bidderId")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    private Integer isbidReceived;
    private String isbidReceivedResponse;
    private Integer isbidWithdrawn;
    private String isbidWithdrawnResponse;
    private Integer isbidModified;
    private String isbidModifiedResponse;
    private Integer isbidDulySigned;
    private String isbidDulySignedResponse;
    private String bidNumber;
    private Date bidDate;
    private Integer bidValidity;
    private Integer bidSecurity;
    private Integer isbidSecure;
    private String isbidSecureResponse;
    private Integer isbidCostAttached;
    private String isbidCostAttachedResponse;
    private Double unconditionalDiscount;
    private String unconditionalDiscountResponse;
    private Double readOutPriceTaxes;
    private String readOutPriceTaxesResponse;
    private String bidNumberResponse;
    private String bidDateResponse;
    private String bidValidityResponse;
    private String bidSecurityResponse;

    public Integer getBidopeningId() {
        return bidopeningId;
    }

    public void setBidopeningId(Integer bidopeningId) {
        this.bidopeningId = bidopeningId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public Integer getIsbidReceived() {
        return isbidReceived;
    }

    public void setIsbidReceived(Integer isbidReceived) {
        this.isbidReceived = isbidReceived;
    }

    public String getIsbidReceivedResponse() {
        return isbidReceivedResponse;
    }

    public void setIsbidReceivedResponse(String isbidReceivedResponse) {
        this.isbidReceivedResponse = isbidReceivedResponse;
    }

    public Integer getIsbidWithdrawn() {
        return isbidWithdrawn;
    }

    public void setIsbidWithdrawn(Integer isbidWithdrawn) {
        this.isbidWithdrawn = isbidWithdrawn;
    }

    public String getIsbidWithdrawnResponse() {
        return isbidWithdrawnResponse;
    }

    public void setIsbidWithdrawnResponse(String isbidWithdrawnResponse) {
        this.isbidWithdrawnResponse = isbidWithdrawnResponse;
    }

    public Integer getIsbidModified() {
        return isbidModified;
    }

    public void setIsbidModified(Integer isbidModified) {
        this.isbidModified = isbidModified;
    }

    public String getIsbidModifiedResponse() {
        return isbidModifiedResponse;
    }

    public void setIsbidModifiedResponse(String isbidModifiedResponse) {
        this.isbidModifiedResponse = isbidModifiedResponse;
    }

    public Integer getIsbidDulySigned() {
        return isbidDulySigned;
    }

    public void setIsbidDulySigned(Integer isbidDulySigned) {
        this.isbidDulySigned = isbidDulySigned;
    }

    public String getIsbidDulySignedResponse() {
        return isbidDulySignedResponse;
    }

    public void setIsbidDulySignedResponse(String isbidDulySignedResponse) {
        this.isbidDulySignedResponse = isbidDulySignedResponse;
    }

    public String getBidNumber() {
        return bidNumber;
    }

    public void setBidNumber(String bidNumber) {
        this.bidNumber = bidNumber;
    }

    public Date getBidDate() {
        return bidDate;
    }

    public void setBidDate(Date bidDate) {
        this.bidDate = bidDate;
    }

    public Integer getBidValidity() {
        return bidValidity;
    }

    public void setBidValidity(Integer bidValidity) {
        this.bidValidity = bidValidity;
    }

    public Integer getBidSecurity() {
        return bidSecurity;
    }

    public void setBidSecurity(Integer bidSecurity) {
        this.bidSecurity = bidSecurity;
    }

    public Integer getIsbidSecure() {
        return isbidSecure;
    }

    public void setIsbidSecure(Integer isbidSecure) {
        this.isbidSecure = isbidSecure;
    }

    public String getIsbidSecureResponse() {
        return isbidSecureResponse;
    }

    public void setIsbidSecureResponse(String isbidSecureResponse) {
        this.isbidSecureResponse = isbidSecureResponse;
    }

    public Integer getIsbidCostAttached() {
        return isbidCostAttached;
    }

    public void setIsbidCostAttached(Integer isbidCostAttached) {
        this.isbidCostAttached = isbidCostAttached;
    }

    public String getIsbidCostAttachedResponse() {
        return isbidCostAttachedResponse;
    }

    public void setIsbidCostAttachedResponse(String isbidCostAttachedResponse) {
        this.isbidCostAttachedResponse = isbidCostAttachedResponse;
    }

    public Double getUnconditionalDiscount() {
        return unconditionalDiscount;
    }

    public void setUnconditionalDiscount(Double unconditionalDiscount) {
        this.unconditionalDiscount = unconditionalDiscount;
    }

    public String getUnconditionalDiscountResponse() {
        return unconditionalDiscountResponse;
    }

    public void setUnconditionalDiscountResponse(String unconditionalDiscountResponse) {
        this.unconditionalDiscountResponse = unconditionalDiscountResponse;
    }

    public Double getReadOutPriceTaxes() {
        return readOutPriceTaxes;
    }

    public void setReadOutPriceTaxes(Double readOutPriceTaxes) {
        this.readOutPriceTaxes = readOutPriceTaxes;
    }

    public String getReadOutPriceTaxesResponse() {
        return readOutPriceTaxesResponse;
    }

    public void setReadOutPriceTaxesResponse(String readOutPriceTaxesResponse) {
        this.readOutPriceTaxesResponse = readOutPriceTaxesResponse;
    }

    public String getBidNumberResponse() {
        return bidNumberResponse;
    }

    public void setBidNumberResponse(String bidNumberResponse) {
        this.bidNumberResponse = bidNumberResponse;
    }

    public String getBidDateResponse() {
        return bidDateResponse;
    }

    public void setBidDateResponse(String bidDateResponse) {
        this.bidDateResponse = bidDateResponse;
    }

    public String getBidValidityResponse() {
        return bidValidityResponse;
    }

    public void setBidValidityResponse(String bidValidityResponse) {
        this.bidValidityResponse = bidValidityResponse;
    }

    public String getBidSecurityResponse() {
        return bidSecurityResponse;
    }

    public void setBidSecurityResponse(String bidSecurityResponse) {
        this.bidSecurityResponse = bidSecurityResponse;
    }

}
