package com.qspear.procurement.persistence.methods;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.qspear.procurement.persistence.TeqipPackage;

@Entity
@Table(name = "teqip_package_emddetails")

public class TeqipPackageEMDDetails extends AuditEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer emdDetailId;
	String bankName;
	String branchName;
	Double emdPercentage;
        Double emdAmount;
        String bankAccountHolder;
        String accountNumber;
	String ifscCode;
        String remarks;
	Integer isEMDRequired;
        @ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "packageId")
	private TeqipPackage teqipPackage;

    public Integer getEmdDetailId() {
        return emdDetailId;
    }

    public void setEmdDetailId(Integer emdDetailId) {
        this.emdDetailId = emdDetailId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Double getEmdPercentage() {
        return emdPercentage;
    }

    public void setEmdPercentage(Double emdPercentage) {
        this.emdPercentage = emdPercentage;
    }

    public Double getEmdAmount() {
        return emdAmount;
    }

    public void setEmdAmount(Double emdAmount) {
        this.emdAmount = emdAmount;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBankAccountHolder() {
        return bankAccountHolder;
    }

    public void setBankAccountHolder(String bankAccountHolder) {
        this.bankAccountHolder = bankAccountHolder;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public Integer getIsEMDRequired() {
        return isEMDRequired;
    }

    public void setIsEMDRequired(Integer isEMDRequired) {
        this.isEMDRequired = isEMDRequired;
    }
    public TeqipPackage getTeqipPackage() {
		return teqipPackage;
	}


	public void setTeqipPackage(TeqipPackage teqipPackage) {
		this.teqipPackage = teqipPackage;
	}

}
