/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_amendmentdetail")
public class TeqipPackageAmendmentdetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "packageAmendmentDetailId")
    private Integer packageAmendmentDetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "PACKAGE_ID")
    private TeqipPackage teqipPackage;

    private Integer isAmendmentRequired;
    private Integer isAmmendmentComplete;
    private Date periodExpiryDate;
    private Double perfSecurityAmount;
    private Date workCompletionDate;
    private Date perfSecurityExpiryDate;

    public Integer getPackageAmendmentDetailId() {
        return packageAmendmentDetailId;
    }

    public void setPackageAmendmentDetailId(Integer packageAmendmentDetailId) {
        this.packageAmendmentDetailId = packageAmendmentDetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Integer getIsAmendmentRequired() {
        return isAmendmentRequired;
    }

    public void setIsAmendmentRequired(Integer isAmendmentRequired) {
        this.isAmendmentRequired = isAmendmentRequired;
    }

    public Date getPeriodExpiryDate() {
        return periodExpiryDate;
    }

    public void setPeriodExpiryDate(Date periodExpiryDate) {
        this.periodExpiryDate = periodExpiryDate;
    }

    public Double getPerfSecurityAmount() {
        return perfSecurityAmount;
    }

    public void setPerfSecurityAmount(Double perfSecurityAmount) {
        this.perfSecurityAmount = perfSecurityAmount;
    }

    public Integer getIsAmmendmentComplete() {
        return isAmmendmentComplete;
    }

    public void setIsAmmendmentComplete(Integer isAmmendmentComplete) {
        this.isAmmendmentComplete = isAmmendmentComplete;
    }

    public Date getWorkCompletionDate() {
        return workCompletionDate;
    }

    public void setWorkCompletionDate(Date workCompletionDate) {
        this.workCompletionDate = workCompletionDate;
    }

    public Date getPerfSecurityExpiryDate() {
        return perfSecurityExpiryDate;
    }

    public void setPerfSecurityExpiryDate(Date perfSecurityExpiryDate) {
        this.perfSecurityExpiryDate = perfSecurityExpiryDate;
    }

}
