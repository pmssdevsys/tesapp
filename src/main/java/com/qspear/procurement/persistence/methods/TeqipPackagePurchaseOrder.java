/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_purchaseorder")
public class TeqipPackagePurchaseOrder extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pack_purchaseorderid")
    private Integer packagePurchaseOrderId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "supplierid")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    @Column(name = "purchaseorderno")
    private String purchaseOrderNo;

//    @Column(name = "gstpercentage")
//    private Double gstPercentage;
    @Column(name = "octroiother")
    private Double octroiOther;

    @Column(name = "evaluatedcost")
    private Double evaluatedCost;

    private Double performanceSecurityAmount;

    private Date poDate;

    private Double totalBaseCost;
    private Double totalContractValue;
    Integer isPoFinalised;

    public Integer getIsPoFinalised() {
        return isPoFinalised;
    }

    public void setIsPoFinalised(Integer isPoFinalised) {
        this.isPoFinalised = isPoFinalised;
    }

    public Integer getPackagePurchaseOrderId() {
        return packagePurchaseOrderId;
    }

    public void setPackagePurchaseOrderId(Integer packagePurchaseOrderId) {
        this.packagePurchaseOrderId = packagePurchaseOrderId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public String getPurchaseOrderNo() {
        return purchaseOrderNo;
    }

    public void setPurchaseOrderNo(String purchaseOrderNo) {
        this.purchaseOrderNo = purchaseOrderNo;
    }

//    public Double getGstPercentage() {
//        return gstPercentage;
//    }
//
//    public void setGstPercentage(Double gstPercentage) {
//        this.gstPercentage = gstPercentage;
//    }
    public Double getOctroiOther() {
        return octroiOther;
    }

    public void setOctroiOther(Double octroiOther) {
        this.octroiOther = octroiOther;
    }

    public Double getEvaluatedCost() {
        return evaluatedCost;
    }

    public void setEvaluatedCost(Double evaluatedCost) {
        this.evaluatedCost = evaluatedCost;
    }

    public Double getPerformanceSecurityAmount() {
        return performanceSecurityAmount;
    }

    public void setPerformanceSecurityAmount(Double performanceSecurityAmount) {
        this.performanceSecurityAmount = performanceSecurityAmount;
    }

    public Date getPoDate() {
        return poDate;
    }

    public void setPoDate(Date poDate) {
        this.poDate = poDate;
    }

    public Double getTotalBaseCost() {
        return totalBaseCost;
    }

    public void setTotalBaseCost(Double totalBaseCost) {
        this.totalBaseCost = totalBaseCost;
    }

    public Double getTotalContractValue() {
        return totalContractValue;
    }

    public void setTotalContractValue(Double totalContractValue) {
        this.totalContractValue = totalContractValue;
    }

}
