/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_rfpdocument")
public class TeqipPackageRFPDocument extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer rfpDocumentId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    private Double minTechnicalPassingScore;
    private Double weightageForTechnicalProposal;
    private Double weightageForFinancialProposal;
    private String referenceNo;
    private Integer paragraph1_2;
    private String paragraph1_3;
    private String paragraph1_4;
    private Integer paragraph1_14;
    private Date paragraph1_14Date;
    private Date paragraph2_1;
    private String paragraph2_1Address;
    private Integer paragraph3_3;
    private String paragraph3_4;
    private Integer paragraph3_4IsTraining;
    private String paragraph3_4Information;
    private Integer paragraph3_6;
    private Integer paragraph4_3;
    private String paragraph4_5;
    private Date paragraph4_5Date;
    private Date paragraph4_5DateTime;
    private Date paragraph6_1Date;
    private String paragraph6_1Address;
    private Date paragraph7_1;
    private String paragraph7_1Address;

    public Integer getRfpDocumentId() {
        return rfpDocumentId;
    }

    public void setRfpDocumentId(Integer rfpDocumentId) {
        this.rfpDocumentId = rfpDocumentId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Double getMinTechnicalPassingScore() {
        return minTechnicalPassingScore;
    }

    public void setMinTechnicalPassingScore(Double minTechnicalPassingScore) {
        this.minTechnicalPassingScore = minTechnicalPassingScore;
    }

    public Double getWeightageForTechnicalProposal() {
        return weightageForTechnicalProposal;
    }

    public void setWeightageForTechnicalProposal(Double weightageForTechnicalProposal) {
        this.weightageForTechnicalProposal = weightageForTechnicalProposal;
    }

    public Double getWeightageForFinancialProposal() {
        return weightageForFinancialProposal;
    }

    public void setWeightageForFinancialProposal(Double weightageForFinancialProposal) {
        this.weightageForFinancialProposal = weightageForFinancialProposal;
    }

    public Integer getParagraph1_2() {
        return paragraph1_2;
    }

    public void setParagraph1_2(Integer paragraph1_2) {
        this.paragraph1_2 = paragraph1_2;
    }

    public String getParagraph1_3() {
        return paragraph1_3;
    }

    public void setParagraph1_3(String paragraph1_3) {
        this.paragraph1_3 = paragraph1_3;
    }

    public String getParagraph1_4() {
        return paragraph1_4;
    }

    public void setParagraph1_4(String paragraph1_4) {
        this.paragraph1_4 = paragraph1_4;
    }

    public Integer getParagraph1_14() {
        return paragraph1_14;
    }

    public void setParagraph1_14(Integer paragraph1_14) {
        this.paragraph1_14 = paragraph1_14;
    }

    public Date getParagraph1_14Date() {
        return paragraph1_14Date;
    }

    public void setParagraph1_14Date(Date paragraph1_14Date) {
        this.paragraph1_14Date = paragraph1_14Date;
    }

    public Date getParagraph2_1() {
        return paragraph2_1;
    }

    public void setParagraph2_1(Date paragraph2_1) {
        this.paragraph2_1 = paragraph2_1;
    }

    public String getParagraph2_1Address() {
        return paragraph2_1Address;
    }

    public void setParagraph2_1Address(String paragraph2_1Address) {
        this.paragraph2_1Address = paragraph2_1Address;
    }

    public Integer getParagraph3_3() {
        return paragraph3_3;
    }

    public void setParagraph3_3(Integer paragraph3_3) {
        this.paragraph3_3 = paragraph3_3;
    }

    public String getParagraph3_4() {
        return paragraph3_4;
    }

    public void setParagraph3_4(String paragraph3_4) {
        this.paragraph3_4 = paragraph3_4;
    }

    public Integer getParagraph3_4IsTraining() {
        return paragraph3_4IsTraining;
    }

    public void setParagraph3_4IsTraining(Integer paragraph3_4IsTraining) {
        this.paragraph3_4IsTraining = paragraph3_4IsTraining;
    }

    public String getParagraph3_4Information() {
        return paragraph3_4Information;
    }

    public void setParagraph3_4Information(String paragraph3_4Information) {
        this.paragraph3_4Information = paragraph3_4Information;
    }

    public Integer getParagraph3_6() {
        return paragraph3_6;
    }

    public void setParagraph3_6(Integer paragraph3_6) {
        this.paragraph3_6 = paragraph3_6;
    }

    public Integer getParagraph4_3() {
        return paragraph4_3;
    }

    public void setParagraph4_3(Integer paragraph4_3) {
        this.paragraph4_3 = paragraph4_3;
    }

    public String getParagraph4_5() {
        return paragraph4_5;
    }

    public void setParagraph4_5(String paragraph4_5) {
        this.paragraph4_5 = paragraph4_5;
    }

    public Date getParagraph4_5Date() {
        return paragraph4_5Date;
    }

    public void setParagraph4_5Date(Date paragraph4_5Date) {
        this.paragraph4_5Date = paragraph4_5Date;
    }

    public Date getParagraph4_5DateTime() {
        return paragraph4_5DateTime;
    }

    public void setParagraph4_5DateTime(Date paragraph4_5DateTime) {
        this.paragraph4_5DateTime = paragraph4_5DateTime;
    }

    public Date getParagraph6_1Date() {
        return paragraph6_1Date;
    }

    public void setParagraph6_1Date(Date paragraph6_1Date) {
        this.paragraph6_1Date = paragraph6_1Date;
    }

    public String getParagraph6_1Address() {
        return paragraph6_1Address;
    }

    public void setParagraph6_1Address(String paragraph6_1Address) {
        this.paragraph6_1Address = paragraph6_1Address;
    }

    public Date getParagraph7_1() {
        return paragraph7_1;
    }

    public void setParagraph7_1(Date paragraph7_1) {
        this.paragraph7_1 = paragraph7_1;
    }

    public String getParagraph7_1Address() {
        return paragraph7_1Address;
    }

    public void setParagraph7_1Address(String paragraph7_1Address) {
        this.paragraph7_1Address = paragraph7_1Address;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

}
