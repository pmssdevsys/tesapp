/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_issuerfp")
public class TeqipPackageIssueRFP extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer issueOfRfpId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;
    private Date actualPreProposalDate;
    private String preProposalMinutes;
    private Integer clarificationIssuedToConsultants;
    private Date clarificationIssuedDate;
    private String clarificationDetails;
    
    private Date currentSubmissionDate;
    private String reasonOfExtension;
    private Date extendedDate;
    
    Date techProposalOpeningDate;
    Date plannedFinancialOpeningDate;
    Date plannedFinancialOpeningDateTime;
    
    Date financialOpeningDate;
    Date financialOpeningDateTime;
    Date contractSignDate;

    public Integer getIssueOfRfpId() {
        return issueOfRfpId;
    }

    public void setIssueOfRfpId(Integer issueOfRfpId) {
        this.issueOfRfpId = issueOfRfpId;
    }

   
    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Integer getClarificationIssuedToConsultants() {
        return clarificationIssuedToConsultants;
    }

    public void setClarificationIssuedToConsultants(Integer clarificationIssuedToConsultants) {
        this.clarificationIssuedToConsultants = clarificationIssuedToConsultants;
    }

    public Date getActualPreProposalDate() {
        return actualPreProposalDate;
    }

    public void setActualPreProposalDate(Date actualPreProposalDate) {
        this.actualPreProposalDate = actualPreProposalDate;
    }

    public String getPreProposalMinutes() {
        return preProposalMinutes;
    }

    public void setPreProposalMinutes(String preProposalMinutes) {
        this.preProposalMinutes = preProposalMinutes;
    }

    public Date getClarificationIssuedDate() {
        return clarificationIssuedDate;
    }

    public void setClarificationIssuedDate(Date clarificationIssuedDate) {
        this.clarificationIssuedDate = clarificationIssuedDate;
    }

    public String getClarificationDetails() {
        return clarificationDetails;
    }

    public void setClarificationDetails(String clarificationDetails) {
        this.clarificationDetails = clarificationDetails;
    }

    public Date getCurrentSubmissionDate() {
        return currentSubmissionDate;
    }

    public void setCurrentSubmissionDate(Date currentSubmissionDate) {
        this.currentSubmissionDate = currentSubmissionDate;
    }

    public String getReasonOfExtension() {
        return reasonOfExtension;
    }

    public void setReasonOfExtension(String reasonOfExtension) {
        this.reasonOfExtension = reasonOfExtension;
    }

    public Date getExtendedDate() {
        return extendedDate;
    }

    public void setExtendedDate(Date extendedDate) {
        this.extendedDate = extendedDate;
    }

    public Date getTechProposalOpeningDate() {
        return techProposalOpeningDate;
    }

    public void setTechProposalOpeningDate(Date techProposalOpeningDate) {
        this.techProposalOpeningDate = techProposalOpeningDate;
    }

    public Date getPlannedFinancialOpeningDate() {
        return plannedFinancialOpeningDate;
    }

    public void setPlannedFinancialOpeningDate(Date plannedFinancialOpeningDate) {
        this.plannedFinancialOpeningDate = plannedFinancialOpeningDate;
    }

    public Date getPlannedFinancialOpeningDateTime() {
        return plannedFinancialOpeningDateTime;
    }

    public void setPlannedFinancialOpeningDateTime(Date plannedFinancialOpeningDateTime) {
        this.plannedFinancialOpeningDateTime = plannedFinancialOpeningDateTime;
    }

    public Date getFinancialOpeningDate() {
        return financialOpeningDate;
    }

    public void setFinancialOpeningDate(Date financialOpeningDate) {
        this.financialOpeningDate = financialOpeningDate;
    }

    public Date getFinancialOpeningDateTime() {
        return financialOpeningDateTime;
    }

    public void setFinancialOpeningDateTime(Date financialOpeningDateTime) {
        this.financialOpeningDateTime = financialOpeningDateTime;
    }

    public Date getContractSignDate() {
        return contractSignDate;
    }

    public void setContractSignDate(Date contractSignDate) {
        this.contractSignDate = contractSignDate;
    }

}
