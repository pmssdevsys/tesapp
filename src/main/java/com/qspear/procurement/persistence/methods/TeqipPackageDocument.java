/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSBudgetException;
import com.qspear.procurement.persistence.TeqipPackage;
import java.io.File;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_documentdetails")
public class TeqipPackageDocument extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "package_documentid")
    private Integer packageDocumentId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @Column(name = "documentcategory")
    private String documentCategory;

    @Column(name = "documentfilename")
    private String documentfilename;

    private String originalFileName;

    @Column(name = "description")
    private String description;

    public Integer getPackageDocumentId() {
        return packageDocumentId;
    }

    public void setPackageDocumentId(Integer packageDocumentId) {
        this.packageDocumentId = packageDocumentId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public String getDocumentCategory() {
        return documentCategory;
    }

    public void setDocumentCategory(String documentCategory) {
        this.documentCategory = documentCategory;
    }

    public String getDocumentfilename() {
        return documentfilename;
    }

    public void setDocumentfilename(String documentfilename) {
        this.documentfilename = documentfilename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public File getFileLocation() {
        File file = new File(MethodConstants.UPLOAD_LOCATION + File.separator
//                + this.getDocumentCategory() + File.separator
                + this.getDocumentfilename()).getAbsoluteFile();

        System.out.println(file);
        if (!file.exists()) {
            throw new PMSSBudgetException("Sorry. The file you are looking for does not exist.",
                    new ErrorCode("FILE_NOT_FOUND", "Sorry. The file you are looking for does not exist."));

        }
        return file;
    }

}
