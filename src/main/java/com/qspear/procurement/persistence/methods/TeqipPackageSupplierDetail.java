/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_supplierdetail")
public class TeqipPackageSupplierDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pack_supplierdetailid")
    private Integer packageSupplierDetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "supplierid")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    Integer isShortListed;
    String comments;

    String supplierRegistrationNumber;

    Integer isQuotationSent;
    Integer isEmailSent;

    String quotationNumber;
    Date quotationDate;
    Double quotedPrice;

    @ManyToOne(cascade = CascadeType.DETACH)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "invitation_documentid")
    TeqipPackageDocument invitationDocument;

    @ManyToOne(cascade = CascadeType.DETACH)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "invitation_financial_documentid")
    TeqipPackageDocument invitationFinancialDocument;

    @ManyToOne(cascade = CascadeType.DETACH)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "quotation_documentid")
    TeqipPackageDocument quotationDocument;

    private Integer isFormResponsive;
    private String isFromResponsiveSource;

    public Integer getPackageSupplierDetailId() {
        return packageSupplierDetailId;
    }

    public void setPackageSupplierDetailId(Integer packageSupplierDetailId) {
        this.packageSupplierDetailId = packageSupplierDetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public Integer getIsShortListed() {
        return isShortListed;
    }

    public void setIsShortListed(Integer isShortListed) {
        this.isShortListed = isShortListed;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getSupplierRegistrationNumber() {
        return supplierRegistrationNumber;
    }

    public void setSupplierRegistrationNumber(String supplierRegistrationNumber) {
        this.supplierRegistrationNumber = supplierRegistrationNumber;
    }

    public Integer getIsQuotationSent() {
        return isQuotationSent;
    }

    public void setIsQuotationSent(Integer isQuotationSent) {
        this.isQuotationSent = isQuotationSent;
    }

    public Integer getIsEmailSent() {
        return isEmailSent;
    }

    public void setIsEmailSent(Integer isEmailSent) {
        this.isEmailSent = isEmailSent;
    }

    public TeqipPackageDocument getInvitationDocument() {
        return invitationDocument;
    }

    public void setInvitationDocument(TeqipPackageDocument invitationDocument) {
        this.invitationDocument = invitationDocument;
    }

    public TeqipPackageDocument getQuotationDocument() {
        return quotationDocument;
    }

    public void setQuotationDocument(TeqipPackageDocument quotationDocument) {
        this.quotationDocument = quotationDocument;
    }

    public String getQuotationNumber() {
        return quotationNumber;
    }

    public void setQuotationNumber(String quotationNumber) {
        this.quotationNumber = quotationNumber;
    }

    public Date getQuotationDate() {
        return quotationDate;
    }

    public void setQuotationDate(Date quotationDate) {
        this.quotationDate = quotationDate;
    }

    public Double getQuotedPrice() {
        return quotedPrice;
    }

    public void setQuotedPrice(Double quotedPrice) {
        this.quotedPrice = quotedPrice;
    }

    public Integer getIsFormResponsive() {
        return isFormResponsive;
    }

    public void setIsFormResponsive(Integer isFormResponsive) {
        this.isFormResponsive = isFormResponsive;
    }

    public String getIsFromResponsiveSource() {
        return isFromResponsiveSource;
    }

    public void setIsFromResponsiveSource(String isFromResponsiveSource) {
        this.isFromResponsiveSource = isFromResponsiveSource;
    }

    public TeqipPackageDocument getInvitationFinancialDocument() {
        return invitationFinancialDocument;
    }

    public void setInvitationFinancialDocument(TeqipPackageDocument invitationFinancialDocument) {
        this.invitationFinancialDocument = invitationFinancialDocument;
    }

}
