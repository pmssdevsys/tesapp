/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_pmss_suppliermaster")
public class TeqipPmssSupplierMaster extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "supplierID")
    private Integer supplierId;

    @Column(name = "suppliername")
    private String supplierName;

    @Column(name = "supllieraddress")
    private String supllierAddress;

    @Column(name = "suppliercity")
    private String supplierCity;

    @Column(name = "supplierstate")
    private String supplierState;

    @Column(name = "suppliercountry")
    private String supplierCountry;

    @Column(name = "suppliernationality")
    private String supplierNationality;

    @Column(name = "suppliersource")
    private String supplierSource;

    @Column(name = "supplierspecificsource")
    private String supplierSpecificSource;

    @Column(name = "supplierrepresentativename")
    private String supplierRepresentativeName;

    @Column(name = "supplierpincode")
    private String supplierPincode;

    @Column(name = "supplierphoneno")
    private String supplierPhoneno;

    @Column(name = "supliertannumber")
    private String suplierTanNumber;

    @Column(name = "suplierfaxnumber")
    private String suplierFaxNumber;

    @Column(name = "suplierpannumber")
    private String suplierPanNumber;

//    @Column(name = "supliergstnumber")
//    private String suplierGstNumber;
    @Column(name = "supliertaxnumber")
    private String suplierTaxNumber;

    @Column(name = "supliereletter")
    private String supliereLetter;

    @Column(name = "suplieremailid")
    private String suplierEmailId;

    @Column(name = "pmss_type")
    private Integer pmssType;

    private String gstNo;
    private String remarks;
    String epfNo;
    String esiNo;
    String contractLabourLicenseNo;

    public String getEpfNo() {
        return epfNo;
    }

    public void setEpfNo(String epfNo) {
        this.epfNo = epfNo;
    }

    public String getEsiNo() {
        return esiNo;
    }

    public void setEsiNo(String esiNo) {
        this.esiNo = esiNo;
    }

    public String getContractLabourLicenseNo() {
        return contractLabourLicenseNo;
    }

    public void setContractLabourLicenseNo(String contractLabourLicenseNo) {
        this.contractLabourLicenseNo = contractLabourLicenseNo;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupllierAddress() {
        return supllierAddress;
    }

    public void setSupllierAddress(String supllierAddress) {
        this.supllierAddress = supllierAddress;
    }

    public String getSupplierCity() {
        return supplierCity;
    }

    public void setSupplierCity(String supplierCity) {
        this.supplierCity = supplierCity;
    }

    public String getSupplierState() {
        return supplierState;
    }

    public void setSupplierState(String supplierState) {
        this.supplierState = supplierState;
    }

    public String getSupplierCountry() {
        return supplierCountry;
    }

    public void setSupplierCountry(String supplierCountry) {
        this.supplierCountry = supplierCountry;
    }

    public String getSupplierNationality() {
        return supplierNationality;
    }

    public void setSupplierNationality(String supplierNationality) {
        this.supplierNationality = supplierNationality;
    }

    public String getSupplierSource() {
        return supplierSource;
    }

    public void setSupplierSource(String supplierSource) {
        this.supplierSource = supplierSource;
    }

    public String getSupplierSpecificSource() {
        return supplierSpecificSource;
    }

    public void setSupplierSpecificSource(String supplierSpecificSource) {
        this.supplierSpecificSource = supplierSpecificSource;
    }

    public String getSupplierRepresentativeName() {
        return supplierRepresentativeName;
    }

    public void setSupplierRepresentativeName(String supplierRepresentativeName) {
        this.supplierRepresentativeName = supplierRepresentativeName;
    }

    public String getSupplierPincode() {
        return supplierPincode;
    }

    public void setSupplierPincode(String supplierPincode) {
        this.supplierPincode = supplierPincode;
    }

    public String getSupplierPhoneno() {
        return supplierPhoneno;
    }

    public void setSupplierPhoneno(String supplierPhoneno) {
        this.supplierPhoneno = supplierPhoneno;
    }

    public String getSuplierTanNumber() {
        return suplierTanNumber;
    }

    public void setSuplierTanNumber(String suplierTanNumber) {
        this.suplierTanNumber = suplierTanNumber;
    }

    public String getSuplierFaxNumber() {
        return suplierFaxNumber;
    }

    public void setSuplierFaxNumber(String suplierFaxNumber) {
        this.suplierFaxNumber = suplierFaxNumber;
    }

    public String getSuplierPanNumber() {
        return suplierPanNumber;
    }

    public void setSuplierPanNumber(String suplierPanNumber) {
        this.suplierPanNumber = suplierPanNumber;
    }

//    public String getSuplierGstNumber() {
//        return suplierGstNumber;
//    }
//
//    public void setSuplierGstNumber(String suplierGstNumber) {
//        this.suplierGstNumber = suplierGstNumber;
//    }
    public String getSuplierTaxNumber() {
        return suplierTaxNumber;
    }

    public void setSuplierTaxNumber(String suplierTaxNumber) {
        this.suplierTaxNumber = suplierTaxNumber;
    }

    public String getSupliereLetter() {
        return supliereLetter;
    }

    public void setSupliereLetter(String supliereLetter) {
        this.supliereLetter = supliereLetter;
    }

    public String getSuplierEmailId() {
        return suplierEmailId;
    }

    public void setSuplierEmailId(String suplierEmailId) {
        this.suplierEmailId = suplierEmailId;
    }

    public Integer getPmssType() {
        return pmssType;
    }

    public void setPmssType(Integer pmssType) {
        this.pmssType = pmssType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }



}
