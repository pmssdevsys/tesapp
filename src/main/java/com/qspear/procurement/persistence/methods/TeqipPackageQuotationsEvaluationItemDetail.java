/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipPackage;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_quotationevaluationitemdetails")
public class TeqipPackageQuotationsEvaluationItemDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer quotationItemDetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "bidderId")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    @ManyToOne
    @JoinColumn(name = "itemId")
    private TeqipItemMaster teqipItemMaster;

    private Double itemQty;
    private Double basicCost;
    private Double totalPrice;
    private String comment;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public TeqipItemMaster getTeqipItemMaster() {
        return teqipItemMaster;
    }

    public void setTeqipItemMaster(TeqipItemMaster teqipItemMaster) {
        this.teqipItemMaster = teqipItemMaster;
    }

    public Double getItemQty() {
        return itemQty;
    }

    public void setItemQty(Double itemQty) {
        this.itemQty = itemQty;
    }

    public Double getBasicCost() {
        return basicCost;
    }

    public void setBasicCost(Double basicCost) {
        this.basicCost = basicCost;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getQuotationItemDetailId() {
        return quotationItemDetailId;
    }

    public void setQuotationItemDetailId(Integer quotationItemDetailId) {
        this.quotationItemDetailId = quotationItemDetailId;
    }

}
