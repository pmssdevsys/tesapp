/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_consultantevaluation")
public class TeqipPackageConsultantEvaluation extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer consultantEvaluationId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "supplierid")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "techsubcriteriaId")
    TeqipPackageTechSubCriteria teqipPackageTechSubCriteria;

    Double marks;

    public Integer getConsultantEvaluationId() {
        return consultantEvaluationId;
    }

    public void setConsultantEvaluationId(Integer consultantEvaluationId) {
        this.consultantEvaluationId = consultantEvaluationId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public TeqipPackageTechSubCriteria getTeqipPackageTechSubCriteria() {
        return teqipPackageTechSubCriteria;
    }

    public void setTeqipPackageTechSubCriteria(TeqipPackageTechSubCriteria teqipPackageTechSubCriteria) {
        this.teqipPackageTechSubCriteria = teqipPackageTechSubCriteria;
    }

    public Double getMarks() {
        return marks;
    }

    public void setMarks(Double marks) {
        this.marks = marks;
    }

}
