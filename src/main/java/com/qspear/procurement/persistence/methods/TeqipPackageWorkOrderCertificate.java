package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the TEQIP_ITEM_WORK_DETAIL database table.
 *
 */
@Entity
@Table(name = "teqip_package_workordercertificate")
public class TeqipPackageWorkOrderCertificate extends AuditEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "package_wowccid")
    private Integer packageWoWccId;

    @ManyToOne
    @JoinColumn(name = "PACKAGE_ID")
    private TeqipPackage teqipPackage;

    @Column(name = "wccactualcompletiondate")
    private Date actualCompletionDate;

    @Column(name = "wccworkdone")
    private String workDone;

    @Column(name = "wccbalancework")
    private String balanceWork;

    private String comment;
    
    private String woNumber;

    public Integer getPackageWoWccId() {
        return packageWoWccId;
    }

    public void setPackageWoWccId(Integer packageWoWccId) {
        this.packageWoWccId = packageWoWccId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Date getActualCompletionDate() {
        return actualCompletionDate;
    }

    public void setActualCompletionDate(Date actualCompletionDate) {
        this.actualCompletionDate = actualCompletionDate;
    }

    public String getWorkDone() {
        return workDone;
    }

    public void setWorkDone(String workDone) {
        this.workDone = workDone;
    }

    public String getBalanceWork() {
        return balanceWork;
    }

    public void setBalanceWork(String balanceWork) {
        this.balanceWork = balanceWork;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getWoNumber() {
        return woNumber;
    }

    public void setWoNumber(String woNumber) {
        this.woNumber = woNumber;
    }
    
    
}
