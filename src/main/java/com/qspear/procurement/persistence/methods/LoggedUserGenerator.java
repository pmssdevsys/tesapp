/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.security.util.LoggedUser;
import org.hibernate.tuple.ValueGenerator;

/**
 *
 * @author jaspreet
 */
public class LoggedUserGenerator implements ValueGenerator<Integer> {

    @Override
    public Integer generateValue(org.hibernate.Session session, Object owner) {
        return LoggedUser.get().getUserid();

    }
}
