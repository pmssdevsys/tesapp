/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipPackage;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_grnitemdetails")
public class TeqipPackageGRNItemDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pack_grnitemdetaiid")
    private Integer packGrnItemDetailId;

    @Column(name = "itemcode")
    private String itemCode;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "ITEM_ID")
    private TeqipItemMaster teqipItemMaster;

    @Column(name = "itemcomment")
    private String itemComment;

    @Column(name = "receivedqty")
    private Double receivedQty;

    @Column(name = "stockregistorname")
    private String stockRegistorName;

    @Column(name = "pageno")
    private String pageNumber;

    @Column(name = "serialno")
    private String serialNumber;

    private Double gst;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "pack_grnidetailId")
    private TeqipPackageGRNDetail teqipPackageGRNDetails;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @OneToMany(mappedBy = "teqipPackageGRNItemDetails", cascade = CascadeType.ALL)
    private List<TeqipPackageGRNItemDistribution> teqipPackageGRNItemDistributions;

    public Integer getPackGrnItemDetailId() {
        return packGrnItemDetailId;
    }

    public void setPackGrnItemDetailId(Integer packGrnItemDetailId) {
        this.packGrnItemDetailId = packGrnItemDetailId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemComment() {
        return itemComment;
    }

    public void setItemComment(String itemComment) {
        this.itemComment = itemComment;
    }

    public Double getReceivedQty() {
        return receivedQty;
    }

    public void setReceivedQty(Double receivedQty) {
        this.receivedQty = receivedQty;
    }

    public String getStockRegistorName() {
        return stockRegistorName;
    }

    public void setStockRegistorName(String stockRegistorName) {
        this.stockRegistorName = stockRegistorName;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public TeqipPackageGRNDetail getTeqipPackageGRNDetails() {
        return teqipPackageGRNDetails;
    }

    public void setTeqipPackageGRNDetails(TeqipPackageGRNDetail teqipPackageGRNDetails) {
        this.teqipPackageGRNDetails = teqipPackageGRNDetails;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public List<TeqipPackageGRNItemDistribution> getTeqipPackageGRNItemDistributions() {
        return teqipPackageGRNItemDistributions;
    }

    public void setTeqipPackageGRNItemDistributions(List<TeqipPackageGRNItemDistribution> teqipPackageGRNItemDistributions) {
        this.teqipPackageGRNItemDistributions = teqipPackageGRNItemDistributions;
    }

    public TeqipItemMaster getTeqipItemMaster() {
        return teqipItemMaster;
    }

    public void setTeqipItemMaster(TeqipItemMaster teqipItemMaster) {
        this.teqipItemMaster = teqipItemMaster;
    }

    public Double getGst() {
        return gst;
    }

    public void setGst(Double gst) {
        this.gst = gst;
    }

}
