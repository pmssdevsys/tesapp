/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_pocontractgenereration")
public class TeqipPackagePoContractGenereration extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer contractGenerationId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "bidderId")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    private Date loageneratedDate;
    private String poNumber;
    private Date performanceSecurityDate;
    private Date contractDocsignedDate;
    private Date contractStartDate;
    private Date expectedDeliveryDate;
    private Double perfSecurityAmount;
    private Integer arbitratorAgreed;
    private String perfSecurityInstrument;
    private Date perfSecurityExpiryDate;
    private Double vatPercentage;
    private Double otherOctroi;
    private Double evaluatedPrice;
    private Double totalBaseCost;

    Integer isMobilizationAdvancePaid;
    Integer isBankSecurityGiven;
    Double mobilizationAmount;
    Double sumOfAllApplicableTaxes;
    Double totalContractValue;
    Date workStartDate;
    Date workCompletionDate;
    Date maintainencePeriodExpiryDate;

    public Date getMaintainencePeriodExpiryDate() {
        return maintainencePeriodExpiryDate;
    }

    public void setMaintainencePeriodExpiryDate(Date maintainencePeriodExpiryDate) {
        this.maintainencePeriodExpiryDate = maintainencePeriodExpiryDate;
    }

    public Integer getContractGenerationId() {
        return contractGenerationId;
    }

    public void setContractGenerationId(Integer contractGenerationId) {
        this.contractGenerationId = contractGenerationId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public Date getLoageneratedDate() {
        return loageneratedDate;
    }

    public void setLoageneratedDate(Date loageneratedDate) {
        this.loageneratedDate = loageneratedDate;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public Date getPerformanceSecurityDate() {
        return performanceSecurityDate;
    }

    public void setPerformanceSecurityDate(Date performanceSecurityDate) {
        this.performanceSecurityDate = performanceSecurityDate;
    }

    public Date getContractDocsignedDate() {
        return contractDocsignedDate;
    }

    public void setContractDocsignedDate(Date contractDocsignedDate) {
        this.contractDocsignedDate = contractDocsignedDate;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public Double getPerfSecurityAmount() {
        return perfSecurityAmount;
    }

    public void setPerfSecurityAmount(Double perfSecurityAmount) {
        this.perfSecurityAmount = perfSecurityAmount;
    }

    public Integer getArbitratorAgreed() {
        return arbitratorAgreed;
    }

    public void setArbitratorAgreed(Integer arbitratorAgreed) {
        this.arbitratorAgreed = arbitratorAgreed;
    }

    public String getPerfSecurityInstrument() {
        return perfSecurityInstrument;
    }

    public void setPerfSecurityInstrument(String perfSecurityInstrument) {
        this.perfSecurityInstrument = perfSecurityInstrument;
    }

    public Date getPerfSecurityExpiryDate() {
        return perfSecurityExpiryDate;
    }

    public void setPerfSecurityExpiryDate(Date perfSecurityExpiryDate) {
        this.perfSecurityExpiryDate = perfSecurityExpiryDate;
    }

    public Double getVatPercentage() {
        return vatPercentage;
    }

    public void setVatPercentage(Double vatPercentage) {
        this.vatPercentage = vatPercentage;
    }

    public Double getOtherOctroi() {
        return otherOctroi;
    }

    public void setOtherOctroi(Double otherOctroi) {
        this.otherOctroi = otherOctroi;
    }

    public Double getEvaluatedPrice() {
        return evaluatedPrice;
    }

    public void setEvaluatedPrice(Double evaluatedPrice) {
        this.evaluatedPrice = evaluatedPrice;
    }

    public Double getTotalBaseCost() {
        return totalBaseCost;
    }

    public void setTotalBaseCost(Double totalBaseCost) {
        this.totalBaseCost = totalBaseCost;
    }

    public Integer getIsMobilizationAdvancePaid() {
        return isMobilizationAdvancePaid;
    }

    public void setIsMobilizationAdvancePaid(Integer isMobilizationAdvancePaid) {
        this.isMobilizationAdvancePaid = isMobilizationAdvancePaid;
    }

    public Integer getIsBankSecurityGiven() {
        return isBankSecurityGiven;
    }

    public void setIsBankSecurityGiven(Integer isBankSecurityGiven) {
        this.isBankSecurityGiven = isBankSecurityGiven;
    }

    public Double getMobilizationAmount() {
        return mobilizationAmount;
    }

    public void setMobilizationAmount(Double mobilizationAmount) {
        this.mobilizationAmount = mobilizationAmount;
    }

    public Double getSumOfAllApplicableTaxes() {
        return sumOfAllApplicableTaxes;
    }

    public void setSumOfAllApplicableTaxes(Double sumOfAllApplicableTaxes) {
        this.sumOfAllApplicableTaxes = sumOfAllApplicableTaxes;
    }

    public Double getTotalContractValue() {
        return totalContractValue;
    }

    public void setTotalContractValue(Double totalContractValue) {
        this.totalContractValue = totalContractValue;
    }

    public Date getWorkStartDate() {
        return workStartDate;
    }

    public void setWorkStartDate(Date workStartDate) {
        this.workStartDate = workStartDate;
    }

    public Date getWorkCompletionDate() {
        return workCompletionDate;
    }

    public void setWorkCompletionDate(Date workCompletionDate) {
        this.workCompletionDate = workCompletionDate;
    }

}
