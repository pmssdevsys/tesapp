/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_bidmeetingdetail")
public class TeqipPackageBidMeetingDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer bidmeetingDetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    private Date actualprebidMeetingDate;
    private Integer anyCorrigendum;
    private Integer isclarificationIssues;
    private Date clarificationprebidMeetingDate;
    private Date prebidMinutes;
    private String prebidMom;
    private Date currentSubmissionDate;
    private Date extendedDate;
    private String reasonforExtension;
    private Date meetingClarificationDate;

    public Date getMeetingClarificationDate() {
        return meetingClarificationDate;
    }

    public void setMeetingClarificationDate(Date meetingClarificationDate) {
        this.meetingClarificationDate = meetingClarificationDate;
    }

    public Integer getBidmeetingDetailId() {
        return bidmeetingDetailId;
    }

    public void setBidmeetingDetailId(Integer bidmeetingDetailId) {
        this.bidmeetingDetailId = bidmeetingDetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Date getActualprebidMeetingDate() {
        return actualprebidMeetingDate;
    }

    public void setActualprebidMeetingDate(Date actualprebidMeetingDate) {
        this.actualprebidMeetingDate = actualprebidMeetingDate;
    }

    public Integer getAnyCorrigendum() {
        return anyCorrigendum;
    }

    public void setAnyCorrigendum(Integer anyCorrigendum) {
        this.anyCorrigendum = anyCorrigendum;
    }

    public Integer getIsclarificationIssues() {
        return isclarificationIssues;
    }

    public void setIsclarificationIssues(Integer isclarificationIssues) {
        this.isclarificationIssues = isclarificationIssues;
    }

    public Date getClarificationprebidMeetingDate() {
        return clarificationprebidMeetingDate;
    }

    public void setClarificationprebidMeetingDate(Date clarificationprebidMeetingDate) {
        this.clarificationprebidMeetingDate = clarificationprebidMeetingDate;
    }

    public Date getPrebidMinutes() {
        return prebidMinutes;
    }

    public void setPrebidMinutes(Date prebidMinutes) {
        this.prebidMinutes = prebidMinutes;
    }

    public String getPrebidMom() {
        return prebidMom;
    }

    public void setPrebidMom(String prebidMom) {
        this.prebidMom = prebidMom;
    }

    public Date getCurrentSubmissionDate() {
        return currentSubmissionDate;
    }

    public void setCurrentSubmissionDate(Date currentSubmissionDate) {
        this.currentSubmissionDate = currentSubmissionDate;
    }

    public Date getExtendedDate() {
        return extendedDate;
    }

    public void setExtendedDate(Date extendedDate) {
        this.extendedDate = extendedDate;
    }

    public String getReasonforExtension() {
        return reasonforExtension;
    }

    public void setReasonforExtension(String reasonforExtension) {
        this.reasonforExtension = reasonforExtension;
    }

}
