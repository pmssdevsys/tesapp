///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.qspear.procurement.persistence.methods;
//
//import com.qspear.procurement.persistence.TeqipPackage;
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
///**
// *
// * @author jaspreet
// */
//@Entity
//@Table(name = "teqip_package_question_detail")
//public class TeqipPackageQuestionDetail extends AuditEntity {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "questionid")
//    private Integer questionId;
//
//    @Column(name = "question_category")
//    private String questionCategory;
//
//    @Column(name = "question_desc")
//    private String questionDesc;
//
//    @Column(name = "question_decision")
//    private Integer questionDecision;
//
//    @Column(name = "question_nonresponsive_value")
//    private Integer questionNonResponsiveValue;
//
//    @ManyToOne(cascade = CascadeType.DETACH)
//    @JoinColumn(name = "packageId")
//    private TeqipPackage teqipPackage;
//
//    public Integer getQuestionId() {
//        return questionId;
//    }
//
//    public void setQuestionId(Integer questionId) {
//        this.questionId = questionId;
//    }
//
//    public String getQuestionCategory() {
//        return questionCategory;
//    }
//
//    public void setQuestionCategory(String questionCategory) {
//        this.questionCategory = questionCategory;
//    }
//
//    public String getQuestionDesc() {
//        return questionDesc;
//    }
//
//    public void setQuestionDesc(String questionDesc) {
//        this.questionDesc = questionDesc;
//    }
//
//    public Integer getQuestionDecision() {
//        return questionDecision;
//    }
//
//    public void setQuestionDecision(Integer questionDecision) {
//        this.questionDecision = questionDecision;
//    }
//
//    public Integer getQuestionNonResponsiveValue() {
//        return questionNonResponsiveValue;
//    }
//
//    public void setQuestionNonResponsiveValue(Integer questionNonResponsiveValue) {
//        this.questionNonResponsiveValue = questionNonResponsiveValue;
//    }
//
//    public TeqipPackage getTeqipPackage() {
//        return teqipPackage;
//    }
//
//    public void setTeqipPackage(TeqipPackage teqipPackage) {
//        this.teqipPackage = teqipPackage;
//    }
//
//}
