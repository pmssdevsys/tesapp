/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_questionmaster")
public class TeqipPackageQuestionMaster extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer questionId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @Column(name = "question_category")
    private String questionCategory;

    @Column(name = "question_desc")
    private String questionDesc;

    private Integer decision;

    private String questionNonResponsiveValue;

    private Integer isStandardQuestion;

    private String type;

    private String options;

    private Integer isevalatedPrice;

    private Integer isreadOut;

    @Column(name = "minInputValue")
    String minValue;
    @Column(name = "maxInputValue")
    String maxValue;

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public String getQuestionCategory() {
        return questionCategory;
    }

    public void setQuestionCategory(String questionCategory) {
        this.questionCategory = questionCategory;
    }

    public String getQuestionDesc() {
        return questionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        this.questionDesc = questionDesc;
    }

    public Integer getDecision() {
        return decision;
    }

    public void setDecision(Integer decision) {
        this.decision = decision;
    }

    public String getQuestionNonResponsiveValue() {
        return questionNonResponsiveValue;
    }

    public void setQuestionNonResponsiveValue(String questionNonResponsiveValue) {
        this.questionNonResponsiveValue = questionNonResponsiveValue;
    }

    public Integer getIsStandardQuestion() {
        return isStandardQuestion;
    }

    public void setIsStandardQuestion(Integer isStandardQuestion) {
        this.isStandardQuestion = isStandardQuestion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getIsevalatedPrice() {
        return isevalatedPrice;
    }

    public void setIsevalatedPrice(Integer isevalatedPrice) {
        this.isevalatedPrice = isevalatedPrice;
    }

    public Integer getIsreadOut() {
        return isreadOut;
    }

    public void setIsreadOut(Integer isreadOut) {
        this.isreadOut = isreadOut;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public TeqipPackageQuestionMaster clone() {
        TeqipPackageQuestionMaster master = new TeqipPackageQuestionMaster();
        master.setTeqipPackage(this.teqipPackage);
        master.setQuestionCategory(this.questionCategory);
        master.setDecision(decision);
        master.setIsStandardQuestion(isStandardQuestion);
        master.setIsevalatedPrice(isevalatedPrice);
        master.setIsreadOut(isreadOut);
        master.setMaxValue(maxValue);
        master.setMinValue(minValue);
        master.setOptions(options);
        master.setQuestionDesc(questionDesc);
        master.setQuestionNonResponsiveValue(questionNonResponsiveValue);
        master.setType(type);
        return master;
    }

}
