/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_bidinstandscc")
public class TeqipPackageBidINSTAndSCC extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer bidinstructionId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    private String chequeFavour;
    private String payableAt;
    private Date bidsellStartTime;
    private Date bidsellEndTime;
    private Double postalCharge;
    private Double bidSecurity;
    private String bidOfficer;
    private Date prebidMeetingDate;
    private Date prebidmeetingDateTime;
    private Double warranty;
    private String arbitrationPlace;
    private String addressofNotice;
    private Double minLiquidatedDamage;
    private Double maxLiquidatedDamage;
    private String placeOfOpeningOfBids;
    private String performanceSecurity;

    public String getPerformanceSecurity() {
        return performanceSecurity;
    }

    public void setPerformanceSecurity(String performanceSecurity) {
        this.performanceSecurity = performanceSecurity;
    }
    
    
    public Integer getBidinstructionId() {
        return bidinstructionId;
    }

    public void setBidinstructionId(Integer bidinstructionId) {
        this.bidinstructionId = bidinstructionId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public String getChequeFavour() {
        return chequeFavour;
    }

    public void setChequeFavour(String chequeFavour) {
        this.chequeFavour = chequeFavour;
    }

    public String getPayableAt() {
        return payableAt;
    }

    public void setPayableAt(String payableAt) {
        this.payableAt = payableAt;
    }

    public Date getBidsellStartTime() {
        return bidsellStartTime;
    }

    public void setBidsellStartTime(Date bidsellStartTime) {
        this.bidsellStartTime = bidsellStartTime;
    }

    public Date getBidsellEndTime() {
        return bidsellEndTime;
    }

    public void setBidsellEndTime(Date bidsellEndTime) {
        this.bidsellEndTime = bidsellEndTime;
    }

    public Double getPostalCharge() {
        return postalCharge;
    }

    public void setPostalCharge(Double postalCharge) {
        this.postalCharge = postalCharge;
    }

    public Double getBidSecurity() {
        return bidSecurity;
    }

    public void setBidSecurity(Double bidSecurity) {
        this.bidSecurity = bidSecurity;
    }

    public String getBidOfficer() {
        return bidOfficer;
    }

    public void setBidOfficer(String bidOfficer) {
        this.bidOfficer = bidOfficer;
    }

    public Date getPrebidMeetingDate() {
        return prebidMeetingDate;
    }

    public void setPrebidMeetingDate(Date prebidMeetingDate) {
        this.prebidMeetingDate = prebidMeetingDate;
    }

    public Date getPrebidmeetingDateTime() {
        return prebidmeetingDateTime;
    }

    public void setPrebidmeetingDateTime(Date prebidmeetingDateTime) {
        this.prebidmeetingDateTime = prebidmeetingDateTime;
    }

    public Double getWarranty() {
        return warranty;
    }

    public void setWarranty(Double warranty) {
        this.warranty = warranty;
    }

    public String getArbitrationPlace() {
        return arbitrationPlace;
    }

    public void setArbitrationPlace(String arbitrationPlace) {
        this.arbitrationPlace = arbitrationPlace;
    }

    public String getAddressofNotice() {
        return addressofNotice;
    }

    public void setAddressofNotice(String addressofNotice) {
        this.addressofNotice = addressofNotice;
    }

    public Double getMinLiquidatedDamage() {
        return minLiquidatedDamage;
    }

    public void setMinLiquidatedDamage(Double minLiquidatedDamage) {
        this.minLiquidatedDamage = minLiquidatedDamage;
    }

    public Double getMaxLiquidatedDamage() {
        return maxLiquidatedDamage;
    }

    public void setMaxLiquidatedDamage(Double maxLiquidatedDamage) {
        this.maxLiquidatedDamage = maxLiquidatedDamage;
    }

    public String getPlaceOfOpeningOfBids() {
        return placeOfOpeningOfBids;
    }

    public void setPlaceOfOpeningOfBids(String placeOfOpeningOfBids) {
        this.placeOfOpeningOfBids = placeOfOpeningOfBids;
    }

}
