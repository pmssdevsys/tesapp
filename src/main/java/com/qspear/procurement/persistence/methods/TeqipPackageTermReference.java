/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_termreference")
public class TeqipPackageTermReference extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer termOfReferenceId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    private Integer isworldBankNoc;
    private Date worldBankNocDate;
    private Date eoiOpeningDate;
    private String remarks;
    private Date dateOfShortlistingFirms;

    public Date getDateOfShortlistingFirms() {
        return dateOfShortlistingFirms;
    }

    public void setDateOfShortlistingFirms(Date dateOfShortlistingFirms) {
        this.dateOfShortlistingFirms = dateOfShortlistingFirms;
    }
    
    public Integer getTermOfReferenceId() {
        return termOfReferenceId;
    }

    public void setTermOfReferenceId(Integer termOfReferenceId) {
        this.termOfReferenceId = termOfReferenceId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Integer getIsworldBankNoc() {
        return isworldBankNoc;
    }

    public void setIsworldBankNoc(Integer isworldBankNoc) {
        this.isworldBankNoc = isworldBankNoc;
    }

    public Date getWorldBankNocDate() {
        return worldBankNocDate;
    }

    public void setWorldBankNocDate(Date worldBankNocDate) {
        this.worldBankNocDate = worldBankNocDate;
    }

    public Date getEoiOpeningDate() {
        return eoiOpeningDate;
    }

    public void setEoiOpeningDate(Date eoiOpeningDate) {
        this.eoiOpeningDate = eoiOpeningDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
