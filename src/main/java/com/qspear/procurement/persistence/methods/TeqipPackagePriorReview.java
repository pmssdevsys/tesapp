/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.User;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_prior_review")
public class TeqipPackagePriorReview extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer packagePriorReviewId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    Integer isPriorReviewRequired;
    Integer stageIndex;
    String stageName;
    String priorReviewStageText;
    String reviewStatus;
    String comments;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "reviewedByUserId")
    User user;
    
    Date updatedTime;
    Date submittedTime;
    String requestorName;
    String roleOfNextActor;

    public String getRoleOfNextActor() {
        return roleOfNextActor;
    }

    public void setRoleOfNextActor(String roleOfNextActor) {
        this.roleOfNextActor = roleOfNextActor;
    }
    public Date getSubmittedTime() {
        return submittedTime;
    }

    public void setSubmittedTime(Date submittedTime) {
        this.submittedTime = submittedTime;
    }

    public String getRequestorName() {
        return requestorName;
    }

    public void setRequestorName(String requestorName) {
        this.requestorName = requestorName;
    }
    

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getPackagePriorReviewId() {
        return packagePriorReviewId;
    }

    public void setPackagePriorReviewId(Integer packagePriorReviewId) {
        this.packagePriorReviewId = packagePriorReviewId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Integer getIsPriorReviewRequired() {
        return isPriorReviewRequired;
    }

    public void setIsPriorReviewRequired(Integer isPriorReviewRequired) {
        this.isPriorReviewRequired = isPriorReviewRequired;
    }

    public Integer getStageIndex() {
        return stageIndex;
    }

    public void setStageIndex(Integer stageIndex) {
        this.stageIndex = stageIndex;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getPriorReviewStageText() {
        return priorReviewStageText;
    }

    public void setPriorReviewStageText(String priorReviewStageText) {
        this.priorReviewStageText = priorReviewStageText;
    }

    public String getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(String reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
