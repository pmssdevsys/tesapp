/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_techcriteria")
public class TeqipPackageTechCriteria extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer techCriteriaId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    private String techCriteriaDesc;
    private Double marks;
    private Integer isActive;

    @OneToMany(mappedBy = "teqipPackageTechCriteria", cascade = CascadeType.ALL)
    private List<TeqipPackageTechSubCriteria> techSubCriterias;

    public Integer getTechCriteriaId() {
        return techCriteriaId;
    }

    public void setTechCriteriaId(Integer techCriteriaId) {
        this.techCriteriaId = techCriteriaId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public String getTechCriteriaDesc() {
        return techCriteriaDesc;
    }

    public void setTechCriteriaDesc(String techCriteriaDesc) {
        this.techCriteriaDesc = techCriteriaDesc;
    }

    public Double getMarks() {
        return marks;
    }

    public void setMarks(Double marks) {
        this.marks = marks;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public List<TeqipPackageTechSubCriteria> getTechSubCriterias() {
        return techSubCriterias;
    }

    public void setTechSubCriterias(List<TeqipPackageTechSubCriteria> techSubCriterias) {
        this.techSubCriterias = techSubCriterias;
    }

}
