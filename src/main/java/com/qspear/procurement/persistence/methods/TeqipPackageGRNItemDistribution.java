/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_grnitemdistribution")
public class TeqipPackageGRNItemDistribution extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pack_grnitemdistributionid")
    private Integer packGrnItemDistributionId;

    @Column(name = "quantity")
    private Double quantity;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "ITEM_DEPARTMENT_ID")
    private TeqipPmssDepartmentMaster teqipPmssDepartmentMaster;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "pack_grnitemdetaiid")
    private TeqipPackageGRNItemDetail teqipPackageGRNItemDetails;

    public Integer getPackGrnItemDistributionId() {
        return packGrnItemDistributionId;
    }

    public void setPackGrnItemDistributionId(Integer packGrnItemDistributionId) {
        this.packGrnItemDistributionId = packGrnItemDistributionId;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public TeqipPmssDepartmentMaster getTeqipPmssDepartmentMaster() {
        return teqipPmssDepartmentMaster;
    }

    public void setTeqipPmssDepartmentMaster(TeqipPmssDepartmentMaster teqipPmssDepartmentMaster) {
        this.teqipPmssDepartmentMaster = teqipPmssDepartmentMaster;
    }

    public TeqipPackageGRNItemDetail getTeqipPackageGRNItemDetails() {
        return teqipPackageGRNItemDetails;
    }

    public void setTeqipPackageGRNItemDetails(TeqipPackageGRNItemDetail teqipPackageGRNItemDetails) {
        this.teqipPackageGRNItemDetails = teqipPackageGRNItemDetails;
    }

}
