package com.qspear.procurement.persistence.methods;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qspear.procurement.security.util.LoggedUser;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GeneratorType;

/**
 *
 * @author jaspreet
 */
@MappedSuperclass
public class AuditEntity {

    @JsonIgnore
    @Column(name = "MODIFY_ON")
    private Date modifyOn;

    @JsonIgnore
    @Column(name = "CREATED_ON")
    private Date createdOn;

    @JsonIgnore
    @Column(name = "MODIFY_BY")
    private Integer modifyBy;

    @JsonIgnore
    @Column(name = "CREATED_BY")
    private Integer createdBy;

    public Date getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Date modifyOn) {
        this.modifyOn = modifyOn;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    @PrePersist
    protected void onCreate() {
        createdOn = createdOn == null ? new Date(new Date().getTime()) : createdOn;
        modifyOn = new Date(new Date().getTime());
        createdBy = createdBy == null ? LoggedUser.get().getUserid() : createdBy;
        modifyBy =  LoggedUser.get().getUserid();

    }

    @PreUpdate
    protected void onUpdate() {
        modifyOn = new Date(new Date().getTime());
        modifyBy =  LoggedUser.get().getUserid() ;

    }

}
