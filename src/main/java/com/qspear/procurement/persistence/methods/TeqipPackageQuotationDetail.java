/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_quotationdetail")
public class TeqipPackageQuotationDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pack_quotationdetailid")
    private Integer packQuotationDetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @Column(name = "quotationopeningdate")
    private Date quotationOpeningDate;

    @Column(name = "quotationopeningtime")
    private Date quotationOpeningTime;

    @Column(name = "quotationmom")
    private String quotationMom;

    public Integer getPackQuotationDetailId() {
        return packQuotationDetailId;
    }

    public void setPackQuotationDetailId(Integer packQuotationDetailId) {
        this.packQuotationDetailId = packQuotationDetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Date getQuotationOpeningDate() {
        return quotationOpeningDate;
    }

    public void setQuotationOpeningDate(Date quotationOpeningDate) {
        this.quotationOpeningDate = quotationOpeningDate;
    }

    public Date getQuotationOpeningTime() {
        return quotationOpeningTime;
    }

    public void setQuotationOpeningTime(Date quotationOpeningTime) {
        this.quotationOpeningTime = quotationOpeningTime;
    }

    public String getQuotationMom() {
        return quotationMom;
    }

    public void setQuotationMom(String quotationMom) {
        this.quotationMom = quotationMom;
    }

}
