/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_bidadvertisement")
public class TeqipPackageBidAdvertisement extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer bidadvertisementId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    private String selectPackage;
    private String localPaperName;
    private String nationalPaperName;
    private Date advertisementDate;
    private Date actualpublicationLocaleDate;
    private Date actualpublicationNationalDate;
    private Date eoiSubmissionDate;

    public Integer getBidadvertisementId() {
        return bidadvertisementId;
    }

    public void setBidadvertisementId(Integer bidadvertisementId) {
        this.bidadvertisementId = bidadvertisementId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public String getSelectPackage() {
        return selectPackage;
    }

    public void setSelectPackage(String selectPackage) {
        this.selectPackage = selectPackage;
    }

    public String getLocalPaperName() {
        return localPaperName;
    }

    public void setLocalPaperName(String localPaperName) {
        this.localPaperName = localPaperName;
    }

    public String getNationalPaperName() {
        return nationalPaperName;
    }

    public void setNationalPaperName(String nationalPaperName) {
        this.nationalPaperName = nationalPaperName;
    }

    public Date getAdvertisementDate() {
        return advertisementDate;
    }

    public void setAdvertisementDate(Date advertisementDate) {
        this.advertisementDate = advertisementDate;
    }

    public Date getActualpublicationLocaleDate() {
        return actualpublicationLocaleDate;
    }

    public void setActualpublicationLocaleDate(Date actualpublicationLocaleDate) {
        this.actualpublicationLocaleDate = actualpublicationLocaleDate;
    }

    public Date getActualpublicationNationalDate() {
        return actualpublicationNationalDate;
    }

    public void setActualpublicationNationalDate(Date actualpublicationNationalDate) {
        this.actualpublicationNationalDate = actualpublicationNationalDate;
    }

    public Date getEoiSubmissionDate() {
        return eoiSubmissionDate;
    }

    public void setEoiSubmissionDate(Date eoiSubmissionDate) {
        this.eoiSubmissionDate = eoiSubmissionDate;
    }
    

}
