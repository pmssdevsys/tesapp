/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_qcbs_contractdetail")
public class TeqipPackageQCBSContractDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer contractDetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    private String memberInCharge;
    private String authorizedRepresentativeOfClient;
    private Date contractStartDate;
    private Date commencementServiceDate;
    private String arbitrationProceedingHeld;
    private Integer deliveryPeriodInMonths;
    private Double interestPercentageOnDelayedPayments;
    private Double basicValue;
    Double contractValue;
    Double applicableTax;
    Integer isAdvancePaymentRequired;
    Integer paymentMilestonesDefined;
    Integer isBankGuaranteeSubmitted;
    Integer isArbitratorAgreed;
    String contractNumber;
      Integer isWordWorkBankNOC;
    Date wordBankNOCDate;
    Double proposalAmount;

    public Integer getContractDetailId() {
        return contractDetailId;
    }

    public void setContractDetailId(Integer contractDetailId) {
        this.contractDetailId = contractDetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public String getMemberInCharge() {
        return memberInCharge;
    }

    public void setMemberInCharge(String memberInCharge) {
        this.memberInCharge = memberInCharge;
    }

    public String getAuthorizedRepresentativeOfClient() {
        return authorizedRepresentativeOfClient;
    }

    public void setAuthorizedRepresentativeOfClient(String authorizedRepresentativeOfClient) {
        this.authorizedRepresentativeOfClient = authorizedRepresentativeOfClient;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public Date getCommencementServiceDate() {
        return commencementServiceDate;
    }

    public void setCommencementServiceDate(Date commencementServiceDate) {
        this.commencementServiceDate = commencementServiceDate;
    }

    public String getArbitrationProceedingHeld() {
        return arbitrationProceedingHeld;
    }

    public void setArbitrationProceedingHeld(String arbitrationProceedingHeld) {
        this.arbitrationProceedingHeld = arbitrationProceedingHeld;
    }

    public Integer getDeliveryPeriodInMonths() {
        return deliveryPeriodInMonths;
    }

    public void setDeliveryPeriodInMonths(Integer deliveryPeriodInMonths) {
        this.deliveryPeriodInMonths = deliveryPeriodInMonths;
    }

    public Double getInterestPercentageOnDelayedPayments() {
        return interestPercentageOnDelayedPayments;
    }

    public void setInterestPercentageOnDelayedPayments(Double interestPercentageOnDelayedPayments) {
        this.interestPercentageOnDelayedPayments = interestPercentageOnDelayedPayments;
    }

    public Double getBasicValue() {
        return basicValue;
    }

    public void setBasicValue(Double basicValue) {
        this.basicValue = basicValue;
    }

    public Double getContractValue() {
        return contractValue;
    }

    public void setContractValue(Double contractValue) {
        this.contractValue = contractValue;
    }

    public Double getApplicableTax() {
        return applicableTax;
    }

    public void setApplicableTax(Double applicableTax) {
        this.applicableTax = applicableTax;
    }

    public Integer getIsAdvancePaymentRequired() {
        return isAdvancePaymentRequired;
    }

    public void setIsAdvancePaymentRequired(Integer isAdvancePaymentRequired) {
        this.isAdvancePaymentRequired = isAdvancePaymentRequired;
    }

    public Integer getPaymentMilestonesDefined() {
        return paymentMilestonesDefined;
    }

    public void setPaymentMilestonesDefined(Integer paymentMilestonesDefined) {
        this.paymentMilestonesDefined = paymentMilestonesDefined;
    }

    public Integer getIsBankGuaranteeSubmitted() {
        return isBankGuaranteeSubmitted;
    }

    public void setIsBankGuaranteeSubmitted(Integer isBankGuaranteeSubmitted) {
        this.isBankGuaranteeSubmitted = isBankGuaranteeSubmitted;
    }

    public Integer getIsArbitratorAgreed() {
        return isArbitratorAgreed;
    }

    public void setIsArbitratorAgreed(Integer isArbitratorAgreed) {
        this.isArbitratorAgreed = isArbitratorAgreed;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    
    public Integer getIsWordWorkBankNOC() {
        return isWordWorkBankNOC;
    }

    public void setIsWordWorkBankNOC(Integer isWordWorkBankNOC) {
        this.isWordWorkBankNOC = isWordWorkBankNOC;
    }

    public Date getWordBankNOCDate() {
        return wordBankNOCDate;
    }

    public void setWordBankNOCDate(Date wordBankNOCDate) {
        this.wordBankNOCDate = wordBankNOCDate;
    }

    public Double getProposalAmount() {
        return proposalAmount;
    }

    public void setProposalAmount(Double proposalAmount) {
        this.proposalAmount = proposalAmount;
    }
}
