/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_workorder")
public class TeqipPackageWorkOrder extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "package_workorderid")
    private Integer packageWorkorderId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @Column(name = "WORKORDERNO")
    private String workOrderNo;

    @Column(name = "perfsecurityrcvd")
    private Date perfsecurityrcvd;

    @Column(name = "perfsecurityexpdate")
    private Date perfsecurityexpdate;

    @Column(name = "contractstartdate")
    private Date contractstartdate;

    @Column(name = "workstartdate")
    private Date workstartdate;

    @Column(name = "workcompletiondate")
    private Date workcompletiondate;

    @Column(name = "basicvalue")
    private Double basicvalue;

    @Column(name = "sumapplicable")
    private Double sumapplicable;

    @Column(name = "contractvalue")
    private Double contractvalue;

    private Double performanceSecurityAmount;
    private Date workGeneratedDate;

    public Integer getPackageWorkorderId() {
        return packageWorkorderId;
    }

    public void setPackageWorkorderId(Integer packageWorkorderId) {
        this.packageWorkorderId = packageWorkorderId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Date getPerfsecurityrcvd() {
        return perfsecurityrcvd;
    }

    public void setPerfsecurityrcvd(Date perfsecurityrcvd) {
        this.perfsecurityrcvd = perfsecurityrcvd;
    }

    public Date getPerfsecurityexpdate() {
        return perfsecurityexpdate;
    }

    public void setPerfsecurityexpdate(Date perfsecurityexpdate) {
        this.perfsecurityexpdate = perfsecurityexpdate;
    }

    public Date getContractstartdate() {
        return contractstartdate;
    }

    public void setContractstartdate(Date contractstartdate) {
        this.contractstartdate = contractstartdate;
    }

    public Date getWorkstartdate() {
        return workstartdate;
    }

    public void setWorkstartdate(Date workstartdate) {
        this.workstartdate = workstartdate;
    }

    public Date getWorkcompletiondate() {
        return workcompletiondate;
    }

    public void setWorkcompletiondate(Date workcompletiondate) {
        this.workcompletiondate = workcompletiondate;
    }

    public Double getBasicvalue() {
        return basicvalue;
    }

    public void setBasicvalue(Double basicvalue) {
        this.basicvalue = basicvalue;
    }

    public Double getSumapplicable() {
        return sumapplicable;
    }

    public void setSumapplicable(Double sumapplicable) {
        this.sumapplicable = sumapplicable;
    }

    public Double getContractvalue() {
        return contractvalue;
    }

    public void setContractvalue(Double contractvalue) {
        this.contractvalue = contractvalue;
    }

    public String getWorkOrderNo() {
        return workOrderNo;
    }

    public Double getPerformanceSecurityAmount() {
        return performanceSecurityAmount;
    }

    public void setPerformanceSecurityAmount(Double performanceSecurityAmount) {
        this.performanceSecurityAmount = performanceSecurityAmount;
    }

    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public Date getWorkGeneratedDate() {
        return workGeneratedDate;
    }

    public void setWorkGeneratedDate(Date workGeneratedDate) {
        this.workGeneratedDate = workGeneratedDate;
    }

}
