package com.qspear.procurement.persistence.methods;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.qspear.procurement.persistence.TeqipPackage;

@Entity
@Table(name = "teqip_package_eoiextension")
public class TeqipPackageEOIExtension  extends AuditEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer eoiextensionId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;
    
    Date extendedDate;
    String reasonforExtension;
    Date submissionDate;
	
	public Integer getEoiextensionId() {
		return eoiextensionId;
	}
	public void setEoiextensionId(Integer eoiextensionId) {
		this.eoiextensionId = eoiextensionId;
	}
	public TeqipPackage getTeqipPackage() {
		return teqipPackage;
	}
	public void setTeqipPackage(TeqipPackage teqipPackage) {
		this.teqipPackage = teqipPackage;
	}
	public Date getExtendedDate() {
		return extendedDate;
	}
	public void setExtendedDate(Date extendedDate) {
		this.extendedDate = extendedDate;
	}
	public String getReasonforExtension() {
		return reasonforExtension;
	}
	public void setReasonforExtension(String reasonforExtension) {
		this.reasonforExtension = reasonforExtension;
	}
	public Date getSubmissionDate() {
		return submissionDate;
	}
	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}


}
