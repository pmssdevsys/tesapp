/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_techsubcriteria")
public class TeqipPackageTechSubCriteria extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer techsubcriteriaId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "techcriteriaId")
    private TeqipPackageTechCriteria teqipPackageTechCriteria;

    private String techCriteriaDesc;
    private Double marks;
    private Integer isActive;

    public Integer getTechsubcriteriaId() {
        return techsubcriteriaId;
    }

    public void setTechsubcriteriaId(Integer techsubcriteriaId) {
        this.techsubcriteriaId = techsubcriteriaId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public String getTechCriteriaDesc() {
        return techCriteriaDesc;
    }

    public void setTechCriteriaDesc(String techCriteriaDesc) {
        this.techCriteriaDesc = techCriteriaDesc;
    }

    public Double getMarks() {
        return marks;
    }

    public void setMarks(Double marks) {
        this.marks = marks;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public TeqipPackageTechCriteria getTeqipPackageTechCriteria() {
        return teqipPackageTechCriteria;
    }

    public void setTeqipPackageTechCriteria(TeqipPackageTechCriteria teqipPackageTechCriteria) {
        this.teqipPackageTechCriteria = teqipPackageTechCriteria;
    }

}
