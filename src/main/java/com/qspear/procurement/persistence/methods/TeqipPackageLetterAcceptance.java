/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_letteracceptance")
public class TeqipPackageLetterAcceptance extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "package_letteracceptanceid")
    private Integer packageLetterAcceptanceId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @Column(name = "maintanceperiodexpirydate")
    private Date maintancePeriodExpiryDate;

    @Column(name = "loagenerateddate")
    private Date loaGeneratedDate;

    @Column(name = "newbidvalidity")
    private Integer newBidValidity;

    @Column(name = "securityamount")
    private Double securityAmount;

    public Integer getPackageLetterAcceptanceId() {
        return packageLetterAcceptanceId;
    }

    public void setPackageLetterAcceptanceId(Integer packageLetterAcceptanceId) {
        this.packageLetterAcceptanceId = packageLetterAcceptanceId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Date getMaintancePeriodExpiryDate() {
        return maintancePeriodExpiryDate;
    }

    public void setMaintancePeriodExpiryDate(Date maintancePeriodExpiryDate) {
        this.maintancePeriodExpiryDate = maintancePeriodExpiryDate;
    }

    public Date getLoaGeneratedDate() {
        return loaGeneratedDate;
    }

    public void setLoaGeneratedDate(Date loaGeneratedDate) {
        this.loaGeneratedDate = loaGeneratedDate;
    }

    public Integer getNewBidValidity() {
        return newBidValidity;
    }

    public void setNewBidValidity(Integer newBidValidity) {
        this.newBidValidity = newBidValidity;
    }

    public Double getSecurityAmount() {
        return securityAmount;
    }

    public void setSecurityAmount(Double securityAmount) {
        this.securityAmount = securityAmount;
    }

}
