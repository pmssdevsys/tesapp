/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_biddetail")
public class TeqipPackageBidDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer biddetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;
    
    private Date salebidDocument;
    private Date lastreceiptBidDate;
    private Date lastreceiptBidTime;
    private Date lastSbdDate;
    private Date openingDate;
//    private Date opendingDateTime;
    private Integer validityQuotation;
    private Integer bidsecurityValidity;
    private Double costbidDocument;

    public Integer getBiddetailId() {
        return biddetailId;
    }

    public void setBiddetailId(Integer biddetailId) {
        this.biddetailId = biddetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Date getSalebidDocument() {
        return salebidDocument;
    }

    public void setSalebidDocument(Date salebidDocument) {
        this.salebidDocument = salebidDocument;
    }

    public Date getLastreceiptBidDate() {
        return lastreceiptBidDate;
    }

    public void setLastreceiptBidDate(Date lastreceiptBidDate) {
        this.lastreceiptBidDate = lastreceiptBidDate;
    }

    public Date getLastreceiptBidTime() {
        return lastreceiptBidTime;
    }

    public void setLastreceiptBidTime(Date lastreceiptBidTime) {
        this.lastreceiptBidTime = lastreceiptBidTime;
    }

    public Date getLastSbdDate() {
        return lastSbdDate;
    }

    public void setLastSbdDate(Date lastSbdDate) {
        this.lastSbdDate = lastSbdDate;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

//    public Date getOpendingDateTime() {
//        return opendingDateTime;
//    }
//
//    public void setOpendingDateTime(Date opendingDateTime) {
//        this.opendingDateTime = opendingDateTime;
//    }

    public Integer getValidityQuotation() {
        return validityQuotation;
    }

    public void setValidityQuotation(Integer validityQuotation) {
        this.validityQuotation = validityQuotation;
    }

    public Integer getBidsecurityValidity() {
        return bidsecurityValidity;
    }

    public void setBidsecurityValidity(Integer bidsecurityValidity) {
        this.bidsecurityValidity = bidsecurityValidity;
    }

    public Double getCostbidDocument() {
        return costbidDocument;
    }

    public void setCostbidDocument(Double costbidDocument) {
        this.costbidDocument = costbidDocument;
    }

}
