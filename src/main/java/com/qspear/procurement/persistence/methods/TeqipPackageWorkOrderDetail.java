/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_workorderdetail")
public class TeqipPackageWorkOrderDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer WorkOrderDetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "bidderId")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    private String workName;
    private String workSpecification;
    private Double workCost;
    private Double workEstimatedCost;
    private String comment;

    public Integer getWorkOrderDetailId() {
        return WorkOrderDetailId;
    }

    public void setWorkOrderDetailId(Integer WorkOrderDetailId) {
        this.WorkOrderDetailId = WorkOrderDetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getWorkSpecification() {
        return workSpecification;
    }

    public void setWorkSpecification(String workSpecification) {
        this.workSpecification = workSpecification;
    }

    public Double getWorkCost() {
        return workCost;
    }

    public void setWorkCost(Double workCost) {
        this.workCost = workCost;
    }

    public Double getWorkEstimatedCost() {
        return workEstimatedCost;
    }

    public void setWorkEstimatedCost(Double workEstimatedCost) {
        this.workEstimatedCost = workEstimatedCost;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
