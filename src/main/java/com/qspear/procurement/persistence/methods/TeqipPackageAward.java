/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_award")
public class TeqipPackageAward extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pack_quotationdetailid")
    private Integer packageAwardDetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "supplierid")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    @Column(name = "ispofinalize")
    private Integer isPofInalize;

    @Column(name = "userresponse")
    private Integer userResponse;

    @Column(name = "awardcomments")
    private String awardComments;

    public Integer getPackageAwardDetailId() {
        return packageAwardDetailId;
    }

    public void setPackageAwardDetailId(Integer packageAwardDetailId) {
        this.packageAwardDetailId = packageAwardDetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public Integer getIsPofInalize() {
        return isPofInalize;
    }

    public void setIsPofInalize(Integer isPofInalize) {
        this.isPofInalize = isPofInalize;
    }

    public Integer getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(Integer userResponse) {
        this.userResponse = userResponse;
    }

    public String getAwardComments() {
        return awardComments;
    }

    public void setAwardComments(String awardComments) {
        this.awardComments = awardComments;
    }

}
