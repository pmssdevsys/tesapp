/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_grndetail")
public class TeqipPackageGRNDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pack_grndetailid")
    private Integer packGrnDetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "supplierid")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "grn_package_documentid")
    TeqipPackageDocument grnDocument;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "grn_letter_documentid")
    TeqipPackageDocument grnLetterDocument;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "asset_package_documentid")
    TeqipPackageDocument assetDocument;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "asset_letter_documentid")
    TeqipPackageDocument assetLetterDocument;

    @Column(name = "supplydate")
    private Date supplyDate;

    @Column(name = "grnno")
    private String grnno;

    @OneToMany(mappedBy = "teqipPackageGRNDetails", cascade = CascadeType.ALL)
    private List<TeqipPackageGRNItemDetail> teqipPackageGRNItemDetails;

    public Integer getPackGrnDetailId() {
        return packGrnDetailId;
    }

    public void setPackGrnDetailId(Integer packGrnDetailId) {
        this.packGrnDetailId = packGrnDetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public Date getSupplyDate() {
        return supplyDate;
    }

    public void setSupplyDate(Date supplyDate) {
        this.supplyDate = supplyDate;
    }

    public String getGrnno() {
        return grnno;
    }

    public void setGrnno(String grnno) {
        this.grnno = grnno;
    }

    public TeqipPackageDocument getGrnDocument() {
        return grnDocument;
    }

    public void setGrnDocument(TeqipPackageDocument grnDocument) {
        this.grnDocument = grnDocument;
    }

    public TeqipPackageDocument getAssetDocument() {
        return assetDocument;
    }

    public void setAssetDocument(TeqipPackageDocument assetDocument) {
        this.assetDocument = assetDocument;
    }

    public List<TeqipPackageGRNItemDetail> getTeqipPackageGRNItemDetails() {
        return teqipPackageGRNItemDetails;
    }

    public void setTeqipPackageGRNItemDetails(List<TeqipPackageGRNItemDetail> teqipPackageGRNItemDetails) {
        this.teqipPackageGRNItemDetails = teqipPackageGRNItemDetails;
    }

    public TeqipPackageDocument getGrnLetterDocument() {
        return grnLetterDocument;
    }

    public void setGrnLetterDocument(TeqipPackageDocument grnLetterDocument) {
        this.grnLetterDocument = grnLetterDocument;
    }

    public TeqipPackageDocument getAssetLetterDocument() {
        return assetLetterDocument;
    }

    public void setAssetLetterDocument(TeqipPackageDocument assetLetterDocument) {
        this.assetLetterDocument = assetLetterDocument;
    }

}
