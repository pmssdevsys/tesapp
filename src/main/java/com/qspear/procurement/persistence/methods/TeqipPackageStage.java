/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_stage")
public class TeqipPackageStage extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer packageStageId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageStageIndexId")
    private TeqipPackageStageIndex teqipPackageStageIndex;

    Date actionTime;
    String actorName;
    String actionString;
    String actionType;
    String comments;

    public Integer getPackageStageId() {
        return packageStageId;
    }

    public void setPackageStageId(Integer packageStageId) {
        this.packageStageId = packageStageId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public Date getActionTime() {
        return actionTime;
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getActionString() {
        return actionString;
    }

    public void setActionString(String actionString) {
        this.actionString = actionString;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public TeqipPackageStageIndex getTeqipPackageStageIndex() {
        return teqipPackageStageIndex;
    }

    public void setTeqipPackageStageIndex(TeqipPackageStageIndex teqipPackageStageIndex) {
        this.teqipPackageStageIndex = teqipPackageStageIndex;
    }

}
