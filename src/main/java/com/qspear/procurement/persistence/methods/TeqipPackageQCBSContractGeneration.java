/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_qcbs_contractgeneration")
public class TeqipPackageQCBSContractGeneration extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer contractGenerationId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "lowSupplierId")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    private String recommendationComments;
    private Double contractValue;
    private Date dateOfNegotiation;
    private String negotiationComments;
    private Integer isNegotiationSuccessful;

    public Integer getContractGenerationId() {
        return contractGenerationId;
    }

    public void setContractGenerationId(Integer contractGenerationId) {
        this.contractGenerationId = contractGenerationId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public String getRecommendationComments() {
        return recommendationComments;
    }

    public void setRecommendationComments(String recommendationComments) {
        this.recommendationComments = recommendationComments;
    }

    public Double getContractValue() {
        return contractValue;
    }

    public void setContractValue(Double contractValue) {
        this.contractValue = contractValue;
    }

    public Date getDateOfNegotiation() {
        return dateOfNegotiation;
    }

    public void setDateOfNegotiation(Date dateOfNegotiation) {
        this.dateOfNegotiation = dateOfNegotiation;
    }

    public String getNegotiationComments() {
        return negotiationComments;
    }

    public void setNegotiationComments(String negotiationComments) {
        this.negotiationComments = negotiationComments;
    }

    public Integer getIsNegotiationSuccessful() {
        return isNegotiationSuccessful;
    }

    public void setIsNegotiationSuccessful(Integer isNegotiationSuccessful) {
        this.isNegotiationSuccessful = isNegotiationSuccessful;
    }

}
