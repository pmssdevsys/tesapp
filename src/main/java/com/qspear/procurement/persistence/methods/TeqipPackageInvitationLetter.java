/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_invitationletter")
public class TeqipPackageInvitationLetter extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer invitationLetterId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "categoryId")
    private TeqipCategorymaster teqipCategorymaster;

    private Integer quotationValidityRequired;
    private String trainingClause;
    private String warranty;
    private Integer workCompletionPeriod;
    private Double minliquidatedDamages;
    private Date lastSubmissionDate;
    private Date lastSubmissionTime;
    private String installationClause;
    private String amc;
    private Date plannedBidOpeningDate;
    private Date plannedBidOpeningTime;
    private Double maxliquidatedDamages;
    private Double quotedPrice;
    private Double performanceSecurityInPercentage;
    private Integer numberOfBids;

    public Integer getNumberOfBids() {
        return numberOfBids;
    }

    public void setNumberOfBids(Integer numberOfBids) {
        this.numberOfBids = numberOfBids;
    }
    
    
    public Double getPerformanceSecurityInPercentage() {
        return performanceSecurityInPercentage;
    }

    public void setPerformanceSecurityInPercentage(Double performanceSecurityInPercentage) {
        this.performanceSecurityInPercentage = performanceSecurityInPercentage;
    }

    public Integer getInvitationLetterId() {
        return invitationLetterId;
    }

    public void setInvitationLetterId(Integer invitationLetterId) {
        this.invitationLetterId = invitationLetterId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipCategorymaster getTeqipCategorymaster() {
        return teqipCategorymaster;
    }

    public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
        this.teqipCategorymaster = teqipCategorymaster;
    }

    public Integer getQuotationValidityRequired() {
        return quotationValidityRequired;
    }

    public void setQuotationValidityRequired(Integer quotationValidityRequired) {
        this.quotationValidityRequired = quotationValidityRequired;
    }

    public String getTrainingClause() {
        return trainingClause;
    }

    public void setTrainingClause(String trainingClause) {
        this.trainingClause = trainingClause;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public Integer getWorkCompletionPeriod() {
        return workCompletionPeriod;
    }

    public void setWorkCompletionPeriod(Integer workCompletionPeriod) {
        this.workCompletionPeriod = workCompletionPeriod;
    }

    public Double getMinliquidatedDamages() {
        return minliquidatedDamages;
    }

    public void setMinliquidatedDamages(Double minliquidatedDamages) {
        this.minliquidatedDamages = minliquidatedDamages;
    }

    public Date getLastSubmissionDate() {
        return lastSubmissionDate;
    }

    public void setLastSubmissionDate(Date lastSubmissionDate) {
        this.lastSubmissionDate = lastSubmissionDate;
    }

    public Date getLastSubmissionTime() {
        return lastSubmissionTime;
    }

    public void setLastSubmissionTime(Date lastSubmissionTime) {
        this.lastSubmissionTime = lastSubmissionTime;
    }

    public String getInstallationClause() {
        return installationClause;
    }

    public void setInstallationClause(String installationClause) {
        this.installationClause = installationClause;
    }

    public String getAmc() {
        return amc;
    }

    public void setAmc(String amc) {
        this.amc = amc;
    }

    public Date getPlannedBidOpeningDate() {
        return plannedBidOpeningDate;
    }

    public void setPlannedBidOpeningDate(Date plannedBidOpeningDate) {
        this.plannedBidOpeningDate = plannedBidOpeningDate;
    }

    public Date getPlannedBidOpeningTime() {
        return plannedBidOpeningTime;
    }

    public void setPlannedBidOpeningTime(Date plannedBidOpeningTime) {
        this.plannedBidOpeningTime = plannedBidOpeningTime;
    }

    public Double getMaxliquidatedDamages() {
        return maxliquidatedDamages;
    }

    public void setMaxliquidatedDamages(Double maxliquidatedDamages) {
        this.maxliquidatedDamages = maxliquidatedDamages;
    }

    public Double getQuotedPrice() {
        return quotedPrice;
    }

    public void setQuotedPrice(Double quotedPrice) {
        this.quotedPrice = quotedPrice;
    }

}
