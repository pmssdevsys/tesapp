/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_paymentdetail")
public class TeqipPackagePaymentDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pack_paymentid")
    private Integer packagePaymentId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    private String paymentDescription;
    private Double milestonePercentage;
    private Date expectedDeliveryDate;
    private Integer expectedDeliveryPeriod;
    private Double expectedPaymentAmount;
    private String poNumber;
    private Date poDate;
    private Integer isWCCGenerated;
    private Integer milestoneActualCompletionPeriod;
    private String paymentDate;
    private String chequeDraftNumber;
    private Date expectedPaymentdate;
    private Integer isWorkCompleted;
    private Date actualCompletionDate;
    private Double actualPaymentAmount;
    private Double totalPaymentTillNow;
    private String comments;
    private Double gst;
    private Double octroi;
    private Integer isliquidateDamageWaived;
    private Double liquidatedDamages;
    private Double expectedPaymentAmountLD;
    private Integer actualCompletionPeriod;
    private Date actualPaymentDate;
    private Double retentionMoney;
    private String comment1;
    private String comment2;
    private String comment3;
    private String uploadCompletionCertificate;
    private String contractManagmentComment;
    Integer deductionForMobilization;
    Integer isQualityCheckCarriedOut;
    String paymentMode;
    Date paymentModeDate;
    String bankName;
    String bankDetails;

    public Date getPaymentModeDate() {
        return paymentModeDate;
    }

    public void setPaymentModeDate(Date paymentModeDate) {
        this.paymentModeDate = paymentModeDate;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(String bankDetails) {
        this.bankDetails = bankDetails;
    }
    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Integer getPackagePaymentId() {
        return packagePaymentId;
    }

    public void setPackagePaymentId(Integer packagePaymentId) {
        this.packagePaymentId = packagePaymentId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public Double getMilestonePercentage() {
        return milestonePercentage;
    }

    public void setMilestonePercentage(Double milestonePercentage) {
        this.milestonePercentage = milestonePercentage;
    }

    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public Integer getExpectedDeliveryPeriod() {
        return expectedDeliveryPeriod;
    }

    public void setExpectedDeliveryPeriod(Integer expectedDeliveryPeriod) {
        this.expectedDeliveryPeriod = expectedDeliveryPeriod;
    }

    public Double getExpectedPaymentAmount() {
        return expectedPaymentAmount;
    }

    public void setExpectedPaymentAmount(Double expectedPaymentAmount) {
        this.expectedPaymentAmount = expectedPaymentAmount;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public Date getPoDate() {
        return poDate;
    }

    public void setPoDate(Date poDate) {
        this.poDate = poDate;
    }

    public Integer getIsWCCGenerated() {
        return isWCCGenerated;
    }

    public void setIsWCCGenerated(Integer isWCCGenerated) {
        this.isWCCGenerated = isWCCGenerated;
    }

    public Integer getMilestoneActualCompletionPeriod() {
        return milestoneActualCompletionPeriod;
    }

    public void setMilestoneActualCompletionPeriod(Integer milestoneActualCompletionPeriod) {
        this.milestoneActualCompletionPeriod = milestoneActualCompletionPeriod;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getChequeDraftNumber() {
        return chequeDraftNumber;
    }

    public void setChequeDraftNumber(String chequeDraftNumber) {
        this.chequeDraftNumber = chequeDraftNumber;
    }

    public Date getExpectedPaymentdate() {
        return expectedPaymentdate;
    }

    public void setExpectedPaymentdate(Date expectedPaymentdate) {
        this.expectedPaymentdate = expectedPaymentdate;
    }

    public Integer getIsWorkCompleted() {
        return isWorkCompleted;
    }

    public void setIsWorkCompleted(Integer isWorkCompleted) {
        this.isWorkCompleted = isWorkCompleted;
    }

    public Date getActualCompletionDate() {
        return actualCompletionDate;
    }

    public void setActualCompletionDate(Date actualCompletionDate) {
        this.actualCompletionDate = actualCompletionDate;
    }

    public Double getActualPaymentAmount() {
        return actualPaymentAmount;
    }

    public void setActualPaymentAmount(Double actualPaymentAmount) {
        this.actualPaymentAmount = actualPaymentAmount;
    }

    public Double getTotalPaymentTillNow() {
        return totalPaymentTillNow;
    }

    public void setTotalPaymentTillNow(Double totalPaymentTillNow) {
        this.totalPaymentTillNow = totalPaymentTillNow;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Double getGst() {
        return gst;
    }

    public void setGst(Double gst) {
        this.gst = gst;
    }

    public Double getOctroi() {
        return octroi;
    }

    public void setOctroi(Double octroi) {
        this.octroi = octroi;
    }

    public Integer getIsliquidateDamageWaived() {
        return isliquidateDamageWaived;
    }

    public void setIsliquidateDamageWaived(Integer isliquidateDamageWaived) {
        this.isliquidateDamageWaived = isliquidateDamageWaived;
    }

    public Double getLiquidatedDamages() {
        return liquidatedDamages;
    }

    public void setLiquidatedDamages(Double liquidatedDamages) {
        this.liquidatedDamages = liquidatedDamages;
    }

    public Double getExpectedPaymentAmountLD() {
        return expectedPaymentAmountLD;
    }

    public void setExpectedPaymentAmountLD(Double expectedPaymentAmountLD) {
        this.expectedPaymentAmountLD = expectedPaymentAmountLD;
    }

    public Integer getActualCompletionPeriod() {
        return actualCompletionPeriod;
    }

    public void setActualCompletionPeriod(Integer actualCompletionPeriod) {
        this.actualCompletionPeriod = actualCompletionPeriod;
    }

    public Date getActualPaymentDate() {
        return actualPaymentDate;
    }

    public void setActualPaymentDate(Date actualPaymentDate) {
        this.actualPaymentDate = actualPaymentDate;
    }

    public Double getRetentionMoney() {
        return retentionMoney;
    }

    public void setRetentionMoney(Double retentionMoney) {
        this.retentionMoney = retentionMoney;
    }

    public String getComment1() {
        return comment1;
    }

    public void setComment1(String comment1) {
        this.comment1 = comment1;
    }

    public String getComment2() {
        return comment2;
    }

    public void setComment2(String comment2) {
        this.comment2 = comment2;
    }

    public String getComment3() {
        return comment3;
    }

    public void setComment3(String comment3) {
        this.comment3 = comment3;
    }

    public String getContractManagmentComment() {
        return contractManagmentComment;
    }

    public void setContractManagmentComment(String contractManagmentComment) {
        this.contractManagmentComment = contractManagmentComment;
    }

    public String getUploadCompletionCertificate() {
        return uploadCompletionCertificate;
    }

    public void setUploadCompletionCertificate(String uploadCompletionCertificate) {
        this.uploadCompletionCertificate = uploadCompletionCertificate;
    }

    public Integer getDeductionForMobilization() {
        return deductionForMobilization;
    }

    public void setDeductionForMobilization(Integer deductionForMobilization) {
        this.deductionForMobilization = deductionForMobilization;
    }

    public Integer getIsQualityCheckCarriedOut() {
        return isQualityCheckCarriedOut;
    }

    public void setIsQualityCheckCarriedOut(Integer isQualityCheckCarriedOut) {
        this.isQualityCheckCarriedOut = isQualityCheckCarriedOut;
    }

}
