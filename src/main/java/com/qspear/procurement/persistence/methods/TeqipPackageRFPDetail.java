/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_rfpdetail")
public class TeqipPackageRFPDetail extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer rfpdetailId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    private String typeOfContract;
    private Date proposalSubmissionDate;
    private Date proposalSubmissionDateTime;
    private Date preProposalMeetingDate;
    private Date preProposalMeetingDateTime;
    private String preProposalVenue;
    Integer isWordWorkBankNOC;
    Date wordBankNOCDate;

    public String getPreProposalVenue() {
        return preProposalVenue;
    }

    public void setPreProposalVenue(String preProposalVenue) {
        this.preProposalVenue = preProposalVenue;
    }

    public Integer getRfpdetailId() {
        return rfpdetailId;
    }

    public void setRfpdetailId(Integer rfpdetailId) {
        this.rfpdetailId = rfpdetailId;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public String getTypeOfContract() {
        return typeOfContract;
    }

    public void setTypeOfContract(String typeOfContract) {
        this.typeOfContract = typeOfContract;
    }

    public Date getProposalSubmissionDate() {
        return proposalSubmissionDate;
    }

    public void setProposalSubmissionDate(Date proposalSubmissionDate) {
        this.proposalSubmissionDate = proposalSubmissionDate;
    }

    public Date getProposalSubmissionDateTime() {
        return proposalSubmissionDateTime;
    }

    public void setProposalSubmissionDateTime(Date proposalSubmissionDateTime) {
        this.proposalSubmissionDateTime = proposalSubmissionDateTime;
    }

    public Date getPreProposalMeetingDate() {
        return preProposalMeetingDate;
    }

    public void setPreProposalMeetingDate(Date preProposalMeetingDate) {
        this.preProposalMeetingDate = preProposalMeetingDate;
    }

    public Date getPreProposalMeetingDateTime() {
        return preProposalMeetingDateTime;
    }

    public void setPreProposalMeetingDateTime(Date preProposalMeetingDateTime) {
        this.preProposalMeetingDateTime = preProposalMeetingDateTime;
    }

    public Integer getIsWordWorkBankNOC() {
        return isWordWorkBankNOC;
    }

    public void setIsWordWorkBankNOC(Integer isWordWorkBankNOC) {
        this.isWordWorkBankNOC = isWordWorkBankNOC;
    }

    public Date getWordBankNOCDate() {
        return wordBankNOCDate;
    }

    public void setWordBankNOCDate(Date wordBankNOCDate) {
        this.wordBankNOCDate = wordBankNOCDate;
    }

}
