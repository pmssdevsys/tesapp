/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence.methods;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_package_completechecklist")
public class TeqipPackageCompleteCheckList extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pack_completechecklistid")
    private Integer packageCompleteCheckListId;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "packageId")
    private TeqipPackage teqipPackage;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "supplierid")
    private TeqipPmssSupplierMaster teqipPmssSupplierMaster;

    @Column(name = "goodreceived")
    private Integer goodReceived;

    @Column(name = "paymentdone")
    private Integer paymentDone;

    @Column(name = "installationdone")
    private Integer installationDone;

    @Column(name = "trainingdone")
    private Integer trainingDone;

    @Column(name = "procurementdate")
    private Date procurementDate;

    @Column(name = "workcompleted")
    private Integer workCompleted;

    @Column(name = "envchecklistupdated")
    private Integer envchecklistUpdated;

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipPmssSupplierMaster getTeqipPmssSupplierMaster() {
        return teqipPmssSupplierMaster;
    }

    public void setTeqipPmssSupplierMaster(TeqipPmssSupplierMaster teqipPmssSupplierMaster) {
        this.teqipPmssSupplierMaster = teqipPmssSupplierMaster;
    }

    public Integer getGoodReceived() {
        return goodReceived;
    }

    public void setGoodReceived(Integer goodReceived) {
        this.goodReceived = goodReceived;
    }

    public Integer getPaymentDone() {
        return paymentDone;
    }

    public void setPaymentDone(Integer paymentDone) {
        this.paymentDone = paymentDone;
    }

    public Integer getInstallationDone() {
        return installationDone;
    }

    public void setInstallationDone(Integer installationDone) {
        this.installationDone = installationDone;
    }

    public Integer getTrainingDone() {
        return trainingDone;
    }

    public void setTrainingDone(Integer trainingDone) {
        this.trainingDone = trainingDone;
    }

    public Date getProcurementDate() {
        return procurementDate;
    }

    public void setProcurementDate(Date procurementDate) {
        this.procurementDate = procurementDate;
    }

    public Integer getWorkCompleted() {
        return workCompleted;
    }

    public void setWorkCompleted(Integer workCompleted) {
        this.workCompleted = workCompleted;
    }

    public Integer getEnvchecklistUpdated() {
        return envchecklistUpdated;
    }

    public void setEnvchecklistUpdated(Integer envchecklistUpdated) {
        this.envchecklistUpdated = envchecklistUpdated;
    }

    public Integer getPackageCompleteCheckListId() {
        return packageCompleteCheckListId;
    }

    public void setPackageCompleteCheckListId(Integer packageCompleteCheckListId) {
        this.packageCompleteCheckListId = packageCompleteCheckListId;
    }

}
