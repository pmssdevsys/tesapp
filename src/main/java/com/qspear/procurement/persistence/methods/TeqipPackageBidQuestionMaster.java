///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.qspear.procurement.persistence.methods;
//
//import com.qspear.procurement.persistence.TeqipPackage;
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
///**
// *
// * @author jaspreet
// */
//@Entity
//@Table(name = "teqip_package_bidquestionmaster")
//public class TeqipPackageBidQuestionMaster extends AuditEntity {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Integer bidQuestionId;
//
//    @ManyToOne(cascade = CascadeType.DETACH)
//    @JoinColumn(name = "packageId")
//    private TeqipPackage teqipPackage;
//
//    private String questionCategory;
//    private String questionDesc;
//    private String questionResponse;
//    private String questionNonResponse;
//    private Integer STATUS;
//
//    public Integer getBidQuestionId() {
//        return bidQuestionId;
//    }
//
//    public void setBidQuestionId(Integer bidQuestionId) {
//        this.bidQuestionId = bidQuestionId;
//    }
//
//    public TeqipPackage getTeqipPackage() {
//        return teqipPackage;
//    }
//
//    public void setTeqipPackage(TeqipPackage teqipPackage) {
//        this.teqipPackage = teqipPackage;
//    }
//
//    public String getQuestionCategory() {
//        return questionCategory;
//    }
//
//    public void setQuestionCategory(String questionCategory) {
//        this.questionCategory = questionCategory;
//    }
//
//    public String getQuestionDesc() {
//        return questionDesc;
//    }
//
//    public void setQuestionDesc(String questionDesc) {
//        this.questionDesc = questionDesc;
//    }
//
//    public String getQuestionResponse() {
//        return questionResponse;
//    }
//
//    public void setQuestionResponse(String questionResponse) {
//        this.questionResponse = questionResponse;
//    }
//
//    public String getQuestionNonResponse() {
//        return questionNonResponse;
//    }
//
//    public void setQuestionNonResponse(String questionNonResponse) {
//        this.questionNonResponse = questionNonResponse;
//    }
//
//    public Integer getSTATUS() {
//        return STATUS;
//    }
//
//    public void setSTATUS(Integer STATUS) {
//        this.STATUS = STATUS;
//    }
//
//}
