package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the TEQIP_ITEM_MASTER database table.
 * 
 */
@Entity
@Table(name="TEQIP_ITEM_MASTER")
public class TeqipItemMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ITEM_ID")
	private Integer itemId;

	@Column(name="CREATED_BY")
	private Integer createdBy;

	@Column(name="CREATED_ON")
	private Timestamp createdOn;

	private String description;

	@Column(name="ITEM_NAME")
	private String itemName;

	@Column(name="MODIFY_BY")
	private Integer modifyBy;

	@Temporal(TemporalType.DATE)
	@Column(name="MODIFY_ON")
	private Date modifyOn;

	private Boolean status;


	//bi-directional many-to-one association to TeqipCategorymaster
	@ManyToOne
	@JoinColumn(name="ITEM_CATEGORY_ID")
	private TeqipCategorymaster teqipCategorymaster;

	//bi-directional many-to-one association to TeqipSubcategorymaster
	@ManyToOne
	@JoinColumn(name="SUB_CATEGORY_ID")
	private TeqipSubcategorymaster teqipSubcategorymaster;
        
           @OneToMany(mappedBy = "teqipItemMaster")
    private List<TeqipItemGoodsDetail> teqipItemGoodsDetails;


	public TeqipItemMaster() {
	}

	public Integer getItemId() {
		return this.itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getModifyBy() {
		return this.modifyBy;
	}

	public void setModifyBy(Integer modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyOn() {
		return this.modifyOn;
	}

	public void setModifyDate(Date modifyOn) {
		this.modifyOn = modifyOn;
	}

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public TeqipCategorymaster getTeqipCategorymaster() {
		return this.teqipCategorymaster;
	}

	public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
		this.teqipCategorymaster = teqipCategorymaster;
	}

	public TeqipSubcategorymaster getTeqipSubcategorymaster() {
		return this.teqipSubcategorymaster;
	}

	public void setTeqipSubcategorymaster(TeqipSubcategorymaster teqipSubcategorymaster) {
		this.teqipSubcategorymaster = teqipSubcategorymaster;
	}

}