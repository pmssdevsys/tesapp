package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the TEQIP_ACTIVITIYMASTER database table.
 * 
 */
@Entity
@Table(name="TEQIP_ACTIVITIYMASTER")
public class TeqipActivitiymaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ACTIVITY_ID")
	private Integer activityId;

	@Column(name="ACTIVITY_NAME")
	private String activityName;

	@Column(name="CREATED_BY")
	private Integer createdBy;

	@Column(name="CREATED_ON")
	private Timestamp createdOn;

	private Boolean isactive;

	//bi-directional many-to-one association to TeqipCategorymaster
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="CATEGORY_ID")
	private TeqipCategorymaster teqipCategorymaster;

	//bi-directional many-to-one association to TeqipPackage
	@OneToMany(mappedBy="teqipActivitiymaster", cascade = CascadeType.ALL)
	private List<TeqipPackage> teqipPackages;

	public TeqipActivitiymaster() {
	}

	public Integer getActivityId() {
		return this.activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public String getActivityName() {
		return this.activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public TeqipCategorymaster getTeqipCategorymaster() {
		return this.teqipCategorymaster;
	}

	public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
		this.teqipCategorymaster = teqipCategorymaster;
	}

	public List<TeqipPackage> getTeqipPackages() {
		return this.teqipPackages;
	}

	public void setTeqipPackages(List<TeqipPackage> teqipPackages) {
		this.teqipPackages = teqipPackages;
	}

	public TeqipPackage addTeqipPackage(TeqipPackage teqipPackage) {
		getTeqipPackages().add(teqipPackage);
		teqipPackage.setTeqipActivitiymaster(this);

		return teqipPackage;
	}

	public TeqipPackage removeTeqipPackage(TeqipPackage teqipPackage) {
		getTeqipPackages().remove(teqipPackage);
		teqipPackage.setTeqipActivitiymaster(null);

		return teqipPackage;
	}


}