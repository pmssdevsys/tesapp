package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the TEQIP_CATEGORYMASTER database table.
 * 
 */
@Entity
@Table(name="TEQIP_CATEGORYMASTER")
public class TeqipCategorymaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="CAT_ID")
	private Integer catId;

	@Column(name="CATEGORY_NAME")
	private String categoryName;

	@Column(name="ISACTIVE")
	private Boolean isactive;

	//bi-directional many-to-one association to TeqipActivitiymaster
	@OneToMany(mappedBy="teqipCategorymaster", cascade=CascadeType.ALL)
	private List<TeqipActivitiymaster> teqipActivitiymasters;

	//bi-directional many-to-one association to TeqipPmssThreshholdmaster
	@OneToMany(mappedBy="teqipCategorymaster", cascade=CascadeType.ALL)
	private List<TeqipPmssThreshholdmaster> teqipPmssThreshholdmasters;

	//bi-directional many-to-one association to TeqipProcurementmethod
	@OneToMany(mappedBy="teqipCategorymaster", cascade=CascadeType.ALL)
	private List<TeqipProcurementmethod> teqipProcurementmethods;

	//bi-directional many-to-one association to TeqipProcurementmethodTimeline
	@OneToMany(mappedBy="teqipCategorymaster", cascade=CascadeType.ALL)
	private List<TeqipProcurementmethodTimeline> teqipProcurementmethodTimelines;

	//bi-directional many-to-one association to TeqipSubcategorymaster
	@OneToMany(mappedBy="teqipCategorymaster", cascade=CascadeType.ALL)
	private List<TeqipSubcategorymaster> teqipSubcategorymasters;

	//bi-directional many-to-one association to TeqipSubcomponentmaster
	@OneToMany(mappedBy="teqipCategorymaster", cascade=CascadeType.ALL)
	private List<TeqipSubcomponentmaster> teqipSubcomponentmasters;

	public TeqipCategorymaster() {
	}

	public Integer getCatId() {
		return this.catId;
	}

	public void setCatId(Integer catId) {
		this.catId = catId;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Boolean getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public List<TeqipActivitiymaster> getTeqipActivitiymasters() {
		return this.teqipActivitiymasters;
	}

	public void setTeqipActivitiymasters(List<TeqipActivitiymaster> teqipActivitiymasters) {
		this.teqipActivitiymasters = teqipActivitiymasters;
	}

	public TeqipActivitiymaster addTeqipActivitiymaster(TeqipActivitiymaster teqipActivitiymaster) {
		getTeqipActivitiymasters().add(teqipActivitiymaster);
		teqipActivitiymaster.setTeqipCategorymaster(this);

		return teqipActivitiymaster;
	}

	public TeqipActivitiymaster removeTeqipActivitiymaster(TeqipActivitiymaster teqipActivitiymaster) {
		getTeqipActivitiymasters().remove(teqipActivitiymaster);
		teqipActivitiymaster.setTeqipCategorymaster(null);

		return teqipActivitiymaster;
	}

	public List<TeqipPmssThreshholdmaster> getTeqipPmssThreshholdmasters() {
		return this.teqipPmssThreshholdmasters;
	}

	public void setTeqipPmssThreshholdmasters(List<TeqipPmssThreshholdmaster> teqipPmssThreshholdmasters) {
		this.teqipPmssThreshholdmasters = teqipPmssThreshholdmasters;
	}

	public TeqipPmssThreshholdmaster addTeqipPmssThreshholdmaster(TeqipPmssThreshholdmaster teqipPmssThreshholdmaster) {
		getTeqipPmssThreshholdmasters().add(teqipPmssThreshholdmaster);
		teqipPmssThreshholdmaster.setTeqipCategorymaster(this);

		return teqipPmssThreshholdmaster;
	}

	public TeqipPmssThreshholdmaster removeTeqipPmssThreshholdmaster(TeqipPmssThreshholdmaster teqipPmssThreshholdmaster) {
		getTeqipPmssThreshholdmasters().remove(teqipPmssThreshholdmaster);
		teqipPmssThreshholdmaster.setTeqipCategorymaster(null);

		return teqipPmssThreshholdmaster;
	}

	public List<TeqipProcurementmethod> getTeqipProcurementmethods() {
		return this.teqipProcurementmethods;
	}

	public void setTeqipProcurementmethods(List<TeqipProcurementmethod> teqipProcurementmethods) {
		this.teqipProcurementmethods = teqipProcurementmethods;
	}

	public TeqipProcurementmethod addTeqipProcurementmethod(TeqipProcurementmethod teqipProcurementmethod) {
		getTeqipProcurementmethods().add(teqipProcurementmethod);
		teqipProcurementmethod.setTeqipCategorymaster(this);

		return teqipProcurementmethod;
	}

	public TeqipProcurementmethod removeTeqipProcurementmethod(TeqipProcurementmethod teqipProcurementmethod) {
		getTeqipProcurementmethods().remove(teqipProcurementmethod);
		teqipProcurementmethod.setTeqipCategorymaster(null);

		return teqipProcurementmethod;
	}

	public List<TeqipProcurementmethodTimeline> getTeqipProcurementmethodTimelines() {
		return this.teqipProcurementmethodTimelines;
	}

	public void setTeqipProcurementmethodTimelines(List<TeqipProcurementmethodTimeline> teqipProcurementmethodTimelines) {
		this.teqipProcurementmethodTimelines = teqipProcurementmethodTimelines;
	}

	public List<TeqipSubcategorymaster> getTeqipSubcategorymasters() {
		return this.teqipSubcategorymasters;
	}

	public void setTeqipSubcategorymasters(List<TeqipSubcategorymaster> teqipSubcategorymasters) {
		this.teqipSubcategorymasters = teqipSubcategorymasters;
	}

	public TeqipSubcategorymaster addTeqipSubcategorymaster(TeqipSubcategorymaster teqipSubcategorymaster) {
		getTeqipSubcategorymasters().add(teqipSubcategorymaster);
		teqipSubcategorymaster.setTeqipCategorymaster(this);

		return teqipSubcategorymaster;
	}

	public TeqipSubcategorymaster removeTeqipSubcategorymaster(TeqipSubcategorymaster teqipSubcategorymaster) {
		getTeqipSubcategorymasters().remove(teqipSubcategorymaster);
		teqipSubcategorymaster.setTeqipCategorymaster(null);

		return teqipSubcategorymaster;
	}

	public List<TeqipSubcomponentmaster> getTeqipSubcomponentmasters() {
		return this.teqipSubcomponentmasters;
	}

	public void setTeqipSubcomponentmasters(List<TeqipSubcomponentmaster> teqipSubcomponentmasters) {
		this.teqipSubcomponentmasters = teqipSubcomponentmasters;
	}

	public TeqipSubcomponentmaster addTeqipSubcomponentmaster(TeqipSubcomponentmaster teqipSubcomponentmaster) {
		getTeqipSubcomponentmasters().add(teqipSubcomponentmaster);
		teqipSubcomponentmaster.setTeqipCategorymaster(this);

		return teqipSubcomponentmaster;
	}

	public TeqipSubcomponentmaster removeTeqipSubcomponentmaster(TeqipSubcomponentmaster teqipSubcomponentmaster) {
		getTeqipSubcomponentmasters().remove(teqipSubcomponentmaster);
		teqipSubcomponentmaster.setTeqipCategorymaster(null);

		return teqipSubcomponentmaster;
	}

}