package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the TEQIP_STATEMASTER database table.
 *
 */
@Entity
@Table(name = "TEQIP_CITYMASTER")
public class TeqipCitymaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer cityId;
    private String cityName;
    private Integer pincode;
    private Boolean isactive;

    @ManyToOne
    @JoinColumn(name = "stateId")
    private TeqipStatemaster statemaster;

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public TeqipStatemaster getStatemaster() {
        return statemaster;
    }

    public void setStatemaster(TeqipStatemaster statemaster) {
        this.statemaster = statemaster;
    }

}
