package com.qspear.procurement.persistence;

import com.qspear.procurement.persistence.methods.AuditEntity;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * The persistent class for the TEQIP_PACKAGE database table.
 *
 */
@Entity
@Table(name = "TEQIP_PACKAGE_rev_history")
public class TeqipPackageHistory extends AuditEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer packageHistoryId;

    @Column(name = "REVISEID")
    private Integer reviseId;

    @Column(name = "ESTIMATED_COST")
    private Double estimatedCost;

    @Temporal(TemporalType.DATE)
    @Column(name = "FINANCIAL_SANCTION_DATE")
    private Date financialSanctionDate;

    private Boolean iscoe;

    private Boolean isgem;

    private Boolean isprop;

    private String justification;

    @Column(name = "PACKAGE_CODE")
    private String packageCode;

    @Column(name = "PACKAGE_NAME")
    private String packageName;

    @Column(name = "REVISION_COMMENTS")
    private String revisionComments;

    @Column(name = "SERVICE_PROVIDER_ID")
    private Integer serviceProviderId;

    @Column(name = "TYPEOFPLAN_CREATOR")
    private Integer typeofplanCreator;

    @Column(name = "PACKAGE_INITIATEDATE")
    private Date initiateDate;

    private String packageMetaData;

    //bi-directional many-to-one association to TeqipCategorymaster
    @ManyToOne()
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "PACKAGE_ID")
    private TeqipPackage teqipPackage;

    //bi-directional many-to-one association to TeqipCategorymaster
    @ManyToOne()
    @JoinColumn(name = "CATEGORY_ID")
    private TeqipCategorymaster teqipCategorymaster;

    //bi-directional many-to-one association to TeqipSubcategorymaster
    @ManyToOne()
    @JoinColumn(name = "SUB_CATEGORY_ID")
    private TeqipSubcategorymaster teqipSubcategorymaster;

    //bi-directional many-to-one association to TeqipActivitiymaster
    @ManyToOne()
    @JoinColumn(name = "ACTIVITY_ID")
    private TeqipActivitiymaster teqipActivitiymaster;

    //bi-directional many-to-one association to TeqipProcurementmethod
    @ManyToOne()
    @JoinColumn(name = "PROCUREMENT_METHOD_ID")
    private TeqipProcurementmethod teqipProcurementmethod;

    //bi-directional many-to-one association to TeqipProcurementmaster
    @ManyToOne()
    @JoinColumn(name = "PROCUREMENT_PLAN_ID")
    private TeqipProcurementmaster teqipProcurementmaster;

    //bi-directional many-to-one association to TeqipInstitution
    @ManyToOne()
    @JoinColumn(name = "INSTITUTION_ID")
    private TeqipInstitution teqipInstitution;

    //bi-directional many-to-one association to TeqipStatemaster
    @ManyToOne()
    @JoinColumn(name = "SPFUID")
    private TeqipStatemaster teqipStatemaster;

    //bi-directional many-to-one association to TeqipPmssProcurementstage
    @ManyToOne()
    @JoinColumn(name = "PROCUREMENTSTAGE_ID")
    private TeqipPmssProcurementstage teqipPmssProcurementstage;

    //bi-directional many-to-one association to TeqipSubcomponentmaster
    @ManyToOne()
    @JoinColumn(name = "SUB_COMPONENT_ID")
    private TeqipSubcomponentmaster teqipSubcomponentmaster;

    @OneToMany(mappedBy = "teqipPackageHistory")
    private List<TeqipItemGoodsDetailHistory> teqipItemGoodsDetails;

    @OneToMany(mappedBy = "teqipPackageHistory")
    private List<TeqipItemWorkDetailHistory> teqipItemWorkDetails;

    public TeqipPackageHistory() {

    }

    public TeqipPackageHistory(TeqipPackage teqipPackage) {
        TeqipPackageHistory teqipPackageHistory = this;

        teqipPackageHistory.setReviseId(teqipPackage.getReviseId());
        teqipPackageHistory.setEstimatedCost(teqipPackage.getEstimatedCost());
        teqipPackageHistory.setFinancialSanctionDate(teqipPackage.getFinancialSanctionDate());
        teqipPackageHistory.setIscoe(teqipPackage.getIscoe());
        teqipPackageHistory.setIsgem(teqipPackage.getIsgem());
        teqipPackageHistory.setIsprop(teqipPackage.getIsprop());
        teqipPackageHistory.setJustification(teqipPackage.getJustification());
        teqipPackageHistory.setPackageCode(teqipPackage.getPackageCode());
        teqipPackageHistory.setPackageName(teqipPackage.getPackageName());
        teqipPackageHistory.setRevisionComments(teqipPackage.getRevisionComments());
        teqipPackageHistory.setServiceProviderId(teqipPackage.getServiceProviderId());
        teqipPackageHistory.setTypeofplanCreator(teqipPackage.getTypeofplanCreator());
        teqipPackageHistory.setInitiateDate(teqipPackage.getInitiateDate());
        teqipPackageHistory.setPackageMetaData(teqipPackage.getPackageMetaData());
        teqipPackageHistory.setTeqipPackage(teqipPackage);
        teqipPackageHistory.setTeqipCategorymaster(teqipPackage.getTeqipCategorymaster());
        teqipPackageHistory.setTeqipSubcategorymaster(teqipPackage.getTeqipSubcategorymaster());
        teqipPackageHistory.setTeqipActivitiymaster(teqipPackage.getTeqipActivitiymaster());
        teqipPackageHistory.setTeqipProcurementmethod(teqipPackage.getTeqipProcurementmethod());
        teqipPackageHistory.setTeqipProcurementmaster(teqipPackage.getTeqipProcurementmaster());
        teqipPackageHistory.setTeqipInstitution(teqipPackage.getTeqipInstitution());
        teqipPackageHistory.setTeqipStatemaster(teqipPackage.getTeqipStatemaster());
        teqipPackageHistory.setTeqipPmssProcurementstage(teqipPackage.getTeqipPmssProcurementstage());
        teqipPackageHistory.setTeqipSubcomponentmaster(teqipPackage.getTeqipSubcomponentmaster());
    }

    public Integer getPackageHistoryId() {
        return packageHistoryId;
    }

    public void setPackageHistoryId(Integer packageHistoryId) {
        this.packageHistoryId = packageHistoryId;
    }


    public Integer getReviseId() {
        return reviseId;
    }

    public void setReviseId(Integer reviseId) {
        this.reviseId = reviseId;
    }

    public Double getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(Double estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public Date getFinancialSanctionDate() {
        return financialSanctionDate;
    }

    public void setFinancialSanctionDate(Date financialSanctionDate) {
        this.financialSanctionDate = financialSanctionDate;
    }

    public Boolean getIscoe() {
        return iscoe;
    }

    public void setIscoe(Boolean iscoe) {
        this.iscoe = iscoe;
    }

    public Boolean getIsgem() {
        return isgem;
    }

    public void setIsgem(Boolean isgem) {
        this.isgem = isgem;
    }

    public Boolean getIsprop() {
        return isprop;
    }

    public void setIsprop(Boolean isprop) {
        this.isprop = isprop;
    }

    public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getRevisionComments() {
        return revisionComments;
    }

    public void setRevisionComments(String revisionComments) {
        this.revisionComments = revisionComments;
    }

    public Integer getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(Integer serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public Integer getTypeofplanCreator() {
        return typeofplanCreator;
    }

    public void setTypeofplanCreator(Integer typeofplanCreator) {
        this.typeofplanCreator = typeofplanCreator;
    }

    public Date getInitiateDate() {
        return initiateDate;
    }

    public void setInitiateDate(Date initiateDate) {
        this.initiateDate = initiateDate;
    }

    public String getPackageMetaData() {
        return packageMetaData;
    }

    public void setPackageMetaData(String packageMetaData) {
        this.packageMetaData = packageMetaData;
    }

    public TeqipPackage getTeqipPackage() {
        return teqipPackage;
    }

    public void setTeqipPackage(TeqipPackage teqipPackage) {
        this.teqipPackage = teqipPackage;
    }

    public TeqipCategorymaster getTeqipCategorymaster() {
        return teqipCategorymaster;
    }

    public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
        this.teqipCategorymaster = teqipCategorymaster;
    }

    public TeqipSubcategorymaster getTeqipSubcategorymaster() {
        return teqipSubcategorymaster;
    }

    public void setTeqipSubcategorymaster(TeqipSubcategorymaster teqipSubcategorymaster) {
        this.teqipSubcategorymaster = teqipSubcategorymaster;
    }

    public TeqipActivitiymaster getTeqipActivitiymaster() {
        return teqipActivitiymaster;
    }

    public void setTeqipActivitiymaster(TeqipActivitiymaster teqipActivitiymaster) {
        this.teqipActivitiymaster = teqipActivitiymaster;
    }

    public TeqipProcurementmethod getTeqipProcurementmethod() {
        return teqipProcurementmethod;
    }

    public void setTeqipProcurementmethod(TeqipProcurementmethod teqipProcurementmethod) {
        this.teqipProcurementmethod = teqipProcurementmethod;
    }

    public TeqipProcurementmaster getTeqipProcurementmaster() {
        return teqipProcurementmaster;
    }

    public void setTeqipProcurementmaster(TeqipProcurementmaster teqipProcurementmaster) {
        this.teqipProcurementmaster = teqipProcurementmaster;
    }

    public TeqipInstitution getTeqipInstitution() {
        return teqipInstitution;
    }

    public void setTeqipInstitution(TeqipInstitution teqipInstitution) {
        this.teqipInstitution = teqipInstitution;
    }

    public TeqipStatemaster getTeqipStatemaster() {
        return teqipStatemaster;
    }

    public void setTeqipStatemaster(TeqipStatemaster teqipStatemaster) {
        this.teqipStatemaster = teqipStatemaster;
    }

    public TeqipPmssProcurementstage getTeqipPmssProcurementstage() {
        return teqipPmssProcurementstage;
    }

    public void setTeqipPmssProcurementstage(TeqipPmssProcurementstage teqipPmssProcurementstage) {
        this.teqipPmssProcurementstage = teqipPmssProcurementstage;
    }

    public TeqipSubcomponentmaster getTeqipSubcomponentmaster() {
        return teqipSubcomponentmaster;
    }

    public void setTeqipSubcomponentmaster(TeqipSubcomponentmaster teqipSubcomponentmaster) {
        this.teqipSubcomponentmaster = teqipSubcomponentmaster;
    }

    public List<TeqipItemGoodsDetailHistory> getTeqipItemGoodsDetails() {
        return teqipItemGoodsDetails;
    }

    public void setTeqipItemGoodsDetails(List<TeqipItemGoodsDetailHistory> teqipItemGoodsDetails) {
        this.teqipItemGoodsDetails = teqipItemGoodsDetails;
    }

    public List<TeqipItemWorkDetailHistory> getTeqipItemWorkDetails() {
        return teqipItemWorkDetails;
    }

    public void setTeqipItemWorkDetails(List<TeqipItemWorkDetailHistory> teqipItemWorkDetails) {
        this.teqipItemWorkDetails = teqipItemWorkDetails;
    }

}
