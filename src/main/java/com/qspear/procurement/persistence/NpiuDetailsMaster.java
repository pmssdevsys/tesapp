package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "teqip_npiumaster")
public class NpiuDetailsMaster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;
	@Column(name = "Name")
	private String name;
	@Column(name = "Code")
	private String code;
	@Column(name = "ProjectName")
	private String projectName;
	@Column(name = "CreditNumber")
	private String creditNumber;
	@Column(name = "EmailID")
	private String emailId;
	@Column(name = "Address")
	private String address;
	@Column(name = "ProjectCost")
	private Double projectCost;
	@Column(name = "AllocatedBudget")
	private Double allocatedBudget;
	@Column(name = "CREATED_BY")
	private Integer createdBy;
	@Column(name = "FaxNumber")
	private String faxNumber;
	@Column(name = "ISACTIVE")
	private Integer isactive;
	@Column(name = "Description")
	private String description;
	@Column(name = "MODIFY_BY")
	private Integer modifiedby;
	@Column(name = "MODIFY_ON")
	private Date modifiedOn;

	public Integer getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Timestamp createdon) {
		this.createdon = createdon;
	}

	@Column(name = "CREATED_ON")
	private Timestamp createdon;

	public Double getAllocatedBudget() {
		return allocatedBudget;
	}

	public void setAllocatedBudget(Double allocatedBudget) {
		this.allocatedBudget = allocatedBudget;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getCreditNumber() {
		return creditNumber;
	}

	public void setCreditNumber(String creditNumber) {
		this.creditNumber = creditNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getProjectCost() {
		return projectCost;
	}

	public void setProjectCost(Double projectCost) {
		this.projectCost = projectCost;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
