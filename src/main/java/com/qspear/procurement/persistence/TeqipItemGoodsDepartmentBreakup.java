/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.persistence;

import com.qspear.procurement.persistence.methods.AuditEntity;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author jaspreet
 */
@Entity
@Table(name = "teqip_item_goods_department_breakup")
public class TeqipItemGoodsDepartmentBreakup extends AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Integer departmentBreakUpId;

    @Column(name = "QUANTITY")
    private Double quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_DETAIL_ID")
    private TeqipItemGoodsDetail teqipItemGoodsDetail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_DEPARTMENT_ID")
    private TeqipPmssDepartmentMaster teqipPmssDepartmentMaster;
    
     private Integer reviseId;

    public Integer getDepartmentBreakUpId() {
        return departmentBreakUpId;
    }

    public void setDepartmentBreakUpId(Integer departmentBreakUpId) {
        this.departmentBreakUpId = departmentBreakUpId;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public TeqipPmssDepartmentMaster getTeqipPmssDepartmentMaster() {
        return teqipPmssDepartmentMaster;
    }

    public void setTeqipPmssDepartmentMaster(TeqipPmssDepartmentMaster teqipPmssDepartmentMaster) {
        this.teqipPmssDepartmentMaster = teqipPmssDepartmentMaster;
    }

    public TeqipItemGoodsDetail getTeqipItemGoodsDetail() {
        return teqipItemGoodsDetail;
    }

    public void setTeqipItemGoodsDetail(TeqipItemGoodsDetail teqipItemGoodsDetail) {
        this.teqipItemGoodsDetail = teqipItemGoodsDetail;
    }

    public Integer getReviseId() {
        return reviseId;
    }

    public void setReviseId(Integer reviseId) {
        this.reviseId = reviseId;
    }

}
