package com.qspear.procurement.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the teqip_pmss_procurementstages database table.
 *
 */
@Entity
@Table(name = "teqip_pmss_procurementstages")
@NamedQuery(name = "TeqipPmssProcurementstage.findAll", query = "SELECT t FROM TeqipPmssProcurementstage t")
public class TeqipPmssProcurementstage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "STAGEID")
    private Integer stageid;

    @Column(name = "PROCUREMENTSTAGEID")
    private Integer procurementstageid;

    private Boolean isactive;

    @Column(name = "ORDERNO")
    private Integer orderNo;

    @Column(name = "PROCUREMENTSTAGES")
    private String procurementstages;

    @Column(name = "ROLE_ID")
    private Integer roleId;

    @Column(name = "STAGE_DESCRIPTION")
    private String stageDescription;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "TYPE")
    private Integer type;

    private Integer revision;

    public TeqipPmssProcurementstage() {
    }

    public Integer getProcurementstageid() {
        return this.procurementstageid;
    }

    public void setProcurementstageid(Integer procurementstageid) {
        this.procurementstageid = procurementstageid;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public Integer getOrderNo() {
        return this.orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    public String getProcurementstages() {
        return this.procurementstages;
    }

    public void setProcurementstages(String procurementstages) {
        this.procurementstages = procurementstages;
    }

    public Integer getRoleId() {
        return this.roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getStageDescription() {
        return this.stageDescription;
    }

    public void setStageDescription(String stageDescription) {
        this.stageDescription = stageDescription;
    }

    public Integer getStageid() {
        return this.stageid;
    }

    public void setStageid(Integer stageid) {
        this.stageid = stageid;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

}
