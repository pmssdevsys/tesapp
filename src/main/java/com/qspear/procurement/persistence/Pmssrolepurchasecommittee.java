package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="teqip_pmssrolepurchasecommittee")
//@NamedQuery(name="Pmssrolepurchasecommittee.findAll", query="SELECT t FROM teqip_pmssrolepurchasecommittee t")
public class Pmssrolepurchasecommittee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name="PCRoleID")
	private Integer pcroleid;
	@Column(name="PCRoleName")
	private String pcroleName;
	@Column(name="CREATED_BY")
	private Integer createdby;
	@Column(name="CREATED_ON")
	private Integer createdon;
	@Column(name="MODIFY_ON")
	private Date modifiedon;
	@Column(name="MODIFY_BY")
	private Integer modifiedby;
	@Column(name="ISACTIVE")
	private Integer isactive;
	public Integer getPcroleid() {
		return pcroleid;
	}
	public void setPcroleid(Integer pcroleid) {
		this.pcroleid = pcroleid;
	}
	public String getPcroleName() {
		return pcroleName;
	}
	public void setPcroleName(String pcroleName) {
		this.pcroleName = pcroleName;
	}
	public Integer getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}
	public Integer getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Integer createdon) {
		this.createdon = createdon;
	}
	public Date getModifiedon() {
		return modifiedon;
	}
	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}
	public Integer getModifiedby() {
		return modifiedby;
	}
	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
