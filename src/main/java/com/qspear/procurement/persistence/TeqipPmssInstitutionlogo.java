package com.qspear.procurement.persistence;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSBudgetException;

/**
 * The persistent class for the teqip_pmss_institutionlogo database table.
 *
 */
@Entity
@Table(name = "teqip_pmss_institutionlogo")
@NamedQuery(name = "TeqipPmssInstitutionlogo.findAll", query = "SELECT t FROM TeqipPmssInstitutionlogo t")
public class TeqipPmssInstitutionlogo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer institutionLogoID;

    @ManyToOne
    @JoinColumn(name = "INSTITUTION_ID")
    private TeqipInstitution teqipInstitution;

    @ManyToOne
    @JoinColumn(name = "SPFUID")
    private TeqipStatemaster teqipStatemaster;

    @Column(name = "ISACTIVE")
    private Boolean isactice;

    @Column(name = "CREATED_BY")
    private Integer createdBy;

    @Column(name = "CREATED_ON")
    private Timestamp createdOn;

    private String description;

    private String originalFileName;

    private String systemFileName;

    @Column(name = "Modify_by")
    private Integer modifiedby;
    @Column(name = "type")
    private Integer typeNpcEdcil;

    public Integer getTypeNpcEdcil() {
        return typeNpcEdcil;
    }

    public void setTypeNpcEdcil(Integer typeNpcEdcil) {
        this.typeNpcEdcil = typeNpcEdcil;
    }
   

    public Integer getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }

    //bi-directional many-to-one association to TeqipInstitution
    public Boolean getIsactice() {
        return isactice;
    }

    public void setIsactice(Boolean isactice) {
        this.isactice = isactice;
    }

    public TeqipPmssInstitutionlogo() {
    }

    public Integer getInstitutionLogoID() {
        return this.institutionLogoID;
    }

    public void setInstitutionLogoID(Integer institutionLogoID) {
        this.institutionLogoID = institutionLogoID;
    }

    public Integer getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginalFileName() {
        return this.originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getSystemFileName() {
        return this.systemFileName;
    }

    public void setSystemFileName(String systemFileName) {
        this.systemFileName = systemFileName;
    }

    public TeqipInstitution getTeqipInstitution() {
        return this.teqipInstitution;
    }

    public void setTeqipInstitution(TeqipInstitution teqipInstitution) {
        this.teqipInstitution = teqipInstitution;
    }

    public TeqipStatemaster getTeqipStatemaster() {
        return teqipStatemaster;
    }

    public void setTeqipStatemaster(TeqipStatemaster teqipStatemaster) {
        this.teqipStatemaster = teqipStatemaster;
    }

    public File getFileLocation() {
        File file = new File(MethodConstants.UPLOAD_LOCATION
                + (this.getSystemFileName() != null && this.getSystemFileName().contains("Attachments")
                ? "" : File.separator + "logo")
                + File.separator + this.getSystemFileName());

        System.out.println(file);
        if (!file.exists()) {
            return null;
        }
        return file;
    }
}
