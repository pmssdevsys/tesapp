package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name="TEQIP_PMSS_suppliermaster")

public class SupplierMaster implements Serializable {

	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer supplierID;
	//@NotNull
	private String suppliername;
//	@NotNull
	private String supllieraddress;
//	@NotNull
	private String suppliercity;
	//@NotNull
	private String supplierstate;
	//@NotNull
	private String suppliercountry;
	//@NotNull
	private String suppliernationality;
	private String suppliersource;
	private String supplierspecificsource;
	private String supplierrepresentativename;
	//@NotNull
	private String supplierpincode;
	private String supplierphoneno;
	private String supliertannumber;
	private String suplierfaxnumber;
	private String suplierpannumber;
	private Integer pmss_type;
	private String supliergstnumber;
	private String supliertaxnumber;
	private String suplieremailid;
	private String supliereletter;
	private Timestamp CREATED_ON;
	private Integer CREATED_BY;
	private Date MODIFY_ON;
	private Integer MODIFY_BY;
	private Byte ISACTIVE;
	String epfNo;
    String esiNo;
    String contractLabourLicenseNo;

	
	public String getEpfNo() {
		return epfNo;
	}
	public void setEpfNo(String epfNo) {
		this.epfNo = epfNo;
	}
	public String getEsiNo() {
		return esiNo;
	}
	public void setEsiNo(String esiNo) {
		this.esiNo = esiNo;
	}
	public String getContractLabourLicenseNo() {
		return contractLabourLicenseNo;
	}
	public void setContractLabourLicenseNo(String contractLabourLicenseNo) {
		this.contractLabourLicenseNo = contractLabourLicenseNo;
	}
	public Integer getPmss_type() {
		return pmss_type;
	}
	public void setPmss_type(Integer pmss_type) {
		this.pmss_type = pmss_type;
	}
	//@NotNull
	
	public String getSupliertannumber() {
		return supliertannumber;
	}
	public void setSupliertannumber(String supliertannumber) {
		this.supliertannumber = supliertannumber;
	}
	public String getSuplierfaxnumber() {
		return suplierfaxnumber;
	}
	public void setSuplierfaxnumber(String suplierfaxnumber) {
		this.suplierfaxnumber = suplierfaxnumber;
	}
	public String getSuplierpannumber() {
		return suplierpannumber;
	}
	public void setSuplierpannumber(String suplierpannumber) {
		this.suplierpannumber = suplierpannumber;
	}
	public String getSupliergstnumber() {
		return supliergstnumber;
	}
	public void setSupliergstnumber(String supliergstnumber) {
		this.supliergstnumber = supliergstnumber;
	}
	public String getSupliertaxnumber() {
		return supliertaxnumber;
	}
	public void setSupliertaxnumber(String supliertaxnumber) {
		this.supliertaxnumber = supliertaxnumber;
	}
	public String getSuplieremailid() {
		return suplieremailid;
	}
	public void setSuplieremailid(String suplieremailid) {
		this.suplieremailid = suplieremailid;
	}
	public String getSupliereletter() {
		return supliereletter;
	}
	public void setSupliereletter(String supliereletter) {
		this.supliereletter = supliereletter;
	}
	
	
	public Integer getSupplierID() {
		return supplierID;
	}
	public void setSupplierID(Integer supplierID) {
		this.supplierID = supplierID;
	}
	public String getSuppliername() {
		return suppliername;
	}
	public void setSuppliername(String suppliername) {
		this.suppliername = suppliername;
	}
	public String getSupllieraddress() {
		return supllieraddress;
	}
	public void setSupllieraddress(String supllieraddress) {
		this.supllieraddress = supllieraddress;
	}
	public String getSuppliercity() {
		return suppliercity;
	}
	public void setSuppliercity(String suppliercity) {
		this.suppliercity = suppliercity;
	}
	public String getSupplierstate() {
		return supplierstate;
	}
	public void setSupplierstate(String supplierstate) {
		this.supplierstate = supplierstate;
	}
	public String getSuppliercountry() {
		return suppliercountry;
	}
	public void setSuppliercountry(String suppliercountry) {
		this.suppliercountry = suppliercountry;
	}
	public String getSuppliernationality() {
		return suppliernationality;
	}
	public void setSuppliernationality(String suppliernationality) {
		this.suppliernationality = suppliernationality;
	}
	public String getSuppliersource() {
		return suppliersource;
	}
	public void setSuppliersource(String suppliersource) {
		this.suppliersource = suppliersource;
	}
	public String getSupplierspecificsource() {
		return supplierspecificsource;
	}
	public void setSupplierspecificsource(String supplierspecificsource) {
		this.supplierspecificsource = supplierspecificsource;
	}
	public String getSupplierrepresentativename() {
		return supplierrepresentativename;
	}
	public void setSupplierrepresentativename(String supplierrepresentativename) {
		this.supplierrepresentativename = supplierrepresentativename;
	}
	public String getSupplierpincode() {
		return supplierpincode;
	}
	public void setSupplierpincode(String supplierpincode) {
		this.supplierpincode = supplierpincode;
	}
	public String getSupplierphoneno() {
		return supplierphoneno;
	}
	public void setSupplierphoneno(String supplierphoneno) {
		this.supplierphoneno = supplierphoneno;
	}
	public String getSuupliertannumber() {
		return supliertannumber;
	}
	public void setSuupliertannumber(String supliertannumber) {
		this.supliertannumber = supliertannumber;
	}
	public String getSuuplierfaxnumber() {
		return suplierfaxnumber;
	}
	public void setSuuplierfaxnumber(String suplierfaxnumber) {
		this.suplierfaxnumber = suplierfaxnumber;
	}
	public String getSuuplierpannumber() {
		return suplierpannumber;
	}
	public void setSuuplierpannumber(String suplierpannumber) {
		this.suplierpannumber = suplierpannumber;
	}
	public String getSuupliergstnumber() {
		return supliergstnumber;
	}
	public void setSuupliergstnumber(String supliergstnumber) {
		this.supliergstnumber = supliergstnumber;
	}
	public String getSuupliertaxnumber() {
		return supliertaxnumber;
	}
	public void setSuupliertaxnumber(String supliertaxnumber) {
		this.supliertaxnumber = supliertaxnumber;
	}
	public String getSuuplieremailid() {
		return suplieremailid;
	}
	public void setSuuplieremailid(String suplieremailid) {
		this.suplieremailid = suplieremailid;
	}
	public String getSuupliereletter() {
		return supliereletter;
	}
	public void setSuupliereletter(String supliereletter) {
		this.supliereletter = supliereletter;
	}
	public Timestamp getCREATED_ON() {
		return CREATED_ON;
	}
	public void setCREATED_ON(Timestamp cREATED_ON) {
		CREATED_ON = cREATED_ON;
	}
	public Integer getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(Integer cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public Date getMODIFY_ON() {
		return MODIFY_ON;
	}
	public void setMODIFY_ON(Date mODIFY_ON) {
		MODIFY_ON = mODIFY_ON;
	}
	public Integer getMODIFY_BY() {
		return MODIFY_BY;
	}
	public void setMODIFY_BY(Integer mODIFY_BY) {
		MODIFY_BY = mODIFY_BY;
	}
	public Byte getISACTIVE() {
		return ISACTIVE;
	}
	public void setISACTIVE(Byte iSACTIVE) {
		ISACTIVE = iSACTIVE;
	}
	
	

	
	
	
	
}
