package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the TEQIP_STATEMASTER database table.
 *
 */
@Entity
@Table(name = "TEQIP_STATEMASTER")
public class TeqipStatemaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "STATE_ID")
    private Integer stateId;

    private Boolean isactive;

    @Column(name = "STATE_CODE")
    private String stateCode;

    @Column(name = "STATE_NAME")
    private String stateName;

    @Column(name = "EmailID")
    String emailID;

    @Column(name = "PhoneNumber")
    String phoneNumber;

    @Column(name = "FaxNumber")
    String faxNumber;

    @Column(name = "Address")
    String address;

    @Column(name = "WebsiteURL")
    String websiteURL;
    
    @Column(name = "AllocatedBudget")
    Double allocatedBudget;

    //bi-directional many-to-one association to TeqipInstitution
    @OneToMany(mappedBy = "teqipStatemaster")
    private List<TeqipInstitution> teqipInstitutions;

    //bi-directional many-to-one association to TeqipPackage
    @OneToMany(mappedBy = "teqipStatemaster")
    private List<TeqipPackage> teqipPackages;

    //bi-directional many-to-one association to TeqipUsersMaster
    @OneToMany(mappedBy = "teqipStatemaster")
    private List<TeqipUsersMaster> teqipUsersMasters;

    public TeqipStatemaster() {
    }

    public Integer getStateId() {
        return this.stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Boolean getIsactive() {
        return this.isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateName() {
        return this.stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public List<TeqipInstitution> getTeqipInstitutions() {
        return this.teqipInstitutions;
    }

    public void setTeqipInstitutions(List<TeqipInstitution> teqipInstitutions) {
        this.teqipInstitutions = teqipInstitutions;
    }

    public TeqipInstitution addTeqipInstitution(TeqipInstitution teqipInstitution) {
        getTeqipInstitutions().add(teqipInstitution);
        teqipInstitution.setTeqipStatemaster(this);

        return teqipInstitution;
    }

    public TeqipInstitution removeTeqipInstitution(TeqipInstitution teqipInstitution) {
        getTeqipInstitutions().remove(teqipInstitution);
        teqipInstitution.setTeqipStatemaster(null);

        return teqipInstitution;
    }

    public List<TeqipPackage> getTeqipPackages() {
        return this.teqipPackages;
    }

    public void setTeqipPackages(List<TeqipPackage> teqipPackages) {
        this.teqipPackages = teqipPackages;
    }

    public TeqipPackage addTeqipPackage(TeqipPackage teqipPackage) {
        getTeqipPackages().add(teqipPackage);
        teqipPackage.setTeqipStatemaster(this);

        return teqipPackage;
    }

    public TeqipPackage removeTeqipPackage(TeqipPackage teqipPackage) {
        getTeqipPackages().remove(teqipPackage);
        teqipPackage.setTeqipStatemaster(null);

        return teqipPackage;
    }

    public List<TeqipUsersMaster> getTeqipUsersMasters() {
        return this.teqipUsersMasters;
    }

    public void setTeqipUsersMasters(List<TeqipUsersMaster> teqipUsersMasters) {
        this.teqipUsersMasters = teqipUsersMasters;
    }

    public TeqipUsersMaster addTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
        getTeqipUsersMasters().add(teqipUsersMaster);
        teqipUsersMaster.setTeqipStatemaster(this);

        return teqipUsersMaster;
    }

    public TeqipUsersMaster removeTeqipUsersMaster(TeqipUsersMaster teqipUsersMaster) {
        getTeqipUsersMasters().remove(teqipUsersMaster);
        teqipUsersMaster.setTeqipStatemaster(null);

        return teqipUsersMaster;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public Double getAllocatedBudget() {
        return allocatedBudget;
    }

    public void setAllocatedBudget(Double allocatedBudget) {
        this.allocatedBudget = allocatedBudget;
    }


}
