package com.qspear.procurement.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the TEQIP_INSTITUTIONTYPE database table.
 * 
 */
@Entity
@Table(name="TEQIP_INSTITUTIONTYPE")
public class TeqipInstitutiontype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="INSTITUTIONTYPE_ID")
	private Integer institutiontypeId;

	@Column(name="INSTITUTIONTYPE_NAME")
	private String institutiontypeName;

	private Boolean isactive;

	//bi-directional many-to-one association to TeqipInstitution
	@OneToMany(mappedBy="teqipInstitutiontype")
	private List<TeqipInstitution> teqipInstitutions;

	public TeqipInstitutiontype() {
	}

	public Integer getInstitutiontypeId() {
		return this.institutiontypeId;
	}

	public void setInstitutiontypeId(Integer institutiontypeId) {
		this.institutiontypeId = institutiontypeId;
	}

	public String getInstitutiontypeName() {
		return this.institutiontypeName;
	}

	public void setInstitutiontypeName(String institutiontypeName) {
		this.institutiontypeName = institutiontypeName;
	}

	public Boolean getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public List<TeqipInstitution> getTeqipInstitutions() {
		return this.teqipInstitutions;
	}

	public void setTeqipInstitutions(List<TeqipInstitution> teqipInstitutions) {
		this.teqipInstitutions = teqipInstitutions;
	}

	public TeqipInstitution addTeqipInstitution(TeqipInstitution teqipInstitution) {
		getTeqipInstitutions().add(teqipInstitution);
		teqipInstitution.setTeqipInstitutiontype(this);

		return teqipInstitution;
	}

	public TeqipInstitution removeTeqipInstitution(TeqipInstitution teqipInstitution) {
		getTeqipInstitutions().remove(teqipInstitution);
		teqipInstitution.setTeqipInstitutiontype(null);

		return teqipInstitution;
	}

}