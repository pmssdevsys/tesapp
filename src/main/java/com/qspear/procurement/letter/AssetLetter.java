/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDetail;
import com.qspear.procurement.persistence.methods.TeqipPackagePoContractGenereration;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrder;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageGRNDetailRepository;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jaspreet
 */
@Service
@Transactional
public class AssetLetter extends AbstractLetter {

    @Autowired
    TeqipPackageRepository teqipPackageRepository;

    @Autowired
    PackageGRNDetailRepository grnDetailRepository;

    public void generate(Integer packageId, String type, Map<String, String[]> param) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }

        Integer packageGRNDetailId = param.containsKey("packageGRNDetailId")
                ? Integer.valueOf(param.get("packageGRNDetailId")[0]) : null;
        if (packageGRNDetailId == null || packageGRNDetailId == 0) {
            throw new RuntimeException("packageGRNDetailId not Defined");

        }

        TeqipPackageGRNDetail gRNDetail = grnDetailRepository.findOne(packageGRNDetailId);
        if (gRNDetail == null) {
            throw new RuntimeException("GRN not Defined");

        }

        TeqipPmssSupplierMaster teqipPmssSupplierMaster = null;

        if (MethodConstants.DOC_CAT_ASSET_LETTER_NCB.equals(type)) {
            TeqipPackagePoContractGenereration findByTeqipPackage = poContractGenererationRepository.findByTeqipPackage(teqipPackage);
            if (findByTeqipPackage == null) {
                throw new RuntimeException("LOA not Defined");
            } else {
                teqipPmssSupplierMaster = findByTeqipPackage.getTeqipPmssSupplierMaster();
            }

        } else {
            TeqipPackagePurchaseOrder findByTeqipPackage = purchaseOrderRepository.findByTeqipPackage(teqipPackage);
            if (findByTeqipPackage == null) {
                throw new RuntimeException("PurchaseOrder not Defined");
            }

            teqipPmssSupplierMaster = gRNDetail.getTeqipPmssSupplierMaster();
        }

        if (gRNDetail.getAssetLetterDocument() != null) {
            this.deleteSpecificDoc(gRNDetail.getAssetLetterDocument());
        }

        Map<String, String> words = this.getWordMap(teqipPackage);
        this.setWordMap(words, teqipPmssSupplierMaster);

        if (gRNDetail.getTeqipPackageGRNItemDetails() != null && !gRNDetail.getTeqipPackageGRNItemDetails().isEmpty()) {
            this.setWordMap(words, gRNDetail.getTeqipPackageGRNItemDetails().get(0));
        }

        Map<String, String[][]> table = new HashMap<>();
        this.setTableMapGrnItemDetails(table,gRNDetail.getTeqipPackageGRNItemDetails());
        TeqipPackageDocument createDoc = this.createDoc(teqipPackage, teqipPmssSupplierMaster, type, type, words, table, null);
        gRNDetail.setAssetLetterDocument(createDoc);
        grnDetailRepository.saveAndFlush(gRNDetail);

    }

    private void setWordMap(Map<String, String> words, TeqipPackageGRNItemDetail get) {
        words.put("$$GRN_ITEM_PAGENUMBER$$", get.getPageNumber());
        words.put("$$GRN_ITEM_SERIAL_NUMBER$$", get.getSerialNumber());
        words.put("$$GRN_ITEM_REGISTORNAME$$", get.getStockRegistorName());

    }

}
