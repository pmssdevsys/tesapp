/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackagePaymentDetail;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public abstract class AbstractListMapper extends AbstractWordMapper {

    Map<String, String[]> getListMap(TeqipPackage packageDetail) {
        Map<String, String[]> words = new HashMap<>();

        this.setPaymentDetailList(words, packageDetail.getTeqipPackagePaymentDetails());
        return words;

    }

    void setPaymentDetailList(Map<String, String[]> words, List<TeqipPackagePaymentDetail> teqipPackagePaymentDetails) {

        if (teqipPackagePaymentDetails != null && !teqipPackagePaymentDetails.isEmpty()) {

            String[] value = new String[teqipPackagePaymentDetails.size()];
            int index = 0;
            for (TeqipPackagePaymentDetail packagePaymentDetail : teqipPackagePaymentDetails) {

                value[index] = (packagePaymentDetail.getPaymentDescription() + " - "
                        + AbstractWordMapper.getString(packagePaymentDetail.getMilestonePercentage()) + "% of total cost");
                index++;
            }

            words.put("$$PAYMENT_DETAILS$$", value);

        }
    }
}
