/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageBidDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageBidINSTAndSCC;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class SBDGenerateLetter extends AbstractLetter {

    public void generate(Integer packageId, String type, Map<String, String[]> param) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }

        TeqipPackageBidDetail bidDetail = packageBidDetailRepository.findByTeqipPackage(teqipPackage);
        TeqipPackageBidINSTAndSCC bidINSTAndSCC = packageBidINSTAndSCCRepository.findByTeqipPackage(teqipPackage);

        if (bidDetail == null || bidINSTAndSCC == null) {
            throw new RuntimeException("Bid Detail is missing");

        }

        this.deletePreviousDoc(teqipPackage, type);

        Map<String, String> words = this.getWordMap(teqipPackage);

        words.put("$$NUMBER_BIDDER_COUNT$$", AbstractWordMapper.getString(teqipPackage.getTeqipPackageSupplierDetails().size()));

        Map<String, String[][]> table = new HashMap<>();

        this.setTableMapSupplier(table, teqipPackage.getTeqipPackageSupplierDetails());
        this.setTableMapItemGoodsDetails(table, teqipPackage.getTeqipItemGoodsDetails());
        this.setTableMapPayment(table, teqipPackage.getTeqipPackagePaymentDetails());
        this.setTableMapItemWorksDetails(table,teqipPackage.getTeqipItemWorkDetails());
        this.setTableMapQuestionMaster(table, this.filterQuestion(teqipPackage.getTeqipPackageQuestionMasters(), "post_evaluation"), "$$POST_QUALIFICATION_QUESTION_TABLE$$");
        this.createDoc(teqipPackage, null, type, type, words, table, null);

    }

}
