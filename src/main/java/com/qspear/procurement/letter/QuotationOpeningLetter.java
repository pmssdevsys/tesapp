/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class QuotationOpeningLetter extends AbstractLetter {

  
    public void generate(Integer packageId, String type) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }

        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();

        if (teqipPackageSupplierDetails == null) {
            throw new RuntimeException("No Supplier Defined");

        }

        this.deletePreviousDoc(teqipPackage, type);

        Map<String, String> words = this.getWordMap(teqipPackage);

        Map<String, String[][]> table = new HashMap<>();
        
        this.setTableMapQuotationOpenning(table, teqipPackage);
        
        this.setTableMapInsPurchaseCommittee(table, teqipPackage.getTeqipInstitution() != null?
                teqipPackage.getTeqipInstitution().getTeqipPmssInstitutionpurchasecommittes(): null);

         this.setTableMapInstructionToConsultants(table, teqipPackage);
       this.setTableMapEOIOpenning(table, teqipPackage);
       
        
        TeqipPackageDocument createDoc = this.createDoc(teqipPackage, null, type,type, words, table, null);
       
        for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {
            teqipPackageSupplierDetail.setQuotationDocument(createDoc);
            packageSupplierRepository.saveAndFlush(teqipPackageSupplierDetail);

        }

    }

}
