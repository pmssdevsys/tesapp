/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAward;
import com.qspear.procurement.persistence.methods.TeqipPackageBidDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageInvitationLetter;
import com.qspear.procurement.persistence.methods.TeqipPackageLetterAcceptance;
import com.qspear.procurement.persistence.methods.TeqipPackagePoContractGenereration;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class LetterOfAcceptance extends AbstractLetter {

    public void generate(Integer packageId, String type) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }

        TeqipPmssSupplierMaster teqipPmssSupplierMaster = null;

        if (MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE_DC.equals(type)) {

            List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();
            if (teqipPackageSupplierDetails != null && !teqipPackageSupplierDetails.isEmpty()) {
                teqipPmssSupplierMaster = teqipPackageSupplierDetails.get(0).getTeqipPmssSupplierMaster();
            }

        }
        TeqipPackageBidDetail bidDetail = null;
        TeqipPackagePoContractGenereration contractGenereration = null;

        if (MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE_NCB.equals(type)) {
//
            bidDetail = packageBidDetailRepository.findByTeqipPackage(teqipPackage);
//
            contractGenereration = poContractGenererationRepository.findByTeqipPackage(teqipPackage);
//            if (contractGenereration == null) {
//                throw new RuntimeException("LOA not Defined");
//            }
//            teqipPmssSupplierMaster = contractGenereration.getTeqipPmssSupplierMaster();
//
//        } else {
        }
            TeqipPackageAward teqipPackageAward = packageAwardRepository.findByTeqipPackage(teqipPackage);

            if (teqipPackageAward != null) {
                teqipPmssSupplierMaster = teqipPackageAward.getTeqipPmssSupplierMaster();
            }

//        }

        if (teqipPmssSupplierMaster == null) {
            throw new RuntimeException("No Supplier Defined");
        }

        this.deletePreviousDoc(teqipPackage, type);

        Map<String, String> words = this.getWordMap(teqipPackage);
        this.setWordMap(words, letterOfAcceptanceRepository.findByTeqipPackage(teqipPackage));
        this.setWordMap(words, teqipPmssSupplierMaster);
        
        Date setLoaDate=this.setLoaDate(teqipPackage, teqipPmssSupplierMaster);   
        words.put("$$INV_LASTSUB_BIDDATE$$", setLoaDate != null ? dateformat.format(setLoaDate) : null);
        if (MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE_NCB.equals(type)) {
            if (bidDetail != null) {
             setLoaDate=this.setLoaDate(teqipPackage, teqipPmssSupplierMaster);   
             words.put("$$INV_LASTSUB_BIDDATE$$", setLoaDate != null ? dateformat.format(setLoaDate) : null);
            }
            if (contractGenereration != null) {
                words.put("$$LOA_SECURITY_AMOUNT$$", AbstractWordMapper.getString(contractGenereration.getPerfSecurityAmount()));
            }
        }
        
        
        
        Double setReadOutPrice = this.setReadOutPrice(teqipPackage, teqipPmssSupplierMaster);
        setReadOutPrice = setReadOutPrice==null ?0.0f:setReadOutPrice;
        
        if (MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE_DC.equals(type)) {
            TeqipPackageInvitationLetter invitationLetterDetail = inviationLetterRepository.findByTeqipPackage(teqipPackage);
            setReadOutPrice = invitationLetterDetail.getQuotedPrice();
            setReadOutPrice = setReadOutPrice==null ?0.0f:setReadOutPrice;
        }
        
        words.put("$$QUES_READ_OUT$$", AbstractWordMapper.getString(setReadOutPrice));
        words.put("$$QUES_READ_OUT_WORDS$$", LetterOfAcceptance.convertToIndianCurrency( AbstractWordMapper.getString(setReadOutPrice)));

        Map<String, String[][]> table = new HashMap<>();

        this.createDoc(teqipPackage, null, type, type, words, table, null);

    }

    private void setWordMap(Map<String, String> words, TeqipPackageLetterAcceptance letterAcceptance) {
        if (letterAcceptance == null) {
            return;
        }
        words.put("$$LOA_SECURITY_AMOUNT$$", AbstractWordMapper.getString(letterAcceptance.getSecurityAmount()));
        words.put("$$LOA_MAIN_EXPIRE_DATE$$", letterAcceptance.getMaintancePeriodExpiryDate() == null
                ? "" : dateformat.format(letterAcceptance.getMaintancePeriodExpiryDate()));

    }

}
