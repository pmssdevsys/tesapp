/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPmssInstitutionlogo;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.util.DocXReplaceText;
import com.qspear.procurement.util.MethodUtility;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jaspreet
 */
@Service
public abstract class AbstractLetter extends AbstractTableMapper {

	TeqipPackageDocument createDoc(TeqipPackage teqipPackage, TeqipPmssSupplierMaster teqipPmssSupplierMaster,
			String type, String fileName, Map<String, String> words, Map<String, String[][]> table,
			Map<String, String[]> listMap) throws IOException, InvalidFormatException {

		String packageDirectoryPath = MethodUtility.getDirectoryPath();

		Map<String, File> fileMap = this.getFileMap(teqipPackage);

		DocXReplaceText wordReplaceText = new DocXReplaceText(words, table, listMap, fileMap);

		File origFile = new File(packageDirectoryPath + type);
		if (!origFile.exists()) {
			origFile.mkdir();
		}
		String outputFileName = String.valueOf(new Date().getTime());

		wordReplaceText.generateDoc(packageDirectoryPath + "doc" + File.separator + type,
				packageDirectoryPath + type + File.separator + outputFileName);

		TeqipPackageDocument entity = new TeqipPackageDocument();
		entity.setDocumentCategory(type);
		entity.setDocumentfilename(type + File.separator + outputFileName + ".docx");
		if (teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode().equalsIgnoreCase("CQS")) {
			entity.setOriginalFileName(fileName.replaceAll("QCBS", "CQS")
					+ (teqipPmssSupplierMaster != null ? "_" + teqipPmssSupplierMaster.getSupplierId() : "") + ".docx");
		} else if (teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode().equalsIgnoreCase("LCS")) {
			entity.setOriginalFileName(fileName.replaceAll("QCBS", "LCS")
					+ (teqipPmssSupplierMaster != null ? "_" + teqipPmssSupplierMaster.getSupplierId() : "") + ".docx");
		} else if (teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode().equalsIgnoreCase("SSS")) {
			entity.setOriginalFileName(fileName.replaceAll("QCBS", "SSS")
					+ (teqipPmssSupplierMaster != null ? "_" + teqipPmssSupplierMaster.getSupplierId() : "") + ".docx");
		} else {
			entity.setOriginalFileName(fileName
					+ (teqipPmssSupplierMaster != null ? "_" + teqipPmssSupplierMaster.getSupplierId() : "") + ".docx");

		}
		entity.setTeqipPackage(teqipPackage);

		documentRepository.saveAndFlush(entity);
		return entity;
	}

	@Transactional
	void deletePreviousDoc(TeqipPackage teqipPackage, String type) {
		List<TeqipPackageDocument> previousDoc = documentRepository.findByTeqipPackageAndDocumentCategory(teqipPackage,
				type);

		documentRepository.deleteInBatch(previousDoc);
	}

	@Transactional
	void deleteSpecificDoc(TeqipPackageDocument document) {
		documentRepository.delete(document);
	}

	private Map<String, File> getFileMap(TeqipPackage teqipPackage) {

		TeqipInstitution teqipInstitution = teqipPackage.getTeqipInstitution();
		Map<String, File> fileMap = new HashMap<>();
		File f = null;
		if (teqipInstitution != null) {

			List<TeqipPmssInstitutionlogo> teqipPmssInstitutionlogos = teqipInstitution.getTeqipPmssInstitutionlogos();
			if (teqipPmssInstitutionlogos != null && !teqipPmssInstitutionlogos.isEmpty()) {

				TeqipPmssInstitutionlogo get = teqipPmssInstitutionlogos.get(teqipPmssInstitutionlogos.size() - 1);
				if (get != null) {

					File f1 = get.getFileLocation();
					if (f1 != null && f1.exists()) {

						f = f1;
					}
				}
			}
		}
		fileMap.put("$$INST_LOGO$$", f);
		return fileMap;
	}

	public static String convertToIndianCurrency(String num) {
		if (num == null)
			return "";

		BigDecimal bd = new BigDecimal(num);
		long number = bd.longValue();
		long no = bd.longValue();
		int decimal = (int) (bd.remainder(BigDecimal.ONE).doubleValue() * 100);
		int digits_length = String.valueOf(no).length();
		int i = 0;
		ArrayList<String> str = new ArrayList<>();
		HashMap<Integer, String> words = new HashMap<>();
		words.put(0, "");
		words.put(1, "One");
		words.put(2, "Two");
		words.put(3, "Three");
		words.put(4, "Four");
		words.put(5, "Five");
		words.put(6, "Six");
		words.put(7, "Seven");
		words.put(8, "Eight");
		words.put(9, "Nine");
		words.put(10, "Ten");
		words.put(11, "Eleven");
		words.put(12, "Twelve");
		words.put(13, "Thirteen");
		words.put(14, "Fourteen");
		words.put(15, "Fifteen");
		words.put(16, "Sixteen");
		words.put(17, "Seventeen");
		words.put(18, "Eighteen");
		words.put(19, "Nineteen");
		words.put(20, "Twenty");
		words.put(30, "Thirty");
		words.put(40, "Forty");
		words.put(50, "Fifty");
		words.put(60, "Sixty");
		words.put(70, "Seventy");
		words.put(80, "Eighty");
		words.put(90, "Ninety");
		String digits[] = { "", "Hundred", "Thousand", "Lakh", "Crore" };
		while (i < digits_length) {
			int divider = (i == 2) ? 10 : 100;
			number = no % divider;
			no = no / divider;
			i += divider == 10 ? 1 : 2;
			if (number > 0) {
				int counter = str.size();
				String plural = (counter > 0 && number > 9) ? "s" : "";
				String tmp = (number < 21) ? words.get(Integer.valueOf((int) number)) + " " + digits[counter] + plural
						: words.get(Integer.valueOf((int) Math.floor(number / 10) * 10)) + " "
								+ words.get(Integer.valueOf((int) (number % 10))) + " " + digits[counter] + plural;
				str.add(tmp);
			} else {
				str.add("");
			}
		}

		Collections.reverse(str);
		String Rupees = String.join(" ", str).trim();

		String paise = (decimal) > 0 ? " And Paise " + words.get(Integer.valueOf((int) (decimal - decimal % 10))) + " "
				+ words.get(Integer.valueOf((int) (decimal % 10))) : "";
		return "In Words:"+Rupees + paise + " Only";
	}

}
