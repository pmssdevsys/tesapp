/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageGRNDetailRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsEvaluationItemDetailRepository;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jaspreet
 */
@Service
@Transactional
public class GRNLetter extends AbstractLetter {

    @Autowired
    PackageGRNDetailRepository grnDetailRepository;

    @Autowired
    PackageQuotationsEvaluationItemDetailRepository quotationsEvaluationDetailRepository;

    public void generate(Integer packageId, String type, Map<String, String[]> param) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }

        Integer packageGRNDetailId = param.containsKey("packageGRNDetailId")
                ? Integer.valueOf(param.get("packageGRNDetailId")[0]) : null;
        if (packageGRNDetailId == null || packageGRNDetailId == 0) {
            throw new RuntimeException("packageGRNDetailId not Defined");

        }

        TeqipPackageGRNDetail gRNDetail = grnDetailRepository.findOne(packageGRNDetailId);
        if (gRNDetail == null) {
            throw new RuntimeException("GRN not Defined");

        }

        TeqipPmssSupplierMaster teqipPmssSupplierMaster = gRNDetail.getTeqipPmssSupplierMaster();

        if (gRNDetail.getGrnLetterDocument() != null) {
            this.deleteSpecificDoc(gRNDetail.getGrnLetterDocument());
        }

        Map<String, String> words = this.getWordMap(teqipPackage);
        this.setWordMap(words, teqipPmssSupplierMaster);
        this.setWordMap(words, gRNDetail);

        Map<String, String[][]> table = new HashMap<>();
        this.setTableMapGRNItem(table, words, gRNDetail, teqipPackage, teqipPmssSupplierMaster);

        TeqipPackageDocument createDoc = this.createDoc(teqipPackage, teqipPmssSupplierMaster, type, type, words, table, null);
        gRNDetail.setGrnLetterDocument(createDoc);
        grnDetailRepository.saveAndFlush(gRNDetail);
    }

    private void setWordMap(Map<String, String> words, TeqipPackageGRNDetail gRNDetail) {
        words.put("$$GRN_SUPPLYDATE$$", gRNDetail.getSupplyDate() != null
                ? dateformat.format(gRNDetail.getSupplyDate()) : null);
    }

}
