/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class InvitationLetterQCBS extends AbstractLetter {
    
    @Autowired
    TeqipPackageRepository teqipPackageRepository;
    
    @Autowired
    PackageSupplierRepository packageSupplierRepository;
    
    public void generate(Integer packageId, String type) throws IOException, InvalidFormatException {
        
        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);
        
        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }
        
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();
        
       if (MethodConstants.DOC_CAT_INVITATION_FINANCIAL_QCBS.equalsIgnoreCase(type)) {
            teqipPackageSupplierDetails =   getNonResponsivePackageSupplier(teqipPackage);
       }
        
        if (teqipPackageSupplierDetails == null) {
            throw new RuntimeException("No Supplier Defined");
            
        }
        
        this.deletePreviousDoc(teqipPackage, type);
        
        Map<String, String> words = this.getWordMap(teqipPackage);
        Map<String, String[][]> table = new HashMap<>();
        this.setTableMapSupplier(table, teqipPackage.getTeqipPackageSupplierDetails());
        this.setTableMapInstructionToConsultants(table, teqipPackage);
        this.setTableMapInsPurchaseCommittee(table, teqipPackage.getTeqipInstitution() != null
                ? teqipPackage.getTeqipInstitution().getTeqipPmssInstitutionpurchasecommittes() : null);        
        this.setTableMapEOIOpenning(table, teqipPackage);
        
        
        for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {
            
            
            TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
            this.setWordMap(words, teqipPmssSupplierMaster);
            
            TeqipPackageDocument createDoc = this.createDoc(teqipPackage, teqipPmssSupplierMaster, type, type, words, table, null);
            
            if (MethodConstants.DOC_CAT_INVITATION_QCBS.equalsIgnoreCase(type)) {
                teqipPackageSupplierDetail.setInvitationDocument(createDoc);
            }
            if (MethodConstants.DOC_CAT_INVITATION_FINANCIAL_QCBS.equalsIgnoreCase(type)) {
                teqipPackageSupplierDetail.setInvitationFinancialDocument(createDoc);
            }
            
            packageSupplierRepository.saveAndFlush(teqipPackageSupplierDetail);
        }
        
    }
    
}
