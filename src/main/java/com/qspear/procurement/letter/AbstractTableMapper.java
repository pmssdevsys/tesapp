/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipItemGoodsDetail;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipItemWorkDetail;
import com.qspear.procurement.persistence.TeqipPmssInstitutionpurchasecommitte;
import com.qspear.procurement.persistence.methods.TeqipPackageBidDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageBidINSTAndSCC;
import com.qspear.procurement.persistence.methods.TeqipPackageConsultantEvaluation;
import com.qspear.procurement.persistence.methods.TeqipPackageFinancialOpeningData;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageInvitationLetter;
import com.qspear.procurement.persistence.methods.TeqipPackagePaymentDetail;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrderDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQuestionMaster;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageTechSubCriteria;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.ConsultantEvaluationRepository;
import com.qspear.procurement.repositories.method.FinancialOpeningDataRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public abstract class AbstractTableMapper extends AbstractListMapper {

    @Autowired
    FinancialOpeningDataRepository financialOpeningDataRepository;
    @Autowired
    ConsultantEvaluationRepository consultantEvaluationRepository;

    void setTableMapItemGoodsDetails(Map<String, String[][]> table, List<TeqipItemGoodsDetail> teqipItemGoodsDetails) {

        if (teqipItemGoodsDetails != null && teqipItemGoodsDetails.size() > 0) {

            String[][] itemDetail = new String[teqipItemGoodsDetails.size() + 1][5];
            itemDetail[0] = new String[]{"Sr. No", "Item Name", "Quantity",
//                "Delivery Period(In days)",
                "Place of Delivery", "Installation Requirement (if any)"};

            String[][] itemDetailSBD = new String[teqipItemGoodsDetails.size() + 1][4];
            itemDetailSBD[0] = new String[]{"Brief Description", "Unit Price", "Quantity", "Delivery schedule"};

            String[][] itemSpecification = new String[teqipItemGoodsDetails.size() + 1][3];
            itemSpecification[0] = new String[]{"Sr. No", "Item Name", "Specifications"};

            String[][] itemQtytable = new String[teqipItemGoodsDetails.size() + 1][3];
            itemQtytable[0] = new String[]{"Sr. No", "Item Name", "Quantity"};

            int index = 1;
            for (TeqipItemGoodsDetail teqipItemGoodsDetail : teqipItemGoodsDetails) {
                itemSpecification[index] = new String[]{AbstractWordMapper.getString(index),
                    teqipItemGoodsDetail.getTeqipItemMaster().getItemName(),
                    teqipItemGoodsDetail.getItemMainSpecification()
                };
                itemQtytable[index] = new String[]{AbstractWordMapper.getString(index),
                    teqipItemGoodsDetail.getTeqipItemMaster().getItemName(),
                    AbstractWordMapper.getString(teqipItemGoodsDetail.getItemQnt())
                };

                itemDetail[index] = new String[]{AbstractWordMapper.getString(index),
                    teqipItemGoodsDetail.getTeqipItemMaster().getItemName(),
                    AbstractWordMapper.getString(teqipItemGoodsDetail.getItemQnt()),
//                    teqipItemGoodsDetail.getDeliveyPeriod() == null ? "" : AbstractWordMapper.getString(teqipItemGoodsDetail.getDeliveyPeriod()),
                    teqipItemGoodsDetail.getPlaceOfDelivery(),
                    teqipItemGoodsDetail.getInstllationRequirement()

                };
                itemDetailSBD[index] = new String[]{
                    teqipItemGoodsDetail.getTeqipItemMaster().getItemName(),
                    AbstractWordMapper.getString(teqipItemGoodsDetail.getItemCostUnit()),
                    AbstractWordMapper.getString(teqipItemGoodsDetail.getItemQnt()),
                    teqipItemGoodsDetail.getDeliveyPeriod() == null ? "" : AbstractWordMapper.getString(teqipItemGoodsDetail.getDeliveyPeriod()),
                    ""
                };
                index++;
            }

            table.put("$$ITEM_SPECIFICATION_TABLE$$", itemSpecification);
            table.put("$$ITEM_QTY_TABLE$$", itemQtytable);
            table.put("$$ITEM_DETAIL_TABLE$$", itemDetail);
            table.put("$$ITEM_DETAIL_TABLE_SBD$$", itemDetailSBD);

        } else {
            table.put("$$ITEM_DETAIL_TABLE$$", null);
        }
    }

    void setTableMapSupplier(Map<String, String[][]> table, List<TeqipPackageSupplierDetail> packageSupplierDetails) {
        if (packageSupplierDetails != null && !packageSupplierDetails.isEmpty()) {

            ArrayList<TeqipPackageFinancialOpeningData> financialOpeningDatas = financialOpeningDataRepository.findByTeqipPackage(packageSupplierDetails.get(0).getTeqipPackage());

            String[][] supplierDetail = new String[packageSupplierDetails.size() + 1][3];
            supplierDetail[0] = new String[]{"Sr. No", "Bidder Name", "Nationality"};

            String[][] supplierDetailWithReadOut = new String[packageSupplierDetails.size() + 1][5];
            supplierDetailWithReadOut[0] = new String[]{"Sr. No", "Bidder Name", "Nationality", "Bid Price as Evaluated Price", "Remarks"};

            String[][] supplierDetailWithReason = new String[packageSupplierDetails.size() + 1][5];
            supplierDetailWithReason[0] = new String[]{"Sr. No", "Bidder Name", "Nationality", "Bid Price as Evaluated Price", "Reason"};

            String[][] supplierAddressDetail = new String[packageSupplierDetails.size() + 1][4];
            supplierAddressDetail[0] = new String[]{"Name", "Address", "Contact", "Email"};

            String[][] supplierTechnicalScore = new String[packageSupplierDetails.size() + 1][6];
            supplierTechnicalScore[0] = new String[]{"Name", "Price", "Technical Score", "Financial Score", "Combined Score", "Rank"};

            String[][] consultanttable = new String[packageSupplierDetails.size() + 1][3];
            consultanttable[0] = new String[]{"Sr. No", "Consultant(s)", "Email"};

            String[][] financialOpening = new String[2][packageSupplierDetails.size() + 1];
            financialOpening[0][0] = "Consultant Name";
            financialOpening[1][0] = "Price in INR";

            int index = 1;
            for (TeqipPackageSupplierDetail teqipItemGoodsDetail : packageSupplierDetails) {

                TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipItemGoodsDetail.getTeqipPmssSupplierMaster();

                List<TeqipPackageFinancialOpeningData> collect = financialOpeningDatas.stream().filter((object) -> {
                    return object.getTeqipPmssSupplierMaster() != null && teqipPmssSupplierMaster != null
                            && Objects.equals(teqipPmssSupplierMaster.getSupplierId(), object.getTeqipPmssSupplierMaster().getSupplierId());
                }).collect(Collectors.toList());

                supplierDetail[index] = new String[]{AbstractWordMapper.getString(index),
                    teqipPmssSupplierMaster.getSupplierName(),
                    teqipPmssSupplierMaster.getSupplierNationality()
                };

                Double setReadOutPrice = setReadOutPrice(teqipItemGoodsDetail.getTeqipPackage(), teqipPmssSupplierMaster);
                supplierDetailWithReadOut[index] = new String[]{AbstractWordMapper.getString(index),
                    teqipPmssSupplierMaster.getSupplierName(),
                    teqipPmssSupplierMaster.getSupplierNationality(),
                    setReadOutPrice != null ? AbstractWordMapper.getString(setReadOutPrice) : "",
                    teqipPmssSupplierMaster.getRemarks()
                };
                supplierDetailWithReason[index] = new String[]{AbstractWordMapper.getString(index),
                    teqipPmssSupplierMaster.getSupplierName(),
                    teqipPmssSupplierMaster.getSupplierNationality(),
                    setReadOutPrice != null ? AbstractWordMapper.getString(setReadOutPrice) : "",
                    ""
                };
                supplierAddressDetail[index] = new String[]{
                    teqipPmssSupplierMaster.getSupplierName(),
                    teqipPmssSupplierMaster.getSupllierAddress() + ", " + teqipPmssSupplierMaster.getSupplierCity() + ", " + teqipPmssSupplierMaster.getSupplierState(),
                    teqipPmssSupplierMaster.getSupplierPhoneno(),
                    teqipPmssSupplierMaster.getSuplierEmailId()
                };

                TeqipPackageFinancialOpeningData financialOpeningData = null;
                if (collect != null && !collect.isEmpty()) {
                    financialOpeningData = collect.get(0);
                }
                supplierTechnicalScore[index] = new String[]{
                    teqipPmssSupplierMaster.getSupplierName(),
                    financialOpeningData != null ? AbstractWordMapper.getString(financialOpeningData.getPrice()) : null,
                    financialOpeningData != null ? AbstractWordMapper.getString(financialOpeningData.getTechnicalScore()) : null,
                    financialOpeningData != null ? AbstractWordMapper.getString(financialOpeningData.getFinancialScore()) : null,
                    financialOpeningData != null ? AbstractWordMapper.getString(financialOpeningData.getTotalScore()) : null,
                    financialOpeningData != null ? AbstractWordMapper.getString(financialOpeningData.getRank()) : null
                };

                consultanttable[index] = new String[]{AbstractWordMapper.getString(index),
                    teqipPmssSupplierMaster.getSupplierName(),
                    teqipPmssSupplierMaster.getSuplierEmailId()
                };

                financialOpening[0][index] = teqipPmssSupplierMaster.getSupplierName();
                financialOpening[1][index] = financialOpeningData != null ? AbstractWordMapper.getString(financialOpeningData.getPrice()) : null;

                index++;
            }

            table.put("$$BID_SUPPLIER_TABLE$$", supplierDetail);

            table.put("$$BID_SUPPLIER_TABLE_WITH_REMARK$$", supplierDetailWithReadOut);

            table.put("$$BID_SUPPLIER_TABLE_WITH_REASON$$", supplierDetailWithReason);

            table.put("$$SUPPLIER_TABLE_WITH_ADDR$$", supplierAddressDetail);

            table.put("$$SUPPLIER_TABLE_WITH_TECHNICAL_SCORE$$", supplierTechnicalScore);

            table.put("$$CONSULATANT_TABLE$$", consultanttable);

            table.put("$$FINANCIAL_OPENING_TABLE$$", financialOpening);
        }
    }

    void setTableMapItemWorksDetails(Map<String, String[][]> table, List<TeqipItemWorkDetail> teqipItemWorkDetails) {
        if (teqipItemWorkDetails != null && !teqipItemWorkDetails.isEmpty()) {
            TeqipPackageInvitationLetter invitationLetter = inviationLetterRepository.findByTeqipPackage(teqipItemWorkDetails.get(0).getTeqipPackage());

            String[][] itemDetail = new String[teqipItemWorkDetails.size() + 1][4];
            itemDetail[0] = new String[]{"Sr. No", "Brief Description of the Works ", "Approximate value of Works (Rs.)  ", "Period of Completion (In Days)"};

            int index = 1;
            for (TeqipItemWorkDetail teqipItemGoodsDetail : teqipItemWorkDetails) {

                itemDetail[index] = new String[]{AbstractWordMapper.getString(index),
                    teqipItemGoodsDetail.getWorkName(),
                    AbstractWordMapper.getString(teqipItemGoodsDetail.getWorkCost()),
                    invitationLetter == null ? AbstractWordMapper.getString(teqipItemGoodsDetail.getDeliveyPeriod()) : AbstractWordMapper.getString(invitationLetter.getWorkCompletionPeriod())

                };

                index++;
            }

            table.put("$$WORK_DETAIL_TABLE$$", itemDetail);

        }
    }

    void setTableMapQuestionMaster(Map<String, String[][]> table, List<TeqipPackageQuestionMaster> questionMasters, String key) {
        if (questionMasters != null) {

            String[][] itemDetail = new String[questionMasters.size() + 1][2];
            itemDetail[0] = new String[]{"Sr. No", "Questions"};

            int index = 1;
            for (TeqipPackageQuestionMaster master : questionMasters) {

                itemDetail[index] = new String[]{AbstractWordMapper.getString(index),
                    master.getQuestionDesc()
                };

                index++;
            }

            table.put(key, itemDetail);

        }
    }

    void setTableMapQuestionAnswerMaster(Map<String, String[][]> table, TeqipPmssSupplierMaster teqipPmssSupplierMaster, TeqipPackage packageDetail, List<TeqipPackageQuestionMaster> questionMasters, String key) {
        if (questionMasters != null) {

            String[][] itemDetail = new String[questionMasters.size() + 1][3];
            itemDetail[0] = new String[]{"Sr. No", "Questions", "Response"};

            int index = 1;
            for (TeqipPackageQuestionMaster master : questionMasters) {
                String inputCheck = "";
                TeqipPackageQuotationsSupplierInput input = supplierInputRepository.findFirstByTeqipPackageAndTeqipPackageQuestionMasterAndTeqipPmssSupplierMaster(
                        packageDetail, master, teqipPmssSupplierMaster);

                if (input != null) {
                    if ("date".equals(master.getType())) {
                        try {
                            inputCheck = dateformat.format(new Date(Long.valueOf(input.getUserResponse())));
                        } catch (Exception e) {
                            inputCheck = input.getUserResponse();
                        }
                    } else if ("float".equals(master.getType())) {
                        try {
                            inputCheck = AbstractWordMapper.getString(Double.parseDouble(input.getUserResponse()));
                        } catch (Exception e) {
                            inputCheck = input.getUserResponse();
                        }
                    } else {
                        inputCheck = input.getUserResponse();
                    }
                }
                itemDetail[index] = new String[]{AbstractWordMapper.getString(index),
                    master.getQuestionDesc(), inputCheck
                };

                index++;
            }

            table.put(key, itemDetail);

        }
    }

    void setTableMapTechEvaluation(Map<String, String[][]> table, TeqipPackage teqipPackage) {
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();
        List<TeqipPackageConsultantEvaluation> teqipPackageConsultantEvaluations = consultantEvaluationRepository.findByTeqipPackage(teqipPackage);

        teqipPackageSupplierDetails = teqipPackageSupplierDetails.stream() // convert list to stream
                .filter(line ->  line.getIsShortListed() != null && line.getIsShortListed() == 1)
                .collect(Collectors.toList());

        String[][] itemDetail = new String[((teqipPackageSupplierDetails.size() * 2) + teqipPackageConsultantEvaluations.size()) + 1][4];
        itemDetail[0] = new String[]{"Consultants", "Technical Criteria", "Max Marks", "Score"};

        int index = 1;
        for (TeqipPackageSupplierDetail supplier : teqipPackageSupplierDetails) {

            itemDetail[index] = new String[]{
                supplier.getTeqipPmssSupplierMaster().getSupplierName(),
                "", "", ""
            };
            index++;

            List<TeqipPackageConsultantEvaluation> result = teqipPackageConsultantEvaluations.stream() // convert list to stream
                    .filter(line -> Objects.equals(line.getTeqipPmssSupplierMaster().getSupplierId(),
                    supplier.getTeqipPmssSupplierMaster().getSupplierId())) // we dont like mkyong
                    .collect(Collectors.toList());
            Double total = 0.0;
            for (TeqipPackageConsultantEvaluation teqipPackageConsultantEvaluation : result) {
                TeqipPackageTechSubCriteria teqipPackageTechSubCriteria = teqipPackageConsultantEvaluation.getTeqipPackageTechSubCriteria();
                itemDetail[index] = new String[]{"",
                    teqipPackageTechSubCriteria.getTechCriteriaDesc(),
                    AbstractWordMapper.getString(teqipPackageTechSubCriteria.getMarks()),
                    AbstractWordMapper.getString(teqipPackageConsultantEvaluation.getMarks())};
                index++;
                total += teqipPackageConsultantEvaluation.getMarks();
            }
            itemDetail[index] = new String[]{
                "Total",
                "", "", AbstractWordMapper.getString(total)
            };

            index++;
        }

        table.put("$$TECH_EVALUATION_TABLE$$", itemDetail);

    }

    void setTableMapInsPurchaseCommittee(Map<String, String[][]> table, List<TeqipPmssInstitutionpurchasecommitte> institutionpurchasecommittes) {
        if (institutionpurchasecommittes != null) {

            String[][] itemDetail = new String[institutionpurchasecommittes.size() + 1][5];
            itemDetail[0] = new String[]{"Sr. No", "Member", "Department", "Designation", "Role"};

            int index = 1;
            for (TeqipPmssInstitutionpurchasecommitte committe : institutionpurchasecommittes) {

                itemDetail[index] = new String[]{
                    AbstractWordMapper.getString(index),
                    committe.getCommitteMemberName(),
                    committe.getTeqipPmssdepartmentmaster() != null ? committe.getTeqipPmssdepartmentmaster().getDepartment() : "",
                    committe.getDesignation(),
                    committe.getPCommitteRole()
                };

                index++;
            }

            table.put("$$INST_PURCHASE_COMMITEE$$", itemDetail);

        }
    }

    void setTableMapQuotationOpenning(Map<String, String[][]> table, TeqipPackage packageDetail) {
        List<TeqipPackageQuestionMaster> teqipPackageQuestionMasters = packageQuestionRepository
                .findByTeqipPackageAndQuestionCategory(packageDetail, "opening");

        List<TeqipPmssSupplierMaster> nonResponsiveSupplier = new ArrayList<>();
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = packageDetail.getTeqipPackageSupplierDetails();
        if (teqipPackageSupplierDetails != null) {

            for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {
                nonResponsiveSupplier.add(teqipPackageSupplierDetail.getTeqipPmssSupplierMaster());
            }
        }
        if (teqipPackageQuestionMasters != null) {

            String[][] itemDetail = new String[teqipPackageQuestionMasters.size() + 1][nonResponsiveSupplier.size() + 1];

            itemDetail[0][0] = "Questions";

            int index = 1;
            for (TeqipPmssSupplierMaster teqipPackageSupplierDetail : nonResponsiveSupplier) {

                itemDetail[0][index] = teqipPackageSupplierDetail.getSupplierName();
                index++;
            }

            index = 1;
            for (TeqipPackageQuestionMaster teqipPackageQuestionMaster : teqipPackageQuestionMasters) {

                itemDetail[index][0] = teqipPackageQuestionMaster.getQuestionDesc();

                int supindex = 1;
                for (TeqipPmssSupplierMaster teqipPmssSupplierMaster : nonResponsiveSupplier) {

                    TeqipPackageQuotationsSupplierInput input = supplierInputRepository.findFirstByTeqipPackageAndTeqipPackageQuestionMasterAndTeqipPmssSupplierMaster(
                            packageDetail, teqipPackageQuestionMaster, teqipPmssSupplierMaster);

                    if (input != null) {
                        if ("date".equals(teqipPackageQuestionMaster.getType())) {
                            try {
                                itemDetail[index][supindex] = dateformat.format(new Date(Long.valueOf(input.getUserResponse())));
                            } catch (Exception e) {
                                itemDetail[index][supindex] = input.getUserResponse();
                            }
                        } else if ("float".equals(teqipPackageQuestionMaster.getType())) {
                            try {
                                itemDetail[index][supindex] = AbstractWordMapper.getString(Double.parseDouble(input.getUserResponse()));
                            } catch (Exception e) {
                                itemDetail[index][supindex] = input.getUserResponse();
                            }
                        } else {
                            itemDetail[index][supindex] = input.getUserResponse();
                        }
                    }

                    supindex++;
                }

                index++;
            }

            table.put("$$QUOTATION_OPENING_TABLE$$", itemDetail);

        }

    }

    void setTableMapEOIOpenning(Map<String, String[][]> table, TeqipPackage packageDetail) {

        List<TeqipPmssSupplierMaster> nonResponsiveSupplier = new ArrayList<>();
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = packageDetail.getTeqipPackageSupplierDetails();
        if (teqipPackageSupplierDetails != null) {

            for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {
                nonResponsiveSupplier.add(teqipPackageSupplierDetail.getTeqipPmssSupplierMaster());
            }
        }

        String[][] itemDetail = new String[6][nonResponsiveSupplier.size() + 1];

        itemDetail[0][0] = "Question";
        itemDetail[1][0] = "Is EOI Received?";
        itemDetail[2][0] = "Is Annual turnover during past years is as mentioned in EOI?";
        itemDetail[3][0] = "Has min. number of regular employees as required?";
        itemDetail[4][0] = "Have completed similar projects in past years is as mentioned in EOI?";
        itemDetail[5][0] = "Have relevant experience in geographical region.";

        int index = 1;
        for (TeqipPmssSupplierMaster teqipPackageSupplierDetail : nonResponsiveSupplier) {

            itemDetail[0][index] = teqipPackageSupplierDetail.getSupplierName();
            itemDetail[1][index] = "Yes";
            index++;
        }

        table.put("$$EOI_OPENING_TABLE$$", itemDetail);

    }

    void setTableMapQuotationEvaluation(Map<String, String[][]> table, TeqipPackage packageDetail) {

        List<TeqipPackageQuestionMaster> teqipPackageQuestionMasters = packageQuestionRepository
                .findByTeqipPackageAndQuestionCategory(packageDetail, "evaluation");

        List<TeqipPmssSupplierMaster> nonResponsiveSupplier = getNonResponsiveSupplier(packageDetail);

        if (teqipPackageQuestionMasters != null && nonResponsiveSupplier != null) {

            String[][] itemDetail = new String[teqipPackageQuestionMasters.size() + 1][nonResponsiveSupplier.size() + 1];

            itemDetail[0][0] = "Questions";

            int index = 1;
            for (TeqipPmssSupplierMaster teqipPackageSupplierDetail : nonResponsiveSupplier) {

                itemDetail[0][index] = teqipPackageSupplierDetail.getSupplierName();
                index++;
            }

            index = 1;
            for (TeqipPackageQuestionMaster teqipPackageQuestionMaster : teqipPackageQuestionMasters) {

                itemDetail[index][0] = teqipPackageQuestionMaster.getQuestionDesc();

                int supindex = 1;
                for (TeqipPmssSupplierMaster teqipPmssSupplierMaster : nonResponsiveSupplier) {

                    TeqipPackageQuotationsSupplierInput input = supplierInputRepository.findFirstByTeqipPackageAndTeqipPackageQuestionMasterAndTeqipPmssSupplierMaster(
                            packageDetail, teqipPackageQuestionMaster, teqipPmssSupplierMaster);

                    if (input != null) {
                        if ("date".equals(teqipPackageQuestionMaster.getType())) {
                            try {
                                itemDetail[index][supindex] = dateformat.format(new Date(Long.valueOf(input.getUserResponse())));
                            } catch (Exception e) {
                                itemDetail[index][supindex] = input.getUserResponse();
                            }
                        } else if ("float".equals(teqipPackageQuestionMaster.getType())) {
                            try {
                                itemDetail[index][supindex] = AbstractWordMapper.getString(Double.parseDouble(input.getUserResponse()));
                            } catch (Exception e) {
                                itemDetail[index][supindex] = input.getUserResponse();
                            }
                        } else {
                            itemDetail[index][supindex] = input.getUserResponse();
                        }
                    }

                    supindex++;
                }

                index++;
            }

            table.put("$$QUOTATION_EVALUATION_TABLE$$", itemDetail);

        }
    }

    void setTableMapInstructionToConsultantsQCBS(Map<String, String[][]> table, TeqipPackage packageDetail) {

        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = packageDetail.getTeqipPackageSupplierDetails();
        teqipPackageSupplierDetails = teqipPackageSupplierDetails.stream() // convert list to stream
                .filter(line ->  line.getIsShortListed() != null &&  line.getIsShortListed() == 1)
                .collect(Collectors.toList());

        List<TeqipPackageQuestionMaster> teqipPackageQuestionMasters = packageQuestionRepository
                .findByTeqipPackageAndQuestionCategory(packageDetail, "instructionToConsultants");


        if (teqipPackageQuestionMasters != null && teqipPackageSupplierDetails != null) {

            String[][] itemDetail = new String[teqipPackageQuestionMasters.size() + 1][teqipPackageSupplierDetails.size() + 1];

            itemDetail[0][0] = "Questions";

            int index = 1;
            for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {

                TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
                itemDetail[0][index] = teqipPmssSupplierMaster.getSupplierName();
                index++;
            }

            index = 1;
            for (TeqipPackageQuestionMaster teqipPackageQuestionMaster : teqipPackageQuestionMasters) {

                itemDetail[index][0] = teqipPackageQuestionMaster.getQuestionDesc();

                int supindex = 1;
                for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {

                    TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();

                    TeqipPackageQuotationsSupplierInput input = supplierInputRepository.findFirstByTeqipPackageAndTeqipPackageQuestionMasterAndTeqipPmssSupplierMaster(
                            packageDetail, teqipPackageQuestionMaster, teqipPmssSupplierMaster);

                    if (input != null) {
                        if ("date".equals(teqipPackageQuestionMaster.getType())) {
                            try {
                                itemDetail[index][supindex] = dateformat.format(new Date(Long.valueOf(input.getUserResponse())));
                            } catch (Exception e) {
                                itemDetail[index][supindex] = input.getUserResponse();
                            }
                        } else if ("float".equals(teqipPackageQuestionMaster.getType())) {
                            try {
                                itemDetail[index][supindex] = AbstractWordMapper.getString(Double.parseDouble(input.getUserResponse()));
                            } catch (Exception e) {
                                itemDetail[index][supindex] = input.getUserResponse();
                            }
                        } else {
                            itemDetail[index][supindex] = input.getUserResponse();
                        }
                    }

                    supindex++;
                }

                index++;
            }

            table.put("$$INSTRUCTION_TO_CONSULTANT_QCBS_TABLE$$", itemDetail);

        }
    }

    void setTableMapInstructionToConsultants(Map<String, String[][]> table, TeqipPackage packageDetail) {

        List<TeqipPackageQuestionMaster> teqipPackageQuestionMasters = packageQuestionRepository
                .findByTeqipPackageAndQuestionCategory(packageDetail, "instructionToConsultants");

        List<TeqipPmssSupplierMaster> nonResponsiveSupplier = getNonResponsiveSupplier(packageDetail);

        if (teqipPackageQuestionMasters != null && nonResponsiveSupplier != null) {

            String[][] itemDetail = new String[teqipPackageQuestionMasters.size() + 1][nonResponsiveSupplier.size() + 1];

            itemDetail[0][0] = "Questions";

            int index = 1;
            for (TeqipPmssSupplierMaster teqipPackageSupplierDetail : nonResponsiveSupplier) {

                itemDetail[0][index] = teqipPackageSupplierDetail.getSupplierName();
                index++;
            }

            index = 1;
            for (TeqipPackageQuestionMaster teqipPackageQuestionMaster : teqipPackageQuestionMasters) {

                itemDetail[index][0] = teqipPackageQuestionMaster.getQuestionDesc();

                int supindex = 1;
                for (TeqipPmssSupplierMaster teqipPmssSupplierMaster : nonResponsiveSupplier) {

                    TeqipPackageQuotationsSupplierInput input = supplierInputRepository.findFirstByTeqipPackageAndTeqipPackageQuestionMasterAndTeqipPmssSupplierMaster(
                            packageDetail, teqipPackageQuestionMaster, teqipPmssSupplierMaster);

                    if (input != null) {
                        if ("date".equals(teqipPackageQuestionMaster.getType())) {
                            try {
                                itemDetail[index][supindex] = dateformat.format(new Date(Long.valueOf(input.getUserResponse())));
                            } catch (Exception e) {
                                itemDetail[index][supindex] = input.getUserResponse();
                            }
                        } else if ("float".equals(teqipPackageQuestionMaster.getType())) {
                            try {
                                itemDetail[index][supindex] = AbstractWordMapper.getString(Double.parseDouble(input.getUserResponse()));
                            } catch (Exception e) {
                                itemDetail[index][supindex] = input.getUserResponse();
                            }
                        } else {
                            itemDetail[index][supindex] = input.getUserResponse();
                        }
                    }

                    supindex++;
                }

                index++;
            }

            table.put("$$INSTRUCTION_TO_CONSULTANT_TABLE$$", itemDetail);

        }
    }

    void setTableMapGRNItem(Map<String, String[][]> table, Map<String, String> words,
            TeqipPackageGRNDetail gRNDetail,
            TeqipPackage teqipPackage,
            TeqipPmssSupplierMaster teqipPmssSupplierMaster) {

        List<TeqipPackagePurchaseOrderDetail> details = purchaseOrderDetailRepository.findByTeqipPackage(teqipPackage);

        List<TeqipPackageGRNItemDetail> teqipPackageGRNItemDetails = gRNDetail.getTeqipPackageGRNItemDetails();
        if (teqipPackageGRNItemDetails != null) {

            String[][] itemDetail = new String[teqipPackageGRNItemDetails.size() + 1][5];
            itemDetail[0] = new String[]{"DATE OF SUPPLY", "DESCRIPTION", "QUANTITY", "UNIT PRICE(In INR)", "VALUE(In INR)"};

            int index = 1;
            Double totalGRN = 0.0;
            for (TeqipPackageGRNItemDetail detail : teqipPackageGRNItemDetails) {

                TeqipItemMaster teqipItemMaster = detail.getTeqipItemMaster();

                List<TeqipPackagePurchaseOrderDetail> filter = details.stream().filter((obj) -> {
                    return obj.getTeqipItemMaster() != null && teqipItemMaster != null
                            && Objects.equals(obj.getTeqipItemMaster().getItemId(), teqipItemMaster.getItemId());
                }).collect(Collectors.toList());

                Double unitPrice = filter != null && !filter.isEmpty() ? filter.get(0).getBasicCost() : null;
                Double receivedQty = detail.getReceivedQty();
                Double totalPrice = unitPrice != null && receivedQty != null ? unitPrice * receivedQty : null;

                totalGRN += totalPrice == null ? 0.0f : totalPrice;
                itemDetail[index] = new String[]{
                    gRNDetail.getSupplyDate() != null ? dateformat.format(gRNDetail.getSupplyDate()) : "",
                    detail.getTeqipItemMaster() != null ? detail.getTeqipItemMaster().getItemName() : "",
                    AbstractWordMapper.getString(receivedQty),
                    AbstractWordMapper.getString(unitPrice),
                    AbstractWordMapper.getString(totalPrice)
                };

                index++;
            }

            table.put("$$GRN_ITEM_DETAIL$$", itemDetail);
            words.put("$$TOTAL_GRN$$", AbstractWordMapper.getString(totalGRN));
            words.put("$$TOTAL_GRN_WORDS$$", AbstractLetter.convertToIndianCurrency(AbstractWordMapper.getString(totalGRN)));

        }
    }

    public void setTableMapPayment(Map<String, String[][]> table, List<TeqipPackagePaymentDetail> teqipPackagePaymentDetails) {

        if (teqipPackagePaymentDetails != null && !teqipPackagePaymentDetails.isEmpty()) {
            String[][] itemDetail = new String[teqipPackagePaymentDetails.size()][2];

            String[] value = new String[teqipPackagePaymentDetails.size()];
            int index = 0;
            for (TeqipPackagePaymentDetail packagePaymentDetail : teqipPackagePaymentDetails) {

                itemDetail[index] = new String[]{
                    packagePaymentDetail.getPaymentDescription() + ":",
                    AbstractWordMapper.getString(packagePaymentDetail.getMilestonePercentage()) + "% of total cost"
                };

                index++;
            }

            table.put("$$PAYMENT_DETAILS_TABLE$$", itemDetail);

        }

    }

    void setTableMapGrnItemDetails(Map<String, String[][]> table, List<TeqipPackageGRNItemDetail> gRNItemDetails) {
        if (gRNItemDetails != null && !gRNItemDetails.isEmpty()) {

            String[][] itemDetail = new String[gRNItemDetails.size() + 1][6];
            itemDetail[0] = new String[]{"Item Code", "Item Name ", "Received Qty  ", "Stock Register Name ", "Page No. ", "Serial No. "};

            int index = 1;
            for (TeqipPackageGRNItemDetail teqipItemGoodsDetail : gRNItemDetails) {

                itemDetail[index] = new String[]{
                    teqipItemGoodsDetail.getItemCode(),
                    teqipItemGoodsDetail.getTeqipItemMaster().getItemName(),
                    getString(teqipItemGoodsDetail.getReceivedQty()),
                    teqipItemGoodsDetail.getStockRegistorName(),
                    teqipItemGoodsDetail.getPageNumber(),
                    teqipItemGoodsDetail.getSerialNumber()
                };

                index++;
            }

            table.put("$$ASSERT_REGISTER_DETAILS$$", itemDetail);

        }
    }

}
