/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.constants.MethodConstants;
import java.io.IOException;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class GenerateLetterFactory {

    @Autowired
    InvitationLetterGoods invitationGoodsLetter;

    @Autowired
    InvitationLetterWorks invitationWorksLetter;

    @Autowired
    InvitationLetterQCBS invitationLetterQCBS;

    @Autowired
    PurchaseOrderLetter purchaseOrderLetter;

    @Autowired
    WorkOrderLetter workOrderLetter;

    @Autowired
    QuotationOpeningLetter openingLetter;

    @Autowired
    QuotationEvaluationLetter evaluationLetter;

    @Autowired
    LetterOfAcceptance letterOfAcceptance;

    @Autowired
    AssetLetter assetLetter;

    @Autowired
    GRNLetter gRNLetter;

    @Autowired
    BidEvaluationLetter bidEvaluationLetter;

    @Autowired
    SBDGenerateLetter sBDGenerateLetter;
    @Autowired
    PackageLetter packageLetter;

    public void generate(Integer packageId, String type, Map<String, String[]> param) throws IOException, InvalidFormatException {

        switch (type) {
            case MethodConstants.DOC_CAT_INVITATION_GOODS:
                invitationGoodsLetter.generate(packageId, type);
                break;
            case MethodConstants.DOC_CAT_INVITATION_GOODS_DC:
            invitationGoodsLetter.generate(packageId, type);
            break;
            case MethodConstants.DOC_CAT_INVITATION_WORKS:
                invitationWorksLetter.generate(packageId, type);
                break;
            case MethodConstants.DOC_CAT_INVITATION_QCBS:
            case MethodConstants.DOC_CAT_INVITATION_FINANCIAL_QCBS:
                invitationLetterQCBS.generate(packageId, type);
                break;
            case MethodConstants.DOC_CAT_PURCHASE_ORDER_NCB:
            case MethodConstants.DOC_CAT_PURCHASE_ORDER_DC:
            case MethodConstants.DOC_CAT_PURCHASE_ORDER:
                purchaseOrderLetter.generate(packageId, type);
                break;
            case MethodConstants.DOC_CAT_WORK_ORDER_NCB:

            case MethodConstants.DOC_CAT_WORK_ORDER:
                workOrderLetter.generate(packageId, type);
                break;
            case MethodConstants.DOC_CAT_NCB_BID_OPENING:
            case MethodConstants.DOC_CAT_QUOTATION_OPENING:
            case MethodConstants.DOC_CAT_QCBS_EOI_OPENING:
                openingLetter.generate(packageId, type);
                break;
            case MethodConstants.DOC_CAT_QUOTATION_EVALUATION:
            case MethodConstants.DOC_CAT_NCB_BID_EVALUATION:
            case MethodConstants.DOC_CAT_QCBS_FINANCIAL_OPENING:
                evaluationLetter.generate(packageId, type);
                break;
            case MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE:
            case MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE_NCB:
            case MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE_DC:
                letterOfAcceptance.generate(packageId, type);
                break;
            case MethodConstants.DOC_CAT_GRN_LETTER_NCB:
            case MethodConstants.DOC_CAT_GRN_LETTER:
                gRNLetter.generate(packageId, type, param);
                break;
            case MethodConstants.DOC_CAT_ASSET_LETTER_NCB:
            case MethodConstants.DOC_CAT_ASSET_LETTER:
                assetLetter.generate(packageId, type, param);
                break;
            case MethodConstants.DOC_CAT_NCB_BID_AWARD:
                bidEvaluationLetter.generate(packageId, type, param);
                break;
            case MethodConstants.DOC_CAT_NCB_SBD:
            case MethodConstants.DOC_CAT_NCB_SBD_WORK:

                sBDGenerateLetter.generate(packageId, type, param);
                break;
            default:
                packageLetter.generate(packageId, type);
        }
    }

}
