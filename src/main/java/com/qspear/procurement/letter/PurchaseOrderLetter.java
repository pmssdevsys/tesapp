/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackagePoContractGenereration;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrder;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jaspreet
 */
@Service
@Transactional
public class PurchaseOrderLetter extends AbstractLetter {

    public void generate(Integer packageId, String type) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);
        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }

        TeqipPmssSupplierMaster teqipPmssSupplierMaster = null;

        if (MethodConstants.DOC_CAT_PURCHASE_ORDER_NCB.equals(type)) {
            TeqipPackagePoContractGenereration findByTeqipPackage = poContractGenererationRepository.findByTeqipPackage(teqipPackage);
            if (findByTeqipPackage == null) {
                throw new RuntimeException("PO Contract generation is missing");
            }
            
            teqipPmssSupplierMaster = findByTeqipPackage.getTeqipPmssSupplierMaster();

        } else {
            TeqipPackagePurchaseOrder teqipPackagePurchaseOrder = purchaseOrderRepository.findByTeqipPackage(teqipPackage);
            if (teqipPackagePurchaseOrder == null) {
                throw new RuntimeException("PurchaseOrder not Defined");
            }
            
            teqipPmssSupplierMaster = teqipPackagePurchaseOrder.getTeqipPmssSupplierMaster();

        }
        if (teqipPmssSupplierMaster == null) {
            throw new RuntimeException("Supplier not Defined");

        }
        this.deletePreviousDoc(teqipPackage, type);

        Map<String, String> words = this.getWordMap(teqipPackage);
        this.setWordMap(words, teqipPmssSupplierMaster);

        Map<String, String[][]> table = new HashMap<>();
        this.setTableMapItemGoodsDetails(table, teqipPackage.getTeqipItemGoodsDetails());

        Map<String, String[]> listMap = this.getListMap(teqipPackage);

        this.createDoc(teqipPackage, teqipPmssSupplierMaster, type, type, words, table, listMap);

    }

}
