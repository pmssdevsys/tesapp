/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAward;
import com.qspear.procurement.persistence.methods.TeqipPackagePoContractGenereration;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageAwardRepository;
import com.qspear.procurement.repositories.method.WorkOrderRepository;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class WorkOrderLetter extends AbstractLetter {

    @Autowired
    TeqipPackageRepository teqipPackageRepository;
    @Autowired
    WorkOrderRepository workOrderRepository;

    @Autowired
    PackageAwardRepository packageAwardRepository;

    public void generate(Integer packageId, String type) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        TeqipPmssSupplierMaster teqipPmssSupplierMaster = null;

        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }

        if (MethodConstants.DOC_CAT_WORK_ORDER_NCB.equals(type)) {
            TeqipPackagePoContractGenereration findByTeqipPackage = poContractGenererationRepository.findByTeqipPackage(teqipPackage);
            if (findByTeqipPackage == null) {
                throw new RuntimeException("PO Contract generation is missing");
            }

            teqipPmssSupplierMaster = findByTeqipPackage.getTeqipPmssSupplierMaster();

        } else {

            TeqipPackageAward teqipPackageAward = packageAwardRepository.findByTeqipPackage(teqipPackage);
            if (teqipPackageAward == null) {
                List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();
                if (teqipPackageSupplierDetails != null && !teqipPackageSupplierDetails.isEmpty()) {
                    teqipPmssSupplierMaster = teqipPackageSupplierDetails.get(0).getTeqipPmssSupplierMaster();
                }
            } else {

                teqipPmssSupplierMaster = teqipPackageAward.getTeqipPmssSupplierMaster();
            }

        }

        if (teqipPmssSupplierMaster == null) {
            throw new RuntimeException("Supplier not Defined");

        }
        this.deletePreviousDoc(teqipPackage, type);

        Map<String, String> words = this.getWordMap(teqipPackage);
        this.setWordMap(words, teqipPmssSupplierMaster);

        Map<String, String[][]> table = new HashMap<>();
        this.setTableMapItemWorksDetails(table, teqipPackage.getTeqipItemWorkDetails());
        this.setTableMapPayment(table, teqipPackage.getTeqipPackagePaymentDetails());
        this.createDoc(teqipPackage, teqipPmssSupplierMaster, type, type, words, table, null);

    }

}
