/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAward;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractGeneration;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class PackageLetter extends AbstractLetter {

    public void generate(Integer packageId, String type) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }
        TeqipPmssSupplierMaster supplier = null;

        if (type.contains("QCBS")) {
            TeqipPackageQCBSContractGeneration entityGeneration = qcbsContractGenerationRepository.findByTeqipPackage(teqipPackage);
            if (entityGeneration != null) {
                supplier = entityGeneration.getTeqipPmssSupplierMaster();
            }

        }
        if (type.contains("NCB")) {
            TeqipPackageAward teqipPackageAward = packageAwardRepository.findByTeqipPackage(teqipPackage);

            if (teqipPackageAward != null) {
                supplier = teqipPackageAward.getTeqipPmssSupplierMaster();
            }
        }

        this.deletePreviousDoc(teqipPackage, type);

        Map<String, String> words = this.getWordMap(teqipPackage);
        this.setWordMap(words, supplier);
        
        Map<String, String[][]> table = new HashMap<>();
        this.setTableMapSupplier(table, teqipPackage.getTeqipPackageSupplierDetails());
        this.setTableMapItemGoodsDetails(table, teqipPackage.getTeqipItemGoodsDetails());
        this.setTableMapItemWorksDetails(table, teqipPackage.getTeqipItemWorkDetails());
        this.setTableMapInstructionToConsultants(table, teqipPackage);
        this.setTableMapInstructionToConsultantsQCBS(table, teqipPackage);

        this.setTableMapInsPurchaseCommittee(table,  teqipPackage.getTeqipInstitution() != null
                ? teqipPackage.getTeqipInstitution().getTeqipPmssInstitutionpurchasecommittes() : null); 
        this.setTableMapEOIOpenning(table, teqipPackage);
        this.setTableMapTechEvaluation(table, teqipPackage);
        
        this.createDoc(teqipPackage, null, type, type, words, table, null);

    }

}
