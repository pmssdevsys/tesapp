/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageBidAdvertisement;
import com.qspear.procurement.persistence.methods.TeqipPackageBidDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageBidINSTAndSCC;
import com.qspear.procurement.persistence.methods.TeqipPackageExtension;
import com.qspear.procurement.persistence.methods.TeqipPackageInvitationLetter;
import com.qspear.procurement.persistence.methods.TeqipPackageIssueRFP;
import com.qspear.procurement.persistence.methods.TeqipPackagePoContractGenereration;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrder;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrderDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractGeneration;
import com.qspear.procurement.persistence.methods.TeqipPackageQuestionMaster;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageTermReference;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrder;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.ExtensionRepository;
import com.qspear.procurement.repositories.method.InviationLetterRepository;
import com.qspear.procurement.repositories.method.IssueRFPRepository;
import com.qspear.procurement.repositories.method.LetterOfAcceptanceRepository;
import com.qspear.procurement.repositories.method.PackageAwardRepository;
import com.qspear.procurement.repositories.method.PackageBidAdvertisementRepository;
import com.qspear.procurement.repositories.method.PackageBidDetailRepository;
import com.qspear.procurement.repositories.method.PackageBidINSTAndSCCRepository;
import com.qspear.procurement.repositories.method.PackageDocumentRepository;
import com.qspear.procurement.repositories.method.PackagePurchaseOrderDetailRepository;
import com.qspear.procurement.repositories.method.PackagePurchaseOrderRepository;
import com.qspear.procurement.repositories.method.PackageQuestionRepository;
import com.qspear.procurement.repositories.method.PackageQuotationDetailRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsSupplierInputRepository;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import com.qspear.procurement.repositories.method.PoContractGenererationRepository;
import com.qspear.procurement.repositories.method.QCBSContractDetailRepository;
import com.qspear.procurement.repositories.method.QCBSContractGenerationRepository;
import com.qspear.procurement.repositories.method.RFPDetailRepository;
import com.qspear.procurement.repositories.method.RFPDocumentRepository;
import com.qspear.procurement.repositories.method.TermReferenceRepository;
import com.qspear.procurement.repositories.method.WorkOrderRepository;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public abstract class AbstractWordMapper {

    SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat datetimeformat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");

    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    @Autowired
    PackageDocumentRepository documentRepository;

    @Autowired
    PackageQuestionRepository packageQuestionRepository;

    @Autowired
    InviationLetterRepository inviationLetterRepository;

    @Autowired
    PackagePurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    WorkOrderRepository workOrderRepository;

    @Autowired
    PackageQuotationsSupplierInputRepository supplierInputRepository;

    @Autowired
    PackagePurchaseOrderDetailRepository purchaseOrderDetailRepository;

    @Autowired
    TeqipPackageRepository teqipPackageRepository;

    @Autowired
    PackageAwardRepository packageAwardRepository;

    @Autowired
    LetterOfAcceptanceRepository letterOfAcceptanceRepository;

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    @Autowired
    PoContractGenererationRepository poContractGenererationRepository;
    @Autowired
    PackageBidINSTAndSCCRepository packageBidINSTAndSCCRepository;
    @Autowired
    PackageBidDetailRepository packageBidDetailRepository;

    @Autowired
    TermReferenceRepository termReferenceRepository;

    @Autowired
    RFPDocumentRepository rFPDocumentRepository;

    @Autowired
    RFPDetailRepository rFPDetailRepository;

    @Autowired
    IssueRFPRepository issueRFPRepository;

    @Autowired
    QCBSContractGenerationRepository qcbsContractGenerationRepository;
    @Autowired
    QCBSContractDetailRepository qcbsContractDetailRepository;
    @Autowired
    PackageBidAdvertisementRepository packageBidAdvertisementRepository;

    @Autowired
    PackageQuotationDetailRepository packageQuotationDetailRepository;

    @Autowired
    ExtensionRepository extensionRepository;


    Map<String, String> getWordMap(TeqipPackage teqipPackage) {

        dateformat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // Or whatever IST is supposed to be
        timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata")); // Or whatever IST is supposed to be

        Map<String, String> words = new HashMap<>();

        if (teqipPackage != null) {
            Calendar cal = Calendar.getInstance();

            words.put("$$PACKAGE_NAME$$", teqipPackage.getPackageName());
            words.put("$$PACKAGE_DESC$$", teqipPackage.getJustification());

            words.put("$$PACKAGE_ESTCOST$$", AbstractWordMapper.getString(teqipPackage.getEstimatedCost()));
            words.put("$$PACKAGE_CODE$$", teqipPackage.getPackageCode());
            words.put("$$CURRENT_DATE$$", dateformat.format(cal.getTime()));
            words.put("$$PACKAGE_METHOD$$", (teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode()));
            words.put("$$PACKAGE_METHOD_NCB$$", (teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode() + " " + teqipPackage.getTeqipCategorymaster().getCategoryName()));
            cal.add(Calendar.DAY_OF_MONTH, 15);
            words.put("$$CURRENT_DATE_PLUS_15$$", dateformat.format(cal.getTime()));
            words.put("$$PACKAGE_INITIATION_DATE$$", teqipPackage.getPackageInitiationDate() != null
                    ? dateformat.format(teqipPackage.getPackageInitiationDate()) : null);

        }
        TeqipInstitution teqipInstitution = teqipPackage.getTeqipInstitution();
        if (teqipInstitution != null) {

            words.put("$$INST_NAME$$", teqipInstitution.getInstitutionName());
            words.put("$$INST_STATE$$", teqipInstitution.getState());

            words.put("$$INST_ADDR$$", teqipInstitution.getInstitutionName() + "," + teqipInstitution.getAddress() + "," + teqipInstitution.getState());
            words.put("$$INST_DETAIL$$", teqipInstitution.getAddress() + "," + teqipInstitution.getState());
            words.put("$$INST_TELNO$$", teqipInstitution.getTelephonenumber());
            words.put("$$INST_FAXNO$$", teqipInstitution.getFaxnumber());
            words.put("$$INST_EMAIL$$", teqipInstitution.getEmailid());
            words.put("$$INST_WEBSITE$$", teqipInstitution.getWebsiteurl());
        }

        TeqipPackageInvitationLetter invitationLetter = inviationLetterRepository.findByTeqipPackage(teqipPackage);
        if (invitationLetter != null) {

            words.put("$$INV_LASTSUB_TIME$$", invitationLetter.getLastSubmissionTime() != null
                    ? getStringTimeformat(timeFormat.format(invitationLetter.getLastSubmissionTime())) : null);

            words.put("$$INV_LASTSUB_DATE$$", invitationLetter.getLastSubmissionDate() != null
                    ? dateformat.format(invitationLetter.getLastSubmissionDate()) : null);

            words.put("$$INV_BID_OPENING_DATE$$", invitationLetter.getPlannedBidOpeningDate() != null
                    ? dateformat.format(invitationLetter.getPlannedBidOpeningDate()) : null);
            words.put("$$INV_BID_OPENING_TIME$$", invitationLetter.getLastSubmissionTime() != null
                    ? getStringTimeformat(timeFormat.format(invitationLetter.getLastSubmissionTime())) : null);

            words.put("$$INV_COMP_PERIOD$$", AbstractWordMapper.getString(invitationLetter.getWorkCompletionPeriod()));
            words.put("$$INV_LIQUID_DAMAGE$$", AbstractWordMapper.getString(invitationLetter.getMinliquidatedDamages()));

            words.put("$$INV_LIG_DAMAGE_MIN$$", getString(invitationLetter.getMinliquidatedDamages()));
            words.put("$$INV_LIG_DAMAGE_MAX$$", getString(invitationLetter.getMaxliquidatedDamages()));

            words.put("$$INV_WARRANTY$$","0".equals(invitationLetter.getWarranty()) ?"N/A": invitationLetter.getWarranty());
            words.put("$$INV_TRAINING_CLAUSE$$", invitationLetter.getTrainingClause());
            words.put("$$INV_QUOT_VAL_REQ$$", AbstractWordMapper.getString(invitationLetter.getQuotationValidityRequired()));
            words.put("$$INV_INSTALLATION_CLAUSE$$", invitationLetter.getInstallationClause());

            //Fixed Performance Security percentage to 3 for DC method
            String strPFSP = "3";
            if (invitationLetter.getPerformanceSecurityInPercentage() != null) {
                strPFSP = AbstractWordMapper.getString(invitationLetter.getPerformanceSecurityInPercentage());
            }
            words.put("$$PERF_SECURITY_PER$$", strPFSP);

            words.put("$$ITEM_ISINSTALLATION$$", invitationLetter.getInstallationClause() == null ? "" : invitationLetter.getInstallationClause());
            words.put("$$ITEM_ISTRAINING$$", invitationLetter.getTrainingClause() == null ? "" : invitationLetter.getTrainingClause());

        }

        TeqipPackagePurchaseOrder teqipPackagePurchaseOrder = purchaseOrderRepository.findByTeqipPackage(teqipPackage);
        if (teqipPackagePurchaseOrder != null) {

            Double totalContractValue = teqipPackagePurchaseOrder.getTotalContractValue();
            totalContractValue = totalContractValue == null ? 0.0f : totalContractValue;

            words.put("$$PURC_TOTAL$$", AbstractWordMapper.getString(totalContractValue));
            words.put("$$TOTAL_PURC_WORDS$$", AbstractLetter.convertToIndianCurrency(AbstractWordMapper.getString(totalContractValue)));
            words.put("$$PURC_OCTROI$$", AbstractWordMapper.getString(teqipPackagePurchaseOrder.getOctroiOther()));
            words.put("$$PO_DATE$$", dateformat.format(teqipPackagePurchaseOrder.getCreatedOn()));
            words.put("$$PO_NUMBER$$", teqipPackagePurchaseOrder.getPurchaseOrderNo());
            words.put("$$PERF_SECURITY_AMT$$", AbstractWordMapper.getString(teqipPackagePurchaseOrder.getPerformanceSecurityAmount()));

        }

        TeqipPackageWorkOrder teqipPackageWorkOrder = workOrderRepository.findByTeqipPackage(teqipPackage);
        if (teqipPackageWorkOrder != null) {
            words.put("$$WO_BASIC_COST$$", AbstractWordMapper.getString(teqipPackageWorkOrder.getContractvalue()));
            words.put("$$WO_DATE$$", dateformat.format(teqipPackageWorkOrder.getCreatedOn()));
            words.put("$$PERF_SECURITY_AMT$$", AbstractWordMapper.getString(teqipPackageWorkOrder.getPerformanceSecurityAmount()));
        }

        TeqipPackageQuotationDetail packageQuotationDetail = packageQuotationDetailRepository.findByTeqipPackage(teqipPackage);
        if (packageQuotationDetail != null) {
            words.put("$$QUOTATION_BIDOPENING_DATE$$", packageQuotationDetail.getQuotationOpeningDate() != null
                    ? dateformat.format(packageQuotationDetail.getQuotationOpeningDate()) : null);
            words.put("$$QUOTATION_BIDOPENING_DATETIME$$", packageQuotationDetail.getQuotationOpeningTime() != null
                    ? getStringTimeformat(timeFormat.format(packageQuotationDetail.getQuotationOpeningTime())) : null);
        } else {
            words.put("$$QUOTATION_BIDOPENING_DATE$$", "");
        }

//        List<TeqipItemGoodsDetail> teqipItemGoodsDetails = teqipPackage.getTeqipItemGoodsDetails();
//        if (teqipItemGoodsDetails != null && !teqipItemGoodsDetails.isEmpty()) {
//            TeqipItemGoodsDetail get = teqipItemGoodsDetails.get(0);
//            if (get != null) {
//                words.put("$$ITEM_ISINSTALLATION$$", get.getInstallationRequired() != null && get.getInstallationRequired() == 1 ? "Yes" : "No");
//                words.put("$$ITEM_ISTRAINING$$", get.getTrainingRequired() != null && get.getTrainingRequired() == 1 ? "Yes" : "No");
//
//            }
//
//        }
        TeqipPackagePoContractGenereration poContractGenereration = poContractGenererationRepository.findByTeqipPackage(teqipPackage);

        if (poContractGenereration != null) {

            Double totalBaseCost = poContractGenereration.getTotalBaseCost();
            totalBaseCost = totalBaseCost == null ? 0.0f : totalBaseCost;
            Double evaluatedPrice = poContractGenereration.getEvaluatedPrice();
            evaluatedPrice = evaluatedPrice == null ? 0.0f : evaluatedPrice;
            Double totalContractValue = poContractGenereration.getTotalContractValue();
            totalContractValue = totalContractValue == null ? 0.0f : totalContractValue;

            words.put("$$PURC_TOTAL$$", AbstractWordMapper.getString(totalBaseCost));
            words.put("$$PURC_TOTAL_CONTRACT$$", AbstractWordMapper.getString(totalContractValue));
            words.put("$$PURC_TOTAL_CONTRACT_WORDS$$", AbstractLetter.convertToIndianCurrency(AbstractWordMapper.getString(totalContractValue)));
            words.put("$$PURC_DELIVERY_DATE$$", poContractGenereration.getExpectedDeliveryDate() != null
                    ? dateformat.format(poContractGenereration.getExpectedDeliveryDate()) : null);

            words.put("$$PURC_OCTROI$$", AbstractWordMapper.getString(poContractGenereration.getOtherOctroi()));
            words.put("$$PO_DATE$$", dateformat.format(poContractGenereration.getCreatedOn()));
            words.put("$$PO_NUMBER$$", poContractGenereration.getPoNumber());
            words.put("$$WO_DATE$$", dateformat.format(poContractGenereration.getCreatedOn()));
            words.put("$$INV_COMP_PERIOD$$", poContractGenereration.getWorkCompletionDate() != null
                    ? dateformat.format(poContractGenereration.getWorkCompletionDate()) : "");
            words.put("$$WO_BASIC_COST$$", AbstractWordMapper.getString(evaluatedPrice));

            words.put("$$PERF_SECURITY_AMT$$", AbstractWordMapper.getString(poContractGenereration.getPerfSecurityAmount()));

        }

        Double toShowTax = 0.0;
        Double totalAppTax = 0.0;

        List<TeqipPackagePurchaseOrderDetail> teqipPackagePurchaseOrderDetail = purchaseOrderDetailRepository.findByTeqipPackage(teqipPackage);
        if (teqipPackagePurchaseOrderDetail != null) {

            for (TeqipPackagePurchaseOrderDetail abcd : teqipPackagePurchaseOrderDetail) {
                Double itemQty = abcd.getItemQty();
                itemQty = itemQty == null ? 0 : itemQty;
                Double basisCost = abcd.getBasicCost();
                basisCost = basisCost == null ? 0 : basisCost;

                Double totalCostWithTax = abcd.getTotalPrice();
                totalCostWithTax = totalCostWithTax == null ? 0 : totalCostWithTax;

                totalAppTax += totalCostWithTax;
                toShowTax += (itemQty * basisCost);
            }

        }

        words.put("$$PURC_EVALUATEDCOST$$", AbstractWordMapper.getString(toShowTax));
        words.put("$$PURC_GSTPERCENTAGE$$", AbstractWordMapper.getString(totalAppTax - toShowTax));
        words.put("$$PURC_FINAL$$", AbstractWordMapper.getString(totalAppTax));

        TeqipPackageBidDetail bidDetail = packageBidDetailRepository.findByTeqipPackage(teqipPackage);
        if (bidDetail != null) {

            words.put("$$BID_DOC_TIME$$", bidDetail.getSalebidDocument() != null
                    ? getStringTimeformat(timeFormat.format(bidDetail.getSalebidDocument())) : null);
            words.put("$$BID_DOC_DATE$$", bidDetail.getSalebidDocument() != null
                    ? dateformat.format(bidDetail.getSalebidDocument()) : null);

            words.put("$$BID_LAST_TIME$$", bidDetail.getLastSbdDate() != null
                    ? getStringTimeformat(timeFormat.format(bidDetail.getLastSbdDate())) : null);
            words.put("$$BID_LAST_DATE$$", bidDetail.getLastSbdDate() != null
                    ? dateformat.format(bidDetail.getLastSbdDate()) : null);

            words.put("$$BID_LAST_RECIPT_TIME$$", bidDetail.getLastreceiptBidDate() != null
                    ? getStringTimeformat(timeFormat.format(bidDetail.getLastreceiptBidDate())) : null);
            words.put("$$BID_LAST_RECIPT_DATE$$", bidDetail.getLastreceiptBidDate() != null
                    ? dateformat.format(bidDetail.getLastreceiptBidDate()) : null);

            words.put("$$BID_OPENING_TIME$$", bidDetail.getOpeningDate() != null
                    ? getStringTimeformat(timeFormat.format(bidDetail.getOpeningDate())) : null);
            words.put("$$BID_OPENING_DATE$$", bidDetail.getOpeningDate() != null
                    ? dateformat.format(bidDetail.getOpeningDate()) : null);

            words.put("$$BID_COST$$", AbstractWordMapper.getString(bidDetail.getCostbidDocument()));

        }
        TeqipPackageBidINSTAndSCC bidINSTAndSCC = packageBidINSTAndSCCRepository.findByTeqipPackage(teqipPackage);

        if (bidINSTAndSCC != null) {

            words.put("$$BIDINST_ADDR_PURCHASER$$", bidINSTAndSCC.getAddressofNotice());
            words.put("$$BIDINST_PLACE_OPENING$$", bidINSTAndSCC.getPlaceOfOpeningOfBids());

            words.put("$$BIDINST_PAYABLEAT$$", bidINSTAndSCC.getPayableAt());
            words.put("$$BIDINST_POSTAL$$", AbstractWordMapper.getString(bidINSTAndSCC.getPostalCharge()));
            words.put("$$OFFICER_BID$$", bidINSTAndSCC.getBidOfficer());

            words.put("$$BIDINST_CHEQUE_FAVOUR$$", bidINSTAndSCC.getChequeFavour());
            words.put("$$BIDINST_PRE_MEETING_DATE$$", bidINSTAndSCC.getPrebidMeetingDate() != null
                    ? dateformat.format(bidINSTAndSCC.getPrebidMeetingDate()) : null);
            words.put("$$BIDINST_PRE_MEETING_TIME$$", bidINSTAndSCC.getPrebidmeetingDateTime() != null
                    ? getStringTimeformat(timeFormat.format(bidINSTAndSCC.getPrebidmeetingDateTime())) : null);
            words.put("$$BIDINST_SELL_END_DATE$$", bidINSTAndSCC.getBidsellEndTime() != null
                    ? dateformat.format(bidINSTAndSCC.getBidsellEndTime()) : null);
            words.put("$$BIDINST_SELL_END_TIME$$", bidINSTAndSCC.getBidsellEndTime() != null
                    ? timeFormat.format(bidINSTAndSCC.getBidsellEndTime()) : null);
            words.put("$$INV_WARRANTY$$", AbstractWordMapper.getString(bidINSTAndSCC.getWarranty()));
            words.put("$$INV_LIQUID_DAMAGE$$", AbstractWordMapper.getString(bidINSTAndSCC.getMinLiquidatedDamage()));

            words.put("$$INV_LIG_DAMAGE_MIN$$", getString(bidINSTAndSCC.getMinLiquidatedDamage()));
            words.put("$$INV_LIG_DAMAGE_MAX$$", getString(bidINSTAndSCC.getMaxLiquidatedDamage()));
            words.put("$$PERF_SECURITY_PER$$", bidINSTAndSCC.getPerformanceSecurity());
            words.put("$$BID_SECURITY_INR$$", getString(bidINSTAndSCC.getBidSecurity()));
            words.put("$$BIDINST_SELL_START_DATE$$", bidINSTAndSCC.getBidsellStartTime() != null
                    ? timeFormat.format(bidINSTAndSCC.getBidsellStartTime()) : null);
            words.put("$$BIDINST_SELL_START_TIME$$", bidINSTAndSCC.getBidsellStartTime() != null
                    ? timeFormat.format(bidINSTAndSCC.getBidsellStartTime()) : null);
            
        }

        TeqipPackageTermReference termReference = termReferenceRepository.findByTeqipPackage(teqipPackage);
        if (termReference != null) {
            words.put("$$TERM_EOI_TIME$$", termReference.getEoiOpeningDate() != null
                    ? getStringTimeformat(timeFormat.format(termReference.getEoiOpeningDate())) : null);
            words.put("$$TERM_EOI_DATE$$", termReference.getEoiOpeningDate() != null
                    ? dateformat.format(termReference.getEoiOpeningDate()) : null);
            words.put("$$TERM_WORLD_NOC_DATE$$", termReference.getWorldBankNocDate() != null
                    ? dateformat.format(termReference.getWorldBankNocDate()) : null);

        }
        
    
        List<TeqipPackageExtension> extension = extensionRepository.findByTeqipPackage(teqipPackage);
        if (extension != null && extension.size() > 0) {
            TeqipPackageExtension packageExtension = extension.get(extension.size() - 1);
            if (packageExtension != null) {
                words.put("$$CLOSING_EXTENSION_DATE$$", packageExtension.getExtendedDate() != null
                        ? dateformat.format(packageExtension.getExtendedDate()) : null);
            }
        }

        TeqipPackageRFPDocument entityDocument = rFPDocumentRepository.findByTeqipPackage(teqipPackage);

        if (entityDocument != null) {
            words.put("$$REF_PARAGRAPH_7_1$$", entityDocument.getParagraph7_1() != null
                    ? datetimeformat.format(entityDocument.getParagraph7_1()) : null);
            words.put("$$REF_PARAGRAPH_7_1_ADDR$$", entityDocument.getParagraph7_1Address());

            words.put("$$REF_PARAGRAPH_6_1$$", entityDocument.getParagraph6_1Date() != null
                    ? datetimeformat.format(entityDocument.getParagraph6_1Date()) : null);
            words.put("$$REF_PARAGRAPH_6_1_ADDR$$", entityDocument.getParagraph6_1Address());

            words.put("$$REF_PARAGRAPH_4_5$$", entityDocument.getParagraph4_5Date() != null
                    ? datetimeformat.format(entityDocument.getParagraph4_5Date()) : null);
            words.put("$$REF_PARAGRAPH_4_5_ADDR$$", entityDocument.getParagraph4_5());
            words.put("$$REF_PARAGRAPH_4_3$$", AbstractWordMapper.getString(entityDocument.getParagraph4_3()));

            words.put("$$REF_PARAGRAPH_3_4_TRAINING$$", entityDocument.getParagraph3_4IsTraining() == null
                    || entityDocument.getParagraph3_4IsTraining() == 0 ? "No" : "Yes");

            words.put("$$REF_PARAGRAPH_3_4_Info$$", entityDocument.getParagraph3_4Information());
            words.put("$$REF_PARAGRAPH_3_4$$", entityDocument.getParagraph3_4());
            words.put("$$REF_PARAGRAPH_3_3$$", AbstractWordMapper.getString(entityDocument.getParagraph3_3()));
            words.put("$$REF_PARAGRAPH_3_3$$", AbstractWordMapper.getString(entityDocument.getParagraph3_3()));

            words.put("$$REF_PARAGRAPH_2_1$$", entityDocument.getParagraph2_1() != null
                    ? dateformat.format(entityDocument.getParagraph2_1()) : null);
            words.put("$$REF_PARAGRAPH_2_1_ADDR$$", entityDocument.getParagraph2_1Address());

            words.put("$$REF_PARAGRAPH_1_14$$", entityDocument.getParagraph1_14Date() != null
                    ? dateformat.format(entityDocument.getParagraph1_14Date()) : null);
            words.put("$$REF_PARAGRAPH_1_14_Days$$", AbstractWordMapper.getString(entityDocument.getParagraph1_14()));

            words.put("$$REF_PARAGRAPH_1_4$$", entityDocument.getParagraph1_4());

            words.put("$$REF_PARAGRAPH_1_3$$", entityDocument.getParagraph1_3());
            words.put("$$REF_PARAGRAPH_1_2$$", entityDocument.getParagraph1_2() == null || entityDocument.getParagraph1_2() == 0 ? "No" : "Yes");

        }
        TeqipPackageRFPDetail rFPDetail = rFPDetailRepository.findByTeqipPackage(teqipPackage);
        if (rFPDetail != null) {
            words.put("$$REF_TYPE_CONTRACT$$", rFPDetail.getTypeOfContract());
            words.put("$$REF_PROPOSAL_DATE$$", rFPDetail.getProposalSubmissionDate()!= null
                    ? dateformat.format(rFPDetail.getProposalSubmissionDate()) : null);
            words.put("$$REF_PROPOSAL_TIME$$", rFPDetail.getProposalSubmissionDate() != null
                    ? timeFormat.format(rFPDetail.getProposalSubmissionDate()) : null);
           

        }
        TeqipPackageIssueRFP issueRFP = issueRFPRepository.findByTeqipPackage(teqipPackage);

        if (issueRFP != null) {
            words.put("$$ISSUE_REF_OPENING_DATE$$", issueRFP.getFinancialOpeningDate() != null
                    ? dateformat.format(issueRFP.getFinancialOpeningDate()) : null);
            words.put("$$CONTRACT_SIGN_DATE$$", issueRFP.getContractSignDate() != null
                    ? dateformat.format(issueRFP.getContractSignDate()) : null);
            words.put("$$REF_PLANNED_PROPOSAL_DATE$$", issueRFP.getPlannedFinancialOpeningDate()!= null
                    ? dateformat.format(issueRFP.getPlannedFinancialOpeningDate()) : null);
            words.put("$$REF_PLANNED_PROPOSAL_TIME$$", issueRFP.getPlannedFinancialOpeningDateTime()!= null
                    ? timeFormat.format(issueRFP.getPlannedFinancialOpeningDateTime()) : null);
           

        }
        TeqipPackageQCBSContractGeneration entityGeneration = qcbsContractGenerationRepository.findByTeqipPackage(teqipPackage);

        if (entityGeneration != null) {
            words.put("$$CONTRACT_NEGOTIATION$$", entityGeneration.getIsNegotiationSuccessful() == null || entityGeneration.getIsNegotiationSuccessful() == 0 ? "No" : "Yes");
            words.put("$$CONTRACT_NEGOTIATION_DATE$$", entityGeneration.getDateOfNegotiation() != null
                    ? dateformat.format(entityGeneration.getDateOfNegotiation()) : null);

        }
        TeqipPackageQCBSContractDetail contractDetailEntity = qcbsContractDetailRepository.findByTeqipPackage(teqipPackage);
        if (contractDetailEntity != null) {
            words.put("$$CONTRACT_START_DATE$$", contractDetailEntity.getContractStartDate() != null
                    ? dateformat.format(contractDetailEntity.getContractStartDate()) : null);
            words.put("$$CONTRACT_VALUE$$", contractDetailEntity.getContractValue() != null
                    ? AbstractWordMapper.getString(contractDetailEntity.getContractValue()) : null);
            words.put("$$CONTRACT_DELIVERY$$", AbstractWordMapper.getString(contractDetailEntity.getDeliveryPeriodInMonths()));

        }
        TeqipPackageBidAdvertisement bidAdvertisement = packageBidAdvertisementRepository.findByTeqipPackage(teqipPackage);
        if(bidAdvertisement!=null){
               words.put("$$LAST_EOI_SUBMISSION_DATE$$", bidAdvertisement.getEoiSubmissionDate()!= null
                    ? dateformat.format(bidAdvertisement.getEoiSubmissionDate()) : null);
        
        	words.put("$$EOI_SubmissionDate$$",bidAdvertisement.getEoiSubmissionDate()!=null
        			? dateformat.format(bidAdvertisement.getEoiSubmissionDate()):null);
        	words.put("$$actual_publicationNationalDate$$", bidAdvertisement.getActualpublicationNationalDate()!=null
        			?dateformat.format(bidAdvertisement.getActualpublicationNationalDate()):null);
        	words.put("$$actual_publicationLocaleDate$$", bidAdvertisement.getActualpublicationLocaleDate()!=null
        			? dateformat.format(bidAdvertisement.getActualpublicationLocaleDate()): null);
        	words.put("$$advertisementDate$$",bidAdvertisement.getAdvertisementDate()!=null
        			? dateformat.format(bidAdvertisement.getAdvertisementDate()): null);
        }else{
                words.put("$$LAST_EOI_SUBMISSION_DATE$$",  "____________");
        
        	words.put("$$EOI_SubmissionDate$$",null);
        	
        }

        return words;
    }

    Double setReadOutPrice(TeqipPackage teqipPackage, TeqipPmssSupplierMaster supplier) {
        Double readOut = null;
        TeqipPackageQuestionMaster question = packageQuestionRepository.findFirstByTeqipPackageAndIsevalatedPrice(teqipPackage, 1);

        if (question != null) {
            TeqipPackageQuotationsSupplierInput input = supplierInputRepository.findFirstByTeqipPackageAndTeqipPackageQuestionMasterAndTeqipPmssSupplierMaster(teqipPackage, question, supplier);
            if (input != null) {
                readOut = Double.valueOf(input.getUserResponse());
            }
        }
        return readOut;

    }

    Date setLoaDate(TeqipPackage teqipPackage, TeqipPmssSupplierMaster supplier) {
        Date loaDate = null;
        TeqipPackageQuestionMaster question = packageQuestionRepository.findByTeqipPackageAndQuestionCategoryAndType(teqipPackage, "opening", "date");

        if (question != null) {
            TeqipPackageQuotationsSupplierInput input = supplierInputRepository.findFirstByTeqipPackageAndTeqipPackageQuestionMasterAndTeqipPmssSupplierMaster(teqipPackage, question, supplier);
            if (input != null) {
                loaDate = new Date(Long.valueOf(input.getUserResponse()));
            }
        }
        return loaDate;

    }

    List<TeqipPmssSupplierMaster> getNonResponsiveSupplier(TeqipPackage teqipPackage) {
        List<TeqipPmssSupplierMaster> supplierList = new ArrayList<>();
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();
        if (teqipPackageSupplierDetails != null) {
            for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {
                TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
                if (Objects.equals(teqipPackageSupplierDetail.getIsFormResponsive(), 0)
                        && Objects.equals(teqipPackageSupplierDetail.getIsFromResponsiveSource(), "OPENING")) {
                } else {
                    supplierList.add(teqipPmssSupplierMaster);

                }
            }
        }
        
        return supplierList;

    }

      public  List<TeqipPackageSupplierDetail> getNonResponsivePackageSupplier(TeqipPackage teqipPackage) {
        List<TeqipPackageSupplierDetail> supplierList = new ArrayList<>();
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();
        if (teqipPackageSupplierDetails != null) {
            for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {
                TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
                if (Objects.equals(teqipPackageSupplierDetail.getIsFormResponsive(), 0)
                        && Objects.equals(teqipPackageSupplierDetail.getIsFromResponsiveSource(), "OPENING")) {
                } else {
                    supplierList.add(teqipPackageSupplierDetail);

                }
            }
        }
        
        return supplierList;

    }
       
    void setWordMap(Map<String, String> words, TeqipPmssSupplierMaster supplier
    ) {
        if (supplier != null) {
            words.put("$$SUP_NAME$$", supplier.getSupplierName());
            words.put("$$SUP_ADDR$$", supplier.getSupllierAddress());
            words.put("$$SUP_CITY$$", supplier.getSupplierCity());
            words.put("$$SUP_STATE$$", supplier.getSupplierState() + "-"
                    + supplier.getSupplierPincode()
            );
            words.put("$$SUP_PHONE$$", supplier.getSupplierPhoneno());
            words.put("$$SUP_EMAIO$$", supplier.getSuplierEmailId());

        }
    }

    List<TeqipPackageQuestionMaster> filterQuestion(List<TeqipPackageQuestionMaster> teqipPackageQuestionMasters, String category
    ) {
        if (teqipPackageQuestionMasters != null) {

            return teqipPackageQuestionMasters.stream().filter((object) -> {
                return Objects.equals(category, object.getQuestionCategory());
            }).collect(Collectors.toList());

        }
        return null;
    }

    static String getString(Integer f) {
        return String.valueOf(f);
    }

    static String getString(Double f) {
        if (f != null) {
            if (f % 1 > 0) {
                return String.format("%.2f", f);
            } else {
                return String.format("%.0f", f);
            }
        }
        return "";
    }

    static String getStringTimeformat(String f) {
        if (f != null) {
            if ("00:00".equalsIgnoreCase(f)) {
                f = "12:00 AM";
                return f;
            }
        }
        return f;
    }
}
