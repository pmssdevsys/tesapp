/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAward;
import com.qspear.procurement.persistence.methods.TeqipPackageBidDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageBidINSTAndSCC;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageBidDetailRepository;
import com.qspear.procurement.repositories.method.PackageBidINSTAndSCCRepository;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class BidEvaluationLetter extends AbstractLetter {

    @Autowired
    PackageBidDetailRepository packageBidDetailRepository;

    @Autowired
    PackageBidINSTAndSCCRepository packageBidINSTAndSCCRepository;

    public void generate(Integer packageId, String type, Map<String, String[]> param) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }

        TeqipPackageAward teqipPackageAward = packageAwardRepository.findByTeqipPackage(teqipPackage);

        if (teqipPackageAward == null || teqipPackageAward.getTeqipPmssSupplierMaster() == null) {
            throw new RuntimeException("Supplier not found");

        }
        TeqipPackageBidDetail bidDetail = packageBidDetailRepository.findByTeqipPackage(teqipPackage);
        TeqipPackageBidINSTAndSCC bidINSTAndSCC = packageBidINSTAndSCCRepository.findByTeqipPackage(teqipPackage);

        if (bidDetail == null || bidINSTAndSCC == null) {
            throw new RuntimeException("Bid Detail is missing");

        }

        TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageAward.getTeqipPmssSupplierMaster();
        this.deletePreviousDoc(teqipPackage, type);

        Map<String, String> words = this.getWordMap(teqipPackage);
        this.setWordMap(words, teqipPmssSupplierMaster);

        words.put("$$NUMBER_BIDDER_COUNT$$", teqipPackage.getTeqipPackageSupplierDetails() == null ? "" : AbstractWordMapper.getString(teqipPackage.getTeqipPackageSupplierDetails().size()));
        words.put("$$QUES_READ_OUT$$", AbstractWordMapper.getString(this.setReadOutPrice(teqipPackage, teqipPmssSupplierMaster)));
        words.put("$$QUES_READ_OUT_WORDS$$", this.convertToIndianCurrency(AbstractWordMapper.getString(this.setReadOutPrice(teqipPackage, teqipPmssSupplierMaster))));

        Map<String, String[][]> table = new HashMap<>();

        this.setTableMapSupplier(table, teqipPackage.getTeqipPackageSupplierDetails());

        this.setTableMapQuestionMaster(table, this.filterQuestion(teqipPackage.getTeqipPackageQuestionMasters(), "evaluation"), "$$EVALUATION_QUESTION_TABLE$$");

        this.setTableMapQuestionAnswerMaster(table,teqipPmssSupplierMaster,teqipPackage, this.filterQuestion(teqipPackage.getTeqipPackageQuestionMasters(), "evaluation"), "$$EVALUATION_QUESTION_TABLE$$");
        
        this.setTableMapQuestionMaster(table, this.filterQuestion(teqipPackage.getTeqipPackageQuestionMasters(), "post_evaluation"), "$$POST_QUALIFICATION_QUESTION_TABLE$$");
        
        this.setTableMapQuestionAnswerMaster(table,teqipPmssSupplierMaster,teqipPackage, this.filterQuestion(teqipPackage.getTeqipPackageQuestionMasters(), "post_evaluation"), "$$POST_QUALIFICATION_QUESTION_TABLE$$");
        
        this.setTableMapQuotationEvaluation(table,teqipPackage);
                
        this.setTableMapItemGoodsDetails(table, teqipPackage.getTeqipItemGoodsDetails());

        this.createDoc(teqipPackage, null, type, type, words, table, null);

    }

}
