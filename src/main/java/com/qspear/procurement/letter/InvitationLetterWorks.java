/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.letter;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class InvitationLetterWorks extends AbstractLetter {

    @Autowired
    TeqipPackageRepository teqipPackageRepository;

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    public void generate(Integer packageId, String type) throws IOException, InvalidFormatException {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new RuntimeException("Package not found");
        }

        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();

        if (teqipPackageSupplierDetails == null) {
            throw new RuntimeException("No Supplier Defined");

        }

        this.deletePreviousDoc(teqipPackage, MethodConstants.DOC_CAT_INVITATION_WORKS_BLANK);
        this.deletePreviousDoc(teqipPackage, type);

        Map<String, String[][]> table = new HashMap<>();
        this.setTableMapItemWorksDetails(table, teqipPackage.getTeqipItemWorkDetails());
        this.setTableMapPayment(table, teqipPackage.getTeqipPackagePaymentDetails());
        Map<String, String[]> listMap = this.getListMap(teqipPackage);

        Map<String, String> words = this.getWordMap(teqipPackage);
        
        this.createDoc(teqipPackage, null,MethodConstants.DOC_CAT_INVITATION_WORKS_BLANK,
                MethodConstants.DOC_CAT_INVITATION_WORKS, words, table, listMap);

        for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {

            TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
            this.setWordMap(words, teqipPmssSupplierMaster);

            TeqipPackageDocument createDoc = this.createDoc(teqipPackage, teqipPmssSupplierMaster, type,type, words, table, listMap);
            teqipPackageSupplierDetail.setInvitationDocument(createDoc);

            packageSupplierRepository.saveAndFlush(teqipPackageSupplierDetail);
        }

    }

}
