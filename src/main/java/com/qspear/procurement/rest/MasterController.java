package com.qspear.procurement.rest;

import com.qspear.procurement.model.CityMaster;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.qspear.procurement.model.ItemSpecificationModel;
import com.qspear.procurement.model.PrecurementMethodTimeLinePojo;
import com.qspear.procurement.model.StateMaster;
import com.qspear.procurement.model.TeqipActivitiymasterModel;
import com.qspear.procurement.model.TeqipCategorymasterModel;
import com.qspear.procurement.model.TeqipDepartmentModel;
import com.qspear.procurement.model.TeqipItemMasterModel;
import com.qspear.procurement.model.TeqipProcurementmethodModel;
import com.qspear.procurement.model.TeqipSubcategorymasterModel;
import com.qspear.procurement.model.TeqipSubcomponentModel;
import com.qspear.procurement.model.methods.SupplierDetails;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.Teqip_procurement_revisedtimeline;
import com.qspear.procurement.persistence.Teqip_procurementmethod_Usertimeline;
import com.qspear.procurement.repositories.CategoryRepository;
import com.qspear.procurement.repositories.ProcurementmethodRepository;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.Teqip_procurementmethod_UsertimelineRepo;
import com.qspear.procurement.service.PMSSReferenceService;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin
@RequestMapping("pmss/reference")
@RestController

public class MasterController {

    @Autowired
    private PMSSReferenceService pmssReferenceServiceImpl;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    protected CategoryRepository categoryRepository;

    @Autowired
    protected TeqipPackageRepository teqipPackageRepository;

    @Autowired
    protected ProcurementmethodRepository procurementmethodRepository;

    @Autowired
    TeqipPackageRepository packageRepository;

    @Autowired
    Teqip_procurementmethod_UsertimelineRepo Teqip_procurementmethod_UsertimelineRepo;

    @GetMapping("/category")
    public ResponseEntity<List<TeqipCategorymasterModel>> getCategoryMaster(
            @RequestParam(value = "categoryName", required = true) String categoryName) {
        return new ResponseEntity<List<TeqipCategorymasterModel>>(
                pmssReferenceServiceImpl.retrieveCategory(httpServletRequest, categoryName), HttpStatus.OK);
    }

    @GetMapping("/subcategory")
    public ResponseEntity<List<TeqipSubcategorymasterModel>> getSubCategoryMaster(
            @RequestParam(value = "categoryId", required = true) String categoryId) {
        return new ResponseEntity<List<TeqipSubcategorymasterModel>>(
                pmssReferenceServiceImpl.retrieveSubCategory(categoryId, httpServletRequest), HttpStatus.OK);
    }

    @GetMapping("/subcomponent/{categoryId}/{subCategoryId}")
    public ResponseEntity<List<TeqipSubcomponentModel>> getSubComponentMaster(@PathVariable Integer categoryId,
            @PathVariable Integer subCategoryId) {
        return new ResponseEntity<List<TeqipSubcomponentModel>>(
                pmssReferenceServiceImpl.getSubComponentMaster(categoryId, subCategoryId, httpServletRequest),
                HttpStatus.OK);
    }

    @GetMapping("/departments/{type}/{instituteId}")
    public ResponseEntity<List<TeqipDepartmentModel>> getDepartmentsforStateOrInst(
             @PathVariable String type,
            @PathVariable Integer instituteId) {
        return new ResponseEntity<List<TeqipDepartmentModel>>(
                pmssReferenceServiceImpl.getDepartments(type,instituteId, httpServletRequest), HttpStatus.OK);
    }
      @GetMapping("/departments/{type}")
    public ResponseEntity<List<TeqipDepartmentModel>> getDepartmentsforNpiuOrEdcil(
             @PathVariable String type) {
        return new ResponseEntity<List<TeqipDepartmentModel>>(
                pmssReferenceServiceImpl.getDepartments(type,0, httpServletRequest), HttpStatus.OK);
    }

    @GetMapping("/activity")
    public ResponseEntity<List<TeqipActivitiymasterModel>> getActivityMaster(
            @RequestParam(value = "categoryId", required = true) String categoryId) {
        return new ResponseEntity<List<TeqipActivitiymasterModel>>(
                pmssReferenceServiceImpl.retrieveActivity(categoryId, httpServletRequest), HttpStatus.OK);
    }

    @GetMapping("/procMethods")
    public ResponseEntity<List<TeqipProcurementmethodModel>> getProcurementMenthods(
            @RequestParam(value = "categoryId", required = true) Integer categoryId,
            @RequestParam(value = "isproprietary", required = true) Boolean isproprietary,
            @RequestParam(value = "isgem", required = true) Boolean isgem,
            @RequestParam(value = "thresholdValue", required = true) Double thresholdValue) {
        return new ResponseEntity<List<TeqipProcurementmethodModel>>(pmssReferenceServiceImpl
                .retrieveProcurementMethods(httpServletRequest, categoryId, isproprietary, isgem, thresholdValue),
                HttpStatus.OK);
    }

    @GetMapping("/items")
    public ResponseEntity<List<TeqipItemMasterModel>> getItems(
            @RequestParam(value = "category", required = true) String category,
            @RequestParam(value = "subCategory", required = true) String subCategory) {
        return new ResponseEntity<List<TeqipItemMasterModel>>(
                pmssReferenceServiceImpl.retrieveItems(category, subCategory), HttpStatus.OK);
    }

    @GetMapping("/items/find")
    public ResponseEntity<List<ItemSpecificationModel>> getSimilarItem(
            @RequestParam(value = "itemName", required = true) String itemName) {
        return new ResponseEntity<List<ItemSpecificationModel>>(
                pmssReferenceServiceImpl.retrieveItems(itemName), HttpStatus.OK);
    }

    @GetMapping("/supplier/{type}")
    public ResponseEntity<List<SupplierDetails>> getSupplier(
            @PathVariable(value = "type", required = true) int type,
            @RequestParam(value = "state", required = false) String state,
            @RequestParam(value = "city", required = false) String city
    ) {
        return new ResponseEntity<List<SupplierDetails>>(
                pmssReferenceServiceImpl.retrieveSupplier(type, state, city), HttpStatus.OK);
    }

    @SuppressWarnings("unused")
    @RequestMapping(value = "/addProcurementmethodUserTimeline", method = RequestMethod.POST)
    public Map<String, Object> addPrecurementMethodUserTimeLine(HttpServletRequest request, @RequestBody PrecurementMethodTimeLinePojo pojo)
            throws Exception {
        Map<String, Object> mapObj = new LinkedHashMap();
        try {
            Teqip_procurementmethod_Usertimeline Usertimelines = pmssReferenceServiceImpl.findbyPackageId(pojo.getPackageId());
            if (Usertimelines == null) {
                Teqip_procurementmethod_Usertimeline Usertimeline = new Teqip_procurementmethod_Usertimeline();
                Usertimeline.setAdvertisementDate_WithoutBankNOC_days(pojo.getAdvertisementDate_WithoutBankNOC_days());
                Usertimeline.setAdvertisementDatedays(pojo.getAdvertisementDatedays());
                Usertimeline.setBankNOCForBiddingDocumentsdays(pojo.getBankNOCForBiddingDocumentsdays());
                Usertimeline.setBidDocumentationPreprationDatedays(pojo.getBidDocumentationPreprationDatedays());
                Usertimeline.setBidInvitationDatedays(pojo.getBidInvitationDatedays());
                Usertimeline.setBidOpeningDatedays(pojo.getBidOpeningDatedays());
                Usertimeline.setContractAwardDate_WithoutBankNOCdays(pojo.getContractAwardDate_WithoutBankNOCdays());
                Usertimeline.setContractAwardDatedays(pojo.getContractAwardDatedays());
                Usertimeline.setContractCompletionDate_WithoutBankNOC_days(pojo.getContractCompletionDate_WithoutBankNOC_days());
                Usertimeline.setContractCompletionDatedays(pojo.getContractCompletionDatedays());
                Usertimeline.setCreatedBy(pojo.getCreatedBy());
                Usertimeline.setCreatedOn(pojo.getCreatedOn());
                Usertimeline.setEvaluationDate_WithoutBankNOCdays(pojo.getEvaluationDate_WithoutBankNOCdays());
                Usertimeline.setEvaluationDatedays(pojo.getEvaluationDatedays());
                Usertimeline.setFinalDraftToBeForwardedToTheBankDatedays(pojo.getFinalDraftToBeForwardedToTheBankDatedays());
                Usertimeline.setLastDateToReceiveProposalsdays(pojo.getLastDateToReceiveProposalsdays());
                Usertimeline.setModifiedBy(pojo.getModifiedBy());
                Usertimeline.setModifiedOn(pojo.getModifiedOn());
                Usertimeline.setNoObjectionFromBankForEvaluationdays(pojo.getNoObjectionFromBankForEvaluationdays());
                Usertimeline.setNoObjectionFromBankForRFPdays(pojo.getNoObjectionFromBankForRFPdays());
                Usertimeline.setrFPIssuedDate_WithoutBankNOCdays(pojo.getrFPIssuedDate_WithoutBankNOCdays());
                Usertimeline.setrFPIssuedDatedays(pojo.getrFPIssuedDatedays());
                Usertimeline.settORFinalizationDate_WithoutBankNOCdays(pojo.gettORFinalizationDate_WithoutBankNOCdays());
                Usertimeline.settORFinalizationDatedays(pojo.gettORFinalizationDatedays());
                Usertimeline.setLastdateofsubmissionofEOI(pojo.getLastdateofsubmissionofEOI());
                Usertimeline.setShortlistingOfEOI(pojo.getShortlistingOfEOI());
                Usertimeline.setFinancialEvaluationDate(pojo.getFinancialEvaluationDate());
                Usertimeline.setrFPApprovalDate(pojo.getrFPApprovalDate());
                Usertimeline.setProposalInvitationDate(pojo.getProposalInvitationDate());
                Usertimeline.setProposalSubmissionDate(pojo.getProposalSubmissionDate());

                if (pojo.getCategoryId() != null) {
                    TeqipCategorymaster category = categoryRepository.findOne(pojo.getCategoryId());
                    Usertimeline.setTeqipCategorymaster(category);
                }
                if (pojo.getProcurementmethodId() != null) {
                    TeqipProcurementmethod procurementmethod = procurementmethodRepository.findOne(pojo.getProcurementmethodId());
                    Usertimeline.setProcurementmethod(procurementmethod);
                }
                if (pojo.getPackageId() != null) {
                    TeqipPackage pack = packageRepository.findOne(pojo.getPackageId());
                    Usertimeline.setTeqipPackage(pack);
                }

                Teqip_procurementmethod_Usertimeline userstimline = Teqip_procurementmethod_UsertimelineRepo.save(Usertimeline);

                if (userstimline != null) {
                    mapObj.put("RESULT", "SUCESS");
                    mapObj.put("DATA", userstimline);

                } else {
                    mapObj.put("RESULT", "FAIL");
                }
            } else {
                throw new RuntimeException("Package id already exist");
            }
        } catch (Exception e) {
            mapObj.put("TOKEN", "ABC@123");
            mapObj.put("RESULT", "FAIL");
            e.printStackTrace();
        }
        return mapObj;
    }

    @SuppressWarnings("unused")
    @PutMapping("/updateProcurementmethodUserTimeline")
    public Map<String, Object> updatePrecurementMethodUserTimeLine(HttpServletRequest request, @RequestBody PrecurementMethodTimeLinePojo pojo)
            throws Exception {
        Map<String, Object> mapObj = new LinkedHashMap();
        try {
            Teqip_procurementmethod_Usertimeline Usertimeline = pmssReferenceServiceImpl.getProcUserTimeLine(pojo.getPackageId());
            Usertimeline.setAdvertisementDate_WithoutBankNOC_days(pojo.getAdvertisementDate_WithoutBankNOC_days());
            Usertimeline.setAdvertisementDatedays(pojo.getAdvertisementDatedays());
            Usertimeline.setBankNOCForBiddingDocumentsdays(pojo.getBankNOCForBiddingDocumentsdays());
            Usertimeline.setBidDocumentationPreprationDatedays(pojo.getBidDocumentationPreprationDatedays());
            Usertimeline.setBidInvitationDatedays(pojo.getBidInvitationDatedays());
            Usertimeline.setBidOpeningDatedays(pojo.getBidOpeningDatedays());
            Usertimeline.setContractAwardDate_WithoutBankNOCdays(pojo.getContractAwardDate_WithoutBankNOCdays());
            Usertimeline.setContractAwardDatedays(pojo.getContractAwardDatedays());
            Usertimeline.setContractCompletionDate_WithoutBankNOC_days(pojo.getContractCompletionDate_WithoutBankNOC_days());
            Usertimeline.setContractCompletionDatedays(pojo.getContractCompletionDatedays());
            Usertimeline.setCreatedBy(pojo.getCreatedBy());
            Usertimeline.setCreatedOn(pojo.getCreatedOn());
            Usertimeline.setEvaluationDate_WithoutBankNOCdays(pojo.getEvaluationDate_WithoutBankNOCdays());
            Usertimeline.setEvaluationDatedays(pojo.getEvaluationDatedays());
            Usertimeline.setFinalDraftToBeForwardedToTheBankDatedays(pojo.getFinalDraftToBeForwardedToTheBankDatedays());
            Usertimeline.setLastDateToReceiveProposalsdays(pojo.getLastDateToReceiveProposalsdays());
            Usertimeline.setModifiedBy(pojo.getModifiedBy());
            Usertimeline.setModifiedOn(pojo.getModifiedOn());
            Usertimeline.setNoObjectionFromBankForEvaluationdays(pojo.getNoObjectionFromBankForEvaluationdays());
            Usertimeline.setNoObjectionFromBankForRFPdays(pojo.getNoObjectionFromBankForRFPdays());
            Usertimeline.setrFPIssuedDate_WithoutBankNOCdays(pojo.getrFPIssuedDate_WithoutBankNOCdays());
            Usertimeline.setrFPIssuedDatedays(pojo.getrFPIssuedDatedays());
            Usertimeline.settORFinalizationDate_WithoutBankNOCdays(pojo.gettORFinalizationDate_WithoutBankNOCdays());
            Usertimeline.settORFinalizationDatedays(pojo.gettORFinalizationDatedays());
            Usertimeline.setLastdateofsubmissionofEOI(pojo.getLastdateofsubmissionofEOI());
            Usertimeline.setShortlistingOfEOI(pojo.getShortlistingOfEOI());
            Usertimeline.setFinancialEvaluationDate(pojo.getFinancialEvaluationDate());
            Usertimeline.setrFPApprovalDate(pojo.getrFPApprovalDate());
            Usertimeline.setProposalInvitationDate(pojo.getProposalInvitationDate());
            Usertimeline.setProposalSubmissionDate(pojo.getProposalSubmissionDate());

            Teqip_procurementmethod_Usertimeline userstimline = pmssReferenceServiceImpl.saveUserTimeLine(Usertimeline);

            if (userstimline != null) {
                mapObj.put("RESULT", "SUCESS");
                mapObj.put("DATA", userstimline);

            } else {
                mapObj.put("RESULT", "FAIL");
            }
        } catch (Exception e) {
            mapObj.put("TOKEN", "ABC@123");
            mapObj.put("RESULT", "FAIL");
            e.printStackTrace();
        }
        return mapObj;
    }

    @GetMapping("/getprocurementUserTimeLine/{packageid}")
    public ResponseEntity<PrecurementMethodTimeLinePojo> getProcurementUserTimeLine(@PathVariable("packageid") Integer packageid) {
        return new ResponseEntity<PrecurementMethodTimeLinePojo>(pmssReferenceServiceImpl.findProcureUserTime(packageid), HttpStatus.OK);
    }

    @SuppressWarnings("unused")
    @RequestMapping(value = "/addProcurementmethodrevisedTimeline", method = RequestMethod.POST)
    public Map<String, Object> addPrecurementMethodrevisedTimeLine(@RequestBody(required = true) PrecurementMethodTimeLinePojo revised)
            throws Exception {
        return pmssReferenceServiceImpl.saverevisedPlan(revised);
    }

    @SuppressWarnings("unused")
    @RequestMapping(value = "/updateProcurementmethodrevisedTimeline", method = RequestMethod.PUT)
    public Map<String, Object> updatePrecurementMethodrevisedTimeLine(@RequestBody(required = true) PrecurementMethodTimeLinePojo revised)
            throws Exception {
        return pmssReferenceServiceImpl.updaterevisedPlan(revised);
    }

    @GetMapping("getprocurementrevisedTimeLine/{packageId}")
    public ResponseEntity<PrecurementMethodTimeLinePojo> getProcurementrevisedTimeLine(@PathVariable("packageId") Integer packageId) {
        return new ResponseEntity<PrecurementMethodTimeLinePojo>(pmssReferenceServiceImpl.findProcurerevisedTime(packageId), HttpStatus.OK);
    }

    @GetMapping("/states")
    public ResponseEntity<List<StateMaster>> getState() {
        return new ResponseEntity(pmssReferenceServiceImpl.findAllState(), HttpStatus.OK);
    }

    @GetMapping("/cities")
    public ResponseEntity<List<CityMaster>> getCities(
            @RequestParam(value = "state") String stateName) {
        return new ResponseEntity(pmssReferenceServiceImpl.findAllCities(stateName), HttpStatus.OK);
    }

}
