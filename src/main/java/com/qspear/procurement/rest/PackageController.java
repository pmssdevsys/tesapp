package com.qspear.procurement.rest;

import com.qspear.procurement.constants.ProcurementConstants;
import com.qspear.procurement.model.PackageHistoryList;
import com.qspear.procurement.model.PlanSatus;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.qspear.procurement.model.TeqipPackageModel;
import com.qspear.procurement.model.TeqipPlanAprovalModel;
import com.qspear.procurement.model.TeqipProcurementmasterModel;
import com.qspear.procurement.model.TeqipSuccess;
import com.qspear.procurement.model.TeqipTimelineModel;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPackageHistory;
import com.qspear.procurement.persistence.TeqipPlanApprovalstatus;
import com.qspear.procurement.security.util.LoginUser;
import com.qspear.procurement.service.PMSSReferenceService;
import com.qspear.procurement.service.PackageService;
import com.qspear.procurement.service.mapper.ProcurementMapper;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RequestMapping("pmss")
@RestController

public class PackageController {

    @Autowired
    private PMSSReferenceService pmssReferenceServiceImpl;

    @Autowired
    private PackageService packageServiceImpl;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    ProcurementMapper procurementMapper;

    @PostMapping("/package")
    ResponseEntity<TeqipPackageModel> savePackage(@RequestBody(required = true) TeqipPackageModel packageDetails) {

        return new ResponseEntity<TeqipPackageModel>(
                packageServiceImpl.savePackage(httpServletRequest, packageDetails), HttpStatus.OK);
    }

    @DeleteMapping("/package/{packageId}")
    ResponseEntity<TeqipSuccess> deletePackage(@PathVariable int packageId) {

        packageServiceImpl.deletePackage(packageId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/supplier/{packageId}/{supplierId}")
    ResponseEntity<TeqipSuccess> deleteSupplier(@PathVariable int packageId, @PathVariable int supplierId) {

        packageServiceImpl.deleteSupplier(packageId, supplierId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/payment/{packageId}/{packagePaymentId}")
    ResponseEntity<TeqipSuccess> deletePackagePayment(@PathVariable int packageId, @PathVariable int packagePaymentId) {

        packageServiceImpl.deletePackagePayment(packageId, packagePaymentId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/upload/{packageId}/{packageDocumentId}")
    ResponseEntity<TeqipSuccess> deletePackageUpload(@PathVariable int packageId, @PathVariable int packageDocumentId) {

        packageServiceImpl.deletePackageUpload(packageId, packageDocumentId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/question/{packageId}/{questionId}")
    ResponseEntity<TeqipSuccess> deleteQuestion(@PathVariable int packageId, @PathVariable int questionId) {

        packageServiceImpl.deleteQuestion(packageId, questionId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/technicalCriteria/{packageId}/{technicalCriteriaId}")
    ResponseEntity<TeqipSuccess> deleteTechnicalCriteria(@PathVariable int packageId, @PathVariable int technicalCriteriaId) {

        packageServiceImpl.deleteTechnicalCriteria(packageId, technicalCriteriaId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/technicalSubCriteria/{packageId}/{technicalSubCriteriaId}")
    ResponseEntity<TeqipSuccess> deleteTechnicalSubCriteria(@PathVariable int packageId, @PathVariable int technicalSubCriteriaId) {

        packageServiceImpl.deleteTechnicalSubCriteria(packageId, technicalSubCriteriaId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/techEvalCommittee/{packageId}/{memberId}")
    ResponseEntity<TeqipSuccess> deleteTechEvalCommittee(@PathVariable int packageId, @PathVariable int memberId) {

        packageServiceImpl.deleteTechEvalCommittee(packageId, memberId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @GetMapping("/reference/plan")
    public ResponseEntity<List<TeqipProcurementmasterModel>> getActiveProcurementPlans() {
        return new ResponseEntity<List<TeqipProcurementmasterModel>>(
                pmssReferenceServiceImpl.retrievePlans(httpServletRequest), HttpStatus.OK);
    }

    @GetMapping("/reference/plan/status/{type}/{id}")
    public ResponseEntity<PlanSatus> getProcurementPlansStatus(
            @PathVariable Integer id,
            @PathVariable String type) {
        return new ResponseEntity<PlanSatus>(
                pmssReferenceServiceImpl.retrievePlanStatus(type, id), HttpStatus.OK);
    }

    @GetMapping("/package/timeline/{categoryId}/{methodId}")
    ResponseEntity<TeqipTimelineModel> packageTimeline(@PathVariable Integer categoryId, @PathVariable Integer methodId,
            @RequestParam(value = "sanctionDate", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date sanctionDate) {
        return new ResponseEntity<TeqipTimelineModel>(
                packageServiceImpl.getPackageTimeline(
                        methodId, sanctionDate, categoryId),
                HttpStatus.OK);
    }

    /**
     * Fetch packages according to the user Role
     *
     * @param packageState ALL|INITIATE|INPROGRESS |CANCELLED|COMPLETED
     * @return
     */
    @GetMapping("package/{packageState}")
    ResponseEntity<List<TeqipPackageModel>> packageList(
            @PathVariable String packageState,
            @RequestParam(required = false) Integer planId,
            @RequestParam(required = false) Integer id,
            @RequestParam(required = false) String roleType,
            @RequestParam(required = false) String method,
            @RequestParam(required = false) Integer categoryId,
            @RequestParam(required = false) Integer subCategoryId) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute("claim");

        if (roleType == null) {
            roleType = ((String) claim.getPmssrole()).toUpperCase();
        }
        if (id == null) {
            if (ProcurementConstants.isInstitutionalRole(roleType)) {
                id = ((Integer) claim.getInstid());
            }

            if (ProcurementConstants.isStateRole(roleType)) {
                id = ((Integer) claim.getStateid());
            }
        }
        List<TeqipPackage> packages = packageServiceImpl.getPackages(roleType, id, planId, packageState);
        List<TeqipPackageModel> mapTeqipPackages = procurementMapper.mapTeqipPackages(packages, true);
        mapTeqipPackages = procurementMapper.filterPackageModel(mapTeqipPackages, method, categoryId, subCategoryId);

        if (mapTeqipPackages != null) {
            Collections.sort(mapTeqipPackages);
        }
        return new ResponseEntity<List<TeqipPackageModel>>(
                mapTeqipPackages,
                HttpStatus.OK);
    }

    /**
     * Fetch service return list of rejection comments
     *
     * @param type INST|SPFU|NPIU
     * @return
     */
    @GetMapping("package/rejectionComments/{type}/{id}")
    ResponseEntity<List<TeqipPlanAprovalModel>> rejectionComments(
            @PathVariable Integer id,
            @PathVariable String type) {

        return new ResponseEntity<>(
                packageServiceImpl.rejectionComments(type, id),
                HttpStatus.OK);
    }

    /**
     * Fetch packages for PPview
     *
     * @param type INST|SPFU|NPIU
     * @return
     */
    @GetMapping("package/ppview/{type}/{id}")
    ResponseEntity<List<TeqipPackageModel>> ppview(
            @PathVariable Integer id,
            @PathVariable String type) {

        return new ResponseEntity<>(
                packageServiceImpl.ppview(type, id),
                HttpStatus.OK);
    }

    @GetMapping("package/prirorReview")
    ResponseEntity<List<TeqipPackageModel>> priorReview() {

        return new ResponseEntity<>(
                packageServiceImpl.priorReview(),
                HttpStatus.OK);
    }

    @PostMapping("package/initiate/{packageId}")
    ResponseEntity<TeqipSuccess> initiatePackage(@PathVariable int packageId,
            @RequestParam(value = "initiateDate", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date initiateDate,
            @RequestParam(value = "isInitiated", required = false, defaultValue = "0") int isInitiated,
            @RequestParam(value = "sanctionDate", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date sanctionDate
    ) {

        packageServiceImpl.initiatePackage(packageId, initiateDate, isInitiated, sanctionDate);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/deleteWCC/{packageId}/{wccId}")
    ResponseEntity<TeqipSuccess> deleteWorkCompletionCertificate(@PathVariable int packageId, @PathVariable int wccId) {

        packageServiceImpl.deleteWorkCompletionCertificate(packageId, wccId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/deleteGRNItem/{packageId}/{grnItemId}")
    ResponseEntity<TeqipSuccess> deleteGRMItem(@PathVariable int packageId, @PathVariable int grnItemId) {

        packageServiceImpl.deleteGRMItem(packageId, grnItemId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @DeleteMapping("/deleteGRN/{packageId}/{grnId}")
    ResponseEntity<TeqipSuccess> deleteGRM(@PathVariable int packageId, @PathVariable int grnItemId) {

        packageServiceImpl.deleteGRN(packageId, grnItemId);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    /**
     *
     * @param type revision|approval
     * @return
     */
    @GetMapping("/package/history")
    ResponseEntity<List<PackageHistoryList>> packageHistoryList(
            @RequestParam(required = false) Integer instituteId,
            @RequestParam(required = false) Integer stateId) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute("claim");
        String roleType = ((String) claim.getPmssrole()).toUpperCase();

        if (ProcurementConstants.isInstitutionalRole(roleType)) {
            instituteId = ((Integer) claim.getInstid());
            stateId = 0;
        }

        if (ProcurementConstants.isStateRole(roleType)) {
            instituteId = 0;
            stateId = ((Integer) claim.getStateid());
        }

        if (instituteId == null) {
            instituteId = 0;
        }
        if (stateId == null) {
            stateId = 0;
        }

        return new ResponseEntity<List<PackageHistoryList>>(
                packageServiceImpl.packageHistory(instituteId, stateId),
                HttpStatus.OK);
    }

    /**
     * Fetch packages according to the user Role
     *
     * @param packageState ALL|INITIATE|INPROGRESS |CANCELLED|COMPLETED
     * @return
     */
    @GetMapping("package/revision/{revisionid}")
    ResponseEntity<List<TeqipPackageModel>> packageRevision(
            @PathVariable int revisionid,
            @RequestParam(required = false) Integer planId,
            @RequestParam(required = true) int id,
            @RequestParam(required = true) String roleType) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute("claim");

        List<TeqipPackageHistory> packages = packageServiceImpl.getPackagesHistory(roleType, id, planId, revisionid);
        List<TeqipPackageModel> mapTeqipPackages = procurementMapper.mapTeqipPackagesHistory(packages);
        return new ResponseEntity<List<TeqipPackageModel>>(
                mapTeqipPackages,
                HttpStatus.OK);
    }

}
