package com.qspear.procurement.rest;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qspear.procurement.constants.PmssConstant;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSException;
import com.qspear.procurement.model.FileObect;
import com.qspear.procurement.model.LoginModel;
import com.qspear.procurement.model.Mail;
import com.qspear.procurement.model.TeqipSuccess;
import com.qspear.procurement.model.UserLoginDetails;
import com.qspear.procurement.persistence.TeqipUsersDetail;
import com.qspear.procurement.repositories.TeqipUserDetailsRepository;
import com.qspear.procurement.security.util.AesUtil;

@CrossOrigin
@RestController
@RequestMapping("/pmss/password")

public class SignUpController {
	@Autowired
	private JavaMailSender sender;
	@Autowired
	TeqipUserDetailsRepository teqipUserDetailsRepository;

	@PostMapping("/forgetPassword")
	ResponseEntity<TeqipSuccess> provideCredential(@RequestBody Mail mail) throws Exception {
		if (mail == null) {
			return null;
		}
		TeqipUsersDetail detail = teqipUserDetailsRepository.findByemail(mail.getFrom());
		if (detail != null) {
			
			String pawordDecrypts = AesUtil.decrypt(detail.getPassword());
			JavaMailSenderImpl mailsender = new JavaMailSenderImpl();
			Mail details = new Mail();
			Properties props = mailsender.getJavaMailProperties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.debug", "true");
			props.put(" Djava.net.preferIPv4Stack", "true");
			props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

			details.setFrom("kashif1991nadim@gmail.com");
			details.setTo(new String[] { detail.getEmail() });
			String a = "Your userName is" + ": " + detail.getUserName() + "~" + "Your Password is" + ": "
					+ pawordDecrypts;
			details.setBody("Your userName is" + ": " + detail.getUserName() + "<br/>" + "Your Password is" + ": "
					+ pawordDecrypts);
			details.setSubject("User Credentials");
			//sendEmail(details);
			
			if(detail.getEmailattemptTime()==null){
				detail.setEmailattemptTime(  new Date());
				teqipUserDetailsRepository.save(detail);
			}
			else if(detail.getEmailattemptTime()!=null){
				LocalDateTime t=LocalDateTime.now();
				int current=t.getSecond();
				int old=detail.getEmailattemptTime().getSeconds();
					
				if (Math.abs(current-old) >= 60*10){
					detail.setEmailattemptTime(  new Date());
					teqipUserDetailsRepository.save(detail);
				}
				else if(Math.abs(current-old) < 60*10){
					int count =0;
					if(detail.getEmailAttempt()==null || detail.getEmailAttempt()==0){
						count=0+1;
						detail.setEmailAttempt(count);
						teqipUserDetailsRepository.save(detail);
						sendEmail(details);
						return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true), HttpStatus.OK);
					}
					else if(detail.getEmailAttempt()!=null){
					 
						if(detail.getEmailAttempt()<5){
					if(count<5){
						count =detail.getEmailAttempt()+1;
					
					detail.setEmailAttempt(count);
					
					teqipUserDetailsRepository.save(detail);
					sendEmail(details);
					return new ResponseEntity<TeqipSuccess>(new TeqipSuccess(5-count+" attempts are left userdetail has been already sent to email", true), HttpStatus.OK);
					}

					}
						else if(detail.getEmailAttempt()>=4){
						
		                      detail.setIsBlocked(true);
		                      detail.setBlockedAt(new Timestamp(new Date().getTime()));
							teqipUserDetailsRepository.save(detail);
								return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("you are blocked for 24 hours", true), HttpStatus.OK);
							}
				
					}
				}
			}
	return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true), HttpStatus.OK);
		}
		 else {
			return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Failed ", false), HttpStatus.OK);
		}
	}

	@PostMapping("/resetPassword")
	ResponseEntity<LoginModel> resetCredential(@RequestBody UserLoginDetails userinfo) throws Exception {
		if (userinfo == null) {
			return null;
		}
		try {
			// String
			// oldpawordDecrypt=Base64Encoder.decodePassword(userinfo.getOldPassword());
			String oldpawordDecrypt = AesUtil.decrypt(userinfo.getOldPassword());
			// String
			// newpawordDecrypt=Base64Encoder.decodePassword(userinfo.getNewPassword());
			String newpawordDecrypt = AesUtil.decrypt(userinfo.getNewPassword());
			// String
			// confirmpawordDecrypt=Base64Encoder.decodePassword(userinfo.getConfirmNewPassword());
			String confirmpawordDecrypt = AesUtil.decrypt(userinfo.getConfirmNewPassword());
			TeqipUsersDetail detail = teqipUserDetailsRepository.find(userinfo.getUserName(), true);
			
			String pawordDecrypts = AesUtil.decrypt(detail.getPassword());
			if(pawordDecrypts.equals(oldpawordDecrypt)){
				System.out.println("Hello");
			}
			
			if (pawordDecrypts !=null&&pawordDecrypts.equals(oldpawordDecrypt)) {
				if (confirmpawordDecrypt.equalsIgnoreCase(newpawordDecrypt)) {
					detail.setPassword(userinfo.getConfirmNewPassword());

				} else {
					throw new PMSSException(" NewPassword and ConfirmNewPassword does't match.",
							new ErrorCode("PMSS-500", " NewPassword and ConfirmNewPassword does't match."));

				}
				TeqipUsersDetail updateuser = teqipUserDetailsRepository.save(detail);
				JavaMailSenderImpl mailsender = new JavaMailSenderImpl();
				Mail details = new Mail();
				Properties props = mailsender.getJavaMailProperties();
				props.put("mail.transport.protocol", "smtp");
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.debug", "true");
				props.put(" Djava.net.preferIPv4Stack", "true");
				props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

				details.setFrom("pmssapp@gmail.com");
				details.setTo(new String[] { detail.getEmail() });
				String changepwd = AesUtil.decrypt(updateuser.getPassword());
				details.setBody("Your userName is" + ": " + updateuser.getUserName() + "<br/>" + "Your Password is"
						+ ": " + changepwd);
				// String newline =
				details.setSubject("Password has been changed successfully");
				sendEmail(details);
				// return new ResponseEntity<TeqipSuccess>(new
				// TeqipSuccess("Success", true), HttpStatus.OK);
				return new ResponseEntity<LoginModel>(
						new LoginModel(PmssConstant.SUCCUESS, "Password has been changed successfully", null, null),
						null, HttpStatus.OK);
			} else {
				// return new ResponseEntity<TeqipSuccess>(new
				// TeqipSuccess("Failed", false), HttpStatus.OK);
				return new ResponseEntity<LoginModel>(
						new LoginModel(PmssConstant.FAIL, "please enter valid user name and password", null, null),
						null, HttpStatus.OK);
			}
		} catch (Exception ex) {
			return new ResponseEntity<LoginModel>(new LoginModel(PmssConstant.FAIL, ex.getMessage(), null, null),
					HttpStatus.OK);

		}
	}

	private void sendEmail(Mail mail) throws Exception {
		MimeMessage message = sender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(message, true, CharEncoding.UTF_8);

		helper.setTo(mail.getTo());

		if (mail.getFrom() != null) {
			helper.setFrom(mail.getFrom());
		}

		if (mail.getCc() != null) {
			helper.setCc(mail.getCc());
		}
		if (mail.getBcc() != null) {

			helper.setBcc(mail.getBcc());
		}
		helper.setSubject(mail.getSubject());

		helper.setText(mail.getBody(), true); // set to html

		if (mail.getFileobj() != null) {
			for (FileObect fileObect : mail.getFileobj()) {
				helper.addAttachment(fileObect.getFileName(), fileObect.getFile());
			}
		}
		sender.send(message);
	}

}
