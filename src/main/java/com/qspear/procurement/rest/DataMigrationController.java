package com.qspear.procurement.rest;

import com.qspear.procurement.migration.DataMigrationService;
import com.qspear.procurement.model.TeqipSuccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by e1002703 on 4/3/2018.
 */
@CrossOrigin
@RestController
@RequestMapping("/pmss/migration")
public class DataMigrationController {

    @Autowired
    DataMigrationService dataMigrationService;

    @PostMapping("/load")
    public ResponseEntity<TeqipSuccess> migrateData(
            @RequestParam(required = false) Boolean onlyMaster) {
        onlyMaster = onlyMaster == null ? false : onlyMaster;
        dataMigrationService.execute(onlyMaster);
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

}
