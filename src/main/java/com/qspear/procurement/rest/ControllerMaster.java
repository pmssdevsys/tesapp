package com.qspear.procurement.rest;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.qspear.procurement.configuration.dao.ProcurmentPlanDaoImp;
import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.constants.PmssConstant;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.JsonResponse;
import com.qspear.procurement.exception.PMSSBudgetException;
import com.qspear.procurement.model.ComponentMasterModel;
import com.qspear.procurement.model.InstitutionlogoPojo;
import com.qspear.procurement.model.NpiuDetailsMasterPojo;
import com.qspear.procurement.model.PrecurementMethodTimeLinePojo;
import com.qspear.procurement.model.Rolemastermodel;
import com.qspear.procurement.model.StateAllData;
import com.qspear.procurement.model.SupplierMasterModel;
import com.qspear.procurement.model.TeqipActivitiymasterModel;
import com.qspear.procurement.model.TeqipCategorymasterModel;
import com.qspear.procurement.model.TeqipDepartmentModel;
import com.qspear.procurement.model.TeqipInstitutionPojo;
import com.qspear.procurement.model.TeqipInstitutionTypeModel;
import com.qspear.procurement.model.TeqipInstitutiondepartmentmappingModel;
import com.qspear.procurement.model.TeqipInstitutionsubcomponentmappingPojo;
import com.qspear.procurement.model.TeqipItemMasterModel;
import com.qspear.procurement.model.TeqipPmssInstitutionpurchasecommittePojo;
import com.qspear.procurement.model.TeqipPmssThreshholdmodel;
import com.qspear.procurement.model.TeqipStatemastermodel;
import com.qspear.procurement.model.TeqipSubcategorymasterModel;
import com.qspear.procurement.model.TeqipSubcomponentModel;
import com.qspear.procurement.model.TeqipSubcomponentmasterModel;
import com.qspear.procurement.model.TeqipSuccess;
import com.qspear.procurement.model.UserLoginDetails;
import com.qspear.procurement.model.UserPojo;
import com.qspear.procurement.persistence.DepartmentMaster;
import com.qspear.procurement.persistence.NpiuDetailsMaster;
import com.qspear.procurement.persistence.SupplierMaster;
import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import com.qspear.procurement.persistence.TeqipInstitutionsubcomponentmapping;
import com.qspear.procurement.persistence.TeqipInstitutiontype;
import com.qspear.procurement.persistence.TeqipPmssInstitutionlogo;
import com.qspear.procurement.persistence.TeqipPmssInstitutionpurchasecommitte;
import com.qspear.procurement.persistence.TeqipRolemaster;
import com.qspear.procurement.persistence.TeqipUsersDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import com.qspear.procurement.repositories.TeqipInstitutionRepository;
import com.qspear.procurement.repositories.TeqipPmssInstitutionlogoRepository;
import com.qspear.procurement.security.util.LoginUser;
import com.qspear.procurement.service.ProcurmentPlanServiceImp;

@CrossOrigin
@RestController
@RequestMapping("/pmss/configuration")
public class ControllerMaster {

	@Autowired
	ProcurmentPlanServiceImp procurmentPlanServiceImp;

	@Autowired
	ProcurmentPlanDaoImp procurmentPlanDaoImp;
	@Autowired
	TeqipInstitutionRepository teqipInstitutionRepository;
	@Autowired
	TeqipPmssInstitutionlogoRepository teqipPmssInstitutionlogoRepository;

	@GetMapping("/getallactivity")
	public ArrayList<TeqipActivitiymasterModel> getAllActivity(HttpServletRequest request) {
	HttpSession session=	request.getSession();

		System.out.println("ipAdd-->"+	session.getId());
		System.out.println("browserName--->"+ request.getHeader("user-agent"));

		return (ArrayList<TeqipActivitiymasterModel>) procurmentPlanServiceImp.findAllActivityList();
	}

	@GetMapping("/getAllCategories")
	public ArrayList<TeqipCategorymasterModel> getAllCat() {
		return (ArrayList<TeqipCategorymasterModel>) procurmentPlanServiceImp.findAllCategoryList();
	}

	@PostMapping("/addnewactivity")
	@ExceptionHandler(java.sql.SQLException.class)
	public JsonResponse createActivity(@Valid @RequestBody TeqipActivitiymasterModel newActivity) {
		// user.setCreated_by(1);

		try {
			String activity = procurmentPlanServiceImp.findActivityByName(newActivity.getActivityName());
			if (activity == null) {
				TeqipActivitiymasterModel response = procurmentPlanServiceImp.save(newActivity);
				return (new JsonResponse(PmssConstant.SUCCUESS, PmssConstant.SUCCESS_MESSAGE, response));
			} else {
				throw new Exception("Activity Is Already Exist");
			}
		} catch (Exception e) {
			return (new JsonResponse(PmssConstant.FALIURE, e.getMessage(), PmssConstant.EMPTY_LIST));
		}

	}

	@PutMapping("/updateActivity")
	public Map<String,Object> updateActivity(@RequestBody TeqipActivitiymasterModel activity) {
		Map<String,Object> map=new HashMap();
		try{
		TeqipActivitiymasterModel activitymodel = procurmentPlanServiceImp.updateActivity(activity);
		map.put("Success",activitymodel.getActivityId() ) ;
		}
		catch(Exception e){
			map.put("Failure", e.getMessage());
		}
		return map;
	}

	@GetMapping("/getAllComponent")
	public List<ComponentMasterModel> getAllComponent() {
		return (List<ComponentMasterModel>) procurmentPlanServiceImp.findAllComponenet();
	}

	@GetMapping("/getSubComponentById/{componentid}")
	List<TeqipSubcomponentModel> getAllSubComponent(@PathVariable(value = ("componentid")) Long componentId) {
		Integer activeStatus = 1;
		List<TeqipSubcomponentModel> subcomponentmodel = procurmentPlanServiceImp.getAllSubComonentById(componentId,
				activeStatus);

		return subcomponentmodel;

	}

	@PostMapping("/addComponent")
	public ComponentMasterModel addComponent(@RequestBody ComponentMasterModel component) {
		ComponentMasterModel response = procurmentPlanServiceImp.saveComponent(component);
		return response;
	}

	@PostMapping("/addSubComponent")
	public TeqipSubcomponentModel createSubComponent(@RequestBody TeqipSubcomponentModel subComponent) {
		return procurmentPlanServiceImp.addSubComponent(subComponent);
	}

	@DeleteMapping("/disableSubComponent/{id}")
	public ResponseEntity<TeqipSuccess> disableSubComponent(@PathVariable("id") Integer id) {
		procurmentPlanServiceImp.updateSubComponent(id);
		return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true), HttpStatus.OK);
	}

	@PutMapping("/updateSubComponent")
	public TeqipSubcomponentModel updateSubComponentMaster(@RequestBody TeqipSubcomponentModel subComponent) {
		return procurmentPlanServiceImp.saveSubcomponent(subComponent);
	}

	@GetMapping("/getAllCategory")
	public List<TeqipSubcategorymasterModel> getCategory() throws Exception {
		ArrayList<TeqipSubcategorymasterModel> resp = procurmentPlanServiceImp.getSubCategoryMasterDataById();
		System.out.println(resp.size());
		return resp;
	}

	@PostMapping("addInCategory")
	public Map<String, Object> addSubCategory(@RequestBody TeqipSubcategorymasterModel pojo) throws Exception {
		
		return procurmentPlanServiceImp.saveSubCategory(pojo);
	}

	@RequestMapping("getInstitutionType")
	public List<TeqipInstitutiontype> getInstitutionType(HttpServletRequest request) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList institutiontype = new ArrayList();
		List<TeqipInstitutiontype> lists = procurmentPlanServiceImp.getInstitutionType();
		for (TeqipInstitutiontype type : lists) {
			TeqipInstitutionTypeModel model = new TeqipInstitutionTypeModel();
			model.setInstitutiontypeId(type.getInstitutiontypeId());
			model.setInstitutiontypeName(type.getInstitutiontypeName());
			model.setIsactive(type.getIsactive());
			institutiontype.add(model);

		}

		return institutiontype;
	}

	@GetMapping("/getAllSupplierMaster")
	public Map<String, Object> getAllSupplier(@RequestParam(name = "name", required = false) String name,
			@RequestParam(name = "suppliername", required = false) String suppliername) {
		Map<String, Object> map = new HashMap();
		List<SupplierMaster> list = null;
		try {
			if (name.equalsIgnoreCase("supplier")) {
				list = procurmentPlanServiceImp.getAllSupp(suppliername);

			} else if (suppliername.equalsIgnoreCase("statedata")) {
				list = procurmentPlanServiceImp.getAllSuppState(name);
			}
			map.put("Success", list);
			return map;
		} catch (Exception e) {
			map.put("Failure", "data not available");
			return map;
		}
	}

	@GetMapping("/getAllSupplierForEdit/{id}")
	public Map<String, Object> getAllSupplierForEdit(@PathVariable("id") Integer id) {
		Map<String, Object> list = new HashMap();
		try {
			list = procurmentPlanServiceImp.getSupplierForEdit(id);
			// map.put("Success", list);
			return list;

		}

		catch (Exception e) {
			list.put("Failure", "data not available");
			return list;
		}

	}

	@GetMapping("/getAllSupplierName/{word}")
	public List getAllSupplierState(@PathVariable("word") String word) {
		return procurmentPlanServiceImp.getAllSupplierState(word);
	}

	@GetMapping("/getStateForSupplier")
	public List<Object[]> getStateForSupplier() {
		return procurmentPlanServiceImp.getStateForSupplier();
	}

	@GetMapping("/getAllStatename")
	public List<SupplierMaster> getAllSupplierName() {
		return procurmentPlanServiceImp.getAllSupplierName();
	}

	@PostMapping("/postSupplierMaster")
	public Map<String, Object> createSupplierMaster(@RequestBody SupplierMasterModel supplierMaster) {
		return procurmentPlanServiceImp.saveSupplier(supplierMaster);
	}

	@PutMapping("/putSupplierMaster")
	public Map<String, Object> updateSupplier(@RequestBody SupplierMasterModel supplierMaster) {
		return procurmentPlanServiceImp.updateSupplier(supplierMaster);

	}

	@DeleteMapping("/delSupplierMaster/{SupplierID}")
	public ResponseEntity<SupplierMaster> deleteSupplierMaster(@PathVariable(value = "SupplierID") Integer SupplierID) {
		return procurmentPlanServiceImp.desablesupplier(SupplierID);
	}

	@GetMapping("getItemMaster/{itemname}")
	public Map<String, Object> getItemMaster(HttpServletRequest request, @PathVariable("itemname") String itemname)
			throws Exception {
		return procurmentPlanServiceImp.findItemMasterByActiveStatus(itemname);
	}

	@GetMapping("getItemMasterbyCatId")
	public Map<String, Object> getItemMasterByCatId(@RequestParam("catId") Integer catId,
			@RequestParam("subcatid") Integer subcatid) {
		return procurmentPlanServiceImp.findItemMasterByCatId(catId, subcatid);

	}

	@GetMapping("getItemName")
	public List getItemName() {

		return procurmentPlanServiceImp.getItemName();
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/addItemMaster", method = RequestMethod.POST)
	public Map<String, Object> addItemDetails(HttpServletRequest request, @RequestBody TeqipItemMasterModel item) {
		return procurmentPlanServiceImp.addItemMaster(item);
	}

	@GetMapping("getSubCategoryByCatId/{id}")
	public ArrayList getSubCategory(@PathVariable("id") Integer catId) throws Exception {
		return procurmentPlanServiceImp.getSubCategoryMasterDataByCatId(catId);
	}

	@SuppressWarnings("unused")
	@PutMapping("updateItemMaster")
	public Map<String, Object> updateItemMaster(HttpServletRequest request, @RequestBody TeqipItemMasterModel item) {
		return procurmentPlanServiceImp.updateItem(item);
	}

	@GetMapping("getAllDepartmentMaster")
	public List<DepartmentMaster> getAllDepartmentMaster() {
		return (List<DepartmentMaster>) procurmentPlanServiceImp.findAlltDepartments();
	}

	@PostMapping("/addDepartment")
	public TeqipPmssDepartmentMaster createUser(@Valid @RequestBody TeqipDepartmentModel department) {
		// department.setIsactive(1);
		return procurmentPlanServiceImp.saveDept(department);
	}

	@GetMapping("getInstituteData")
	public Map<String, Object> getInstituteAllData(HttpServletRequest request) throws Exception {
		return procurmentPlanServiceImp.findinstituteAlldata(request);

	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/addInstitution", method = RequestMethod.POST)
	public Map<String, Object> addInstitution(HttpServletRequest request, @RequestBody TeqipInstitutionPojo institute)
			throws Exception {
		return procurmentPlanServiceImp.addInstitution(request, institute);
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/updateInstitution", method = RequestMethod.PUT)
	public Map<String, Object> updateInstitution(HttpServletRequest request,
			@RequestBody TeqipInstitutionPojo institute) throws Exception {

		return procurmentPlanServiceImp.updateInstitution(institute);
	}

	@GetMapping("getInstituteInfo")
	public Map<String, Object> getInstituteInfo(HttpServletRequest reques) throws Exception {
		return procurmentPlanServiceImp.getInstituteInfo();
	}

	@GetMapping("getInstituteByInstTypeId/{id}")
	public Map<String, Object> getInstituteByInstTypeId(@PathVariable("id") Integer id) throws Exception {
		return procurmentPlanServiceImp.getInstituteTypeById(id);
	}

	@GetMapping("getInstituteDataByStateId/{id}")
	public Map<String, Object> getInstituteDataByStateId(@PathVariable("id") Integer id) throws Exception {
		return procurmentPlanServiceImp.getInstituteDataByStateId(id);

	}

	@GetMapping("getInstByStateId/{id}")
	public Map<String, Object> getInstituteSDataByStateId(@PathVariable("id") Integer id) throws Exception {
		return procurmentPlanServiceImp.getInstitutesDataByStateId(id);

	}

	@GetMapping("getAllStateDataById/{id}")
	// @ExceptionHandler(Exception.class)
	public Map<String, Object> getAllStateDataById(@PathVariable("id") Integer id) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList al = new ArrayList();
		try {
			List<Object[]> objList = procurmentPlanDaoImp.getAllSubComponentByState(id);

			if (objList != null && objList.size() > 0) {
				StateAllData t1 = new StateAllData();

				for (Object[] ob : objList) {
					StateAllData t2 = t1.clone();
					t2.setIsactive((Byte) ob[0]);

					t2.setSubcomponentname((String) ob[1]);
					t2.setInstitutionSubcomponentMapping_ID((Integer) ob[2]);
					t2.setINSTITUTION_ID((Integer) ob[3]);

					t2.setStateId((Integer) ob[4]);
					// t2.setSubcomponentId((Integer) ob[3]);
					t2.setAllocatedbudget((Double) ob[5]);
					t2.setBudgetforcivilworks((Double) ob[6]);
					t2.setBudgetforservices((Double) ob[7]);
					t2.setBudgetforprocurment((Double) ob[8]);
					t2.setSUBCOMPONENTCODE((String) ob[9]);
					t2.setSubcomponentId((Integer) ob[10]);
					al.add(t2);
				}
			}
			map.put("Success", al);
			return map;
		} catch (Exception e) {
			map.put(PmssConstant.FALIURE_MESSAGE, e.getMessage());
			return map;
		}
	}

	@GetMapping("getAllBudgetByNpc")
	// @ExceptionHandler(Exception.class)
	public Map<String, Object> getAllBudgetByNpc() throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();

		try {
			List<StateAllData> al = procurmentPlanDaoImp.getAllNpcBudget().stream().map(ob -> {
				StateAllData t2 = new StateAllData();
				t2.setIsactive((Byte) ob[0]);
				t2.setSubcomponentname((String) ob[1]);
				t2.setInstitutionSubcomponentMapping_ID((Integer) ob[2]);
				t2.setINSTITUTION_ID((Integer) ob[3]);
				t2.setStateId((Integer) ob[4]);
				// t2.setSubcomponentId((Integer) ob[3]);
				t2.setAllocatedbudget((Double) ob[5]);
				t2.setBudgetforcivilworks((Double) ob[6]);
				t2.setBudgetforservices((Double) ob[7]);
				t2.setBudgetforprocurment((Double) ob[8]);
				t2.setSUBCOMPONENTCODE((String) ob[9]);
				t2.setSubcomponentId((Integer) ob[10]);
				return t2;
			}).collect(Collectors.toList());
			map.put("Success", al);
		}

		catch (Exception e) {

			map.put("Failure", e.getMessage());

		}
		return map;
	}

	@GetMapping("getAllBudgetByEdcil")
	public Map<String, Object> getAllBudgetByEdcil() throws CloneNotSupportedException {
		Map<String, Object> map = new LinkedHashMap<>();
		List<StateAllData> al;

		try {
			al = procurmentPlanDaoImp.getAllEdcilBudget().stream().map(ob -> new StateAllData(ob))
					.collect(Collectors.toList());
			if (al != null)
				map.put("Success", al);
			return map;
		} catch (Exception e) {

			map.put("Failure", e.getMessage());
			return map;
		}
	}

	@GetMapping("getPurchaseByStateDataById/{id}")
	// @ExceptionHandler(Exception.class)
	public Map<String, Object> getPurchaseByStateDataById(@PathVariable("id") Integer id) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList al = new ArrayList();
		try {

			List<TeqipPmssInstitutionpurchasecommittePojo> instPurchaseCommitte = procurmentPlanServiceImp
					.getStatePurchaseCommitteByStateId(id);
			if (instPurchaseCommitte != null) {
				map.put("Success", instPurchaseCommitte);

			}
		} catch (Exception e) {
			map.put(PmssConstant.FALIURE_MESSAGE, e.getMessage());

		}
		return map;
	}

	@GetMapping("getPurchaseForNpc")
	// @ExceptionHandler(Exception.class)
	public Map<String, Object> getPurchaseForNpc() throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList al = new ArrayList();
		try {

			List<TeqipPmssInstitutionpurchasecommittePojo> instPurchaseCommitte = procurmentPlanServiceImp
					.getPurchaseCommitteForNpc();
			if (instPurchaseCommitte != null) {
				map.put("Success", instPurchaseCommitte);

			}
		} catch (Exception e) {
			map.put(PmssConstant.FALIURE_MESSAGE, e.getMessage());

		}
		return map;
	}

	@GetMapping("getPurchaseForEdcil")
	// @ExceptionHandler(Exception.class)
	public Map<String, Object> getPurchaseForEdcil() throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList al = new ArrayList();
		try {

			List<TeqipPmssInstitutionpurchasecommittePojo> instPurchaseCommitte = procurmentPlanServiceImp
					.getPurchaseCommitteForEdcil();
			if (instPurchaseCommitte != null) {
				map.put("Success", instPurchaseCommitte);

			}
		} catch (Exception e) {
			map.put(PmssConstant.FALIURE_MESSAGE, e.getMessage());

		}
		return map;
	}

	@GetMapping("getDepartmentByStateId/{id}")
	public Map<String, Object> getDepartmentByStateId(@PathVariable("id") Integer id) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList al = new ArrayList();
		try {
			List<Object[]> objList = procurmentPlanDaoImp.getAllDepartmentByState(id);
			List<TeqipInstitutiondepartmentmappingModel> mylist = new ArrayList<>();
			TeqipInstitutiondepartmentmappingModel ob = new TeqipInstitutiondepartmentmappingModel();

			if (objList.size() > 0 && objList != null) {
				for (Object[] obj : objList) {
					TeqipInstitutiondepartmentmappingModel objDept = ob.clone();
					objDept.setIsactive((Byte) obj[0]);
					objDept.setInstitutionDepartmentMapping_ID((Integer) obj[1]);
					objDept.setDepartmentcode((String) obj[3]);
					objDept.setDepartmenthead((String) obj[2]);
					objDept.setDepartmentname((String) obj[4]);
					objDept.setDepartmentmasterid((Integer) obj[5]);
					objDept.setSpfuid((Integer) obj[6]);
					mylist.add(objDept);
				}

			}
			map.put("Success", mylist);
		} catch (Exception e) {
			map.put(PmssConstant.FALIURE_MESSAGE, e.getMessage());
		}
		return map;

	}

	@GetMapping("getDepartmentForNpc")
	public Map<String, Object> getDepartmentForNpc() throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList al = new ArrayList();
		try {
			List<Object[]> objList = procurmentPlanDaoImp.getAllDepartmentForNpc();
			List<TeqipInstitutiondepartmentmappingModel> mylist = new ArrayList<>();
			TeqipInstitutiondepartmentmappingModel ob = new TeqipInstitutiondepartmentmappingModel();

			if (objList.size() > 0 && objList != null) {
				for (Object[] obj : objList) {
					TeqipInstitutiondepartmentmappingModel objDept = ob.clone();
					objDept.setIsactive((Byte) obj[0]);
					objDept.setInstitutionDepartmentMapping_ID((Integer) obj[1]);
					objDept.setDepartmentcode((String) obj[3]);
					objDept.setDepartmenthead((String) obj[2]);
					objDept.setDepartmentname((String) obj[4]);
					objDept.setDepartmentmasterid((Integer) obj[5]);
					objDept.setSpfuid((Integer) obj[6]);
					mylist.add(objDept);
				}

			}
			map.put("Success", mylist);
		} catch (Exception e) {
			map.put(PmssConstant.FALIURE_MESSAGE, e.getMessage());
		}
		return map;

	}

	@GetMapping("getDepartmentForEdcil")
	public Map<String, Object> getDepartmentForEdcil() {
		Map<String, Object> map = new LinkedHashMap<>();

		try {
			List<TeqipInstitutiondepartmentmappingModel> mylist = procurmentPlanDaoImp.getAllDepartmentForEdcil()
					.stream().map(obj -> {
						TeqipInstitutiondepartmentmappingModel objDept = new TeqipInstitutiondepartmentmappingModel();
						objDept.setIsactive((Byte) obj[0]);
						objDept.setInstitutionDepartmentMapping_ID((Integer) obj[1]);
						objDept.setDepartmentcode((String) obj[3]);
						objDept.setDepartmenthead((String) obj[2]);
						objDept.setDepartmentname((String) obj[4]);
						objDept.setDepartmentmasterid((Integer) obj[5]);
						objDept.setSpfuid((Integer) obj[6]);
						return objDept;
					}).collect(Collectors.toList());

			map.put("Success", mylist);
		}

		catch (Exception e) {
			map.put(PmssConstant.FALIURE_MESSAGE, e.getMessage());
		}
		return map;

	}

	@GetMapping("getInstLogoByStateId/{id}")
	public Map<String, Object> getInstLogoByStateId(@PathVariable("id") Integer id) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList al = new ArrayList();
		try {

			List<TeqipPmssInstitutionlogo> logoList = procurmentPlanServiceImp.getInstituteLogoByStateId(id);
			if (logoList != null)
				map.put("Success", logoList);
		} catch (Exception e) {
			map.put(PmssConstant.FALIURE_MESSAGE, e.getMessage());
		}
		return map;
	}

	@GetMapping("getInstLogoForNpc/{id}")
	public Map<String, Object> getInstLogoForNpc(@PathVariable("id") Integer id) throws Exception {
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList al = new ArrayList();
		try {
			List<TeqipPmssInstitutionlogo> logoList = procurmentPlanServiceImp.getAllLogoForNpc(id);
			if (logoList != null)
				map.put("Success", logoList);
		} catch (Exception e) {
			e.printStackTrace();
			map.put(PmssConstant.FALIURE_MESSAGE, e.getMessage());
		}
		return map;
	}

	@GetMapping("getAllInstituteDataById/{id}")
	@ExceptionHandler(Exception.class)
	public Map<String, Object> getAllInstituteDataByInstituteId(@PathVariable("id") Integer id) throws Exception {
		Map<String, Object> map = new LinkedHashMap();
		Map<String, Object> deptMap = new LinkedHashMap<>();

		List<Object[]> objList = procurmentPlanDaoImp.getInstitutedpartByInstituteId(id);
		List<TeqipInstitutiondepartmentmappingModel> mylist = new ArrayList<>();
		if (objList.size() >= 0) {
			for (Object[] obj : objList) {
				/*
				 * deptMap.put("departmentcode", obj[0]);
				 * deptMap.put("departmenthead", obj[1]);
				 * deptMap.put("departmentname", obj[2]);
				 */
				TeqipInstitutiondepartmentmappingModel objDept = new TeqipInstitutiondepartmentmappingModel();
				objDept.setIsactive((Byte) obj[0]);
				objDept.setInstitutionDepartmentMapping_ID((Integer) obj[1]);
				objDept.setDepartmentcode((String) obj[3]);
				objDept.setDepartmenthead((String) obj[2]);
				objDept.setDepartmentname((String) obj[4]);
				objDept.setDepartmentmasterid((Integer) obj[5]);
				mylist.add(objDept);
			}

			List<TeqipPmssInstitutionlogo> logoList = procurmentPlanServiceImp.getInstituteLogoByInstId(id);
			List<TeqipInstitutionsubcomponentmappingPojo> subcomponent = procurmentPlanServiceImp
					.getTeqipInstitutionsubcomponentByInstId(id);
			// List<TeqipInstitutionsubcomponentmappingPojo> subComponentData =
			// (List<TeqipInstitutionsubcomponentmappingPojo>)
			// getTeqipInstitutionsubcomponentByInstId(id);
			List<TeqipPmssInstitutionpurchasecommittePojo> instPurchaseCommitte = procurmentPlanServiceImp
					.getInstitutionPurchaseCommitteByInstId(id);
			map.put("Result", "Success");
			map.put("instPurchaseCommitte", instPurchaseCommitte);
			map.put("subComponentData", subcomponent);
			map.put("departmentData", mylist);
			map.put("InstituteLogoData", logoList);
			map.put("Status", "OK");
			map.put("token", "ABC@123");
		} else {
			map.put("Status", "Fail");
			map.put("token", "ABC@123");
		}
		return map;
	}
	// @SuppressWarnings("unused")
	// @RequestMapping(value = "/addUser", method = RequestMethod.POST)
	// public Map<String, Object> addUser(HttpServletRequest request,
	// @RequestBody UserPojo pojo) throws Exception {
	// return procurmentPlanServiceImp.saveNewUser(pojo);
	// }

	@GetMapping("getUsers")
	public ResponseEntity<List<TeqipUsersDetail>> getAllProcurmentPlans() throws Exception {
		List<TeqipUsersDetail> list = procurmentPlanServiceImp.findAllUserData();
		return new ResponseEntity<List<TeqipUsersDetail>>(list, HttpStatus.OK);
	}

	@GetMapping("getRoles")
	public ResponseEntity<List<TeqipRolemaster>> getAllRoles() throws Exception {
		List<TeqipRolemaster> lists = procurmentPlanServiceImp.findAllRoles();
		ArrayList list = new ArrayList();
		for (TeqipRolemaster master : lists) {
			Rolemastermodel model = new Rolemastermodel();
			model.setPmssRole(master.getPmssRole());
			model.setRoleDescription(master.getRoleDescription());
			model.setRoleId(master.getRoleId());
			list.add(model);
		}
		return new ResponseEntity<List<TeqipRolemaster>>(list, HttpStatus.OK);
	}

	// return list of inst and state
	@GetMapping("getAllInstituteData")
	public Map<String, Object> getAllInstituteData(HttpServletRequest request) throws Exception {
		return procurmentPlanServiceImp.findAllinstandSt();
	}

	@PostMapping("addInstituteSubComponenet")
	public Map<String, Object> addInstituteSubComponenet(@RequestBody TeqipInstitutionsubcomponentmappingPojo pojo)
			throws Exception {
		return procurmentPlanServiceImp.addinstpurchasecommittee(pojo);
	}

	@PutMapping("updateInstituteSubComponenet")
	public Map<String, Object> updateInstituteSubComponenet(@RequestBody TeqipInstitutionsubcomponentmappingPojo pojo)
			throws Exception {
		return procurmentPlanServiceImp.updateInstSubcomponenet(pojo);
	}

	@DeleteMapping("/disableinsSubComponent/{institutionSubcomponentMapping_ID}")
	public ResponseEntity<TeqipInstitutionsubcomponentmapping> updateForDeleteDept(
			@PathVariable(value = "institutionSubcomponentMapping_ID") Integer institutionSubcomponentMapping_ID) {
		return procurmentPlanServiceImp.saveInsSubComponenet(institutionSubcomponentMapping_ID);

	}

	@PostMapping("addInstituteDept")
	public Map<String, Object> addInInstitutionDept(@RequestBody TeqipInstitutiondepartmentmappingModel pojo)
			throws Exception {
		return procurmentPlanServiceImp.addnewinstdept(pojo);
	}

	@PutMapping("updateDept")
	public Map<String, Object> updateDeptMapping(@Valid @RequestBody TeqipInstitutiondepartmentmappingModel pojo)
			throws Exception {
		return procurmentPlanServiceImp.updateInstDept(pojo);

	}

	@DeleteMapping("/disableDept/{deptId}")
	public ResponseEntity<TeqipInstitutiondepartmentmapping> disableForDept(
			@PathVariable(value = "deptId") Integer deptId) {
		return procurmentPlanServiceImp.desabledept(deptId);

	}

	@PostMapping("addInstitutelogo")
	public Map<String, Object> addInInstitutionLogo(@Valid @RequestBody InstitutionlogoPojo pojo) throws Exception {
		return procurmentPlanServiceImp.addinstLogo(pojo);
	}

	@PutMapping("updateInstitutelogo")
	public Map<String, Object> updateInInstitutionLogo(@Valid @RequestBody InstitutionlogoPojo pojo) throws Exception {
		return procurmentPlanServiceImp.updateLogo(pojo);
	}

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public Map<String, Object> uploadFile(@RequestParam("file") MultipartFile file,
			@RequestParam(name = "spfuid", required = false) Integer spfuid,
			@RequestParam("Description") String Description,
			@RequestParam(name = "instituteid", required = false) Integer instituteid,
			@RequestParam(name = "typeNpcEdcil", required = false) Integer typeNpcEdcil) throws IOException {
		
	try{
		return procurmentPlanServiceImp.savelogoimg(file, instituteid, Description, spfuid, typeNpcEdcil);
		/*	
		*/
	}
	catch(Exception e){
		Map<String,Object> map=new HashMap();
		map.put("Filure",e.getMessage());
		return map;
	}
	}

	@RequestMapping(value = "/updateFile", method = RequestMethod.PUT, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public Map<String, Object> updateUploadFile(@RequestParam("file") MultipartFile file,
			@RequestParam("Description") String Description,
			@RequestParam(name = "spfuid", required = false) Integer spfuid,
			@RequestParam(name = "instituteid", required = false) Integer instituteid,
			@RequestParam(name = "typeNpcEdcil", required = false) Integer typeNpcEdcil,
			@RequestParam("institutionLogoID") Integer institutionLogoID) throws IOException {
		return procurmentPlanServiceImp.updatelogo(file, institutionLogoID, Description,spfuid,instituteid,typeNpcEdcil);
		/*	
		*/
	}

	@DeleteMapping("disableinstituteLogo/{institutionLogoID}")
	public ResponseEntity<TeqipPmssInstitutionlogo> deleteInstitutionLogo(
			@PathVariable(value = "institutionLogoID") Integer institutionLogoID) {
		return procurmentPlanServiceImp.desableinsLogoById(institutionLogoID);

	}

	@PostMapping("/addpurchagecommittee")
	public Map<String, Object> addPurchage(@Valid @RequestBody TeqipPmssInstitutionpurchasecommittePojo pojo) {
		return procurmentPlanServiceImp.addinstpurchasecommittee(pojo);
	}

	@PutMapping("updatepurchase")
	public Map<String, Object> updatePurchaseCommittee(
			@Valid @RequestBody TeqipPmssInstitutionpurchasecommittePojo pojo) throws Exception {
		return procurmentPlanServiceImp.updatepurchasecommittee(pojo);

	}

	@DeleteMapping("disablePurchaseCommitee/{institutionPurchaseCommitte_ID}")
	public ResponseEntity<TeqipPmssInstitutionpurchasecommitte> deletePurchseCommitee(
			@PathVariable(value = "institutionPurchaseCommitte_ID") Integer institutionPurchaseCommitte_ID) {
		return procurmentPlanServiceImp.desablepurchasecommitee(institutionPurchaseCommitte_ID);

	}

	@GetMapping("getAllSubComponents")
	List<TeqipSubcomponentmasterModel> getAllSubComponents() {
		return (List<TeqipSubcomponentmasterModel>) procurmentPlanServiceImp.findAllsubcomponent();

	}

	@RequestMapping(value = "/getLoginDetails", method = RequestMethod.POST)
	public Map<String, Object> Details(HttpServletRequest request, @RequestBody UserLoginDetails objList)
			throws Exception {
		Map<String, Object> mapObj = new LinkedHashMap();
		String name = objList.getName();
		String userName = objList.getUserName();
		Integer roleid = objList.getRoleid();
		String institutionName = objList.getInstitutionName();
		String state = objList.getState();
		LoginUser claim = (LoginUser) request.getAttribute("claim");
		String instiutename = claim.getInstname();
		String statename = claim.getStatname();
		StringBuilder sb = new StringBuilder(
				"select UD.user_name,UD.ROLE_ID,RM.ROLE_DESCRIPTION, UM.INSTITUTION_ID, UM.SPFUID, IM.INSTITUTION_NAME,SM.STATE_NAME,UD.ISACTIVE,UD.NAME from teqip_users_detail UD INNER JOIN teqip_rolemaster RM ON UD.ROLE_ID = RM.ROLE_ID INNER JOIN teqip_users_master UM ON UD.USERID = UM.USER_ID RIGHT JOIN teqip_institutions IM ON IM.INSTITUTION_ID = UM.INSTITUTION_ID RIGHT JOIN teqip_statemaster SM ON SM.STATE_ID = UM.SPFUID WHERE UD.ISACTIVE = 1");
		StringBuilder sb1 = null;
		if (claim.getPmssrole().equalsIgnoreCase("INST") || claim.getPmssrole().equalsIgnoreCase("INST_DIR"))// claim.getRoleId()==5
																												// ||
																												// claim.getRoleId()==6
																												// ||
																												// claim.getRoleId()==9)
		{
			sb1 = new StringBuilder(
					"SELECT ud.user_name,ud.ROLE_ID,role.ROLE_DESCRIPTION,um.INSTITUTION_ID,um.SPFUID,ins.INSTITUTION_NAME,state.STATE_NAME,ud.ISACTIVE,ud.NAME FROM teqip_users_master um,teqip_users_detail ud,teqip_rolemaster role,teqip_statemaster state,teqip_institutions ins  where  um.USER_ID=ud.USERID and um.INSTITUTION_ID=ins.INSTITUTION_ID and um.SPFUID=state.STATE_ID  and um.ROLE_ID=role.ROLE_ID and um.INSTITUTION_ID in(select INSTITUTION_ID from teqip_institutions where  INSTITUTION_NAME= '"
							+ instiutename + "' )");
		} else if (claim.getPmssrole().equalsIgnoreCase("SPFU"))// claim.getRoleId()==4
																// ||
																// claim.getRoleId()==7
																// ||
																// claim.getRoleId()==8)
		{
			sb1 = new StringBuilder(
					"SELECT ud.user_name,ud.ROLE_ID,role.ROLE_DESCRIPTION,um.INSTITUTION_ID,um.SPFUID,ins.INSTITUTION_NAME,state.STATE_NAME,ud.ISACTIVE,ud.NAME FROM teqip_users_master um,teqip_users_detail ud,teqip_rolemaster role,teqip_statemaster state,teqip_institutions ins  where  um.USER_ID=ud.USERID and um.INSTITUTION_ID=ins.INSTITUTION_ID and um.SPFUID=state.STATE_ID  and um.ROLE_ID=role.ROLE_ID and um.SPFUID in(select STATE_ID from teqip_statemaster where  STATE_NAME= '"
							+ statename + "')");

		}
		try {
			if (null != userName & !userName.isEmpty()) {
				sb = sb.append(" and " + "UD.user_name=:userName");
				if (sb1 != null)
					sb1 = sb1.append(" and " + "ud.user_name=:userName");
			}
		} catch (Exception e) {
			// System.out.println("userName is null");
			// e.printStackTrace();
		}
		try {
			if (null != name & !name.isEmpty()) {
				sb = sb.append(" and " + "UD.NAME=:name");
			}
		} catch (Exception e) {
			// System.out.println(" name is null");
		}

		if (null != roleid) {
			sb = sb.append(" and " + "UD.ROLE_ID=:roleid");
		}
		try {
			if (null != institutionName & !institutionName.isEmpty()) {
				sb = sb.append(" and " + "IM.INSTITUTION_NAME=:institutionName");
				if (sb1 != null)
					sb1 = sb1.append("  and " + "ins.INSTITUTION_NAME=:institutionName");
			}
		} catch (Exception e) {
			// System.out.println("institute null");
		}
		try {
			if (null != state & !state.isEmpty()) {
				sb = sb.append(" and " + "SM.STATE_NAME=:state");
			}
		} catch (Exception e) {
			// System.out.println("state is null");
		}
		String str = null;
		if (!claim.getPmssrole().equalsIgnoreCase("Inst") && !claim.getPmssrole().equalsIgnoreCase("INST_DIR")
				&& !claim.getPmssrole().equalsIgnoreCase("SPFU")) {// claim.getRoleId()!=4
																	// &&
																	// claim.getRoleId()!=7
																	// &&
																	// claim.getRoleId()!=8){
			str = sb.toString();
		} else if (sb1 != null) {
			str = sb1.toString();
		}
		// System.out.println(str);
		;
		List<Object[]> list = null;
		if (str != null)
			list = procurmentPlanDaoImp.getByString(str, userName, roleid, institutionName, state, name);
		ArrayList l = new ArrayList();
		Iterator it = null;
		if (list != null)
			it = list.iterator();
		if (it != null) {
			while (it.hasNext()) {
				Object[] obj = (Object[]) it.next();
				UserLoginDetails objlist = new UserLoginDetails();
				// objlist.setId((Integer) obj[0]);
				objlist.setUserName((String) obj[0]);
				// objlist.setIsactive((Boolean) obj[1]);
				objlist.setRole((String) obj[2]);
				objlist.setState((String) obj[6]);
				objlist.setInstitutionName((String) obj[5]);
				objlist.setIsactive((Boolean) obj[7]);
				objlist.setName((String) obj[8]);
				l.add(objlist);
			}
		}
		mapObj.put("Status", "Success");
		mapObj.put("Data", l);

		return mapObj;

	}

	@GetMapping("getAllINpiu")
	List<NpiuDetailsMaster> getAllNpiuDetails() {
		return (List<NpiuDetailsMaster>) procurmentPlanServiceImp.findNpiu();

	}

	@GetMapping("getnpiuMaster/{id}")
	public Map<String, Object> getNpiuByCode(@PathVariable(value = "id", required = true) Integer id) throws Exception {
		return procurmentPlanServiceImp.findNpiuById(id);
	}

	@GetMapping("getnpiuMasterById/{id}")
	public Map<String, Object> getNpiuByCodeId(@PathVariable(value = "id", required = true) Integer id) {
		return procurmentPlanServiceImp.getnpiupurchasecommittee(id);
	}

	@GetMapping("getRolemaster")
	public ResponseEntity<List<TeqipRolemaster>> getAllRolemaster() throws Exception {
		List<TeqipRolemaster> list = procurmentPlanServiceImp.findAllRolesmaster();
		return new ResponseEntity<List<TeqipRolemaster>>(list, HttpStatus.OK);
	}

	@GetMapping("getAllRolemaster")
	public List<TeqipRolemaster> getAllrolemaster() throws Exception {
		List<TeqipRolemaster> list = procurmentPlanServiceImp.findAll();
		return list;
	}

	@GetMapping("/getStById")
	public Map<String, Object> getAllSt(HttpServletRequest req) {
		return procurmentPlanServiceImp.getStateById(req);

	}

	@GetMapping("/getstatesById/{id}")
	public Map<String, Object> getStateById(@PathVariable(value = ("id")) Integer id) throws Exception {
		return procurmentPlanServiceImp.findStateMasterById(id);
	}

	@GetMapping("getpurchaseCommitteByStId/{id}")

	public Map<String, Object> getpuchaseCommitteId(@PathVariable("id") Long id) throws Exception {

		return procurmentPlanServiceImp.getpurchasecommitteeByid(id);
	}

	@GetMapping("getProcurementmethod")
	public Map<String, Object> getPrecurementMethod() throws Exception {
		return procurmentPlanServiceImp.getAll();
	}

	@PostMapping("/poststate")
	public Map<String, Object> createState(@Valid @RequestBody TeqipStatemastermodel statemaster) {
		return procurmentPlanServiceImp.saveSatemaster(statemaster);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/putState")
	public Map<String, Object> updateState(@RequestBody TeqipStatemastermodel statemaster) {
		return procurmentPlanServiceImp.updateState(statemaster);
	}

	@GetMapping("getProcurementmethodDeatils/{procurementId}")
	public Map<String, Object> getPrecurementMethodDetailsById(
			@PathVariable(value = "procurementId", required = true) Integer procurementId) throws Exception {
		return procurmentPlanServiceImp.findprocurmentById(procurementId);
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/addProcurementmethod", method = RequestMethod.POST)
	public Map<String, Object> addPrecurementMethod(HttpServletRequest request,
			@RequestBody PrecurementMethodTimeLinePojo procure) throws Exception {
		return procurmentPlanServiceImp.addNewprocurement(procure);
	}

	@GetMapping("getProcurementmethodDeatilsWithPrior/{procurementId}")
	public Map<String, Object> getPrecurementMethodDetailsWithPriorById(
			@PathVariable(value = "procurementId", required = true) Integer procurementId) throws Exception {
		return procurmentPlanServiceImp.getprocurementwithPrior(procurementId);
	}

	@GetMapping("getProcurementmethodDeatilsWithoutPrior/{procurementId}")
	public Map<String, Object> getPrecurementMethodDetailsWithoutPriorById(
			@PathVariable(value = "procurementId", required = true) Integer procurementId) throws Exception {
		return procurmentPlanServiceImp.getprocurementwithoughtPrior(procurementId);
	}

	@GetMapping("getThreshholdsMasterData")
	public Map<String, Object> getThreshholdsMasterData(HttpServletRequest request) throws Exception {
		return procurmentPlanServiceImp.findthreshold();
	}

	@GetMapping("getProcurementMethodsByCatId/{id}")
	public Map<String, Object> getProcurementMethodsByCatId(@PathVariable("id") Long catId) throws Exception {
		return procurmentPlanServiceImp.findprocurementBycatId(catId);
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "saveThreshholdsMaster", method = RequestMethod.POST)
	public Map<String, Object> saveThreshholdsMaster(HttpServletRequest request,
			@RequestBody TeqipPmssThreshholdmodel thres) throws Exception {
		return procurmentPlanServiceImp.addthreshold(thres);
	}

	@SuppressWarnings("unused")
	@PutMapping("updateThreshholdsMaster")
	public Map<String, Object> updateThreshholdsMaster(HttpServletRequest request,
			@RequestBody TeqipPmssThreshholdmodel pojo) throws Exception {
		return procurmentPlanServiceImp.updateThreshold(request, pojo);
	}

	@PostMapping("/addDepartmentMaster")
	public Map<String, Object> addDepartment(HttpServletRequest req, @RequestBody TeqipDepartmentModel department) {
		// department.setIsactive(1);
		return procurmentPlanServiceImp.saveDepartment(req, department);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/updateDepartmentMaster")
	public Map<String, Object> updateUser(HttpServletRequest req, @RequestBody TeqipDepartmentModel updatedepartment) {

		return procurmentPlanServiceImp.updateDepartmentMaster(req, updatedepartment);
	}

	@DeleteMapping("disableDeptmnt/{deptId}")
	public ResponseEntity<DepartmentMaster> deleteDepartment(@PathVariable(value = "deptId") Integer deptId) {
		return procurmentPlanServiceImp.desabledepartmentt(deptId);
	}

	@GetMapping("/userinfo/{username}")
	public Map<String, Object> getUserInfo(@PathVariable("username") String username) {
		return procurmentPlanServiceImp.UserInfoByUsername(username);
	}

	@GetMapping(value = "/download/{institutelogoid}")
	public ResponseEntity<Resource> downloadFile(HttpServletResponse response,
			@PathVariable(value = "institutelogoid") Integer institutelogoid, HttpServletRequest req)
			throws IOException {

		TeqipPmssInstitutionlogo entity = teqipPmssInstitutionlogoRepository.findOne(institutelogoid);

		OutputStream outputStream = response.getOutputStream();

		if (entity.getInstitutionLogoID() != institutelogoid) {
			String errorMessage = "Sorry. Invalid URL requested";
			outputStream.write(errorMessage.getBytes(Charset.defaultCharset()));
			outputStream.close();

		}
		Resource file = getFileLocations(entity.getSystemFileName());
		if (file != null) {

			String contentType = req.getServletContext().getMimeType(file.getFile().getAbsolutePath());
			if (contentType == null) {
				contentType = "application/octet-stream";
			}
			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
					.body(file);

		} else {
			throw new PMSSBudgetException("Sorry. The file you are looking for does not exist.",
					new ErrorCode("Image_NOT_FOUND", "Sorry. The file you are looking for does not exist."));

		}
	}

	@GetMapping("/getcity/{stid}")
	public Map<String, Object> getCity(@PathVariable("stid") Integer stid) {

		return procurmentPlanServiceImp.findCityBystId(stid);
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public Map<String, Object> addUser(@RequestBody UserPojo pojo) throws Exception {
		Map<String, Object> mapObj = procurmentPlanServiceImp.saveNewUser(pojo);

		return mapObj;
	}

	public Resource getFileLocations(String systemfileName) throws IOException {

		String path = MethodConstants.UPLOAD_LOCATION
				+ (systemfileName != null && systemfileName.contains("Attachments") ? "" : File.separator + "logo");
		Path rootLocation = Paths.get(path);

		Path filePath = rootLocation.resolve(systemfileName);
		org.springframework.core.io.Resource resource = new UrlResource(filePath.toUri());
		return resource;
	}

	@RequestMapping(value = "/addInstitutionType", method = RequestMethod.POST)
	public Map<String, Object> addInstitutionType(HttpServletRequest request,
			@RequestBody TeqipInstitutionTypeModel institutionType) throws Exception {
		return procurmentPlanServiceImp.saveInstitutionType(institutionType);
	}

	@PutMapping("updateInstitutionType")
	public Map<String, Object> updateInstitutionTypeById(@RequestBody TeqipInstitutionTypeModel institutionType)
			throws Exception {
		return procurmentPlanServiceImp.updateInstitutionType(institutionType);
	}

	@RequestMapping(value = "/addNpiuDetails", method = RequestMethod.POST)
	public Map<String, Object> addNpiuDetailsmaster(HttpServletRequest request,
			@RequestBody NpiuDetailsMasterPojo npiuDetails) throws Exception {
		return procurmentPlanServiceImp.addnpiu(npiuDetails);
	}

	@RequestMapping(value = "/editNpiuDetails", method = RequestMethod.POST)
	public Map<String, Object> editNpiuDetailsmaster(HttpServletRequest request,
			@RequestBody NpiuDetailsMasterPojo npiuDetails) throws Exception {
		return procurmentPlanServiceImp.editnpiu(npiuDetails);
	}

}
