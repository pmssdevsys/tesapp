package com.qspear.procurement.rest;

import static com.qspear.procurement.security.constant.SecurityConstants.HEADER_STRING;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qspear.procurement.constants.PmssConstant;
import com.qspear.procurement.model.LoginModel;
import com.qspear.procurement.persistence.TrnAuditTrial;
import com.qspear.procurement.persistence.User;
import com.qspear.procurement.persistence.UserAttempts;
import com.qspear.procurement.repositories.AuditRepository;
import com.qspear.procurement.repositories.UserAttemptRepository;
import com.qspear.procurement.security.util.AesUtil;
import com.qspear.procurement.security.util.TokenGenerator;
import com.qspear.procurement.service.UserControllerService;

@CrossOrigin
@RequestMapping("pmss")
@RestController
public class UserController {

	@Autowired
	private UserControllerService userControllerServiceImpl;
	@Autowired
	UserAttemptRepository userAttemptRepository;
	@Autowired
	AuditRepository auditRepository;

	@SuppressWarnings("unused")
	@PostMapping("/login")
	ResponseEntity<LoginModel> login(@RequestHeader(value = "token", required = true) String token,
			@RequestBody(required = true) User user, HttpServletRequest request) {
		String jwttoken = null;

		if (null != token && !token.isEmpty()) {
			try {
				if (null != user && null != user.getUserName() && !user.getUserName().isEmpty()
						&& null != user.getPassword() && !user.getPassword().isEmpty()) {
					User loginUser = userControllerServiceImpl.findByUserName(user.getUserName());
					if (loginUser == null) {
						return new ResponseEntity<LoginModel>(
								new LoginModel(PmssConstant.FAIL, "please enter valid user name", null, null), null,
								HttpStatus.OK);
					}
					// String pawordDecrypt =
					// Base64Encoder.decodePassword(user.getPassword());
					String pawordDecrypts = AesUtil.decrypt(user.getPassword());
					String dbDecryptpwd = AesUtil.decrypt(loginUser.getPassword());
					if (null != loginUser && dbDecryptpwd.equals(pawordDecrypts)) {
						if (loginUser.getIsBlocked()) {
							userControllerServiceImpl.isBlocked(loginUser);

						}
						jwttoken = TokenGenerator.token(loginUser);
						HttpHeaders responseHeaders = new HttpHeaders();
						responseHeaders.add(HEADER_STRING, jwttoken);
						// LoginModel loginModel = new LoginModel(status,
						// loginMessage, jwttoken, data);
						// loginModel.setToken(jwttoken);
						// loginModel.setLoginMessage("Login Sucessfully");
						UserAttempts userAttempts = userAttemptRepository.findByUsername(loginUser.getUserName());
						userAttempts = userAttempts != null ? userAttempts : new UserAttempts();
						userAttempts.setUsername(loginUser.getUserName());
						userAttempts.setAttempts(0);
						userAttempts.setUpdateTime(new Date());

						///////////////////////////////
						TrnAuditTrial audit = new TrnAuditTrial();
						audit.setDescription("User Successfully login");
						audit.setIpAddress(request.getRemoteAddr());
						audit.setSessionId(jwttoken);
						audit.setUpdateTime(new Date());
						audit.setUrl(request.getRequestURL().toString());
						audit.setUserAgent(request.getHeader("User-Agent"));
						audit.setUsername(user.getUserName());
						auditRepository.save(audit);
						userAttemptRepository.save(userAttempts);
						return new ResponseEntity<LoginModel>(
								new LoginModel(PmssConstant.SUCCUESS, "Login Sucessfully", jwttoken, null),
								responseHeaders, HttpStatus.OK);
					} else {
						if (loginUser == null) {
							return new ResponseEntity<LoginModel>(new LoginModel(PmssConstant.FAIL,
									"please enter valid user name and password", null, null), null, HttpStatus.OK);
						}
						userControllerServiceImpl.attempt(loginUser);
						return new ResponseEntity<LoginModel>(new LoginModel(PmssConstant.FAIL,
								"please enter valid user name and password", null, null), null, HttpStatus.OK);
					}
				}
			} catch (Exception ex) {
				TrnAuditTrial audit = new TrnAuditTrial();
				audit.setDescription("Error: " + ex.getMessage());
				audit.setIpAddress(request.getRemoteAddr());
				// audit.setSessionId(loginModel.getToken());
				audit.setUpdateTime(new Date());
				audit.setUrl(request.getRequestURL().toString());
				audit.setUserAgent(request.getHeader("User-Agent"));
				audit.setUsername(user.getUserName());
				auditRepository.save(audit);
				return new ResponseEntity<LoginModel>(new LoginModel(PmssConstant.FAIL, ex.getMessage(), null, null),
						null, HttpStatus.OK);
			}

		} else {
			return new ResponseEntity(HttpStatus.PROXY_AUTHENTICATION_REQUIRED);
		}
		return new ResponseEntity(HttpStatus.PROXY_AUTHENTICATION_REQUIRED);
	}
	@GetMapping("logout")
	public Map<String, Object> logout( HttpServletRequest request) {

		Map<String, Object> map = new HashMap();
		String token = request.getHeader("Authorization");
		TrnAuditTrial trn = auditRepository.expireToken(token);
		trn.setLogoutTime(new Date());
		trn.setLogoutstatus(1);
		auditRepository.save(trn);

		map.put("logout ", trn.getSessionId());
		return map;
	}
}
