package com.qspear.procurement.rest;

import com.qspear.procurement.letter.GenerateLetterFactory;
import com.qspear.procurement.model.TeqipSuccess;
import com.qspear.procurement.model.TeqipSupplierVerify;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.SupplierVerify;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.repositories.method.PackageDocumentRepository;
import com.qspear.procurement.service.MethodService;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

/**
 * Created by e1002703 on 4/3/2018.
 */
@CrossOrigin
@RestController
@RequestMapping("/pmss/methods")
public class MethodsController {

    @Autowired
    MethodService methodService;

    @Autowired
    GenerateLetterFactory generateLetterFactory;

    @Autowired
    PackageDocumentRepository documentRepository;

    @GetMapping("/package/{id}")
    public ResponseEntity<MethodPackageResponse> getPackageDetails(@PathVariable(value = "id") int packageId) {

        return methodService.getPackageDetails(packageId);
    }

    @PostMapping("/package/{id}")
    public ResponseEntity<MethodPackageResponse> update(@PathVariable(value = "id") int packageId,
            @RequestBody MethodPackageUpdateRequest methodPackageUpdateRequest) {

        return methodService.updatePackageComponents(packageId, methodPackageUpdateRequest);
    }
    
    
    @PostMapping("/verify/supplier")
    public ResponseEntity<TeqipSupplierVerify> verifySupplier(@RequestBody SupplierVerify TeqipSuccess) {

        return new ResponseEntity<TeqipSupplierVerify>(methodService.verifySupplier(TeqipSuccess),
                HttpStatus.OK);
    }

    @PostMapping(value = "/generate/{type}/{packageId}")
    ResponseEntity<TeqipSuccess> generateLetter(@PathVariable(value = "type") String type,
            @PathVariable(value = "packageId") Integer packageId,
            HttpServletRequest request) throws IOException, InvalidFormatException {

        generateLetterFactory.generate(packageId, type, request.getParameterMap());
        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);

    }

    @GetMapping(value = "/download/{packageId}/{documentId}")
    public void downloadFile(HttpServletResponse response,
            @PathVariable(value = "packageId") int packageId,
            @PathVariable(value = "documentId") int documentId) throws IOException {

        TeqipPackageDocument entity = documentRepository.findOne(documentId);

        OutputStream outputStream = response.getOutputStream();

        if (entity.getTeqipPackage().getPackageId() != packageId) {
            String errorMessage = "Sorry. Invalid URL requested";
            outputStream.write(errorMessage.getBytes(Charset.defaultCharset()));
            outputStream.close();
            return;
        }

        File file = entity.getFileLocation();
        
        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        if (mimeType == null) {
            System.out.println("mimetype is not detectable, will take default");
            mimeType = "application/octet-stream";
        }

        System.out.println("mimetype : " + mimeType);

        response.setContentType(mimeType);

        /* "Content-Disposition : inline" will show viewable types [like images/text/pdf/anything viewable by browser] right on browser 
            while others(zip e.g) will be directly downloaded [may provide save as popup, based on your browser setting.]*/
        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + entity.getOriginalFileName() + "\""));

        /* "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your browser setting*/
        //response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
        response.setContentLength((int) file.length());

        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

        //Copy bytes from source to destination(outputstream in this example), closes both streams.
        FileCopyUtils.copy(inputStream, outputStream);
    }

}
