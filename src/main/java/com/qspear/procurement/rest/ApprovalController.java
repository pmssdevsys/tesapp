 package com.qspear.procurement.rest;

import com.qspear.procurement.model.InstituteWiseApproval;
import com.qspear.procurement.model.StateWiseApproval;
import com.qspear.procurement.model.TeqipSuccess;
import com.qspear.procurement.service.ApprovalService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by e1002703 on 4/3/2018.
 */
@CrossOrigin
@RestController
@RequestMapping("/pmss")
public class ApprovalController {

    @Autowired
    private ApprovalService approvalService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    /**
     *
     *
     * @param planId
     * @param id InstituteID | StateID |NationalID
     * @param roleType INST|SPFU|NPIU
     * @param comment
     * @param stage
     * @return
     */
    @PostMapping({"/approval/procurementStage/{planId}/{stage}","/procurementStage/{planId}/{stage}"})
    ResponseEntity<TeqipSuccess> packageStage(@PathVariable int planId,
            @RequestParam Integer id,
            @RequestParam String roleType,
            @RequestParam String comment,
            @PathVariable String stage) {

        String submitStatus = approvalService.changePackageStatus(planId, id, roleType, stage, comment, httpServletRequest);

        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess(submitStatus, true),
                HttpStatus.OK);
    }

    /**
     * 
     * @param type revision|approval
     * @return 
     */
    @GetMapping("/approval/state/all")
    ResponseEntity<List<StateWiseApproval>> stateWiseApprovalOrRevisionPackage() {

        return new ResponseEntity<List<StateWiseApproval>>(
                approvalService.stateWiseApprovalOrRevisionPackage( httpServletRequest),
                HttpStatus.OK);
    }

    @GetMapping("/approval/institute/{stateId}")
    ResponseEntity<List<InstituteWiseApproval>> instituteWiseApproval(@PathVariable Integer stateId) {

        if(stateId == null){
            stateId= 0;
        }
        return new ResponseEntity<List<InstituteWiseApproval>>(
                approvalService.instituteWiseApprovalOrRevisionPackage(stateId, httpServletRequest),
                HttpStatus.OK);
    }
    @GetMapping("/approval/institute/{stateId}/all")
    ResponseEntity<List<InstituteWiseApproval>> instituteWiseNotInApproval(@PathVariable int stateId) {

        return new ResponseEntity<List<InstituteWiseApproval>>(
                approvalService.instituteWiseNotInApprovalOrRevisionPackage(stateId, httpServletRequest),
                HttpStatus.OK);
    }
    

}
