package com.qspear.procurement.rest;

import com.qspear.procurement.model.FileObect;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSBudgetException;
import com.qspear.procurement.model.TeqipSuccess;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.Map;
import com.qspear.procurement.model.Mail;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.codec.CharEncoding;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@CrossOrigin
@RequestMapping("pmss/sendMail")
public class MailController {

    @Autowired
    private JavaMailSender sender;

    @Autowired
    private Configuration freemarkerConfig;

    @Autowired
    protected TeqipPackageRepository teqipPackageRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    @PostMapping()
    ResponseEntity<TeqipSuccess> sendMail(@RequestBody Mail mail) throws Exception {

        sendEmail(mail);

        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    @RequestMapping("/invitationLetter/{packageId}")
    ResponseEntity<TeqipSuccess> sendInvitaionLetter(@PathVariable int packageId,
            @RequestBody Mail mail) throws Exception {

        TeqipPackage teqipPackage = this.teqipPackageRepository.findOne(packageId);
        if (teqipPackage == null) {
            throw new PMSSBudgetException("Package is not there.",
                    new ErrorCode("PACKAGE_NOT_FOUND", "Package issue"));

        }

        TeqipPmssSupplierMaster teqipPmssSupplierMaster = this.supplierRepository.findOne(mail.getSupplierId());

        TeqipPackageSupplierDetail packageSupplierDetail
                = packageSupplierRepository.findByTeqipPackageAndTeqipPmssSupplierMaster(teqipPackage, teqipPmssSupplierMaster);

        if (teqipPmssSupplierMaster == null || packageSupplierDetail == null) {
            throw new PMSSBudgetException("Supplier is not there.",
                    new ErrorCode("SUPPLIER_NOT_FOUND", "Supplier issue"));

        }

        TeqipPackageDocument invitationDocument = packageSupplierDetail.getInvitationDocument();

        if (invitationDocument == null) {
            throw new PMSSBudgetException("Invitation Letter is not generated for the supplier.",
                    new ErrorCode("INV_LETTER_NOT_FOUND", "Invitation Letter issue"));

        }

        FileObect[] fileobj = new FileObect[1];
        fileobj[0] = new FileObect(invitationDocument.getOriginalFileName(), invitationDocument.getFileLocation());
        mail.setFileobj(fileobj);

        sendEmail(mail);

        packageSupplierDetail.setIsEmailSent(1);
        packageSupplierRepository.saveAndFlush(packageSupplierDetail);

        return new ResponseEntity<TeqipSuccess>(new TeqipSuccess("Success", true),
                HttpStatus.OK);
    }

    private String templateToBody(String template, Map<String, String> model) throws Exception {
        freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/");
        Template t = freemarkerConfig.getTemplate(template);
        return FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

    }

    private void sendEmail(Mail mail) throws Exception {
        MimeMessage message = sender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true, CharEncoding.UTF_8);

        helper.setTo(mail.getTo());

        if (mail.getFrom() != null) {
            helper.setFrom(mail.getFrom());
        }
        
        if (mail.getCc() != null) {
            helper.setCc(mail.getCc());
        }
        if (mail.getBcc() != null) {

            helper.setBcc(mail.getBcc());
        }
        if(mail.getReplyTo()!=null){
        	helper.setReplyTo(mail.getFrom());
        }
        helper.setSubject(mail.getSubject());

        helper.setText(mail.getBody(), true); // set to html

        if (mail.getFileobj() != null) {
            for (FileObect fileObect : mail.getFileobj()) {
                helper.addAttachment(fileObect.getFileName(), fileObect.getFile());
            }
        }
        sender.send(message);
    }

}
