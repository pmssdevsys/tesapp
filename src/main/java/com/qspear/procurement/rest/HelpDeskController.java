package com.qspear.procurement.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.jni.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.qspear.procurement.constants.PmssConstant;
import com.qspear.procurement.exception.JsonResponse;
import com.qspear.procurement.model.TeqipHelpdeskTicketModel;
import com.qspear.procurement.persistence.HelpDeskTicketDetails;
import com.qspear.procurement.persistence.Helpdesk_ticket;
import com.qspear.procurement.persistence.Helpdesk_tickethistory;
import com.qspear.procurement.service.ProcurmentPlanServiceImp;
import com.qspear.procurement.util.FileUtils;

@CrossOrigin
@RestController
@RequestMapping("/pmss/helpdesk")
public class HelpDeskController {

	@Autowired
	ProcurmentPlanServiceImp procurmentPlanServiceImp;

	
	@GetMapping("ticketType")
	Map<String ,Object> getTicketType(){
		Map<String, Object> map = new HashMap();
		try {
			List<HelpDeskTicketDetails> ab = procurmentPlanServiceImp.getHelpDeskTicketType();

			map.put("Success", ab);
			return map;
		} catch (Exception e) {
			map.put("Failure", "Failure");
			return map;
		}

	}
	
	@GetMapping("getInstituteForHelpdesk")
	Map<String ,Object> getInstituteForHelpdesk(HttpServletRequest req){
		Map<String, Object> map = new HashMap();
		try {
			Map<String, Object> ab = procurmentPlanServiceImp.findinstituteAlldata(req );

			//map.put("Success", ab);
			return ab;
		} catch (Exception e) {
			map.put("Failure", "Failure");
			return map;
		}

	}

	@GetMapping("ticketPriority")
	Map<String ,Object> getTicketPriority(){
		Map<String, Object> map = new HashMap();
		try {
			List<HelpDeskTicketDetails> ab = procurmentPlanServiceImp.getHelpDeskTicketPriority();

			map.put("Success", ab);
			return map;
		} catch (Exception e) {
			map.put("Failure", "Failure");
			return map;
		}

	}
	@GetMapping("getAllCommentAndStatus/{id}")
	public Map<String,Object> getAllStatusAndComment(@PathVariable(value="id")Integer id){
		Map<String,Object> map=new HashMap();
	try{  List<TeqipHelpdeskTicketModel> ab= 	procurmentPlanServiceImp.getAllStatusAndComment(id);
map.put("Success", ab);
	return map;
		}
	catch(Exception e){
		map.put("Failure", "Failure");
		return map;
		}
	}
	//admin can change status and and insert comment
	@PutMapping("changetAllStatus")
	public Map<String,Object> changeAllStatusAndComment(@RequestBody Helpdesk_tickethistory helpdesk_tickethistory){
	Map<String,Object> map=new HashMap();
	Integer ab= null;
		try{   ab= 	procurmentPlanServiceImp.updateAllStatusAndComment(helpdesk_tickethistory);
          
            
		if(ab==0)
			map.put("Failed", "Enter a valid comments");
		if(ab!=null)
		map.put("Success", ab);
		return map;

		}
	catch(Exception e){
	
		
		map.put("Failure", e.getMessage());
		return map;
		}
	}
	
	
	
	@GetMapping("getHelpDeskData")
	public Map<String, Object> getHelpDeskData() {
		Map<String, Object> map = new HashMap();
		try {
			List<Helpdesk_ticket> ab = procurmentPlanServiceImp.getHelpDesk();

			map.put("Success", ab);
			return map;
		} catch (Exception e) {
			map.put("Failure", "Failure");
			return map;
		}
	}

	@PostMapping("addhelpdesk")
	public Map<String, Object> addInHelpDesk(@RequestParam("locationid") Integer locationid,
			@RequestParam("statusid") Integer statusid, @RequestParam("typeid") Integer typeid,
			@RequestParam("priorityid") Integer priorityid, @RequestParam("subject") String subject,
			@RequestParam("description") String description,
			@RequestParam(name = "ticketsevirtyid", required = false) Integer ticketsevirtyid,
			@RequestParam("userphone") String userphone, @RequestParam("name") String name,
			@RequestParam("mobileno") String mobileno, @RequestParam("email") String email,
			@RequestParam(name = "file", required = false) MultipartFile file) {
		Map<String, Object> map = new HashMap();
		try {
			String	status=null;
			if(file!=null){
			status=FileUtils.checkFileMimeType(file);
			}
		if(status!="valid" && file!=null)
		{
			map.put("Invalid File","Please enter the File with valid name and extension");
		}
		else if(status=="valid"){
			Integer ticket = procurmentPlanServiceImp.addInHelpDesk(locationid, statusid, typeid, priorityid, subject,
					description, ticketsevirtyid, userphone, name, mobileno, email, file);

			map.put("Sucess", ticket);
		}
		else if (file==null){
			Integer ticket = procurmentPlanServiceImp.addInHelpDesk(locationid, statusid, typeid, priorityid, subject,
					description, ticketsevirtyid, userphone, name, mobileno, email, file);

			map.put("Sucess", ticket);

		}
			
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("Failure", "Failure");
			return map;

		}
	}
	
	@PostMapping("/updatehelpdesk")
	public Map<String,Object> updateInHelpDesk(@RequestParam("hdnTicketId") Integer hdnTicketId,@RequestParam("locationid") Integer locationid,@RequestParam("statusid")Integer statusid,@RequestParam("typeid")Integer typeid,
			@RequestParam("priorityid") Integer priorityid,@RequestParam("subject") String subject,@RequestParam("description") String description,@RequestParam(name="ticketsevirtyid",required=false)Integer ticketsevirtyid,@RequestParam("modifidby")Integer modifidby,@RequestParam(name="modifiedon",required=false)String modifiedon,
			@RequestParam("userphone") String userphone,@RequestParam("name") String name,@RequestParam("mobileno")
	String mobileno,@RequestParam("email")String email,@RequestParam(name="file",required=false)MultipartFile file){
		Map<String,Object> map=new HashMap();
		try{
			String	status=FileUtils.checkFileMimeType(file);
			if(status!="valid")
			{
				map.put("Invalid File","Please enter the File with valid size and valid name and extension");
			}
			else if(status=="valid"){
			
			Integer	ticket=procurmentPlanServiceImp.editInHelpDesk(hdnTicketId, locationid, statusid, typeid, priorityid, subject, description, ticketsevirtyid, modifidby, modifiedon, userphone, name, mobileno, email, file);
			
					map.put("Sucess", ticket);
			}
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("Failure", "Failure");
			return map;

		}

}
	@GetMapping("allHelpdeskStatus")
public Map<String,Object> allHelpdeskStatus(){
		Map<String,Object> map=new HashMap();
	
try{  List<HelpDeskTicketDetails> ab= 	procurmentPlanServiceImp.getAllStatus();

			map.put("Sucess", ab);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			map.put("Failure", "Failure");
			return map;

}
	
	}
	
	@DeleteMapping("deleteHelpDesk/{id}")
	public Map<String,Object> deleteHelpDesk(@PathVariable(value="id") Integer id){
		Map<String,Object> map=new HashMap();
		try{
	//		log.info("request"+"request");
		Integer id1=	procurmentPlanServiceImp.deleteAllHelpdesk(id);
		map.put("Sucess", id1);
		return map;
	} catch (Exception e) {
		e.printStackTrace();
		map.put("Failure", "Failure");
		return map;

		}
	}
	
	@GetMapping("/downloadAllfiles")
	public ResponseEntity downloadAllFile( @RequestParam("id") Long id,
			@RequestParam(name = "locationId", required = false) Integer locationId,
			@RequestParam(name = "fileName", required = false) String fileName, HttpServletRequest req)
			throws IOException {
		try {
			// String contentType = "";
			Resource file = null;
			ResponseEntity returnFile = null;
				file=procurmentPlanServiceImp.downloadHelpDeskFile(id.intValue(),locationId,fileName);
				String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
			if (extension.equalsIgnoreCase("jpeg") || extension.equalsIgnoreCase("jpg")) {
				returnFile = ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(file);
			}
			else if (extension.equalsIgnoreCase("png")) {
				returnFile = ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(file);
			}
			else if (extension.equalsIgnoreCase("pdf")) {
				returnFile = ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF).body(file);
			}
			else if (extension.equalsIgnoreCase("xlsx") || extension.equalsIgnoreCase("xls")
					|| extension.equalsIgnoreCase("doc") || extension.equalsIgnoreCase("docx")) {
				String contentType = req.getServletContext().getMimeType(file.getFile().getAbsolutePath());
				if (contentType == null) {
					contentType = "application/octet-stream";
				}
				returnFile = ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
						.body(file);
			}
			return returnFile;

		} catch (Exception e) {
			// throw new MisException("No File Found");
			return new ResponseEntity<JsonResponse>(new JsonResponse(PmssConstant.FAIL, "File Not Found", e.getMessage()),
					null, HttpStatus.OK);
		}


}
}
