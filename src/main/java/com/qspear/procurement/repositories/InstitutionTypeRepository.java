package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipInstitutiontype;

@Repository
public interface InstitutionTypeRepository extends JpaRepository<TeqipInstitutiontype, Integer> {
}
