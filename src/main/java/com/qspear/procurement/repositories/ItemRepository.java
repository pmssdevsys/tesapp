package com.qspear.procurement.repositories;

import com.qspear.procurement.model.ItemSpecificationModel;
import com.qspear.procurement.model.TeqipItemMasterModel;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface ItemRepository extends JpaRepository<TeqipItemMaster, Integer> {

    List<TeqipItemMaster> findDistinctItemNameByTeqipCategorymasterAndTeqipSubcategorymaster(TeqipCategorymaster teqipCategorymaster,
            TeqipSubcategorymaster teqipSubcategorymaster);

    @Query("SELECT new com.qspear.procurement.model.ItemSpecificationModel("
            + " im.itemId,"
            + " im.description,"
            + " im.itemName,"
            + " gd.itemSpecification,"
            + " cm.catId,"
            + " scm.subcatId"
            + " ) FROM TeqipItemMaster im "
            + " LEFT JOIN im.teqipCategorymaster cm"
            + " LEFT JOIN im.teqipSubcategorymaster scm "
            + " LEFT JOIN  im.teqipItemGoodsDetails gd "
            + " WHERE im.itemName LIKE CONCAT('%',:itemName,'%') ")
    public List<ItemSpecificationModel> findByItemNameContaining(@Param("itemName") String itemName);
     
    @Query(value=" select i.ITEM_ID,i.ITEM_NAME,i.DESCRIPTION,c.CATEGORY_NAME,s.SUB_CATEGORY_NAME,c.CAT_ID,s.SUBCAT_ID from TEQIP_ITEM_MASTER i,TEQIP_CATEGORYMASTER c,TEQIP_SUBCATEGORYMASTER s where i.ITEM_CATEGORY_ID=c.CAT_ID and i.SUB_CATEGORY_ID=s.SUBCAT_ID and i.STATUS=:activeStatus and i.ITEM_NAME=:itemname order by i.ITEM_ID desc",nativeQuery=true)
	List<Object[]> findItemMasterByActiveStatus(@Param("activeStatus")Boolean activeStatus,@Param("itemname")String itemname);
	
	  @Query(value="select i.ITEM_ID,i.ITEM_NAME,i.DESCRIPTION,c.CATEGORY_NAME,s.SUB_CATEGORY_NAME,c.CAT_ID,s.SUBCAT_ID from TEQIP_ITEM_MASTER i,TEQIP_CATEGORYMASTER c,TEQIP_SUBCATEGORYMASTER s where i.ITEM_CATEGORY_ID=c.CAT_ID and i.SUB_CATEGORY_ID=s.SUBCAT_ID and i.STATUS=:activeStatus and  i.ITEM_CATEGORY_ID=:catId and i.SUB_CATEGORY_ID=:subcatid order by i.ITEM_ID desc",nativeQuery=true)
	//@Query("select new com.qspear.procurement.model.TeqipItemMasterModel(c.CAT_IDs.SUBCAT_ID,c.CATEGORY_NAME,s.SUB_CATEGORY_NAME) from TeqipItemMaster i,TEQIP_CATEGORYMASTER c,TEQIP_SUBCATEGORYMASTER s where i.ITEM_CATEGORY_ID=c.CAT_ID  i.SUB_CATEGORY_ID=s.SUBCAT_ID and i.STATUS=:activeStatus and  i.ITEM_CATEGORY_ID=:catId order by i.ITEM_ID desc")	
	public List<Object []> findItemMasterByCatId(@Param("activeStatus")Boolean activeStatus,@Param("catId")Integer  catId,@Param("subcatid")Integer subcatid);

	@Query(value="SELECT ITEM_NAME FROM teqip_item_master where STATUS=1",nativeQuery=true)
	String[] getItemName();
}
