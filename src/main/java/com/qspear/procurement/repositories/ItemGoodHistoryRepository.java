package com.qspear.procurement.repositories;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipItemGoodsDetailHistory;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ItemGoodHistoryRepository extends JpaRepository<TeqipItemGoodsDetailHistory, Integer> {

}
