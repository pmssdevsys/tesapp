package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.qspear.procurement.persistence.HelpDeskTicketDetails;


public interface HelpDeskTicketDetailsRepository extends JpaRepository<HelpDeskTicketDetails,Integer> {

	@Query(value="SELECT * FROM edcil.teqip_helpdesk_ticketdetails where tickettypeId in(select t.id from teqip_helpdesk_tickettype t where t.name='TicketType')",nativeQuery=true)
	List<HelpDeskTicketDetails> getTicketType();
	

	@Query(value="SELECT * FROM edcil.teqip_helpdesk_ticketdetails where tickettypeId in(select t.id from teqip_helpdesk_tickettype t where t.name='TicketPriority')",nativeQuery=true)
	List<HelpDeskTicketDetails> getTicketPriority();
	
	 @Query(value="SELECT * FROM edcil.teqip_helpdesk_ticketdetails where tickettypeId=4",nativeQuery=true)   
	 List<HelpDeskTicketDetails> getAllStatus();
}
