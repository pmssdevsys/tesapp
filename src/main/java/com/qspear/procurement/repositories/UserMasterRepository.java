package com.qspear.procurement.repositories;

import com.qspear.procurement.persistence.TeqipUsersMaster;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserMasterRepository extends JpaRepository<TeqipUsersMaster, Integer> {
}
