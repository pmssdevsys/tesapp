package com.qspear.procurement.repositories;

import com.qspear.procurement.model.TeqipActivitiymasterModel;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipActivitiymaster;
import java.util.ArrayList;
import org.springframework.data.repository.query.Param;

@Repository
public interface ActivityRepository extends JpaRepository<TeqipActivitiymaster, Integer> {

	@Query("select actmaster.activityId,actmaster.activityName from TeqipActivitiymaster actmaster order by actmaster.activityName asc" /*where actmaster.teqipCategorymaster=:teqipCategorymaster*/)
	List<Object[]> findByTeqipCategorymaster(/*TeqipCategorymaster teqipCategorymaster*/);
        @Query(value="select * from teqip_activitiymaster a where a.isactive=1  order by a.ACTIVITY_NAME asc",nativeQuery=true)
	ArrayList<TeqipActivitiymaster> findAllActivity();
        @Query(value="select a.ACTIVITY_NAME from teqip_activitiymaster a where a.ACTIVITY_NAME=:activity_name and a.ISACTIVE=1",nativeQuery=true)
   	String findActivityByName(@Param("activity_name")String activity_name);
	TeqipActivitiymaster save(TeqipActivitiymasterModel newActivity);

}
