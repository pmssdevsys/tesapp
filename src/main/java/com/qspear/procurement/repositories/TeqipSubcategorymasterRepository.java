package com.qspear.procurement.repositories;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import org.springframework.data.jpa.repository.JpaRepository;

import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TeqipSubcategorymasterRepository extends JpaRepository<TeqipSubcategorymaster, Integer> {

    	@Query("select submaster.subcatId,submaster.subCategoryName from TeqipSubcategorymaster submaster where submaster.teqipCategorymaster=:teqipCategorymaster order by submaster.subCategoryName asc")
	List<Object[]> findByTeqipCategorymaster(@Param("teqipCategorymaster") TeqipCategorymaster teqipCategorymaster);
        @Query(value="SELECT s.* FROM TEQIP_SUBCATEGORYMASTER s,TEQIP_CATEGORYMASTER c where s.CATEGORY_ID =c.CAT_ID AND s.ISACTIVE= c.ISACTIVE",nativeQuery=true)
	List<TeqipSubcategorymaster> getSubCategoryMasterDataById();
	@Query(value="SELECT s.* FROM TEQIP_SUBCATEGORYMASTER s where s.CATEGORY_ID =:catId AND s.ISACTIVE =:activeStatus",nativeQuery=true)
	List<TeqipSubcategorymaster> findSubCategory(@Param("catId")Integer catId,@Param("activeStatus")Boolean activeStatus);


}
