package com.qspear.procurement.repositories;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipItemWorkDetailHistory;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ItemWorkHistoryRepository extends JpaRepository<TeqipItemWorkDetailHistory, Integer> {

}
