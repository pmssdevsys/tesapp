package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPmssInstitutionlogo;

@Repository
public interface TeqipPmssInstitutionlogoRepository extends JpaRepository<TeqipPmssInstitutionlogo, Integer>{

}
