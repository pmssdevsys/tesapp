package com.qspear.procurement.repositories;

import com.qspear.procurement.persistence.Teqip_procurement_revisedtimeline;
import com.qspear.procurement.persistence.Teqip_procurementmethod_Usertimeline;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface Teqip_procurementmethod_revisedtimelineRepository extends JpaRepository<Teqip_procurement_revisedtimeline, Integer> {

    @Query(value = "select * from teqip_procurementmethod_revisedtimeline t where t.packageId=:packageId", nativeQuery = true)
    Teqip_procurement_revisedtimeline findrevisedtimeline(@Param("packageId") Integer packageId);

    @Query(value = "select * from teqip_procurementmethod_revisedtimeline t where t.PROCUREMENTMETHOD_REVISEDTIMELINEID=:procurementmethodrevisedtimelineId", nativeQuery = true)
    Teqip_procurement_revisedtimeline findrevisedtime(@Param("procurementmethodrevisedtimelineId") Integer procurementmethodrevisedtimelineId);

    @Query(value = "select * from teqip_procurementmethod_revisedtimeline t where t.packageId=:packageId", nativeQuery = true)
    Teqip_procurement_revisedtimeline findPackOne(@Param("packageId") Integer packageId);

}
