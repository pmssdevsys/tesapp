package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qspear.procurement.persistence.Helpdesk_ticket;


public interface Helpdesk_ticketRepository extends JpaRepository<Helpdesk_ticket, Integer>{

	@Query(value="SELECT * FROM teqip_helpdesk_ticket where HDnTicketId=:id",nativeQuery=true)
	public Helpdesk_ticket getTicketObjet(@Param("id")Integer id);
	
	@Query(value="select ht.*,cl.ticketType as Type, cl1.ticketType as Priority, cl2.ticketType as Status,b.AttachedDocName,inst.INSTITUTION_NAME from  teqip_institutions inst, teqip_helpdesk_ticket ht LEFT JOIN teqip_helpdesk_documentattachment B  ON  ht.HDnTicketId = B.TicketID and  B.IsActive=1,teqip_helpdesk_ticketdetails cl,teqip_helpdesk_ticketdetails cl1, teqip_helpdesk_ticketdetails cl2  where  cl2.id=ht.HDnTicketStatusId and  cl1.id=ht.HdnTicketPriorityId  and cl.id=ht.HDnTicketTypeId  and ht.HDnLocationId= inst.INSTITUTION_ID order by ht.HDnTicketId desc",nativeQuery=true)	
    public  List <Object[]> getDataForAdmin();
    
	@Query(value = "select ht.*,cl.ticketType as Type," + "cl1.ticketType as Priority, cl2.ticketType as"
			+ "Status,b.AttachedDocName,inst.INSTITUTION_NAME from "
			+ "teqip_institutions inst, teqip_helpdesk_ticket ht LEFT JOIN teqip_helpdesk_documentattachment B  ON  ht.HDnTicketId = B.TicketID and  B.IsActive=1,teqip_helpdesk_ticketdetails cl,teqip_helpdesk_ticketdetails cl1,"
			+ "teqip_helpdesk_ticketdetails cl2  where  cl2.id=ht.HDnTicketStatusId and "
			+ "cl1.id=ht.HdnTicketPriorityId  and cl.id=ht.HDnTicketTypeId  and "
			+ " ht.HDnLocationId= inst.INSTITUTION_ID and  ht.HDnLocationId in (SELECT t.INSTITUTION_ID FROM   teqip_institutions t where t.SPFUID= ("
			+ "SELECT tm.SPFUID    FROM teqip_users_detail  d,teqip_rolemaster r,teqip_users_master tm "
			+ "where r.ROLE_ID=tm.ROLE_ID and d.USERID=tm.USER_ID and  d.USER_NAME=:username  )   group by t.INSTITUTION_ID) order by ht.HDnTicketId desc", nativeQuery = true)
	  public  List <Object[]> getDataInCaseState(@Param("username")String username);
	  
	@Query(value = "select ht.*,cl.ticketType as Type," + " cl1.ticketType as Priority, cl2.ticketType as"
			+ "Status,b.AttachedDocName,inst.INSTITUTION_NAME from teqip_institutions inst, teqip_helpdesk_ticket ht LEFT JOIN teqip_helpdesk_documentattachment B  ON  ht.HDnTicketId = B.TicketID and B.IsActive=1,teqip_helpdesk_ticketdetails cl,teqip_helpdesk_ticketdetails cl1,"
			+ "teqip_helpdesk_ticketdetails cl2  where  cl2.id=ht.HDnTicketStatusId and "
			+ "cl1.id=ht.HdnTicketPriorityId  and cl.id=ht.HDnTicketTypeId  and"
			+ " ht.HDnLocationId= inst.INSTITUTION_ID and  ht.HDnLocationId in (SELECT t.INSTITUTION_ID FROM   teqip_institutions t where t.INSTITUTION_ID= ("
			+ "SELECT tm.INSTITUTION_ID    FROM teqip_users_detail  d,teqip_rolemaster r,teqip_users_master tm "
			+ "where r.ROLE_ID=tm.ROLE_ID and d.USERID=tm.USER_ID and  d.USER_NAME=:username  )   group by t.INSTITUTION_ID)"
			+ "order by ht.HDnTicketId desc", nativeQuery = true)
	  public List<Object[]>getDataInCaseOfInstitute(@Param("username")String username);
	 
	  @Query(value="SELECT * FROM teqip_helpdesk_ticket where HDnTicketId=:id",nativeQuery=true)
	  public Helpdesk_ticket getTicketNo(@Param("id") Integer id);
}
