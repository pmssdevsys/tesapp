package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qspear.procurement.persistence.TeqipInstitution;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TeqipInstitutionRepository extends JpaRepository<TeqipInstitution, Integer> {

    @Query(nativeQuery = true,
            value = " select   STATE_ID,"
            + "	s.STATE_NAME,"
            + "	'',"
            + "	temp2.E as co,"
            + "	temp.A as A, 		"
            + "	temp.B as B, 		"
            + "	temp.C as C ,"
            + "	temp.D as D,"
            + "	'INST'"
            + "	from teqip_statemaster s "
            + "	left join"
            + "	(		SELECT  case when pp.SPFUID is not null then pp.SPFUID else i.SPFUID end as SID  ,     "
            + "					SUM(case when CATEGORY_ID =1  then ESTIMATED_COST end) as A, 		"
            + "					SUM(case when CATEGORY_ID =2  then ESTIMATED_COST end) as B, 		"
            + "					SUM(case when CATEGORY_ID =3  then ESTIMATED_COST end )as C, 		"
            + "					SUM(ESTIMATED_COST) as D 		"
            + "			FROM teqip_package pp "
            + "			left join teqip_institutions i on  pp.INSTITUTION_ID  = i.INSTITUTION_ID"
            + "			left join teqip_institutiontype it on it.INSTITUTIONTYPE_ID = i.INSTITUTIONTYPE_ID"
            + "			WHERE ((pp.INSTITUTION_ID is not null and it.INSTITUTIONTYPE_NAME !='Centrally Funded Institution') or   pp.SPFUID is not null)"
            + "			 AND 1 = CASE WHEN 0 = :instId THEN 1 WHEN i.INSTITUTION_ID = :instId THEN 1 ELSE 0 END"
            + "			GROUP BY SID"
            + "	) as temp on  temp.SID =  s.STATE_ID"
            + "	left join"
            + "	(		SELECT  case when pas.SPFUID is not null then pas.SPFUID else i.SPFUID end as SID  ,     "
            + "					COUNT(*) as E		"
            + "			FROM teqip_plan_approvalstatus pas "
            + "			INNER JOIN  teqip_pmss_procurementstages ps  on ps.STAGEID = pas.PROCUREMENTSTAGEID"
            + "			left join teqip_institutions i on  pas.INSTITUTION_ID  = i.INSTITUTION_ID"
            + "			left join teqip_institutiontype it on it.INSTITUTIONTYPE_ID = i.INSTITUTIONTYPE_ID"
            + "			WHERE ((pas.INSTITUTION_ID is not null and it.INSTITUTIONTYPE_NAME !='Centrally Funded Institution') or   pas.SPFUID is not null  )"
            + "			and   pas.currentStage =1 and ps.PROCUREMENTSTAGEID IN (:stage) "
            + "			AND 1 = CASE WHEN 0 = :instId THEN 1 WHEN i.INSTITUTION_ID = :instId THEN 1 ELSE 0 END"
            + "			GROUP BY SID"
            + "	) as temp2 on  temp2.SID =  s.STATE_ID"
            + "	where   1 = CASE WHEN 0 = :stateId THEN 1 WHEN  s.STATE_ID = :stateId THEN 1 ELSE 0 END")
    public List<Object[]> stateWiseApprovalStateInstPackage(@Param("stage") List<Integer> approvalStages,
            @Param("stateId") int stateId,
            @Param("instId") int instId);

    @Query(nativeQuery = true,
            value = "select 0 as A,"
            + "		'CFI' as B ,"
            + "		'' as C,"
            + "		(select count(*) "
            + "		from teqip_plan_approvalstatus pas"
            + "			inner join teqip_pmss_procurementstages ps on ps.STAGEID = pas.PROCUREMENTSTAGEID"
            + "			inner join teqip_institutions i on pas.INSTITUTION_ID = i.INSTITUTION_ID"
            + "			inner join teqip_institutiontype it on it.INSTITUTIONTYPE_ID = i.INSTITUTIONTYPE_ID"
            + "			where  pas.INSTITUTION_ID is not null and it.INSTITUTIONTYPE_NAME = 'Centrally Funded Institution' "
            + "			and pas.currentStage = 1"
            + "			and ps.PROCUREMENTSTAGEID in (:stage )) as D,		"
            + "		sum( case when pp.CATEGORY_ID = 1 then pp.ESTIMATED_COST end ) as E,"
            + "		sum( case when pp.CATEGORY_ID = 2 then pp.ESTIMATED_COST end ) as F,"
            + "		sum( case when pp.CATEGORY_ID = 3 then pp.ESTIMATED_COST end ) as G,"
            + "		sum( ESTIMATED_COST ) as H,"
            + "		'CFI' as I"
            + "	from teqip_package pp"
            + "	inner join  teqip_institutions i on pp.INSTITUTION_ID = i.INSTITUTION_ID"
            + "	inner join teqip_institutiontype it on it.INSTITUTIONTYPE_ID = i.INSTITUTIONTYPE_ID"
            + "	where  pp.INSTITUTION_ID is not null and it.INSTITUTIONTYPE_NAME = 'Centrally Funded Institution' ")
    public List<Object[]> stateWiseApprovalCFIPackage(@Param("stage") List<Integer> approvalStages);

    @Query(nativeQuery = true,
            value = "	select 0 as A,"
            + "		'NPIU' as B ,"
            + "		'' as C,"
            + "		(select count(*) "
            + "		from teqip_plan_approvalstatus pas"
            + "			inner join teqip_pmss_procurementstages ps on ps.STAGEID = pas.PROCUREMENTSTAGEID"
            + "			where  pp.INSTITUTION_ID is  null and pp.SPFUID is null and CREATED_TYPE =0"
            + "			and pas.currentStage = 1"
            + "			and ps.PROCUREMENTSTAGEID in (:stage )) as D,		"
            + "		sum( case when pp.CATEGORY_ID = 1 then pp.ESTIMATED_COST end ) as E,"
            + "		sum( case when pp.CATEGORY_ID = 2 then pp.ESTIMATED_COST end ) as F,"
            + "		sum( case when pp.CATEGORY_ID = 3 then pp.ESTIMATED_COST end ) as G,"
            + "		sum( ESTIMATED_COST ) as H,"
            + "		'NPIU' as I"
            + "	from teqip_package pp"
            + "	where  pp.INSTITUTION_ID is  null and pp.SPFUID is null and pp.TYPEOFPLAN_CREATOR =0"
            + "	"
    )
    public List<Object[]> stateWiseApprovalNationalPackage(@Param("stage") List<Integer> approvalStages);

    @Query(nativeQuery = true,
            value = "	select 0 as A,"
            + "		'EDCIL' as B ,"
            + "		'' as C,"
            + "		(select count(*) "
            + "                 from teqip_plan_approvalstatus pas"
            + "			inner join teqip_pmss_procurementstages ps on ps.STAGEID = pas.PROCUREMENTSTAGEID"
            + "			where  pas.INSTITUTION_ID is  null and pas.SPFUID is null and CREATED_TYPE =1"
            + "			and pas.currentStage = 1"
            + "			and ps.PROCUREMENTSTAGEID in (:stage )) as D,		"
            + "		sum( case when pp.CATEGORY_ID = 1 then pp.ESTIMATED_COST end ) as E,"
            + "		sum( case when pp.CATEGORY_ID = 2 then pp.ESTIMATED_COST end ) as F,"
            + "		sum( case when pp.CATEGORY_ID = 3 then pp.ESTIMATED_COST end ) as G,"
            + "		sum( ESTIMATED_COST ) as H,"
            + "		'EDCIL' as I"
            + "	from teqip_package pp"
            + "	where  pp.INSTITUTION_ID is  null and pp.SPFUID is null and pp.TYPEOFPLAN_CREATOR =1"
            + "	"
    )
    public List<Object[]> stateWiseApprovalEdcilPackage(@Param("stage") List<Integer> approvalStages);

    @Query(nativeQuery = true,
            value = "select  i.INSTITUTION_ID,  "
            + " i.INSTITUTION_NAME,"
            + " sm.SUBCOMPONENTCODE,"
            + " ps.STAGE_DESCRIPTION as stage, "
            + " pm.END_DATE,"
            + " pas.REVISEID,"
            + " count(*), "
            + " SUM(case when p.CATEGORY_ID =1  then ESTIMATED_COST ELSE 0 end) as A,"
            + " SUM(case when p.CATEGORY_ID =2  then ESTIMATED_COST ELSE 0 end) as B,"
            + " SUM(case when p.CATEGORY_ID =3  then ESTIMATED_COST ELSE 0 end )as C,"
            + " SUM(p.ESTIMATED_COST) as D,"
            + " p.PROCUREMENT_PLAN_ID,"
            + " 'INST' "
            + " FROM teqip_package p   "
            + " inner join teqip_plan_approvalstatus pas on pas.INSTITUTION_ID =  p.INSTITUTION_ID and pas.currentStage=1 "
            + " Inner JOIN teqip_pmss_procurementstages ps ON pas.PROCUREMENTSTAGEID = ps.STAGEID  "
            + " INNER JOIN teqip_procurementmaster  pm on pm.PROCUREMENTMASTER_ID  =  p.PROCUREMENT_PLAN_ID "
            + " INNER JOIN  teqip_institutions i On p.INSTITUTION_ID  =  i.INSTITUTION_ID"
            + " inner join teqip_institutiontype it on it.INSTITUTIONTYPE_ID = i.INSTITUTIONTYPE_ID"
            + " INNER JOIN teqip_institutionsubcomponentmapping g  ON g.INSTITUTION_ID=i.INSTITUTION_ID"
            + " INNER JOIN teqip_subcomponentmaster sm ON sm.ID =  g.SUBCOMPONENT_ID"
            + " where  it.INSTITUTIONTYPE_NAME != 'Centrally Funded Institution' and i.SPFUID = :stateId and ps.PROCUREMENTSTAGEID  IN (:stage)"
            + " AND 1 = CASE WHEN 0 = :instId THEN 1 WHEN i.INSTITUTION_ID = :instId THEN 1 ELSE 0 END "
            + " GROUP BY  i.INSTITUTION_ID,sm.ID, pm.PROCUREMENTMASTER_ID,ps.STAGEID,pas.REVISEID")
    public List<Object[]> institutePackageApproval(@Param("stateId") int stateId, @Param("stage") List<Integer> approvalStages, @Param("instId") int instId);

    @Query(nativeQuery = true,
            value = "select"
            + "	i.INSTITUTION_ID,"
            + "	i.INSTITUTION_NAME,"
            + "	sm.SUBCOMPONENTCODE,"
            + "	ps.STAGE_DESCRIPTION as stage,"
            + "	pm.END_DATE,"
            + "	pas.REVISEID,"
            + "	count(*),"
            + "	sum( case when p.CATEGORY_ID = 1 then ESTIMATED_COST else 0 end ) as A,"
            + "	sum( case when p.CATEGORY_ID = 2 then ESTIMATED_COST else 0 end ) as B,"
            + "	sum( case when p.CATEGORY_ID = 3 then ESTIMATED_COST else 0 end ) as C,"
            + "	sum( p.ESTIMATED_COST ) as D,"
            + "	p.PROCUREMENT_PLAN_ID,"
            + "	'INST'"
            + " from teqip_package p"
            + " inner join teqip_plan_approvalstatus pas on pas.INSTITUTION_ID = p.INSTITUTION_ID and pas.currentStage = 1"
            + " inner join teqip_pmss_procurementstages ps on pas.PROCUREMENTSTAGEID = ps.STAGEID"
            + " inner join teqip_procurementmaster pm on pm.PROCUREMENTMASTER_ID = p.PROCUREMENT_PLAN_ID"
            + " inner join teqip_institutions i on p.INSTITUTION_ID = i.INSTITUTION_ID"
            + " inner join teqip_institutiontype it on it.INSTITUTIONTYPE_ID = i.INSTITUTIONTYPE_ID"
            + " inner join teqip_institutionsubcomponentmapping g on g.INSTITUTION_ID = i.INSTITUTION_ID"
            + " inner join teqip_subcomponentmaster sm on sm.ID = g.SUBCOMPONENT_ID"
            + " where"
            + "	it.INSTITUTIONTYPE_NAME = 'Centrally Funded Institution'"
            + "	and ps.PROCUREMENTSTAGEID in (:stage )"
            + " AND 1 = CASE WHEN 0 = :instId THEN 1 WHEN i.INSTITUTION_ID = :instId THEN 1 ELSE 0 END "
            + " group by i.INSTITUTION_ID,sm.ID, pm.PROCUREMENTMASTER_ID,ps.STAGEID,pas.REVISEID")
    public List<Object[]> instituteCFIPackageApproval(@Param("stage") List<Integer> approvalStages,@Param("instId") int instId);

    @Query(nativeQuery = true,
            value = "select  i.INSTITUTION_ID,  "
            + " i.INSTITUTION_NAME,"
            + " sm.SUBCOMPONENTCODE,"
            + " ps.STAGE_DESCRIPTION as stage, "
            + " pm.END_DATE,"
            + " pas.REVISEID,"
            + " count(*), "
            + " SUM(case when p.CATEGORY_ID =1  then ESTIMATED_COST ELSE 0 end) as A,"
            + " SUM(case when p.CATEGORY_ID =2  then ESTIMATED_COST ELSE 0 end) as B,"
            + " SUM(case when p.CATEGORY_ID =3  then ESTIMATED_COST ELSE 0 end )as C,"
            + " SUM(p.ESTIMATED_COST) as D,"
            + " p.PROCUREMENT_PLAN_ID,"
            + " 'INST' "
            + " FROM teqip_package p   "
            + " inner join teqip_plan_approvalstatus pas on pas.INSTITUTION_ID =  p.INSTITUTION_ID and pas.currentStage=1 "
            + " Inner JOIN teqip_pmss_procurementstages ps ON pas.PROCUREMENTSTAGEID = ps.STAGEID  "
            + " INNER JOIN teqip_procurementmaster  pm on pm.PROCUREMENTMASTER_ID  =  p.PROCUREMENT_PLAN_ID "
            + " INNER JOIN  teqip_institutions i On p.INSTITUTION_ID  =  i.INSTITUTION_ID"
            + " inner join teqip_institutiontype it on it.INSTITUTIONTYPE_ID = i.INSTITUTIONTYPE_ID"
            + " INNER JOIN teqip_institutionsubcomponentmapping g  ON g.INSTITUTION_ID=i.INSTITUTION_ID"
            + " INNER JOIN teqip_subcomponentmaster sm ON sm.ID =  g.SUBCOMPONENT_ID"
            + " where  it.INSTITUTIONTYPE_NAME != 'Centrally Funded Institution' and i.SPFUID = :stateId "
            + " AND 1 = CASE WHEN 0 = :instId THEN 1 WHEN i.INSTITUTION_ID = :instId THEN 1 ELSE 0 END "
            + " GROUP BY  i.INSTITUTION_ID,sm.ID, pm.PROCUREMENTMASTER_ID,ps.STAGEID,pas.REVISEID")
    public List<Object[]> institutePackageApprovalNotIn(@Param("stateId") int stateId, @Param("instId") int instId);

     @Query(nativeQuery = true,
            value = "select"
            + "	i.INSTITUTION_ID,"
            + "	i.INSTITUTION_NAME,"
            + "	sm.SUBCOMPONENTCODE,"
            + "	ps.STAGE_DESCRIPTION as stage,"
            + "	pm.END_DATE,"
            + "	pas.REVISEID,"
            + "	count(*),"
            + "	sum( case when p.CATEGORY_ID = 1 then ESTIMATED_COST else 0 end ) as A,"
            + "	sum( case when p.CATEGORY_ID = 2 then ESTIMATED_COST else 0 end ) as B,"
            + "	sum( case when p.CATEGORY_ID = 3 then ESTIMATED_COST else 0 end ) as C,"
            + "	sum( p.ESTIMATED_COST ) as D,"
            + "	p.PROCUREMENT_PLAN_ID,"
            + "	'INST'"
            + " from teqip_package p"
            + " inner join teqip_plan_approvalstatus pas on pas.INSTITUTION_ID = p.INSTITUTION_ID and pas.currentStage = 1"
            + " inner join teqip_pmss_procurementstages ps on pas.PROCUREMENTSTAGEID = ps.STAGEID"
            + " inner join teqip_procurementmaster pm on pm.PROCUREMENTMASTER_ID = p.PROCUREMENT_PLAN_ID"
            + " inner join teqip_institutions i on p.INSTITUTION_ID = i.INSTITUTION_ID"
            + " inner join teqip_institutiontype it on it.INSTITUTIONTYPE_ID = i.INSTITUTIONTYPE_ID"
            + " inner join teqip_institutionsubcomponentmapping g on g.INSTITUTION_ID = i.INSTITUTION_ID"
            + " inner join teqip_subcomponentmaster sm on sm.ID = g.SUBCOMPONENT_ID"
            + " where"
            + "	it.INSTITUTIONTYPE_NAME = 'Centrally Funded Institution'"
            + " group by i.INSTITUTION_ID,sm.ID, pm.PROCUREMENTMASTER_ID,ps.STAGEID,pas.REVISEID")
    public List<Object[]> instituteCFIPackageApprovalNotIn();

    
    @Query(nativeQuery = true,
            value
            = "select "
            + " sm.STATE_ID, "
            + " sm.STATE_NAME,"
            + " '', "
            + " ps.STAGE_DESCRIPTION as stage,"
            + " pm.END_DATE,"
            + " pas.REVISEID,"
            + " count(*),"
            + " SUM(case when p.CATEGORY_ID =1  then ESTIMATED_COST ELSE 0 end) as A,"
            + " SUM(case when p.CATEGORY_ID =2  then ESTIMATED_COST ELSE 0 end) as B,"
            + " SUM(case when p.CATEGORY_ID =3  then ESTIMATED_COST ELSE 0 end )as C,"
            + " SUM(p.ESTIMATED_COST) as D,"
            + " p.PROCUREMENT_PLAN_ID,"
            + " 'SPFU'"
            + " FROM teqip_package p "
            + " inner join teqip_plan_approvalstatus pas on pas.SPFUID =  p.SPFUID and pas.currentStage=1"
            + " Inner JOIN teqip_pmss_procurementstages ps ON pas.PROCUREMENTSTAGEID = ps.STAGEID"
            + " INNER JOIN teqip_procurementmaster  pm on pm.PROCUREMENTMASTER_ID  =  p.PROCUREMENT_PLAN_ID"
            + " INNER JOIN teqip_statemaster sm ON sm.STATE_ID =  p.SPFUID"
            + " WHERE p.SPFUID = :stateId and ps.PROCUREMENTSTAGEID  IN (:stage)"
            + " AND 1 = CASE WHEN 0 = :loginStateId THEN 1 WHEN sm.STATE_ID = :loginStateId THEN 1 ELSE 0 END"
            + " GROUP BY sm.STATE_ID, pm.PROCUREMENTMASTER_ID,ps.STAGEID,pas.REVISEID")

    public List<Object[]> statePackageApproval(@Param("stateId") int stateId,
            @Param("stage") List<Integer> approvalStages,
            @Param("loginStateId") int loginStateId);

    @Query(nativeQuery = true,
            value
            = "select "
            + " sm.STATE_ID, "
            + " sm.STATE_NAME,"
            + " '', "
            + " ps.STAGE_DESCRIPTION as stage,"
            + " pm.END_DATE,"
            + " pas.REVISEID,"
            + " count(*),"
            + " SUM(case when p.CATEGORY_ID =1  then ESTIMATED_COST ELSE 0 end) as A,"
            + " SUM(case when p.CATEGORY_ID =2  then ESTIMATED_COST ELSE 0 end) as B,"
            + " SUM(case when p.CATEGORY_ID =3  then ESTIMATED_COST ELSE 0 end )as C,"
            + " SUM(p.ESTIMATED_COST) as D,"
            + " p.PROCUREMENT_PLAN_ID,"
            + " 'SPFU'"
            + " FROM teqip_package p "
            + " inner join teqip_plan_approvalstatus pas on pas.SPFUID =  p.SPFUID and pas.currentStage=1"
            + " Inner JOIN teqip_pmss_procurementstages ps ON pas.PROCUREMENTSTAGEID = ps.STAGEID"
            + " INNER JOIN teqip_procurementmaster  pm on pm.PROCUREMENTMASTER_ID  =  p.PROCUREMENT_PLAN_ID"
            + " INNER JOIN teqip_statemaster sm ON sm.STATE_ID =  p.SPFUID"
            + " WHERE p.SPFUID = :stateId "
            + " AND 1 = CASE WHEN 0 = :loginStateId THEN 1 WHEN sm.STATE_ID = :loginStateId THEN 1 ELSE 0 END"
            + " GROUP BY sm.STATE_ID, pm.PROCUREMENTMASTER_ID,ps.STAGEID,pas.REVISEID")

    public List<Object[]> statePackageApprovalNotIn(@Param("stateId") int stateId, @Param("loginStateId") int loginStateId);

    @Query(nativeQuery = true,
            value
            = " select  h.INSTITUTION_ID, ins.INSTITUTION_NAME,"
            + " h.SPFUID,sm.STATE_NAME, "
            + " Min(h.CREATED_ON),"
            + " h.SUB_COMPONENT_ID,sb.SUBCOMPONENTCODE, "
            + " um.USER_NAME, h.REVISEID"
            + " from teqip_package_rev_history h"
            + " LEFT join  teqip_institutions ins on ins.INSTITUTION_ID = h.INSTITUTION_ID"
            + " left join teqip_statemaster sm on sm.STATE_ID = h.SPFUID"
            + " left join teqip_subcomponentmaster sb on sb.ID =  h.SUB_COMPONENT_ID"
            + " left join teqip_users_detail um on um.USERID =  h.CREATED_BY"
            + "  WHERE "
            + "  1 = CASE WHEN 0 = :instituteId THEN 1 WHEN h.INSTITUTION_ID = :instituteId THEN 1 ELSE 0 END"
            + "  AND  1 = CASE WHEN 0 = :stateId THEN 1 WHEN h.SPFUID = :stateId THEN 1 ELSE 0 END"
            + " GROUP BY h.INSTITUTION_ID, ins.INSTITUTION_NAME,h.SPFUID,sm.STATE_NAME,h.SUB_COMPONENT_ID,sb.SUBCOMPONENTCODE,um.USER_NAME, h.REVISEID"
    )
    public List<Object[]> packageHistory(@Param("instituteId") int instituteId,
            @Param("stateId") int stateId);

    @Query(value = "SELECT s.STATE_NAME,t.INSTITUTIONTYPE_NAME, i.INSTITUTION_ID,i.INSTITUTION_NAME,i.SPFUID,i.WEBSITEURL,i.ALLOCATEDBUGET,i.FAXNUMBER,i.TELEPHONENUMBER,i.EMAILID,i.ADDRESS,i.INSTITUTION_CODE,t.INSTITUTIONTYPE_ID from teqip_institutions i, TEQIP_INSTITUTIONTYPE t,teqip_statemaster s where  i.INSTITUTIONTYPE_ID=t.INSTITUTIONTYPE_ID AND s.STATE_ID=i.SPFUID AND t.ISACTIVE = :isActive ORDER BY i.INSTITUTION_NAME", nativeQuery = true)
    public List<Object[]> getActiveInstituteData(@Param("isActive") Boolean status);

    @Query(value = "SELECT * FROM TEQIP_INSTITUTIONS i where i.INSTITUTION_ID = :id", nativeQuery = true)
    public TeqipInstitution getInstituteDataById(@Param("id") Integer instId);

    @Query(value = "SELECT * FROM TEQIP_INSTITUTIONS i where i.SPFUID = :stId", nativeQuery = true)
    public List<TeqipInstitution> getInstByStId(@Param("stId") Integer stId);

    @Query(value = "SELECT * FROM TEQIP_INSTITUTIONS i where i.INSTITUTIONTYPE_ID = :id", nativeQuery = true)
    public List<TeqipInstitution> findIstByTypeId(Integer id);

    @Query(value = "SELECT * FROM TEQIP_INSTITUTIONS i where i.SPFUID = :id", nativeQuery = true)
    public List<TeqipInstitution> getInstituteDataByStateId(@Param("id") Integer id);

    @Query(value = "SELECT * FROM TEQIP_INSTITUTIONS i where i.INSTITUTION_CODE = :institutecode", nativeQuery = true)
    TeqipInstitution findInsByCode(@Param("institutecode") String institutecode);

    @Query(value = "SELECT t.INSTITUTIONTYPE_NAME, i.INSTITUTION_ID,i.INSTITUTION_NAME,i.STATE,i.SPFUID,i.WEBSITEURL,i.ALLOCATEDBUGET,i.FAXNUMBER,i.TELEPHONENUMBER,i.EMAILID,i.ADDRESS,i.INSTITUTION_CODE,t.INSTITUTIONTYPE_ID from teqip_institutions i, TEQIP_INSTITUTIONTYPE t where  i.INSTITUTIONTYPE_ID=t.INSTITUTIONTYPE_ID AND i.SPFUID = :id ORDER BY i.INSTITUTION_NAME", nativeQuery = true)
    public List<Object[]> getInstitutesDataByStateId(@Param("id") Integer id);

}
