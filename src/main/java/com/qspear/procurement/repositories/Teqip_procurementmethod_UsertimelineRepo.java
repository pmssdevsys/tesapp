package com.qspear.procurement.repositories;

import com.qspear.procurement.persistence.Teqip_procurementmethod_Usertimeline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface Teqip_procurementmethod_UsertimelineRepo extends JpaRepository<Teqip_procurementmethod_Usertimeline, Integer> {

    @Query(value = "select * from teqip_procurementmethod_user_timeline t where t.PROCUREMENTMETHOD_USER_TIMELINEID=:procurementmethodtimelineId", nativeQuery = true)
    Teqip_procurementmethod_Usertimeline findOne(@Param("procurementmethodtimelineId") Long procurementmethodtimelineId);

    @Query(value = "select * from teqip_procurementmethod_user_timeline t where t.packageId=:packageId", nativeQuery = true)
    Teqip_procurementmethod_Usertimeline findPackOne(@Param("packageId") Integer packageId);

}
