package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qspear.procurement.persistence.SupplierMaster;

public interface SupplierMasterRepo extends JpaRepository<SupplierMaster,Integer>{
  //@Query(value="select s.supplierID,s.suppliername,s.supllieraddress,s.suplieremailid,s.supplierrepresentativename,s.suppliercity,s.supplierstate,s.suppliercountry,s.suppliernationality,s.suppliersource,s.supplierspecificsource,s.supplierpincode,s.supplierphoneno,s.supliertannumber,s.suplierfaxnumber,s.suplierpannumber,s.supliereletter,s.pmss_type,s.supliergstnumber,s.supliertaxnumber,s.epfNo,s.esiNo,s.contractLabourLicenseNo,state.STATE_ID from TEQIP_PMSS_suppliermaster s,teqip_statemaster state where s.ISACTIVE=1   and s.supplierstate=state.STATE_NAME and 	s.supplierstate=:name order by s.suppliername asc",nativeQuery=true)
@Query(value="Select s.* from TEQIP_PMSS_suppliermaster s where s.isactive=1 and s.supplierstate=:name",nativeQuery=true)
	List<SupplierMaster> findSupplierState(@Param("name")String name);

  //  @Query(value="select s.supplierID,s.suppliername,s.supllieraddress,s.suplieremailid,s.supplierrepresentativename,s.suppliercity,s.supplierstate,s.suppliercountry,s.suppliernationality,s.suppliersource,s.supplierspecificsource,s.supplierpincode,s.supplierphoneno,s.supliertannumber,s.suplierfaxnumber,s.suplierpannumber,s.supliereletter,s.pmss_type,s.supliergstnumber,s.supliertaxnumber,s.epfNo,s.esiNo,s.contractLabourLicenseNo,state.  from TEQIP_PMSS_suppliermaster s,teqip_statemaster state where s.ISACTIVE=1   and s.supplierstate=state.STATE_NAME and s.suppliername=:suppliername order by s.suppliername asc",nativeQuery=true)
	@Query(value="Select s.* from TEQIP_PMSS_suppliermaster s where s.isactive=1 and s.suppliername=:suppliername",nativeQuery=true)
	List<SupplierMaster> findSupplier(@Param("suppliername")String suppliername);

	@Query(value="Select s.* from TEQIP_PMSS_suppliermaster s",nativeQuery=true) 
	List<SupplierMaster> getSupplierState();
	
	
	@Query("Select d.suppliername  from SupplierMaster d")
	List getSuppliername();

}
