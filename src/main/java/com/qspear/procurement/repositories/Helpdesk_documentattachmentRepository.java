package com.qspear.procurement.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qspear.procurement.persistence.Helpdesk_documentattachment;

public interface Helpdesk_documentattachmentRepository extends JpaRepository<Helpdesk_documentattachment,Integer>{

//	@Transactional
//	@Modifying
//	@Query("delete from  helpdesk_documentattachment  where TicketID=:id")
//	public void deleteHelpdeskDoc(@Param("id") Integer id);
//	
	@Query(value="Select * from  helpdesk_documentattachment  where TicketID=:id",nativeQuery=true)
	public Helpdesk_documentattachment findHelpdeskDoc(@Param("id") Integer id);
	
	@Transactional
	@Modifying
	@Query("UPDATE Helpdesk_documentattachment SET IsActive= 0 WHERE TicketID =:id")
	public void updateDocHelpdeskDoc(@Param("id") Integer id);
	
}
