package com.qspear.procurement.repositories;

import com.qspear.procurement.persistence.TeqipUsersDetail;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserDetailRepository extends JpaRepository<TeqipUsersDetail, Long> {

}