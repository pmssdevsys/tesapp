package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qspear.procurement.persistence.TeqipUsersMaster;

public interface TeqipUsersMasterRepo extends JpaRepository<TeqipUsersMaster,Long>{
	 @Query(value="select u.INSTITUTION_ID from edcil.teqip_users_master u where u.USER_ID=:userid",nativeQuery=true)
		Integer findInstById(@Param("userid")Long userid);
	     @Query(value="select u.SPFUID from edcil.teqip_users_master u where u.USER_ID=:userid",nativeQuery=true)
		Long getInState(@Param("userid")Long userid);


}
