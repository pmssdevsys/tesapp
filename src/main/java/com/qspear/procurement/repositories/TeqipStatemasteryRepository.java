package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipStatemaster;

@Repository
public interface TeqipStatemasteryRepository extends JpaRepository<TeqipStatemaster, Integer> {
    @Query(value="select s.STATE_NAME from TEQIP_STATEMASTER s where s.STATE_ID=:supplierstateid",nativeQuery=true)
	String findOnestate(@Param("supplierstateid")Integer supplierstateid);

}
