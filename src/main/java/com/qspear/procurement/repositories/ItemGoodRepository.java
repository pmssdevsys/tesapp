package com.qspear.procurement.repositories;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipItemGoodsDetail;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ItemGoodRepository extends JpaRepository<TeqipItemGoodsDetail, Integer> {

}
