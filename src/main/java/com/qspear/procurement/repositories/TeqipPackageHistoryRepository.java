package com.qspear.procurement.repositories;


import com.qspear.procurement.persistence.TeqipInstitution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPackageHistory;
import com.qspear.procurement.persistence.TeqipProcurementmaster;
import com.qspear.procurement.persistence.TeqipStatemaster;
import java.util.List;

@Repository
public interface TeqipPackageHistoryRepository extends JpaRepository<TeqipPackageHistory, Integer> {

    public List<TeqipPackageHistory> findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndReviseId(TeqipProcurementmaster plan, TeqipInstitution institute, TeqipStatemaster state,Integer reviseId);

    public List<TeqipPackageHistory> findByTeqipInstitutionAndTeqipStatemasterAndReviseId(TeqipInstitution institute, TeqipStatemaster state,Integer reviseId);

  
}
