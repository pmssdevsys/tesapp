package com.qspear.procurement.repositories;

import com.qspear.procurement.persistence.TeqipNpiuMaster;
import org.springframework.data.jpa.repository.JpaRepository;


public interface NpiuMasterRepository extends JpaRepository<TeqipNpiuMaster, Integer> {
}
