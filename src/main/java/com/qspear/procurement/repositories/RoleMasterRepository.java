package com.qspear.procurement.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipRolemaster;

@Repository
public interface RoleMasterRepository extends JpaRepository<TeqipRolemaster, Integer> {

    public TeqipRolemaster findByPmssRole(String USER_ROLE_INST);
    
    @Query(value="SELECT * FROM edcil.teqip_rolemaster where role_id=:id",nativeQuery=true)
   public  TeqipRolemaster getRoleDescription(@Param("id")Integer id);


}
