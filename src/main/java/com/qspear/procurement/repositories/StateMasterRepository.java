package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipStatemaster;

@Repository
public interface StateMasterRepository extends JpaRepository<TeqipStatemaster, Integer> {

    public TeqipStatemaster findFirstByStateName(String stateName);

}
