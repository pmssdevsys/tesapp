package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPmssCurrencymaster;

@Repository
public interface TeqipPmssCurrencymasterRepository extends JpaRepository<TeqipPmssCurrencymaster, Integer> {
	TeqipPmssCurrencymaster findByCurrencyname(String currencyname);
}
