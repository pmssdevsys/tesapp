package com.qspear.procurement.repositories;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipItemWorkDetail;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ItemWorkRepository extends JpaRepository<TeqipItemWorkDetail, Integer> {

}
