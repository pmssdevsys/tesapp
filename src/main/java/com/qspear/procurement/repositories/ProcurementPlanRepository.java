package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipProcurementmaster;

@Repository
public interface ProcurementPlanRepository extends JpaRepository<TeqipProcurementmaster, Integer> {
	List<TeqipProcurementmaster> findByIsactive(Boolean isactive);
}
