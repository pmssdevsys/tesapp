package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.TeqipProcurementmaster;
import com.qspear.procurement.persistence.TeqipStatemaster;

@Repository
public interface TeqipPackageRepository extends JpaRepository<TeqipPackage, Integer> {

   
    public List<TeqipPackage> findByTeqipProcurementmasterAndTypeofplanCreator(TeqipProcurementmaster teqipProcurementmaster, int i);

    List<TeqipPackage> findByTeqipInstitutionAndTeqipProcurementmaster(TeqipInstitution teqipInstitution,
            TeqipProcurementmaster teqipProcurementmaster);

    List<TeqipPackage> findByTeqipStatemasterAndTeqipProcurementmaster(TeqipStatemaster teqipStatemaster,
            TeqipProcurementmaster teqipProcurementmaster);


    public List<TeqipPackage> findByTeqipInstitutionAndTeqipStatemasterAndTypeofplanCreator(TeqipInstitution object, TeqipStatemaster object0,Integer typeofplanCreator);

    public List<TeqipPackage> findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndTypeofplanCreator(TeqipProcurementmaster plan, TeqipInstitution object, TeqipStatemaster object0,Integer typeofplanCreator);

    public List<TeqipPackage> findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndIsPackageCancelledAndTypeofplanCreator(TeqipProcurementmaster plan, TeqipInstitution institute, TeqipStatemaster state, int i,Integer typeofplanCreator);

    public List<TeqipPackage> findByTeqipInstitutionAndTeqipStatemasterAndIsPackageCompletedAndTypeofplanCreator(TeqipInstitution institute, TeqipStatemaster state, int i,Integer typeofplanCreator);

    public List<TeqipPackage> findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndIsPackageCompletedAndTypeofplanCreator(TeqipProcurementmaster plan, TeqipInstitution institute, TeqipStatemaster state, int i,Integer typeofplanCreator);

    public List<TeqipPackage> findByTeqipInstitutionAndTeqipStatemasterAndIsPackageCancelledAndTypeofplanCreator(TeqipInstitution institute, TeqipStatemaster state, int i,Integer typeofplanCreator);

    public List<TeqipPackage> findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndTeqipPmssProcurementstageInAndIsInitiatedAndTypeofplanCreator(TeqipProcurementmaster plan, TeqipInstitution institute, TeqipStatemaster state, List<TeqipPmssProcurementstage> approved, int i,Integer typeofplanCreator);

    public List<TeqipPackage> findByTeqipInstitutionAndTeqipStatemasterAndTeqipPmssProcurementstageInAndIsInitiatedAndTypeofplanCreator(TeqipInstitution institute, TeqipStatemaster state, List<TeqipPmssProcurementstage> approved, int i,Integer typeofplanCreator);

    public List<TeqipPackage> findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndTeqipPmssProcurementstageInAndIsInitiatedAndIsPackageCancelledAndIsPackageCompletedAndInitiateDateNotNullAndTypeofplanCreator(TeqipProcurementmaster plan, TeqipInstitution institute, TeqipStatemaster state, List<TeqipPmssProcurementstage> approved, int i, int i0, int i1,Integer typeofplanCreator);

    public List<TeqipPackage> findByTeqipInstitutionAndTeqipStatemasterAndTeqipPmssProcurementstageInAndIsInitiatedAndIsPackageCancelledAndIsPackageCompletedAndInitiateDateNotNullAndTypeofplanCreator(TeqipInstitution institute, TeqipStatemaster state, List<TeqipPmssProcurementstage> approved, int i, int i0, int i1,Integer typeofplanCreator);

    @Query("SELECT SUM(estimatedCost) FROM TeqipPackage WHERE teqipCategorymaster=:teqipCategorymaster  and teqipInstitution=:teqipInstitution and  packageId != :packageId ")
    public Double findSumOfEstimatedCostByCategoryIdAndTeqipInstitutionAndPackageIdNot(
            @Param("teqipCategorymaster") TeqipCategorymaster teqipCategorymaster,
            @Param("teqipInstitution") TeqipInstitution teqipInstitution,
            @Param("packageId") Integer packageId);

    @Query("SELECT SUM(estimatedCost) FROM TeqipPackage WHERE teqipCategorymaster=:teqipCategorymaster  and teqipStatemaster=:teqipStatemaster  and  packageId != :packageId ")
    public Double findSumOfEstimatedCostByCategoryIdAndTeqipStatemasterAndPackageIdNot(
            @Param("teqipCategorymaster") TeqipCategorymaster teqipCategorymaster,
            @Param("teqipStatemaster") TeqipStatemaster teqipStatemaster,
            @Param("packageId") Integer packageId);

    @Query("SELECT SUM(estimatedCost) FROM TeqipPackage "
            + " WHERE teqipCategorymaster=:teqipCategorymaster  "
            + " and teqipStatemaster is null "
            + " and teqipInstitution is null  "
            + " and typeofplanCreator=:typeofplanCreator "
            + " and  packageId != :packageId ")
    public Double findSumOfEstimatedCostByCategoryIdAndNPIUAndPackageIdNot(
            @Param("teqipCategorymaster") TeqipCategorymaster teqipCategorymaster,
            @Param("typeofplanCreator") Integer typeofplanCreator,
            @Param("packageId") Integer packageId );
    
    

    @Query("SELECT SUM(consumeFromWorks) FROM TeqipPackage WHERE teqipCategorymaster=:teqipCategorymaster  and teqipInstitution=:teqipInstitution")
    public Double findSumOfConsumeFromWorksByCategoryIdAndTeqipInstitution(@Param("teqipCategorymaster") TeqipCategorymaster teqipCategorymaster,
            @Param("teqipInstitution") TeqipInstitution teqipInstitution);

    @Query("SELECT SUM(consumeFromWorks) FROM TeqipPackage WHERE teqipCategorymaster=:teqipCategorymaster  and teqipStatemaster=:teqipStatemaster")
    public Double findSumOfConsumeFromWorksByCategoryIdAndTeqipStatemaster(@Param("teqipCategorymaster") TeqipCategorymaster teqipCategorymaster,
            @Param("teqipStatemaster") TeqipStatemaster teqipStatemaster);

    @Query("SELECT SUM(consumeFromWorks) FROM TeqipPackage "
            + " WHERE teqipCategorymaster=:teqipCategorymaster  "
            + " and teqipStatemaster is null "
            + " and teqipInstitution is null "
            + " and typeofplanCreator=:typeofplanCreator ")
    public Double findSumOfConsumeFromWorksByCategoryIdAndNPIU(
            @Param("teqipCategorymaster") TeqipCategorymaster teqipCategorymaster,
            @Param("typeofplanCreator") Integer typeofplanCreator
    );

    @Query("SELECT p FROM TeqipPackage p  "
            + " WHERE p.isCurrentlyInPriorReview=1 "
            + " and p.roleOfNextActor =:role"
            + " and p.teqipInstitution=:teqipInstitution ")
    public List<TeqipPackage> findPriorReview(@Param("teqipInstitution") TeqipInstitution teqipInstitution,
            @Param("role") String role);

    @Query(" SELECT p FROM TeqipPackage p  "
            + " WHERE p.isCurrentlyInPriorReview=1 "
            + " and p.roleOfNextActor =:role"
            + " and p.teqipInstitution is null "
            + " and p.teqipStatemaster=:teqipStatemaster")
    public List<TeqipPackage> findPriorReviewAllState(
              @Param("teqipStatemaster") TeqipStatemaster teqipStatemaster,
            @Param("role") String role);
    
    @Query(" SELECT p FROM TeqipPackage p "
            + " INNER JOIN p.teqipInstitution i "
            + " WHERE p.isCurrentlyInPriorReview=1"
            + " and p.roleOfNextActor =:role"
            + " and i.teqipStatemaster=:teqipStatemaster")
    public List<TeqipPackage> findPriorReviewAllStateInst(
              @Param("teqipStatemaster") TeqipStatemaster teqipStatemaster,
            @Param("role") String role);

     @Query(" SELECT p FROM TeqipPackage p  "
            + " WHERE p.isCurrentlyInPriorReview=1 "
             + " and p.roleOfNextActor =:role")
    public List<TeqipPackage> findPriorReviewAllNPIU(@Param("role") String role);

    public TeqipPackage findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeofplanCreatorOrderByPackageIdDesc(
            TeqipInstitution teqipInstitution,
            TeqipStatemaster teqipStatemaster,
            Integer typeofplanCreator);
    
   
}
