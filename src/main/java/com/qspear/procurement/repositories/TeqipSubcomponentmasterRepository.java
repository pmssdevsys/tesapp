package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface TeqipSubcomponentmasterRepository extends JpaRepository<TeqipSubcomponentmaster, Integer> {

	List<TeqipSubcomponentmaster> findByTeqipCategorymasterAndIsactive(
			TeqipCategorymaster teqipCategorymaster, 	Boolean isActive);
        	@Query(value="select * from  teqip_subcomponentmaster where COMPONENT_ID=:COMPONENT_ID and ISACTIVE=:ISACTIVE" ,nativeQuery=true)
	 List<TeqipSubcomponentmaster> getAllSubComonentById(@Param(value="COMPONENT_ID")Long COMPONENT_ID, @Param(value="ISACTIVE")Integer activeStatus);
	@Query(value="select * from  teqip_subcomponentmaster where COMPONENT_ID=:componentId and ISACTIVE=:activeStatus" ,nativeQuery=true)
	List<TeqipSubcomponentmaster> findSubcomponenetByCompid(@Param("componentId")Long componentId, @Param("activeStatus")Integer activeStatus);

}