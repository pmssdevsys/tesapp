package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipProcurementmethodTimeline;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface TeqipProcMethodTimelineRepository extends JpaRepository<TeqipProcurementmethodTimeline, String> {

    TeqipProcurementmethodTimeline findByTeqipProcurementmethodAndTeqipCategorymaster(TeqipProcurementmethod procMethod,
            TeqipCategorymaster catMaster);

    @Query(value = "SELECT t.BidDocumentPreparationDate_days,t.BankNOCForBiddingDocuments_days,t.BidInvitationDate_days,t.BidOpeningDate_days,t.ContractAwardDate_days,t.ContractCompletionDate_days,t.TORFinalizationDate_days,t.AdvertisementDate_days,t.FinalDraftToBeForwardedToTheBankDate_days,t.NoObjectionFromBankForRFP_days,t.RFPIssuedDate_days,t.LastDateToReceiveProposals_days,t.EvaluationDate_days,t.NoObjectionFromBankForEvaluation_days,i.PROCUREMENTMETHOD_CODE,i.PROCUREMENTMETHOD_NAME "
            + "FROM  teqip_procurementmethod_timeline t, teqip_procurementmethod i where  t.PROCUREMENTMETHOD_ID=i.PROCUREMENTMETHOD_ID AND i.PROCUREMENTMETHOD_ID=:PROCUREMENTMETHOD_ID", nativeQuery = true)
List<Object[]> findprocurementmethod(@Param("PROCUREMENTMETHOD_ID") Integer PROCUREMENTMETHOD_ID);

//    @Query(value = "SELECT t.BidDocumentPreparationDate_days,t.BankNOCForBiddingDocuments_days,t.BidInvitationDate_days,t.BidOpeningDate_days,t.ContractAwardDate_days,t.ContractCompletionDate_days,t.TORFinalizationDate_days,t.AdvertisementDate_days,t.FinalDraftToBeForwardedToTheBankDate_days,t.NoObjectionFromBankForRFP_days,t.RFPIssuedDate_days,t.LastDateToReceiveProposals_days,t.EvaluationDate_days,t.NoObjectionFromBankForEvaluation_days,i.PROCUREMENTMETHOD_CODE,i.PROCUREMENTMETHOD_NAME "
//            + "FROM  teqip_procurementmethod_timeline t, teqip_procurementmethod i where  t.PROCUREMENTMETHOD_ID=i.PROCUREMENTMETHOD_ID AND i.PROCUREMENTMETHOD_ID=:PROCUREMENTMETHOD_ID", nativeQuery = true)
//    List<Object[]> findById(@Param("PROCUREMENTMETHOD_ID") Integer PROCUREMENTMETHOD_ID);
}
