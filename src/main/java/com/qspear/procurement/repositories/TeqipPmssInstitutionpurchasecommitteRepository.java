package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPmssInstitutionpurchasecommitte;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface TeqipPmssInstitutionpurchasecommitteRepository extends JpaRepository<TeqipPmssInstitutionpurchasecommitte, Integer> {
     @Query(value="select c.isactive,c.institutionPurchaseCommitte_ID,c.CommitteMemberName,c.DesignationName,c.PcCommitteName ,c.committeetype from TEQIP_PMSS_InstitutionPurchaseCommitte c where c.committeetype=:code and c.ISACTIVE=1",nativeQuery=true)
		List<Object[]> getCommitteDetails(@Param("code")String code);
}
