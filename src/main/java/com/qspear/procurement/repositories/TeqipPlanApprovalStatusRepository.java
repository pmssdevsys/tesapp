package com.qspear.procurement.repositories;

import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPlanApprovalstatus;
import com.qspear.procurement.persistence.TeqipStatemaster;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TeqipPlanApprovalStatusRepository extends JpaRepository<TeqipPlanApprovalstatus, Integer> {

    public TeqipPlanApprovalstatus findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(
            TeqipInstitution teqipInstitution, TeqipStatemaster object,Integer type );

    @Transactional
    @Modifying
    @Query(nativeQuery = true,
            value = " UPDATE  teqip_plan_approvalstatus SET currentStage =0 "
            + " WHERE INSTITUTION_ID=:teqipInstitution ")
    public void updateInstCurrentStage(@Param("teqipInstitution") int institutionID);

    @Transactional
    @Modifying
    @Query(nativeQuery = true,
            value = " UPDATE  teqip_plan_approvalstatus SET currentStage =0 "
            + " WHERE  SPFUID =:teqipStatemaster")
    public void updateSPFUCurrentStage(@Param("teqipStatemaster") int stateId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true,
            value = " UPDATE  teqip_plan_approvalstatus SET currentStage =0 "
            + " WHERE INSTITUTION_ID IS NULL AND SPFUID IS NULL and CREATED_TYPE =:type")
    public void updateNpiuOrEdcilCurrentStage(@Param("type") Integer type);

    public List<TeqipPlanApprovalstatus> findAllByTeqipInstitutionAndTeqipStatemasterAndTypeOrderById(TeqipInstitution institute, TeqipStatemaster state,Integer type );

      public List<TeqipPlanApprovalstatus> findAllByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(TeqipInstitution institute, TeqipStatemaster state,Integer type );

}
