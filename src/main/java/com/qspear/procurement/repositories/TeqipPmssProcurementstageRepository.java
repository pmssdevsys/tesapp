package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import org.springframework.data.repository.query.Param;

@Repository
public interface TeqipPmssProcurementstageRepository extends JpaRepository<TeqipPmssProcurementstage, Integer> {

    @Query(nativeQuery = true,
            value = "select stageid"
            + " from teqip_pmss_procurementstages"
            + " where orderNo>:orderNo and ROLE_ID=:roleId and status='approved' and type=:type and  revision=:revision "
            + " ORDER BY orderNo limit 1")
    public Integer findNextApprovedStage(
            @Param("orderNo") int orderNo,
            @Param("type") int type,
            @Param("roleId") int roleId,
            @Param("revision") int revision
    );

    @Query(nativeQuery = true,
            value = "select stageid"
            + " from teqip_pmss_procurementstages "
            + " where orderNo<:orderNo and ROLE_ID=:roleId and  status='rejected' and type=:type and  revision=:revision "
            + " ORDER BY orderNo  DESC limit 1")
    public Integer findPreviousRejectedStage(
            @Param("orderNo") int orderNo,
            @Param("type") int type,
            @Param("roleId") int roleId,
            @Param("revision") int revision);

    @Query(nativeQuery = true,
            value = "select stageid"
            + " from teqip_pmss_procurementstages "
            + " where  status='approved' and ROLE_ID=:roleId and  type=:type and revision=:revision "
            + " ORDER BY orderNo limit 1 ")
    public Integer findFirstApprovedStage(
            @Param("type") int type,
            @Param("roleId") int roleId,
            @Param("revision") int revision);

    @Query(nativeQuery = true,
            value = "select stageid"
            + " from teqip_pmss_procurementstages "
            + " where   type=:type and  ROLE_ID=:roleId and revision=:revision "
            + " ORDER BY orderNo limit 1 ")
    public Integer findInitialApprovedStage(
            @Param("type") int type,
            @Param("roleId") int roleId,
            @Param("revision") int revision);

    @Query(nativeQuery = true,
            value = "select stageid"
            + " from teqip_pmss_procurementstages "
            + " where   type=:type and  ROLE_ID=:roleId and revision = 1 "
            + " ORDER BY orderNo limit 1")
    public Integer findInitialRevisionStage(
            @Param("type") int type,
            @Param("roleId") int roleId);

    public List<TeqipPmssProcurementstage> findByRoleIdAndTypeAndProcurementstageidIn(Integer roleId, int type, Integer[] id);

    public TeqipPmssProcurementstage findFirstByProcurementstageidAndRoleIdAndType(int stageID, int roleId, int type);

}
