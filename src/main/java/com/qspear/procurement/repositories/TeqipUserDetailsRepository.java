package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipUsersDetail;
@Repository
public interface TeqipUserDetailsRepository extends JpaRepository<TeqipUsersDetail, Long>{
	List<TeqipUsersDetail> findAll();
	 @Query("SELECT p FROM TeqipUsersDetail p WHERE  p.userName = :userName and p.isactive=:isactive")
	    public TeqipUsersDetail find(@Param("userName") String userName,@Param("isactive")Boolean status);
	 @Query("SELECT p FROM TeqipUsersDetail p WHERE  p.userName = :username and p.isactive=:isactive")
	TeqipUsersDetail findRoleByUser(String username);
	 @Query("SELECT p FROM TeqipUsersDetail p WHERE  p.email = :email and p.isactive=1")
	TeqipUsersDetail findByemail(@Param("email")String email);
	 @Query(value="SELECT tud.name,tud.middlename, tud.lname,tud.user_name,tud.dob,tud.gender, tud.email, tud.user_mobile,tud.address,tud.pincode,tud.state, tis.institution_name, trm.role_description FROM teqip_users_detail tud, teqip_users_master tum, teqip_institutions tis, teqip_rolemaster trm WHERE tud.userid=tum.user_id AND tis.institution_id=tum.institution_id AND tum.role_id=trm.role_id AND tud.user_name=:username and tud.isactive=1",nativeQuery=true)
		List<Object[]> findUserinfo(@Param("username")String username);
		 @Query("SELECT p FROM TeqipUsersDetail p WHERE  p.password = :oldPassword and p.isactive=1")
	    TeqipUsersDetail findByepassword(@Param("oldPassword")String oldPassword);
}
// and p.isactive=:isactive