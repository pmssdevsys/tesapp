package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qspear.procurement.persistence.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsername(String username);

    public User findByUserid(Integer createdBy);
}