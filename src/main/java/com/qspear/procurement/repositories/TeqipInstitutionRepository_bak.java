//package com.qspear.procurement.repositories;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import com.qspear.procurement.persistence.TeqipInstitution;
//import java.util.List;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//
//public interface TeqipInstitutionRepository_bak extends JpaRepository<TeqipInstitution, Integer> {
//
//    @Query(nativeQuery = true,
//            value = "select s.STATE_ID,s.STATE_NAME,p.sc ,p.co,p.A,p.B,p.C,p.D ,'INST'"
//            + " from teqip_statemaster s"
//            + " left join ("
//            + "	SELECT i.SPFUID, min(sm.SUBCOMPONENTCODE) as sc , count(*) as co, "
//            + "	SUM(pp.A) as A, SUM(pp.B) as B, SUM(pp.C) as C ,SUM(pp.D) as D"
//            + " 	FROM teqip_plan_approvalstatus  pas"
//            + "	INNER JOIN  teqip_pmss_procurementstages ps  on ps.STAGEID = pas.PROCUREMENTSTAGEID "
//            + "	INNER JOIN teqip_institutions i  ON i.INSTITUTION_ID  = pas.INSTITUTION_ID	"
//            + "	INNER JOIN teqip_institutionsubcomponentmapping g  ON g.INSTITUTION_ID=i.INSTITUTION_ID"
//            + "	INNER JOIN teqip_subcomponentmaster sm ON sm.ID =  g.SUBCOMPONENT_ID"
//            + "	INNER JOIN ("
//            + "		SELECT  pp.INSTITUTION_ID, "
//            + "         SUM(case when CATEGORY_ID =1  then ESTIMATED_COST end) as A,"
//            + " 		SUM(case when CATEGORY_ID =2  then ESTIMATED_COST end) as B,"
//            + " 		SUM(case when CATEGORY_ID =3  then ESTIMATED_COST end )as C,"
//            + " 		SUM(ESTIMATED_COST) as D"
//            + " 		FROM teqip_package pp  "
//            + "         WHERE pp.INSTITUTION_ID is not null "
//            + "         GROUP BY pp.INSTITUTION_ID) pp on  i.INSTITUTION_ID =  pp.INSTITUTION_ID"
//            + "	WHERE pas.currentStage =1 and ps.PROCUREMENTSTAGEID IN (:stage) "
//            + " 	 AND 1 = CASE WHEN 0 = :instId THEN 1 WHEN i.INSTITUTION_ID = :instId THEN 1 ELSE 0 END"
//            + "	group by  i.SPFUID"
//            + ") p on s.STATE_ID = p.SPFUID"
//    )
//    public List<Object[]> stateWiseApprovalInstPackage(@Param("stage") List<Integer> approvalStages, @Param("instId") int instId);
//
//    @Query(nativeQuery = true,
//            value = "SELECT  s.STATE_ID,s.STATE_NAME,'' as sc , count(*) as co, "
//            + "	SUM(pp.A) as A, SUM(pp.B) as B, SUM(pp.C) as C ,SUM(pp.D) as D, 'SPFU'"
//            + " from teqip_statemaster s"
//            + " inner join teqip_plan_approvalstatus  pas on s.STATE_ID = pas.SPFUID"
//            + "	INNER JOIN  teqip_pmss_procurementstages ps  on ps.STAGEID = pas.PROCUREMENTSTAGEID "
//            + "	INNER JOIN ("
//            + "		SELECT  pp.SPFUID, "
//            + "         SUM(case when CATEGORY_ID =1  then ESTIMATED_COST end) as A,"
//            + " 		SUM(case when CATEGORY_ID =2  then ESTIMATED_COST end) as B,"
//            + " 		SUM(case when CATEGORY_ID =3  then ESTIMATED_COST end )as C,"
//            + " 		SUM(ESTIMATED_COST) as D"
//            + " 		FROM teqip_package pp  "
//            + "         WHERE pp.SPFUID is not null "
//            + "         GROUP BY pp.SPFUID) pp on  pas.SPFUID =  pp.SPFUID"
//            + "	WHERE pas.currentStage =1 and ps.PROCUREMENTSTAGEID IN (:stage) "
//            + " AND 1 = CASE WHEN 0 = :stateId THEN 1 WHEN  pas.SPFUID = :stateId THEN 1 ELSE 0 end"
//            + " group by  s.STATE_ID")
//    public List<Object[]> stateWiseApprovalStatePackage(@Param("stage") List<Integer> approvalStages, @Param("stateId") int stateId);
//
//    @Query(nativeQuery = true,
//            value = " select 0,'National','' as sc , 	1 as co, "
//            + " SUM(pp.A) as A, SUM(pp.B) as B, SUM(pp.C) as C ,SUM(pp.D) as D, 'NPIU'"
//            + " from teqip_plan_approvalstatus pas"
//            + " INNER JOIN  teqip_pmss_procurementstages ps  on ps.STAGEID = pas.PROCUREMENTSTAGEID "
//            + " inner JOIN("
//            + "		 SELECT  "
//            + "         SUM(case when CATEGORY_ID =1  then ESTIMATED_COST end) as A,"
//            + " 		 SUM(case when CATEGORY_ID =2  then ESTIMATED_COST end) as B,"
//            + " 		 SUM(case when CATEGORY_ID =3  then ESTIMATED_COST end )as C,"
//            + " 		 SUM(ESTIMATED_COST) as D"
//            + " 		 FROM teqip_package pp  "
//            + "         WHERE pp.SPFUID is null and pp.INSTITUTION_ID is null"
//            + "         ) pp"
//            + " WHERE pas.currentStage =1 and ps.PROCUREMENTSTAGEID IN (:stage) "
//    )
//    public List<Object[]> stateWiseApprovalNationalPackage(@Param("stage") List<Integer> approvalStages);
//
//    @Query(nativeQuery = true,
//            value = "	SELECT "
//            + "	i.INSTITUTION_ID, "
//            + " i.INSTITUTION_NAME,"
//            + " sm.SUBCOMPONENTCODE, "
//            + " ps.STAGE_DESCRIPTION as stage,"
//            + " pm.END_DATE,"
//            + " pas.REVISEID ,"
//            + "	 pp.A as A, pp.B as B, pp.C as C ,pp.D as D,"
//            + "	 pas.PROCUREMENT_PLAN_ID,"
//            + "	'INST'"
//            + " FROM teqip_plan_approvalstatus  pas"
//            + " INNER JOIN teqip_procurementmaster  pm on pm.PROCUREMENTMASTER_ID  =  pas.PROCUREMENT_PLAN_ID"
//            + "	INNER JOIN  teqip_pmss_procurementstages ps  on ps.STAGEID = pas.PROCUREMENTSTAGEID "
//            + "	INNER JOIN teqip_institutions i  ON i.INSTITUTION_ID  = pas.INSTITUTION_ID	"
//            + "	INNER JOIN teqip_institutionsubcomponentmapping g  ON g.INSTITUTION_ID=i.INSTITUTION_ID"
//            + "	INNER JOIN teqip_subcomponentmaster sm ON sm.ID =  g.SUBCOMPONENT_ID"
//            + "	INNER JOIN ("
//            + "		SELECT  pp.INSTITUTION_ID, "
//            + "         SUM(case when CATEGORY_ID =1  then ESTIMATED_COST end) as A,"
//            + " 		SUM(case when CATEGORY_ID =2  then ESTIMATED_COST end) as B,"
//            + " 		SUM(case when CATEGORY_ID =3  then ESTIMATED_COST end )as C,"
//            + " 		SUM(ESTIMATED_COST) as D"
//            + " 		FROM teqip_package pp  "
//            + "         WHERE pp.INSTITUTION_ID is not null "
//            + "         GROUP BY pp.INSTITUTION_ID) pp on  i.INSTITUTION_ID =  pp.INSTITUTION_ID"
//            + "	WHERE pas.currentStage =1 and ps.PROCUREMENTSTAGEID IN (:stage) "
//            + " AND 1 = CASE WHEN 0 = :instId THEN 1 WHEN i.INSTITUTION_ID = :instId THEN 1 ELSE 0 END"
//            + "	and i.SPFUID=:stateId")
//    public List<Object[]> institutePackageApproval(@Param("stateId") int stateId,
//            @Param("stage") List<Integer> approvalStages, @Param("instId") int instId);
//
//    @Query(nativeQuery = true,
//            value ="	SELECT "
//            + "	i.INSTITUTION_ID, "
//            + " i.INSTITUTION_NAME,"
//            + " sm.SUBCOMPONENTCODE, "
//            + " ps.STAGE_DESCRIPTION as stage,"
//            + " pm.END_DATE,"
//            + " pas.REVISEID ,"
//            + "	 pp.A as A, pp.B as B, pp.C as C ,pp.D as D,"
//            + "	 pas.PROCUREMENT_PLAN_ID,"
//            + "	'INST'"
//            + " FROM teqip_plan_approvalstatus  pas"
//            + " INNER JOIN teqip_procurementmaster  pm on pm.PROCUREMENTMASTER_ID  =  pas.PROCUREMENT_PLAN_ID"
//            + "	INNER JOIN  teqip_pmss_procurementstages ps  on ps.STAGEID = pas.PROCUREMENTSTAGEID "
//            + "	INNER JOIN teqip_institutions i  ON i.INSTITUTION_ID  = pas.INSTITUTION_ID	"
//            + "	INNER JOIN teqip_institutionsubcomponentmapping g  ON g.INSTITUTION_ID=i.INSTITUTION_ID"
//            + "	INNER JOIN teqip_subcomponentmaster sm ON sm.ID =  g.SUBCOMPONENT_ID"
//            + "	INNER JOIN ("
//            + "		SELECT  pp.INSTITUTION_ID, "
//            + "         SUM(case when CATEGORY_ID =1  then ESTIMATED_COST end) as A,"
//            + " 		SUM(case when CATEGORY_ID =2  then ESTIMATED_COST end) as B,"
//            + " 		SUM(case when CATEGORY_ID =3  then ESTIMATED_COST end )as C,"
//            + " 		SUM(ESTIMATED_COST) as D"
//            + " 		FROM teqip_package pp  "
//            + "         WHERE pp.INSTITUTION_ID is not null "
//            + "         GROUP BY pp.INSTITUTION_ID) pp on  i.INSTITUTION_ID =  pp.INSTITUTION_ID"
//            + "	WHERE pas.currentStage =1 and ps.PROCUREMENTSTAGEID  NOT IN (:stage) "
//            + " AND 1 = CASE WHEN 0 = :instId THEN 1 WHEN i.INSTITUTION_ID = :instId THEN 1 ELSE 0 END"
//            + "	and i.SPFUID=:stateId")
//    public List<Object[]> institutePackageApprovalNotIn(@Param("stateId") int stateId, @Param("stage") List<Integer> approvalStages, @Param("instId") int instId);
//
//    @Query(nativeQuery = true,
//            value
//            = " select "
//            + " sm.STATE_ID, "
//            + " sm.STATE_NAME,"
//            + " '', "
//            + " ps.STAGE_DESCRIPTION as stage,"
//            + " pm.END_DATE,"
//            + " p.REVISEID,"
//            + " count(*),"
//            + " SUM(case when p.CATEGORY_ID =1  then ESTIMATED_COST ELSE 0 end) as A,"
//            + " SUM(case when p.CATEGORY_ID =2  then ESTIMATED_COST ELSE 0 end) as B,"
//            + " SUM(case when p.CATEGORY_ID =3  then ESTIMATED_COST ELSE 0 end )as C,"
//            + " SUM(p.ESTIMATED_COST) as D,"
//            + " p.PROCUREMENT_PLAN_ID,"
//            + " 'SPFU'"
//            + " FROM teqip_package p "
//            + " Inner JOIN teqip_pmss_procurementstages ps ON p.PROCUREMENTSTAGE_ID = ps.STAGEID"
//            + " INNER JOIN teqip_procurementmaster  pm on pm.PROCUREMENTMASTER_ID  =  p.PROCUREMENT_PLAN_ID"
//            + " INNER JOIN teqip_statemaster sm ON sm.STATE_ID =  p.SPFUID"
//            + " WHERE p.SPFUID = :stateId and ps.PROCUREMENTSTAGEID IN (:stage)"
//            + " AND 1 = CASE WHEN 0 = :loginStateId THEN 1 WHEN sm.STATE_ID = :loginStateId THEN 1 ELSE 0 END"
//            + " GROUP BY sm.STATE_ID, pm.PROCUREMENTMASTER_ID,ps.STAGEID,p.REVISEID")
//
//    public List<Object[]> statePackageApproval(@Param("stateId") int stateId, @Param("stage") List<Integer> approvalStages, @Param("loginStateId") int loginStateId);
//
//    @Query(nativeQuery = true,
//            value
//            = " select "
//            + " sm.STATE_ID, "
//            + " sm.STATE_NAME,"
//            + " '', "
//            + " ps.STAGE_DESCRIPTION as stage,"
//            + " pm.END_DATE,"
//            + " p.REVISEID,"
//            + " count(*),"
//            + " SUM(case when p.CATEGORY_ID =1  then ESTIMATED_COST ELSE 0 end) as A,"
//            + " SUM(case when p.CATEGORY_ID =2  then ESTIMATED_COST ELSE 0 end) as B,"
//            + " SUM(case when p.CATEGORY_ID =3  then ESTIMATED_COST ELSE 0 end )as C,"
//            + " SUM(p.ESTIMATED_COST) as D,"
//            + " p.PROCUREMENT_PLAN_ID,"
//            + " 'SPFU'"
//            + " FROM teqip_package p "
//            + " Inner JOIN teqip_pmss_procurementstages ps ON p.PROCUREMENTSTAGE_ID = ps.STAGEID"
//            + " INNER JOIN teqip_procurementmaster  pm on pm.PROCUREMENTMASTER_ID  =  p.PROCUREMENT_PLAN_ID"
//            + " INNER JOIN teqip_statemaster sm ON sm.STATE_ID =  p.SPFUID"
//            + " WHERE p.SPFUID = :stateId and ps.PROCUREMENTSTAGEID NOT IN (:stage)"
//            + " AND 1 = CASE WHEN 0 = :loginStateId THEN 1 WHEN sm.STATE_ID = :loginStateId THEN 1 ELSE 0 END"
//            + " GROUP BY sm.STATE_ID, pm.PROCUREMENTMASTER_ID,ps.STAGEID,p.REVISEID")
//
//    public List<Object[]> statePackageApprovalNotIn(@Param("stateId") int stateId, @Param("stage") List<Integer> approvalStages, @Param("loginStateId") int loginStateId);
//
//    @Query(nativeQuery = true,
//            value
//            = " select  h.INSTITUTION_ID, ins.INSTITUTION_NAME,"
//            + " h.SPFUID,sm.STATE_NAME, "
//            + " Min(h.CREATED_ON),"
//            + " h.SUB_COMPONENT_ID,sb.SUBCOMPONENTCODE, "
//            + " um.USER_NAME, h.REVISEID"
//            + " from teqip_package_rev_history h"
//            + " LEFT join  teqip_institutions ins on ins.INSTITUTION_ID = h.INSTITUTION_ID"
//            + " left join teqip_statemaster sm on sm.STATE_ID = h.SPFUID"
//            + " left join teqip_subcomponentmaster sb on sb.ID =  h.SUB_COMPONENT_ID"
//            + " left join teqip_users_detail um on um.USERID =  h.CREATED_BY"
//            + "  WHERE "
//            + "  1 = CASE WHEN 0 = :instituteId THEN 1 WHEN h.INSTITUTION_ID = :instituteId THEN 1 ELSE 0 END"
//            + "  AND  1 = CASE WHEN 0 = :stateId THEN 1 WHEN h.SPFUID = :stateId THEN 1 ELSE 0 END"
//            + " GROUP BY h.INSTITUTION_ID, ins.INSTITUTION_NAME,h.SPFUID,sm.STATE_NAME,h.SUB_COMPONENT_ID,sb.SUBCOMPONENTCODE,um.USER_NAME, h.REVISEID"
//    )
//    public List<Object[]> packageHistory(@Param("instituteId") int instituteId,
//            @Param("stateId") int stateId);
//
//}
