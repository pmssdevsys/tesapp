package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import com.qspear.procurement.persistence.TeqipStatemaster;

@Repository
public interface TeqipInstitutiondepartmentmappingRepository
        extends JpaRepository<TeqipInstitutiondepartmentmapping, Integer> {

    List<TeqipInstitutiondepartmentmapping> findByTeqipInstitutionAndTeqipStatemasterAndType(
            TeqipInstitution teqipInstitution, TeqipStatemaster teqipStatemaster, Integer type);
}
