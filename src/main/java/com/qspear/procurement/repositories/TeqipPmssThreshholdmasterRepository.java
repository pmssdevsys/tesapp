package com.qspear.procurement.repositories;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPmssThreshholdmaster;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface TeqipPmssThreshholdmasterRepository extends JpaRepository<TeqipPmssThreshholdmaster, Integer> {
	/*List<TeqipPmssThreshholdmaster> findByThresholdMinValueGreaterThanEqualAndThresholdMaxValueLessThanEqualAndIsactiveAndIsproprietary(
			Double thresholdMinValue, Double thresholdMaxValue, Boolean isActive, Integer isproprietary);*/

//	List<TeqipPmssThreshholdmaster> findByIsactiveAndIsproprietary(Boolean isActive, Integer isproprietary);

    public List<TeqipPmssThreshholdmaster> findByIsactiveAndIsproprietaryAndTeqipCategorymaster(Boolean isActive,
            Integer isproprietary, TeqipCategorymaster teqipCategorymaster);

    public List<TeqipPmssThreshholdmaster> findByIsactiveAndIsproprietaryAndIsgemAndTeqipCategorymaster(Boolean isActive, Integer isproprietary, Integer isgem, TeqipCategorymaster teqipCategorymaster);
    @Query(value="SELECT * FROM TEQIP_PMSS_THRESHHOLDMASTER i,TEQIP_CATEGORYMASTER c ,TEQIP_PROCUREMENTMETHOD p where  i.procurementmethodid=p.PROCUREMENTMETHOD_ID AND i.CATEGORYID=c.CAT_ID  order by c.CATEGORY_NAME desc",nativeQuery=true)	
	public List<TeqipPmssThreshholdmaster> findThreshholdMasterByActiveStatus();
}
