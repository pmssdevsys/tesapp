package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qspear.procurement.persistence.NpiuDetailsMaster;

public interface NpiuDetailsMasterRepo extends JpaRepository<NpiuDetailsMaster,Integer>{
	
    @Query(value="select n.ID, n.Code,n.Name,n.ProjectName,n.CreditNumber,n.EmailID,n.Address,n.ProjectCost,n.FaxNumber,n.AllocatedBudget from teqip_npiumaster n where n.ID=:id",nativeQuery=true)
	List<Object[]> findNpiuDetails(@Param("id")Integer id);
	@Query(value="select n.Code from teqip_npiumaster n where n.ID=:id",nativeQuery=true)
	String findNpiuCode(@Param("id")Integer id);

}
