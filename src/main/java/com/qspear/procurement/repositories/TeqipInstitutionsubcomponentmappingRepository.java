package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipInstitutionsubcomponentmapping;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;

@Repository
public interface TeqipInstitutionsubcomponentmappingRepository
        extends JpaRepository<TeqipInstitutionsubcomponentmapping, Integer> {

    TeqipInstitutionsubcomponentmapping findFirstByTeqipInstitutionAndTeqipStatemasterAndType(TeqipInstitution teqipInstitution,TeqipStatemaster teqipStatemaster ,Integer type);

    TeqipInstitutionsubcomponentmapping findFirstByTeqipInstitutionAndTeqipStatemasterAndTeqipSubcomponentmasterAndIsactive(
            TeqipInstitution teqipInstitution,  TeqipStatemaster teqipStatemaster, TeqipSubcomponentmaster teqipSubcomponentmaster, Boolean isActive);


}
