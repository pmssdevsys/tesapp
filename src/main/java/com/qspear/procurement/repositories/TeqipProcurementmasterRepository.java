package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qspear.procurement.persistence.TeqipProcurementmaster;

public interface TeqipProcurementmasterRepository extends JpaRepository<TeqipProcurementmaster, Integer> {

}
