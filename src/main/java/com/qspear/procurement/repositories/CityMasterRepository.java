package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipCitymaster;
import com.qspear.procurement.persistence.TeqipStatemaster;

@Repository
public interface CityMasterRepository extends JpaRepository<TeqipCitymaster, Integer> {

    public List<TeqipCitymaster> findByStatemaster(TeqipStatemaster statemaster);
    @Query(value="select * from TEQIP_CITYMASTER c where c.stateId=:stid",nativeQuery=true)
   	public List<TeqipCitymaster> findCity(@Param("stid")Integer stid);

}
