package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qspear.procurement.persistence.UserAttempts;

public interface UserAttemptRepository extends JpaRepository<UserAttempts,Integer>{

	UserAttempts findByUsername(String userName);

}
