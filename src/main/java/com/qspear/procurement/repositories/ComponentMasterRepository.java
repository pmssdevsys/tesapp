package com.qspear.procurement.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipComponentmaster;

@Repository
public interface ComponentMasterRepository extends JpaRepository<TeqipComponentmaster, Integer> {
	
@Query(value="select * from teqip_componentmaster c where c.ISACTIVE=1 order by c.ID asc ", nativeQuery=true )
	List<TeqipComponentmaster> findAllComponents();

	
}