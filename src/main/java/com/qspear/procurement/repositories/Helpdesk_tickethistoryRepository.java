package com.qspear.procurement.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qspear.procurement.model.TeqipHelpdeskTicketModel;
import com.qspear.procurement.persistence.Helpdesk_tickethistory;


public interface Helpdesk_tickethistoryRepository extends JpaRepository<Helpdesk_tickethistory,Integer>{

	@Query(value="SELECT ht.*,lk.LookUpName FROM mis.helpdesk_tickethistory ht,cnf_lookup lk where  lk.id=ht.HDnTicketStatusId  and ht.HDnTicketId=:HDnTicketId",nativeQuery=true)
	List<TeqipHelpdeskTicketModel> getHelpDeskViewForStatusAndComment(@Param("HDnTicketId")Integer HDnTicketId);

	@Transactional
	@Modifying
		@Query("delete from Helpdesk_tickethistory  where HdnTicketId=:id")
public void deleteTicketHistory(@Param("id")Integer id);
	
	@Query(value="SELECT th.* ,us.NAME,td.ticketType FROM teqip_helpdesk_ticket t,teqip_users_detail us,teqip_helpdesk_ticketdetails  td,teqip_helpdesk_tickethistory th where th.HdnTicketId=t.HDnTicketId and us.USERID=t.HDnLocationId and th.HdnTicketStatusId=td.id and t.HdnTicketId=:id",nativeQuery=true)
  public List<Object[]> getAlStatusAndComment(@Param("id") Integer id);
}
