package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import java.util.ArrayList;
import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<TeqipCategorymaster, Integer> {
	@Query("select master.categoryName,master.catId from TeqipCategorymaster master where master.isactive=:active ORDER BY catId DESC")
	Iterable<Object[]> findByIsactive(@Param("active") Boolean active);
        @Query(value="select * from teqip_categorymaster c ",nativeQuery=true)
	ArrayList<TeqipCategorymaster> findAllCategory();
        @Query(value="select * from teqip_categorymaster c where c.CAT_ID=:catId",nativeQuery=true)
	TeqipCategorymaster findpmsscatbyid(@Param("catId")Integer catId);
	@Query("Select c from TeqipCategorymaster c where c.isactive = :isactive")
	List<TeqipCategorymaster> getCategoryMasterData(@Param("isactive")Boolean activeStatus);
	 @Query("SELECT t FROM TeqipCategorymaster t where t.catId=:teqipCategorymasterId")
	TeqipCategorymaster findCategoryMasterById(@Param("teqipCategorymasterId")Integer teqipCategorymasterId);

	    @Query("Select c from TeqipCategorymaster c where c.isactive = :isactive")	  
	List<TeqipCategorymaster> getCategoryMasterDataByActiveStatus(@Param("isactive")Boolean isActive);
}
