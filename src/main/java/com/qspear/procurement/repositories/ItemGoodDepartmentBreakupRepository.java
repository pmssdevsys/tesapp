package com.qspear.procurement.repositories;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipItemGoodsDepartmentBreakup;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ItemGoodDepartmentBreakupRepository extends JpaRepository<TeqipItemGoodsDepartmentBreakup, Integer> {

}
