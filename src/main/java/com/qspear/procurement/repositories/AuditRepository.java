package com.qspear.procurement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.qspear.procurement.persistence.TrnAuditTrial;

public interface AuditRepository extends JpaRepository<TrnAuditTrial,Integer>{

	@Query(value="SELECT * FROM edcil.teqip_users_audit_trial where session=:token",nativeQuery=true)
	public TrnAuditTrial expireToken(@Param("token") String token);
}
