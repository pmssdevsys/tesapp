package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageLetterAcceptance;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface LetterOfAcceptanceRepository extends JpaRepository<TeqipPackageLetterAcceptance, Integer> {

    public TeqipPackageLetterAcceptance findByTeqipPackage(TeqipPackage teqipPackage);

}
