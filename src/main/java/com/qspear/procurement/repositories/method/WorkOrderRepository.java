package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrder;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface WorkOrderRepository extends JpaRepository<TeqipPackageWorkOrder, Integer> {

    public TeqipPackageWorkOrder findByTeqipPackage(TeqipPackage teqipPackage);

}
