package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageConsultantEvaluation;
import com.qspear.procurement.persistence.methods.TeqipPackageTechSubCriteria;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ConsultantEvaluationRepository extends JpaRepository<TeqipPackageConsultantEvaluation, Integer> {

    public List<TeqipPackageConsultantEvaluation> findByTeqipPackage(TeqipPackage entity);

    public List<TeqipPackageConsultantEvaluation> findByTeqipPackageTechSubCriteria(TeqipPackageTechSubCriteria entity);

}
