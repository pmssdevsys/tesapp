package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageSupplierRepository extends JpaRepository<TeqipPackageSupplierDetail, Integer> {

    public TeqipPackageSupplierDetail findByTeqipPackageAndTeqipPmssSupplierMaster(TeqipPackage teqipPackage, TeqipPmssSupplierMaster teqipPmssSupplierMaster);

}
