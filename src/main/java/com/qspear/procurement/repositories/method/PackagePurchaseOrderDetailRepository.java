package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrderDetail;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackagePurchaseOrderDetailRepository extends JpaRepository<TeqipPackagePurchaseOrderDetail, Integer> {

    public List<TeqipPackagePurchaseOrderDetail> findByTeqipPackage(TeqipPackage teqipPackage);

}
