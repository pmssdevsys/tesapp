//package com.qspear.procurement.repositories.method;
//
//import java.util.List;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//import com.qspear.procurement.persistence.TeqipInstitution;
//import com.qspear.procurement.persistence.TeqipPackage;
//import com.qspear.procurement.persistence.TeqipStatemaster;
//import com.qspear.procurement.persistence.methods.TeqipPackageSpecific;
//import org.springframework.data.repository.CrudRepository;
//
//@Repository
//public interface PackageRepositorySpecific extends CrudRepository<TeqipPackageSpecific, Integer> {
//
//}
