package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAmendmentdetail;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AmendmentdetailRepository extends JpaRepository<TeqipPackageAmendmentdetail, Integer> {

    public TeqipPackageAmendmentdetail findByTeqipPackage(TeqipPackage teqipPackage);

}
