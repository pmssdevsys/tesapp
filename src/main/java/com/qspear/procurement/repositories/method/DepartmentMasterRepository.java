package com.qspear.procurement.repositories.method;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface DepartmentMasterRepository extends JpaRepository<TeqipPmssDepartmentMaster, Integer> {

}
