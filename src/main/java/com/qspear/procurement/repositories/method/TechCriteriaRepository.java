package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface TechCriteriaRepository extends JpaRepository<TeqipPackageTechCriteria, Integer> {

    public ArrayList<TeqipPackageTechCriteria> findByTeqipPackage(TeqipPackage teqipPackage);

}
