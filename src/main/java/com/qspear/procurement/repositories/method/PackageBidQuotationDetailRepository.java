package com.qspear.procurement.repositories.method;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageBidQuotationDetail;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageBidQuotationDetailRepository extends JpaRepository<TeqipPackageBidQuotationDetail, Integer> {

}
