package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsEvaluationItemDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageQuotationsEvaluationItemDetailRepository extends JpaRepository<TeqipPackageQuotationsEvaluationItemDetail, Integer> {

    public List<TeqipPackageQuotationsEvaluationItemDetail> findByTeqipPackage(TeqipPackage entity);

    public List<TeqipPackageQuotationsEvaluationItemDetail> findByTeqipPackageAndTeqipPmssSupplierMasterAndTeqipItemMaster(TeqipPackage teqipPackage, TeqipPmssSupplierMaster teqipPmssSupplierMaster, TeqipItemMaster teqipItemMaster);

}
