package com.qspear.procurement.repositories.method;

import com.qspear.procurement.model.methods.Questions;
import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageQuestionMaster;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageQuestionRepository extends JpaRepository<TeqipPackageQuestionMaster, Integer> {

    public List<TeqipPackageQuestionMaster> findByTeqipPackageAndQuestionCategory(TeqipPackage packageDetail, String quotation_opening);
    
    public TeqipPackageQuestionMaster findByTeqipPackageAndQuestionCategoryAndType(TeqipPackage packageDetail, String quotation_opening,String type);

    public TeqipPackageQuestionMaster findFirstByTeqipPackageAndIsreadOut(TeqipPackage teqipPackage, int isreadOut);
    
    public TeqipPackageQuestionMaster findFirstByTeqipPackageAndIsevalatedPrice(TeqipPackage teqipPackage, int isevalatedPrice);

    public TeqipPackageQuestionMaster findByTeqipPackageAndQuestionDescAndQuestionCategory(TeqipPackage entity, Questions question, String questCategory);

    public List<TeqipPackageQuestionMaster> findByTeqipPackageAndTypeAndQuestionCategory(TeqipPackage teqipPackage, String radio_quotation_received, String opening);

}
