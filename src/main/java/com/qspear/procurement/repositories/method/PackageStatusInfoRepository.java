package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageStage;
import com.qspear.procurement.persistence.methods.TeqipPackageStageIndex;
import com.qspear.procurement.persistence.methods.TeqipPackageStatusInfo;
import java.util.List;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageStatusInfoRepository extends JpaRepository<TeqipPackageStatusInfo, Integer> {

    public TeqipPackageStatusInfo findByTeqipPackage(TeqipPackage teqipPackage);


}
