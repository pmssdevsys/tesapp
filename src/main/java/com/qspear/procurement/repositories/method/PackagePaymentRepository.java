package com.qspear.procurement.repositories.method;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackagePaymentDetail;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackagePaymentRepository extends JpaRepository<TeqipPackagePaymentDetail, Integer> {

}
