package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDocument;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface RFPDocumentRepository extends JpaRepository<TeqipPackageRFPDocument, Integer> {

    public TeqipPackageRFPDocument findByTeqipPackage(TeqipPackage teqipPackage);

}
