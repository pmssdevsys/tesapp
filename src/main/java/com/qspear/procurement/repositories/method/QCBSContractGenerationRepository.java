package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractGeneration;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface QCBSContractGenerationRepository extends JpaRepository<TeqipPackageQCBSContractGeneration, Integer> {

    public TeqipPackageQCBSContractGeneration findByTeqipPackage(TeqipPackage teqipPackage);

}
