package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageInvitationLetter;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface InviationLetterRepository extends JpaRepository<TeqipPackageInvitationLetter, Integer> {

    public TeqipPackageInvitationLetter findByTeqipPackage(TeqipPackage teqipPackage);

}
