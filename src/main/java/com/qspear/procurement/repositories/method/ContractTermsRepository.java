package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageContractTerms;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ContractTermsRepository extends JpaRepository<TeqipPackageContractTerms, Integer> {

    public TeqipPackageContractTerms findByTeqipPackage(TeqipPackage teqipPackage);

}
