package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageQuotationDetail;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageQuotationDetailRepository extends JpaRepository<TeqipPackageQuotationDetail, Integer> {

    public TeqipPackageQuotationDetail findByTeqipPackage(TeqipPackage entity);

}
