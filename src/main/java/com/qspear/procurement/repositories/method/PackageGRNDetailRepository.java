package com.qspear.procurement.repositories.method;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageGRNDetail;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageGRNDetailRepository extends JpaRepository<TeqipPackageGRNDetail, Integer> {

}
