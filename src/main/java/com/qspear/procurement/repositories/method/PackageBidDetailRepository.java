package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageBidDetail;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageBidDetailRepository extends JpaRepository<TeqipPackageBidDetail, Integer> {

    public TeqipPackageBidDetail findByTeqipPackage(TeqipPackage teqipPackage);

}
