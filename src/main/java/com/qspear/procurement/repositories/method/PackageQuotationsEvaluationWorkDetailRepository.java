package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsEvaluationWorkDetail;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageQuotationsEvaluationWorkDetailRepository extends JpaRepository<TeqipPackageQuotationsEvaluationWorkDetail, Integer> {

    public List<TeqipPackageQuotationsEvaluationWorkDetail> findByTeqipPackage(TeqipPackage entity);

}
