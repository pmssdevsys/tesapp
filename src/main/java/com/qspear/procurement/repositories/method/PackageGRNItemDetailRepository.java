package com.qspear.procurement.repositories.method;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDetail;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageGRNItemDetailRepository extends JpaRepository<TeqipPackageGRNItemDetail, Integer> {

}
