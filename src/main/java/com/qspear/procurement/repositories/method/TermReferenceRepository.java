package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageTermReference;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface TermReferenceRepository extends JpaRepository<TeqipPackageTermReference, Integer> {

    public TeqipPackageTermReference findByTeqipPackage(TeqipPackage teqipPackage);

}
