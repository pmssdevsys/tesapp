package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageCompleteCheckList;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageCompleteChecklistRepository extends JpaRepository<TeqipPackageCompleteCheckList, Integer> {

    public TeqipPackageCompleteCheckList findByTeqipPackage(TeqipPackage teqipPackage);

}
