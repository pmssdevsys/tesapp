package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageBidAdvertisement;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageBidAdvertisementRepository extends JpaRepository<TeqipPackageBidAdvertisement, Integer> {

    public TeqipPackageBidAdvertisement findByTeqipPackage(TeqipPackage teqipPackage);

}
