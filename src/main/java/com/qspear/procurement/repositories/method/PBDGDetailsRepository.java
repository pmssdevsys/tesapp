package com.qspear.procurement.repositories.method;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackagePBGDDetails;
@Repository
public interface PBDGDetailsRepository extends JpaRepository<TeqipPackagePBGDDetails,Integer>{

	 TeqipPackagePBGDDetails findByTeqipPackage(TeqipPackage teqipPackage) ;
		
	

}
