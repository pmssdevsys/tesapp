package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAmendmentdetail;
import com.qspear.procurement.persistence.methods.TeqipPackagePriorReview;
import java.util.List;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PriorReviewRepository extends JpaRepository<TeqipPackagePriorReview, Integer> {

    public TeqipPackagePriorReview findByTeqipPackageAndStageName(TeqipPackage teqipPackage, String stageName);

    public List<TeqipPackagePriorReview> findByTeqipPackage(TeqipPackage teqipPackage);


}
