package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageL1SupplierDetails;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface L1SupplierRepository extends JpaRepository<TeqipPackageL1SupplierDetails, Integer> {

    public TeqipPackageL1SupplierDetails findByTeqipPackage(TeqipPackage teqipPackage);

}
