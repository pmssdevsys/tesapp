package com.qspear.procurement.repositories.method;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageContractSanctionOrder;
@Repository
public interface ContractSanctionOrderRepository extends JpaRepository<TeqipPackageContractSanctionOrder,Integer>{

	TeqipPackageContractSanctionOrder findByTeqipPackage(TeqipPackage teqipPackage);

}
