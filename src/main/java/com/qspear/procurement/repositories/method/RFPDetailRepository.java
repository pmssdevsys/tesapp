package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDetail;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface RFPDetailRepository extends JpaRepository<TeqipPackageRFPDetail, Integer> {

    public TeqipPackageRFPDetail findByTeqipPackage(TeqipPackage teqipPackage);

}
