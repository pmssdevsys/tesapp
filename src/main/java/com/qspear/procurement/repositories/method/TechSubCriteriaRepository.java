package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.persistence.methods.TeqipPackageTechSubCriteria;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface TechSubCriteriaRepository extends JpaRepository<TeqipPackageTechSubCriteria, Integer> {

    public ArrayList<TeqipPackageTechSubCriteria> findByTeqipPackageAndTeqipPackageTechCriteria(TeqipPackage teqipPackage, TeqipPackageTechCriteria techCriteria);

}
