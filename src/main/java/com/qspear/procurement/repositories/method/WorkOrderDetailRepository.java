package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderDetail;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface WorkOrderDetailRepository extends JpaRepository<TeqipPackageWorkOrderDetail, Integer> {

    public List<TeqipPackageWorkOrderDetail> findByTeqipPackage(TeqipPackage teqipPackage);

}
