package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageIssueRFP;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface IssueRFPRepository extends JpaRepository<TeqipPackageIssueRFP, Integer> {

    public TeqipPackageIssueRFP findByTeqipPackage(TeqipPackage teqipPackage);

}
