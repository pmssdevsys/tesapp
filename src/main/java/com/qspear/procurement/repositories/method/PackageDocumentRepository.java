package com.qspear.procurement.repositories.method;


import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import java.util.List;

@Repository
public interface PackageDocumentRepository extends JpaRepository<TeqipPackageDocument, Integer> {

    public List<TeqipPackageDocument> findByTeqipPackageAndDocumentCategory(TeqipPackage teqipPackage, String type);

}
