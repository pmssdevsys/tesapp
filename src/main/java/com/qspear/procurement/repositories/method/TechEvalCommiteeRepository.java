package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageTechEvalCommitee;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface TechEvalCommiteeRepository extends JpaRepository<TeqipPackageTechEvalCommitee, Integer> {

    public ArrayList<TeqipPackageTechEvalCommitee> findByTeqipPackage(TeqipPackage teqipPackage);

}
