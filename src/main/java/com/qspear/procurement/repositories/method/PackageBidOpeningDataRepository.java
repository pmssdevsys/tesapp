package com.qspear.procurement.repositories.method;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageBidOpeningData;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageBidOpeningDataRepository extends JpaRepository<TeqipPackageBidOpeningData, Integer> {

}
