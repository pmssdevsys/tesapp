package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageFinancialOpeningData;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface FinancialOpeningDataRepository extends JpaRepository<TeqipPackageFinancialOpeningData, Integer> {

    public ArrayList<TeqipPackageFinancialOpeningData> findByTeqipPackage(TeqipPackage teqipPackage);

}
