package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAmendmentdetail;
import com.qspear.procurement.persistence.methods.TeqipPackageExtension;
import java.util.List;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ExtensionRepository extends JpaRepository<TeqipPackageExtension, Integer> {

    public List<TeqipPackageExtension> findByTeqipPackage(TeqipPackage teqipPackage);

}
