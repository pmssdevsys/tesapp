package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageStage;
import com.qspear.procurement.persistence.methods.TeqipPackageStageIndex;

import java.util.List;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageStageIndexRepository extends JpaRepository<TeqipPackageStageIndex, Integer> {

    public TeqipPackageStageIndex findTopByTeqipPackageAndCategoryOrderByPackageStageIndexIdDesc(TeqipPackage teqipPackage, String category);

     public List<TeqipPackageStageIndex> findByTeqipPackageAndCategory(TeqipPackage teqipPackage,  String category);

}
