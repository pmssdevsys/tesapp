package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageQuestionMaster;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import java.util.List;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageQuotationsSupplierInputRepository extends JpaRepository<TeqipPackageQuotationsSupplierInput, Integer> {

    public List<TeqipPackageQuotationsSupplierInput> findByTeqipPackageAndTeqipPmssSupplierMaster(TeqipPackage entity, TeqipPmssSupplierMaster teqipPmssSupplierMaster);

    public List<TeqipPackageQuotationsSupplierInput> findByTeqipPackage(TeqipPackage entity);

    public TeqipPackageQuotationsSupplierInput findFirstByTeqipPackageAndTeqipPackageQuestionMasterAndTeqipPmssSupplierMaster(TeqipPackage teqipPackage, TeqipPackageQuestionMaster question, TeqipPmssSupplierMaster supplier);

    public List<TeqipPackageQuotationsSupplierInput> findByTeqipPackageAndTeqipPackageQuestionMasterAndUserResponse(TeqipPackage teqipPackage, TeqipPackageQuestionMaster get, String yes);

}
