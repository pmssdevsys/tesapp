package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackagePurchaseOrderRepository extends JpaRepository<TeqipPackagePurchaseOrder, Integer> {

    public TeqipPackagePurchaseOrder findByTeqipPackage(TeqipPackage teqipPackage);

}
