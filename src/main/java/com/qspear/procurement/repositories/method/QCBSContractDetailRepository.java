package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractDetail;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface QCBSContractDetailRepository extends JpaRepository<TeqipPackageQCBSContractDetail, Integer> {

    public TeqipPackageQCBSContractDetail findByTeqipPackage(TeqipPackage teqipPackage);

}
