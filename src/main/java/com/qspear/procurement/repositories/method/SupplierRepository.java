package com.qspear.procurement.repositories.method;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface SupplierRepository extends JpaRepository<TeqipPmssSupplierMaster, Integer> {

    public List<TeqipPmssSupplierMaster> findByPmssType(int type);

    public List<TeqipPmssSupplierMaster> findByPmssTypeAndSupplierCity(int type, String city);

    public List<TeqipPmssSupplierMaster> findByPmssTypeAndSupplierState(int type, String state);

    public List<TeqipPmssSupplierMaster> findByPmssTypeAndSupplierStateAndSupplierCity(int type, String state, String city);

    @Query(value = " FROM  TeqipPmssSupplierMaster WHERE suplierEmailId = :emailId ")
    public List<TeqipPmssSupplierMaster> findSupplierByEmail(@Param("emailId") String emailId);

    @Query(value = " FROM  TeqipPmssSupplierMaster WHERE supplierPhoneno = :phoneNo  ")
    public List<TeqipPmssSupplierMaster> findSupplierByPhone(@Param("phoneNo") String phoneNo);

    @Query(value = " FROM  TeqipPmssSupplierMaster WHERE suplierPanNumber = :panNo ")
    public List<TeqipPmssSupplierMaster> findSupplierByPan(@Param("panNo") String panNo);

    @Query(value = " FROM  TeqipPmssSupplierMaster WHERE gstNo = :gstNo ")
    public List<TeqipPmssSupplierMaster> findSupplierByGstNo(@Param("gstNo") String gstNo);

    @Query(value = " FROM  TeqipPmssSupplierMaster WHERE suplierEmailId = :emailId  and supplierId !=:supplierId ")
    public List<TeqipPmssSupplierMaster> findSupplierByEmailAndNotSupplierId(@Param("emailId") String emailId, @Param("supplierId") Integer supplierId);

    @Query(value = " FROM  TeqipPmssSupplierMaster WHERE supplierPhoneno = :phoneNo and supplierId !=:supplierId  ")
    public List<TeqipPmssSupplierMaster> findSupplierByPhoneAndNotSupplierId(@Param("phoneNo") String phoneNo, @Param("supplierId") Integer supplierId);

    @Query(value = " FROM  TeqipPmssSupplierMaster WHERE suplierPanNumber = :panNo  and supplierId !=:supplierId")
    public List<TeqipPmssSupplierMaster> findSupplierByPanAndNotSupplierId(@Param("panNo") String panNo, @Param("supplierId") Integer supplierId);

    @Query(value = " FROM  TeqipPmssSupplierMaster WHERE gstNo = :gstNo  and supplierId !=:supplierId")
    public List<TeqipPmssSupplierMaster> findSupplierByGstNoAndNotSupplierId(@Param("gstNo") String gstNo, @Param("supplierId") Integer supplierId);

}
