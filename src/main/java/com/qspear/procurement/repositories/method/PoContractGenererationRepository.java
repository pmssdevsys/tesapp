package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackagePoContractGenereration;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PoContractGenererationRepository extends JpaRepository<TeqipPackagePoContractGenereration, Integer> {

    public TeqipPackagePoContractGenereration findByTeqipPackage(TeqipPackage teqipPackage);

}
