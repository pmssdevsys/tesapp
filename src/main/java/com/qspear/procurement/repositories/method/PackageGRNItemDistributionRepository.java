package com.qspear.procurement.repositories.method;

import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDistribution;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageGRNItemDistributionRepository extends JpaRepository<TeqipPackageGRNItemDistribution, Integer> {

}
