package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageBidMeetingDetail;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageBidMeetingDetailRepository extends JpaRepository<TeqipPackageBidMeetingDetail, Integer> {

    public TeqipPackageBidMeetingDetail findByTeqipPackage(TeqipPackage teqipPackage);

}
