package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface WorkOrderCertificateRepository extends JpaRepository<TeqipPackageWorkOrderCertificate, Integer> {

    List<TeqipPackageWorkOrderCertificate> findByTeqipPackage(TeqipPackage teqipPackage);

}
