package com.qspear.procurement.repositories.method;

import com.qspear.procurement.persistence.TeqipPackage;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.persistence.methods.TeqipPackageBidINSTAndSCC;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PackageBidINSTAndSCCRepository extends JpaRepository<TeqipPackageBidINSTAndSCC, Integer> {

    public TeqipPackageBidINSTAndSCC findByTeqipPackage(TeqipPackage teqipPackage);

}
