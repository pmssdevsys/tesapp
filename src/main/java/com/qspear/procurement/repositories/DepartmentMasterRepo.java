package com.qspear.procurement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.qspear.procurement.persistence.DepartmentMaster;

public interface DepartmentMasterRepo extends JpaRepository<DepartmentMaster,Integer> {
    @Query(value="select * from teqip_pmssdepartmentmaster m where m.ISACTIVE=1",nativeQuery=true)
	List<DepartmentMaster> findAllDept();

}
