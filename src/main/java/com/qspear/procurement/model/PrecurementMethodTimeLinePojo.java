package com.qspear.procurement.model;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.Teqip_procurement_revisedtimeline;
import com.qspear.procurement.persistence.Teqip_procurementmethod_Usertimeline;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PrecurementMethodTimeLinePojo {

    private String procurementmethodcode;
    private String procurementmethodname;
    private Integer categoryId;
    private Integer packageId;
    private Integer procurementmethodId;
    private Integer procurementmethodtimelineId;
    private Integer procurementmethodrevisedId;
    private Date bidDocumentationPreprationDatedays;
    private Date bankNOCForBiddingDocumentsdays;
    private Date bidInvitationDatedays;
    private Date bidOpeningDatedays;
    private Date tORFinalizationDatedays;
    private Date advertisementDatedays;
    private Date finalDraftToBeForwardedToTheBankDatedays;
    private Date noObjectionFromBankForRFPdays;
    private Date rFPIssuedDatedays;
    private Date lastDateToReceiveProposalsdays;
    private Date AdvertisementDate_WithoutBankNOC_days;
    private Date evaluationDatedays;
    private Date noObjectionFromBankForEvaluationdays;
    private Date contractCompletionDatedays;
    private Date contractAwardDatedays;
    private Date tORFinalizationDate_WithoutBankNOCdays;
    private Date rFPIssuedDate_WithoutBankNOCdays;
    private Date evaluationDate_WithoutBankNOCdays;
    private Date contractAwardDate_WithoutBankNOCdays;
    private Date contractCompletionDate_WithoutBankNOC_days;
    private Integer createdBy;
    private Timestamp createdOn;
    private Integer modifiedBy;
    private Date modifiedOn;
    private Date lastdateofsubmissionofEOI;
    private Date shortlistingOfEOI;
    private Date rFPApprovalDate;
    private Date financialEvaluationDate;
    private Date packageInitatedate;
    private String purchasetype;
    private Date proposalInvitationDate;
    private Date proposalSubmissionDate;

    public PrecurementMethodTimeLinePojo() {
    }

    public PrecurementMethodTimeLinePojo(
            Teqip_procurement_revisedtimeline reviseTimeLine,
            Teqip_procurementmethod_Usertimeline procUserTimeLine,
            TeqipTimelineModel teqipTimeline,
            TeqipPackage teqipPackage) {

        this.setPackageId(teqipPackage.getPackageId());
        this.setPurchasetype(teqipPackage.getPurchaseType());
        if (teqipPackage.getTeqipCategorymaster() != null) {
            this.setCategoryId(teqipPackage.getTeqipCategorymaster().getCatId());
        }

        if (teqipPackage.getTeqipProcurementmethod() != null) {
            this.setProcurementmethodId(teqipPackage.getTeqipProcurementmethod().getProcurementmethodId());
            this.setProcurementmethodcode(teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode());
            this.setProcurementmethodname(teqipPackage.getTeqipProcurementmethod().getProcurementmethodName());
        }

        if (reviseTimeLine != null) {
            this.setProcurementmethodrevisedId(reviseTimeLine.getProcurementmethodrevisedtimelineId());
            this.setAdvertisementDate_WithoutBankNOC_days(reviseTimeLine.getAdvertisementDate_WithoutBankNOC_days());
            this.setAdvertisementDatedays(reviseTimeLine.getAdvertisementDatedays());
            this.setBankNOCForBiddingDocumentsdays(reviseTimeLine.getBankNOCForBiddingDocumentsdays());
            this.setBidDocumentationPreprationDatedays(reviseTimeLine.getBidDocumentationPreprationDatedays());
            this.setBidInvitationDatedays(reviseTimeLine.getBidInvitationDatedays());
            this.setBidOpeningDatedays(reviseTimeLine.getBidOpeningDatedays());
            this.setContractAwardDate_WithoutBankNOCdays(reviseTimeLine.getContractAwardDate_WithoutBankNOCdays());
            this.setContractAwardDatedays(reviseTimeLine.getContractAwardDatedays());
            this.setContractCompletionDate_WithoutBankNOC_days(reviseTimeLine.getContractCompletionDate_WithoutBankNOC_days());
            this.setContractCompletionDatedays(reviseTimeLine.getContractCompletionDatedays());
            this.setCreatedBy(reviseTimeLine.getCreatedBy());
            this.setCreatedOn(reviseTimeLine.getCreatedOn());
            this.setEvaluationDate_WithoutBankNOCdays(reviseTimeLine.getEvaluationDate_WithoutBankNOCdays());
            this.setEvaluationDatedays(reviseTimeLine.getEvaluationDatedays());
            this.setFinalDraftToBeForwardedToTheBankDatedays(reviseTimeLine.getFinalDraftToBeForwardedToTheBankDatedays());
            this.setLastDateToReceiveProposalsdays(reviseTimeLine.getLastDateToReceiveProposalsdays());
            this.setModifiedBy(reviseTimeLine.getModifiedBy());
            this.setModifiedOn(reviseTimeLine.getModifiedOn());
            this.setNoObjectionFromBankForEvaluationdays(reviseTimeLine.getNoObjectionFromBankForEvaluationdays());
            this.setNoObjectionFromBankForRFPdays(reviseTimeLine.getNoObjectionFromBankForRFPdays());
            this.setrFPIssuedDate_WithoutBankNOCdays(reviseTimeLine.getrFPIssuedDate_WithoutBankNOCdays());
            this.setrFPIssuedDatedays(reviseTimeLine.getrFPIssuedDatedays());
            this.settORFinalizationDate_WithoutBankNOCdays(reviseTimeLine.gettORFinalizationDate_WithoutBankNOCdays());
            this.settORFinalizationDatedays(reviseTimeLine.gettORFinalizationDatedays());
            this.setLastdateofsubmissionofEOI(reviseTimeLine.getLastdateofsubmissionofEOI());
            this.setShortlistingOfEOI(reviseTimeLine.getShortlistingOfEOI());
            this.setFinancialEvaluationDate(reviseTimeLine.getFinancialEvaluationDate());
            this.setrFPApprovalDate(reviseTimeLine.getrFPApprovalDate());
            this.setProposalInvitationDate(reviseTimeLine.getProposalInvitationDate());
            this.setProposalSubmissionDate(reviseTimeLine.getProposalSubmissionDate());

        }
        if (procUserTimeLine != null) {
            //if(this.getProcurementmethodtimelineId()==null){
            //	this.setProcurementmethodtimelineId(procUserTimeLine.getTeqip_procurementmethod_UsertimelineId());
            //}
            if (this.getAdvertisementDate_WithoutBankNOC_days() == null) {
                this.setAdvertisementDate_WithoutBankNOC_days(procUserTimeLine.getAdvertisementDate_WithoutBankNOC_days());
            }
            if (this.getAdvertisementDatedays() == null) {
                this.setAdvertisementDatedays(procUserTimeLine.getAdvertisementDatedays());
            }
            if (this.getBankNOCForBiddingDocumentsdays() == null) {
                this.setBankNOCForBiddingDocumentsdays(procUserTimeLine.getBankNOCForBiddingDocumentsdays());
            }

            if (this.getBidDocumentationPreprationDatedays() == null) {
                this.setBidDocumentationPreprationDatedays(procUserTimeLine.getBidDocumentationPreprationDatedays());
            }
            if (this.getBidInvitationDatedays() == null) {
                this.setBidInvitationDatedays(procUserTimeLine.getBidInvitationDatedays());
            }

            if (this.getBidOpeningDatedays() == null) {
                this.setBidOpeningDatedays(procUserTimeLine.getBidOpeningDatedays());
            }
            if (this.getContractAwardDate_WithoutBankNOCdays() == null) {
                this.setContractAwardDate_WithoutBankNOCdays(procUserTimeLine.getContractAwardDate_WithoutBankNOCdays());
            }
            if (this.getContractAwardDatedays() == null) {
                this.setContractAwardDatedays(procUserTimeLine.getContractAwardDatedays());
            }
            if (this.getContractCompletionDate_WithoutBankNOC_days() == null) {
                this.setContractCompletionDate_WithoutBankNOC_days(procUserTimeLine.getContractCompletionDate_WithoutBankNOC_days());
            }
            if (this.getContractCompletionDatedays() == null) {
                this.setContractCompletionDatedays(procUserTimeLine.getContractCompletionDatedays());
            }
            if (this.getCreatedBy() == null) {
                this.setCreatedBy(procUserTimeLine.getCreatedBy());
            }
            if (this.getCreatedOn() == null) {
                this.setCreatedOn(procUserTimeLine.getCreatedOn());
            }
            if (this.getEvaluationDate_WithoutBankNOCdays() == null) {
                this.setEvaluationDate_WithoutBankNOCdays(procUserTimeLine.getEvaluationDate_WithoutBankNOCdays());
            }
            if (this.getEvaluationDatedays() == null) {
                this.setEvaluationDatedays(procUserTimeLine.getEvaluationDatedays());
            }
            if (this.getFinalDraftToBeForwardedToTheBankDatedays() == null) {
                this.setFinalDraftToBeForwardedToTheBankDatedays(procUserTimeLine.getFinalDraftToBeForwardedToTheBankDatedays());
            }
            if (this.getLastDateToReceiveProposalsdays() == null) {
                this.setLastDateToReceiveProposalsdays(procUserTimeLine.getLastDateToReceiveProposalsdays());
            }
            if (this.getModifiedBy() == null) {
                this.setModifiedBy(procUserTimeLine.getModifiedBy());
            }
            if (this.getModifiedOn() == null) {
                this.setModifiedOn(procUserTimeLine.getModifiedOn());
            }
            if (this.getNoObjectionFromBankForEvaluationdays() == null) {
                this.setNoObjectionFromBankForEvaluationdays(procUserTimeLine.getNoObjectionFromBankForEvaluationdays());
            }
            if (this.getNoObjectionFromBankForRFPdays() == null) {
                this.setNoObjectionFromBankForRFPdays(procUserTimeLine.getNoObjectionFromBankForRFPdays());
            }
            if (this.getrFPIssuedDate_WithoutBankNOCdays() == null) {
                this.setrFPIssuedDate_WithoutBankNOCdays(procUserTimeLine.getrFPIssuedDate_WithoutBankNOCdays());
            }
            if (this.getrFPIssuedDatedays() == null) {
                this.setrFPIssuedDatedays(procUserTimeLine.getrFPIssuedDatedays());
            }
            if (this.gettORFinalizationDate_WithoutBankNOCdays() == null) {
                this.settORFinalizationDate_WithoutBankNOCdays(procUserTimeLine.gettORFinalizationDate_WithoutBankNOCdays());
            }
            if (this.gettORFinalizationDatedays() == null) {
                this.settORFinalizationDatedays(procUserTimeLine.gettORFinalizationDatedays());
            }
            if (this.getLastdateofsubmissionofEOI() == null) {
                this.setLastdateofsubmissionofEOI(procUserTimeLine.getLastdateofsubmissionofEOI());
            }
            if (this.getShortlistingOfEOI() == null) {
                this.setShortlistingOfEOI(procUserTimeLine.getShortlistingOfEOI());
            }
            if (this.getFinancialEvaluationDate() == null) {
                this.setFinancialEvaluationDate(procUserTimeLine.getFinancialEvaluationDate());
            }
            if (this.getrFPApprovalDate() == null) {
                this.setrFPApprovalDate(procUserTimeLine.getrFPApprovalDate());
            }

            if (this.getProposalInvitationDate() == null) {
                this.setProposalInvitationDate(procUserTimeLine.getProposalInvitationDate());
            }
            if (this.getProposalSubmissionDate() == null) {
                this.setProposalSubmissionDate(procUserTimeLine.getProposalSubmissionDate());
            }
        }

        if (teqipTimeline != null) {
            if (this.getAdvertisementDate_WithoutBankNOC_days() == null) {
                this.setAdvertisementDate_WithoutBankNOC_days(getDate(teqipTimeline.getAdvertisementDate_WithoutBankNOC_days()));
            }
            if (this.getAdvertisementDatedays() == null) {
                this.setAdvertisementDatedays(getDate(teqipTimeline.getAdvertisementDate_days()));
            }
            if (this.getBankNOCForBiddingDocumentsdays() == null) {
                this.setBankNOCForBiddingDocumentsdays(getDate(teqipTimeline.getBankNOCForBiddingDocuments_days()));
            }
            if (this.getBidDocumentationPreprationDatedays() == null) {
                this.setBidDocumentationPreprationDatedays(getDate(teqipTimeline.getBidDocumentPreparationDate_days()));
            }
            if (this.getBidInvitationDatedays() == null) {
                this.setBidInvitationDatedays(getDate(teqipTimeline.getBidInvitationDate_days()));
            }
            if (this.getBidOpeningDatedays() == null) {
                this.setBidOpeningDatedays(getDate(teqipTimeline.getBidOpeningDate_days()));
            }
            if (this.getContractAwardDate_WithoutBankNOCdays() == null) {
                this.setContractAwardDate_WithoutBankNOCdays(getDate(teqipTimeline.getContractAwardDate_WithoutBankNOC_days()));
            }
            if (this.getContractAwardDatedays() == null) {
                this.setContractAwardDatedays(getDate(teqipTimeline.getContractAwardDate_days()));
            }
            if (this.getContractCompletionDate_WithoutBankNOC_days() == null) {
                this.setContractCompletionDate_WithoutBankNOC_days(getDate(teqipTimeline.getContractCompletionDate_WithoutBankNOC_days()));
            }
            if (this.getContractCompletionDatedays() == null) {
                this.setContractCompletionDatedays(getDate(teqipTimeline.getContractCompletionDate_days()));
            }
            if (this.getEvaluationDate_WithoutBankNOCdays() == null) {
                this.setEvaluationDate_WithoutBankNOCdays(getDate(teqipTimeline.getEvaluationDate_WithoutBankNOC_days()));
            }
            if (this.getEvaluationDatedays() == null) {
                this.setEvaluationDatedays(getDate(teqipTimeline.getEvaluationDate_days()));
            }
            if (this.getFinalDraftToBeForwardedToTheBankDatedays() == null) {
                this.setFinalDraftToBeForwardedToTheBankDatedays(getDate(teqipTimeline.getFinalDraftToBeForwardedToTheBankDate_days()));
            }
            if (this.getLastDateToReceiveProposalsdays() == null) {
                this.setLastDateToReceiveProposalsdays(getDate(teqipTimeline.getLastDateToReceiveProposals_days()));
            }
            if (this.getNoObjectionFromBankForEvaluationdays() == null) {
                this.setNoObjectionFromBankForEvaluationdays(getDate(teqipTimeline.getNoObjectionFromBankForEvaluation_days()));
            }
            if (this.getNoObjectionFromBankForRFPdays() == null) {
                this.setNoObjectionFromBankForRFPdays(getDate(teqipTimeline.getNoObjectionFromBankForRFP_days()));
            }
            if (this.getrFPIssuedDate_WithoutBankNOCdays() == null) {
                this.setrFPIssuedDate_WithoutBankNOCdays(getDate(teqipTimeline.getRFPIssuedDate_WithoutBankNOC_days()));
            }
            if (this.getrFPIssuedDatedays() == null) {
                this.setrFPIssuedDatedays(getDate(teqipTimeline.getRFPIssuedDate_days()));
            }
            if (this.gettORFinalizationDate_WithoutBankNOCdays() == null) {
                this.settORFinalizationDate_WithoutBankNOCdays(getDate(teqipTimeline.getTORFinalizationDate_WithoutBankNOC_days()));
            }
            if (this.gettORFinalizationDatedays() == null) {
                this.settORFinalizationDatedays(getDate(teqipTimeline.getTORFinalizationDate_days()));
            }
            if (this.getLastdateofsubmissionofEOI() == null) {
                this.setLastdateofsubmissionofEOI(getDate(teqipTimeline.getLastdateofsubmissionofEOI()));
            }
            if (this.getShortlistingOfEOI() == null) {
                this.setShortlistingOfEOI(getDate(teqipTimeline.getShortlistingOfEOI()));
            }
            if (this.getFinancialEvaluationDate() == null) {
                this.setFinancialEvaluationDate(getDate(teqipTimeline.getFinancialEvaluationDate()));
            }
            if (this.getrFPApprovalDate() == null) {
                this.setrFPApprovalDate(getDate(teqipTimeline.getrFPApprovalDate()));
            }
        }

    }

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private Date getDate(String str) {
        try {
            return dateFormat.parse(str);
        } catch (Exception e) {
        }
        return null;
    }

    public String getPurchasetype() {
        return purchasetype;
    }

    public void setPurchasetype(String purchasetype) {
        this.purchasetype = purchasetype;
    }

    public void setProcurementmethodtimelineId(Integer procurementmethodtimelineId) {
        this.procurementmethodtimelineId = procurementmethodtimelineId;
    }

    public Date getLastdateofsubmissionofEOI() {
        return lastdateofsubmissionofEOI;
    }

    public void setLastdateofsubmissionofEOI(Date lastdateofsubmissionofEOI) {
        this.lastdateofsubmissionofEOI = lastdateofsubmissionofEOI;
    }

    public Date getShortlistingOfEOI() {
        return shortlistingOfEOI;
    }

    public void setShortlistingOfEOI(Date shortlistingOfEOI) {
        this.shortlistingOfEOI = shortlistingOfEOI;
    }

    public Date getrFPApprovalDate() {
        return rFPApprovalDate;
    }

    public void setrFPApprovalDate(Date rFPApprovalDate) {
        this.rFPApprovalDate = rFPApprovalDate;
    }

    public Date getFinancialEvaluationDate() {
        return financialEvaluationDate;
    }

    public void setFinancialEvaluationDate(Date financialEvaluationDate) {
        this.financialEvaluationDate = financialEvaluationDate;
    }

    public Date getContractCompletionDate_WithoutBankNOC_days() {
        return contractCompletionDate_WithoutBankNOC_days;
    }

    public void setContractCompletionDate_WithoutBankNOC_days(Date contractAwardDate_WithoutBankNOCdays) {
        this.contractCompletionDate_WithoutBankNOC_days = contractCompletionDate_WithoutBankNOC_days;
    }

    public Integer getProcurementmethodId() {
        return procurementmethodId;
    }

    public void setProcurementmethodId(Integer procurementmethodId) {
        this.procurementmethodId = procurementmethodId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getProcurementmethodcode() {
        return procurementmethodcode;
    }

    public void setProcurementmethodcode(String procurementmethodcode) {
        this.procurementmethodcode = procurementmethodcode;
    }

    public String getProcurementmethodname() {
        return procurementmethodname;
    }

    public void setProcurementmethodname(String procurementmethodname) {
        this.procurementmethodname = procurementmethodname;
    }

    public Integer getProcurementmethodtimelineId() {
        return procurementmethodtimelineId;
    }

    public void setProcurementmethodtimeline(Integer procurementmethodtimelineId) {
        this.procurementmethodtimelineId = procurementmethodtimelineId;
    }

    public Date getBidDocumentationPreprationDatedays() {
        return bidDocumentationPreprationDatedays;
    }

    public void setBidDocumentationPreprationDatedays(Date bidDocumentationPreprationDatedays) {
        this.bidDocumentationPreprationDatedays = bidDocumentationPreprationDatedays;
    }

    public Date getBankNOCForBiddingDocumentsdays() {
        return bankNOCForBiddingDocumentsdays;
    }

    public void setBankNOCForBiddingDocumentsdays(Date bankNOCForBiddingDocumentsdays) {
        this.bankNOCForBiddingDocumentsdays = bankNOCForBiddingDocumentsdays;
    }

    public Date getBidInvitationDatedays() {
        return bidInvitationDatedays;
    }

    public void setBidInvitationDatedays(Date bidInvitationDatedays) {
        this.bidInvitationDatedays = bidInvitationDatedays;
    }

    public Date getBidOpeningDatedays() {
        return bidOpeningDatedays;
    }

    public void setBidOpeningDatedays(Date bidOpeningDatedays) {
        this.bidOpeningDatedays = bidOpeningDatedays;
    }

    public Date gettORFinalizationDatedays() {
        return tORFinalizationDatedays;
    }

    public void settORFinalizationDatedays(Date tORFinalizationDatedays) {
        this.tORFinalizationDatedays = tORFinalizationDatedays;
    }

    public Date getAdvertisementDatedays() {
        return advertisementDatedays;
    }

    public void setAdvertisementDatedays(Date advertisementDatedays) {
        this.advertisementDatedays = advertisementDatedays;
    }

    public Date getFinalDraftToBeForwardedToTheBankDatedays() {
        return finalDraftToBeForwardedToTheBankDatedays;
    }

    public void setFinalDraftToBeForwardedToTheBankDatedays(Date finalDraftToBeForwardedToTheBankDatedays) {
        this.finalDraftToBeForwardedToTheBankDatedays = finalDraftToBeForwardedToTheBankDatedays;
    }

    public Date getNoObjectionFromBankForRFPdays() {
        return noObjectionFromBankForRFPdays;
    }

    public void setNoObjectionFromBankForRFPdays(Date noObjectionFromBankForRFPdays) {
        this.noObjectionFromBankForRFPdays = noObjectionFromBankForRFPdays;
    }

    public Date getrFPIssuedDatedays() {
        return rFPIssuedDatedays;
    }

    public void setrFPIssuedDatedays(Date rFPIssuedDatedays) {
        this.rFPIssuedDatedays = rFPIssuedDatedays;
    }

    public Date getLastDateToReceiveProposalsdays() {
        return lastDateToReceiveProposalsdays;
    }

    public void setLastDateToReceiveProposalsdays(Date lastDateToReceiveProposalsdays) {
        this.lastDateToReceiveProposalsdays = lastDateToReceiveProposalsdays;
    }

    public Date getAdvertisementDate_WithoutBankNOC_days() {
        return AdvertisementDate_WithoutBankNOC_days;
    }

    public void setAdvertisementDate_WithoutBankNOC_days(Date advertisementDate_WithoutBankNOC_days) {
        AdvertisementDate_WithoutBankNOC_days = advertisementDate_WithoutBankNOC_days;
    }

    public Date getEvaluationDatedays() {
        return evaluationDatedays;
    }

    public void setEvaluationDatedays(Date evaluationDatedays) {
        this.evaluationDatedays = evaluationDatedays;
    }

    public Date getNoObjectionFromBankForEvaluationdays() {
        return noObjectionFromBankForEvaluationdays;
    }

    public void setNoObjectionFromBankForEvaluationdays(Date noObjectionFromBankForEvaluationdays) {
        this.noObjectionFromBankForEvaluationdays = noObjectionFromBankForEvaluationdays;
    }

    public Date getContractCompletionDatedays() {
        return contractCompletionDatedays;
    }

    public void setContractCompletionDatedays(Date contractCompletionDatedays) {
        this.contractCompletionDatedays = contractCompletionDatedays;
    }

    public Date getContractAwardDatedays() {
        return contractAwardDatedays;
    }

    public void setContractAwardDatedays(Date contractAwardDatedays) {
        this.contractAwardDatedays = contractAwardDatedays;
    }

    public Date gettORFinalizationDate_WithoutBankNOCdays() {
        return tORFinalizationDate_WithoutBankNOCdays;
    }

    public void settORFinalizationDate_WithoutBankNOCdays(Date tORFinalizationDate_WithoutBankNOCdays) {
        this.tORFinalizationDate_WithoutBankNOCdays = tORFinalizationDate_WithoutBankNOCdays;
    }

    public Date getrFPIssuedDate_WithoutBankNOCdays() {
        return rFPIssuedDate_WithoutBankNOCdays;
    }

    public void setrFPIssuedDate_WithoutBankNOCdays(Date rFPIssuedDate_WithoutBankNOCdays) {
        this.rFPIssuedDate_WithoutBankNOCdays = rFPIssuedDate_WithoutBankNOCdays;
    }

    public Date getEvaluationDate_WithoutBankNOCdays() {
        return evaluationDate_WithoutBankNOCdays;
    }

    public void setEvaluationDate_WithoutBankNOCdays(Date evaluationDate_WithoutBankNOCdays) {
        this.evaluationDate_WithoutBankNOCdays = evaluationDate_WithoutBankNOCdays;
    }

    public Date getContractAwardDate_WithoutBankNOCdays() {
        return contractAwardDate_WithoutBankNOCdays;
    }

    public void setContractAwardDate_WithoutBankNOCdays(Date contractAwardDate_WithoutBankNOCdays) {
        this.contractAwardDate_WithoutBankNOCdays = contractAwardDate_WithoutBankNOCdays;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Integer getProcurementmethodrevisedId() {
        return procurementmethodrevisedId;
    }

    public void setProcurementmethodrevisedId(Integer procurementmethodrevisedId) {
        this.procurementmethodrevisedId = procurementmethodrevisedId;
    }

    public Date getPackageInitatedate() {
        return packageInitatedate;
    }

    public void setPackageInitatedate(Date packageInitatedate) {
        this.packageInitatedate = packageInitatedate;
    }

    public Date getProposalInvitationDate() {
        return proposalInvitationDate;
    }

    public void setProposalInvitationDate(Date proposalInvitationDate) {
        this.proposalInvitationDate = proposalInvitationDate;
    }

    public Date getProposalSubmissionDate() {
        return proposalSubmissionDate;
    }

    public void setProposalSubmissionDate(Date proposalSubmissionDate) {
        this.proposalSubmissionDate = proposalSubmissionDate;
    }

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(SimpleDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

}
