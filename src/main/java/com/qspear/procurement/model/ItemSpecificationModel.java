package com.qspear.procurement.model;

public class ItemSpecificationModel {

    private Integer itemId;

    private String description;

    private String itemName;

    private String specification;

    private Integer categoryId;

    private Integer subcategoryId;

    public ItemSpecificationModel() {
    }
    
    

    public ItemSpecificationModel(Integer itemId, String description, String itemName, String specification, Integer categoryId, Integer subcategoryId) {
        this.itemId = itemId;
        this.description = description;
        this.itemName = itemName;
        this.specification = specification;
        this.categoryId = categoryId;
        this.subcategoryId = subcategoryId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

}
