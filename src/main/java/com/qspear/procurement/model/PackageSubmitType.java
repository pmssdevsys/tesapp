package com.qspear.procurement.model;

public enum PackageSubmitType {
	
	APPROVED, REJECTED, REVISION;
	
}
