/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model;

import java.math.BigInteger;

/**
 *
 * @author jaspreet
 */
public class StateWiseApproval {

    Integer stateId;
    String stateName;
    String subComponent;
    BigInteger totalInstitute;
    Double totalCivilWork;
    Double totalGoods;
    Double totalService;
    Double totalEstimate;
    BigInteger planId;
    String roleType;

    public StateWiseApproval(Integer stateId,
            String stateName,
            String subComponent,
            BigInteger totalInstitute,
            Double totalCivilWork,
            Double totalGoods,
            Double totalService,
            Double totalEstimate,
            String roleType) {
        this.stateId = stateId;
        this.stateName = stateName;
        this.subComponent = subComponent;
        this.totalInstitute = totalInstitute;
        this.totalCivilWork = totalCivilWork;
        this.totalGoods = totalGoods;
        this.totalService = totalService;
        this.totalEstimate = totalEstimate;
        this.roleType = roleType;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public BigInteger getTotalInstitute() {
        return totalInstitute;
    }

    public void setTotalInstitute(BigInteger totalInstitute) {
        this.totalInstitute = totalInstitute;
    }

    public Double getTotalCivilWork() {
        return totalCivilWork;
    }

    public void setTotalCivilWork(Double totalCivilWork) {
        this.totalCivilWork = totalCivilWork;
    }

    public Double getTotalGoods() {
        return totalGoods;
    }

    public void setTotalGoods(Double totalGoods) {
        this.totalGoods = totalGoods;
    }

    public Double getTotalService() {
        return totalService;
    }

    public void setTotalService(Double totalService) {
        this.totalService = totalService;
    }

    public String getSubComponent() {
        return subComponent;
    }

    public void setSubComponent(String subComponent) {
        this.subComponent = subComponent;
    }

    public Double getTotalEstimate() {
        return totalEstimate;
    }

    public void setTotalEstimate(Double totalEstimate) {
        this.totalEstimate = totalEstimate;
    }

    public BigInteger getPlanId() {
        return planId;
    }

    public void setPlanId(BigInteger planId) {
        this.planId = planId;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    
}
