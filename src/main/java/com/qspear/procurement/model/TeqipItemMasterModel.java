package com.qspear.procurement.model;

import java.util.Date;
import java.util.List;

/*@Getter
@Setter
@ToString
@NoArgsConstructor*/
public class TeqipItemMasterModel {

	private Integer itemId;

	private Integer createdBy;

	private String description;

	private String itemName;

	private Integer modifyBy;

	private Date modifyOn;

	private Boolean status;
	public TeqipItemMasterModel(Integer teqipCategorymasterId, Integer teqipSubcategorymasterId, String catname,
			String subcatname) {
		super();
		this.teqipCategorymasterId = teqipCategorymasterId;
		this.teqipSubcategorymasterId = teqipSubcategorymasterId;
		this.catname = catname;
		this.subcatname = subcatname;
	}

	public TeqipItemMasterModel() {
		super();
	}

	private List<TeqipItemGoodsDetailModel> teqipItemGoodsDetails;

	private Integer teqipCategorymasterId;

	private Integer teqipSubcategorymasterId;
        
        private String catname;
	private String subcatname;

	public String getCatname() {
		return catname;
	}

	public void setCatname(String catname) {
		this.catname = catname;
	}

	public String getSubcatname() {
		return subcatname;
	}

	public void setSubcatname(String subcatname) {
		this.subcatname = subcatname;
	}

	public Boolean getStatus() {
		return status;
	}
	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(Integer modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyOn() {
		return modifyOn;
	}

	public void setModifyOn(Date modifyOn) {
		this.modifyOn = modifyOn;
	}

	public Boolean isStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public List<TeqipItemGoodsDetailModel> getTeqipItemGoodsDetails() {
		return teqipItemGoodsDetails;
	}

	public void setTeqipItemGoodsDetails(List<TeqipItemGoodsDetailModel> teqipItemGoodsDetails) {
		this.teqipItemGoodsDetails = teqipItemGoodsDetails;
	}

	public Integer getTeqipCategorymasterId() {
		return teqipCategorymasterId;
	}

	public void setTeqipCategorymasterId(Integer teqipCategorymasterId) {
		this.teqipCategorymasterId = teqipCategorymasterId;
	}

	public Integer getTeqipSubcategorymasterId() {
		return teqipSubcategorymasterId;
	}

	public void setTeqipSubcategorymasterId(Integer teqipSubcategorymasterId) {
		this.teqipSubcategorymasterId = teqipSubcategorymasterId;
	}
}