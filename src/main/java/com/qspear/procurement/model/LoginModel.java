package com.qspear.procurement.model;

public class LoginModel {
	int status;
    private String loginMessage;
    private String token;
    Object data;

    public LoginModel(int status, String loginMessage, String token, Object data) {
		super();
		this.status = status;
		this.loginMessage = loginMessage;
		this.token = token;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLoginMessage() {
        return loginMessage;
    }

    public void setLoginMessage(String loginMessage) {
        this.loginMessage = loginMessage;
    }
}
