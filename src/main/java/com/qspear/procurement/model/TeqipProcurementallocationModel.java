package com.qspear.procurement.model;

public class TeqipProcurementallocationModel {
	private Integer procurementallocationId;

	private Integer budgetId;

	private Double goodsPercentage;

	private Double otherPercentage;

	private Double planAmount;

	private Double servicePercentage;

	private Double worksPercentage;

	private Integer teqipProcurementmasterId;

	public Integer getProcurementallocationId() {
		return procurementallocationId;
	}

	public void setProcurementallocationId(Integer procurementallocationId) {
		this.procurementallocationId = procurementallocationId;
	}

	public Integer getBudgetId() {
		return budgetId;
	}

	public void setBudgetId(Integer budgetId) {
		this.budgetId = budgetId;
	}

	public Double getGoodsPercentage() {
		return goodsPercentage;
	}

	public void setGoodsPercentage(Double goodsPercentage) {
		this.goodsPercentage = goodsPercentage;
	}

	public Double getOtherPercentage() {
		return otherPercentage;
	}

	public void setOtherPercentage(Double otherPercentage) {
		this.otherPercentage = otherPercentage;
	}

	public Double getPlanAmount() {
		return planAmount;
	}

	public void setPlanAmount(Double planAmount) {
		this.planAmount = planAmount;
	}

	public Double getServicePercentage() {
		return servicePercentage;
	}

	public void setServicePercentage(Double servicePercentage) {
		this.servicePercentage = servicePercentage;
	}

	public Double getWorksPercentage() {
		return worksPercentage;
	}

	public void setWorksPercentage(Double worksPercentage) {
		this.worksPercentage = worksPercentage;
	}

	public Integer getTeqipProcurementmasterId() {
		return teqipProcurementmasterId;
	}

	public void setTeqipProcurementmasterId(Integer teqipProcurementmasterId) {
		this.teqipProcurementmasterId = teqipProcurementmasterId;
	}
}
