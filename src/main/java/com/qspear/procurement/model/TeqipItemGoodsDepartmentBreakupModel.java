package com.qspear.procurement.model;

import java.sql.Timestamp;

public class TeqipItemGoodsDepartmentBreakupModel {

    private Integer id;

    private Double quantity;

    private Integer createdBy;

    private Timestamp createdOn;

    private Integer modifyBy;

    private Timestamp modifyOn;

    private Integer teqipItemGoodsDetailId;
    private String teqipItemGoodsDetail;

    private Integer teqipPmssDepartmentMasterId;
    private String teqipPmssDepartmentMaster;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Timestamp getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Timestamp modifyOn) {
        this.modifyOn = modifyOn;
    }

    public Integer getTeqipItemGoodsDetailId() {
        return teqipItemGoodsDetailId;
    }

    public void setTeqipItemGoodsDetailId(Integer teqipItemGoodsDetailId) {
        this.teqipItemGoodsDetailId = teqipItemGoodsDetailId;
    }

    public Integer getTeqipPmssDepartmentMasterId() {
        return teqipPmssDepartmentMasterId;
    }

    public void setTeqipPmssDepartmentMasterId(Integer teqipPmssDepartmentMasterId) {
        this.teqipPmssDepartmentMasterId = teqipPmssDepartmentMasterId;
    }

    public String getTeqipPmssDepartmentMaster() {
        return teqipPmssDepartmentMaster;
    }

    public void setTeqipPmssDepartmentMaster(String teqipPmssDepartmentMaster) {
        this.teqipPmssDepartmentMaster = teqipPmssDepartmentMaster;
    }

    public String getTeqipItemGoodsDetail() {
        return teqipItemGoodsDetail;
    }

    public void setTeqipItemGoodsDetail(String teqipItemGoodsDetail) {
        this.teqipItemGoodsDetail = teqipItemGoodsDetail;
    }
    

}
