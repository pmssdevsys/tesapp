package com.qspear.procurement.model;

import java.util.Date;

public class TeqipItemWorkDetailModel {

    private Integer id;

    private Integer createdBy;

    private Integer reviseId;

    private Boolean status;

    private Integer modifyBy;

    private Date modifyOn;

    private Double workCost;

    private String workName;

    private String workSpecification;

    private Integer teqipPackageId;

    private Integer teqipPmssDepartmentMasterId;

    private String teqipPmssDepartmentMaster;

    Integer deliveyPeriod;
    Integer trainingRequired;
    Integer installationRequired;
    String placeofdelivery;
    String instllationrequirement;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Date modifyOn) {
        this.modifyOn = modifyOn;
    }

    public Double getWorkCost() {
        return workCost;
    }

    public void setWorkCost(Double workCost) {
        this.workCost = workCost;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getWorkSpecification() {
        return workSpecification;
    }

    public void setWorkSpecification(String workSpecification) {
        this.workSpecification = workSpecification;
    }

    public Integer getTeqipPackageId() {
        return teqipPackageId;
    }

    public void setTeqipPackageId(Integer teqipPackageId) {
        this.teqipPackageId = teqipPackageId;
    }

    public Integer getReviseId() {
        return reviseId;
    }

    public void setReviseId(Integer reviseId) {
        this.reviseId = reviseId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getTeqipPmssDepartmentMasterId() {
        return teqipPmssDepartmentMasterId;
    }

    public void setTeqipPmssDepartmentMasterId(Integer teqipPmssDepartmentMasterId) {
        this.teqipPmssDepartmentMasterId = teqipPmssDepartmentMasterId;
    }

    public String getTeqipPmssDepartmentMaster() {
        return teqipPmssDepartmentMaster;
    }

    public void setTeqipPmssDepartmentMaster(String teqipPmssDepartmentMaster) {
        this.teqipPmssDepartmentMaster = teqipPmssDepartmentMaster;
    }

    public Integer getDeliveyPeriod() {
        return deliveyPeriod;
    }

    public void setDeliveyPeriod(Integer deliveyPeriod) {
        this.deliveyPeriod = deliveyPeriod;
    }

    public Integer getTrainingRequired() {
        return trainingRequired;
    }

    public void setTrainingRequired(Integer trainingRequired) {
        this.trainingRequired = trainingRequired;
    }

    public Integer getInstallationRequired() {
        return installationRequired;
    }

    public void setInstallationRequired(Integer installationRequired) {
        this.installationRequired = installationRequired;
    }

    public String getPlaceofdelivery() {
        return placeofdelivery;
    }

    public void setPlaceofdelivery(String placeofdelivery) {
        this.placeofdelivery = placeofdelivery;
    }

    public String getInstllationrequirement() {
        return instllationrequirement;
    }

    public void setInstllationrequirement(String instllationrequirement) {
        this.instllationrequirement = instllationrequirement;
    }

}
