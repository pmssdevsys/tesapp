package com.qspear.procurement.model;

import com.qspear.procurement.persistence.TeqipPackage;
import java.util.Date;
import java.util.List;

public class TeqipPackageModel implements Comparable<TeqipPackageModel> {

    Integer packageId;
    String createdBy;
    Date createdOn;
    Integer modifyBy;
    Date modifyOn;
    Integer reviseId;
    Double estimatedCost;
    Double actualCost;
    Date financialSanctionDate;
    Date packageInitiatedate;
    Boolean iscoe;
    Boolean isgem;
    Boolean isprop;
    String justification;
    String packageCode;
    String packageName;
    String revisionComments;
    Integer serviceProviderId;
    List<TeqipItemGoodsDetailModel> teqipItemGoodsDetails;
    List<TeqipItemWorkDetailModel> teqipItemWorkDetails;
    Integer teqipCategorymasterId;
    String teqipCategorymasterName;
    Integer teqipSubcategorymasterId;
    String teqipSubcategorymasterName;
    Integer teqipActivitiymasterId;
    String teqipActivitiymasterName;
    Integer teqipProcurementmethodId;
    String teqipProcurementmethodName;
    String teqipProcurementmethodCode;
    Integer teqipProcurementmasterId;
    String teqipProcurementmasterName;
    Integer teqipInstitutionId;
    String teqipInstitutionName;
    Integer teqipPmssProcurementstageId;
    Integer stageId;
    String teqipPmssProcurementstageName;
    Integer teqipSubcomponentmasterId;
    String teqipSubcomponentmasterName;
    Integer teqipStatemasterId;
    String teqipStatemasterName;
    boolean useOtherCategoryBuget;
    Double consumeFromWorks;
    Date packageInitiationDate;
    Integer isRevisedPackage = 0;
    String currentStage;
    Integer currentStageIndex;
    PrecurementMethodTimeLinePojo timeLine;
    boolean priorRejected;
    Integer isgemdcbid;
    String purchaseType;
    Boolean priorReviewNeeded;

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public PrecurementMethodTimeLinePojo getTimeLine() {
        return timeLine;
    }

    public void setTimeLine(PrecurementMethodTimeLinePojo timeLine) {
        this.timeLine = timeLine;
    }

    public Date getPackageInitiationDate() {
        return packageInitiationDate;
    }

    public void setPackageInitiationDate(Date packageInitiationDate) {
        this.packageInitiationDate = packageInitiationDate;
    }

    public Integer getTeqipStatemasterId() {
        return teqipStatemasterId;
    }

    public void setTeqipStatemasterId(Integer teqipStatemasterId) {
        this.teqipStatemasterId = teqipStatemasterId;
    }

    public String getTeqipStatemasterName() {
        return teqipStatemasterName;
    }

    public void setTeqipStatemasterName(String teqipStatemasterName) {
        this.teqipStatemasterName = teqipStatemasterName;
    }

    Integer typeofplanCreator;

    public Integer getTypeofplanCreator() {
        return typeofplanCreator;
    }

    public void setTypeofplanCreator(Integer typeofplanCreator) {
        this.typeofplanCreator = typeofplanCreator;
    }

    public Integer getTeqipSubcomponentmasterId() {
        return teqipSubcomponentmasterId;
    }

    public void setTeqipSubcomponentmasterId(Integer teqipSubcomponentmasterId) {
        this.teqipSubcomponentmasterId = teqipSubcomponentmasterId;
    }

    public String getTeqipSubcomponentmasterName() {
        return teqipSubcomponentmasterName;
    }

    public void setTeqipSubcomponentmasterName(String teqipSubcomponentmasterName) {
        this.teqipSubcomponentmasterName = teqipSubcomponentmasterName;
    }

    public Integer getTeqipCategorymasterId() {
        return teqipCategorymasterId;
    }

    public void setTeqipCategorymasterId(Integer teqipCategorymasterId) {
        this.teqipCategorymasterId = teqipCategorymasterId;
    }

    public String getTeqipCategorymasterName() {
        return teqipCategorymasterName;
    }

    public void setTeqipCategorymasterName(String teqipCategorymasterName) {
        this.teqipCategorymasterName = teqipCategorymasterName;
    }

    public Integer getTeqipSubcategorymasterId() {
        return teqipSubcategorymasterId;
    }

    public void setTeqipSubcategorymasterId(Integer teqipSubcategorymasterId) {
        this.teqipSubcategorymasterId = teqipSubcategorymasterId;
    }

    public String getTeqipSubcategorymasterName() {
        return teqipSubcategorymasterName;
    }

    public void setTeqipSubcategorymasterName(String teqipSubcategorymasterName) {
        this.teqipSubcategorymasterName = teqipSubcategorymasterName;
    }

    public Integer getTeqipActivitiymasterId() {
        return teqipActivitiymasterId;
    }

    public void setTeqipActivitiymasterId(Integer teqipActivitiymasterId) {
        this.teqipActivitiymasterId = teqipActivitiymasterId;
    }

    public String getTeqipActivitiymasterName() {
        return teqipActivitiymasterName;
    }

    public void setTeqipActivitiymasterName(String teqipActivitiymasterName) {
        this.teqipActivitiymasterName = teqipActivitiymasterName;
    }

    public Integer getTeqipProcurementmethodId() {
        return teqipProcurementmethodId;
    }

    public void setTeqipProcurementmethodId(Integer teqipProcurementmethodId) {
        this.teqipProcurementmethodId = teqipProcurementmethodId;
    }

    public String getTeqipProcurementmethodName() {
        return teqipProcurementmethodName;
    }

    public void setTeqipProcurementmethodName(String teqipProcurementmethodName) {
        this.teqipProcurementmethodName = teqipProcurementmethodName;
    }

    public Integer getTeqipProcurementmasterId() {
        return teqipProcurementmasterId;
    }

    public void setTeqipProcurementmasterId(Integer teqipProcurementmasterId) {
        this.teqipProcurementmasterId = teqipProcurementmasterId;
    }

    public String getTeqipProcurementmasterName() {
        return teqipProcurementmasterName;
    }

    public void setTeqipProcurementmasterName(String teqipProcurementmasterName) {
        this.teqipProcurementmasterName = teqipProcurementmasterName;
    }

    public Integer getTeqipInstitutionId() {
        return teqipInstitutionId;
    }

    public void setTeqipInstitutionId(Integer teqipInstitutionId) {
        this.teqipInstitutionId = teqipInstitutionId;
    }

    public String getTeqipInstitutionName() {
        return teqipInstitutionName;
    }

    public void setTeqipInstitutionName(String teqipInstitutionName) {
        this.teqipInstitutionName = teqipInstitutionName;
    }

    public Integer getTeqipPmssProcurementstageId() {
        return teqipPmssProcurementstageId;
    }

    public void setTeqipPmssProcurementstageId(Integer teqipPmssProcurementstageId) {
        this.teqipPmssProcurementstageId = teqipPmssProcurementstageId;
    }

    public String getTeqipPmssProcurementstageName() {
        return teqipPmssProcurementstageName;
    }

    public void setTeqipPmssProcurementstageName(String teqipPmssProcurementstageName) {
        this.teqipPmssProcurementstageName = teqipPmssProcurementstageName;
    }

    public Boolean getIscoe() {
        return iscoe;
    }

    public Boolean getIsgem() {
        return isgem;
    }

    public Boolean getIsprop() {
        return isprop;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Double getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(Double estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public Date getFinancialSanctionDate() {
        return financialSanctionDate;
    }

    public void setFinancialSanctionDate(Date financialSanctionDate) {
        this.financialSanctionDate = financialSanctionDate;
    }

    public Boolean isIscoe() {
        return iscoe;
    }

    public void setIscoe(Boolean iscoe) {
        this.iscoe = iscoe;
    }

    public Boolean isIsgem() {
        return isgem;
    }

    public void setIsgem(Boolean isgem) {
        this.isgem = isgem;
    }

    public Boolean isIsprop() {
        return isprop;
    }

    public void setIsprop(Boolean isprop) {
        this.isprop = isprop;
    }

    public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Date modifyOn) {
        this.modifyOn = modifyOn;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getRevisionComments() {
        return revisionComments;
    }

    public void setRevisionComments(String revisionComments) {
        this.revisionComments = revisionComments;
    }

    public Integer getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(Integer serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public List<TeqipItemGoodsDetailModel> getTeqipItemGoodsDetails() {
        return teqipItemGoodsDetails;
    }

    public void setTeqipItemGoodsDetails(List<TeqipItemGoodsDetailModel> teqipItemGoodsDetails) {
        this.teqipItemGoodsDetails = teqipItemGoodsDetails;
    }

    public List<TeqipItemWorkDetailModel> getTeqipItemWorkDetails() {
        return teqipItemWorkDetails;
    }

    public void setTeqipItemWorkDetails(List<TeqipItemWorkDetailModel> teqipItemWorkDetails) {
        this.teqipItemWorkDetails = teqipItemWorkDetails;
    }

    public Integer getReviseId() {
        return reviseId;
    }

    public void setReviseId(Integer reviseId) {
        this.reviseId = reviseId;
    }

    public Date getPackageInitiatedate() {
        return packageInitiatedate;
    }

    public void setPackageInitiatedate(Date packageInitiatedate) {
        this.packageInitiatedate = packageInitiatedate;
    }

    public String getTeqipProcurementmethodCode() {
        return teqipProcurementmethodCode;
    }

    public void setTeqipProcurementmethodCode(String teqipProcurementmethodCode) {
        this.teqipProcurementmethodCode = teqipProcurementmethodCode;
    }

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    public boolean isUseOtherCategoryBuget() {
        return useOtherCategoryBuget;
    }

    public void setUseOtherCategoryBuget(boolean useOtherCategoryBuget) {
        this.useOtherCategoryBuget = useOtherCategoryBuget;
    }

    public Double getConsumeFromWorks() {
        return consumeFromWorks;
    }

    public void setConsumeFromWorks(Double consumeFromWorks) {
        this.consumeFromWorks = consumeFromWorks;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getIsRevisedPackage() {
        return isRevisedPackage;
    }

    public void setIsRevisedPackage(Integer isRevisedPackage) {
        this.isRevisedPackage = isRevisedPackage;
    }

    public String getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(String currentStage) {
        this.currentStage = currentStage;
    }

    public Integer getCurrentStageIndex() {
        return currentStageIndex;
    }

    public void setCurrentStageIndex(Integer currentStageIndex) {
        this.currentStageIndex = currentStageIndex;
    }

    public Double getActualCost() {
        return actualCost;
    }

    public void setActualCost(Double actualCost) {
        this.actualCost = actualCost;
    }

    public boolean isPriorRejected() {
        return priorRejected;
    }

    public void setPriorRejected(boolean priorRejected) {
        this.priorRejected = priorRejected;
    }

    public Integer getIsgemdcbid() {
        return isgemdcbid;
    }

    public void setIsgemdcbid(Integer isgemdcbid) {
        this.isgemdcbid = isgemdcbid;
    }

    @Override
    public int compareTo(TeqipPackageModel o) {
        String str1 = this.teqipCategorymasterName != null ? this.teqipCategorymasterName : "";
        String str2 = o != null && o.getTeqipCategorymasterName() != null ? o.getTeqipCategorymasterName() : "";
        return str1.compareTo(str2);
    }

    public Boolean getPriorReviewNeeded() {
        return priorReviewNeeded;
    }

    public void setPriorReviewNeeded(Boolean priorReviewNeeded) {
        this.priorReviewNeeded = priorReviewNeeded;
    }

}
