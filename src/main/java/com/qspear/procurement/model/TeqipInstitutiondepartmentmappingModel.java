package com.qspear.procurement.model;

import java.util.List;

public class TeqipInstitutiondepartmentmappingModel implements Cloneable{
	private int institutionDepartmentMapping_ID;
 private Integer spfuid;
 private String type;
 private Integer typeNpcEdcil;
	public Integer getTypeNpcEdcil() {
	return typeNpcEdcil;
}

public void setTypeNpcEdcil(Integer typeNpcEdcil) {
	this.typeNpcEdcil = typeNpcEdcil;
}

	public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

	public Integer getSpfuid() {
	return spfuid;
}

public void setSpfuid(Integer spfuid) {
	this.spfuid = spfuid;
}

	private String departmentcode;

	private String departmenthead;

	private String departmentname;

	private Integer teqipInstitutionId;
        private Integer departmentmasterid;
	public Integer getDepartmentmasterid() {
	return departmentmasterid;
        }

        public void setDepartmentmasterid(Integer departmentmasterid) {
                this.departmentmasterid = departmentmasterid;
        }
        
        private Byte isactive;

	public Byte getIsactive() {
		return isactive;
	}

	public void setIsactive(Byte isactive) {
		this.isactive = isactive;
	}
	
	private List<TeqipItemGoodsDepartmentBreakupModel> teqipItemGoodsDepartmentBreakups;
	
	private List<TeqipItemWorkDetailModel> teqipItemWorkDetails;

	public int getInstitutionDepartmentMapping_ID() {
		return institutionDepartmentMapping_ID;
	}

	public void setInstitutionDepartmentMapping_ID(int institutionDepartmentMapping_ID) {
		this.institutionDepartmentMapping_ID = institutionDepartmentMapping_ID;
	}

	public String getDepartmentcode() {
		return departmentcode;
	}

	public void setDepartmentcode(String departmentcode) {
		this.departmentcode = departmentcode;
	}

	public String getDepartmenthead() {
		return departmenthead;
	}

	public void setDepartmenthead(String departmenthead) {
		this.departmenthead = departmenthead;
	}

	public String getDepartmentname() {
		return departmentname;
	}

	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}

	public Integer getTeqipInstitutionId() {
		return teqipInstitutionId;
	}

	public void setTeqipInstitutionId(Integer teqipInstitutionId) {
		this.teqipInstitutionId = teqipInstitutionId;
	}

	public List<TeqipItemGoodsDepartmentBreakupModel> getTeqipItemGoodsDepartmentBreakups() {
		return teqipItemGoodsDepartmentBreakups;
	}

	public void setTeqipItemGoodsDepartmentBreakups(
			List<TeqipItemGoodsDepartmentBreakupModel> teqipItemGoodsDepartmentBreakups) {
		this.teqipItemGoodsDepartmentBreakups = teqipItemGoodsDepartmentBreakups;
	}

	public List<TeqipItemWorkDetailModel> getTeqipItemWorkDetails() {
		return teqipItemWorkDetails;
	}

	public void setTeqipItemWorkDetails(List<TeqipItemWorkDetailModel> teqipItemWorkDetails) {
		this.teqipItemWorkDetails = teqipItemWorkDetails;
	}
	public TeqipInstitutiondepartmentmappingModel clone() throws CloneNotSupportedException{
		return (TeqipInstitutiondepartmentmappingModel) super.clone();
	}
}