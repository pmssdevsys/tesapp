package com.qspear.procurement.model;

public class TeqipDepartmentModel {

    private int institutionDepartmentMapping_ID;

    private int teqipPmssDepartmentMasterId;

    private String departmentcode;

    private String departmenthead;

    private String departmentname;
    
    private Integer DepartmentId;
    public Integer  getDepartmentId() {
		return DepartmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		DepartmentId = departmentId;
	}

    public int getInstitutionDepartmentMapping_ID() {
        return institutionDepartmentMapping_ID;
    }

    public void setInstitutionDepartmentMapping_ID(int institutionDepartmentMapping_ID) {
        this.institutionDepartmentMapping_ID = institutionDepartmentMapping_ID;
    }

    public String getDepartmentcode() {
        return departmentcode;
    }

    public void setDepartmentcode(String departmentcode) {
        this.departmentcode = departmentcode;
    }

    public String getDepartmenthead() {
        return departmenthead;
    }

    public void setDepartmenthead(String departmenthead) {
        this.departmenthead = departmenthead;
    }

    public String getDepartmentname() {
        return departmentname;
    }

    public void setDepartmentname(String departmentname) {
        this.departmentname = departmentname;
    }

	public int getTeqipPmssDepartmentMasterId() {
		return teqipPmssDepartmentMasterId;
	}

	public void setTeqipPmssDepartmentMasterId(int teqipPmssDepartmentMasterId) {
		this.teqipPmssDepartmentMasterId = teqipPmssDepartmentMasterId;
	}

 

}
