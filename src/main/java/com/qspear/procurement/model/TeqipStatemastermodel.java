package com.qspear.procurement.model;

import javax.persistence.Column;

public class TeqipStatemastermodel {
	 private Integer stateId;
	 private Double allocatedbudget;
public Double getAllocatedbudget() {
		return allocatedbudget;
	}

	public void setAllocatedbudget(Double allocatedbudget) {
		this.allocatedbudget = allocatedbudget;
	}

private Integer teqipInstitutionId;
private Integer teqipUsersMastersid;
	    public Integer getTeqipInstitutionId() {
	return teqipInstitutionId;
}

public void setTeqipInstitutionId(Integer teqipInstitutionId) {
	this.teqipInstitutionId = teqipInstitutionId;
}

public Integer getTeqipUsersMastersid() {
	return teqipUsersMastersid;
}

public void setTeqipUsersMastersid(Integer teqipUsersMastersid) {
	this.teqipUsersMastersid = teqipUsersMastersid;
}

		private Boolean isactive;

	    private String stateCode;

	    public Integer getStateId() {
			return stateId;
		}

		public void setStateId(Integer stateId) {
			this.stateId = stateId;
		}

		public Boolean getIsactive() {
			return isactive;
		}

		public void setIsactive(Boolean isactive) {
			this.isactive = isactive;
		}

		public String getStateCode() {
			return stateCode;
		}

		public void setStateCode(String stateCode) {
			this.stateCode = stateCode;
		}

		public String getStateName() {
			return stateName;
		}

		public void setStateName(String stateName) {
			this.stateName = stateName;
		}

		public String getEmailID() {
			return emailID;
		}

		public void setEmailID(String emailID) {
			this.emailID = emailID;
		}

		public String getPhoneNumber() {
			return phoneNumber;
		}

		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}

		public String getFaxNumber() {
			return faxNumber;
		}

		public void setFaxNumber(String faxNumber) {
			this.faxNumber = faxNumber;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getWebsiteURL() {
			return websiteURL;
		}

		public void setWebsiteURL(String websiteURL) {
			this.websiteURL = websiteURL;
		}

		private String stateName;

	    String emailID;

	    String phoneNumber;

	    String faxNumber;

	    String address;

	    String websiteURL;

}
