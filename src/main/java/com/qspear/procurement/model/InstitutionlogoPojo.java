package com.qspear.procurement.model;

import java.sql.Timestamp;

public class InstitutionlogoPojo {
	private Integer institutionLogoID;
	private Integer createdBy;
	private Integer instituteid;
	private Integer modifiedby;
	public Integer getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}
	private Timestamp createdOn;
	private String description;
	private String originalFileName;
	private String systemFileName;
	private Integer isactice;
	public int getIsactive() {
		return isactice;
	}

	public void setIsactive(int isactive) {
		this.isactice = isactive;
	}
	public Integer getInstitutionLogoID() {
		return institutionLogoID;
	}
	public void setInstitutionLogoID(Integer institutionLogoID) {
		this.institutionLogoID = institutionLogoID;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getInstituteid() {
		return instituteid;
	}
	public void setInstituteid(Integer instituteid) {
		this.instituteid = instituteid;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOriginalFileName() {
		return originalFileName;
	}
	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}
	public String getSystemFileName() {
		return systemFileName;
	}
	public void setSystemFileName(String systemFileName) {
		this.systemFileName = systemFileName;
	}
	
	}
