package com.qspear.procurement.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

public class TeqipProcurementmasterModel {
	private Integer procurementmasterId;

         @JsonFormat(pattern="yyyy-MM-dd")
	private Date endDate;

	private Double planAmount;

	private String planName;

	private String shortName;

         @JsonFormat(pattern="yyyy-MM-dd")
	private Date startDate;

	private String currentStage;
	
	private Integer stageId;

	private String subComponentName;
	
	private String subComponentCode;
	
	private Integer subComponentId;
        
        private Date lastDateSubmission;

        public Date getLastDateSubmission() {
            return lastDateSubmission;
        }

        public void setLastDateSubmission(Date lastDateSubmission) {
            this.lastDateSubmission = lastDateSubmission;
        }

	public TeqipProcurementmasterModel() {
	}

	public Integer getProcurementmasterId() {
		return this.procurementmasterId;
	}

	public void setProcurementmasterId(Integer procurementmasterId) {
		this.procurementmasterId = procurementmasterId;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getPlanAmount() {
		return this.planAmount;
	}

	public void setPlanAmount(Double planAmount) {
		this.planAmount = planAmount;
	}

	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getCurrentStage() {
		return currentStage;
	}

	public void setCurrentStage(String currentStage) {
		this.currentStage = currentStage;
	}

	public String getSubComponentName() {
		return subComponentName;
	}

	public void setSubComponentName(String subComponentName) {
		this.subComponentName = subComponentName;
	}
	
	public String getSubComponentCode() {
		return subComponentCode;
	}

	public void setSubComponentCode(String subComponentCode) {
		this.subComponentCode = subComponentCode;
	}

	public Integer getSubComponentId() {
		return subComponentId;
	}

	public void setSubComponentId(Integer subComponentId) {
		this.subComponentId = subComponentId;
	}
	
	public Integer getStageId() {
		return stageId;
	}

	public void setStageId(Integer stageId) {
		this.stageId = stageId;
	}
}