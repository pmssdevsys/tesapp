package com.qspear.procurement.model;

import java.io.Serializable;

public class TeqipInstitutionsubcomponentmappingPojo implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer institutionSubcomponentMapping_ID;
	private Integer instituteid;
	private Integer spfuid;
	private Integer id;
    private String subcomponentcode;
    private Integer typeNpcEdcil;
    public Integer getTypeNpcEdcil() {
		return typeNpcEdcil;
	}

	public void setTypeNpcEdcil(Integer typeNpcEdcil) {
		this.typeNpcEdcil = typeNpcEdcil;
	}

	public String getSubcomponentcode() {
		return subcomponentcode;
	}

	public void setSubcomponentcode(String subcomponentcode) {
		this.subcomponentcode = subcomponentcode;
	}

	private Double allocatedbudget;

	private Double budgetforcw;

	private Double budgetforprocuremen;

	private Double budgetforservices;

	private String deptName;
	private Byte isactive;
	public Byte getIsactive() {
		return isactive;
	}

	public void setIsactive(Byte isactive) {
		this.isactive = isactive;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private String allcatedtype;

	public String getAllcatedtype() {
		return allcatedtype;
	}

	public void setAllcatedtype(String allcatedtype) {
		this.allcatedtype = allcatedtype;
	}

	public Integer getSpfuid() {
		return spfuid;
	}

	public void setSpfuid(Integer spfuid) {
		this.spfuid = spfuid;
	}

	public Integer getInstituteid() {
		return instituteid;
	}

	public void setInstituteid(Integer instituteid) {
		this.instituteid = instituteid;
	}

	public TeqipInstitutionsubcomponentmappingPojo() {
	}

	public Integer getInstitutionSubcomponentMapping_ID() {
		return institutionSubcomponentMapping_ID;
	}

	public void setInstitutionSubcomponentMapping_ID(Integer institutionSubcomponentMapping_ID) {
		this.institutionSubcomponentMapping_ID = institutionSubcomponentMapping_ID;
	}

	public Double getAllocatedbudget() {
		return allocatedbudget;
	}

	public void setAllocatedbudget(Double allocatedbudget) {
		this.allocatedbudget = allocatedbudget;
	}

	public Double getBudgetforcw() {
		return budgetforcw;
	}

	public void setBudgetforcw(Double budgetforcw) {
		this.budgetforcw = budgetforcw;
	}

	public Double getBudgetforprocuremen() {
		return budgetforprocuremen;
	}

	public void setBudgetforprocuremen(Double budgetforprocuremen) {
		this.budgetforprocuremen = budgetforprocuremen;
	}

	public Double getBudgetforservices() {
		return budgetforservices;
	}

	public void setBudgetforservices(Double budgetforservices) {
		this.budgetforservices = budgetforservices;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}