/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model;

/**
 *
 * @author QCS
 */
public class Mail {

    String from;
    private String[] to;
    private String[] cc;
    private String[] bcc;
    private String subject;
    private FileObect[] fileobj;
    private String body;
    private Integer supplierId;
    private String replyTo;
    public Mail() {
    }

    public Mail(String[] to, String[] cc, String[] bcc, String subject, String body, FileObect[] fileobj,String replyTo) {
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.fileobj = fileobj;
        this.body = body;
        this.replyTo=replyTo;
    }

    public String getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	public String[] getTo() {
        return to;
    }

    public void setTo(String[] to) {
        this.to = to;
    }

    public String[] getCc() {
        return cc;
    }

    public void setCc(String[] cc) {
        this.cc = cc;
    }

    public String[] getBcc() {
        return bcc;
    }

    public void setBcc(String[] bcc) {
        this.bcc = bcc;
    }

    public FileObect[] getFileobj() {
        return fileobj;
    }

    public void setFileobj(FileObect[] fileobj) {
        this.fileobj = fileobj;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

}
