package com.qspear.procurement.model;

import java.sql.Timestamp;
import java.util.List;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import com.qspear.procurement.persistence.TeqipInstitutionsubcomponentmapping;
import com.qspear.procurement.persistence.TeqipInstitutiontype;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPmssInstitutionlogo;
import com.qspear.procurement.persistence.TeqipPmssInstitutionpurchasecommitte;

public class TeqipInstitutionPojo   {
	private Integer institutionId;

	private String address;
	 private Integer spfu;
	public Integer getSpfuid() {
		return spfuid;
	}


	public void setSpfuid(Integer spfuid) {
		this.spfuid = spfuid;
	}


	private Double allocatedbuget;
        private Integer spfuid;
	public Integer getSpfu() {
	return spfuid;
}


public void setSpfu(Integer spfuid) {
	this.spfuid = spfuid;
}


	private Integer createdBy;

	private Timestamp createdOn;

	private String emailid;

	private String faxnumber;

	private String institutionCode;

	private String institutionName;

	private String state;

	private String telephonenumber;

	private String websiteurl;

    private Integer institutiontype;
	private String institutetypeName;
    public String getInstitutetypeName() {
		return institutetypeName;
	}


	public void setInstitutetypeName(String institutetypeName) {
		this.institutetypeName = institutetypeName;
	}


	private Integer teqipStatemaster;
    
    private Long institutionDepartmentMapping_ID;
	
	private String departmenthead;

	private String departmentname;

	private Long catId;

	private Long subcatId;

	private Byte isactive;
	
	private String subCategoryName;
	
	private TeqipCategorymaster teqipCategorymaster;
	

	private String departmentcode;


	public Integer getInstitutionId() {
		return institutionId;
	}


	public void setInstitutionId(Integer institutionId) {
		this.institutionId = institutionId;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public Double getAllocatedbuget() {
		return allocatedbuget;
	}


	public void setAllocatedbuget(Double allocatedbuget) {
		this.allocatedbuget = allocatedbuget;
	}


	public Integer getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}


	public Timestamp getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}


	public String getEmailid() {
		return emailid;
	}


	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}


	public String getFaxnumber() {
		return faxnumber;
	}


	public void setFaxnumber(String faxnumber) {
		this.faxnumber = faxnumber;
	}


	public String getInstitutionCode() {
		return institutionCode;
	}


	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}


	public String getInstitutionName() {
		return institutionName;
	}


	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getTelephonenumber() {
		return telephonenumber;
	}


	public void setTelephonenumber(String telephonenumber) {
		this.telephonenumber = telephonenumber;
	}


	public String getWebsiteurl() {
		return websiteurl;
	}


	public void setWebsiteurl(String websiteurl) {
		this.websiteurl = websiteurl;
	}


	public Integer getInstitutiontype() {
		return institutiontype;
	}


	public void setInstitutiontype(Integer institutiontype) {
		this.institutiontype = institutiontype;
	}


	public Integer getTeqipStatemaster() {
		return teqipStatemaster;
	}


	public void setTeqipStatemaster(Integer teqipStatemaster) {
		this.teqipStatemaster = teqipStatemaster;
	}


	public Long getInstitutionDepartmentMapping_ID() {
		return institutionDepartmentMapping_ID;
	}


	public void setInstitutionDepartmentMapping_ID(Long institutionDepartmentMapping_ID) {
		this.institutionDepartmentMapping_ID = institutionDepartmentMapping_ID;
	}


	public String getDepartmenthead() {
		return departmenthead;
	}


	public void setDepartmenthead(String departmenthead) {
		this.departmenthead = departmenthead;
	}


	public String getDepartmentname() {
		return departmentname;
	}


	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}


	public Long getCatId() {
		return catId;
	}


	public void setCatId(Long catId) {
		this.catId = catId;
	}


	public Long getSubcatId() {
		return subcatId;
	}


	public void setSubcatId(Long subcatId) {
		this.subcatId = subcatId;
	}


	public Byte getIsactive() {
		return isactive;
	}


	public void setIsactive(Byte isactive) {
		this.isactive = isactive;
	}


	public String getSubCategoryName() {
		return subCategoryName;
	}


	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}


	public TeqipCategorymaster getTeqipCategorymaster() {
		return teqipCategorymaster;
	}


	public void setTeqipCategorymaster(TeqipCategorymaster teqipCategorymaster) {
		this.teqipCategorymaster = teqipCategorymaster;
	}


	public String getDepartmentcode() {
		return departmentcode;
	}


	public void setDepartmentcode(String departmentcode) {
		this.departmentcode = departmentcode;
	}



		}
