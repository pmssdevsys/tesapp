package com.qspear.procurement.model;

import java.util.Date;
import java.util.List;

public class TeqipItemGoodsDetailModel {

    private Integer id;

    private Integer createdBy;

    private Integer reviseId;

    private Boolean status;

    private Double itemCostUnit;

    private Double itemQnt;

    private String itemSpecification;

    private Integer modifyBy;

    private Date modifyOn;

    private Integer teqipItemMasterId;
    private String teqipItemMaster;

    private Integer teqipPackageId;

    private Integer teqipPmssDepartmentMasterId;
    private String teqipPmssDepartmentMaster;

    private List<TeqipItemGoodsDepartmentBreakupModel> teqipItemGoodsDepartmentBreakups;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Double getItemCostUnit() {
        return itemCostUnit;
    }

    public void setItemCostUnit(Double itemCostUnit) {
        this.itemCostUnit = itemCostUnit;
    }

    public Double getItemQnt() {
        return itemQnt;
    }

    public void setItemQnt(Double itemQnt) {
        this.itemQnt = itemQnt;
    }

    public String getItemSpecification() {
        return itemSpecification;
    }

    public void setItemSpecification(String itemSpecification) {
        this.itemSpecification = itemSpecification;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Date getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Date modifyOn) {
        this.modifyOn = modifyOn;
    }

    public Integer getTeqipItemMasterId() {
        return teqipItemMasterId;
    }

    public void setTeqipItemMasterId(Integer teqipItemMasterId) {
        this.teqipItemMasterId = teqipItemMasterId;
    }

    public Integer getTeqipPackageId() {
        return teqipPackageId;
    }

    public void setTeqipPackageId(Integer teqipPackageId) {
        this.teqipPackageId = teqipPackageId;
    }

    public Integer getReviseId() {
        return reviseId;
    }

    public void setReviseId(Integer reviseId) {
        this.reviseId = reviseId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<TeqipItemGoodsDepartmentBreakupModel> getTeqipItemGoodsDepartmentBreakups() {
        return teqipItemGoodsDepartmentBreakups;
    }

    public void setTeqipItemGoodsDepartmentBreakups(
            List<TeqipItemGoodsDepartmentBreakupModel> teqipItemGoodsDepartmentBreakups) {
        this.teqipItemGoodsDepartmentBreakups = teqipItemGoodsDepartmentBreakups;
    }

    public Integer getTeqipPmssDepartmentMasterId() {
        return teqipPmssDepartmentMasterId;
    }

    public void setTeqipPmssDepartmentMasterId(Integer teqipPmssDepartmentMasterId) {
        this.teqipPmssDepartmentMasterId = teqipPmssDepartmentMasterId;
    }

    public String getTeqipItemMaster() {
        return teqipItemMaster;
    }

    public void setTeqipItemMaster(String teqipItemMaster) {
        this.teqipItemMaster = teqipItemMaster;
    }

    public String getTeqipPmssDepartmentMaster() {
        return teqipPmssDepartmentMaster;
    }

    public void setTeqipPmssDepartmentMaster(String teqipPmssDepartmentMaster) {
        this.teqipPmssDepartmentMaster = teqipPmssDepartmentMaster;
    }

}
