package com.qspear.procurement.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.qspear.procurement.persistence.TeqipInstitution;

public class TeqipInstitutionTypeModel {
	private Integer institutiontypeId;

	private String institutiontypeName;

	private Boolean isactive;

	public Integer getInstitutiontypeId() {
		return institutiontypeId;
	}

	public void setInstitutiontypeId(Integer institutiontypeId) {
		this.institutiontypeId = institutiontypeId;
	}

	public String getInstitutiontypeName() {
		return institutiontypeName;
	}

	public void setInstitutiontypeName(String institutiontypeName) {
		this.institutiontypeName = institutiontypeName;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public List<TeqipInstitution> getTeqipInstitutions() {
		return teqipInstitutions;
	}

	public void setTeqipInstitutions(List<TeqipInstitution> teqipInstitutions) {
		this.teqipInstitutions = teqipInstitutions;
	}

	private List<TeqipInstitution> teqipInstitutions;

}
