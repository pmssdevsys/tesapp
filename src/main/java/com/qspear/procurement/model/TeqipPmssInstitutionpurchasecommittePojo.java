package com.qspear.procurement.model;

import java.io.Serializable;
import com.qspear.procurement.persistence.DesignationMasterModel;
import com.qspear.procurement.persistence.Pmssrolepurchasecommittee;
import com.qspear.procurement.persistence.TeqipInstitution;

public class TeqipPmssInstitutionpurchasecommittePojo implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;

	private Integer institutionPurchaseCommitte_ID;
	private String committeMemberName;
	private String pcommitteRoleName;
	private Integer instituteid;
	private Integer deptId;
	private Integer spfuid;
	 private Integer createdBy;
	 private Integer modifiedBy;
	 private Integer typeNpcEdcil;
		public Integer getTypeNpcEdcil() {
			return typeNpcEdcil;
		}

		public void setTypeNpcEdcil(Integer typeNpcEdcil) {
			this.typeNpcEdcil = typeNpcEdcil;
		}
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	private String committeetype;
	private String designation;
	private String designationName;
	private String PCommitteRole;
	private Integer pcroleid;
	private Integer isactive;
	private Pmssrolepurchasecommittee Pmssrolepurchasecommittee;
	private DesignationMasterModel designationMasterModel;
	public int getIsactive() {
		return isactive;
	}

	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public DesignationMasterModel getDesignationMasterModel() {
		return designationMasterModel;
	}

	public void setDesignationMasterModel(DesignationMasterModel designationMasterModel) {
		this.designationMasterModel = designationMasterModel;
	}

	public Pmssrolepurchasecommittee getPmssrolepurchasecommittee() {
		return Pmssrolepurchasecommittee;
	}

	public void setPmssrolepurchasecommittee(Pmssrolepurchasecommittee pmssrolepurchasecommittee) {
		Pmssrolepurchasecommittee = pmssrolepurchasecommittee;
	}
	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	
	public Integer getPcroleid() {
		return pcroleid;
	}

	public void setPcroleid(Integer pcroleid) {
		this.pcroleid = pcroleid;
	}

	public String getCommitteetype() {
		return committeetype;
	}

	public void setCommitteetype(String committeetype) {
		this.committeetype = committeetype;
	}

	public String getCommitteMemberName() {
		return committeMemberName;
	}

	public void setCommitteMemberName(String committeMemberName) {
		this.committeMemberName = committeMemberName;
	}

	public Integer getInstituteid() {
		return instituteid;
	}

	public void setInstituteid(Integer instituteid) {
		this.instituteid = instituteid;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public Integer getSpfuid() {
		return spfuid;
	}

	public void setSpfuid(Integer spfuid) {
		this.spfuid = spfuid;
	}

	private TeqipInstitution teqipInstitution;

	private String pmssdeptName;

	public TeqipPmssInstitutionpurchasecommittePojo() {
	}

	public Integer getInstitutionPurchaseCommitte_ID() {
		return this.institutionPurchaseCommitte_ID;
	}

	public void setInstitutionPurchaseCommitte_ID(Integer institutionPurchaseCommitte_ID) {
		this.institutionPurchaseCommitte_ID = institutionPurchaseCommitte_ID;
	}

/*	public Integer getDesignationid() {
		return designationid;
	}

	public void setDesignationid(Integer designationid) {
		this.designationid = designationid;
	}
*/
	public String getPCommitteRole() {
		return this.PCommitteRole;
	}

	public void setPCommitteRole(String PCommitteRole) {
		this.PCommitteRole = PCommitteRole;
	}

	public TeqipInstitution getTeqipInstitution() {
		return this.teqipInstitution;
	}

	public void setTeqipInstitution(TeqipInstitution teqipInstitution) {
		this.teqipInstitution = teqipInstitution;
	}

	public String getPmssdeptName() {
		return pmssdeptName;
	}

	public void setPmssdeptName(String pmssdeptName) {
		this.pmssdeptName = pmssdeptName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getPcommitteRoleName() {
		return pcommitteRoleName;
	}

	public void setPcommitteRoleName(String pcommitteRoleName) {
		this.pcommitteRoleName = pcommitteRoleName;
	}

	@Override
	public TeqipPmssInstitutionpurchasecommittePojo clone() throws CloneNotSupportedException {
	
		return (TeqipPmssInstitutionpurchasecommittePojo) super.clone();
	}
	
	

}