package com.qspear.procurement.model;

import java.sql.Timestamp;
import java.util.Date;

public class PrecurementMethodTimeLineModel {
	 private String procurementmethodcode;
	    private String procurementmethodname;
	    private Integer categoryId;
	    private Integer packageId;
	    private Integer procurementmethodId;
	    private Integer procurementmethodtimelineId;
	    private Integer procurementmethodrevisedId;
	    private Float bidDocumentationPreprationDatedays;
	    private Float bankNOCForBiddingDocumentsdays;
	    private Float bidInvitationDatedays;
	    private Float bidOpeningDatedays;
	    private Float tORFinalizationDatedays;
	    private Float advertisementDatedays;
	    private Float finalDraftToBeForwardedToTheBankDatedays;
	    private Float noObjectionFromBankForRFPdays;
	    private Float rFPIssuedDatedays;
	    private Float lastDateToReceiveProposalsdays;
	    private Float AdvertisementDate_WithoutBankNOC_days;
	    private Float evaluationDatedays;
	    private Float noObjectionFromBankForEvaluationdays;
	    private Float contractCompletionDatedays;
	    private Float contractAwardDatedays;
	    private Float tORFinalizationDate_WithoutBankNOCdays;
	    private Float rFPIssuedDate_WithoutBankNOCdays;
	    private Float evaluationDate_WithoutBankNOCdays;
	    private Float contractAwardDate_WithoutBankNOCdays;
	    private Float contractCompletionDate_WithoutBankNOC_days;
	    private Integer createdBy;
	    private Timestamp createdOn;
	    private Integer modifiedBy;
	    private Date modifiedOn;
	    private Float lastdateofsubmissionofEOI;
	    private Float shortlistingOfEOI;
	    private Float rFPApprovalDate;
	    private Float financialEvaluationDate;
	    private Float packageInitatedate;
		public String getProcurementmethodcode() {
			return procurementmethodcode;
		}
		public void setProcurementmethodcode(String procurementmethodcode) {
			this.procurementmethodcode = procurementmethodcode;
		}
		public String getProcurementmethodname() {
			return procurementmethodname;
		}
		public void setProcurementmethodname(String procurementmethodname) {
			this.procurementmethodname = procurementmethodname;
		}
		public Integer getCategoryId() {
			return categoryId;
		}
		public void setCategoryId(Integer categoryId) {
			this.categoryId = categoryId;
		}
		public Integer getPackageId() {
			return packageId;
		}
		public void setPackageId(Integer packageId) {
			this.packageId = packageId;
		}
		public Integer getProcurementmethodId() {
			return procurementmethodId;
		}
		public void setProcurementmethodId(Integer procurementmethodId) {
			this.procurementmethodId = procurementmethodId;
		}
		public Integer getProcurementmethodtimelineId() {
			return procurementmethodtimelineId;
		}
		public void setProcurementmethodtimelineId(Integer procurementmethodtimelineId) {
			this.procurementmethodtimelineId = procurementmethodtimelineId;
		}
		public Integer getProcurementmethodrevisedId() {
			return procurementmethodrevisedId;
		}
		public void setProcurementmethodrevisedId(Integer procurementmethodrevisedId) {
			this.procurementmethodrevisedId = procurementmethodrevisedId;
		}
		public Float getBidDocumentationPreprationDatedays() {
			return bidDocumentationPreprationDatedays;
		}
		public void setBidDocumentationPreprationDatedays(Float bidDocumentationPreprationDatedays) {
			this.bidDocumentationPreprationDatedays = bidDocumentationPreprationDatedays;
		}
		public Float getBankNOCForBiddingDocumentsdays() {
			return bankNOCForBiddingDocumentsdays;
		}
		public void setBankNOCForBiddingDocumentsdays(Float bankNOCForBiddingDocumentsdays) {
			this.bankNOCForBiddingDocumentsdays = bankNOCForBiddingDocumentsdays;
		}
		public Float getBidInvitationDatedays() {
			return bidInvitationDatedays;
		}
		public void setBidInvitationDatedays(Float bidInvitationDatedays) {
			this.bidInvitationDatedays = bidInvitationDatedays;
		}
		public Float getBidOpeningDatedays() {
			return bidOpeningDatedays;
		}
		public void setBidOpeningDatedays(Float bidOpeningDatedays) {
			this.bidOpeningDatedays = bidOpeningDatedays;
		}
		public Float gettORFinalizationDatedays() {
			return tORFinalizationDatedays;
		}
		public void settORFinalizationDatedays(Float tORFinalizationDatedays) {
			this.tORFinalizationDatedays = tORFinalizationDatedays;
		}
		public Float getAdvertisementDatedays() {
			return advertisementDatedays;
		}
		public void setAdvertisementDatedays(Float advertisementDatedays) {
			this.advertisementDatedays = advertisementDatedays;
		}
		public Float getFinalDraftToBeForwardedToTheBankDatedays() {
			return finalDraftToBeForwardedToTheBankDatedays;
		}
		public void setFinalDraftToBeForwardedToTheBankDatedays(Float finalDraftToBeForwardedToTheBankDatedays) {
			this.finalDraftToBeForwardedToTheBankDatedays = finalDraftToBeForwardedToTheBankDatedays;
		}
		public Float getNoObjectionFromBankForRFPdays() {
			return noObjectionFromBankForRFPdays;
		}
		public void setNoObjectionFromBankForRFPdays(Float noObjectionFromBankForRFPdays) {
			this.noObjectionFromBankForRFPdays = noObjectionFromBankForRFPdays;
		}
		public Float getrFPIssuedDatedays() {
			return rFPIssuedDatedays;
		}
		public void setrFPIssuedDatedays(Float rFPIssuedDatedays) {
			this.rFPIssuedDatedays = rFPIssuedDatedays;
		}
		public Float getLastDateToReceiveProposalsdays() {
			return lastDateToReceiveProposalsdays;
		}
		public void setLastDateToReceiveProposalsdays(Float lastDateToReceiveProposalsdays) {
			this.lastDateToReceiveProposalsdays = lastDateToReceiveProposalsdays;
		}
		public Float getAdvertisementDate_WithoutBankNOC_days() {
			return AdvertisementDate_WithoutBankNOC_days;
		}
		public void setAdvertisementDate_WithoutBankNOC_days(Float advertisementDate_WithoutBankNOC_days) {
			AdvertisementDate_WithoutBankNOC_days = advertisementDate_WithoutBankNOC_days;
		}
		public Float getEvaluationDatedays() {
			return evaluationDatedays;
		}
		public void setEvaluationDatedays(Float evaluationDatedays) {
			this.evaluationDatedays = evaluationDatedays;
		}
		public Float getNoObjectionFromBankForEvaluationdays() {
			return noObjectionFromBankForEvaluationdays;
		}
		public void setNoObjectionFromBankForEvaluationdays(Float noObjectionFromBankForEvaluationdays) {
			this.noObjectionFromBankForEvaluationdays = noObjectionFromBankForEvaluationdays;
		}
		public Float getContractCompletionDatedays() {
			return contractCompletionDatedays;
		}
		public void setContractCompletionDatedays(Float contractCompletionDatedays) {
			this.contractCompletionDatedays = contractCompletionDatedays;
		}
		public Float getContractAwardDatedays() {
			return contractAwardDatedays;
		}
		public void setContractAwardDatedays(Float contractAwardDatedays) {
			this.contractAwardDatedays = contractAwardDatedays;
		}
		public Float gettORFinalizationDate_WithoutBankNOCdays() {
			return tORFinalizationDate_WithoutBankNOCdays;
		}
		public void settORFinalizationDate_WithoutBankNOCdays(Float tORFinalizationDate_WithoutBankNOCdays) {
			this.tORFinalizationDate_WithoutBankNOCdays = tORFinalizationDate_WithoutBankNOCdays;
		}
		public Float getrFPIssuedDate_WithoutBankNOCdays() {
			return rFPIssuedDate_WithoutBankNOCdays;
		}
		public void setrFPIssuedDate_WithoutBankNOCdays(Float rFPIssuedDate_WithoutBankNOCdays) {
			this.rFPIssuedDate_WithoutBankNOCdays = rFPIssuedDate_WithoutBankNOCdays;
		}
		public Float getEvaluationDate_WithoutBankNOCdays() {
			return evaluationDate_WithoutBankNOCdays;
		}
		public void setEvaluationDate_WithoutBankNOCdays(Float evaluationDate_WithoutBankNOCdays) {
			this.evaluationDate_WithoutBankNOCdays = evaluationDate_WithoutBankNOCdays;
		}
		public Float getContractAwardDate_WithoutBankNOCdays() {
			return contractAwardDate_WithoutBankNOCdays;
		}
		public void setContractAwardDate_WithoutBankNOCdays(Float contractAwardDate_WithoutBankNOCdays) {
			this.contractAwardDate_WithoutBankNOCdays = contractAwardDate_WithoutBankNOCdays;
		}
		public Float getContractCompletionDate_WithoutBankNOC_days() {
			return contractCompletionDate_WithoutBankNOC_days;
		}
		public void setContractCompletionDate_WithoutBankNOC_days(Float contractCompletionDate_WithoutBankNOC_days) {
			this.contractCompletionDate_WithoutBankNOC_days = contractCompletionDate_WithoutBankNOC_days;
		}
		public Integer getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(Integer createdBy) {
			this.createdBy = createdBy;
		}
		public Timestamp getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Timestamp createdOn) {
			this.createdOn = createdOn;
		}
		public Integer getModifiedBy() {
			return modifiedBy;
		}
		public void setModifiedBy(Integer modifiedBy) {
			this.modifiedBy = modifiedBy;
		}
		public Date getModifiedOn() {
			return modifiedOn;
		}
		public void setModifiedOn(Date modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
		public Float getLastdateofsubmissionofEOI() {
			return lastdateofsubmissionofEOI;
		}
		public void setLastdateofsubmissionofEOI(Float lastdateofsubmissionofEOI) {
			this.lastdateofsubmissionofEOI = lastdateofsubmissionofEOI;
		}
		public Float getShortlistingOfEOI() {
			return shortlistingOfEOI;
		}
		public void setShortlistingOfEOI(Float shortlistingOfEOI) {
			this.shortlistingOfEOI = shortlistingOfEOI;
		}
		public Float getrFPApprovalDate() {
			return rFPApprovalDate;
		}
		public void setrFPApprovalDate(Float rFPApprovalDate) {
			this.rFPApprovalDate = rFPApprovalDate;
		}
		public Float getFinancialEvaluationDate() {
			return financialEvaluationDate;
		}
		public void setFinancialEvaluationDate(Float financialEvaluationDate) {
			this.financialEvaluationDate = financialEvaluationDate;
		}
		public Float getPackageInitatedate() {
			return packageInitatedate;
		}
		public void setPackageInitatedate(Float packageInitatedate) {
			this.packageInitatedate = packageInitatedate;
		}

}
