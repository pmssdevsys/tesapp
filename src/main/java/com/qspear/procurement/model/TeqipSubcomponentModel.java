package com.qspear.procurement.model;

public class TeqipSubcomponentModel {

    private Integer subComponentId;
    private Integer componentId;
    private String subComponentCode;
    private String subComponentName;
    private String isactive;

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public String getSubComponentCode() {
        return subComponentCode;
    }

    public void setSubComponentCode(String subComponentCode) {
        this.subComponentCode = subComponentCode;
    }

    public String getSubComponentName() {
        return subComponentName;
    }

    public void setSubComponentName(String subComponentName) {
        this.subComponentName = subComponentName;
    }

    public Integer getSubComponentId() {
        return subComponentId;
    }

    public void setSubComponentId(Integer subComponentId) {
        this.subComponentId = subComponentId;
    }
}
