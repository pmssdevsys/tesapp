package com.qspear.procurement.model;

import com.qspear.procurement.persistence.TeqipStatemaster;

public class StateMaster {

    private Integer stateId;
    private String stateName;

    public StateMaster() {
    }

    public StateMaster(TeqipStatemaster teqipStatemaster) {
        this.stateId = teqipStatemaster.getStateId();
        this.stateName = teqipStatemaster.getStateName();
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

}
