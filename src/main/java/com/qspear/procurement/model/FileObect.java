/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model;

import java.io.File;

/**
 *
 * @author QCS
 */
public class FileObect {
    String fileName;
        File file;
        
        public FileObect(String fileName, File file) {
            this.fileName = fileName;
            this.file = file;
        }
        
        public String getFileName() {
            return fileName;
        }
        
        public File getFile() {
            return file;
        }
    
}
