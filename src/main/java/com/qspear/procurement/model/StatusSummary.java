/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jaspreet
 */
public class StatusSummary {

    String statusName;
    Integer procurementstageid;
        Integer isWaitingObject =0;

    List<StatusTimeLine> statusTimeLine = new ArrayList<>();

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public List<StatusTimeLine> getStatusTimeLine() {
        return statusTimeLine;
    }

    public void setStatusTimeLine(List<StatusTimeLine> statusTimeLine) {
        this.statusTimeLine = statusTimeLine;
    }

    public Integer getProcurementstageid() {
        return procurementstageid;
    }

    public void setProcurementstageid(Integer procurementstageid) {
        this.procurementstageid = procurementstageid;
    }
     public Integer getIsWaitingObject() {
        return isWaitingObject;
    }

    public void setIsWaitingObject(Integer isWaitingObject) {
        this.isWaitingObject = isWaitingObject;
    }


}
