package com.qspear.procurement.model;

public class TeqipProcurementmethodModel implements Comparable<TeqipProcurementmethodModel> {

    private Integer procurementmethodId;

    private String procurementmethodCode;

    private String procurementmethodName;
        
        public TeqipCategorymasterModel getTeqipCategorymasterModel() {
        return TeqipCategorymasterModel;
        }

	public void setTeqipCategorymasterModel(TeqipCategorymasterModel teqipCategorymasterModel) {
		TeqipCategorymasterModel = teqipCategorymasterModel;
	}

	
	private TeqipCategorymasterModel TeqipCategorymasterModel; 
        
         private String categoryName; 
    public TeqipProcurementmethodModel() {
    }

    public Integer getProcurementmethodId() {
        return this.procurementmethodId;
    }

    public void setProcurementmethodId(Integer procurementmethodId) {
        this.procurementmethodId = procurementmethodId;
    }

    public String getProcurementmethodCode() {
        return this.procurementmethodCode;
    }

    public void setProcurementmethodCode(String procurementmethodCode) {
        this.procurementmethodCode = procurementmethodCode;
    }

    public String getProcurementmethodName() {
        return this.procurementmethodName;
    }

    public void setProcurementmethodName(String procurementmethodName) {
        this.procurementmethodName = procurementmethodName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public int compareTo(TeqipProcurementmethodModel o) {
        String str1 = this.categoryName != null ? this.categoryName : "";
        String str2 = o != null && o.getCategoryName() != null ? o.getCategoryName() : "";
        return str1.compareTo(str2);
    }

}
