package com.qspear.procurement.model;

public class StateAllData implements Cloneable {

	private String SUBCOMPONENTCODE;
	private Integer InstitutionSubcomponentMapping_ID;
	private Integer INSTITUTION_ID;
	private Integer stateId;
	private String subcomponentname;
	public String getSubcomponentname() {
		return subcomponentname;
	}
	public void setSubcomponentname(String subcomponentname) {
		this.subcomponentname = subcomponentname;
	}
	private Integer subcomponentId;
	private Double allocatedbudget;
	private Double budgetforprocurment;
	private Double budgetforcivilworks;
	private Double budgetforservices;
	private String ALLOCATEDTYPE;
	private Byte isactive;
	public String getSUBCOMPONENTCODE() {
		return SUBCOMPONENTCODE;
	}
	public void setSUBCOMPONENTCODE(String sUBCOMPONENTCODE) {
		SUBCOMPONENTCODE = sUBCOMPONENTCODE;
	}
	public Integer getInstitutionSubcomponentMapping_ID() {
		return InstitutionSubcomponentMapping_ID;
	}
	public void setInstitutionSubcomponentMapping_ID(Integer institutionSubcomponentMapping_ID) {
		InstitutionSubcomponentMapping_ID = institutionSubcomponentMapping_ID;
	}
	public Integer getINSTITUTION_ID() {
		return INSTITUTION_ID;
	}
	public void setINSTITUTION_ID(Integer iNSTITUTION_ID) {
		INSTITUTION_ID = iNSTITUTION_ID;
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public Integer getSubcomponentId() {
		return subcomponentId;
	}
	public void setSubcomponentId(Integer subcomponentId) {
		this.subcomponentId = subcomponentId;
	}
	public Double getAllocatedbudget() {
		return allocatedbudget;
	}
	public void setAllocatedbudget(Double allocatedbudget) {
		this.allocatedbudget = allocatedbudget;
	}
	public Double getBudgetforprocurment() {
		return budgetforprocurment;
	}
	public void setBudgetforprocurment(Double budgetforprocurment) {
		this.budgetforprocurment = budgetforprocurment;
	}
	public Double getBudgetforcivilworks() {
		return budgetforcivilworks;
	}
	public void setBudgetforcivilworks(Double budgetforcivilworks) {
		this.budgetforcivilworks = budgetforcivilworks;
	}
	public Double getBudgetforservices() {
		return budgetforservices;
	}
	public void setBudgetforservices(Double budgetforservices) {
		this.budgetforservices = budgetforservices;
	}
	public String getALLOCATEDTYPE() {
		return ALLOCATEDTYPE;
	}
	public void setALLOCATEDTYPE(String aLLOCATEDTYPE) {
		ALLOCATEDTYPE = aLLOCATEDTYPE;
	}
	public Byte getIsactive() {
		return isactive;
	}
	public void setIsactive(Byte isactive) {
		this.isactive = isactive;
	}
	public StateAllData clone() throws CloneNotSupportedException{
		return (StateAllData) super.clone();
	}
	
	public StateAllData(Object[] ob) {
		setIsactive((Byte) ob[0]);

		setSubcomponentname((String) ob[1]);
		setInstitutionSubcomponentMapping_ID((Integer) ob[2]);
		setINSTITUTION_ID((Integer) ob[3]);

		setStateId((Integer) ob[4]);
		// t2.setSubcomponentId((Integer) ob[3]);
		setAllocatedbudget((Double) ob[5]);
		setBudgetforcivilworks((Double) ob[6]);
		setBudgetforservices((Double) ob[7]);
		setBudgetforprocurment((Double) ob[8]);
		setSUBCOMPONENTCODE((String) ob[9]);
		setSubcomponentId((Integer) ob[10]);

	}
	public StateAllData(){
		
	}
}
