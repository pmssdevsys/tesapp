package com.qspear.procurement.model;

import java.util.Date;

public class TeqipHelpdeskTicketModel {
	private Integer HDnTicketId;
	private Integer HDnTicketTypeId;
	private Integer HdnTicketPriorityId;
	private Integer HDnInstituteId;
	private Integer HDnTicketStatusId;
	private String HDcTitle;
	private String HDcDetails;
	private Integer CreatedBy;
	private String locatonname;
	private Date CreatedOn;
	private String HdnTicketnumber;
	private String UserName;
	private String UserPhone;
	private String UserMobile;
	private String UserEmail;
	private Integer HdnTicketSeverityId;
	private String type;
	private String priority;
	private String status;
	
	private String docName;
	
	public Integer getHDnTicketId() {
		return HDnTicketId;
	}

	public void setHDnTicketId(Integer hDnTicketId) {
		HDnTicketId = hDnTicketId;
	}

	public Integer getHDnTicketTypeId() {
		return HDnTicketTypeId;
	}

	public void setHDnTicketTypeId(Integer hDnTicketTypeId) {
		HDnTicketTypeId = hDnTicketTypeId;
	}

	public Integer getHdnTicketPriorityId() {
		return HdnTicketPriorityId;
	}

	public void setHdnTicketPriorityId(Integer hdnTicketPriorityId) {
		HdnTicketPriorityId = hdnTicketPriorityId;
	}

	public Integer getHDnInstituteId() {
		return HDnInstituteId;
	}

	public void setHDnInstituteId(Integer hDnInstituteId) {
		HDnInstituteId = hDnInstituteId;
	}

	public Integer getHDnTicketStatusId() {
		return HDnTicketStatusId;
	}

	public void setHDnTicketStatusId(Integer hDnTicketStatusId) {
		HDnTicketStatusId = hDnTicketStatusId;
	}

	public String getHDcTitle() {
		return HDcTitle;
	}

	public void setHDcTitle(String hDcTitle) {
		HDcTitle = hDcTitle;
	}

	public String getHDcDetails() {
		return HDcDetails;
	}

	public void setHDcDetails(String hDcDetails) {
		HDcDetails = hDcDetails;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public String getLocatonname() {
		return locatonname;
	}

	public void setLocatonname(String locatonname) {
		this.locatonname = locatonname;
	}

	public Date getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(Date createdOn) {
		CreatedOn = createdOn;
	}

	public String getHdnTicketnumber() {
		return HdnTicketnumber;
	}

	public void setHdnTicketnumber(String hdnTicketnumber) {
		HdnTicketnumber = hdnTicketnumber;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getUserPhone() {
		return UserPhone;
	}

	public void setUserPhone(String userPhone) {
		UserPhone = userPhone;
	}

	public String getUserMobile() {
		return UserMobile;
	}

	public void setUserMobile(String userMobile) {
		UserMobile = userMobile;
	}

	public String getUserEmail() {
		return UserEmail;
	}

	public void setUserEmail(String userEmail) {
		UserEmail = userEmail;
	}

	public Integer getHdnTicketSeverityId() {
		return HdnTicketSeverityId;
	}

	public void setHdnTicketSeverityId(Integer hdnTicketSeverityId) {
		HdnTicketSeverityId = hdnTicketSeverityId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	

	
	
}
