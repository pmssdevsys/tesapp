package com.qspear.procurement.model;

import java.sql.Timestamp;

import javax.persistence.Column;

public class NpiuDetailsMasterPojo {

	private Integer id;
	private String name;
	private String code;
	private String projectName;
	private String creditNumber;
	private String emailId;
	private Double allocatedBudget;
	private String address;
	private Double projectCost;
	private String faxNumber;
	private Integer isactive;
	private Integer createdBy;
	 private String description;
     public String getDescription() {
      return description;
     }

     public void setDescription(String description) {
      this.description = description;
     }
	
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Timestamp createdon) {
		this.createdon = createdon;
	}
	private Timestamp createdon;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getCreditNumber() {
		return creditNumber;
	}
	public void setCreditNumber(String creditNumber) {
		this.creditNumber = creditNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Double getAllocatedBudget() {
		return allocatedBudget;
	}
	public void setAllocatedBudget(Double allocatedBudget) {
		this.allocatedBudget = allocatedBudget;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Double getProjectCost() {
		return projectCost;
	}
	public void setProjectCost(Double projectCost) {
		this.projectCost = projectCost;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

}
