/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class PackageHistoryList {

    Integer instituteId;
    String instituteName;
    Integer stateId;
    String stateName;
    Date createdOn;
    Integer subComponentId;
    String subComponentName;
    String createdBy;
    Integer revisionId;
    String type;
    Integer id;

    public PackageHistoryList(Integer instituteId, String instituteName,
            Integer stateId, String stateName, Date createdOn, Integer subComponentId,
            String subComponentName, String createdBy, Integer revisionId) {
        this.instituteId = instituteId;
        this.instituteName = instituteName;
        this.stateId = stateId;
        this.stateName = stateName;
        this.createdOn = createdOn;
        this.subComponentId = subComponentId;
        this.subComponentName = subComponentName;
        this.createdBy = createdBy;
        this.revisionId = revisionId;
        if (instituteId != null) {
            id = instituteId;
            type = "INST";
        }
        if (stateId != null) {
            id = stateId;
            type = "SPFU";
        }

        if (instituteId == null && stateId == null) {
            id = 0;
            type = "NPIU";
        }

    }

    public Integer getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Integer instituteId) {
        this.instituteId = instituteId;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getSubComponentId() {
        return subComponentId;
    }

    public void setSubComponentId(Integer subComponentId) {
        this.subComponentId = subComponentId;
    }

    public String getSubComponentName() {
        return subComponentName;
    }

    public void setSubComponentName(String subComponentName) {
        this.subComponentName = subComponentName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getRevisionId() {
        return revisionId;
    }

    public void setRevisionId(Integer revisionId) {
        this.revisionId = revisionId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
