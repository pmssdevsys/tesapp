package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class ContractManagementCriteria {

    Integer isWccGenerated;
    Integer isWorkCompleted;
    String workOrderNumber;
    Integer isLiquidityDamagesWaived;
    Double liquidatedDamages;
    Double totalPaymentTillDate;
    Integer actualCompletionPeriod;
    String actualCompletionPeriodComment;
    Date actualPaymentDate;
    String actualPaymentDateComment;
    Double actualPaymentAmount;
    String actualPaymentAmountComment;
    Double retentionMoney;
    String checkDraftNumber;
    Double expectedPaymentAmountWithLD;

    String poNumber;
    Date actualCompletionDate;

    String comments;

    Integer deductionForMobilization;
    Integer isQualityCheckCarriedOut;
    String paymentMode;
    Date paymentModeDate;
    String bankName;
    String bankDetails;

    public Date getPaymentModeDate() {
        return paymentModeDate;
    }

    public void setPaymentModeDate(Date paymentModeDate) {
        this.paymentModeDate = paymentModeDate;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(String bankDetails) {
        this.bankDetails = bankDetails;
    }
    
    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Integer getIsWccGenerated() {
        return isWccGenerated;
    }

    public void setIsWccGenerated(Integer isWccGenerated) {
        this.isWccGenerated = isWccGenerated;
    }

    public Integer getIsWorkCompleted() {
        return isWorkCompleted;
    }

    public void setIsWorkCompleted(Integer isWorkCompleted) {
        this.isWorkCompleted = isWorkCompleted;
    }

    public String getWorkOrderNumber() {
        return workOrderNumber;
    }

    public void setWorkOrderNumber(String workOrderNumber) {
        this.workOrderNumber = workOrderNumber;
    }

    public Integer getIsLiquidityDamagesWaived() {
        return isLiquidityDamagesWaived;
    }

    public void setIsLiquidityDamagesWaived(Integer isLiquidityDamagesWaived) {
        this.isLiquidityDamagesWaived = isLiquidityDamagesWaived;
    }

    public Double getLiquidatedDamages() {
        return liquidatedDamages;
    }

    public void setLiquidatedDamages(Double liquidatedDamages) {
        this.liquidatedDamages = liquidatedDamages;
    }

    public Double getTotalPaymentTillDate() {
        return totalPaymentTillDate;
    }

    public void setTotalPaymentTillDate(Double totalPaymentTillDate) {
        this.totalPaymentTillDate = totalPaymentTillDate;
    }

    public Integer getActualCompletionPeriod() {
        return actualCompletionPeriod;
    }

    public void setActualCompletionPeriod(Integer actualCompletionPeriod) {
        this.actualCompletionPeriod = actualCompletionPeriod;
    }

    public String getActualCompletionPeriodComment() {
        return actualCompletionPeriodComment;
    }

    public void setActualCompletionPeriodComment(String actualCompletionPeriodComment) {
        this.actualCompletionPeriodComment = actualCompletionPeriodComment;
    }

    public Date getActualPaymentDate() {
        return actualPaymentDate;
    }

    public void setActualPaymentDate(Date actualPaymentDate) {
        this.actualPaymentDate = actualPaymentDate;
    }

    public String getActualPaymentDateComment() {
        return actualPaymentDateComment;
    }

    public void setActualPaymentDateComment(String actualPaymentDateComment) {
        this.actualPaymentDateComment = actualPaymentDateComment;
    }

    public Double getActualPaymentAmount() {
        return actualPaymentAmount;
    }

    public void setActualPaymentAmount(Double actualPaymentAmount) {
        this.actualPaymentAmount = actualPaymentAmount;
    }

    public String getActualPaymentAmountComment() {
        return actualPaymentAmountComment;
    }

    public void setActualPaymentAmountComment(String actualPaymentAmountComment) {
        this.actualPaymentAmountComment = actualPaymentAmountComment;
    }

    public Double getRetentionMoney() {
        return retentionMoney;
    }

    public void setRetentionMoney(Double retentionMoney) {
        this.retentionMoney = retentionMoney;
    }

    public String getCheckDraftNumber() {
        return checkDraftNumber;
    }

    public void setCheckDraftNumber(String checkDraftNumber) {
        this.checkDraftNumber = checkDraftNumber;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public Date getActualCompletionDate() {
        return actualCompletionDate;
    }

    public void setActualCompletionDate(Date actualCompletionDate) {
        this.actualCompletionDate = actualCompletionDate;
    }

    public Double getExpectedPaymentAmountWithLD() {
        return expectedPaymentAmountWithLD;
    }

    public void setExpectedPaymentAmountWithLD(Double expectedPaymentAmountWithLD) {
        this.expectedPaymentAmountWithLD = expectedPaymentAmountWithLD;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getDeductionForMobilization() {
        return deductionForMobilization;
    }

    public void setDeductionForMobilization(Integer deductionForMobilization) {
        this.deductionForMobilization = deductionForMobilization;
    }

    public Integer getIsQualityCheckCarriedOut() {
        return isQualityCheckCarriedOut;
    }

    public void setIsQualityCheckCarriedOut(Integer isQualityCheckCarriedOut) {
        this.isQualityCheckCarriedOut = isQualityCheckCarriedOut;
    }

}
