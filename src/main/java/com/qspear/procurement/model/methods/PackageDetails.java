package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 * Created by e1002703 on 4/2/2018.
 */
public class PackageDetails {

    String method;
    Integer packageId;
    String databaseId;
    String packageCode;
    String packageName;
    String category;
    Integer categoryId;
    Double estimatedCost;
    Date initiationDate;
    String description;
    String subCategory;
    Integer subCategoryId;
    String procMethod;
    Integer procMethodId;
    String subComponent;
    Integer subComponentId;
    Date actualFinancialSanctionDate;
    String quotationInvitationDate;
    String quotationOpeningDate;
    String contractAwardDate;
    String contractCompletionDate;
    String status;
    Integer serviceProviderId;
    Double methodThresholdValue;
    String purchaseType;
    Integer isOldPackage;
    String createdBy;
    String createdByRole;

    public String getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(String purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(Double estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public Date getInitiationDate() {
        return initiationDate;
    }

    public void setInitiationDate(Date initiationDate) {
        this.initiationDate = initiationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getProcMethod() {
        return procMethod;
    }

    public void setProcMethod(String procMethod) {
        this.procMethod = procMethod;
    }

    public Date getActualFinancialSanctionDate() {
        return actualFinancialSanctionDate;
    }

    public void setActualFinancialSanctionDate(Date actualFinancialSanctionDate) {
        this.actualFinancialSanctionDate = actualFinancialSanctionDate;
    }

    public String getQuotationInvitationDate() {
        return quotationInvitationDate;
    }

    public void setQuotationInvitationDate(String quotationInvitationDate) {
        this.quotationInvitationDate = quotationInvitationDate;
    }

    public String getQuotationOpeningDate() {
        return quotationOpeningDate;
    }

    public void setQuotationOpeningDate(String quotationOpeningDate) {
        this.quotationOpeningDate = quotationOpeningDate;
    }

    public String getContractAwardDate() {
        return contractAwardDate;
    }

    public void setContractAwardDate(String contractAwardDate) {
        this.contractAwardDate = contractAwardDate;
    }

    public String getContractCompletionDate() {
        return contractCompletionDate;
    }

    public void setContractCompletionDate(String contractCompletionDate) {
        this.contractCompletionDate = contractCompletionDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Integer getProcMethodId() {
        return procMethodId;
    }

    public void setProcMethodId(Integer procMethodId) {
        this.procMethodId = procMethodId;
    }

    public String getSubComponent() {
        return subComponent;
    }

    public void setSubComponent(String subComponent) {
        this.subComponent = subComponent;
    }

    public Integer getSubComponentId() {
        return subComponentId;
    }

    public void setSubComponentId(Integer subComponentId) {
        this.subComponentId = subComponentId;
    }

    public Integer getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(Integer serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public Double getMethodThresholdValue() {
        return methodThresholdValue;
    }

    public void setMethodThresholdValue(Double methodThresholdValue) {
        this.methodThresholdValue = methodThresholdValue;
    }

    public Integer getIsOldPackage() {
        return isOldPackage;
    }

    public void setIsOldPackage(Integer isOldPackage) {
        this.isOldPackage = isOldPackage;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByRole() {
        return createdByRole;
    }

    public void setCreatedByRole(String createdByRole) {
        this.createdByRole = createdByRole;
    }

}
