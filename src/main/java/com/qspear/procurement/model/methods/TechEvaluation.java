/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
public class TechEvaluation {

    TechEvaluationDetails details;
    ArrayList<QuotationEvaluationForm> evaluationForms;

    public TechEvaluationDetails getDetails() {
        return details;
    }

    public void setDetails(TechEvaluationDetails details) {
        this.details = details;
    }

    public ArrayList<QuotationEvaluationForm> getEvaluationForms() {
        return evaluationForms;
    }

    public void setEvaluationForms(ArrayList<QuotationEvaluationForm> evaluationForms) {
        this.evaluationForms = evaluationForms;
    }

}
