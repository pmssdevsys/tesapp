/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class ContractDetails {

    String memberInCharge;
    String contractNumber;
    Date contractStartDate;
    String authorizedRepresentativeOfClient;
    Date commencementServiceDate;
    Integer deliveryPeriodInMonths;
    String arbitrationProceedingHeldAt;
    Double interestPercentageOnDelayedPayments;
    Double basicValue;
    Double contractValue;
    Double applicableTax;

    public String getMemberInCharge() {
        return memberInCharge;
    }

    public void setMemberInCharge(String memberInCharge) {
        this.memberInCharge = memberInCharge;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public String getAuthorizedRepresentativeOfClient() {
        return authorizedRepresentativeOfClient;
    }

    public void setAuthorizedRepresentativeOfClient(String authorizedRepresentativeOfClient) {
        this.authorizedRepresentativeOfClient = authorizedRepresentativeOfClient;
    }

    public Date getCommencementServiceDate() {
        return commencementServiceDate;
    }

    public void setCommencementServiceDate(Date commencementServiceDate) {
        this.commencementServiceDate = commencementServiceDate;
    }

    public Integer getDeliveryPeriodInMonths() {
        return deliveryPeriodInMonths;
    }

    public void setDeliveryPeriodInMonths(Integer deliveryPeriodInMonths) {
        this.deliveryPeriodInMonths = deliveryPeriodInMonths;
    }

    public String getArbitrationProceedingHeldAt() {
        return arbitrationProceedingHeldAt;
    }

    public void setArbitrationProceedingHeldAt(String arbitrationProceedingHeldAt) {
        this.arbitrationProceedingHeldAt = arbitrationProceedingHeldAt;
    }

    public Double getInterestPercentageOnDelayedPayments() {
        return interestPercentageOnDelayedPayments;
    }

    public void setInterestPercentageOnDelayedPayments(Double interestPercentageOnDelayedPayments) {
        this.interestPercentageOnDelayedPayments = interestPercentageOnDelayedPayments;
    }

    public Double getBasicValue() {
        return basicValue;
    }

    public void setBasicValue(Double basicValue) {
        this.basicValue = basicValue;
    }

    public Double getContractValue() {
        return contractValue;
    }

    public void setContractValue(Double contractValue) {
        this.contractValue = contractValue;
    }

    public Double getApplicableTax() {
        return applicableTax;
    }

    public void setApplicableTax(Double applicableTax) {
        this.applicableTax = applicableTax;
    }
      public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

}
