/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class AmendmentDetail {

    Integer isAmmendmentRequired;
    Integer isAmmendmentComplete;
    Date maintainencePeriodExpiryDate;
    Date workCompletionDate;
    Double performanceSecurityAmount;
    Date performanceSecurityExpiryDate;

    public Date getWorkCompletionDate() {
        return workCompletionDate;
    }

    public void setWorkCompletionDate(Date workCompletionDate) {
        this.workCompletionDate = workCompletionDate;
    }

    public Double getPerformanceSecurityAmount() {
        return performanceSecurityAmount;
    }

    public void setPerformanceSecurityAmount(Double performanceSecurityAmount) {
        this.performanceSecurityAmount = performanceSecurityAmount;
    }

    public Integer getIsAmmendmentRequired() {
        return isAmmendmentRequired;
    }

    public void setIsAmmendmentRequired(Integer isAmmendmentRequired) {
        this.isAmmendmentRequired = isAmmendmentRequired;
    }

    public Date getMaintainencePeriodExpiryDate() {
        return maintainencePeriodExpiryDate;
    }

    public void setMaintainencePeriodExpiryDate(Date maintainencePeriodExpiryDate) {
        this.maintainencePeriodExpiryDate = maintainencePeriodExpiryDate;
    }

    public Date getPerformanceSecurityExpiryDate() {
        return performanceSecurityExpiryDate;
    }

    public void setPerformanceSecurityExpiryDate(Date performanceSecurityExpiryDate) {
        this.performanceSecurityExpiryDate = performanceSecurityExpiryDate;
    }

    public Integer getIsAmmendmentComplete() {
        return isAmmendmentComplete;
    }

    public void setIsAmmendmentComplete(Integer isAmmendmentComplete) {
        this.isAmmendmentComplete = isAmmendmentComplete;
    }

}
