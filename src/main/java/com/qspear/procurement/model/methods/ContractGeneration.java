/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
public class ContractGeneration {

    RecommendationDetails recommendationDetails;
    NegitiationDetails negitiationDetails;
    ContractDetails contractDetails;
    AwardGenerationChecklists awardGenerationChecklists;
    ArrayList<Uploads> cGeneration;
    FinancialProposalDetails financialProposalDetails;

    public RecommendationDetails getRecommendationDetails() {
        return recommendationDetails;
    }

    public void setRecommendationDetails(RecommendationDetails recommendationDetails) {
        this.recommendationDetails = recommendationDetails;
    }

    public NegitiationDetails getNegitiationDetails() {
        return negitiationDetails;
    }

    public void setNegitiationDetails(NegitiationDetails negitiationDetails) {
        this.negitiationDetails = negitiationDetails;
    }

    public ContractDetails getContractDetails() {
        return contractDetails;
    }

    public void setContractDetails(ContractDetails contractDetails) {
        this.contractDetails = contractDetails;
    }

    public AwardGenerationChecklists getAwardGenerationChecklists() {
        return awardGenerationChecklists;
    }

    public void setAwardGenerationChecklists(AwardGenerationChecklists awardGenerationChecklists) {
        this.awardGenerationChecklists = awardGenerationChecklists;
    }

    public ArrayList<Uploads> getcGeneration() {
        return cGeneration;
    }

    public void setcGeneration(ArrayList<Uploads> cGeneration) {
        this.cGeneration = cGeneration;
    }

    public FinancialProposalDetails getFinancialProposalDetails() {
        return financialProposalDetails;
    }

    public void setFinancialProposalDetails(FinancialProposalDetails financialProposalDetails) {
        this.financialProposalDetails = financialProposalDetails;
    }

}
