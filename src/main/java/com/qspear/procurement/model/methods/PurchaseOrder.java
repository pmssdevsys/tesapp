package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class PurchaseOrder {

    GeneratePO generatePo;
    ArrayList<PurchaseOrderItems> items;
    ArrayList<Uploads> poGeneration;

    public GeneratePO getGeneratePo() {
        return generatePo;
    }

    public void setGeneratePo(GeneratePO generatePo) {
        this.generatePo = generatePo;
    }

    public ArrayList<PurchaseOrderItems> getItems() {
        return items;
    }

    public void setItems(ArrayList<PurchaseOrderItems> items) {
        this.items = items;
    }

    public ArrayList<Uploads> getPoGeneration() {
        return poGeneration;
    }

    public void setPoGeneration(ArrayList<Uploads> poGeneration) {
        this.poGeneration = poGeneration;
    }

}
