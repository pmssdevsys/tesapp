/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class ProposalExtension {

    Date currentSubmissionDate;
    String reasonOfExtension;
    Date extendedDate;

    public Date getCurrentSubmissionDate() {
        return currentSubmissionDate;
    }

    public void setCurrentSubmissionDate(Date currentSubmissionDate) {
        this.currentSubmissionDate = currentSubmissionDate;
    }

    public String getReasonOfExtension() {
        return reasonOfExtension;
    }

    public void setReasonOfExtension(String reasonOfExtension) {
        this.reasonOfExtension = reasonOfExtension;
    }

    public Date getExtendedDate() {
        return extendedDate;
    }

    public void setExtendedDate(Date extendedDate) {
        this.extendedDate = extendedDate;
    }

}
