/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class GRNDetail {

    Integer packageGRNDetailId;
    Integer supplierId;
    String supplierName;
    Date supplyDate;
    String grnNumber;
    ArrayList<GRNItem> grnItems;
    Uploads grnUpload;
    Uploads assetUpload;
    ArrayList<Uploads> grnlGeneration;
    ArrayList<Uploads> alGeneration;

    public Integer getPackageGRNDetailId() {
        return packageGRNDetailId;
    }

    public void setPackageGRNDetailId(Integer packageGRNDetailId) {
        this.packageGRNDetailId = packageGRNDetailId;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Date getSupplyDate() {
        return supplyDate;
    }

    public void setSupplyDate(Date supplyDate) {
        this.supplyDate = supplyDate;
    }

    public String getGrnNumber() {
        return grnNumber;
    }

    public void setGrnNumber(String grnNumber) {
        this.grnNumber = grnNumber;
    }

    public ArrayList<GRNItem> getGrnItems() {
        return grnItems;
    }

    public void setGrnItems(ArrayList<GRNItem> grnItems) {
        this.grnItems = grnItems;
    }

    public Uploads getGrnUpload() {
        return grnUpload;
    }

    public void setGrnUpload(Uploads grnUpload) {
        this.grnUpload = grnUpload;
    }

    public Uploads getAssetUpload() {
        return assetUpload;
    }

    public void setAssetUpload(Uploads assetUpload) {
        this.assetUpload = assetUpload;
    }

    public ArrayList<Uploads> getGrnlGeneration() {
        return grnlGeneration;
    }

    public void setGrnlGeneration(ArrayList<Uploads> grnlGeneration) {
        this.grnlGeneration = grnlGeneration;
    }

    public ArrayList<Uploads> getAlGeneration() {
        return alGeneration;
    }

    public void setAlGeneration(ArrayList<Uploads> alGeneration) {
        this.alGeneration = alGeneration;
    }

}
