/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
public class RFP {

    RFPDetail details;
    RfpDocument rfpDocument;
    ArrayList<Uploads> rfpGeneration;

    public RFPDetail getDetails() {
        return details;
    }

    public void setDetails(RFPDetail details) {
        this.details = details;
    }

    public RfpDocument getRfpDocument() {
        return rfpDocument;
    }

    public void setRfpDocument(RfpDocument rfpDocument) {
        this.rfpDocument = rfpDocument;
    }

    public ArrayList<Uploads> getRfpGeneration() {
        return rfpGeneration;
    }

    public void setRfpGeneration(ArrayList<Uploads> rfpGeneration) {
        this.rfpGeneration = rfpGeneration;
    }

}
