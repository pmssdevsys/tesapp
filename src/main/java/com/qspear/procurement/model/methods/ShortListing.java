/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class ShortListing {

    Integer id;
    Date eoiOpeningDate;
    String remarks;
    ArrayList<ShortlistedConsultants> shortlistedConsultants;
    Date dateOfShortlistingFirms;
    ArrayList<Uploads> eoiGeneration;

    public Date getDateOfShortlistingFirms() {
        return dateOfShortlistingFirms;
    }

    public void setDateOfShortlistingFirms(Date dateOfShortlistingFirms) {
        this.dateOfShortlistingFirms = dateOfShortlistingFirms;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getEoiOpeningDate() {
        return eoiOpeningDate;
    }

    public void setEoiOpeningDate(Date eoiOpeningDate) {
        this.eoiOpeningDate = eoiOpeningDate;
    }

    public ArrayList<ShortlistedConsultants> getShortlistedConsultants() {
        return shortlistedConsultants;
    }

    public void setShortlistedConsultants(ArrayList<ShortlistedConsultants> shortlistedConsultants) {
        this.shortlistedConsultants = shortlistedConsultants;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public ArrayList<Uploads> getEoiGeneration() {
        return eoiGeneration;
    }

    public void setEoiGeneration(ArrayList<Uploads> eoiGeneration) {
            this.eoiGeneration = eoiGeneration;
    }

}
