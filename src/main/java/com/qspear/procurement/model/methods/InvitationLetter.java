package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 * Created by e1002703 on 4/2/2018.
 */
public class InvitationLetter {

    InvitationLetterDetail details;
    ArrayList<Uploads> ilGeneration;

    public InvitationLetterDetail getDetails() {
        return details;
    }

    public void setDetails(InvitationLetterDetail details) {
        this.details = details;
    }

    public ArrayList<Uploads> getIlGeneration() {
        return ilGeneration;
    }

    public void setIlGeneration(ArrayList<Uploads> ilGeneration) {
        this.ilGeneration = ilGeneration;
    }
}
