/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class NegitiationDetails {

    Integer l1SuplierId;
    String l1SupplierName;
    String recommendationComments;
    Double contractValue;
    String negotiationComments;
    Integer isNegotiationSuccessful;
    Date dateOfNegotiation;

    public Date getDateOfNegotiation() {
        return dateOfNegotiation;
    }

    public void setDateOfNegotiation(Date dateOfNegotiation) {
        this.dateOfNegotiation = dateOfNegotiation;
    }

    public Integer getL1SuplierId() {
        return l1SuplierId;
    }

    public void setL1SuplierId(Integer l1SuplierId) {
        this.l1SuplierId = l1SuplierId;
    }

    public String getL1SupplierName() {
        return l1SupplierName;
    }

    public void setL1SupplierName(String l1SupplierName) {
        this.l1SupplierName = l1SupplierName;
    }

    public String getRecommendationComments() {
        return recommendationComments;
    }

    public void setRecommendationComments(String recommendationComments) {
        this.recommendationComments = recommendationComments;
    }

    public Double getContractValue() {
        return contractValue;
    }

    public void setContractValue(Double contractValue) {
        this.contractValue = contractValue;
    }

    public String getNegotiationComments() {
        return negotiationComments;
    }

    public void setNegotiationComments(String negotiationComments) {
        this.negotiationComments = negotiationComments;
    }

    public Integer getIsNegotiationSuccessful() {
        return isNegotiationSuccessful;
    }

    public void setIsNegotiationSuccessful(Integer isNegotiationSuccessful) {
        this.isNegotiationSuccessful = isNegotiationSuccessful;
    }

}
