/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class GenerateContract {

    Integer supplierId;
    String supplierName;
    String po;
    Date contractStartDate;
    Date contractDocumentSigned;
    Date expectedDeliveryDate;
    Date securityRvcdDate;
    Double securityAmount;
    Integer isArbitratorAgreed;
    String securityInstrument;
    Double vatPercent;
    Date securityExpDate;
    Double octroi;
    Double evaluatedPrice;
    Double baseCost;
    Integer isMobilizationAdvancePaid;
    Integer isBankSecurityGiven;
    Double mobilizationAmount;
    Double sumOfAllApplicableTaxes;
    Double totalContractValue;
    Date workStartDate;
    Date workCompletionDate;
    Date maintainencePeriodExpiryDate;

    public Date getMaintainencePeriodExpiryDate() {
        return maintainencePeriodExpiryDate;
    }

    public void setMaintainencePeriodExpiryDate(Date maintainencePeriodExpiryDate) {
        this.maintainencePeriodExpiryDate = maintainencePeriodExpiryDate;
    }
    
    
    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getPo() {
        return po;
    }

    public void setPo(String po) {
        this.po = po;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public Date getContractDocumentSigned() {
        return contractDocumentSigned;
    }

    public void setContractDocumentSigned(Date contractDocumentSigned) {
        this.contractDocumentSigned = contractDocumentSigned;
    }

    public Date getSecurityRvcdDate() {
        return securityRvcdDate;
    }

    public void setSecurityRvcdDate(Date securityRvcdDate) {
        this.securityRvcdDate = securityRvcdDate;
    }

    public Double getSecurityAmount() {
        return securityAmount;
    }

    public void setSecurityAmount(Double securityAmount) {
        this.securityAmount = securityAmount;
    }

    public Integer getIsArbitratorAgreed() {
        return isArbitratorAgreed;
    }

    public void setIsArbitratorAgreed(Integer isArbitratorAgreed) {
        this.isArbitratorAgreed = isArbitratorAgreed;
    }

    public String getSecurityInstrument() {
        return securityInstrument;
    }

    public void setSecurityInstrument(String securityInstrument) {
        this.securityInstrument = securityInstrument;
    }

    public Double getVatPercent() {
        return vatPercent;
    }

    public void setVatPercent(Double vatPercent) {
        this.vatPercent = vatPercent;
    }

    public Date getSecurityExpDate() {
        return securityExpDate;
    }

    public void setSecurityExpDate(Date securityExpDate) {
        this.securityExpDate = securityExpDate;
    }

    public Double getOctroi() {
        return octroi;
    }

    public void setOctroi(Double octroi) {
        this.octroi = octroi;
    }

    public Double getEvaluatedPrice() {
        return evaluatedPrice;
    }

    public void setEvaluatedPrice(Double evaluatedPrice) {
        this.evaluatedPrice = evaluatedPrice;
    }

    public Double getBaseCost() {
        return baseCost;
    }

    public void setBaseCost(Double baseCost) {
        this.baseCost = baseCost;
    }

    public Integer getIsMobilizationAdvancePaid() {
        return isMobilizationAdvancePaid;
    }

    public void setIsMobilizationAdvancePaid(Integer isMobilizationAdvancePaid) {
        this.isMobilizationAdvancePaid = isMobilizationAdvancePaid;
    }

    public Integer getIsBankSecurityGiven() {
        return isBankSecurityGiven;
    }

    public void setIsBankSecurityGiven(Integer isBankSecurityGiven) {
        this.isBankSecurityGiven = isBankSecurityGiven;
    }

    public Double getMobilizationAmount() {
        return mobilizationAmount;
    }

    public void setMobilizationAmount(Double mobilizationAmount) {
        this.mobilizationAmount = mobilizationAmount;
    }

    public Double getSumOfAllApplicableTaxes() {
        return sumOfAllApplicableTaxes;
    }

    public void setSumOfAllApplicableTaxes(Double sumOfAllApplicableTaxes) {
        this.sumOfAllApplicableTaxes = sumOfAllApplicableTaxes;
    }

    public Double getTotalContractValue() {
        return totalContractValue;
    }

    public void setTotalContractValue(Double totalContractValue) {
        this.totalContractValue = totalContractValue;
    }

    public Date getWorkStartDate() {
        return workStartDate;
    }

    public void setWorkStartDate(Date workStartDate) {
        this.workStartDate = workStartDate;
    }

    public Date getWorkCompletionDate() {
        return workCompletionDate;
    }

    public void setWorkCompletionDate(Date workCompletionDate) {
        this.workCompletionDate = workCompletionDate;
    }

}
