package com.qspear.procurement.model.methods;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by e1002703 on 4/2/2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Questions {

    Integer packageQuestionId;
    String question;
    Integer decision;
    String value;
    String questionNonResponsiveValue;
    Integer isStandardQuestion;
    String type;
    String options;
    String userResponse;
    String reasonForNonResponsiveNess;
    Integer isEvaluatedPrice;
    Integer isReadOutPrice;
    String questionCategory;
    Integer packageQuotationEvaluationId;
    String minValue;
    String maxValue;

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getDecision() {
        return decision;
    }

    public void setDecision(Integer decision) {
        this.decision = decision;
    }

    public String getQuestionNonResponsiveValue() {
        return questionNonResponsiveValue;
    }

    public void setQuestionNonResponsiveValue(String questionNonResponsiveValue) {
        this.questionNonResponsiveValue = questionNonResponsiveValue;
    }

    public Integer getPackageQuestionId() {
        return packageQuestionId;
    }

    public void setPackageQuestionId(Integer packageQuestionId) {
        this.packageQuestionId = packageQuestionId;
    }

    public Integer getIsStandardQuestion() {
        return isStandardQuestion;
    }

    public void setIsStandardQuestion(Integer isStandardQuestion) {
        this.isStandardQuestion = isStandardQuestion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(String userResponse) {
        this.userResponse = userResponse;
    }

    public String getReasonForNonResponsiveNess() {
        return reasonForNonResponsiveNess;
    }

    public void setReasonForNonResponsiveNess(String reasonForNonResponsiveNess) {
        this.reasonForNonResponsiveNess = reasonForNonResponsiveNess;
    }

    public Integer getIsEvaluatedPrice() {
        return isEvaluatedPrice;
    }

    public void setIsEvaluatedPrice(Integer isEvaluatedPrice) {
        this.isEvaluatedPrice = isEvaluatedPrice;
    }

    public Integer getIsReadOutPrice() {
        return isReadOutPrice;
    }

    public void setIsReadOutPrice(Integer isReadOutPrice) {
        this.isReadOutPrice = isReadOutPrice;
    }

    public String getQuestionCategory() {
        return questionCategory;
    }

    public void setQuestionCategory(String questionCategory) {
        this.questionCategory = questionCategory;
    }

    public Integer getPackageQuotationEvaluationId() {
        return packageQuotationEvaluationId;
    }

    public void setPackageQuotationEvaluationId(Integer packageQuotationEvaluationId) {
        this.packageQuotationEvaluationId = packageQuotationEvaluationId;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

}
