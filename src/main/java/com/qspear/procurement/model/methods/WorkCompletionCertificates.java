/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class WorkCompletionCertificates {

    Integer workCompletionCertificateId;
    Date actualDateOfCompletion;
    String workDone;
    String balanceWork;
    String comments;
    String woNumber;

    public Integer getWorkCompletionCertificateId() {
        return workCompletionCertificateId;
    }

    public void setWorkCompletionCertificateId(Integer workCompletionCertificateId) {
        this.workCompletionCertificateId = workCompletionCertificateId;
    }

    public Date getActualDateOfCompletion() {
        return actualDateOfCompletion;
    }

    public void setActualDateOfCompletion(Date actualDateOfCompletion) {
        this.actualDateOfCompletion = actualDateOfCompletion;
    }

    public String getWorkDone() {
        return workDone;
    }

    public void setWorkDone(String workDone) {
        this.workDone = workDone;
    }

    public String getBalanceWork() {
        return balanceWork;
    }

    public void setBalanceWork(String balanceWork) {
        this.balanceWork = balanceWork;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getWoNumber() {
        return woNumber;
    }

    public void setWoNumber(String woNumber) {
        this.woNumber = woNumber;
    }

}
