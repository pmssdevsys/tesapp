/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class PreBidMeetingInformation {

    String packageCode;
    String packageName;
    Date prebidMeetingDate;
    Date prebidMeetingTime;
    Date actualMeetingDate;
    Date meetingClarificationDate;
    Integer isCorregendum;
    Integer isClarifiationIssued;
    String prebidMOM;

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Date getPrebidMeetingDate() {
        return prebidMeetingDate;
    }

    public void setPrebidMeetingDate(Date prebidMeetingDate) {
        this.prebidMeetingDate = prebidMeetingDate;
    }

    public Date getPrebidMeetingTime() {
        return prebidMeetingTime;
    }

    public void setPrebidMeetingTime(Date prebidMeetingTime) {
        this.prebidMeetingTime = prebidMeetingTime;
    }

    public Date getActualMeetingDate() {
        return actualMeetingDate;
    }

    public void setActualMeetingDate(Date actualMeetingDate) {
        this.actualMeetingDate = actualMeetingDate;
    }

    public Date getMeetingClarificationDate() {
        return meetingClarificationDate;
    }

    public void setMeetingClarificationDate(Date meetingClarificationDate) {
        this.meetingClarificationDate = meetingClarificationDate;
    }

    public Integer getIsCorregendum() {
        return isCorregendum;
    }

    public void setIsCorregendum(Integer isCorregendum) {
        this.isCorregendum = isCorregendum;
    }

    public Integer getIsClarifiationIssued() {
        return isClarifiationIssued;
    }

    public void setIsClarifiationIssued(Integer isClarifiationIssued) {
        this.isClarifiationIssued = isClarifiationIssued;
    }

    public String getPrebidMOM() {
        return prebidMOM;
    }

    public void setPrebidMOM(String prebidMOM) {
        this.prebidMOM = prebidMOM;
    }

}
