/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class AdvDetails {

    String localPaperName;
    String nationalPaperName;
    Date dateOfReleaseOfAdv;
    Date actualPublicationLocalDate;
    Date actualPublicationNationalDate;
    Date eoiSubmissionDate;
    
    public String getLocalPaperName() {
        return localPaperName;
    }

    public void setLocalPaperName(String localPaperName) {
        this.localPaperName = localPaperName;
    }

    public String getNationalPaperName() {
        return nationalPaperName;
    }

    public void setNationalPaperName(String nationalPaperName) {
        this.nationalPaperName = nationalPaperName;
    }

    public Date getDateOfReleaseOfAdv() {
        return dateOfReleaseOfAdv;
    }

    public void setDateOfReleaseOfAdv(Date dateOfReleaseOfAdv) {
        this.dateOfReleaseOfAdv = dateOfReleaseOfAdv;
    }

    public Date getActualPublicationLocalDate() {
        return actualPublicationLocalDate;
    }

    public void setActualPublicationLocalDate(Date actualPublicationLocalDate) {
        this.actualPublicationLocalDate = actualPublicationLocalDate;
    }

    public Date getActualPublicationNationalDate() {
        return actualPublicationNationalDate;
    }

    public void setActualPublicationNationalDate(Date actualPublicationNationalDate) {
        this.actualPublicationNationalDate = actualPublicationNationalDate;
    }

    public Date getEoiSubmissionDate() {
        return eoiSubmissionDate;
    }

    public void setEoiSubmissionDate(Date eoiSubmissionDate) {
        this.eoiSubmissionDate = eoiSubmissionDate;
    }
    

}
