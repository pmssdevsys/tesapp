package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class QuotationEvaluation {

    Integer l1Bidder;
    String selectedContractorForPostQualification;

    ArrayList<QuotationEvaluationForm> evaluationForms;

    ArrayList<QuotationForm> postQualificationForms;

    ArrayList<Uploads> qeGeneration;

    public Integer getL1Bidder() {
        return l1Bidder;
    }

    public void setL1Bidder(Integer l1Bidder) {
        this.l1Bidder = l1Bidder;
    }

    public String getSelectedContractorForPostQualification() {
        return selectedContractorForPostQualification;
    }

    public void setSelectedContractorForPostQualification(String selectedContractorForPostQualification) {
        this.selectedContractorForPostQualification = selectedContractorForPostQualification;
    }

    public ArrayList<QuotationEvaluationForm> getEvaluationForms() {
        return evaluationForms;
    }

    public void setEvaluationForms(ArrayList<QuotationEvaluationForm> evaluationForms) {
        this.evaluationForms = evaluationForms;
    }

    public ArrayList<QuotationForm> getPostQualificationForms() {
        return postQualificationForms;
    }

    public void setPostQualificationForms(ArrayList<QuotationForm> postQualificationForms) {
        this.postQualificationForms = postQualificationForms;
    }

    public ArrayList<Uploads> getQeGeneration() {
        return qeGeneration;
    }

    public void setQeGeneration(ArrayList<Uploads> qeGeneration) {
        this.qeGeneration = qeGeneration;
    }

}
