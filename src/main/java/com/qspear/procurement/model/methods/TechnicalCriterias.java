/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TechnicalCriterias {

    Integer id;
    String description;
    Double marks;
    Double consultantMarks;
    ArrayList<SubCriteria> subCriteria;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMarks() {
        return marks;
    }

    public void setMarks(Double marks) {
        this.marks = marks;
    }

    public ArrayList<SubCriteria> getSubCriteria() {
        return subCriteria;
    }

    public void setSubCriteria(ArrayList<SubCriteria> subCriteria) {
        this.subCriteria = subCriteria;
    }

    public Double getConsultantMarks() {
        return consultantMarks;
    }

    public void setConsultantMarks(Double consultantMarks) {
        this.consultantMarks = consultantMarks;
    }

}
