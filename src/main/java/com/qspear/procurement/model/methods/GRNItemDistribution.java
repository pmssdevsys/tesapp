/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

/**
 *
 * @author jaspreet
 */
public class GRNItemDistribution {

    Integer packageGRNItemDistributionId;
    Integer departmentId;
    String departmentName;
    Double quantity;

    public Integer getPackageGRNItemDistributionId() {
        return packageGRNItemDistributionId;
    }

    public void setPackageGRNItemDistributionId(Integer packageGRNItemDistributionId) {
        this.packageGRNItemDistributionId = packageGRNItemDistributionId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

}
