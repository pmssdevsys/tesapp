/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
public class TechEvaluationDetail {

    ArrayList<TechnicalCriterias> technicalCriterias;

    public ArrayList<TechnicalCriterias> getTechnicalCriterias() {
        return technicalCriterias;
    }

    public void setTechnicalCriterias(ArrayList<TechnicalCriterias> technicalCriterias) {
        this.technicalCriterias = technicalCriterias;
    }

}
