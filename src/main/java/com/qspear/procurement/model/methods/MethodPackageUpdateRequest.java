package com.qspear.procurement.model.methods;

/**
 * Created by e1002703 on 4/4/2018.
 */
public class MethodPackageUpdateRequest extends MethodPackageResponse {
    boolean isPartial;
    boolean isNew;

    public boolean isPartial() {
        return isPartial;
    }

    public void setPartial(boolean partial) {
        isPartial = partial;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }
}
