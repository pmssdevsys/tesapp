package com.qspear.procurement.model.methods;

import com.qspear.procurement.model.TeqipItemWorkDetailModel;
import java.util.ArrayList;

/**
 * Created by e1002703 on 4/2/2018.
 */
public class ProcuringDepartmentDetails {

    String departmentName;
    ArrayList<Items> items;
    ArrayList<TeqipItemWorkDetailModel> works;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Items> getItems() {
        return items;
    }

    public void setItems(ArrayList<Items> items) {
        this.items = items;
    }

    public ArrayList<TeqipItemWorkDetailModel> getWorks() {
        return works;
    }

    public void setWorks(ArrayList<TeqipItemWorkDetailModel> works) {
        this.works = works;
    }

}
