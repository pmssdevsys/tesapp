package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class CompleteCheckList {

    private Integer packageCompleteCheckListId;

    private Integer supplierId;
    
    private String supplierName;
    
    private Integer goodReceived;

    private Integer paymentDone;

    private Integer installationDone;

    private Integer trainingDone;

    private Date procurementDate;

    private Integer workCompleted;

    private Integer envchecklistUpdated;

    public Integer getGoodReceived() {
        return goodReceived;
    }

    public void setGoodReceived(Integer goodReceived) {
        this.goodReceived = goodReceived;
    }

    public Integer getPaymentDone() {
        return paymentDone;
    }

    public void setPaymentDone(Integer paymentDone) {
        this.paymentDone = paymentDone;
    }

    public Integer getInstallationDone() {
        return installationDone;
    }

    public void setInstallationDone(Integer installationDone) {
        this.installationDone = installationDone;
    }

    public Integer getTrainingDone() {
        return trainingDone;
    }

    public void setTrainingDone(Integer trainingDone) {
        this.trainingDone = trainingDone;
    }

    public Date getProcurementDate() {
        return procurementDate;
    }

    public void setProcurementDate(Date procurementDate) {
        this.procurementDate = procurementDate;
    }

    public Integer getWorkCompleted() {
        return workCompleted;
    }

    public void setWorkCompleted(Integer workCompleted) {
        this.workCompleted = workCompleted;
    }

    public Integer getEnvchecklistUpdated() {
        return envchecklistUpdated;
    }

    public void setEnvchecklistUpdated(Integer envchecklistUpdated) {
        this.envchecklistUpdated = envchecklistUpdated;
    }

    public Integer getPackageCompleteCheckListId() {
        return packageCompleteCheckListId;
    }

    public void setPackageCompleteCheckListId(Integer packageCompleteCheckListId) {
        this.packageCompleteCheckListId = packageCompleteCheckListId;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    
}
