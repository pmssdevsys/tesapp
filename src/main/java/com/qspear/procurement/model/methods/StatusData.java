/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

/**
 *
 * @author jaspreet
 */
public class StatusData {

    StatusInformation information;
    StatusSummary summary;

    public StatusInformation getInformation() {
        return information;
    }

    public void setInformation(StatusInformation information) {
        this.information = information;
    }

    public StatusSummary getSummary() {
        return summary;
    }

    public void setSummary(StatusSummary summary) {
        this.summary = summary;
    }

}
