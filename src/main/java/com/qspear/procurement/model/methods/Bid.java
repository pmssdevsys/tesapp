/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
public class Bid {

    BidDetail bidDetail;
    BidINSTAndSCC sccInformation;
    ArrayList<Uploads> bidGeneration;

    public BidDetail getBidDetail() {
        return bidDetail;
    }

    public void setBidDetail(BidDetail bidDetail) {
        this.bidDetail = bidDetail;
    }

    public BidINSTAndSCC getSccInformation() {
        return sccInformation;
    }

    public void setSccInformation(BidINSTAndSCC sccInformation) {
        this.sccInformation = sccInformation;
    }

    public ArrayList<Uploads> getBidGeneration() {
        return bidGeneration;
    }

    public void setBidGeneration(ArrayList<Uploads> bidGeneration) {
        this.bidGeneration = bidGeneration;
    }

}
