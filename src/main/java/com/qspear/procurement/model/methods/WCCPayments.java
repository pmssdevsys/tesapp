/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.List;

/**
 *
 * @author jaspreet
 */
public class WCCPayments {

    List<WorkCompletionCertificates> workCompletionCertificates;
    AmendmentDetail ammendmentDetails;

    public List<WorkCompletionCertificates> getWorkCompletionCertificates() {
        return workCompletionCertificates;
    }

    public void setWorkCompletionCertificates(List<WorkCompletionCertificates> workCompletionCertificates) {
        this.workCompletionCertificates = workCompletionCertificates;
    }

    public AmendmentDetail getAmmendmentDetails() {
        return ammendmentDetails;
    }

    public void setAmmendmentDetails(AmendmentDetail ammendmentDetails) {
        this.ammendmentDetails = ammendmentDetails;
    }

}
