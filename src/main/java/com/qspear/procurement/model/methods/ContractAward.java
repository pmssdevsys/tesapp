/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class ContractAward {

    Date contractSignDate;
    ArrayList<Uploads> pGeneration;

    public Date getContractSignDate() {
        return contractSignDate;
    }

    public void setContractSignDate(Date contractSignDate) {
        this.contractSignDate = contractSignDate;
    }

    public ArrayList<Uploads> getpGeneration() {
        return pGeneration;
    }

    public void setpGeneration(ArrayList<Uploads> pGeneration) {
        this.pGeneration = pGeneration;
    }

}
