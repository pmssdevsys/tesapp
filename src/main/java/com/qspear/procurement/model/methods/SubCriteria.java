/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author jaspreet
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubCriteria {

    Integer id;
    Integer consultantEvaluationId;
    String subCriteriaQuestionText;
    Double marks;
    Double consultantMarks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubCriteriaQuestionText() {
        return subCriteriaQuestionText;
    }

    public void setSubCriteriaQuestionText(String subCriteriaQuestionText) {
        this.subCriteriaQuestionText = subCriteriaQuestionText;
    }

    public Double getMarks() {
        return marks;
    }

    public void setMarks(Double marks) {
        this.marks = marks;
    }

    public Double getConsultantMarks() {
        return consultantMarks;
    }

    public void setConsultantMarks(Double consultantMarks) {
        this.consultantMarks = consultantMarks;
    }

    public Integer getConsultantEvaluationId() {
        return consultantEvaluationId;
    }

    public void setConsultantEvaluationId(Integer consultantEvaluationId) {
        this.consultantEvaluationId = consultantEvaluationId;
    }

}
