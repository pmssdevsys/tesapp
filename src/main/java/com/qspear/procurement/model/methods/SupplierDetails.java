package com.qspear.procurement.model.methods;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;

/**
 * Created by e1002703 on 4/2/2018.
 */
public class SupplierDetails {

    Integer packageSupplierId;
    Integer supplierId;
    String supplierName;
    String packageName;
    String itemName;
    String address;
    String phoneNo;
    String emailId;
    String panNo;
    String tanNo;
    String taxNo;
    String faxNo;
    String country;
    String nationality;
    String supplierTermination;
    String city;
    String supplierSource;
    String specificSource;
    String pinCode;
    String state;
    String reprensentative;
    String letter;
    String letterLink;
    String quotationFile;
    String downloadFile;
    String downloadLinkString;
    MailDetails mailDetails;
    Integer pmssType;
    String gstNo;
    String remarks;
    String supplierRegistrationNumber;
    Integer isShortlisted;
    String comments;
    String invitationLetterLink;
    String invitationLetterFileName;

    String invitationLetterFinancialLink;
    String invitationLetterFinancialFileName;

    Integer isQuotationSent;
    Integer isEmailSent;
    String quotationFileLink;
    String epfNo;
    String esiNo;
    String contractLabourLicenseNo;

    String quotationNumber;
    Date quotationDate;
    Double quotedPrice;

    public String getSpecificSource() {
        return specificSource;
    }

    public void setSpecificSource(String specificSource) {
        this.specificSource = specificSource;
    }

    public String getEpfNo() {
        return epfNo;
    }

    public void setEpfNo(String epfNo) {
        this.epfNo = epfNo;
    }

    public String getEsiNo() {
        return esiNo;
    }

    public void setEsiNo(String esiNo) {
        this.esiNo = esiNo;
    }

    public String getContractLabourLicenseNo() {
        return contractLabourLicenseNo;
    }

    public void setContractLabourLicenseNo(String contractLabourLicenseNo) {
        this.contractLabourLicenseNo = contractLabourLicenseNo;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getTanNo() {
        return tanNo;
    }

    public void setTanNo(String tanNo) {
        this.tanNo = tanNo;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getSupplierTermination() {
        return supplierTermination;
    }

    public void setSupplierTermination(String supplierTermination) {
        this.supplierTermination = supplierTermination;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSupplierSource() {
        return supplierSource;
    }

    public void setSupplierSource(String supplierSource) {
        this.supplierSource = supplierSource;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
//
//    public String getQuotationDocument() {
//        return quotationDocument;
//    }
//
//    public void setQuotationDocument(String quotationDocument) {
//        this.quotationDocument = quotationDocument;
//    }
//
//    public String getQuotationDocumentLink() {
//        return quotationDocumentLink;
//    }
//
//    public void setQuotationDocumentLink(String quotationDocumentLink) {
//        this.quotationDocumentLink = quotationDocumentLink;
//    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReprensentative() {
        return reprensentative;
    }

    public void setReprensentative(String reprensentative) {
        this.reprensentative = reprensentative;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public String getLetterLink() {
        return letterLink;
    }

    public void setLetterLink(String letterLink) {
        this.letterLink = letterLink;
    }

    public String getQuotationFile() {
        return quotationFile;
    }

    public void setQuotationFile(String quotationFile) {
        this.quotationFile = quotationFile;
    }

    public String getDownloadFile() {
        return downloadFile;
    }

    public void setDownloadFile(String downloadFile) {
        this.downloadFile = downloadFile;
    }

    public String getDownloadLinkString() {
        return downloadLinkString;
    }

    public void setDownloadLinkString(String downloadLinkString) {
        this.downloadLinkString = downloadLinkString;
    }

    public MailDetails getMailDetails() {
        return mailDetails;
    }

    public void setMailDetails(MailDetails mailDetails) {
        this.mailDetails = mailDetails;
    }

    public Integer getPackageSupplierId() {
        return packageSupplierId;
    }

    public void setPackageSupplierId(Integer packageSupplierId) {
        this.packageSupplierId = packageSupplierId;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public Integer getPmssType() {
        return pmssType;
    }

    public void setPmssType(Integer pmssType) {
        this.pmssType = pmssType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSupplierRegistrationNumber() {
        return supplierRegistrationNumber;
    }

    public void setSupplierRegistrationNumber(String supplierRegistrationNumber) {
        this.supplierRegistrationNumber = supplierRegistrationNumber;
    }

    public Integer getIsShortlisted() {
        return isShortlisted;
    }

    public void setIsShortlisted(Integer isShortlisted) {
        this.isShortlisted = isShortlisted;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getInvitationLetterLink() {
        return invitationLetterLink;
    }

    public void setInvitationLetterLink(String invitationLetterLink) {
        this.invitationLetterLink = invitationLetterLink;
    }

    public Integer getIsQuotationSent() {
        return isQuotationSent;
    }

    public void setIsQuotationSent(Integer isQuotationSent) {
        this.isQuotationSent = isQuotationSent;
    }

    public Integer getIsEmailSent() {
        return isEmailSent;
    }

    public void setIsEmailSent(Integer isEmailSent) {
        this.isEmailSent = isEmailSent;
    }

    public String getQuotationFileLink() {
        return quotationFileLink;
    }

    public void setQuotationFileLink(String quotationFileLink) {
        this.quotationFileLink = quotationFileLink;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getInvitationLetterFileName() {
        return invitationLetterFileName;
    }

    public void setInvitationLetterFileName(String invitationLetterFileName) {
        this.invitationLetterFileName = invitationLetterFileName;
    }

    public String getQuotationNumber() {
        return quotationNumber;
    }

    public void setQuotationNumber(String quotationNumber) {
        this.quotationNumber = quotationNumber;
    }

    public Date getQuotationDate() {
        return quotationDate;
    }

    public void setQuotationDate(Date quotationDate) {
        this.quotationDate = quotationDate;
    }

    public Double getQuotedPrice() {
        return quotedPrice;
    }

    public void setQuotedPrice(Double quotedPrice) {
        this.quotedPrice = quotedPrice;
    }

    public String getInvitationLetterFinancialLink() {
        return invitationLetterFinancialLink;
    }

    public void setInvitationLetterFinancialLink(String invitationLetterFinancialLink) {
        this.invitationLetterFinancialLink = invitationLetterFinancialLink;
    }

    public String getInvitationLetterFinancialFileName() {
        return invitationLetterFinancialFileName;
    }

    public void setInvitationLetterFinancialFileName(String invitationLetterFinancialFileName) {
        this.invitationLetterFinancialFileName = invitationLetterFinancialFileName;
    }

}
