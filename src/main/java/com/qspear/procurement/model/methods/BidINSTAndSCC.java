/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class BidINSTAndSCC {

    Integer bidinstructionId;
    String packageCode;
    String packageName;
    String draftInFavour;
    String payableAt;
    Date bdsStartTime;
    Date bdsEndTime;
    Double postalCharges;
    Double bidSecurity;
    String invitingOfficer;
    Date prebidMeetingDate;
    Date prebidMeetingTime;
    Double warranty;
    String arbitrationProceeding;
    String addressPurchaserNotice;
    Double liquidatedDamages;
    Double liquidatedDamagesMaximum;
    String placeOfOpeningOfBids;
    String performanceSecurity;

    public String getPerformanceSecurity() {
        return performanceSecurity;
    }

    public void setPerformanceSecurity(String performanceSecurity) {
        this.performanceSecurity = performanceSecurity;
    }

    public Integer getBidinstructionId() {
        return bidinstructionId;
    }

    public void setBidinstructionId(Integer bidinstructionId) {
        this.bidinstructionId = bidinstructionId;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDraftInFavour() {
        return draftInFavour;
    }

    public void setDraftInFavour(String draftInFavour) {
        this.draftInFavour = draftInFavour;
    }

    public String getPayableAt() {
        return payableAt;
    }

    public void setPayableAt(String payableAt) {
        this.payableAt = payableAt;
    }

    public Date getBdsStartTime() {
        return bdsStartTime;
    }

    public void setBdsStartTime(Date bdsStartTime) {
        this.bdsStartTime = bdsStartTime;
    }

    public Date getBdsEndTime() {
        return bdsEndTime;
    }

    public void setBdsEndTime(Date bdsEndTime) {
        this.bdsEndTime = bdsEndTime;
    }

    public Double getPostalCharges() {
        return postalCharges;
    }

    public void setPostalCharges(Double postalCharges) {
        this.postalCharges = postalCharges;
    }

    public Double getBidSecurity() {
        return bidSecurity;
    }

    public void setBidSecurity(Double bidSecurity) {
        this.bidSecurity = bidSecurity;
    }

    public String getInvitingOfficer() {
        return invitingOfficer;
    }

    public void setInvitingOfficer(String invitingOfficer) {
        this.invitingOfficer = invitingOfficer;
    }

    public Date getPrebidMeetingDate() {
        return prebidMeetingDate;
    }

    public void setPrebidMeetingDate(Date prebidMeetingDate) {
        this.prebidMeetingDate = prebidMeetingDate;
    }

    public Date getPrebidMeetingTime() {
        return prebidMeetingTime;
    }

    public void setPrebidMeetingTime(Date prebidMeetingTime) {
        this.prebidMeetingTime = prebidMeetingTime;
    }

    public Double getWarranty() {
        return warranty;
    }

    public void setWarranty(Double warranty) {
        this.warranty = warranty;
    }

    public String getArbitrationProceeding() {
        return arbitrationProceeding;
    }

    public void setArbitrationProceeding(String arbitrationProceeding) {
        this.arbitrationProceeding = arbitrationProceeding;
    }

    public String getAddressPurchaserNotice() {
        return addressPurchaserNotice;
    }

    public void setAddressPurchaserNotice(String addressPurchaserNotice) {
        this.addressPurchaserNotice = addressPurchaserNotice;
    }

    public Double getLiquidatedDamages() {
        return liquidatedDamages;
    }

    public void setLiquidatedDamages(Double liquidatedDamages) {
        this.liquidatedDamages = liquidatedDamages;
    }

    public Double getLiquidatedDamagesMaximum() {
        return liquidatedDamagesMaximum;
    }

    public void setLiquidatedDamagesMaximum(Double liquidatedDamagesMaximum) {
        this.liquidatedDamagesMaximum = liquidatedDamagesMaximum;
    }

    public String getPlaceOfOpeningOfBids() {
        return placeOfOpeningOfBids;
    }

    public void setPlaceOfOpeningOfBids(String placeOfOpeningOfBids) {
        this.placeOfOpeningOfBids = placeOfOpeningOfBids;
    }

}
