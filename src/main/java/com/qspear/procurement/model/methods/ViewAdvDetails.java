/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class ViewAdvDetails  extends GenerateAdv{

 
    Date publicationDateLocal;
    Date publicationDateNational;

    public Date getPublicationDateLocal() {
        return publicationDateLocal;
    }

    public void setPublicationDateLocal(Date publicationDateLocal) {
        this.publicationDateLocal = publicationDateLocal;
    }

    public Date getPublicationDateNational() {
        return publicationDateNational;
    }

    public void setPublicationDateNational(Date publicationDateNational) {
        this.publicationDateNational = publicationDateNational;
    }

}
