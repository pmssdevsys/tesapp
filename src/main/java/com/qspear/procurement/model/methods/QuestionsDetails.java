package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 * Created by e1002703 on 4/2/2018.
 */
public class QuestionsDetails {
    String questionCategory;
    ArrayList<Questions> questions;

    public String getQuestionCategory() {
        return questionCategory;
    }

    public void setQuestionCategory(String questionCategory) {
        this.questionCategory = questionCategory;
    }

    public ArrayList<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Questions> questions) {
        this.questions = questions;
    }
}
