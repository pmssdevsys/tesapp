/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
public class GRNItem {

    Integer packageGrnItemDetailId;
    String itemcode;
    Integer itemMasterid;
    String itemName;
    String itemComment;
    Double receivedQty;
    String stockRegistorName;
    String pageno;
    String serialno;
    Double gst;
    ArrayList<GRNItemDistribution> grnItemDistributions;

    public Integer getPackageGrnItemDetailId() {
        return packageGrnItemDetailId;
    }

    public void setPackageGrnItemDetailId(Integer packageGrnItemDetailId) {
        this.packageGrnItemDetailId = packageGrnItemDetailId;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemComment() {
        return itemComment;
    }

    public void setItemComment(String itemComment) {
        this.itemComment = itemComment;
    }

    public Double getReceivedQty() {
        return receivedQty;
    }

    public void setReceivedQty(Double receivedQty) {
        this.receivedQty = receivedQty;
    }

    public String getStockRegistorName() {
        return stockRegistorName;
    }

    public void setStockRegistorName(String stockRegistorName) {
        this.stockRegistorName = stockRegistorName;
    }

    public String getPageno() {
        return pageno;
    }

    public void setPageno(String pageno) {
        this.pageno = pageno;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public ArrayList<GRNItemDistribution> getGrnItemDistributions() {
        return grnItemDistributions;
    }

    public void setGrnItemDistributions(ArrayList<GRNItemDistribution> grnItemDistributions) {
        this.grnItemDistributions = grnItemDistributions;
    }

    public Integer getItemMasterid() {
        return itemMasterid;
    }

    public void setItemMasterid(Integer itemMasterid) {
        this.itemMasterid = itemMasterid;
    }

    public Double getGst() {
        return gst;
    }

    public void setGst(Double gst) {
        this.gst = gst;
    }
    
}
