/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class LoaAward {

    String poNumber;
    Date loaGeneratedDate;
    GenerateContract generateContract;
    ArrayList<PurchaseOrderItems> items;
    ArrayList<WorkOrderWorks> works;
    ArrayList<Uploads> loaGeneration;
    ArrayList<Uploads> contractGeneration;
    ArrayList<Uploads> woGeneration;
    Date maintainencePeriodExpiryDate;

    public Date getMaintainencePeriodExpiryDate() {
        return maintainencePeriodExpiryDate;
    }

    public void setMaintainencePeriodExpiryDate(Date maintainencePeriodExpiryDate) {
        this.maintainencePeriodExpiryDate = maintainencePeriodExpiryDate;
    }
    
    public ArrayList<Uploads> getLoaGeneration() {
        return loaGeneration;
    }

    public void setLoaGeneration(ArrayList<Uploads> loaGeneration) {
        this.loaGeneration = loaGeneration;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public Date getLoaGeneratedDate() {
        return loaGeneratedDate;
    }

    public void setLoaGeneratedDate(Date loaGeneratedDate) {
        this.loaGeneratedDate = loaGeneratedDate;
    }

    public GenerateContract getGenerateContract() {
        return generateContract;
    }

    public void setGenerateContract(GenerateContract generateContract) {
        this.generateContract = generateContract;
    }

    public ArrayList<PurchaseOrderItems> getItems() {
        return items;
    }

    public void setItems(ArrayList<PurchaseOrderItems> items) {
        this.items = items;
    }

    public ArrayList<WorkOrderWorks> getWorks() {
        return works;
    }

    public void setWorks(ArrayList<WorkOrderWorks> works) {
        this.works = works;
    }

    public ArrayList<Uploads> getContractGeneration() {
        return contractGeneration;
    }

    public void setContractGeneration(ArrayList<Uploads> contractGeneration) {
        this.contractGeneration = contractGeneration;
    }

    public ArrayList<Uploads> getWoGeneration() {
        return woGeneration;
    }

    public void setWoGeneration(ArrayList<Uploads> woGeneration) {
        this.woGeneration = woGeneration;
    }

}
