/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

/**
 *
 * @author jaspreet
 */
public class IssueOfRFP {

    PreProposalMeetingDetails preProposalMeetingDetails;
    ProposalExtension proposalExtension;

    public PreProposalMeetingDetails getPreProposalMeetingDetails() {
        return preProposalMeetingDetails;
    }

    public void setPreProposalMeetingDetails(PreProposalMeetingDetails preProposalMeetingDetails) {
        this.preProposalMeetingDetails = preProposalMeetingDetails;
    }

    public ProposalExtension getProposalExtension() {
        return proposalExtension;
    }

    public void setProposalExtension(ProposalExtension proposalExtension) {
        this.proposalExtension = proposalExtension;
    }

}
