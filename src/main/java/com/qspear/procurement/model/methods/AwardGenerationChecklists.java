/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

/**
 *
 * @author jaspreet
 */
public class AwardGenerationChecklists {

    Integer isAdvancePaymentRequired;
    Integer paymentMilestonesDefined;
    Integer isBankGuaranteeSubmitted;
    Integer isArbitratorAgreed;

    public Integer getIsAdvancePaymentRequired() {
        return isAdvancePaymentRequired;
    }

    public void setIsAdvancePaymentRequired(Integer isAdvancePaymentRequired) {
        this.isAdvancePaymentRequired = isAdvancePaymentRequired;
    }

    public Integer getPaymentMilestonesDefined() {
        return paymentMilestonesDefined;
    }

    public void setPaymentMilestonesDefined(Integer paymentMilestonesDefined) {
        this.paymentMilestonesDefined = paymentMilestonesDefined;
    }

    public Integer getIsBankGuaranteeSubmitted() {
        return isBankGuaranteeSubmitted;
    }

    public void setIsBankGuaranteeSubmitted(Integer isBankGuaranteeSubmitted) {
        this.isBankGuaranteeSubmitted = isBankGuaranteeSubmitted;
    }

    public Integer getIsArbitratorAgreed() {
        return isArbitratorAgreed;
    }

    public void setIsArbitratorAgreed(Integer isArbitratorAgreed) {
        this.isArbitratorAgreed = isArbitratorAgreed;
    }

}
