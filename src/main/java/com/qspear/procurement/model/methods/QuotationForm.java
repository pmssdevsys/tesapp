package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class QuotationForm {

    String supplierName;
    Integer supplierId;
    Integer isFormResponsive;

    ArrayList<Questions> questions;

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public ArrayList<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Questions> questions) {
        this.questions = questions;
    }

    public Integer getIsFormResponsive() {
        return isFormResponsive;
    }

    public void setIsFormResponsive(Integer isFormResponsive) {
        this.isFormResponsive = isFormResponsive;
    }

}
