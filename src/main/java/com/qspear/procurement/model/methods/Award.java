package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class Award {

    Integer packageAwardDetailId;
    String l1Supplier;
    Integer l1SupplierId;
    String recommandationComments;
    ArrayList<QuotationForm> extendValidity;
    ArrayList<Uploads> awardGeneration;

    public String getL1Supplier() {
        return l1Supplier;
    }

    public void setL1Supplier(String l1Supplier) {
        this.l1Supplier = l1Supplier;
    }

    public String getRecommandationComments() {
        return recommandationComments;
    }

    public void setRecommandationComments(String recommandationComments) {
        this.recommandationComments = recommandationComments;
    }

    public Integer getL1SupplierId() {
        return l1SupplierId;
    }

    public void setL1SupplierId(Integer l1SupplierId) {
        this.l1SupplierId = l1SupplierId;
    }

    public Integer getPackageAwardDetailId() {
        return packageAwardDetailId;
    }

    public void setPackageAwardDetailId(Integer packageAwardDetailId) {
        this.packageAwardDetailId = packageAwardDetailId;
    }

    public ArrayList<QuotationForm> getExtendValidity() {
        return extendValidity;
    }

    public void setExtendValidity(ArrayList<QuotationForm> extendValidity) {
        this.extendValidity = extendValidity;
    }

    public ArrayList<Uploads> getAwardGeneration() {
        return awardGeneration;
    }

    public void setAwardGeneration(ArrayList<Uploads> awardGeneration) {
        this.awardGeneration = awardGeneration;
    }

}
