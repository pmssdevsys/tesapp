/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

/**
 *
 * @author jaspreet
 */
public class QuotationWork {

    Integer quotationDetailId;
    String workName;
    String workSpecification;
    Double workEstimatedCost;
    Double workCost;
    String comments;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuotationDetailId() {
        return quotationDetailId;
    }

    public void setQuotationDetailId(Integer quotationDetailId) {
        this.quotationDetailId = quotationDetailId;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getWorkSpecification() {
        return workSpecification;
    }

    public void setWorkSpecification(String workSpecification) {
        this.workSpecification = workSpecification;
    }

    public Double getWorkEstimatedCost() {
        return workEstimatedCost;
    }

    public void setWorkEstimatedCost(Double workEstimatedCost) {
        this.workEstimatedCost = workEstimatedCost;
    }

    public Double getWorkCost() {
        return workCost;
    }

    public void setWorkCost(Double workCost) {
        this.workCost = workCost;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}
