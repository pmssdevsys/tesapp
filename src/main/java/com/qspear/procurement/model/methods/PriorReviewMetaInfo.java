/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

/**
 *
 * @author jaspreet
 */
public class PriorReviewMetaInfo {

    String roleOfNextActor;
    Integer isCurrentlyInPriorReview;

    public String getRoleOfNextActor() {
        return roleOfNextActor;
    }

    public void setRoleOfNextActor(String roleOfNextActor) {
        this.roleOfNextActor = roleOfNextActor;
    }

    public Integer getIsCurrentlyInPriorReview() {
        return isCurrentlyInPriorReview;
    }

    public void setIsCurrentlyInPriorReview(Integer isCurrentlyInPriorReview) {
        this.isCurrentlyInPriorReview = isCurrentlyInPriorReview;
    }

}
