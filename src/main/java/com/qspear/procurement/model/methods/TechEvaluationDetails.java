/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class TechEvaluationDetails {

    Integer id;
    Date techProposalOpeningDate;
    Date plannedFinancialOpeningDate;
    Date plannedFinancialOpeningTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTechProposalOpeningDate() {
        return techProposalOpeningDate;
    }

    public void setTechProposalOpeningDate(Date techProposalOpeningDate) {
        this.techProposalOpeningDate = techProposalOpeningDate;
    }

    public Date getPlannedFinancialOpeningDate() {
        return plannedFinancialOpeningDate;
    }

    public void setPlannedFinancialOpeningDate(Date plannedFinancialOpeningDate) {
        this.plannedFinancialOpeningDate = plannedFinancialOpeningDate;
    }

    public Date getPlannedFinancialOpeningTime() {
        return plannedFinancialOpeningTime;
    }

    public void setPlannedFinancialOpeningTime(Date plannedFinancialOpeningTime) {
        this.plannedFinancialOpeningTime = plannedFinancialOpeningTime;
    }

}
