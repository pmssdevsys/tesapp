/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

/**
 *
 * @author jaspreet
 */
public class PostQualificationForm {

    Integer isTotalMonetaryValueReceived;
    Integer isIncomeTaxCertificateReceived;
    Integer isFinancialStandingReportReceived;
    Integer isLitigationReportReceived;
    Integer hasSatisfactoryWorkCompleted;
    Integer hasValidElectricalLicense;
    Integer hasValidSanitaryWorkLicense;
    Integer supplierId;
    String supplierName;

    public Integer getIsTotalMonetaryValueReceived() {
        return isTotalMonetaryValueReceived;
    }

    public void setIsTotalMonetaryValueReceived(Integer isTotalMonetaryValueReceived) {
        this.isTotalMonetaryValueReceived = isTotalMonetaryValueReceived;
    }

    public Integer getIsIncomeTaxCertificateReceived() {
        return isIncomeTaxCertificateReceived;
    }

    public void setIsIncomeTaxCertificateReceived(Integer isIncomeTaxCertificateReceived) {
        this.isIncomeTaxCertificateReceived = isIncomeTaxCertificateReceived;
    }

    public Integer getIsFinancialStandingReportReceived() {
        return isFinancialStandingReportReceived;
    }

    public void setIsFinancialStandingReportReceived(Integer isFinancialStandingReportReceived) {
        this.isFinancialStandingReportReceived = isFinancialStandingReportReceived;
    }

    public Integer getIsLitigationReportReceived() {
        return isLitigationReportReceived;
    }

    public void setIsLitigationReportReceived(Integer isLitigationReportReceived) {
        this.isLitigationReportReceived = isLitigationReportReceived;
    }

    public Integer getHasSatisfactoryWorkCompleted() {
        return hasSatisfactoryWorkCompleted;
    }

    public void setHasSatisfactoryWorkCompleted(Integer hasSatisfactoryWorkCompleted) {
        this.hasSatisfactoryWorkCompleted = hasSatisfactoryWorkCompleted;
    }

    public Integer getHasValidElectricalLicense() {
        return hasValidElectricalLicense;
    }

    public void setHasValidElectricalLicense(Integer hasValidElectricalLicense) {
        this.hasValidElectricalLicense = hasValidElectricalLicense;
    }

    public Integer getHasValidSanitaryWorkLicense() {
        return hasValidSanitaryWorkLicense;
    }

    public void setHasValidSanitaryWorkLicense(Integer hasValidSanitaryWorkLicense) {
        this.hasValidSanitaryWorkLicense = hasValidSanitaryWorkLicense;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

}
