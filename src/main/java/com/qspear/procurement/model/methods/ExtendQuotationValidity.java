package com.qspear.procurement.model.methods;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class ExtendQuotationValidity {
    String supplierName;
    String validityText;
    String validity;

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getValidityText() {
        return validityText;
    }

    public void setValidityText(String validityText) {
        this.validityText = validityText;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }
}
