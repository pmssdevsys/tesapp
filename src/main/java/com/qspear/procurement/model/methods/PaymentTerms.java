package com.qspear.procurement.model.methods;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class PaymentTerms {
    String paymentDescription;
    String expectedDeliveryPeriod;
    String milestonePercentage;
    String expectedPaymentAmount;
    String expectedDeliveryDate;
    String expectedPaymentDate;

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public String getExpectedDeliveryPeriod() {
        return expectedDeliveryPeriod;
    }

    public void setExpectedDeliveryPeriod(String expectedDeliveryPeriod) {
        this.expectedDeliveryPeriod = expectedDeliveryPeriod;
    }

    public String getMilestonePercentage() {
        return milestonePercentage;
    }

    public void setMilestonePercentage(String milestonePercentage) {
        this.milestonePercentage = milestonePercentage;
    }

    public String getExpectedPaymentAmount() {
        return expectedPaymentAmount;
    }

    public void setExpectedPaymentAmount(String expectedPaymentAmount) {
        this.expectedPaymentAmount = expectedPaymentAmount;
    }

    public String getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(String expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public String getExpectedPaymentDate() {
        return expectedPaymentDate;
    }

    public void setExpectedPaymentDate(String expectedPaymentDate) {
        this.expectedPaymentDate = expectedPaymentDate;
    }
}
