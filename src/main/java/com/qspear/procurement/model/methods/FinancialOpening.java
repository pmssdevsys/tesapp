/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class FinancialOpening {

    Integer id;
    Date financialOpeningDate;
    Date financialOpeningTime;
    ArrayList<OpeningData> openingData;
    ArrayList<Uploads> foiGeneration;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFinancialOpeningDate() {
        return financialOpeningDate;
    }

    public void setFinancialOpeningDate(Date financialOpeningDate) {
        this.financialOpeningDate = financialOpeningDate;
    }

    public Date getFinancialOpeningTime() {
        return financialOpeningTime;
    }

    public void setFinancialOpeningTime(Date financialOpeningTime) {
        this.financialOpeningTime = financialOpeningTime;
    }

    public ArrayList<OpeningData>  getOpeningData() {
        return openingData;
    }

    public void setOpeningData(ArrayList<OpeningData>  openingData) {
        this.openingData = openingData;
    }

    public ArrayList<Uploads> getFoiGeneration() {
        return foiGeneration;
    }

    public void setFoiGeneration(ArrayList<Uploads> foiGeneration) {
        this.foiGeneration = foiGeneration;
    }

}
