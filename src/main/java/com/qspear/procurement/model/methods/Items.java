package com.qspear.procurement.model.methods;

import com.qspear.procurement.model.TeqipItemGoodsDepartmentBreakupModel;
import java.util.List;

/**
 * Created by e1002703 on 4/2/2018.
 */
public class Items {

    Integer itemId;
    Integer itemMasterId;
    String itemName;
    String itemDescription;
    Integer departmentId;
    String department;
    Double estimatedCost;
    Double quantity;
    Integer deliveyPeriod;
    Integer trainingRequired;
    Integer installationRequired;
    String itemmainspecification;
    String placeofdelivery;
    String instllationrequirement;
    List<TeqipItemGoodsDepartmentBreakupModel> itemBreakUp;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(Double estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Integer getDeliveyPeriod() {
        return deliveyPeriod;
    }

    public void setDeliveyPeriod(Integer deliveyPeriod) {
        this.deliveyPeriod = deliveyPeriod;
    }

    public Integer getTrainingRequired() {
        return trainingRequired;
    }

    public void setTrainingRequired(Integer trainingRequired) {
        this.trainingRequired = trainingRequired;
    }

    public Integer getInstallationRequired() {
        return installationRequired;
    }

    public void setInstallationRequired(Integer installationRequired) {
        this.installationRequired = installationRequired;
    }

    public String getItemmainspecification() {
        return itemmainspecification;
    }

    public void setItemmainspecification(String itemmainspecification) {
        this.itemmainspecification = itemmainspecification;
    }

    public String getPlaceofdelivery() {
        return placeofdelivery;
    }

    public void setPlaceofdelivery(String placeofdelivery) {
        this.placeofdelivery = placeofdelivery;
    }

    public String getInstllationrequirement() {
        return instllationrequirement;
    }

    public void setInstllationrequirement(String instllationrequirement) {
        this.instllationrequirement = instllationrequirement;
    }

    public Integer getItemMasterId() {
        return itemMasterId;
    }

    public void setItemMasterId(Integer itemMasterId) {
        this.itemMasterId = itemMasterId;
    }

    public List<TeqipItemGoodsDepartmentBreakupModel> getItemBreakUp() {
        return itemBreakUp;
    }

    public void setItemBreakUp(List<TeqipItemGoodsDepartmentBreakupModel> itemBreakUp) {
        this.itemBreakUp = itemBreakUp;
    }

}
