/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class BankNOCDetails {
    Integer id;
    Integer isWordWorkBankNOC;
    Date wordBankNOCDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsWordWorkBankNOC() {
        return isWordWorkBankNOC;
    }

    public void setIsWordWorkBankNOC(Integer isWordWorkBankNOC) {
        this.isWordWorkBankNOC = isWordWorkBankNOC;
    }

    public Date getWordBankNOCDate() {
        return wordBankNOCDate;
    }

    public void setWordBankNOCDate(Date wordBankNOCDate) {
        this.wordBankNOCDate = wordBankNOCDate;
    }
    
    
}
