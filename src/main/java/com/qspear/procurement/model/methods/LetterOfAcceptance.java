/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class LetterOfAcceptance {

    Integer letterofAcceptanceId;
    Date maintenancePeriod;
    Double performanceSecurity;
    Date loaGeneratedDate;
    Integer newBidValidity;
    ArrayList<Uploads> loaGeneration;

    
    public Integer getLetterofAcceptanceId() {
        return letterofAcceptanceId;
    }

    public void setLetterofAcceptanceId(Integer letterofAcceptanceId) {
        this.letterofAcceptanceId = letterofAcceptanceId;
    }


    public Date getLoaGeneratedDate() {
        return loaGeneratedDate;
    }

    public void setLoaGeneratedDate(Date loaGeneratedDate) {
        this.loaGeneratedDate = loaGeneratedDate;
    }

    public Integer getNewBidValidity() {
        return newBidValidity;
    }

    public void setNewBidValidity(Integer newBidValidity) {
        this.newBidValidity = newBidValidity;
    }

    public Date getMaintenancePeriod() {
        return maintenancePeriod;
    }

    public void setMaintenancePeriod(Date maintenancePeriod) {
        this.maintenancePeriod = maintenancePeriod;
    }

    public Double getPerformanceSecurity() {
        return performanceSecurity;
    }

    public void setPerformanceSecurity(Double performanceSecurity) {
        this.performanceSecurity = performanceSecurity;
    }

    public ArrayList<Uploads> getLoaGeneration() {
        return loaGeneration;
    }

    public void setLoaGeneration(ArrayList<Uploads> loaGeneration) {
        this.loaGeneration = loaGeneration;
    }
    

    
}
