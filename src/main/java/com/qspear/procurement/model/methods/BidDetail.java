/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class BidDetail {

    Integer biddetailId;
    Date sbdDate;
    Date lastDateReceiptsofBids;
    Date lastDateSBD;
    Date dateOpeningBid;
    Integer validityQuotation;
    Integer bidSecurityValidity;
    Double costOfBidDocument;

    public Integer getBiddetailId() {
        return biddetailId;
    }

    public void setBiddetailId(Integer biddetailId) {
        this.biddetailId = biddetailId;
    }

    public Date getSbdDate() {
        return sbdDate;
    }

    public void setSbdDate(Date sbdDate) {
        this.sbdDate = sbdDate;
    }

    public Date getLastDateReceiptsofBids() {
        return lastDateReceiptsofBids;
    }

    public void setLastDateReceiptsofBids(Date lastDateReceiptsofBids) {
        this.lastDateReceiptsofBids = lastDateReceiptsofBids;
    }

    public Date getLastDateSBD() {
        return lastDateSBD;
    }

    public void setLastDateSBD(Date lastDateSBD) {
        this.lastDateSBD = lastDateSBD;
    }

    public Date getDateOpeningBid() {
        return dateOpeningBid;
    }

    public void setDateOpeningBid(Date dateOpeningBid) {
        this.dateOpeningBid = dateOpeningBid;
    }

    public Integer getValidityQuotation() {
        return validityQuotation;
    }

    public void setValidityQuotation(Integer validityQuotation) {
        this.validityQuotation = validityQuotation;
    }

    public Integer getBidSecurityValidity() {
        return bidSecurityValidity;
    }

    public void setBidSecurityValidity(Integer bidSecurityValidity) {
        this.bidSecurityValidity = bidSecurityValidity;
    }

    public Double getCostOfBidDocument() {
        return costOfBidDocument;
    }

    public void setCostOfBidDocument(Double costOfBidDocument) {
        this.costOfBidDocument = costOfBidDocument;
    }

}
