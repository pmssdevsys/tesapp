/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.List;

/**
 *
 * @author jaspreet
 */
public class StatusSummary {

    List<StageModel> stages;
    StageModel nextStage;

    public List<StageModel> getStages() {
        return stages;
    }

    public void setStages(List<StageModel> stages) {
        this.stages = stages;
    }

    public StageModel getNextStage() {
        return nextStage;
    }

    public void setNextStage(StageModel nextStage) {
        this.nextStage = nextStage;
    }

}
