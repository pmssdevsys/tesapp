/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

/**
 *
 * @author jaspreet
 */
public class RecommendationDetails {

    Integer l1SuplierId;
    String l1SupplierName;
    String recommendationComments;
    Double contractValue;

    public Integer getL1SuplierId() {
        return l1SuplierId;
    }

    public void setL1SuplierId(Integer l1SuplierId) {
        this.l1SuplierId = l1SuplierId;
    }

    public String getL1SupplierName() {
        return l1SupplierName;
    }

    public void setL1SupplierName(String l1SupplierName) {
        this.l1SupplierName = l1SupplierName;
    }

    public String getRecommendationComments() {
        return recommendationComments;
    }

    public void setRecommendationComments(String recommendationComments) {
        this.recommendationComments = recommendationComments;
    }

    public Double getContractValue() {
        return contractValue;
    }

    public void setContractValue(Double contractValue) {
        this.contractValue = contractValue;
    }

}
