/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
public class RfpDocument {

    String pkgName;
    String pkgCode;
    String referenceNo;
    TechnicalCriteriaParameters technicalCriteriaParameters;
    ArrayList<TechnicalCriterias> technicalCriterias;

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public String getPkgCode() {
        return pkgCode;
    }

    public void setPkgCode(String pkgCode) {
        this.pkgCode = pkgCode;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public TechnicalCriteriaParameters getTechnicalCriteriaParameters() {
        return technicalCriteriaParameters;
    }

    public void setTechnicalCriteriaParameters(TechnicalCriteriaParameters technicalCriteriaParameters) {
        this.technicalCriteriaParameters = technicalCriteriaParameters;
    }

    public ArrayList<TechnicalCriterias> getTechnicalCriterias() {
        return technicalCriterias;
    }

    public void setTechnicalCriterias(ArrayList<TechnicalCriterias> technicalCriterias) {
        this.technicalCriterias = technicalCriterias;
    }

}
