package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 * Created by e1002703 on 4/2/2018.
 */
public class PaymentDetails {

    Integer packagePaymentId;
    String paymentDescription;
    Double milestonePercentage;
    Date expectedDeliveryDate;
    Integer expectedDeliveryPeriod;
    Double expectedPaymentAmount;
    Date expectedPaymentDate;
    String poNumber;
    Integer isWCCGenerated;
    Integer milestoneActualCompletionPeriod;
    String paymentDate;
    String chequeDraftNumber;
    Integer isWorkCompleted;
    Date actualCompletionDate;
    Double actualPaymentAmount;
    Double totalPaymentTillNow;
    String comments;
    String uploadCompletionCertificate;
    ContractManagementCriteria contractManagementCriteria;

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public Double getMilestonePercentage() {
        return milestonePercentage;
    }

    public void setMilestonePercentage(Double milestonePercentage) {
        this.milestonePercentage = milestonePercentage;
    }

    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public Integer getExpectedDeliveryPeriod() {
        return expectedDeliveryPeriod;
    }

    public void setExpectedDeliveryPeriod(Integer expectedDeliveryPeriod) {
        this.expectedDeliveryPeriod = expectedDeliveryPeriod;
    }

    public Double getExpectedPaymentAmount() {
        return expectedPaymentAmount;
    }

    public void setExpectedPaymentAmount(Double expectedPaymentAmount) {
        this.expectedPaymentAmount = expectedPaymentAmount;
    }

    public Date getExpectedPaymentDate() {
        return expectedPaymentDate;
    }

    public void setExpectedPaymentDate(Date expectedPaymentDate) {
        this.expectedPaymentDate = expectedPaymentDate;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public Integer getIsWCCGenerated() {
        return isWCCGenerated;
    }

    public void setIsWCCGenerated(Integer isWCCGenerated) {
        this.isWCCGenerated = isWCCGenerated;
    }

    public Integer getMilestoneActualCompletionPeriod() {
        return milestoneActualCompletionPeriod;
    }

    public void setMilestoneActualCompletionPeriod(Integer milestoneActualCompletionPeriod) {
        this.milestoneActualCompletionPeriod = milestoneActualCompletionPeriod;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getChequeDraftNumber() {
        return chequeDraftNumber;
    }

    public void setChequeDraftNumber(String chequeDraftNumber) {
        this.chequeDraftNumber = chequeDraftNumber;
    }

    public Integer getIsWorkCompleted() {
        return isWorkCompleted;
    }

    public void setIsWorkCompleted(Integer isWorkCompleted) {
        this.isWorkCompleted = isWorkCompleted;
    }

    public Date getActualCompletionDate() {
        return actualCompletionDate;
    }

    public void setActualCompletionDate(Date actualCompletionDate) {
        this.actualCompletionDate = actualCompletionDate;
    }

    public Double getActualPaymentAmount() {
        return actualPaymentAmount;
    }

    public void setActualPaymentAmount(Double actualPaymentAmount) {
        this.actualPaymentAmount = actualPaymentAmount;
    }

    public Double getTotalPaymentTillNow() {
        return totalPaymentTillNow;
    }

    public void setTotalPaymentTillNow(Double totalPaymentTillNow) {
        this.totalPaymentTillNow = totalPaymentTillNow;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getPackagePaymentId() {
        return packagePaymentId;
    }

    public void setPackagePaymentId(Integer packagePaymentId) {
        this.packagePaymentId = packagePaymentId;
    }

    public ContractManagementCriteria getContractManagementCriteria() {
        return contractManagementCriteria;
    }

    public void setContractManagementCriteria(ContractManagementCriteria contractManagementCriteria) {
        this.contractManagementCriteria = contractManagementCriteria;
    }

    public String getUploadCompletionCertificate() {
        return uploadCompletionCertificate;
    }

    public void setUploadCompletionCertificate(String uploadCompletionCertificate) {
        this.uploadCompletionCertificate = uploadCompletionCertificate;
    }

}
