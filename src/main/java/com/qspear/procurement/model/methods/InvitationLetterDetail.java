/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class InvitationLetterDetail {

    Integer invitationLetterId;
    Integer quotationValidityRequired;
    String trainingClause;
    String warranty;
    Integer workCompletionPeriod;
    Double liquidatedDamages;
    Date lastSubmissionDate;
    Date lastSubmissionTime;
    String installationClause;
    String amc;
    Date plannedBidOpeningDate;
    Date plannedBidOpeningTime;
    String procuringDepartment;
    Double maxLiquidatedDamages;
    boolean generateInvitationLetter;
    Double quotedPrice;
    Double performanceSecurityInPercentage;
    Integer numberOfBids;

    public Integer getNumberOfBids() {
        return numberOfBids;
    }

    public void setNumberOfBids(Integer numberOfBids) {
        this.numberOfBids = numberOfBids;
    }

    public Integer getInvitationLetterId() {
        return invitationLetterId;
    }

    public void setInvitationLetterId(Integer invitationLetterId) {
        this.invitationLetterId = invitationLetterId;
    }

    public Integer getQuotationValidityRequired() {
        return quotationValidityRequired;
    }

    public void setQuotationValidityRequired(Integer quotationValidityRequired) {
        this.quotationValidityRequired = quotationValidityRequired;
    }

    public String getTrainingClause() {
        return trainingClause;
    }

    public void setTrainingClause(String trainingClause) {
        this.trainingClause = trainingClause;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public Integer getWorkCompletionPeriod() {
        return workCompletionPeriod;
    }

    public void setWorkCompletionPeriod(Integer workCompletionPeriod) {
        this.workCompletionPeriod = workCompletionPeriod;
    }

    public Double getLiquidatedDamages() {
        return liquidatedDamages;
    }

    public void setLiquidatedDamages(Double liquidatedDamages) {
        this.liquidatedDamages = liquidatedDamages;
    }

    public Date getLastSubmissionDate() {
        return lastSubmissionDate;
    }

    public void setLastSubmissionDate(Date lastSubmissionDate) {
        this.lastSubmissionDate = lastSubmissionDate;
    }

    public Date getLastSubmissionTime() {
        return lastSubmissionTime;
    }

    public void setLastSubmissionTime(Date lastSubmissionTime) {
        this.lastSubmissionTime = lastSubmissionTime;
    }

    public String getInstallationClause() {
        return installationClause;
    }

    public void setInstallationClause(String installationClause) {
        this.installationClause = installationClause;
    }

    public String getAmc() {
        return amc;
    }

    public void setAmc(String amc) {
        this.amc = amc;
    }

    public Date getPlannedBidOpeningDate() {
        return plannedBidOpeningDate;
    }

    public void setPlannedBidOpeningDate(Date plannedBidOpeningDate) {
        this.plannedBidOpeningDate = plannedBidOpeningDate;
    }

    public Date getPlannedBidOpeningTime() {
        return plannedBidOpeningTime;
    }

    public void setPlannedBidOpeningTime(Date plannedBidOpeningTime) {
        this.plannedBidOpeningTime = plannedBidOpeningTime;
    }

    public String getProcuringDepartment() {
        return procuringDepartment;
    }

    public void setProcuringDepartment(String procuringDepartment) {
        this.procuringDepartment = procuringDepartment;
    }

    public boolean isGenerateInvitationLetter() {
        return generateInvitationLetter;
    }

    public void setGenerateInvitationLetter(boolean generateInvitationLetter) {
        this.generateInvitationLetter = generateInvitationLetter;
    }

    public Double getMaxLiquidatedDamages() {
        return maxLiquidatedDamages;
    }

    public void setMaxLiquidatedDamages(Double maxLiquidatedDamages) {
        this.maxLiquidatedDamages = maxLiquidatedDamages;
    }

    public Double getQuotedPrice() {
        return quotedPrice;
    }

    public void setQuotedPrice(Double quotedPrice) {
        this.quotedPrice = quotedPrice;
    }

    public Double getPerformanceSecurityInPercentage() {
        return performanceSecurityInPercentage;
    }

    public void setPerformanceSecurityInPercentage(Double performanceSecurityInPercentage) {
        this.performanceSecurityInPercentage = performanceSecurityInPercentage;
    }

}
