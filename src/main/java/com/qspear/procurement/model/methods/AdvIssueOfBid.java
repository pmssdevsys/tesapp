/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
public class AdvIssueOfBid {

    GenerateAdv generateAdv;
    ViewAdvDetails viewAdvDetails;
    PreBidMeetingInformation prebidMeetingInformation;
    BidExtension bidExtension;
    ArrayList<Uploads> advGeneration;

    public GenerateAdv getGenerateAdv() {
        return generateAdv;
    }

    public void setGenerateAdv(GenerateAdv generateAdv) {
        this.generateAdv = generateAdv;
    }

    public ViewAdvDetails getViewAdvDetails() {
        return viewAdvDetails;
    }

    public void setViewAdvDetails(ViewAdvDetails viewAdvDetails) {
        this.viewAdvDetails = viewAdvDetails;
    }

    public PreBidMeetingInformation getPrebidMeetingInformation() {
        return prebidMeetingInformation;
    }

    public void setPrebidMeetingInformation(PreBidMeetingInformation prebidMeetingInformation) {
        this.prebidMeetingInformation = prebidMeetingInformation;
    }

    public BidExtension getBidExtension() {
        return bidExtension;
    }

    public void setBidExtension(BidExtension bidExtension) {
        this.bidExtension = bidExtension;
    }

    public ArrayList<Uploads> getAdvGeneration() {
        return advGeneration;
    }

    public void setAdvGeneration(ArrayList<Uploads> advGeneration) {
        this.advGeneration = advGeneration;
    }

}
