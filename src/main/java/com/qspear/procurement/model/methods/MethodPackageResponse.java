package com.qspear.procurement.model.methods;

import com.fasterxml.jackson.databind.JsonNode;
import com.qspear.procurement.model.PrecurementMethodTimeLinePojo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by e1002703 on 4/2/2018.
 */
public class MethodPackageResponse {

    PackageDetails pkgDetails;
    InvitationLetter invitationLetter;
    QuotationOpening quotationOpening;
    QuotationEvaluation quotationEvaluation;
    Award award;
    PurchaseOrder purchaseOrder;
    GRN grn;
    WorkOrder workOrder;
    LetterOfAcceptance letterOfAcceptance;
    WCCPayments wccPayments;
    Bid genofBid;
    AdvIssueOfBid advIssueOfBid;
    LoaAward loaAward;

    ArrayList<Uploads> uploads;
    ArrayList<PaymentDetails> paymentDetails;
    ArrayList<SupplierDetails> supplierDetails;
    ArrayList<ProcuringDepartmentDetails> procuringDepartmentDetails;
    ArrayList<QuestionsDetails> questionsDetails;
    ArrayList<Extension> extension;
    CompleteCheckList completeCheckLists;
    JsonNode packageMetaInfo;

    TermsOfReference termsOfReference;
    ArrayList<TechEvalCommittee> techEvalCommittee;
    ShortListing shortListing;
    RFP rfp;
    IssueOfRFP issueOfRFP;
    AdvExpOfInterest advExpOfInterest;
    TechEvaluation techEvaluation;
    FinancialOpening financialOpening;
    ContractGeneration contractGeneration;
    ContractAward contractAward;
    ContractTerms contractTerms;
    StatusData statusData;
    PrecurementMethodTimeLinePojo timeLine;
    List<PriorReview> priorReview;
    PriorReviewMetaInfo priorReviewMetaInfo;

    L1Supplier l1Supplier;

    PBDGDetails pbdgDetails;
    EMDDetails emdDetails;
    ArrayList<EOIExtension> eoiextensions;
    ContractSanctionOrder contractSanctionOrder;
    Map<String, ArrayList<Uploads>> docGeneration;

    public ArrayList<EOIExtension> getEoiextensions() {
        return eoiextensions;
    }

    public void setEoiextensions(ArrayList<EOIExtension> eoiextensions) {
        this.eoiextensions = eoiextensions;
    }

    public ContractSanctionOrder getContractSanctionOrder() {
        return contractSanctionOrder;
    }

    public void setContractSanctionOrder(ContractSanctionOrder contractSanctionOrder) {
        this.contractSanctionOrder = contractSanctionOrder;
    }

    public PBDGDetails getPbdgDetails() {
        return pbdgDetails;
    }

    public void setPbdgDetails(PBDGDetails pbdgDetails) {
        this.pbdgDetails = pbdgDetails;
    }

    public EMDDetails getEmdDetails() {
        return emdDetails;
    }

    public void setEmdDetails(EMDDetails emdDetails) {
        this.emdDetails = emdDetails;
    }

    public QuotationOpening getQuotationOpening() {
        return quotationOpening;
    }

    public void setQuotationOpening(QuotationOpening quotationOpening) {
        this.quotationOpening = quotationOpening;
    }

    public QuotationEvaluation getQuotationEvaluation() {
        return quotationEvaluation;
    }

    public void setQuotationEvaluation(QuotationEvaluation quotationEvaluation) {
        this.quotationEvaluation = quotationEvaluation;
    }

    public Award getAward() {
        return award;
    }

    public void setAward(Award award) {
        this.award = award;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public PackageDetails getPkgDetails() {
        return pkgDetails;
    }

    public void setPkgDetails(PackageDetails pkgDetails) {
        this.pkgDetails = pkgDetails;
    }

    public InvitationLetter getInvitationLetter() {
        return invitationLetter;
    }

    public void setInvitationLetter(InvitationLetter invitationLetter) {
        this.invitationLetter = invitationLetter;
    }

    public GRN getGrn() {
        return grn;
    }

    public void setGrn(GRN grn) {
        this.grn = grn;
    }

    public ArrayList<Uploads> getUploads() {
        return uploads;
    }

    public void setUploads(ArrayList<Uploads> uploads) {
        this.uploads = uploads;
    }

    public WorkOrder getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(WorkOrder workOrder) {
        this.workOrder = workOrder;
    }

    public LetterOfAcceptance getLetterOfAcceptance() {
        return letterOfAcceptance;
    }

    public void setLetterOfAcceptance(LetterOfAcceptance letterOfAcceptance) {
        this.letterOfAcceptance = letterOfAcceptance;
    }

    public Bid getGenofBid() {
        return genofBid;
    }

    public void setGenofBid(Bid genofBid) {
        this.genofBid = genofBid;
    }

    public LoaAward getLoaAward() {
        return loaAward;
    }

    public void setLoaAward(LoaAward loaAward) {
        this.loaAward = loaAward;
    }

    public AdvIssueOfBid getAdvIssueOfBid() {
        return advIssueOfBid;
    }

    public void setAdvIssueOfBid(AdvIssueOfBid advIssueOfBid) {
        this.advIssueOfBid = advIssueOfBid;
    }

    public WCCPayments getWccPayments() {
        return wccPayments;
    }

    public void setWccPayments(WCCPayments wccPayments) {
        this.wccPayments = wccPayments;
    }

    public ArrayList<PaymentDetails> getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(ArrayList<PaymentDetails> paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public ArrayList<SupplierDetails> getSupplierDetails() {
        return supplierDetails;
    }

    public void setSupplierDetails(ArrayList<SupplierDetails> supplierDetails) {
        this.supplierDetails = supplierDetails;
    }

    public JsonNode getPackageMetaInfo() {
        return packageMetaInfo;
    }

    public void setPackageMetaInfo(JsonNode packageMetaInfo) {
        this.packageMetaInfo = packageMetaInfo;
    }

    public ArrayList<ProcuringDepartmentDetails> getProcuringDepartmentDetails() {
        return procuringDepartmentDetails;
    }

    public void setProcuringDepartmentDetails(ArrayList<ProcuringDepartmentDetails> procuringDepartmentDetails) {
        this.procuringDepartmentDetails = procuringDepartmentDetails;
    }

    public ArrayList<QuestionsDetails> getQuestionsDetails() {
        return questionsDetails;
    }

    public void setQuestionsDetails(ArrayList<QuestionsDetails> questionsDetails) {
        this.questionsDetails = questionsDetails;
    }

    public CompleteCheckList getCompleteCheckLists() {
        return completeCheckLists;
    }

    public void setCompleteCheckLists(CompleteCheckList completeCheckLists) {
        this.completeCheckLists = completeCheckLists;
    }

    public TermsOfReference getTermsOfReference() {
        return termsOfReference;
    }

    public void setTermsOfReference(TermsOfReference termsOfReference) {
        this.termsOfReference = termsOfReference;
    }

    public ArrayList<TechEvalCommittee> getTechEvalCommittee() {
        return techEvalCommittee;
    }

    public void setTechEvalCommittee(ArrayList<TechEvalCommittee> techEvalCommittee) {
        this.techEvalCommittee = techEvalCommittee;
    }

    public ShortListing getShortListing() {
        return shortListing;
    }

    public void setShortListing(ShortListing shortListing) {
        this.shortListing = shortListing;
    }

    public RFP getRfp() {
        return rfp;
    }

    public void setRfp(RFP rfp) {
        this.rfp = rfp;
    }

    public IssueOfRFP getIssueOfRFP() {
        return issueOfRFP;
    }

    public void setIssueOfRFP(IssueOfRFP issueOfRFP) {
        this.issueOfRFP = issueOfRFP;
    }

    public AdvExpOfInterest getAdvExpOfInterest() {
        return advExpOfInterest;
    }

    public void setAdvExpOfInterest(AdvExpOfInterest advExpOfInterest) {
        this.advExpOfInterest = advExpOfInterest;
    }

    public TechEvaluation getTechEvaluation() {
        return techEvaluation;
    }

    public void setTechEvaluation(TechEvaluation techEvaluation) {
        this.techEvaluation = techEvaluation;
    }

    public FinancialOpening getFinancialOpening() {
        return financialOpening;
    }

    public void setFinancialOpening(FinancialOpening financialOpening) {
        this.financialOpening = financialOpening;
    }

    public ContractGeneration getContractGeneration() {
        return contractGeneration;
    }

    public void setContractGeneration(ContractGeneration contractGeneration) {
        this.contractGeneration = contractGeneration;
    }

    public ContractAward getContractAward() {
        return contractAward;
    }

    public void setContractAward(ContractAward contractAward) {
        this.contractAward = contractAward;
    }

    public ContractTerms getContractTerms() {
        return contractTerms;
    }

    public void setContractTerms(ContractTerms contractTerms) {
        this.contractTerms = contractTerms;
    }

    public StatusData getStatusData() {
        return statusData;
    }

    public void setStatusData(StatusData statusData) {
        this.statusData = statusData;
    }

    public PrecurementMethodTimeLinePojo getTimeLine() {
        return timeLine;
    }

    public void setTimeLine(PrecurementMethodTimeLinePojo timeLine) {
        this.timeLine = timeLine;
    }

    public List<PriorReview> getPriorReview() {
        return priorReview;
    }

    public void setPriorReview(List<PriorReview> priorReview) {
        this.priorReview = priorReview;
    }

    public ArrayList<Extension> getExtension() {
        return extension;
    }

    public void setExtension(ArrayList<Extension> extension) {
        this.extension = extension;
    }

    public PriorReviewMetaInfo getPriorReviewMetaInfo() {
        return priorReviewMetaInfo;
    }

    public void setPriorReviewMetaInfo(PriorReviewMetaInfo priorReviewMetaInfo) {
        this.priorReviewMetaInfo = priorReviewMetaInfo;
    }

    public L1Supplier getL1Supplier() {
        return l1Supplier;
    }

    public void setL1Supplier(L1Supplier l1Supplier) {
        this.l1Supplier = l1Supplier;
    }

    public Map<String, ArrayList<Uploads>> getDocGeneration() {
        return docGeneration;
    }

    public void setDocGeneration(Map<String, ArrayList<Uploads>> docGeneration) {
        this.docGeneration = docGeneration;
    }

}
