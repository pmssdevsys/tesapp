/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;

/**
 *
 * @author jaspreet
 */
public class QuotationDetail {

    Double packageReadOutPrice;
    Double totalPriceOfItems;
    ArrayList<QuotationItems> items;
    ArrayList<QuotationWork> works;

    public Double getPackageReadOutPrice() {
        return packageReadOutPrice;
    }
    
    public void setPackageReadOutPrice(Double packageReadOutPrice) {
        this.packageReadOutPrice = packageReadOutPrice;
    }

    public Double getTotalPriceOfItems() {
        return totalPriceOfItems;
    }

    public void setTotalPriceOfItems(Double totalPriceOfItems) {
        this.totalPriceOfItems = totalPriceOfItems;
    }

    public ArrayList<QuotationItems> getItems() {
        return items;
    }

    public void setItems(ArrayList<QuotationItems> items) {
        this.items = items;
    }

    public ArrayList<QuotationWork> getWorks() {
        return works;
    }

    public void setWorks(ArrayList<QuotationWork> works) {
        this.works = works;
    }
    

}
