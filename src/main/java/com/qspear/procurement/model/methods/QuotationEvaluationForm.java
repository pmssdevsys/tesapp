package com.qspear.procurement.model.methods;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;

/**
 * Created by e1002703 on 4/3/2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuotationEvaluationForm {

    String supplierName;
    Integer supplierId;

    ArrayList<Questions> questions;

    QuotationDetail quotationDetail;

    TechEvaluationDetail techEvaluationDetail;

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public ArrayList<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Questions> questions) {
        this.questions = questions;
    }

    public TechEvaluationDetail getTechEvaluationDetail() {
        return techEvaluationDetail;
    }

    public void setTechEvaluationDetail(TechEvaluationDetail techEvaluationDetail) {
        this.techEvaluationDetail = techEvaluationDetail;
    }

    public QuotationDetail getQuotationDetail() {
        return quotationDetail;
    }

    public void setQuotationDetail(QuotationDetail quotationDetail) {
        this.quotationDetail = quotationDetail;
    }

}
