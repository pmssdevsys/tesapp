/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class BidExtension {

    private Date currentDate;
    private Date extendedDate;
    private String reasonForExtension;

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public Date getExtendedDate() {
        return extendedDate;
    }

    public void setExtendedDate(Date extendedDate) {
        this.extendedDate = extendedDate;
    }

    public String getReasonForExtension() {
        return reasonForExtension;
    }

    public void setReasonForExtension(String reasonForExtension) {
        this.reasonForExtension = reasonForExtension;
    }

}
