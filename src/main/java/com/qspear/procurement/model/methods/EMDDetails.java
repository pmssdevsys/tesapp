package com.qspear.procurement.model.methods;

public class EMDDetails {
	Integer emdDetailId;
	String bankName;
	String branchName;
	Double emdPercentage;
        Double emdAmount;
        String remarks;
        String bankAccountHolder;
        String accountNumber;
	String ifscCode;
	Integer isEMDRequired;
	
        
        
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

    public Integer getEmdDetailId() {
        return emdDetailId;
    }

    public void setEmdDetailId(Integer emdDetailId) {
        this.emdDetailId = emdDetailId;
    }

    public Double getEmdPercentage() {
        return emdPercentage;
    }

    public void setEmdPercentage(Double emdPercentage) {
        this.emdPercentage = emdPercentage;
    }

    public Double getEmdAmount() {
        return emdAmount;
    }

    public void setEmdAmount(Double emdAmount) {
        this.emdAmount = emdAmount;
    }

    public String getBankAccountHolder() {
        return bankAccountHolder;
    }

    public void setBankAccountHolder(String bankAccountHolder) {
        this.bankAccountHolder = bankAccountHolder;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public Integer getIsEMDRequired() {
        return isEMDRequired;
    }

    public void setIsEMDRequired(Integer isEMDRequired) {
        this.isEMDRequired = isEMDRequired;
    }
	
}
