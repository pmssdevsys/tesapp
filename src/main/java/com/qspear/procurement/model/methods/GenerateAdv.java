/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class GenerateAdv {

    String localPaperName;
    String nationalPaperName;
    Date dateReleaseAdv;
    String packageName;
    Integer packageId;

    public String getLocalPaperName() {
        return localPaperName;
    }

    public void setLocalPaperName(String localPaperName) {
        this.localPaperName = localPaperName;
    }

    public String getNationalPaperName() {
        return nationalPaperName;
    }

    public void setNationalPaperName(String nationalPaperName) {
        this.nationalPaperName = nationalPaperName;
    }

    public Date getDateReleaseAdv() {
        return dateReleaseAdv;
    }

    public void setDateReleaseAdv(Date dateReleaseAdv) {
        this.dateReleaseAdv = dateReleaseAdv;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

}
