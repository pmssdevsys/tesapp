/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class FinancialProposalDetails {
     Integer isWordWorkBankNOC;
    Date wordBankNOCDate;
    Double proposalAmount;

    public Integer getIsWordWorkBankNOC() {
        return isWordWorkBankNOC;
    }

    public void setIsWordWorkBankNOC(Integer isWordWorkBankNOC) {
        this.isWordWorkBankNOC = isWordWorkBankNOC;
    }

    public Date getWordBankNOCDate() {
        return wordBankNOCDate;
    }

    public void setWordBankNOCDate(Date wordBankNOCDate) {
        this.wordBankNOCDate = wordBankNOCDate;
    }

    public Double getProposalAmount() {
        return proposalAmount;
    }

    public void setProposalAmount(Double proposalAmount) {
        this.proposalAmount = proposalAmount;
    }

}
