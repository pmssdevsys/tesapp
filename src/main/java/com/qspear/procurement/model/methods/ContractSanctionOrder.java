package com.qspear.procurement.model.methods;

import java.util.Date;

public class ContractSanctionOrder {
	Date contractDate;
    String contractNumber;
    Date sanctionDate;
    private String sanctionNumber;
    private Integer contractSanctionId;
	public Date getContractDate() {
		return contractDate;
	}
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public Date getSanctionDate() {
		return sanctionDate;
	}
	public void setSanctionDate(Date sanctionDate) {
		this.sanctionDate = sanctionDate;
	}
	public String getSanctionNumber() {
		return sanctionNumber;
	}
	public void setSanctionNumber(String sanctionNumber) {
		this.sanctionNumber = sanctionNumber;
	}
	public Integer getContractSanctionId() {
		return contractSanctionId;
	}
	public void setContractSanctionId(Integer contractSanctionId) {
		this.contractSanctionId = contractSanctionId;
	}
}
