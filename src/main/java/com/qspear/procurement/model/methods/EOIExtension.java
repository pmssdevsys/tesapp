package com.qspear.procurement.model.methods;

import java.util.Date;

import com.qspear.procurement.persistence.TeqipPackage;

public class EOIExtension {

    private Integer eoiextensionId;

    Date extendedDate;
    String reasonforExtension;
    Date submissionDate;

    public Integer getEoiextensionId() {
        return eoiextensionId;
    }

    public void setEoiextensionId(Integer eoiextensionId) {
        this.eoiextensionId = eoiextensionId;
    }

    public Date getExtendedDate() {
        return extendedDate;
    }

    public void setExtendedDate(Date extendedDate) {
        this.extendedDate = extendedDate;
    }

    public String getReasonforExtension() {
        return reasonforExtension;
    }

    public void setReasonforExtension(String reasonforExtension) {
        this.reasonforExtension = reasonforExtension;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

}
