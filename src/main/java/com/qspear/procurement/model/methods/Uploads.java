package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class Uploads {
    Integer packageDocumentId;
    String documentCategory;
    String fileName;
    String fileNameLink;
    String description;
    String uploadedBy;
    Date uploadedDate;
     byte[] file;

    public  String  getDocumentCategory() {
        return documentCategory;
    }

    public void setDocumentCategory( String  documentCategory) {
        this.documentCategory = documentCategory;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileNameLink() {
        return fileNameLink;
    }

    public void setFileNameLink(String fileNameLink) {
        this.fileNameLink = fileNameLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public Date getUploadedDate() {
        return uploadedDate;
    }

    public void setUploadedDate(Date uploadedDate) {
        this.uploadedDate = uploadedDate;
    }

    public Integer getPackageDocumentId() {
        return packageDocumentId;
    }

    public void setPackageDocumentId(Integer packageDocumentId) {
        this.packageDocumentId = packageDocumentId;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

 
}
