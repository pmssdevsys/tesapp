package com.qspear.procurement.model.methods;

import java.util.List;

public class StageModel {

    Integer packageStageIndexId;
    String stageName;
    Integer stageIndex;
    List<ActionModel> actionsList;

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public Integer getStageIndex() {
        return stageIndex;
    }

    public void setStageIndex(Integer stageIndex) {
        this.stageIndex = stageIndex;
    }

    public List<ActionModel> getActionsList() {
        return actionsList;
    }

    public void setActionsList(List<ActionModel> actionsList) {
        this.actionsList = actionsList;
    }

    public Integer getPackageStageIndexId() {
        return packageStageIndexId;
    }

    public void setPackageStageIndexId(Integer packageStageIndexId) {
        this.packageStageIndexId = packageStageIndexId;
    }

}
