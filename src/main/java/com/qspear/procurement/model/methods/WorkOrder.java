/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class WorkOrder {

    Integer packageWorkOrderId;
    String workOrderNo;

    Date performanceSecurityReceivedDate;
    Date performanceSecurityExpiryDate;
    Date contractStartDate;
    Date workStartDate;
    Date workCompletionDate;
    Double basicValue;
    Double sumOfApplicableTaxes;
    Double contractValue;
    Date workGeneratedDate;
    Double performanceSecurityAmount;

    ArrayList<WorkOrderWorks> works;

    ArrayList<Uploads> woGeneration;

    public Integer getPackageWorkOrderId() {
        return packageWorkOrderId;
    }

    public void setPackageWorkOrderId(Integer packageWorkOrderId) {
        this.packageWorkOrderId = packageWorkOrderId;
    }

    public Date getPerformanceSecurityReceivedDate() {
        return performanceSecurityReceivedDate;
    }

    public void setPerformanceSecurityReceivedDate(Date performanceSecurityReceivedDate) {
        this.performanceSecurityReceivedDate = performanceSecurityReceivedDate;
    }

    public Date getPerformanceSecurityExpiryDate() {
        return performanceSecurityExpiryDate;
    }

    public void setPerformanceSecurityExpiryDate(Date performanceSecurityExpiryDate) {
        this.performanceSecurityExpiryDate = performanceSecurityExpiryDate;
    }

    public Date getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(Date contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public Date getWorkStartDate() {
        return workStartDate;
    }

    public void setWorkStartDate(Date workStartDate) {
        this.workStartDate = workStartDate;
    }

    public Date getWorkCompletionDate() {
        return workCompletionDate;
    }

    public void setWorkCompletionDate(Date workCompletionDate) {
        this.workCompletionDate = workCompletionDate;
    }

    public Double getBasicValue() {
        return basicValue;
    }

    public void setBasicValue(Double basicValue) {
        this.basicValue = basicValue;
    }

    public Double getSumOfApplicableTaxes() {
        return sumOfApplicableTaxes;
    }

    public void setSumOfApplicableTaxes(Double sumOfApplicableTaxes) {
        this.sumOfApplicableTaxes = sumOfApplicableTaxes;
    }

    public Double getContractValue() {
        return contractValue;
    }

    public void setContractValue(Double contractValue) {
        this.contractValue = contractValue;
    }

    public ArrayList<WorkOrderWorks> getWorks() {
        return works;
    }

    public void setWorks(ArrayList<WorkOrderWorks> works) {
        this.works = works;
    }

    public String getWorkOrderNo() {
        return workOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public ArrayList<Uploads> getWoGeneration() {
        return woGeneration;
    }

    public void setWoGeneration(ArrayList<Uploads> woGeneration) {
        this.woGeneration = woGeneration;
    }

    public Date getWorkGeneratedDate() {
        return workGeneratedDate;
    }

    public void setWorkGeneratedDate(Date workGeneratedDate) {
        this.workGeneratedDate = workGeneratedDate;
    }

    public Double getPerformanceSecurityAmount() {
        return performanceSecurityAmount;
    }

    public void setPerformanceSecurityAmount(Double performanceSecurityAmount) {
        this.performanceSecurityAmount = performanceSecurityAmount;
    }

    
}

