/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

/**
 *
 * @author jaspreet
 */
public class WorkOrderWorks {

    Integer woWorkId;
    String workName;
    String workSpecification;
    Double workEstimatedCost;
    Double workCost;
    String comments;
    Integer supplierId;
    String supplierName;

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getWorkSpecification() {
        return workSpecification;
    }

    public void setWorkSpecification(String workSpecification) {
        this.workSpecification = workSpecification;
    }

    public Double getWorkEstimatedCost() {
        return workEstimatedCost;
    }

    public void setWorkEstimatedCost(Double workEstimatedCost) {
        this.workEstimatedCost = workEstimatedCost;
    }

    public Double getWorkCost() {
        return workCost;
    }

    public void setWorkCost(Double workCost) {
        this.workCost = workCost;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getWoWorkId() {
        return woWorkId;
    }

    public void setWoWorkId(Integer woWorkId) {
        this.woWorkId = woWorkId;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

}
