package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class GeneratePO {

    Integer packagePurchaseOrderId;
    Integer supplierId;
    String supplierName;
    String poNumber;
    Date poDate;
    Double gst;
    Double octroi;
    Double totalContractValue;
    Double evaluatedCost;
    Double totalBaseCost;
    Double performanceSecurityAmount;
    Integer isPoFinalised;

    public Integer getIsPoFinalised() {
        return isPoFinalised;
    }

    public void setIsPoFinalised(Integer isPoFinalised) {
        this.isPoFinalised = isPoFinalised;
    }

    public Integer getPackagePurchaseOrderId() {
        return packagePurchaseOrderId;
    }

    public void setPackagePurchaseOrderId(Integer packagePurchaseOrderId) {
        this.packagePurchaseOrderId = packagePurchaseOrderId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public Date getPoDate() {
        return poDate;
    }

    public void setPoDate(Date poDate) {
        this.poDate = poDate;
    }

    public Double getGst() {
        return gst;
    }

    public void setGst(Double gst) {
        this.gst = gst;
    }

    public Double getOctroi() {
        return octroi;
    }

    public void setOctroi(Double octroi) {
        this.octroi = octroi;
    }

    public Double getTotalContractValue() {
        return totalContractValue;
    }

    public void setTotalContractValue(Double totalContractValue) {
        this.totalContractValue = totalContractValue;
    }

    public Double getEvaluatedCost() {
        return evaluatedCost;
    }

    public void setEvaluatedCost(Double evaluatedCost) {
        this.evaluatedCost = evaluatedCost;
    }

    public Double getTotalBaseCost() {
        return totalBaseCost;
    }

    public void setTotalBaseCost(Double totalBaseCost) {
        this.totalBaseCost = totalBaseCost;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public Double getPerformanceSecurityAmount() {
        return performanceSecurityAmount;
    }

    public void setPerformanceSecurityAmount(Double performanceSecurityAmount) {
        this.performanceSecurityAmount = performanceSecurityAmount;
    }

}
