/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class PriorReview {

    Integer isPriorReviewRequired;
    Integer stageIndex;
    String stageName;
    String priorReviewStageText;
    String reviewStatus;
    String comments;
    Integer reviewedByUserId;
    String reviewedByUserName;
    String reviewedByUserRoleType;
    Date updatedTime;
    Date submittedTime;
    String requestorName;
    String roleOfNextActor;

    public String getRoleOfNextActor() {
        return roleOfNextActor;
    }

    public void setRoleOfNextActor(String roleOfNextActor) {
        this.roleOfNextActor = roleOfNextActor;
    }
    
    public Date getSubmittedTime() {
        return submittedTime;
    }

    public void setSubmittedTime(Date submittedTime) {
        this.submittedTime = submittedTime;
    }

    public String getRequestorName() {
        return requestorName;
    }

    public void setRequestorName(String requestorName) {
        this.requestorName = requestorName;
    }    
    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getIsPriorReviewRequired() {
        return isPriorReviewRequired;
    }

    public void setIsPriorReviewRequired(Integer isPriorReviewRequired) {
        this.isPriorReviewRequired = isPriorReviewRequired;
    }

    public Integer getStageIndex() {
        return stageIndex;
    }

    public void setStageIndex(Integer stageIndex) {
        this.stageIndex = stageIndex;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getPriorReviewStageText() {
        return priorReviewStageText;
    }

    public void setPriorReviewStageText(String priorReviewStageText) {
        this.priorReviewStageText = priorReviewStageText;
    }

    public String getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(String reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getReviewedByUserId() {
        return reviewedByUserId;
    }

    public void setReviewedByUserId(Integer reviewedByUserId) {
        this.reviewedByUserId = reviewedByUserId;
    }

    public String getReviewedByUserName() {
        return reviewedByUserName;
    }

    public void setReviewedByUserName(String reviewedByUserName) {
        this.reviewedByUserName = reviewedByUserName;
    }

    public String getReviewedByUserRoleType() {
        return reviewedByUserRoleType;
    }

    public void setReviewedByUserRoleType(String reviewedByUserRoleType) {
        this.reviewedByUserRoleType = reviewedByUserRoleType;
    }

}
