/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class RFPDetail {

    Integer id;
    String typeOfContract;
    Date proposalSubmissionDate;
    Date preProposalMeetingDate;
    String preProposalVenue;
    Integer isWordWorkBankNOC;
    Date wordBankNOCDate;
    

    public String getPreProposalVenue() {
        return preProposalVenue;
    }

    public void setPreProposalVenue(String preProposalVenue) {
        this.preProposalVenue = preProposalVenue;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeOfContract() {
        return typeOfContract;
    }

    public void setTypeOfContract(String typeOfContract) {
        this.typeOfContract = typeOfContract;
    }

    public Date getProposalSubmissionDate() {
        return proposalSubmissionDate;
    }

    public void setProposalSubmissionDate(Date proposalSubmissionDate) {
        this.proposalSubmissionDate = proposalSubmissionDate;
    }

    public Date getPreProposalMeetingDate() {
        return preProposalMeetingDate;
    }

    public void setPreProposalMeetingDate(Date preProposalMeetingDate) {
        this.preProposalMeetingDate = preProposalMeetingDate;
    }

    public Integer getIsWordWorkBankNOC() {
        return isWordWorkBankNOC;
    }

    public void setIsWordWorkBankNOC(Integer isWordWorkBankNOC) {
        this.isWordWorkBankNOC = isWordWorkBankNOC;
    }

    public Date getWordBankNOCDate() {
        return wordBankNOCDate;
    }

    public void setWordBankNOCDate(Date wordBankNOCDate) {
        this.wordBankNOCDate = wordBankNOCDate;
    }

}
