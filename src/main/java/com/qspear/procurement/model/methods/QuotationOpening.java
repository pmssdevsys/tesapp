package com.qspear.procurement.model.methods;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by e1002703 on 4/3/2018.
 */
public class QuotationOpening {

    Integer packQuotationDetailId;
    Print print;
    Date quotationOpeningDate;
    String quotationMom;
    Date quotationOpeningTime;
    ArrayList<QuotationForm> quotationForm;
    ArrayList<Uploads> qoGeneration;

    public Print getPrint() {
        return print;
    }

    public void setPrint(Print print) {
        this.print = print;
    }

    public Date getQuotationOpeningDate() {
        return quotationOpeningDate;
    }

    public void setQuotationOpeningDate(Date quotationOpeningDate) {
        this.quotationOpeningDate = quotationOpeningDate;
    }

    public String getQuotationMom() {
        return quotationMom;
    }

    public void setQuotationMom(String quotationMom) {
        this.quotationMom = quotationMom;
    }

    public Date getQuotationOpeningTime() {
        return quotationOpeningTime;
    }

    public void setQuotationOpeningTime(Date quotationOpeningTime) {
        this.quotationOpeningTime = quotationOpeningTime;
    }

    public ArrayList<QuotationForm> getQuotationForm() {
        return quotationForm;
    }

    public void setQuotationForm(ArrayList<QuotationForm> quotationForm) {
        this.quotationForm = quotationForm;
    }

    public Integer getPackQuotationDetailId() {
        return packQuotationDetailId;
    }

    public void setPackQuotationDetailId(Integer packQuotationDetailId) {
        this.packQuotationDetailId = packQuotationDetailId;
    }

    public ArrayList<Uploads> getQoGeneration() {
        return qoGeneration;
    }

    public void setQoGeneration(ArrayList<Uploads> qoGeneration) {
        this.qoGeneration = qoGeneration;
    }

}
