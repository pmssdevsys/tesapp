/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model.methods;

import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class PreProposalMeetingDetails {

    Integer id;
    Date actualPreProposalDate;
    Integer clarificationIssuedToConsultants;
    String preProposalMinutes;
    Date clarificationIssuedDate;
    String clarificationDetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getActualPreProposalDate() {
        return actualPreProposalDate;
    }

    public void setActualPreProposalDate(Date actualPreProposalDate) {
        this.actualPreProposalDate = actualPreProposalDate;
    }

    public Integer getClarificationIssuedToConsultants() {
        return clarificationIssuedToConsultants;
    }

    public void setClarificationIssuedToConsultants(Integer clarificationIssuedToConsultants) {
        this.clarificationIssuedToConsultants = clarificationIssuedToConsultants;
    }

    public String getPreProposalMinutes() {
        return preProposalMinutes;
    }

    public void setPreProposalMinutes(String preProposalMinutes) {
        this.preProposalMinutes = preProposalMinutes;
    }

    public Date getClarificationIssuedDate() {
        return clarificationIssuedDate;
    }

    public void setClarificationIssuedDate(Date clarificationIssuedDate) {
        this.clarificationIssuedDate = clarificationIssuedDate;
    }

    public String getClarificationDetails() {
        return clarificationDetails;
    }

    public void setClarificationDetails(String clarificationDetails) {
        this.clarificationDetails = clarificationDetails;
    }

}
