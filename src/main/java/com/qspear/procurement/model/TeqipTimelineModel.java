package com.qspear.procurement.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

public class TeqipTimelineModel {

    @JsonProperty(value = "AdvertisementDate")
    private String advertisementDate_days;

    @JsonProperty(value = "AdvertisementDateWithoutBankNOC")
    private String advertisementDate_WithoutBankNOC_days;

    @JsonProperty(value = "BankNOCForBiddingDocuments")
    private String bankNOCForBiddingDocuments_days;

    @JsonProperty(value = "BidDocumentPreparationDate")
    private String bidDocumentPreparationDate_days;

    @JsonProperty(value = "BidInvitationDate")
    private String bidInvitationDate_days;

    @JsonProperty(value = "BidOpeningDate")
    private String bidOpeningDate_days;

    @JsonProperty(value = "ContractAwardDate")
    private String contractAwardDate_days;

    @JsonProperty(value = "ContractAwardDateWithoutBankNOC")
    private String contractAwardDate_WithoutBankNOC_days;

    @JsonProperty(value = "ContractCompletionDate")
    private String contractCompletionDate_days;

    @JsonProperty(value = "ContractCompletionDateWithoutBankNOC")
    private String contractCompletionDate_WithoutBankNOC_days;

    @JsonProperty(value = "EvaluationDate")
    private String evaluationDate_days;

    @JsonProperty(value = "EvaluationDateWithoutBankNOC")
    private String evaluationDate_WithoutBankNOC_days;

    @JsonProperty(value = "FinalDraftToBeForwardedToTheBankDate")
    private String finalDraftToBeForwardedToTheBankDate_days;

    @JsonProperty(value = "LastDateToReceiveProposals")
    private String lastDateToReceiveProposals_days;

    @JsonProperty(value = "LastDateToReceiveProposalsWithoutBankNOC")
    private String lastDateToReceiveProposals_WithoutBankNOC_days;

    @JsonProperty(value = "NoObjectionFromBankForEvaluation")
    private String noObjectionFromBankForEvaluation_days;

    @JsonProperty(value = "NoObjectionFromBankForRFP")
    private String noObjectionFromBankForRFP_days;

    @JsonProperty(value = "RFPIssuedDate")
    private String RFPIssuedDate_days;

    @JsonProperty(value = "RFPIssuedDateWithoutBankNOC")
    private String RFPIssuedDate_WithoutBankNOC_days;

    @JsonProperty(value = "TORFinalizationDate")
    private String TORFinalizationDate_days;

    @JsonProperty(value = "TORFinalizationDateWithoutBankNOC")
    private String TORFinalizationDate_WithoutBankNOC_days;

    @JsonProperty(value = "LastdateofsubmissionofEOI")
    private String lastdateofsubmissionofEOI;

    @JsonProperty(value = "ShortlistingOfEOI")
    private String shortlistingOfEOI;

    @JsonProperty(value = "RFPApprovalDate")
    private String rFPApprovalDate;

    @JsonProperty(value = "FinancialEvaluationDate")
    private String financialEvaluationDate;
   
    public String getAdvertisementDate_days() {
        return advertisementDate_days;
    }

    public void setAdvertisementDate_days(String advertisementDate_days) {
        this.advertisementDate_days = advertisementDate_days;
    }

    public String getAdvertisementDate_WithoutBankNOC_days() {
        return advertisementDate_WithoutBankNOC_days;
    }

    public void setAdvertisementDate_WithoutBankNOC_days(String advertisementDate_WithoutBankNOC_days) {
        this.advertisementDate_WithoutBankNOC_days = advertisementDate_WithoutBankNOC_days;
    }

    public String getBankNOCForBiddingDocuments_days() {
        return bankNOCForBiddingDocuments_days;
    }

    public void setBankNOCForBiddingDocuments_days(String bankNOCForBiddingDocuments_days) {
        this.bankNOCForBiddingDocuments_days = bankNOCForBiddingDocuments_days;
    }

    public String getBidDocumentPreparationDate_days() {
        return bidDocumentPreparationDate_days;
    }

    public void setBidDocumentPreparationDate_days(String bidDocumentPreparationDate_days) {
        this.bidDocumentPreparationDate_days = bidDocumentPreparationDate_days;
    }

    public String getBidInvitationDate_days() {
        return bidInvitationDate_days;
    }

    public void setBidInvitationDate_days(String bidInvitationDate_days) {
        this.bidInvitationDate_days = bidInvitationDate_days;
    }

    public String getBidOpeningDate_days() {
        return bidOpeningDate_days;
    }

    public void setBidOpeningDate_days(String bidOpeningDate_days) {
        this.bidOpeningDate_days = bidOpeningDate_days;
    }

    public String getContractAwardDate_days() {
        return contractAwardDate_days;
    }

    public void setContractAwardDate_days(String contractAwardDate_days) {
        this.contractAwardDate_days = contractAwardDate_days;
    }

    public String getContractAwardDate_WithoutBankNOC_days() {
        return contractAwardDate_WithoutBankNOC_days;
    }

    public void setContractAwardDate_WithoutBankNOC_days(String contractAwardDate_WithoutBankNOC_days) {
        this.contractAwardDate_WithoutBankNOC_days = contractAwardDate_WithoutBankNOC_days;
    }

    public String getContractCompletionDate_days() {
        return contractCompletionDate_days;
    }

    public void setContractCompletionDate_days(String contractCompletionDate_days) {
        this.contractCompletionDate_days = contractCompletionDate_days;
    }

    public String getContractCompletionDate_WithoutBankNOC_days() {
        return contractCompletionDate_WithoutBankNOC_days;
    }

    public void setContractCompletionDate_WithoutBankNOC_days(String contractCompletionDate_WithoutBankNOC_days) {
        this.contractCompletionDate_WithoutBankNOC_days = contractCompletionDate_WithoutBankNOC_days;
    }

    public String getEvaluationDate_days() {
        return evaluationDate_days;
    }

    public void setEvaluationDate_days(String evaluationDate_days) {
        this.evaluationDate_days = evaluationDate_days;
    }

    public String getEvaluationDate_WithoutBankNOC_days() {
        return evaluationDate_WithoutBankNOC_days;
    }

    public void setEvaluationDate_WithoutBankNOC_days(String evaluationDate_WithoutBankNOC_days) {
        this.evaluationDate_WithoutBankNOC_days = evaluationDate_WithoutBankNOC_days;
    }

    public String getFinalDraftToBeForwardedToTheBankDate_days() {
        return finalDraftToBeForwardedToTheBankDate_days;
    }

    public void setFinalDraftToBeForwardedToTheBankDate_days(String finalDraftToBeForwardedToTheBankDate_days) {
        this.finalDraftToBeForwardedToTheBankDate_days = finalDraftToBeForwardedToTheBankDate_days;
    }

    public String getLastDateToReceiveProposals_days() {
        return lastDateToReceiveProposals_days;
    }

    public void setLastDateToReceiveProposals_days(String lastDateToReceiveProposals_days) {
        this.lastDateToReceiveProposals_days = lastDateToReceiveProposals_days;
    }

    public String getLastDateToReceiveProposals_WithoutBankNOC_days() {
        return lastDateToReceiveProposals_WithoutBankNOC_days;
    }

    public void setLastDateToReceiveProposals_WithoutBankNOC_days(String lastDateToReceiveProposals_WithoutBankNOC_days) {
        this.lastDateToReceiveProposals_WithoutBankNOC_days = lastDateToReceiveProposals_WithoutBankNOC_days;
    }

    public String getNoObjectionFromBankForEvaluation_days() {
        return noObjectionFromBankForEvaluation_days;
    }

    public void setNoObjectionFromBankForEvaluation_days(String noObjectionFromBankForEvaluation_days) {
        this.noObjectionFromBankForEvaluation_days = noObjectionFromBankForEvaluation_days;
    }

    public String getNoObjectionFromBankForRFP_days() {
        return noObjectionFromBankForRFP_days;
    }

    public void setNoObjectionFromBankForRFP_days(String noObjectionFromBankForRFP_days) {
        this.noObjectionFromBankForRFP_days = noObjectionFromBankForRFP_days;
    }

    public String getRFPIssuedDate_days() {
        return RFPIssuedDate_days;
    }

    public void setRFPIssuedDate_days(String rFPIssuedDate_days) {
        RFPIssuedDate_days = rFPIssuedDate_days;
    }

    public String getRFPIssuedDate_WithoutBankNOC_days() {
        return RFPIssuedDate_WithoutBankNOC_days;
    }

    public void setRFPIssuedDate_WithoutBankNOC_days(String rFPIssuedDate_WithoutBankNOC_days) {
        RFPIssuedDate_WithoutBankNOC_days = rFPIssuedDate_WithoutBankNOC_days;
    }

    public String getTORFinalizationDate_days() {
        return TORFinalizationDate_days;
    }

    public void setTORFinalizationDate_days(String tORFinalizationDate_days) {
        TORFinalizationDate_days = tORFinalizationDate_days;
    }

    public String getTORFinalizationDate_WithoutBankNOC_days() {
        return TORFinalizationDate_WithoutBankNOC_days;
    }

    public void setTORFinalizationDate_WithoutBankNOC_days(String tORFinalizationDate_WithoutBankNOC_days) {
        TORFinalizationDate_WithoutBankNOC_days = tORFinalizationDate_WithoutBankNOC_days;
    }

    public String getLastdateofsubmissionofEOI() {
        return lastdateofsubmissionofEOI;
    }

    public void setLastdateofsubmissionofEOI(String lastdateofsubmissionofEOI) {
        this.lastdateofsubmissionofEOI = lastdateofsubmissionofEOI;
    }

    public String getShortlistingOfEOI() {
        return shortlistingOfEOI;
    }

    public void setShortlistingOfEOI(String shortlistingOfEOI) {
        this.shortlistingOfEOI = shortlistingOfEOI;
    }

    public String getrFPApprovalDate() {
        return rFPApprovalDate;
    }

    public void setrFPApprovalDate(String rFPApprovalDate) {
        this.rFPApprovalDate = rFPApprovalDate;
    }

    public String getFinancialEvaluationDate() {
        return financialEvaluationDate;
    }

    public void setFinancialEvaluationDate(String financialEvaluationDate) {
        this.financialEvaluationDate = financialEvaluationDate;
    }
}
