package com.qspear.procurement.model;

/*@Getter
@Setter
@ToString
@NoArgsConstructor*/
public class TeqipSubcomponentmasterModel {

	private Integer componentId;

	private Boolean isactive;

	private String subComponentName;

	private String subcomponentcode;

	private Integer teqipCategorymasterId;

	private Integer teqipSubcategorymasterId;
        
        private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsactive() {
		return isactive;
	}
	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Boolean isIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public String getSubComponentName() {
		return subComponentName;
	}

	public void setSubComponentName(String subComponentName) {
		this.subComponentName = subComponentName;
	}

	public String getSubcomponentcode() {
		return subcomponentcode;
	}

	public void setSubcomponentcode(String subcomponentcode) {
		this.subcomponentcode = subcomponentcode;
	}

	public Integer getTeqipCategorymasterId() {
		return teqipCategorymasterId;
	}

	public void setTeqipCategorymasterId(Integer teqipCategorymasterId) {
		this.teqipCategorymasterId = teqipCategorymasterId;
	}

	public Integer getTeqipSubcategorymasterId() {
		return teqipSubcategorymasterId;
	}

	public void setTeqipSubcategorymasterId(Integer teqipSubcategorymasterId) {
		this.teqipSubcategorymasterId = teqipSubcategorymasterId;
	}
} 
