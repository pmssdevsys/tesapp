package com.qspear.procurement.model;

import java.sql.Timestamp;
import java.util.Date;

public class SupplierMasterModel {
	private Integer supplierID;
	private String suppliername;
	private String supllieraddress;
	private String suppliercity;
	private String supplierstate;
	private Integer supplierstateid;
	public String getSupplierstate() {
		return supplierstate;
	}
	public void setSupplierstate(String supplierstate) {
		this.supplierstate = supplierstate;
	}
	private String suppliercountry;
	private String suppliernationality;
	private String suppliersource;
	private String supplierspecificsource;
	private String supplierrepresentativename;
	private String supplierpincode;
	private String supplierphoneno;
	private String supliertannumber;
	private String suplierfaxnumber;
	private String suplierpannumber;
	private Integer pmss_type;
	private String supliergstnumber;
	private String supliertaxnumber;
	private String suplieremailid;
	private String supliereletter;
	private Byte ISACTIVE;
	private Timestamp CREATED_ON;
	private Integer CREATED_BY;
	private Date MODIFY_ON;
	private Integer MODIFY_BY;
	String epfNo;
    String esiNo;
    String contractLabourLicenseNo;
    private Integer stateId;

	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public String getEpfNo() {
		return epfNo;
	}
	public void setEpfNo(String epfNo) {
		this.epfNo = epfNo;
	}
	public String getEsiNo() {
		return esiNo;
	}
	public void setEsiNo(String esiNo) {
		this.esiNo = esiNo;
	}
	public String getContractLabourLicenseNo() {
		return contractLabourLicenseNo;
	}
	public void setContractLabourLicenseNo(String contractLabourLicenseNo) {
		this.contractLabourLicenseNo = contractLabourLicenseNo;
	}
	public Timestamp getCREATED_ON() {
		return CREATED_ON;
	}
	public void setCREATED_ON(Timestamp cREATED_ON) {
		CREATED_ON = cREATED_ON;
	}
	public Integer getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(Integer cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public Date getMODIFY_ON() {
		return MODIFY_ON;
	}
	public void setMODIFY_ON(Date mODIFY_ON) {
		MODIFY_ON = mODIFY_ON;
	}
	public Integer getMODIFY_BY() {
		return MODIFY_BY;
	}
	public void setMODIFY_BY(Integer mODIFY_BY) {
		MODIFY_BY = mODIFY_BY;
	}
	public Integer getSupplierID() {
		return supplierID;
	}
	public void setSupplierID(Integer supplierID) {
		this.supplierID = supplierID;
	}
	public String getSuppliername() {
		return suppliername;
	}
	public void setSuppliername(String suppliername) {
		this.suppliername = suppliername;
	}
	public String getSupllieraddress() {
		return supllieraddress;
	}
	public void setSupllieraddress(String supllieraddress) {
		this.supllieraddress = supllieraddress;
	}
	public String getSuppliercity() {
		return suppliercity;
	}
	public void setSuppliercity(String suppliercity) {
		this.suppliercity = suppliercity;
	}
	
	public Integer getSupplierstateid() {
		return supplierstateid;
	}
	public void setSupplierstateid(Integer supplierstateid) {
		this.supplierstateid = supplierstateid;
	}
	public String getSuppliercountry() {
		return suppliercountry;
	}
	public void setSuppliercountry(String suppliercountry) {
		this.suppliercountry = suppliercountry;
	}
	public String getSuppliernationality() {
		return suppliernationality;
	}
	public void setSuppliernationality(String suppliernationality) {
		this.suppliernationality = suppliernationality;
	}
	public String getSuppliersource() {
		return suppliersource;
	}
	public void setSuppliersource(String suppliersource) {
		this.suppliersource = suppliersource;
	}
	public String getSupplierspecificsource() {
		return supplierspecificsource;
	}
	public void setSupplierspecificsource(String supplierspecificsource) {
		this.supplierspecificsource = supplierspecificsource;
	}
	public String getSupplierrepresentativename() {
		return supplierrepresentativename;
	}
	public void setSupplierrepresentativename(String supplierrepresentativename) {
		this.supplierrepresentativename = supplierrepresentativename;
	}
	public String getSupplierpincode() {
		return supplierpincode;
	}
	public void setSupplierpincode(String supplierpincode) {
		this.supplierpincode = supplierpincode;
	}
	public String getSupplierphoneno() {
		return supplierphoneno;
	}
	public void setSupplierphoneno(String supplierphoneno) {
		this.supplierphoneno = supplierphoneno;
	}
	public String getSupliertannumber() {
		return supliertannumber;
	}
	public void setSupliertannumber(String supliertannumber) {
		this.supliertannumber = supliertannumber;
	}
	public String getSuplierfaxnumber() {
		return suplierfaxnumber;
	}
	public void setSuplierfaxnumber(String suplierfaxnumber) {
		this.suplierfaxnumber = suplierfaxnumber;
	}
	public String getSuplierpannumber() {
		return suplierpannumber;
	}
	public void setSuplierpannumber(String suplierpannumber) {
		this.suplierpannumber = suplierpannumber;
	}
	public Integer getPmss_type() {
		return pmss_type;
	}
	public void setPmss_type(Integer pmss_type) {
		this.pmss_type = pmss_type;
	}
	public String getSupliergstnumber() {
		return supliergstnumber;
	}
	public void setSupliergstnumber(String supliergstnumber) {
		this.supliergstnumber = supliergstnumber;
	}
	public String getSupliertaxnumber() {
		return supliertaxnumber;
	}
	public void setSupliertaxnumber(String supliertaxnumber) {
		this.supliertaxnumber = supliertaxnumber;
	}
	public String getSuplieremailid() {
		return suplieremailid;
	}
	public void setSuplieremailid(String suplieremailid) {
		this.suplieremailid = suplieremailid;
	}
	public String getSupliereletter() {
		return supliereletter;
	}
	public void setSupliereletter(String supliereletter) {
		this.supliereletter = supliereletter;
	}
	public Byte getISACTIVE() {
		return ISACTIVE;
	}
	public void setISACTIVE(Byte iSACTIVE) {
		ISACTIVE = iSACTIVE;
	}
	
}
