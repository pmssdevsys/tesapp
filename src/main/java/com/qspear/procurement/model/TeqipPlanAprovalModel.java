package com.qspear.procurement.model;

import java.util.Date;
import java.util.List;

public class TeqipPlanAprovalModel {

    private Integer id;
    private String comment;
    private String createdBy;
    private Date createdOn;
    private Boolean lastRejection;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getLastRejection() {
        return lastRejection;
    }

    public void setLastRejection(Boolean lastRejection) {
        this.lastRejection = lastRejection;
    }

}
