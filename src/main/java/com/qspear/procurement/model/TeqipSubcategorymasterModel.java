package com.qspear.procurement.model;

import com.qspear.procurement.persistence.TeqipCategorymaster;

public class TeqipSubcategorymasterModel {

	private Integer subcatId;
	private Integer teqipCategorymasterId;
	public Integer getTeqipCategorymasterId() {
		return teqipCategorymasterId;
	}

	public void setTeqipCategorymasterId(Integer teqipCategorymasterId) {
		this.teqipCategorymasterId = teqipCategorymasterId;
	}

	private String subCategoryName;
	private TeqipCategorymaster categorymaster;
	private TeqipCategorymasterModel catmastermodel;

	public TeqipCategorymasterModel getCatmastermodel() {
		return catmastermodel;
	}

	public void setCatmastermodel(TeqipCategorymasterModel catmastermodel) {
		this.catmastermodel = catmastermodel;
	}

	public TeqipCategorymaster getCategorymaster() {
		return categorymaster;
	}

	public void setCategorymaster(TeqipCategorymaster categorymaster) {
		this.categorymaster = categorymaster;
	}

	public Integer getSubcatId() {
		return subcatId;
	}

	public void setSubcatId(Integer subcatId) {
		this.subcatId = subcatId;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
}
