/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model;

import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author jaspreet
 */
public class InstituteWiseApproval {

    Integer instituteId;
    String InstituteName;
    BigInteger totalpackage;
    String subComponent;
    String procurementStage;
    Date lastDateOFSubmission;
    Integer revisionId;
    Double totalCivilWork;
    Double totalGoods;
    Double totalService;
    Double totalEstimate;
    Integer planId;
    String roleType;

    public InstituteWiseApproval(Integer instituteId,
            String InstituteName,
            String subComponent,
            String procurementStage,
            Date lastDateOFSubmission,
            Integer revisionId,
            BigInteger totalpackage,
            Double totalCivilWork,
            Double totalGoods,
            Double totalService,
            Double totalEstimate,
            Integer planId,
            String roleType) {
        this.instituteId = instituteId;
        this.InstituteName = InstituteName;
        this.totalpackage = totalpackage;
        this.subComponent = subComponent;
        this.procurementStage = procurementStage;
        this.lastDateOFSubmission = lastDateOFSubmission;
        this.revisionId = revisionId;
        this.totalCivilWork = totalCivilWork;
        this.totalGoods = totalGoods;
        this.totalService = totalService;
        this.totalEstimate = totalEstimate;
        this.planId = planId;
        this.roleType = roleType;
    }

    public Integer getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Integer instituteId) {
        this.instituteId = instituteId;
    }

    public String getInstituteName() {
        return InstituteName;
    }

    public void setInstituteName(String InstituteName) {
        this.InstituteName = InstituteName;
    }

    public String getSubComponent() {
        return subComponent;
    }

    public void setSubComponent(String subComponent) {
        this.subComponent = subComponent;
    }

    public String getProcurementStage() {
        return procurementStage;
    }

    public void setProcurementStage(String procurementStage) {
        this.procurementStage = procurementStage;
    }

    public Date getLastDateOFSubmission() {
        return lastDateOFSubmission;
    }

    public void setLastDateOFSubmission(Date lastDateOFSubmission) {
        this.lastDateOFSubmission = lastDateOFSubmission;
    }

    public Integer getRevisionId() {
        return revisionId;
    }

    public void setRevisionId(Integer revisionId) {
        this.revisionId = revisionId;
    }

    public Double getTotalCivilWork() {
        return totalCivilWork;
    }

    public void setTotalCivilWork(Double totalCivilWork) {
        this.totalCivilWork = totalCivilWork;
    }

    public Double getTotalGoods() {
        return totalGoods;
    }

    public void setTotalGoods(Double totalGoods) {
        this.totalGoods = totalGoods;
    }

    public Double getTotalService() {
        return totalService;
    }

    public void setTotalService(Double totalService) {
        this.totalService = totalService;
    }

    public BigInteger getTotalpackage() {
        return totalpackage;
    }

    public void setTotalpackage(BigInteger totalpackage) {
        this.totalpackage = totalpackage;
    }

    public Double getTotalEstimate() {
        return totalEstimate;
    }

    public void setTotalEstimate(Double totalEstimate) {
        this.totalEstimate = totalEstimate;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

}
