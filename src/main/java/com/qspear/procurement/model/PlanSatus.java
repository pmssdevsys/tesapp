/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.model;

import java.util.List;

/**
 *
 * @author jaspreet
 */
public class PlanSatus {

    PlanInfo planInfo;
    List<StatusSummary> statusSummary;

    public PlanInfo getPlanInfo() {
        return planInfo;
    }

    public void setPlanInfo(PlanInfo planInfo) {
        this.planInfo = planInfo;
    }

    public List<StatusSummary> getStatusSummary() {
        return statusSummary;
    }

    public void setStatusSummary(List<StatusSummary> statusSummary) {
        this.statusSummary = statusSummary;
    }

}
