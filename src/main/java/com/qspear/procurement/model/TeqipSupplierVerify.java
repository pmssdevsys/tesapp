package com.qspear.procurement.model;

import java.util.ArrayList;
import java.util.List;

public class TeqipSupplierVerify {

    private List<String> message = new ArrayList();
    private boolean status;

    public TeqipSupplierVerify(List<String> message, boolean status) {
        this.message = message;
        this.status = status;
    }

    public TeqipSupplierVerify(boolean status) {
        this.status = status;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

}
