package com.qspear.procurement.model;

import java.math.BigInteger;
import java.util.Date;

public class UserPojo {
	private Date dob;
	private String email;
	private String gender;
	private Boolean isactive;
	private Date joiningDate;
	private String lname;
	private String name;
	private String password;
	private String spouseName;
	private String userMobile;
	private Integer roleid;
	private Integer stateId;
	private String institutecode;
	private Integer institutionid;

	public Integer getStateId() {
		return stateId;
	}



	public void setStateid(Integer stateId) {
		this.stateId = stateId;
	}



	public Integer getInstitutionid() {
		return institutionid;
	}



	public void setInstitutionid(Integer institutionid) {
		this.institutionid = institutionid;
	}

	public String getInstitutecode() {
		return institutecode;
	}

	public void setInstitute_code(String institutecode) {
		this.institutecode = institutecode;
	}
		public Integer getRoleid() {
		return roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}
	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Date getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	


}
