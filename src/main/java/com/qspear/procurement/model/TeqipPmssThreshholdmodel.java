package com.qspear.procurement.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;

public class TeqipPmssThreshholdmodel {

	@Id
	private Integer thresholdid;

	private Boolean isactive;

	private Integer isproprietary;

	private String review;

	private Double thresholdMaxValue;
	private Integer catId;
	private Integer procurementmethodId;

	public Integer getProcurementmethodId() {
		return procurementmethodId;
	}

	public void setProcurementmethodId(Integer procurementmethodId) {
		this.procurementmethodId = procurementmethodId;
	}

	public Integer getCatId() {
		return catId;
	}

	public void setCatId(Integer catId) {
		this.catId = catId;
	}

	private Double thresholdMinValue;

	private TeqipCategorymasterModel teqipCategorymaster;

	private TeqipProcurementmethodModel teqipProcurementmethod;

	public Integer getThresholdid() {
		return thresholdid;
	}

	public void setThresholdid(Integer thresholdid) {
		this.thresholdid = thresholdid;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Integer getIsproprietary() {
		return isproprietary;
	}

	public void setIsproprietary(Integer isproprietary) {
		this.isproprietary = isproprietary;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public Double getThresholdMaxValue() {
		return thresholdMaxValue;
	}

	public void setThresholdMaxValue(Double thresholdMaxValue) {
		this.thresholdMaxValue = thresholdMaxValue;
	}

	public Double getThresholdMinValue() {
		return thresholdMinValue;
	}

	public void setThresholdMinValue(Double thresholdMinValue) {
		this.thresholdMinValue = thresholdMinValue;
	}

	public TeqipCategorymasterModel getTeqipCategorymaster() {
		return teqipCategorymaster;
	}

	public void setTeqipCategorymaster(TeqipCategorymasterModel teqipCategorymaster) {
		this.teqipCategorymaster = teqipCategorymaster;
	}

	public TeqipProcurementmethodModel getTeqipProcurementmethod() {
		return teqipProcurementmethod;
	}

	public void setTeqipProcurementmethod(TeqipProcurementmethodModel teqipProcurementmethod) {
		this.teqipProcurementmethod = teqipProcurementmethod;
	}

}
