package com.qspear.procurement.model;

import com.qspear.procurement.persistence.TeqipCitymaster;

public class CityMaster {

    private Integer cityId;
    private String cityName;
    private Integer pincode;

    public CityMaster() {
    }

    public CityMaster(TeqipCitymaster citymaster) {
        this.cityId = citymaster.getCityId();
        this.cityName = citymaster.getCityName();
        this.pincode = citymaster.getPincode();
    }

    
    
    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

}
