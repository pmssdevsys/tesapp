package com.qspear.procurement.model;

import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;


public class Base64Encoder {

	private static String constant="aA@12#a" ;
	
	public static String encodePassword(String password) {

		String str=constant.concat(password);
		Encoder encoder = Base64.getEncoder();
		String encodedString = encoder.encodeToString(str.getBytes());
		String againEncoder = encoder.encodeToString(encodedString.getBytes());
		return againEncoder;
	}

	public static String decodePassword(String password) {

	//	String password="WVVGQU1USWpZVmRXVmtkUlZURlZVMWR3V2xaVlJqUlVWM0JPVFVFOVBRPT0=";
		
		Decoder decoder = Base64.getDecoder();
		byte[] decodedByte = decoder.decode(password);
		byte[] againdeCoder = decoder.decode(new String(decodedByte));
		String decodedString = new String(againdeCoder);
		String pwd=decodedString.substring(constant.length());
		System.out.println("pppp : " + pwd);
		return pwd;

}
}