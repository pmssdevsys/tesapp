/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.util;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;


/*
 * @author jaspreet
 */
public class WordReplaceText {

    private HWPFDocument replaceWord(File file,
            Map<String, String> words,
            Map<String, String[][]> table) throws IOException {

        HWPFDocument doc = new HWPFDocument(new POIFSFileSystem(file));

        Range r = doc.getRange();
        for (int i = 0; i < r.numSections(); ++i) {
            Section s = r.getSection(i);
            for (int j = 0; j < s.numParagraphs(); j++) {
                Paragraph p = s.getParagraph(j);
                for (int k = 0; k < p.numCharacterRuns(); k++) {
                    CharacterRun run = p.getCharacterRun(k);
                    String text = run.text();

                    Set<String> keySet = words.keySet();
                    for (String string : keySet) {
                        if (text.contains(string)) {
                            run.replaceText(string, words.get(string));
                        }

                    }

                    Set<String> keySet1 = table.keySet();
                    for (String string : keySet1) {
                        if (text.contains(string)) {
                            run.replaceText(string, "");

                            replaceTable(run, table.get(string));
                        }

                    }

                }
            }
        }

        return doc;
    }

    private void saveDocument(HWPFDocument doc, String file) throws FileNotFoundException, IOException {
        FileOutputStream out = new FileOutputStream(file);
        doc.write(out);

    }

    private void replaceTable(CharacterRun run, String[][] tableData) {

        int rows = tableData.length;

        int columns = tableData[0].length;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                tableData[i][j]=  tableData[i][j] == null ?"":tableData[i][j];
            }
        }

        Table table = run.insertTableBefore((short) columns, rows);
        for (int rowIdx = 0; rowIdx < rows; rowIdx++) {
            TableRow row = table.getRow(rowIdx);
            for (int colIdx = 0; colIdx < columns; colIdx++) {
                TableCell cell = row.getCell(colIdx);

                try {
                    Paragraph par = cell.getParagraph(0);

                    CharacterRun insertBefore = par.insertBefore(tableData[rowIdx][colIdx]);
                    if (rowIdx == 0) {
                        insertBefore.setBold(true);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

    }

    public void generateDoc(String  file, Map<String, String> words, Map<String, String[][]> table, String outputFile) throws IOException {
        HWPFDocument replaceWord = this.replaceWord(new File(file+".doc"), words, table);
         this.saveDocument(replaceWord, outputFile+".doc");

    }

}
