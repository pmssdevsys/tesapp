/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.util;

import com.qspear.procurement.constants.MethodConstants;
import java.io.File;
import java.io.FileOutputStream;

/**
 *
 * @author jaspreet
 */
public class MethodUtility {

    public static String saveDocument(Integer packageId, String type, String fileName, byte[] file) {

        String filePathAndName = "";
        try {

            String directory = getDirectoryPath();
            File origFile = new File(directory + type);
            if (!origFile.exists()) {
                origFile.mkdir();
            }

            filePathAndName = directory + type+ File.separator+ fileName;
            FileOutputStream out = new FileOutputStream(filePathAndName);
            out.write(file);
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePathAndName;

    }

    public static String getDirectoryPath() {

        File rootFile = new File(MethodConstants.UPLOAD_LOCATION);
        String directory = rootFile.getAbsolutePath();

        return directory  + File.separator;
    }

}
