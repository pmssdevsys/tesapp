package com.qspear.procurement.util.rest;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.ErrorModel;
import com.qspear.procurement.exception.PMSSBudgetException;
import com.qspear.procurement.exception.PMSSException;

@RestControllerAdvice(basePackages = "com.qspear.procurement.rest")
public class ExceptionTranslator {

	@ExceptionHandler(java.lang.ExceptionInInitializerError.class)
	public ResponseEntity<PMSSException> notFoundException(final ExceptionInInitializerError e) {

		return new ResponseEntity<PMSSException>(new PMSSException(new ErrorCode("PMSS_GLOBAL_EXCE", e.getMessage())),
				HttpStatus.OK);
	}

	@ExceptionHandler(PMSSException.class)
	@ResponseBody
	public ResponseEntity<PMSSException> throwPMSSException(PMSSException pmssException) {
		return new ResponseEntity<PMSSException>(pmssException, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(PMSSBudgetException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ErrorModel processRestClientException(PMSSBudgetException exception) {
		ErrorModel errorModel = new ErrorModel();
		errorModel.setErrorCode(exception.getCode().getCode());
		errorModel.setErrorMessage(exception.getMessage());
		return errorModel;
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public final ResponseEntity<ErrorDetails> handleAllExceptions(Exception ex, WebRequest request) {
	  ErrorDetails errorDetails = new ErrorDetails( "Un-authrized request",
	      request.getDescription(false));
	  return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
