package com.qspear.procurement.util;


import java.io.File;
import java.util.Collection;

import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import com.qspear.procurement.exception.ErrorCode;

import eu.medsea.mimeutil.MimeUtil;

public class FileUtils {
	private static Long fileLength = 2L;
	private static final String IMAGE_PATTERN = "!#$%&*()+=|<>?{}\\[\\]~-";

	public static String checkFileMimeType(MultipartFile file) {
		ErrorCode eroor = new ErrorCode("Pmss101", "Invalid File");
//		
//		 MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
//	       
//	       .FileUtils. Collection<?> mimeTypes = MimeUtil.getMimeTypes();
//	        System.out.println(mimeTypes);
		//String formatExtension = FileFormatUtil.loadFormatToExtension(formatInfo.getLoadFormat());

		Long b = file.getSize() / (1024 * 1024);
		if (b  >= fileLength) {
			return "Not Valid";
		}
		File f = new File(file.getOriginalFilename());
		MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
		String mimeType = fileTypeMap.getContentType(f);
		System.out.println(file.getOriginalFilename() + " : " + mimeType);
		if (!mimeType.equalsIgnoreCase("image/jpeg") && !mimeType.equalsIgnoreCase("application/octet-stream"))
			return "Not Valid";

		String image = file.getOriginalFilename();
		String[] imageName = image.split("\\.\\.");
		if (imageName.length > 1)
			if (imageName[0] != null && imageName[1] != null) {
				return "Not Valid";
			}
		String fileNameWithOutExt = FilenameUtils.removeExtension(file.getOriginalFilename());
		String[] metaArr = fileNameWithOutExt.split("");
		for (int i = 0; i < fileNameWithOutExt.length() - 1; i++) {
			if (IMAGE_PATTERN.contains(metaArr[i])) {
				return "Not Valid";

			}

		}

		return "valid";

	}
	

	
	public static String checkDocumentFile(String file) {

	Boolean b1=file.contains("..");
	Integer ab=0;
	String[] metaArr = file.split("");
	for (int i = 0; i < file.length() - 1; i++) {
		if (IMAGE_PATTERN.contains(metaArr[i])) {
			return "Not Valid";

		}

	}
		if (  b1==true){
			ab=1;
				return "Not Valid";
			
		}
	
			else if( b1!=true) {
		String extension=	file.substring(file.lastIndexOf('.')+1 );
			if(!extension.equalsIgnoreCase("jpeg") && !extension.equalsIgnoreCase("jpg")&& !extension.equalsIgnoreCase("png")
					&& !extension.equalsIgnoreCase("docx") && !extension.equalsIgnoreCase("doc")&& !extension.equalsIgnoreCase("pdf") )
			{
				ab=1;
				return "Not Valid";
			}
			
           }
			
				return "valid";
			

	}
	
	
}
