package com.qspear.procurement.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.qspear.procurement.persistence.TeqipRolemaster;
import com.qspear.procurement.repositories.RoleMasterRepository;

public class UserCreater {
	public String createUser(Integer roleId,String institutionCode,String Lname, String stateCode,TeqipRolemaster roledesc ){
		Map<String,String> map=new  HashMap();
		map.put("admin", "admin"+Lname);
		map.put("Centralc Project Advisor", "cpa"+Lname.toLowerCase().trim());
		map.put("Procurement Coordinator", "pcr"+Lname.toLowerCase().trim());
		map.put("State Procurement Coordinator", "sprc"+stateCode);
		map.put("Institution Procurement Coordinator", "pc"+institutionCode);
		map.put("Associate Procurement Coordinator", "apc"+Lname.toLowerCase().trim());
		map.put("Institution Head/Director", "dir"+institutionCode);
		map.put("Institution TEQIP Coordinator", "teq"+institutionCode);
		map.put("State Project Advisor (SPA)", "spa"+stateCode);
		map.put("Administrator", "ar"+Lname.toLowerCase().trim());
		map.put("WB Team Task Leader", "wbt"+Lname.toLowerCase().trim());
		map.put("WB Sr. Procurement Specialist", "wbs"+Lname.toLowerCase().trim());
		map.put("WB Procurement Assistant", "wbp"+Lname.toLowerCase().trim());
		map.put("SS & NPD TEQIP", "ssn"+Lname.toLowerCase().trim());
		map.put("Director","dr"+Lname.toLowerCase().trim());
		map.put("Section Officer", "so"+Lname.toLowerCase().trim());
		map.put("Sr Procurement Coordinator", "spc"+Lname.toLowerCase().trim());
		map.put("State Project Coordinator", "stpc"+stateCode);
		map.put("MHRD", "mh"+Lname.toLowerCase().trim());
		map.put("Procurement Coordinator Head", "pch"+Lname.toLowerCase().trim());
		
		
		String value=map.get(roledesc.getRoleDescription());
		return value;
	}

}

