package com.qspear.procurement.util;

import java.lang.reflect.Field;
import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.util.ReflectionUtils;
import org.springframework.stereotype.Component;

import com.qspear.procurement.constants.ProcurementConstants;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSBudgetException;
import com.qspear.procurement.model.TeqipPackageModel;
import com.qspear.procurement.model.TeqipProcurementmasterModel;
import com.qspear.procurement.model.TeqipTimelineModel;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipInstitutionsubcomponentmapping;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPlanApprovalstatus;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.TeqipProcurementmethodTimeline;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;
import com.qspear.procurement.repositories.StateMasterRepository;
import com.qspear.procurement.repositories.TeqipInstitutionsubcomponentmappingRepository;
import com.qspear.procurement.security.util.LoggedUser;
import com.qspear.procurement.security.util.LoginUser;
import com.qspear.procurement.service.AbstractPackageService;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

@Component("ProcurementUtility")
public class ProcurementUtility extends AbstractPackageService {

    SimpleDateFormat formatters =  new SimpleDateFormat("dd/MM/yyyy");

    public TeqipTimelineModel getTeqipTimeline(TeqipProcurementmethodTimeline teqipPackageTimeline,
            Date sanctionDate) {

        TeqipTimelineModel timelineModel = new TeqipTimelineModel();

        if (teqipPackageTimeline != null) {
            for (Field declareField : teqipPackageTimeline.getClass().getDeclaredFields()) {
                try {
                    declareField.setAccessible(true);
                    Object dayValue = declareField.get(teqipPackageTimeline);
                    if (dayValue instanceof Double) {
                        Calendar c = Calendar.getInstance();
                        c.setTime(new Date()); // Now use today date.
                        c.add(Calendar.DATE, ((Double) (dayValue)).intValue()); // Adding 5 days
                        String format = formatters.format(c.getTime());
                        
//                        LocalDate plusDays = sanctionDate.plusDays(((Double) (dayValue)).longValue());
//                        String format = plusDays.format(formatters);
                        ReflectionUtils.setField(TeqipTimelineModel.class.getDeclaredField(declareField.getName()),
                                timelineModel, format);
                    }
                } catch (Exception e) {
                    // Field not found
                }
            }
        }
        return timelineModel;
    }

    public String getPackageCode() {

        LoginUser claim = LoggedUser.get();

        StringBuilder packageCode = new StringBuilder(ProcurementConstants.PACKAGE_CODE);
        packageCode.append(new SimpleDateFormat("yyyy").format(new Date()) + "/");

        if (ProcurementConstants.isInstitutionalRole(claim.getPmssrole())) {
            // For Institute
            packageCode.append(claim.getStatecode() + "/"
                    + claim.getInstcode() + "/");
        } else if (ProcurementConstants.isStateRole(claim.getPmssrole())) {
            // For State
            packageCode.append(claim.getStatecode() + "/");
        } else if (ProcurementConstants.isNationalRole(claim.getPmssrole())) {
            // For NPIU
            packageCode.append(ProcurementConstants.USER_ROLE_NPIU + "/");
        }
        else if (ProcurementConstants.isEdcilRole(claim.getPmssrole())) {
            // For NPIU
            packageCode.append(ProcurementConstants.USER_ROLE_EDCIL + "/");
        }

        return packageCode.toString();
    }

    public void setSubcomponentDetails(TeqipProcurementmasterModel teqipProcurementmasterModel,
            TeqipInstitutionsubcomponentmappingRepository teqipInstitutionsubcomponentmappingRepository,
            HttpServletRequest httpServletRequest) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute(ProcurementConstants.USER_AUTH_REQ_OBJECT);

        TeqipInstitutionsubcomponentmapping teqipInstitutionsubcomponentmapping = null;

        if (ProcurementConstants.isInstitutionalRole(claim.getPmssrole())) {// For Institute
            // For Institute get plan
            Integer instituteId = claim.getInstid();

            TeqipInstitution teqipInstitution = teqipInstitutionRepository.findOne(instituteId);
            int typeId= 0;
            if(teqipInstitution.getTeqipInstitutiontype() != null){
                typeId= teqipInstitution.getTeqipInstitutiontype().getInstitutiontypeId();
            }

            teqipInstitutionsubcomponentmapping = teqipInstitutionsubcomponentmappingRepository
                    .findFirstByTeqipInstitutionAndTeqipStatemasterAndType(teqipInstitution, null,0);

            TeqipPlanApprovalstatus approvalstatus = teqipPlanApprovalStatusRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(teqipInstitution, null,typeId);
            if (null != approvalstatus && approvalstatus.getTeqipPmssProcurementstage() != null) {
                teqipProcurementmasterModel.setCurrentStage(approvalstatus.getTeqipPmssProcurementstage().getProcurementstages());
                teqipProcurementmasterModel.setStageId(approvalstatus.getTeqipPmssProcurementstage().getProcurementstageid());
            } else {
                teqipProcurementmasterModel.setCurrentStage("Under Preparation");
                teqipProcurementmasterModel.setStageId(1);
            }

        } else if (ProcurementConstants.isStateRole(claim.getPmssrole())) {// For State
            // For Institute get plan
            Integer stateId = claim.getStateid();

            TeqipStatemaster teqipStatemaster = teqipStatemasteryRepository.findOne(stateId);
            teqipStatemaster.setStateId(stateId);

            teqipInstitutionsubcomponentmapping = teqipInstitutionsubcomponentmappingRepository
                    .findFirstByTeqipInstitutionAndTeqipStatemasterAndType(null,teqipStatemaster,0);

            TeqipPlanApprovalstatus approvalstatus = teqipPlanApprovalStatusRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(null, teqipStatemaster,0);
            if (null != approvalstatus && approvalstatus.getTeqipPmssProcurementstage() != null) {
                teqipProcurementmasterModel
                        .setCurrentStage(approvalstatus.getTeqipPmssProcurementstage().getProcurementstages());
                teqipProcurementmasterModel.setStageId(approvalstatus.getTeqipPmssProcurementstage().getProcurementstageid());
            } else {
                teqipProcurementmasterModel.setCurrentStage("Under Preparation");
                teqipProcurementmasterModel.setStageId(1);
            }
        } else if (ProcurementConstants.isNationalRole(claim.getPmssrole())) {

            teqipInstitutionsubcomponentmapping = teqipInstitutionsubcomponentmappingRepository
                    .findFirstByTeqipInstitutionAndTeqipStatemasterAndType(null,null,0);

            
            TeqipPlanApprovalstatus approvalstatus = teqipPlanApprovalStatusRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(null, null,0);
            if (null != approvalstatus && approvalstatus.getTeqipPmssProcurementstage() != null) {
                teqipProcurementmasterModel
                        .setCurrentStage(approvalstatus.getTeqipPmssProcurementstage().getProcurementstages());
                teqipProcurementmasterModel.setStageId(approvalstatus.getTeqipPmssProcurementstage().getProcurementstageid());
            } else {
                teqipProcurementmasterModel.setCurrentStage("Under Preparation");
                teqipProcurementmasterModel.setStageId(1);
            }

        } else if (ProcurementConstants.isEdcilRole(claim.getPmssrole())) {

            teqipInstitutionsubcomponentmapping = teqipInstitutionsubcomponentmappingRepository
                    .findFirstByTeqipInstitutionAndTeqipStatemasterAndType(null,null,1);

            
            TeqipPlanApprovalstatus approvalstatus = teqipPlanApprovalStatusRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(null, null,1);
            if (null != approvalstatus && approvalstatus.getTeqipPmssProcurementstage() != null) {
                teqipProcurementmasterModel
                        .setCurrentStage(approvalstatus.getTeqipPmssProcurementstage().getProcurementstages());
                teqipProcurementmasterModel.setStageId(approvalstatus.getTeqipPmssProcurementstage().getProcurementstageid());
            } else {
                teqipProcurementmasterModel.setCurrentStage("Under Preparation");
                teqipProcurementmasterModel.setStageId(1);
            }
        }

        if (teqipInstitutionsubcomponentmapping != null) {
            TeqipSubcomponentmaster teqipSubcomponentmaster = teqipInstitutionsubcomponentmapping
                    .getTeqipSubcomponentmaster();

            if (teqipSubcomponentmaster != null) {
                teqipProcurementmasterModel.setSubComponentCode(teqipSubcomponentmaster.getSubcomponentcode());
                teqipProcurementmasterModel.setSubComponentName(teqipSubcomponentmaster.getSubComponentName());
                teqipProcurementmasterModel.setSubComponentId(teqipSubcomponentmaster.getId());
            }
        }
    }

    public TeqipStatemaster getTeqipStatemasterDetailsFromSession(HttpServletRequest httpServletRequest) {
        LoginUser claim = (LoginUser) httpServletRequest.getAttribute(ProcurementConstants.USER_AUTH_REQ_OBJECT);
        TeqipStatemaster teqipStatemaster = null;

        Integer stateId = claim.getStateid();

        if (null != stateId) {
            teqipStatemaster = teqipStatemasteryRepository.findOne(stateId);
        }

        return teqipStatemaster;
    }

    public Double getAvailableBudget(Integer packageId, TeqipCategorymaster category, Double totalBudget,
            TeqipInstitution teqipInstitution, TeqipStatemaster statemaster, Integer type) {

        Double alreadyUsedByWork
                = teqipInstitution != null
                        ? teqipPackageRepository.findSumOfEstimatedCostByCategoryIdAndTeqipInstitutionAndPackageIdNot(category, teqipInstitution, packageId)
                        : statemaster != null
                                ? teqipPackageRepository.findSumOfEstimatedCostByCategoryIdAndTeqipStatemasterAndPackageIdNot(category, statemaster, packageId)
                                : teqipPackageRepository.findSumOfEstimatedCostByCategoryIdAndNPIUAndPackageIdNot(category, packageId,type);
        Double consumeFormWork = null;

        if (category.getCatId() == 1) {
            // Find amount counsumed by Goods Category
            TeqipCategorymaster goodsCategory = new TeqipCategorymaster();
            goodsCategory.setCatId(2);

            consumeFormWork = teqipInstitution != null
                    ? teqipPackageRepository.findSumOfConsumeFromWorksByCategoryIdAndTeqipInstitution(goodsCategory, teqipInstitution)
                    : statemaster != null
                            ? teqipPackageRepository.findSumOfConsumeFromWorksByCategoryIdAndTeqipStatemaster(goodsCategory, statemaster)
                            : teqipPackageRepository.findSumOfConsumeFromWorksByCategoryIdAndNPIU(goodsCategory ,type);
        }

        Double totalConsumeByWork = (consumeFormWork != null ? consumeFormWork : 0)
                + (alreadyUsedByWork != null ? alreadyUsedByWork : 0);

        return totalBudget - totalConsumeByWork;

    }

    private void validateBudget(TeqipPackageModel packageModel,
            TeqipInstitution teqipInstitution, TeqipStatemaster statemaster, Integer type) {

       
        TeqipCategorymaster category = new TeqipCategorymaster();
        category.setCatId(packageModel.getTeqipCategorymasterId());

        Integer packageId = packageModel.getPackageId() != null
                ? packageModel.getPackageId()
                : 0;

        Double estimatedCost = packageModel.getEstimatedCost();

        TeqipInstitutionsubcomponentmapping teqipInstitutionsubcomponentmapping = teqipInstitutionsubcomponentmappingRepository
                .findFirstByTeqipInstitutionAndTeqipStatemasterAndType(teqipInstitution, statemaster, type);

        Double totalBudget = category.getCatId() == 1 ? teqipInstitutionsubcomponentmapping.getBudgetforcw()
                : category.getCatId() == 2 ? teqipInstitutionsubcomponentmapping.getBudgetforprocuremen()
                : category.getCatId() == 3 ? teqipInstitutionsubcomponentmapping.getBudgetforservices() : 0;

        Double availableBudget = getAvailableBudget(packageId, category, totalBudget, teqipInstitution, statemaster,type); // -95500 
        availableBudget = availableBudget - estimatedCost; // -95500 -20000 = -115500

        if (category.getCatId() == 2 && availableBudget < 0f) {

            Double budgetNeededfromWork = availableBudget < 0 ? Math.abs(estimatedCost) : Math.abs(availableBudget); // 20000

            //Find available budget in work category
            TeqipCategorymaster workCategory = new TeqipCategorymaster();
            workCategory.setCatId(1);

            Double availableWorkBudget = getAvailableBudget(packageId, workCategory, teqipInstitutionsubcomponentmapping.getBudgetforcw(),
                    teqipInstitution, statemaster,type);

            if ((availableWorkBudget - budgetNeededfromWork) >= 0) {
                if (packageModel.isUseOtherCategoryBuget()) {

                    packageModel.setConsumeFromWorks(budgetNeededfromWork);
                    return;
                } else {

                    throw new PMSSBudgetException("You can use budget from work " + availableWorkBudget,
                            new ErrorCode("PMSS-USE-WORK-BUDGET", "Budget issue"));
                }
            }

        }

        if (availableBudget < 0f) {
            throw new PMSSBudgetException("Max available budget is " + (availableBudget + estimatedCost),
                    new ErrorCode("PMSS-BUDGET-EXCEED", "Budget issue"));
        }
    }

    public void checkBudgetBeforeCreatePlan(HttpServletRequest httpServletRequest,
            TeqipPackageModel packageModel) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute(ProcurementConstants.USER_AUTH_REQ_OBJECT);

        if (ProcurementConstants.isInstitutionalRole(claim.getPmssrole())) {
            // For Institute
            TeqipInstitution teqipInstitution = getTeqipInstitutionFromSession(httpServletRequest);
            int type =0;
            if(teqipInstitution.getTeqipInstitutiontype() != null){
                type = teqipInstitution.getTeqipInstitutiontype().getInstitutiontypeId();
            }

            this.validateBudget(packageModel, teqipInstitution, null,type);

        } else if (ProcurementConstants.isStateRole(claim.getPmssrole())) {// For State

            TeqipStatemaster teqipStatemaster = new TeqipStatemaster();
            teqipStatemaster.setStateId(claim.getStateid());

            this.validateBudget(packageModel, null, teqipStatemaster,0);

        } else if (ProcurementConstants.isNationalRole(claim.getPmssrole())) {// For NPIU

            this.validateBudget(packageModel, null, null,0);
            
         } else if (ProcurementConstants.isEdcilRole(claim.getPmssrole())) {// For EDCIL

            this.validateBudget(packageModel, null, null,1);
        }
    }

    public TeqipInstitution getTeqipInstitutionFromSession(HttpServletRequest httpServletRequest) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute(ProcurementConstants.USER_AUTH_REQ_OBJECT);

        Integer instId = claim.getInstid();

        if (null != instId) {
            return teqipInstitutionRepository.findOne(instId);
        }

        return null;
    }

    public Integer getReviseId(TeqipPackage teqipPackage) {
        TeqipPlanApprovalstatus approvalstatus = teqipPlanApprovalStatusRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(
                teqipPackage.getTeqipInstitution(), teqipPackage.getTeqipStatemaster(), teqipPackage.getTypeofplanCreator());

        return approvalstatus != null ? approvalstatus.getReviseid() : 1;

    }

 

}
