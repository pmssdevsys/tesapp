/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.util;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTblWidth;


/*
 * @author jaspreet
 */
public class DocXReplaceText {

    Map<String, String> wordsMap;
    Map<String, String[][]> tableMap;
    Map<String, String[]> listMap;
    Map<String, File> fileMap;

    public DocXReplaceText(Map<String, String> words,
            Map<String, String[][]> table,
            Map<String, String[]> listMap,
            Map<String, File> fileMap) {
        this.wordsMap = words;
        this.tableMap = table;
        this.listMap = listMap;
        this.fileMap = fileMap;

    }

    private XWPFDocument replaceWord(File file) throws IOException, InvalidFormatException {

        XWPFDocument doc = new XWPFDocument(OPCPackage.open(file));

        this.replaceText(doc);

        return doc;

    }

    class TempObject {

        XWPFParagraph fParagraph;
        String[] value;

        public TempObject(XWPFParagraph fParagraph, String[] value) {
            this.fParagraph = fParagraph;
            this.value = value;
        }

        public XWPFParagraph getfParagraph() {
            return fParagraph;
        }

        public void setfParagraph(XWPFParagraph fParagraph) {
            this.fParagraph = fParagraph;
        }

        public String[] getValue() {
            return value;
        }

        public void setValue(String[] value) {
            this.value = value;
        }

    }

    private void replaceText(XWPFDocument doc,
            List<XWPFParagraph> paragraphs) {
        List<TempObject> toInserList = new ArrayList<>();

        for (XWPFParagraph paragraph : paragraphs) {
            List<XWPFRun> runs = paragraph.getRuns();

            if (runs != null) {
                for (XWPFRun r : runs) {

                    String text = r.text();

                    System.out.println(">>>>" + text);

                    if (wordsMap != null) {
                        for (String find : wordsMap.keySet()) {
                            if (text != null && text.contains(find)) {
                                String wordMapFind = wordsMap.get(find);
                                if (wordMapFind == null || "null".equals(wordMapFind) || "nu".equals(wordMapFind)) {
                                    wordMapFind = "";
                                }
                                text = text.replace(find, wordMapFind);

                            }
                        }
                    }

                    if (listMap != null) {
                        for (String find : listMap.keySet()) {
                            if (text != null && text.contains(find)) {
                                toInserList.add(new TempObject(paragraph, listMap.get(find)));

                                text = text.replace(find, "");
                            }
                        }
                    }

                    if (tableMap != null) {
                        for (String find : tableMap.keySet()) {
                            if (text != null && text.contains(find)) {
                                if(tableMap.get(find)!=null){
                                XmlCursor cursor = paragraph.getCTP().newCursor();
                                XWPFTable table = doc.insertNewTbl(cursor);
                                fillTable(table, tableMap.get(find),find);
                                }
                                text = text.replace(find, "");
                                
                            }
                        }
                    }
                    if (fileMap != null) {
                        for (String find : fileMap.keySet()) {
                            if (text != null && text.contains(find)) {

                                try {

                                    r.addPicture(new FileInputStream(fileMap.get(find)),
                                            Document.PICTURE_TYPE_JPEG, "institute",
                                            Units.toEMU(50), Units.toEMU(50));

                                } catch (Exception ex) {
                                    Logger.getLogger(DocXReplaceText.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                text = text.replace(find, "");
                            }
                        }
                    }

                    r.setText(text, 0);

                }
            }

        }

        for (TempObject tempObject : toInserList) {
            XWPFRun createRun = tempObject.getfParagraph().createRun();

            for (String string : tempObject.getValue()) {
                createRun.setBold(true);
                createRun.setFontSize(10);
                createRun.setFontFamily("arial");
                createRun.setText(string == null || "null".equals(string) ? "" : string);
                createRun.addBreak();
            }

        }
    }

    static Dimension getImageDimension(File imgFile) throws IOException {
        BufferedImage img = ImageIO.read(imgFile);
        return new Dimension(img.getWidth(), img.getHeight());
    }

    private void replaceText(XWPFDocument doc) {

        for (XWPFTable tbl : doc.getTables()) {
            for (XWPFTableRow row : tbl.getRows()) {
                for (XWPFTableCell cell : row.getTableCells()) {
                    this.replaceText(doc, cell.getParagraphs());
                }
            }
        }

        this.replaceText(doc, doc.getParagraphs());

    }

    private void saveDocument(XWPFDocument doc, String file) throws FileNotFoundException, IOException {
        FileOutputStream out = new FileOutputStream(file);
        doc.write(out);

    }

    public void generateDoc(String file, String outputFile) throws IOException, InvalidFormatException {
        XWPFDocument replaceWord = this.replaceWord(new File(file + ".docx"));
        this.saveDocument(replaceWord, outputFile + ".docx");
    }

    private void fillTable(XWPFTable table, String[][] tableData, String key) {

        System.out.println(tableData.toString());
        CTTblWidth width = table.getCTTbl().addNewTblPr().addNewTblW();
        width.setType(STTblWidth.DXA);
        width.setW(BigInteger.valueOf(9072));

        int rows = tableData.length;
        int columns = tableData[0].length;

        for (int i = 0; i < rows; i++) {

            XWPFTableRow row = table.getRow(i) != null ? table.getRow(i) : table.createRow();

            for (int j = 0; j < columns; j++) {
                XWPFTableCell cell = row.getCell(j) != null ? row.getCell(j) : row.createCell();
                XWPFRun run = cell.addParagraph().createRun();
                if (i == 0 && !"$$PAYMENT_DETAILS_TABLE$$".equalsIgnoreCase(key)) {
                    run.setBold(true);
                    
                }
                run.setFontSize(10);
                run.setFontFamily("arial");
                run.setText(tableData[i][j] == null || "null".equals(tableData[i][j]) || "nu".equals(tableData[i][j])
                        ? "" : tableData[i][j]);
            }
        }

    }

}
