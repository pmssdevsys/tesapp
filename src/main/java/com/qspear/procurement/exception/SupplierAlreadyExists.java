/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 *
 * @author jaspreet
// */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class SupplierAlreadyExists  extends RuntimeException{

    public SupplierAlreadyExists(String message) {
        super(message);
    }
    
    
}
