package com.qspear.procurement.exception;


	public class JsonResponse {
		int status;
		String message;
		Object data;
		public int getStatus() {
			return status;
		}
		public JsonResponse(int status, String message, Object data) {
			super();
			this.status = status;
			this.message = message;
			this.data = data;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public Object getData() {
			return data;
		}
		public void setData(Object data) {
			this.data = data;
		}
	}


