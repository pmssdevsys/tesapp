package com.qspear.procurement.exception;

public class PMSSException extends RuntimeException {
	
	private static final long serialVersionUID = 7718828512143293558L;
	private final ErrorCode code;

	public PMSSException(ErrorCode code) {
		super();
		this.code = code;
	}

	public PMSSException(String message, Throwable cause, ErrorCode code) {
		super(message, cause);
		this.code = code;
	}

	public PMSSException(String message, ErrorCode code) {
		super(message);
		this.code = code;
	}

	public PMSSException(Throwable cause, ErrorCode code) {
		super(cause);
		this.code = code;
	}

	public ErrorCode getCode() {
		return this.code;
	}
}