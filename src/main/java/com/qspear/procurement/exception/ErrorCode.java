package com.qspear.procurement.exception;

public class ErrorCode {
	
	private String code;
	private String Message;
	
	
	public ErrorCode(String code, String message) {
		super();
		this.code = code;
		Message = message;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}

}
