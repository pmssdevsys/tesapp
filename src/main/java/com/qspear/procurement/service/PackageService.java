package com.qspear.procurement.service;

import com.qspear.procurement.constants.ProcurementConstants;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSBudgetException;
import com.qspear.procurement.model.PackageHistoryList;
import com.qspear.procurement.model.PrecurementMethodTimeLinePojo;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.qspear.procurement.model.TeqipPackageModel;
import com.qspear.procurement.model.TeqipPlanAprovalModel;
import com.qspear.procurement.model.TeqipTimelineModel;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPackageHistory;
import com.qspear.procurement.persistence.TeqipPlanApprovalstatus;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.TeqipProcurementmaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipProcurementmethodTimeline;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.persistence.User;
import com.qspear.procurement.persistence.methods.TeqipPackageConsultantEvaluation;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDistribution;
import com.qspear.procurement.persistence.methods.TeqipPackagePaymentDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQuestionMaster;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.persistence.methods.TeqipPackageTechEvalCommitee;
import com.qspear.procurement.persistence.methods.TeqipPackageTechSubCriteria;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.ConsultantEvaluationRepository;
import com.qspear.procurement.repositories.method.TechCriteriaRepository;
import com.qspear.procurement.repositories.method.TechEvalCommiteeRepository;
import com.qspear.procurement.repositories.method.TechSubCriteriaRepository;
import com.qspear.procurement.security.util.LoginUser;
import com.qspear.procurement.service.mapper.InvitationLetterMapper;
import com.qspear.procurement.service.mapper.ItemGoodsMapper;
import com.qspear.procurement.service.mapper.ItemMasterMapper;
import com.qspear.procurement.service.mapper.ItemWorkMapper;
import com.qspear.procurement.service.mapper.ProcurementMapper;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import com.qspear.procurement.repositories.method.PackageGRNDetailRepository;
import com.qspear.procurement.repositories.method.PackageGRNItemDetailRepository;
import com.qspear.procurement.repositories.method.PackageGRNItemDistributionRepository;
import com.qspear.procurement.repositories.method.WorkOrderCertificateRepository;
import com.qspear.procurement.security.util.LoggedUser;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PackageService extends AbstractPackageService {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    ItemWorkMapper itemWorkMapper;

    @Autowired
    ProcurementMapper procurementMapper;

    @Autowired
    PMSSReferenceService referenceService;

    @Autowired
    InvitationLetterMapper invitationLetterMapper;

    @Autowired
    ItemGoodsMapper itemGoodsMapper;

    @Autowired
    ItemMasterMapper itemMasterMapper;

    @Autowired
    TechEvalCommiteeRepository techEvalCommiteeRepository;

    @Autowired
    TechCriteriaRepository techCriteriaRepository;

    @Autowired
    TechSubCriteriaRepository techSubCriteriaRepository;

    @Autowired
    ConsultantEvaluationRepository consultantEvaluationRepository;

    @Autowired
    WorkOrderCertificateRepository workOrderCertificateRepository;

    @Autowired
    PackageGRNDetailRepository packageGRNDetailRepository;

    @Autowired
    PackageGRNItemDetailRepository packageGRNItemDetailRepository;

    @Autowired
    PackageGRNItemDistributionRepository packageGRNItemDistributionRepository;
    @Autowired
    UserControllerService userControllerService;

    public TeqipPackageModel savePackage(HttpServletRequest httpServletRequest, TeqipPackageModel packageModel) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute("claim");
        String role = claim.getPmssrole();

        procurementUtility.checkBudgetBeforeCreatePlan(httpServletRequest, packageModel);

        TeqipPackage teqipPackage = procurementMapper.savePackageModel(packageModel);

        // Logic for setting plan creator
        teqipPackage.setTypeofplanCreator(0);
        if (ProcurementConstants.isInstitutionalRole(role)) {
            TeqipInstitution teqipInstitution = procurementUtility.getTeqipInstitutionFromSession(httpServletRequest);
            teqipPackage.setTeqipInstitution(teqipInstitution);

            teqipPackage.setTypeofplanCreator(teqipInstitution.getTeqipInstitutiontype().getInstitutiontypeId());

            teqipPackage.setTeqipStatemaster(null);
        }
        if (ProcurementConstants.isStateRole(role)) {
            teqipPackage.setTeqipInstitution(null);
            teqipPackage.setTeqipStatemaster(procurementUtility.getTeqipStatemasterDetailsFromSession(httpServletRequest));

        }
        
        if (ProcurementConstants.isNationalRole(role) || ProcurementConstants.isEdcilRole(role)) {
            teqipPackage.setTeqipInstitution(null);
            teqipPackage.setTeqipStatemaster(null);
        }

        if ( ProcurementConstants.isEdcilRole(role)) {
           teqipPackage.setTypeofplanCreator(1);
        }
        
         int type = teqipPackage.getTypeofplanCreator();

        TeqipPlanApprovalstatus approvalstatus = teqipPlanApprovalStatusRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(
                teqipPackage.getTeqipInstitution(), teqipPackage.getTeqipStatemaster(),teqipPackage.getTypeofplanCreator());

        if (approvalstatus == null) {
            int roleId = ProcurementConstants.isInstitutionalRole(role)? 5: ProcurementConstants.isStateRole(role)?4:10;
            
            Integer findInitialApprovedStage = teqipPmssProcurementstageRepository.findInitialApprovedStage(
                    type, roleId, 0);

            TeqipPmssProcurementstage findOne = teqipPmssProcurementstageRepository.findOne(findInitialApprovedStage);

            approvalstatus = new TeqipPlanApprovalstatus();
            approvalstatus.setTeqipProcurementmaster(teqipPackage.getTeqipProcurementmaster());
            approvalstatus.setTeqipInstitution(teqipPackage.getTeqipInstitution());
            approvalstatus.setTeqipStatemaster(teqipPackage.getTeqipStatemaster());
            approvalstatus.setTeqipPmssProcurementstage(findOne);
            approvalstatus.setReviseid(1);
            approvalstatus.setCurrentStage(1);
            approvalstatus.setType(type);

            teqipPlanApprovalStatusRepository.saveAndFlush(approvalstatus);
        }

        TeqipPmssProcurementstage teqipPmssProcurementstage = approvalstatus.getTeqipPmssProcurementstage();

        if (teqipPmssProcurementstage.getProcurementstageid() != 1
                && teqipPmssProcurementstage.getProcurementstageid() != 2
                && teqipPmssProcurementstage.getProcurementstageid() != 10) {
            throw new PMSSBudgetException("Invalid Package stage ",
                    new ErrorCode("PMSS-PACKAGE-STAGE", "Package stage issue"));
        }

        teqipPackage.setTeqipPmssProcurementstage(teqipPmssProcurementstage);
        teqipPackage.setReviseId(approvalstatus.getReviseid());
        teqipPackage.setIsPackageCancelled(teqipPackage.getIsPackageCancelled() == null ? 0 : teqipPackage.getIsPackageCancelled());
        teqipPackage.setIsPackageCompleted(teqipPackage.getIsPackageCompleted() == null ? 0 : teqipPackage.getIsPackageCompleted());
        teqipPackage.setIsInitiated(teqipPackage.getIsInitiated() == null ? 0 : teqipPackage.getIsInitiated());

        if (teqipPackage.getPackageCode() == null) {
            TeqipPackage lastpackage = teqipPackageRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeofplanCreatorOrderByPackageIdDesc(
                    teqipPackage.getTeqipInstitution(), 
                    teqipPackage.getTeqipStatemaster(),
                    type);

            int id = 0;
            if (lastpackage != null) {
                String str = lastpackage.getPackageCode();
                id = Integer.parseInt(str.substring(str.lastIndexOf("/")+1))+1;
            }else{
                id = 1;
            }
            teqipPackage.setPackageCode(procurementUtility.getPackageCode() + id);
        }

        teqipPackage = teqipPackageRepository.saveAndFlush(teqipPackage);

        int packageId = teqipPackage.getPackageId();

        itemGoodsMapper.mapObjectToEntity(packageModel.getTeqipItemGoodsDetails(), teqipPackage);
        itemWorkMapper.mapObjectToEntity(packageModel.getTeqipItemWorkDetails(), teqipPackage);

        this.entityManager.clear();

        return procurementMapper.mapTeqipPackage(teqipPackageRepository.findOne(packageId), true);
    }

    public List<TeqipPackage> getPackages(TeqipProcurementmaster plan, 
            TeqipStatemaster state, 
            TeqipInstitution institute,
            Integer typeofplanCreator,
            String status,
            List<TeqipPmssProcurementstage> approved) {

        if (status.equals("ALL")) {

            if (plan != null) {
                return teqipPackageRepository.findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndTypeofplanCreator(plan, institute, state,typeofplanCreator);
            } else {
                return teqipPackageRepository.findByTeqipInstitutionAndTeqipStatemasterAndTypeofplanCreator(institute, state,typeofplanCreator);
            }
        }
        if (status.equals("INITIATE")) {

            if (plan != null) {
                return teqipPackageRepository.findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndTeqipPmssProcurementstageInAndIsInitiatedAndTypeofplanCreator(
                        plan, institute, state, approved, 0,typeofplanCreator);

            } else {
                return teqipPackageRepository.findByTeqipInstitutionAndTeqipStatemasterAndTeqipPmssProcurementstageInAndIsInitiatedAndTypeofplanCreator(
                        institute, state, approved, 0,typeofplanCreator);

            }
        }
        if (status.equals("INPROGESS")) {

            if (plan != null) {
                return teqipPackageRepository.findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndTeqipPmssProcurementstageInAndIsInitiatedAndIsPackageCancelledAndIsPackageCompletedAndInitiateDateNotNullAndTypeofplanCreator(
                        plan, institute, state, approved, 1, 0, 0,typeofplanCreator);

            } else {
                return teqipPackageRepository.findByTeqipInstitutionAndTeqipStatemasterAndTeqipPmssProcurementstageInAndIsInitiatedAndIsPackageCancelledAndIsPackageCompletedAndInitiateDateNotNullAndTypeofplanCreator(
                        institute, state, approved, 1, 0, 0,typeofplanCreator);
            }
        }
        if (status.equals("CANCELLED")) {

            if (plan != null) {
                return teqipPackageRepository.findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndIsPackageCancelledAndTypeofplanCreator(plan, institute, state, 1,typeofplanCreator);
            } else {
                return teqipPackageRepository.findByTeqipInstitutionAndTeqipStatemasterAndIsPackageCancelledAndTypeofplanCreator(institute, state, 1,typeofplanCreator);
            }
        }
        if (status.equals("COMPLETED")) {

            if (plan != null) {
                return teqipPackageRepository.findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndIsPackageCompletedAndTypeofplanCreator(plan, institute, state, 1,typeofplanCreator);
            } else {
                return teqipPackageRepository.findByTeqipInstitutionAndTeqipStatemasterAndIsPackageCompletedAndTypeofplanCreator(institute, state, 1,typeofplanCreator);
            }
        }
        return null;
    }

    public List<TeqipPackage> getInstitutePackage(
            int instituteId,
            Integer planId,
            String status) {

        TeqipInstitution institute = teqipInstitutionRepository.findOne(instituteId);
        Integer type = institute.getTeqipInstitutiontype().getInstitutiontypeId();

        TeqipProcurementmaster plan = null;
        if (planId != null && planId != 0) {
            plan = procurementPlanRepository.findOne(planId);
        }

        List<TeqipPmssProcurementstage> approved = null;
        if (status.equals("INITIATE") || status.equals("INPROGESS")) {

            approved = teqipPmssProcurementstageRepository.findByRoleIdAndTypeAndProcurementstageidIn(5, type, new Integer[]{6, 12});
        }

        return getPackages(plan, null, institute,type, status, approved);

    }

    public List<TeqipPackage> getStatePackage(
            int stateId,
            Integer planId,
            String status
    ) {

        TeqipProcurementmaster plan = null;
        if (planId != null && planId != 0) {
            plan = procurementPlanRepository.findOne(planId);
        }

        TeqipStatemaster state = teqipStatemasteryRepository.findOne(stateId);

        List<TeqipPmssProcurementstage> approved = null;
        if (status.equals("INITIATE") || status.equals("INPROGESS")) {

            approved = teqipPmssProcurementstageRepository.findByRoleIdAndTypeAndProcurementstageidIn(4, 0, new Integer[]{6, 12});
        }

        return getPackages(plan, state, null,0, status, approved);
    }

    public List<TeqipPackage> getNationalPackage(
            Integer planId,
            String status
    ) {

        TeqipProcurementmaster plan = null;
        if (planId != null && planId != 0) {
            plan = procurementPlanRepository.findOne(planId);
        }

        List<TeqipPmssProcurementstage> approved = null;
        if (status.equals("INITIATE") || status.equals("INPROGESS")) {

            approved = teqipPmssProcurementstageRepository.findByRoleIdAndTypeAndProcurementstageidIn(10, 0, new Integer[]{6, 12});
        }

        return getPackages(plan, null, null,0, status, approved);

    }
    public List<TeqipPackage> getEdcilPackage(
            Integer planId,
            String status
    ) {

        TeqipProcurementmaster plan = null;
        if (planId != null && planId != 0) {
            plan = procurementPlanRepository.findOne(planId);
        }

        List<TeqipPmssProcurementstage> approved = null;
        if (status.equals("INITIATE") || status.equals("INPROGESS")) {

            approved = teqipPmssProcurementstageRepository.findByRoleIdAndTypeAndProcurementstageidIn(10, 1, new Integer[]{6, 12});
        }

        return getPackages(plan, null, null,1, status, approved);

    }

    public List<TeqipPackageModel> retrievePrepareAndRevisePackage(HttpServletRequest httpServletRequest, int planId) {
        List<TeqipPackage> teqipPackage = null;

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute("claim");

        TeqipProcurementmaster teqipProcurementmaster = teqipProcurementmasterRepository.findOne(planId);

        if (ProcurementConstants.isInstitutionalRole(claim.getPmssrole())) {// For Institute

            TeqipInstitution teqipInstitution = new TeqipInstitution();
            teqipInstitution.setInstitutionId(claim.getInstid());
            teqipPackage = teqipPackageRepository.findByTeqipInstitutionAndTeqipProcurementmaster(teqipInstitution, teqipProcurementmaster);

        } else if (ProcurementConstants.isStateRole(claim.getPmssrole())) {// For State

            TeqipStatemaster teqipStatemaster = new TeqipStatemaster();
            teqipStatemaster.setStateId(claim.getStateid());
            teqipPackage = teqipPackageRepository.findByTeqipStatemasterAndTeqipProcurementmaster(teqipStatemaster, teqipProcurementmaster);

        } else if (ProcurementConstants.isNationalRole(claim.getPmssrole())) {// For NPIU

            teqipPackage = teqipPackageRepository.findByTeqipProcurementmasterAndTypeofplanCreator(teqipProcurementmaster,0);

        }
        else if (ProcurementConstants.isEdcilRole(claim.getPmssrole())) {// For EDCIL

            teqipPackage = teqipPackageRepository.findByTeqipProcurementmasterAndTypeofplanCreator(teqipProcurementmaster,1);

        }
        return procurementMapper.mapTeqipPackages(teqipPackage, true);
    }

    public TeqipTimelineModel getPackageTimeline(Integer procMethodId, Date sanctionDate, Integer catId) {

        TeqipProcurementmethod teqipProcMethod = new TeqipProcurementmethod();
        teqipProcMethod.setProcurementmethodId(procMethodId);

        TeqipCategorymaster teqipCatMaster = new TeqipCategorymaster();
        teqipCatMaster.setCatId(catId);

        TeqipProcurementmethodTimeline teqipPackageTimeline = timelineRepository
                .findByTeqipProcurementmethodAndTeqipCategorymaster(teqipProcMethod, teqipCatMaster);

        return procurementUtility.getTeqipTimeline(teqipPackageTimeline, sanctionDate);
    }

    public void deletePackage(int packageId) {

        TeqipPackage entity = this.teqipPackageRepository.findOne(packageId);

        this.itemGoodsMapper.deleteItemGoods(entity.getTeqipItemGoodsDetails(), new ArrayList<Integer>());
        this.itemWorkMapper.deletePackageWorks(entity.getTeqipItemWorkDetails(), new ArrayList<Integer>());

        this.teqipPackageRepository.delete(packageId);
    }

    public void deleteSupplier(int packageId, int supplierId) {

        TeqipPackage teqipPackage = this.teqipPackageRepository.findOne(packageId);
        TeqipPmssSupplierMaster teqipPmssSupplierMaster = this.supplierRepository.findOne(supplierId);

        TeqipPackageSupplierDetail packageSupplierDetail
                = packageSupplierRepository.findByTeqipPackageAndTeqipPmssSupplierMaster(teqipPackage, teqipPmssSupplierMaster);

        if (packageSupplierDetail != null) {

            packageSupplierRepository.delete(packageSupplierDetail);
        } else {

            throw new PMSSBudgetException("Supplier is not there.",
                    new ErrorCode("SUPPLIER NOT FOUND", "Supplier issue"));

        }
    }

    /**
     * Fetch institute, state or national packages
     *
     * @param packageType INST | SPFU | NPIU
     * @param id
     * @param planId
     * @param packageState ALL|INITIATE|INPROGRESS
     * @return
     */
    public List<TeqipPackage> getPackages(
            String packageType,
            int id,
            Integer planId,
            String packageState) {

        if (ProcurementConstants.isInstitutionalRole(packageType)) {
            return this.getInstitutePackage(id, planId, packageState);
        }
        if (ProcurementConstants.isStateRole(packageType)) {
            return this.getStatePackage(id, planId, packageState);

        }
        if (ProcurementConstants.isNationalRole(packageType)) {
            return this.getNationalPackage(planId, packageState);

        }
        if (ProcurementConstants.isEdcilRole(packageType)) {
            return this.getEdcilPackage(planId, packageState);

        }
        return null;
    }

    /**
     * Fetch institute, state or national packages
     *
     * @param packageType INST | SPFU | NPIU
     * @param id
     * @param planId
     * @param revisionId
     * @return
     */
    public List<TeqipPackageHistory> getPackagesHistory(
            String packageType,
            int id,
            Integer planId,
            Integer revisionId) {

        TeqipInstitution institute = null;
        if (ProcurementConstants.isInstitutionalRole(packageType) && id != 0) {

            institute = teqipInstitutionRepository.findOne(id);
        }
        TeqipProcurementmaster plan = null;
        if (planId != null && planId != 0) {
            plan = procurementPlanRepository.findOne(planId);
        }
        TeqipStatemaster state = null;
        if (ProcurementConstants.isStateRole(packageType) && id != 0) {

            state = teqipStatemasteryRepository.findOne(id);
        }

        if (plan != null) {
            return packageHistoryRepository.findByTeqipProcurementmasterAndTeqipInstitutionAndTeqipStatemasterAndReviseId(plan, institute, state, revisionId);
        } else {
            return packageHistoryRepository.findByTeqipInstitutionAndTeqipStatemasterAndReviseId(institute, state, revisionId);
        }

    }

    public void initiatePackage(int packageId, Date initiateDate, int isInitiated, Date sanctionDate) {

        TeqipPackage teqipPackage = this.teqipPackageRepository.findOne(packageId);
        teqipPackage.setInitiateDate(initiateDate);
        teqipPackage.setIsInitiated(isInitiated);
        teqipPackage.setFinancialSanctionDate(sanctionDate);
        if (isInitiated == 1) {
            teqipPackage.setPackageInitiationDate(new Date());
        }
        teqipPackageRepository.saveAndFlush(teqipPackage);
    }

    public void deleteQuestion(int packageId, int questionId) {

        TeqipPackageQuestionMaster question = this.packageQuestionRepository.findOne(questionId);
        if (question.getTeqipPackage().getPackageId() == packageId) {

            this.packageQuestionRepository.delete(question);
        } else {

            throw new PMSSBudgetException("Question is not available.",
                    new ErrorCode("QUESTION NOT FOUND", "Question issue"));
        }
    }

    public void deletePackagePayment(int packageId, int packagePaymentId) {

        TeqipPackagePaymentDetail payment = this.packagePaymentRepository.findOne(packagePaymentId);
        if (payment.getTeqipPackage().getPackageId() == packageId) {

            this.packagePaymentRepository.delete(payment);
        } else {

            throw new PMSSBudgetException("Payment is not available.",
                    new ErrorCode("PAYMENT NOT FOUND", "Payment issue"));
        }

    }

    public void deletePackageUpload(int packageId, int packageDocumentId) {

        TeqipPackageDocument entity = this.documentRepository.findOne(packageDocumentId);
        if (entity.getTeqipPackage().getPackageId() == packageId) {

            this.documentRepository.delete(entity);
        } else {

            throw new PMSSBudgetException("Upload is not available.",
                    new ErrorCode("UPLOAD NOT FOUND", "Upload issue"));
        }

    }

    public void deleteTechnicalCriteria(int packageId, int technicalCriteriaId) {

        TeqipPackageTechCriteria entity = techCriteriaRepository.findOne(technicalCriteriaId);
        if (entity != null && entity.getTeqipPackage().getPackageId() == packageId) {

            List<TeqipPackageTechSubCriteria> techSubCriterias = entity.getTechSubCriterias();
            if (techSubCriterias != null) {
                for (TeqipPackageTechSubCriteria techSubCriteria : techSubCriterias) {
                    this.deleteTechnicalSubCriteria(techSubCriteria);

                }
            }

            this.techCriteriaRepository.delete(entity);
        } else {

            throw new PMSSBudgetException("TechCriteria  is not available.",
                    new ErrorCode("TechCriteria  NOT FOUND", "TechCriteria  issue"));
        }
    }

    public void deleteTechnicalSubCriteria(TeqipPackageTechSubCriteria entity) {

        List<TeqipPackageConsultantEvaluation> consultantEvaluations = consultantEvaluationRepository.findByTeqipPackageTechSubCriteria(entity);

        if (consultantEvaluations != null) {
            consultantEvaluationRepository.deleteInBatch(consultantEvaluations);
        }

        this.techSubCriteriaRepository.delete(entity);
    }

    public void deleteTechnicalSubCriteria(int packageId, int technicalSubCriteriaId) {
        TeqipPackageTechSubCriteria entity = techSubCriteriaRepository.findOne(technicalSubCriteriaId);
        if (entity != null && entity.getTeqipPackage().getPackageId() == packageId) {

            this.deleteTechnicalSubCriteria(entity);

        } else {

            throw new PMSSBudgetException("TechSubCriteria is not available.",
                    new ErrorCode("TechSubCriteria NOT FOUND", "TechSubCriteria issue"));
        }
    }

    public void deleteTechEvalCommittee(int packageId, int memberId) {
        TeqipPackageTechEvalCommitee entity = techEvalCommiteeRepository.findOne(memberId);
        if (entity != null && entity.getTeqipPackage().getPackageId() == packageId) {

            this.techEvalCommiteeRepository.delete(entity);
        } else {

            throw new PMSSBudgetException("TechEvalCommitee is not available.",
                    new ErrorCode("TechEvalCommitee NOT FOUND", "TechEvalCommitee issue"));
        }

    }

    public void deleteWorkCompletionCertificate(int packageId, int wccId) {
        TeqipPackageWorkOrderCertificate entity = this.workOrderCertificateRepository.findOne(wccId);
        if (entity != null && entity.getTeqipPackage().getPackageId() == packageId) {
            this.workOrderCertificateRepository.delete(entity);
        } else {

            throw new PMSSBudgetException("PackageWorkOrderCertificate is not available.",
                    new ErrorCode("PackageWorkOrderCertificate NOT FOUND", "PackageWorkOrderCertificate issue"));
        }

    }

    public void deleteGRMItem(int packageId, int grnItemId) {
        TeqipPackageGRNItemDetail entity = this.packageGRNItemDetailRepository.findOne(grnItemId);
        if (entity != null && entity.getTeqipPackage().getPackageId() == packageId) {
            if (entity.getTeqipPackageGRNItemDistributions() != null && entity.getTeqipPackageGRNItemDistributions().size() > 0) {
                for (TeqipPackageGRNItemDistribution grnItemDistribution : entity.getTeqipPackageGRNItemDistributions()) {
                    this.packageGRNItemDistributionRepository.delete(grnItemDistribution);
                }
            }
            this.packageGRNItemDetailRepository.flush();
            this.packageGRNItemDetailRepository.delete(entity);
        } else {

            throw new PMSSBudgetException("TeqipPackageGRNItemDetail is not available.",
                    new ErrorCode("TeqipPackageGRNItemDetail NOT FOUND", "TeqipPackageGRNItemDetail issue"));
        }

    }

    public void deleteGRN(int packageId, int grnId) {

        TeqipPackageGRNDetail grnentity = this.packageGRNDetailRepository.findOne(grnId);
        if (grnentity != null && grnentity.getTeqipPackage().getPackageId() == packageId) {

            TeqipPackageGRNItemDetail entity = this.packageGRNItemDetailRepository.findOne(grnentity.getPackGrnDetailId());
            if (entity != null && entity.getTeqipPackage().getPackageId() == packageId) {
                if (entity.getTeqipPackageGRNItemDistributions() != null && entity.getTeqipPackageGRNItemDistributions().size() > 0) {
                    for (TeqipPackageGRNItemDistribution grnItemDistribution : entity.getTeqipPackageGRNItemDistributions()) {
                        this.packageGRNItemDistributionRepository.delete(grnItemDistribution);
                    }
                }
                this.packageGRNItemDetailRepository.flush();
                this.packageGRNItemDetailRepository.delete(entity);
            }
            this.packageGRNDetailRepository.flush();
            this.packageGRNDetailRepository.delete(grnentity);
        } else {

            throw new PMSSBudgetException("TeqipPackageGRNDetail is not available.",
                    new ErrorCode("TeqipPackageGRNDetail NOT FOUND", "TeqipPackageGRNDetail issue"));
        }

    }

    public List<PackageHistoryList> packageHistory(int instituteId, int stateId) {
        return procurementMapper.mapPackageHistory(teqipInstitutionRepository.packageHistory(instituteId, stateId));
    }

    public List<TeqipPlanAprovalModel> rejectionComments(String type, Integer id) {

        TeqipInstitution institute = null;
        TeqipStatemaster state = null;
        int typeId=0;
        if (ProcurementConstants.isInstitutionalRole(type)) {
            institute = teqipInstitutionRepository.findOne(id);
            if(institute.getTeqipInstitutiontype() != null){
                 typeId = institute.getTeqipInstitutiontype().getInstitutiontypeId();
            }

        } else if (ProcurementConstants.isStateRole(type)) {
            state = teqipStatemasteryRepository.findOne(id);

        }else if (ProcurementConstants.isEdcilRole(type)) {
           typeId = 1;

        }

        List<TeqipPlanApprovalstatus> approvalstatus = teqipPlanApprovalStatusRepository.findAllByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(
                institute, state, typeId);

        List<TeqipPlanAprovalModel> aprovalModels = new ArrayList<>();
        if (approvalstatus != null && !approvalstatus.isEmpty()) {

            TeqipPlanApprovalstatus get = approvalstatus.get(0);

            int index = 0;
            for (TeqipPlanApprovalstatus entity : approvalstatus) {
                if (entity.getTeqipPmssProcurementstage() != null
                        && "rejected".equals(entity.getTeqipPmssProcurementstage().getStatus())) {

                    TeqipPlanAprovalModel model = new TeqipPlanAprovalModel();
                    model.setId(entity.getId());

                    model.setComment(entity.getStatusComment());
                    model.setCreatedOn(entity.getCreatedOn());

                    User user = entity.getCreatedBy() != null ? userControllerService.findByUserid(entity.getCreatedBy()) : null;
                    if (user != null) {
                        model.setCreatedBy(user.getUserName());
                    }

                    if (index == 0
                            && get.getTeqipPmssProcurementstage_prev() != null
                            && get.getTeqipPmssProcurementstage() != null
                            && "approved".equals(get.getTeqipPmssProcurementstage_prev().getStatus())) {
                        model.setLastRejection(true);
                    }

                    aprovalModels.add(model);
                }
                index++;
            }
        }
        return aprovalModels;
    }

    public List<TeqipPackageModel> ppview(String type, Integer id) {

        List<TeqipPackageModel> list = new ArrayList<>();
        List<TeqipPackage> packages = getPackages(type, id, null, "ALL");
        if (packages != null) {
            for (TeqipPackage pack : packages) {
                Integer packageId = pack.getPackageId();
                PrecurementMethodTimeLinePojo findProcureUserTime = referenceService.findProcurerevisedTime(packageId);
                if (findProcureUserTime == null || findProcureUserTime.getProcurementmethodrevisedId() == null || findProcureUserTime.getProcurementmethodrevisedId() == 0) {
                    findProcureUserTime = referenceService.findProcurerevisedTime(packageId);
                }

                TeqipPackageModel ppvm = procurementMapper.mapTeqipPackage(pack, true);
                ppvm.setTimeLine(findProcureUserTime);
                list.add(ppvm);
            }
        }

        return list;
    }

    public List<TeqipPackageModel> priorReview() {

        LoginUser claim = LoggedUser.get();
        String pmssrole = claim.getPmssrole();

        List<TeqipPackage> packages = new ArrayList<>();

        if (ProcurementConstants.USER_ROLE_INST.equalsIgnoreCase(pmssrole)) {
            TeqipInstitution institute = teqipInstitutionRepository.findOne(claim.getInstid());
            packages.addAll(teqipPackageRepository.findPriorReview(institute, "INST"));
        }
        if (ProcurementConstants.USER_ROLE_INST_DIR.equalsIgnoreCase(pmssrole)) {
            TeqipInstitution institute = teqipInstitutionRepository.findOne(claim.getInstid());
            packages.addAll(teqipPackageRepository.findPriorReview(institute, "INST"));
            packages.addAll(teqipPackageRepository.findPriorReview(institute, "INST_DIR"));

        }
        if (ProcurementConstants.USER_ROLE_SPFU.equalsIgnoreCase(pmssrole)) {
            TeqipStatemaster state = this.teqipStatemasteryRepository.findOne(claim.getStateid());
            packages.addAll(teqipPackageRepository.findPriorReviewAllState(state, "SPFU"));
            packages.addAll(teqipPackageRepository.findPriorReviewAllStateInst(state, "SPFU"));

        }
        if (ProcurementConstants.USER_ROLE_SPA.equalsIgnoreCase(pmssrole)) {
            TeqipStatemaster state = this.teqipStatemasteryRepository.findOne(claim.getStateid());
            packages.addAll(teqipPackageRepository.findPriorReviewAllState(state, "SPFU"));
            packages.addAll(teqipPackageRepository.findPriorReviewAllStateInst(state, "SPFU"));
            packages.addAll(teqipPackageRepository.findPriorReviewAllState(state, "SPA"));
            packages.addAll(teqipPackageRepository.findPriorReviewAllStateInst(state, "SPA"));

        }
        if (ProcurementConstants.USER_ROLE_NPIU.equalsIgnoreCase(pmssrole)
                || ProcurementConstants.USER_ROLE_NPC.equalsIgnoreCase(pmssrole)) {
            packages.addAll(teqipPackageRepository.findPriorReviewAllNPIU("NPIU"));
            packages.addAll(teqipPackageRepository.findPriorReviewAllNPIU("NPC"));
            
        }
        if (ProcurementConstants.USER_ROLE_CPA.equalsIgnoreCase(pmssrole)) {
            packages.addAll(teqipPackageRepository.findPriorReviewAllNPIU("CPA"));
            packages.addAll(teqipPackageRepository.findPriorReviewAllNPIU("NPIU"));
            packages.addAll(teqipPackageRepository.findPriorReviewAllNPIU("NPC"));
        }

        if (ProcurementConstants.USER_ROLE_EDCIL.equalsIgnoreCase(pmssrole)) {
            packages.addAll(teqipPackageRepository.findPriorReviewAllNPIU("EDCIL"));
        }
        
        List<TeqipPackageModel> list = new ArrayList<>();
        if (!packages.isEmpty()) {
            for (TeqipPackage pack : packages) {
                TeqipPackageModel ppvm = procurementMapper.mapTeqipPackage(pack, false);
                list.add(ppvm);
            }
        }

        return list;
    }
}
