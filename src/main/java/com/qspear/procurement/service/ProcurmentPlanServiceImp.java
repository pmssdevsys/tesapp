package com.qspear.procurement.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.stream.Collectors;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSException;
import com.qspear.procurement.model.CityMaster;
import com.qspear.procurement.model.ComponentMasterModel;
import com.qspear.procurement.model.FileObect;
import com.qspear.procurement.model.InstitutionlogoPojo;
import com.qspear.procurement.model.Mail;
import com.qspear.procurement.model.NpiuDetailsMasterPojo;
import com.qspear.procurement.model.PrecurementMethodTimeLineModel;
import com.qspear.procurement.model.PrecurementMethodTimeLinePojo;
import com.qspear.procurement.model.SupplierMasterModel;
import com.qspear.procurement.model.TeqipActivitiymasterModel;
import com.qspear.procurement.model.TeqipCategorymasterModel;
import com.qspear.procurement.model.TeqipDepartmentModel;
import com.qspear.procurement.model.TeqipHelpdeskTicketModel;
import com.qspear.procurement.model.TeqipInstitutionPojo;
import com.qspear.procurement.model.TeqipInstitutionTypeModel;
import com.qspear.procurement.model.TeqipInstitutiondepartmentmappingModel;
import com.qspear.procurement.model.TeqipInstitutionsubcomponentmappingPojo;
import com.qspear.procurement.model.TeqipItemMasterModel;
import com.qspear.procurement.model.TeqipPmssInstitutionpurchasecommittePojo;
import com.qspear.procurement.model.TeqipPmssThreshholdmodel;
import com.qspear.procurement.model.TeqipProcurementmethodModel;
import com.qspear.procurement.model.TeqipStatemastermodel;
import com.qspear.procurement.model.TeqipSubcategorymasterModel;
import com.qspear.procurement.model.TeqipSubcomponentModel;
import com.qspear.procurement.model.TeqipSubcomponentmasterModel;
import com.qspear.procurement.model.UserLoginDetails;
import com.qspear.procurement.model.UserPojo;
import com.qspear.procurement.persistence.DepartmentMaster;
import com.qspear.procurement.persistence.HelpDeskTicketDetails;
import com.qspear.procurement.persistence.Helpdesk_documentattachment;
import com.qspear.procurement.persistence.Helpdesk_ticket;
import com.qspear.procurement.persistence.Helpdesk_tickethistory;
import com.qspear.procurement.persistence.NpiuDetailsMaster;
import com.qspear.procurement.persistence.SupplierMaster;
import com.qspear.procurement.persistence.TeqipActivitiymaster;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipCitymaster;
import com.qspear.procurement.persistence.TeqipComponentmaster;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import com.qspear.procurement.persistence.TeqipInstitutionsubcomponentmapping;
import com.qspear.procurement.persistence.TeqipInstitutiontype;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipPmssInstitutionlogo;
import com.qspear.procurement.persistence.TeqipPmssInstitutionpurchasecommitte;
import com.qspear.procurement.persistence.TeqipPmssThreshholdmaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipRolemaster;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;
import com.qspear.procurement.persistence.TeqipUsersDetail;
import com.qspear.procurement.persistence.TeqipUsersMaster;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import com.qspear.procurement.repositories.HelpDeskTicketDetailsRepository;
import com.qspear.procurement.repositories.Helpdesk_documentattachmentRepository;
import com.qspear.procurement.repositories.Helpdesk_ticketRepository;
import com.qspear.procurement.repositories.Helpdesk_tickethistoryRepository;
import com.qspear.procurement.security.util.AesUtil;
import com.qspear.procurement.security.util.LoggedUser;
import com.qspear.procurement.security.util.LoginUser;
import com.qspear.procurement.util.FileUtils;
import com.qspear.procurement.util.UserCreater;

@Service
// @Transactional
public class ProcurmentPlanServiceImp extends AbstractProcurementPlanRepositoryPackage {

	private static final Exception Exception = null;
	private static final String npiu = "NPIU";
	private static final String inst = "INST";
	private static final String spfu = "SPFU";
	private static final String spa = "SPA";
	@Autowired
	private JavaMailSender sender;
	@Autowired
	Helpdesk_tickethistoryRepository helpdesk_tickethistoryRepository;

	@Autowired
	HelpDeskTicketDetailsRepository helpDeskTicketDetailsRepository;

	@Autowired
	Helpdesk_ticketRepository helpdesk_ticketRepository;
	@Autowired
	Helpdesk_documentattachmentRepository helpdesk_documentattachmentRepository;

	public ArrayList<TeqipActivitiymasterModel> findAllActivityList() {
		// TODO Auto-generated method stub
		List<TeqipActivitiymaster> activityList = activityRepository.findAllActivity();
		ArrayList<TeqipActivitiymasterModel> activitymasterModel = new ArrayList();

		java.util.Iterator<TeqipActivitiymaster> it = activityList.listIterator();
		while (it.hasNext()) {
			TeqipActivitiymasterModel model = new TeqipActivitiymasterModel();
			TeqipActivitiymaster actitvity = it.next();
			model.setActivityId(actitvity.getActivityId());
			model.setActivityName(actitvity.getActivityName());
			activitymasterModel.add(model);

		}
		return activitymasterModel;
	}

	public ArrayList<TeqipCategorymasterModel> findAllCategoryList() {
		// TODO Auto-generated method stub
		List<TeqipCategorymaster> categorymaster = categoryRepository.findAll();
		ArrayList<TeqipCategorymasterModel> categorymastermodel = new ArrayList();
		java.util.Iterator<TeqipCategorymaster> it = categorymaster.listIterator();
		while (it.hasNext()) {
			TeqipCategorymaster category = it.next();
			TeqipCategorymasterModel model = new TeqipCategorymasterModel();
			model.setCatId(category.getCatId());
			model.setCategoryName(category.getCategoryName());
			categorymastermodel.add(model);

		}
		return categorymastermodel;
	}

	public String findActivityByName(String activityName) {
		// TODO Auto-generated method stub
		return activityRepository.findActivityByName(activityName);
	}

	public TeqipActivitiymasterModel save(TeqipActivitiymasterModel newActivity) {
		// TODO Auto-generated method stub
		TeqipActivitiymaster entity = new TeqipActivitiymaster();
		LoginUser claim = LoggedUser.get();
		TeqipCategorymaster categorymaster = categoryRepository.findpmsscatbyid(newActivity.getCatId());
		entity.setActivityId(newActivity.getActivityId());
		entity.setActivityName(newActivity.getActivityName());
		entity.setTeqipCategorymaster(categorymaster);
		entity.setIsactive(true);
		entity.setCreatedBy(claim.getUserid());
		TeqipActivitiymaster activity = activityRepository.save(entity);
		TeqipActivitiymasterModel model = new TeqipActivitiymasterModel();
		model.setActivityId(activity.getActivityId());
		model.setActivityName(activity.getActivityName());
		return model;
	}

	public TeqipActivitiymasterModel updateActivity(TeqipActivitiymasterModel activity) {
		// TODO Auto-generated method stub
		TeqipActivitiymaster activitymster = activityRepository.findOne(activity.getActivityId());
		if (!activitymster.getActivityName().equalsIgnoreCase(activity.getActivityName()))
			activitymster.setActivityName(activity.getActivityName());
		else
			throw new PMSSException("Duplicate entry", new ErrorCode("401", "Failure"));
		TeqipActivitiymaster teqipActivitiymaster = activityRepository.save(activitymster);
		TeqipActivitiymasterModel model = new TeqipActivitiymasterModel();
		model.setActivityId(teqipActivitiymaster.getActivityId());
		model.setActivityName(teqipActivitiymaster.getActivityName());
		return model;
	}

	public List<ComponentMasterModel> findAllComponenet() {
		// TODO Auto-generated method stub
		List<TeqipComponentmaster> componentmaster = componentMasterRepository.findAllComponents();
		List<ComponentMasterModel> componentmasterModel = new ArrayList();
		for (TeqipComponentmaster component : componentmaster) {
			ComponentMasterModel componenetmodel = new ComponentMasterModel();
			componenetmodel.setId(component.getId());

			componenetmodel.setComponentCode(component.getComponentCode());
			componenetmodel.setComponentName(component.getComponentName());
			componenetmodel.setIsactive(component.getIsactive());
			componentmasterModel.add(componenetmodel);

		}
		return componentmasterModel;
	}

	public List<TeqipSubcomponentModel> getAllSubComonentById(Long componentId, Integer activeStatus) {
		// TODO Auto-generated method stub
		List<TeqipSubcomponentmaster> subcomponentmaster = teqipSubcomponentmasterRepository
				.findSubcomponenetByCompid(componentId, activeStatus);
		List<TeqipSubcomponentModel> subcomponenetmodel = new ArrayList();
		for (TeqipSubcomponentmaster subcomponent : subcomponentmaster) {
			TeqipSubcomponentModel subcomp = new TeqipSubcomponentModel();
			subcomp.setComponentId(subcomponent.getTeqipComponentmaster().getId());
			subcomp.setSubComponentId(subcomponent.getId());
			subcomp.setSubComponentCode(subcomponent.getSubcomponentcode());
			subcomp.setSubComponentName(subcomponent.getSubComponentName());
			subcomponenetmodel.add(subcomp);
		}
		return subcomponenetmodel;
	}

	public ComponentMasterModel saveComponent(ComponentMasterModel component) {
		// TODO Auto-generated method stub
		TeqipComponentmaster entity = new TeqipComponentmaster();
		entity.setComponentCode(component.getComponentCode());
		entity.setComponentName(component.getComponentName());
		entity.setIsactive(true);
		TeqipComponentmaster comp = componentMasterRepository.save(entity);
		ComponentMasterModel res = new ComponentMasterModel();
		res.setComponentCode(comp.getComponentCode());
		res.setComponentName(comp.getComponentName());
		return res;
	}

	public TeqipSubcomponentModel addSubComponent(TeqipSubcomponentModel subComponent) {
		// TODO Auto-generated method stub
		TeqipSubcomponentmaster entity = new TeqipSubcomponentmaster();
		TeqipComponentmaster compo = componentMasterRepository.findOne(subComponent.getComponentId());
		entity.setTeqipComponentmaster(compo);
		entity.setIsactive(true);
		entity.setSubcomponentcode(subComponent.getSubComponentCode());
		entity.setSubComponentName(subComponent.getSubComponentName());
		TeqipSubcomponentmaster response = teqipSubcomponentmasterRepository.save(entity);
		TeqipSubcomponentModel res = new TeqipSubcomponentModel();
		res.setSubComponentCode(response.getSubcomponentcode());
		res.setSubComponentName(response.getSubComponentName());
		return res;
	}

	public TeqipSubcomponentModel saveSubcomponent(TeqipSubcomponentModel subComponent) {
		// TODO Auto-generated method stub
		TeqipSubcomponentModel res = new TeqipSubcomponentModel();
		if (subComponent.getSubComponentId() != null) {
			TeqipSubcomponentmaster subcomponentmaster = teqipSubcomponentmasterRepository
					.getOne(subComponent.getSubComponentId());
			subcomponentmaster.setSubcomponentcode(subComponent.getSubComponentCode());
			subcomponentmaster.setSubComponentName(subComponent.getSubComponentName());
			TeqipSubcomponentmaster response = teqipSubcomponentmasterRepository.save(subcomponentmaster);

			res.setSubComponentCode(response.getSubcomponentcode());
			res.setSubComponentName(response.getSubComponentName());

		}
		return res;
	}

	public void updateSubComponent(Integer id) {
		// TODO Auto-generated method stub
		TeqipSubcomponentmaster subComponent = teqipSubcomponentmasterRepository.getOne(id);
		if (subComponent != null) {
			subComponent.setIsactive(false);
			teqipSubcomponentmasterRepository.save(subComponent);
		}

	}

	public ArrayList<TeqipSubcategorymasterModel> getSubCategoryMasterDataById() {
		// TODO Auto-generated method stub
		List<TeqipSubcategorymaster> subcategorymaster = teqipSubcategorymasterRepository.findAll();
		ArrayList<TeqipSubcategorymasterModel> subcategory = new ArrayList();
		for (TeqipSubcategorymaster subcatmaster : subcategorymaster) {
			if (subcatmaster != null) {
				TeqipSubcategorymasterModel subcat = new TeqipSubcategorymasterModel();
				TeqipCategorymasterModel master = new TeqipCategorymasterModel();
				master.setCatId(subcatmaster.getTeqipCategorymaster().getCatId());
				master.setCategoryName(subcatmaster.getTeqipCategorymaster().getCategoryName());
				subcat.setCatmastermodel(master);
				subcat.setSubCategoryName(subcatmaster.getSubCategoryName());
				subcat.setSubcatId(subcatmaster.getSubcatId());
				subcategory.add(subcat);
				subcategory.stream().filter(value -> value != null);
			}

		}
		return subcategory;
	}

	public List<TeqipInstitutiontype> getInstitutionType() {
		// TODO Auto-generated method stub
		return institutionTypeRepository.findAll();
	}

	@Cacheable("suppliername")
	public List<Object[]> getAllSupplierState(String word) {
		List<Object[]> ob = procurmentPlanDaoImp.searchSupplierName(word);
		List al = new ArrayList();
		ob.spliterator();
		for (Object object[] : ob) {
			Map<String, Object> map = new HashMap();
			map.put("suppliername", object[1]);
			al.add(map);
			// al.add(map);

		}
		return al;
	}

	public List<Object[]> getStateForSupplier() {
		List<Object[]> ob = procurmentPlanDaoImp.getStateForSupplier();
		ArrayList al = new ArrayList();
		ob.spliterator();
		for (Object object[] : ob) {
			// Map<String,Object> map=new HashMap();
			// map.put("suppliername", object[1]);
			al.add(object[0]);
			// al.add(map);

		}
		return al;
	}

	public List<SupplierMaster> getAllSupplierName() {
		List<Object[]> list = procurmentPlanDaoImp.getAllSupplierName();
		ArrayList al = new ArrayList();
		for (Object[] ab : list) {
			Map<String, Object> map = new HashMap();
			map.put("label", ab[1]);

			al.add(map);
		}
		return al;
	}

	public List<SupplierMaster> getAllSuppState(String name) {
		// TODO Auto-generated method stub
		List<SupplierMaster> listsupl = supplierMasterRepo.findSupplierState(name);

		return listsupl;

		// List<Object[]> listsupl = supplierMasterRepo.findSupplierState(name);
		// List<SupplierMasterModel> supplist = new ArrayList<>();
		// Map<String, Object> supply = new HashMap<>();
		// for (Object[] obj : listsupl) {
		// SupplierMasterModel suppModel = new SupplierMasterModel();
		// suppModel.setSupplierID((Integer) obj[0]);
		// suppModel.setSuppliername((String) obj[1]);
		// suppModel.setSupllieraddress((String) obj[2]);
		// suppModel.setSuplieremailid((String) obj[3]);
		// suppModel.setSupplierrepresentativename((String) obj[4]);
		// suppModel.setSuppliercity((String) obj[5]);
		// suppModel.setSupplierstate((String) obj[6]);
		// suppModel.setSuppliercountry((String) obj[7]);
		// suppModel.setSuppliernationality((String) obj[8]);
		// suppModel.setSuppliersource((String) obj[9]);
		// suppModel.setSupplierspecificsource((String) obj[10]);
		// suppModel.setSupplierpincode((String) obj[11]);
		// suppModel.setSupplierphoneno((String) obj[12]);
		// suppModel.setSupliertannumber((String) obj[13]);
		// suppModel.setSuplierfaxnumber((String) obj[14]);
		// suppModel.setSuplierpannumber((String) obj[15]);
		// suppModel.setSupliereletter((String) obj[16]);
		// suppModel.setPmss_type((Integer) obj[17]);
		// suppModel.setSupliergstnumber((String) obj[18]);
		// suppModel.setSupliertaxnumber((String) obj[19]);
		// suppModel.setEpfNo((String) obj[20]);
		// suppModel.setEsiNo((String) obj[21]);
		// suppModel.setContractLabourLicenseNo((String) obj[22]);
		// suppModel.setSupplierstateid((Integer) obj[23]);
		// supplist.add(suppModel);
		//
		// }
		// if (supply != null) {
		// supply.put("Success", supplist);
		// } else {
		// supply.put("Failed", "Something Wrong");
		// }
		//
		// return supply;
	}

	public List<SupplierMaster> getAllSupp(String suppliername) {
		// TODO Auto-generated method stub
		String s = suppliername + "%";
		List<SupplierMaster> listsupl = supplierMasterRepo.findSupplier(suppliername);

		return listsupl;
		// List<SupplierMasterModel> supplist = new ArrayList<>();
		// Map<String, Object> supply = new HashMap<>();
		// for (Object[] obj : listsupl) {
		// SupplierMasterModel suppModel = new SupplierMasterModel();
		// suppModel.setSupplierID((Integer) obj[0]);
		// suppModel.setSuppliername((String) obj[1]);
		// suppModel.setSupllieraddress((String) obj[2]);
		// suppModel.setSuplieremailid((String) obj[3]);
		// suppModel.setSupplierrepresentativename((String) obj[4]);
		// suppModel.setSuppliercity((String) obj[5]);
		// suppModel.setSupplierstate((String) obj[6]);
		// suppModel.setSuppliercountry((String) obj[7]);
		// suppModel.setSuppliernationality((String) obj[8]);
		// suppModel.setSuppliersource((String) obj[9]);
		// suppModel.setSupplierspecificsource((String) obj[10]);
		// suppModel.setSupplierpincode((String) obj[11]);
		// suppModel.setSupplierphoneno((String) obj[12]);
		// suppModel.setSupliertannumber((String) obj[13]);
		// suppModel.setSuplierfaxnumber((String) obj[14]);
		// suppModel.setSuplierpannumber((String) obj[15]);
		// suppModel.setSupliereletter((String) obj[16]);
		// suppModel.setPmss_type((Integer) obj[17]);
		// suppModel.setSupliergstnumber((String) obj[18]);
		// suppModel.setSupliertaxnumber((String) obj[19]);
		// suppModel.setEpfNo((String) obj[20]);
		// suppModel.setEsiNo((String) obj[21]);
		// suppModel.setContractLabourLicenseNo((String) obj[22]);
		// suppModel.setSupplierstateid((Integer) obj[23]);
		// supplist.add(suppModel);
		//
		// }
		// if (supply != null) {
		// supply.put("Success", supplist);
		// } else {
		// supply.put("Failed", "Something Wrong");
		// }
		//
		// return supply;
	}

	public Map<String, Object> getSupplierForEdit(Integer id) {
		List<SupplierMasterModel> supplist = new ArrayList<>();
		List<Object[]> listsupl = procurmentPlanDaoImp.getSupplierForEdit(id);

		Map<String, Object> supply = new HashMap<>();
		for (Object[] obj : listsupl) {
			SupplierMasterModel suppModel = new SupplierMasterModel();
			if (obj[0] != null)
				suppModel.setSupplierID((Integer) obj[0]);
			if (obj[1] != null)
				suppModel.setSuppliername((String) obj[1]);
			if (obj[2] != null)
				suppModel.setSupllieraddress((String) obj[2]);
			if (obj[3] != null)
				suppModel.setSuppliercity((String) obj[3]);
			if (obj[4] != null)
				suppModel.setSupplierstate((String) obj[4]);
			if (obj[5] != null)
				suppModel.setSuppliercountry((String) obj[5]);
			if (obj[6] != null)
				suppModel.setSuppliernationality((String) obj[6]);
			if (obj[7] != null)
				suppModel.setSuppliersource((String) obj[7]);
			if (obj[8] != null)
				suppModel.setSupplierspecificsource((String) obj[8]);
			if (obj[9] != null)
				suppModel.setSupplierrepresentativename((String) obj[9]);
			if (obj[10] != null)
				suppModel.setSupplierpincode((String) obj[10]);
			if (obj[11] != null)
				suppModel.setSupplierphoneno((String) obj[11]);
			if (obj[12] != null)
				suppModel.setPmss_type((Integer) obj[12]);
			if (obj[13] != null)
				suppModel.setCREATED_ON((Timestamp) obj[13]);
			if (obj[14] != null)
				suppModel.setCREATED_BY((Integer) obj[14]);
			if (obj[15] != null)
				suppModel.setMODIFY_ON((java.util.Date) obj[15]);
			if (obj[16] != null)
				suppModel.setMODIFY_BY((Integer) obj[16]);
			if (obj[17] != null)
				suppModel.setISACTIVE((Byte) obj[17]);
			if (obj[18] != null)
				suppModel.setSupliertannumber((String) obj[18]);
			if (obj[19] != null)
				suppModel.setSuplierfaxnumber((String) obj[19]);
			if (obj[20] != null)
				suppModel.setSuplierpannumber((String) obj[20]);
			if (obj[21] != null)
				suppModel.setSupliergstnumber((String) obj[21]);
			if (obj[22] != null)
				suppModel.setSupliertaxnumber((String) obj[22]);
			if (obj[23] != null)
				suppModel.setSuplieremailid((String) obj[23]);
			if (obj[24] != null)
				suppModel.setSupliereletter((String) obj[24]);
			if (obj[31] != null)
				suppModel.setSupplierstateid((Integer) obj[31]);

			supplist.add(suppModel);

		}
		if (supply != null) {
			supply.put("Success", supplist);
		} else {
			supply.put("Failed", "Something Wrong");
		}
		return supply;
	}

	public Map<String, Object> saveSupplier(SupplierMasterModel supplierMaster) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		ArrayList SUpplyMasterlist = new ArrayList();
		SupplierMaster master;
		if (supplierMaster == null) {
			return null;
		} else {

			LoginUser claim = LoggedUser.get();
			SupplierMaster supplmaster = new SupplierMaster();
			supplmaster.setSuppliername(supplierMaster.getSuppliername());
			supplmaster.setSuplieremailid(supplierMaster.getSuplieremailid());
			supplmaster.setSupllieraddress(supplierMaster.getSupllieraddress());
			supplmaster.setSupliereletter(supplierMaster.getSupliereletter());
			supplmaster.setSuplierfaxnumber(supplierMaster.getSuplierfaxnumber());
			supplmaster.setSupliergstnumber(supplierMaster.getSupliergstnumber());
			supplmaster.setSuplierpannumber(supplierMaster.getSuplierpannumber());
			supplmaster.setSupliertannumber(supplierMaster.getSupliertannumber());
			supplmaster.setSupliertaxnumber(supplierMaster.getSupliertaxnumber());
			supplmaster.setSuppliercity(supplierMaster.getSuppliercity());
			supplmaster.setSuppliercountry(supplierMaster.getSuppliercountry());
			supplmaster.setSuppliernationality(supplierMaster.getSuppliernationality());
			supplmaster.setSupplierphoneno(supplierMaster.getSupplierphoneno());
			supplmaster.setSupplierpincode(supplierMaster.getSupplierpincode());
			supplmaster.setSupplierrepresentativename(supplierMaster.getSupplierrepresentativename());
			supplmaster.setSuppliersource(supplierMaster.getSuppliersource());
			supplmaster.setSupplierspecificsource(supplierMaster.getSupplierspecificsource());
			if (supplierMaster.getSupplierstateid() != null) {
				supplmaster.setSupplierstate(
						teqipStatemasteryRepository.findOnestate(supplierMaster.getSupplierstateid()));
			}
			supplmaster.setEpfNo(supplierMaster.getEpfNo());
			supplmaster.setEsiNo(supplierMaster.getEsiNo());
			supplmaster.setContractLabourLicenseNo(supplierMaster.getContractLabourLicenseNo());

			supplmaster.setPmss_type(supplierMaster.getPmss_type());
			supplmaster.setISACTIVE(supplierMaster.getISACTIVE());
			supplmaster.setCREATED_BY(claim.getUserid());
			// supplmaster.setCREATED_BY(supplierMaster.getCREATED_BY());
			supplmaster.setMODIFY_BY(claim.getUserid());
			supplmaster.setMODIFY_ON(supplierMaster.getMODIFY_ON());
			master = supplierMasterRepo.save(supplmaster);
			if (master != null) {
				map.put("Success", master.getSupplierID());

			} else {
				map.put("Failed", "Something Wrong");
			}

		}
		return map;
	}

	public Map<String, Object> updateSupplier(SupplierMasterModel supplierMaster) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		SupplierMaster master = null;
		if (supplierMaster == null) {
			return null;
		}
		LoginUser claim = LoggedUser.get();
		if (supplierMaster.getSupplierID() != null) {
			SupplierMaster supplmaster = supplierMasterRepo.getOne(supplierMaster.getSupplierID());
			supplmaster.setSuppliername(supplierMaster.getSuppliername());
			supplmaster.setSuplieremailid(supplierMaster.getSuplieremailid());
			supplmaster.setSupllieraddress(supplierMaster.getSupllieraddress());
			supplmaster.setSupliereletter(supplierMaster.getSupliereletter());
			supplmaster.setSuplierfaxnumber(supplierMaster.getSuplierfaxnumber());
			supplmaster.setSupliergstnumber(supplierMaster.getSupliergstnumber());
			supplmaster.setSuplierpannumber(supplierMaster.getSuplierpannumber());
			supplmaster.setSupliertannumber(supplierMaster.getSupliertannumber());
			supplmaster.setSupliertaxnumber(supplierMaster.getSupliertaxnumber());
			supplmaster.setSuppliercity(supplierMaster.getSuppliercity());
			supplmaster.setSupplierstate(supplierMaster.getSupplierstate());
			supplmaster.setSuppliercountry(supplierMaster.getSuppliercountry());
			supplmaster.setSuppliernationality(supplierMaster.getSuppliernationality());
			supplmaster.setSupplierphoneno(supplierMaster.getSupplierphoneno());
			supplmaster.setSupplierpincode(supplierMaster.getSupplierpincode());
			supplmaster.setSupplierrepresentativename(supplierMaster.getSupplierrepresentativename());
			supplmaster.setSuppliersource(supplierMaster.getSuppliersource());
			supplmaster.setMODIFY_BY(claim.getUserid());
			supplmaster.setSupplierspecificsource(supplierMaster.getSupplierspecificsource());
			if (supplierMaster.getSupplierstateid() != null) {
				supplmaster.setSupplierstate(
						teqipStatemasteryRepository.findOnestate(supplierMaster.getSupplierstateid()));
			}
			supplmaster.setEpfNo(supplierMaster.getEpfNo());
			supplmaster.setEsiNo(supplierMaster.getEsiNo());
			supplmaster.setContractLabourLicenseNo(supplierMaster.getContractLabourLicenseNo());

			supplmaster.setPmss_type(supplierMaster.getPmss_type());
			master = supplierMasterRepo.save(supplmaster);
			if (master != null) {
				map.put("Success", master.getSupplierID());

			} else {
				map.put("Failed", "Something Wrong");
			}

		}
		return map;
	}

	public Map<String, Object> saveSubCategory(TeqipSubcategorymasterModel pojo) {
		// TODO Auto-generated method stub

		Map<String, Object> mapObj = new HashMap();
		TeqipSubcategorymaster subCategory = new TeqipSubcategorymaster();
		if (pojo.getTeqipCategorymasterId() != null) {
			subCategory.setTeqipCategorymaster(categoryRepository.findpmsscatbyid(pojo.getTeqipCategorymasterId()));
		}
		subCategory.setSubCategoryName(pojo.getSubCategoryName());
		subCategory.setIsactive(true);
		TeqipSubcategorymaster obj = teqipSubcategorymasterRepository.save(subCategory);
		TeqipSubcategorymasterModel subcat = new TeqipSubcategorymasterModel();
		TeqipCategorymasterModel master = new TeqipCategorymasterModel();
		master.setCatId(obj.getTeqipCategorymaster().getCatId());
		master.setCategoryName(obj.getTeqipCategorymaster().getCategoryName());
		subcat.setCatmastermodel(master);
		subcat.setSubcatId(obj.getSubcatId());
		subcat.setSubCategoryName(obj.getSubCategoryName());

		try {
			if (obj != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", subcat);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}

		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;

	}

	public List getItemName() {
		String[] list = ItemRepository.getItemName();
		ArrayList al = new ArrayList();
		for (String ab : list) {
			Map<String, Object> map = new HashMap();
			map.put("label", ab);

			al.add(map);
		}
		return al;

	}

	public Map<String, Object> findItemMasterByActiveStatus(String itemname) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		ArrayList itemMasterlist = new ArrayList();
		Boolean activeStatus = true;

		List<Object[]> itemMasterlists = ItemRepository.findItemMasterByActiveStatus(activeStatus, itemname);
		for (Object[] item : itemMasterlists) {
			TeqipItemMasterModel model = new TeqipItemMasterModel();
			model.setItemId((Integer) item[0]);
			model.setItemName((String) item[1]);
			model.setDescription((String) item[2]);
			model.setCatname((String) item[3]);
			model.setSubcatname((String) item[4]);
			model.setTeqipCategorymasterId((Integer) item[5]);
			model.setTeqipSubcategorymasterId((Integer) item[6]);
			itemMasterlist.add(model);

		}

		if (itemMasterlist != null) {
			map.put("itemMasterList", itemMasterlist);
			// map.put("subCategoryList", list);
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.OK);

		} else {
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.BAD_REQUEST);
		}
		return map;

	}

	public Map<String, Object> findItemMasterByCatId(Integer id, Integer subcatid) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		ArrayList itemMasterlist = new ArrayList();
		Boolean activeStatus = true;

		List<Object[]> itemMasterlists = ItemRepository.findItemMasterByCatId(activeStatus, id, subcatid);
		for (Object[] item : itemMasterlists) {
			TeqipItemMasterModel model = new TeqipItemMasterModel();
			model.setItemId((Integer) item[0]);
			model.setItemName((String) item[1]);
			model.setDescription((String) item[2]);
			model.setCatname((String) item[3]);
			model.setSubcatname((String) item[4]);
			model.setTeqipCategorymasterId((Integer) item[5]);
			model.setTeqipSubcategorymasterId((Integer) item[6]);
			itemMasterlist.add(model);

		}

		if (itemMasterlist != null) {
			map.put("itemMasterList", itemMasterlist);
			// map.put("subCategoryList", list);
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.OK);

		} else {
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.BAD_REQUEST);
		}
		return map;

	}

	public Map<String, Object> addItemMaster(TeqipItemMasterModel item) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		LoginUser claim = LoggedUser.get();
		Date date = new Date(0);
		try {
			TeqipItemMaster obj = new TeqipItemMaster();
			obj.setItemName(item.getItemName());
			obj.setDescription(item.getDescription());
			obj.setModifyDate(date);
			obj.setStatus(true);
			if (item.getTeqipCategorymasterId() != null) {
				TeqipCategorymaster categoryMaster = categoryRepository
						.findCategoryMasterById(item.getTeqipCategorymasterId());
				TeqipCategorymaster master = new TeqipCategorymaster();
				master.setCatId(categoryMaster.getCatId());
				obj.setTeqipCategorymaster(master);
			}
			if (item.getTeqipSubcategorymasterId() != null) {
				TeqipSubcategorymaster subcategorymaster = teqipSubcategorymasterRepository
						.findOne(item.getTeqipSubcategorymasterId());
				TeqipSubcategorymaster sub = new TeqipSubcategorymaster();
				sub.setSubcatId(subcategorymaster.getSubcatId());
				obj.setTeqipSubcategorymaster(sub);
			}
			obj.setCreatedBy(claim.getUserid());
			TeqipItemMaster response = ItemRepository.save(obj);
			if (response != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", response);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;

	}

	public ArrayList getSubCategoryMasterDataByCatId(Integer catId) {
		// TODO Auto-generated method stub

		Map<String, Object> map = new LinkedHashMap<>();
		Boolean activeStatus = true;
		ArrayList al = new ArrayList();
		List<TeqipSubcategorymaster> subCategoryList = teqipSubcategorymasterRepository.findSubCategory(catId,
				activeStatus);
		for (TeqipSubcategorymaster ab : subCategoryList) {
			// subCategoryList=null;
			Map<String, Object> map1 = new HashMap();
			map1.put("subcatId", ab.getSubcatId());
			map1.put("subcateName", ab.getSubCategoryName());
			al.add(map1);
		}
		// if (subCategoryList != null) {
		// map.put("subCategoryList", subCategoryList);
		// map.put("TOKEN", "ABC@123");
		// map.put("Status", HttpStatus.OK);
		//
		// } else {
		// map.put("TOKEN", "ABC@123");
		// map.put("Status", HttpStatus.BAD_REQUEST);
		// }

		return al;
	}

	public Map<String, Object> updateItem(TeqipItemMasterModel item) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		Date date = new Date(0);
		try {
			TeqipItemMaster obj = ItemRepository.findOne(item.getItemId());
			obj.setItemName(item.getItemName());
			obj.setDescription(item.getDescription());
			obj.setModifyDate(date);
			obj.setStatus(true);
			if (item.getTeqipCategorymasterId() != null) {
				TeqipCategorymaster categoryMaster = categoryRepository
						.findCategoryMasterById(item.getTeqipCategorymasterId());
				TeqipCategorymaster master = new TeqipCategorymaster();
				master.setCatId(categoryMaster.getCatId());
				obj.setTeqipCategorymaster(master);

			}
			if (item.getTeqipSubcategorymasterId() != null) {
				TeqipSubcategorymaster subcategorymaster = teqipSubcategorymasterRepository
						.findOne(item.getTeqipSubcategorymasterId());
				TeqipSubcategorymaster sub = new TeqipSubcategorymaster();
				sub.setSubcatId(subcategorymaster.getSubcatId());
				obj.setTeqipSubcategorymaster(sub);

			}
			obj.setCreatedBy(2);
			TeqipItemMaster response = ItemRepository.save(obj);
			if (response != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", response);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;

	}

	public List<DepartmentMaster> findAlltDepartments() {
		// TODO Auto-generated method stub
		List<DepartmentMaster> deptmaster = departmentMasterRepo.findAllDept();
		return deptmaster;
	}

	public TeqipPmssDepartmentMaster saveDept(TeqipDepartmentModel department) {
		// TODO Auto-generated method stub
		TeqipPmssDepartmentMaster dept = new TeqipPmssDepartmentMaster();

		TeqipPmssDepartmentMaster deptmaster = departmentMasterRepository.save(dept);
		return deptmaster;
	}

	public Map<String, Object> findinstituteAlldata(HttpServletRequest request) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap<>();
		LoginUser claim = (LoginUser) request.getAttribute("claim");
		String role = claim.getPmssrole();
		if (npiu.equals(role)) {
			ArrayList<TeqipInstitutionPojo> institutelist = new ArrayList();
			Boolean status = true;
			List<Object[]> institute = teqipInstitutionRepository.getActiveInstituteData(status);
			// List<TeqipStateMaster> stateMasterList =
			// stateRepository.findAll();
			// List<InstitutionType> instituteTypelist =
			// procurmentPlanService.getInstitutionType();

			if (institute.size() >= 1) {
				for (Object obj[] : institute) {
					TeqipInstitutionPojo inst = new TeqipInstitutionPojo();
					inst.setState((String) obj[0]);
					inst.setInstitutetypeName((String) obj[1]);
					inst.setInstitutionId((Integer) obj[2]);
					inst.setInstitutionName((String) obj[3]);
					inst.setSpfuid((Integer) obj[4]);
					inst.setWebsiteurl((String) obj[5]);
					inst.setAllocatedbuget((Double) obj[6]);
					inst.setFaxnumber((String) obj[7]);
					inst.setTelephonenumber((String) obj[8]);
					inst.setEmailid((String) obj[9]);
					inst.setAddress((String) obj[10]);
					inst.setInstitutiontype((Integer) obj[12]);
					inst.setInstitutionCode((String) obj[11]);
					institutelist.add(inst);
				}
				map.put("institutelist", institutelist);
				// map.put("stateMasterList", stateMasterList);
				// map.put("instituteTypelist", instituteTypelist);
				map.put("TOKEN", "ABC@123");
				map.put("Status", HttpStatus.OK);

			} else {
				map.put("TOKEN", "ABC@123");
				map.put("Status", HttpStatus.BAD_REQUEST);
			}
		} else if (inst.equals(role)) {
			Integer instId = claim.getInstid();
			ArrayList institutelist = new ArrayList();
			TeqipInstitution institute = teqipInstitutionRepository.getInstituteDataById(instId);
			TeqipInstitutionPojo inst = new TeqipInstitutionPojo();
			inst.setSpfuid(institute.getTeqipStatemaster().getStateId());
			inst.setInstitutiontype(institute.getTeqipInstitutiontype().getInstitutiontypeId());
			inst.setInstitutionId(institute.getInstitutionId());
			inst.setInstitutionCode(institute.getInstitutionCode());
			inst.setAddress(institute.getAddress());
			inst.setAllocatedbuget(institute.getAllocatedbuget());
			inst.setEmailid(institute.getEmailid());
			inst.setFaxnumber(institute.getFaxnumber());
			inst.setInstitutionName(institute.getInstitutionName());
			inst.setWebsiteurl(institute.getWebsiteurl());
			inst.setTelephonenumber(institute.getTelephonenumber());
			inst.setInstitutetypeName(institute.getTeqipInstitutiontype().getInstitutiontypeName());
			if (institute.getTeqipStatemaster().getStateName() != null) {
				inst.setState(institute.getTeqipStatemaster().getStateName());
			}
			institutelist.add(inst);

			if (institutelist != null) {
				map.put("institutelist", institutelist);
				map.put("TOKEN", "ABC@123");
				map.put("Status", HttpStatus.OK);

			} else {
				map.put("TOKEN", "ABC@123");
				map.put("Status", HttpStatus.BAD_REQUEST);
			}
		}
		if (spfu.equals(role) || spa.equals(role)) {
			Integer stId = claim.getStateid();
			ArrayList institutelist = new ArrayList();
			List<TeqipInstitution> institutelists = teqipInstitutionRepository.getInstByStId(stId);
			for (TeqipInstitution institute : institutelists) {
				TeqipInstitutionPojo inst = new TeqipInstitutionPojo();

				inst.setInstitutionId(institute.getInstitutionId());
				inst.setInstitutionCode(institute.getInstitutionCode());
				inst.setAddress(institute.getAddress());
				inst.setAllocatedbuget(institute.getAllocatedbuget());
				inst.setEmailid(institute.getEmailid());
				inst.setFaxnumber(institute.getFaxnumber());
				inst.setInstitutionName(institute.getInstitutionName());
				inst.setWebsiteurl(institute.getWebsiteurl());
				inst.setTelephonenumber(institute.getTelephonenumber());
				inst.setInstitutetypeName(institute.getTeqipInstitutiontype().getInstitutiontypeName());
				if (institute.getTeqipStatemaster().getStateName() != null) {
					inst.setState(institute.getTeqipStatemaster().getStateName());
				}
				institutelist.add(inst);

			}
			if (institutelist != null) {
				map.put("institutelist", institutelist);
				// map.put("stateMasterList", stateMasterList);
				// map.put("instituteTypelist", instituteTypelist);
				map.put("TOKEN", "ABC@123");
				map.put("Status", HttpStatus.OK);

			} else {
				map.put("TOKEN", "ABC@123");
				map.put("Status", HttpStatus.BAD_REQUEST);
			}
		}

		return map;
	}

	public Map<String, Object> addInstitution(HttpServletRequest request, TeqipInstitutionPojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		LoginUser claim = (LoginUser) request.getAttribute("claim");

		try {
			TeqipInstitution obj = new TeqipInstitution();
			obj.setInstitutionCode(pojo.getInstitutionCode());
			obj.setAddress(pojo.getAddress());
			obj.setAllocatedbuget(pojo.getAllocatedbuget());
			obj.setEmailid(pojo.getEmailid());
			obj.setFaxnumber(pojo.getFaxnumber());
			obj.setInstitutionName(pojo.getInstitutionName());

			obj.setWebsiteurl(pojo.getWebsiteurl());
			obj.setTelephonenumber(pojo.getTelephonenumber());
			obj.setCreatedBy(claim.getUserid());
			if (pojo.getTeqipStatemaster() != null) {
				TeqipStatemaster stateMaster = teqipStatemasteryRepository.findOne(pojo.getTeqipStatemaster());
				TeqipStatemaster master = new TeqipStatemaster();
				master.setStateId(stateMaster.getStateId());
				obj.setTeqipStatemaster(master);
				obj.setState(stateMaster.getStateName());
			}
			if (pojo.getInstitutiontype() != null) {
				TeqipInstitutiontype institutiontype = institutionTypeRepository.findOne(pojo.getInstitutiontype());
				TeqipInstitutiontype type = new TeqipInstitutiontype();
				type.setInstitutiontypeId(institutiontype.getInstitutiontypeId());
				obj.setTeqipInstitutiontype(type);
			}
			TeqipInstitution response = teqipInstitutionRepository.save(obj);
			if (response != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", response);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;
	}

	public Map<String, Object> updateInstitution(TeqipInstitutionPojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		LoginUser claim = LoggedUser.get();
		try {
			TeqipInstitution obj = teqipInstitutionRepository.findOne(pojo.getInstitutionId());
			if (obj != null) {
				obj.setInstitutionCode(pojo.getInstitutionCode());
				obj.setAddress(pojo.getAddress());
				obj.setAllocatedbuget(pojo.getAllocatedbuget());
				obj.setEmailid(pojo.getEmailid());
				obj.setFaxnumber(pojo.getFaxnumber());
				obj.setInstitutionName(pojo.getInstitutionName());
				// obj.setState(pojo.getState());
				obj.setWebsiteurl(pojo.getWebsiteurl());
				obj.setTelephonenumber(pojo.getTelephonenumber());
				obj.setCreatedBy(2);

				if (pojo.getTeqipStatemaster() != null) {
					TeqipStatemaster stateMaster = teqipStatemasteryRepository.findOne(pojo.getTeqipStatemaster());
					obj.setTeqipStatemaster(stateMaster);
				}
				if (pojo.getInstitutiontype() != null) {
					TeqipInstitutiontype institutiontype = institutionTypeRepository.findOne(pojo.getInstitutiontype());
					obj.setTeqipInstitutiontype(institutiontype);
				}

				TeqipInstitution responses = teqipInstitutionRepository.save(obj);
				TeqipInstitutionPojo response = new TeqipInstitutionPojo();
				response.setInstitutionCode(responses.getInstitutionCode());
				response.setAddress(responses.getAddress());
				response.setAllocatedbuget(responses.getAllocatedbuget());
				response.setEmailid(responses.getEmailid());
				response.setFaxnumber(responses.getFaxnumber());
				response.setInstitutionName(responses.getInstitutionName());
				response.setState(responses.getTeqipStatemaster().getStateName());
				response.setWebsiteurl(responses.getWebsiteurl());
				response.setTelephonenumber(responses.getTelephonenumber());

				if (response != null) {
					mapObj.put("TOKEN", "ABC@123");
					mapObj.put("RESULT", "SUCESS");
					mapObj.put("DATA", response);

				} else {
					mapObj.put("TOKEN", "ABC@123");
					mapObj.put("RESULT", "FAIL");
				}
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;

	}

	public Map<String, Object> getInstituteInfo() {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList stateMasterList = new ArrayList();
		ArrayList instituteTypelist = new ArrayList();
		List<TeqipStatemaster> stateMasterLists = teqipStatemasteryRepository.findAll();
		for (TeqipStatemaster state : stateMasterLists) {
			TeqipStatemastermodel model = new TeqipStatemastermodel();
			model.setStateCode(state.getStateCode());
			model.setStateName(state.getStateName());
			model.setStateId(state.getStateId());
			model.setAddress(state.getAddress());
			model.setFaxNumber(state.getFaxNumber());
			model.setPhoneNumber(state.getPhoneNumber());
			model.setEmailID(state.getEmailID());
			model.setIsactive(state.getIsactive());
			stateMasterList.add(model);
		}
		List<TeqipInstitutiontype> instituteTypelists = institutionTypeRepository.findAll();
		for (TeqipInstitutiontype instype : instituteTypelists) {
			TeqipInstitutionTypeModel model = new TeqipInstitutionTypeModel();
			model.setInstitutiontypeId(instype.getInstitutiontypeId());
			model.setInstitutiontypeName(instype.getInstitutiontypeName());
			model.setIsactive(instype.getIsactive());
			instituteTypelist.add(model);

		}
		if (stateMasterList != null) {
			map.put("stateMasterList", stateMasterList);
			map.put("instituteTypelist", instituteTypelist);
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.OK);

		} else {
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.BAD_REQUEST);
		}
		return map;
	}

	public Map<String, Object> getInstituteTypeById(Integer id) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap();
		List<TeqipInstitution> Obj = teqipInstitutionRepository.findIstByTypeId(id);
		if (Obj != null) {
			map.put("data", Obj);
			map.put("Status", "OK");
			map.put("token", "ABC@123");
		} else {
			map.put("Status", "Fail");
			map.put("token", "ABC@123");
		}
		return map;
	}

	public Map<String, Object> getInstituteDataByStateId(Integer id) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap();
		List<TeqipInstitution> Obj = teqipInstitutionRepository.getInstituteDataByStateId(id);
		if (Obj.size() >= 1) {

			map.put("data", Obj);
			map.put("Status", "OK");
			map.put("token", "ABC@123");
		} else {
			map.put("Status", "Fail");
			map.put("token", "ABC@123");
		}
		return map;
	}

	public Map<String, Object> getInstitutesDataByStateId(Integer id) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap();
		List<Object[]> list = teqipInstitutionRepository.getInstitutesDataByStateId(id);
		if (list == null) {
			return null;
		}
		ArrayList institutelist = new ArrayList();
		for (Object[] obj : list) {
			TeqipInstitutionPojo inst = new TeqipInstitutionPojo();
			inst.setInstitutetypeName((String) obj[0]);
			inst.setInstitutionId((Integer) obj[1]);
			inst.setInstitutionName((String) obj[2]);
			inst.setState((String) obj[3]);
			inst.setSpfu((Integer) obj[4]);
			inst.setWebsiteurl((String) obj[5]);
			inst.setAllocatedbuget((Double) obj[6]);
			inst.setFaxnumber((String) obj[7]);
			inst.setTelephonenumber((String) obj[8]);
			inst.setEmailid((String) obj[9]);
			inst.setAddress((String) obj[10]);
			inst.setInstitutiontype((Integer) obj[12]);
			inst.setInstitutionCode((String) obj[11]);
			institutelist.add(inst);

			map.put("data", institutelist);
			map.put("Status", "OK");
		}
		return map;
	}

	public List<TeqipInstitutionsubcomponentmappingPojo> getTeqipInstitutionsubcomponentByInstId(Integer id)
			throws Exception {

		Map<String, Object> map = new LinkedHashMap();
		Map<String, Object> deptMap = new LinkedHashMap<>();

		List<Object[]> objList = procurmentPlanDaoImp.getInstitutionsubcomponentByInstId(id);
		List<TeqipInstitutionsubcomponentmappingPojo> mylist = new ArrayList<>();
		if (objList.size() >= 1) {
			for (Object[] obj : objList) {
				TeqipInstitutionsubcomponentmappingPojo objDept = new TeqipInstitutionsubcomponentmappingPojo();
				objDept.setId((Integer) obj[0]);
				objDept.setIsactive((Byte) obj[1]);
				objDept.setDeptName((String) obj[2]);
				objDept.setInstitutionSubcomponentMapping_ID((Integer) obj[3]);
				objDept.setInstituteid((Integer) obj[4]);
				objDept.setSpfuid((Integer) obj[5]);
				objDept.setAllocatedbudget((Double) obj[6]);
				objDept.setBudgetforcw((Double) obj[7]);
				objDept.setBudgetforservices((Double) obj[8]);
				objDept.setBudgetforprocuremen((Double) obj[9]);
				objDept.setSubcomponentcode((String) obj[10]);
				mylist.add(objDept);
			}

		}
		// map.put("data", mylist);
		return mylist;
	}

	public List<TeqipPmssInstitutionpurchasecommittePojo> getStatePurchaseCommitteByStateId(Integer id)
			throws Exception {
		List<Object[]> objList = procurmentPlanDaoImp.getAllPurchaseByStateId(id);
		List<TeqipPmssInstitutionpurchasecommittePojo> mylist = new ArrayList<>();
		TeqipPmssInstitutionpurchasecommittePojo ob = new TeqipPmssInstitutionpurchasecommittePojo();

		if (objList.size() > 0 && objList != null) {
			for (Object[] obj : objList) {
				TeqipPmssInstitutionpurchasecommittePojo dept = ob.clone();
				dept.setIsactive((Byte) obj[0]);
				dept.setInstitutionPurchaseCommitte_ID((Integer) obj[1]);
				dept.setPmssdeptName((String) obj[2]);
				dept.setInstituteid((Integer) obj[3]);
				dept.setSpfuid((Integer) obj[4]);
				// objDept.setInstitutionPurchaseCommitte_ID((Integer)obj[3]);
				dept.setCommitteMemberName((String) obj[5]);
				dept.setDesignation((String) obj[6]);
				dept.setPCommitteRole((String) obj[7]);
				dept.setDeptId((Integer) obj[8]);
				mylist.add(dept);
			}

		}
		return mylist;
	}

	public List<TeqipPmssInstitutionpurchasecommittePojo> getPurchaseCommitteForNpc() throws Exception {
		List<Object[]> objList = procurmentPlanDaoImp.getPurchaseDataForNpc();
		List<TeqipPmssInstitutionpurchasecommittePojo> mylist = new ArrayList<>();
		TeqipPmssInstitutionpurchasecommittePojo ob = new TeqipPmssInstitutionpurchasecommittePojo();

		if (objList.size() > 0 && objList != null) {
			for (Object[] obj : objList) {
				TeqipPmssInstitutionpurchasecommittePojo dept = ob.clone();
				dept.setIsactive((Byte) obj[0]);
				dept.setInstitutionPurchaseCommitte_ID((Integer) obj[1]);
				dept.setPmssdeptName((String) obj[2]);
				dept.setInstituteid((Integer) obj[3]);
				dept.setSpfuid((Integer) obj[4]);
				// objDept.setInstitutionPurchaseCommitte_ID((Integer)obj[3]);
				dept.setCommitteMemberName((String) obj[5]);
				dept.setDesignation((String) obj[6]);
				dept.setPCommitteRole((String) obj[7]);
				dept.setDeptId((Integer) obj[8]);
				mylist.add(dept);
			}

		}
		return mylist;
	}

	public List<TeqipPmssInstitutionpurchasecommittePojo> getPurchaseCommitteForEdcil() throws Exception {
		List<Object[]> objList = procurmentPlanDaoImp.getPurchaseDataForEdcil();
		List<TeqipPmssInstitutionpurchasecommittePojo> mylist = new ArrayList<>();
		TeqipPmssInstitutionpurchasecommittePojo ob = new TeqipPmssInstitutionpurchasecommittePojo();

		if (objList.size() > 0 && objList != null) {
			for (Object[] obj : objList) {
				TeqipPmssInstitutionpurchasecommittePojo dept = ob.clone();
				dept.setIsactive((Byte) obj[0]);
				dept.setInstitutionPurchaseCommitte_ID((Integer) obj[1]);
				dept.setPmssdeptName((String) obj[2]);
				dept.setInstituteid((Integer) obj[3]);
				dept.setSpfuid((Integer) obj[4]);
				// objDept.setInstitutionPurchaseCommitte_ID((Integer)obj[3]);
				dept.setCommitteMemberName((String) obj[5]);
				dept.setDesignation((String) obj[6]);
				dept.setPCommitteRole((String) obj[7]);
				dept.setDeptId((Integer) obj[8]);
				mylist.add(dept);
			}

		}
		return mylist;
	}

	public List<TeqipPmssInstitutionpurchasecommittePojo> getInstitutionPurchaseCommitteByInstId(Integer id)
			throws Exception {

		Map<String, Object> map = new LinkedHashMap();
		Map<String, Object> deptMap = new LinkedHashMap<>();

		List<Object[]> objList = procurmentPlanDaoImp.getInstitutionPurchaseCommitteByInstId(id);
		List<TeqipPmssInstitutionpurchasecommittePojo> mylist = new ArrayList<>();
		if (objList.size() >= 1) {
			for (Object[] obj : objList) {
				TeqipPmssInstitutionpurchasecommittePojo objDept = new TeqipPmssInstitutionpurchasecommittePojo();
				objDept.setIsactive((Byte) obj[0]);
				objDept.setInstitutionPurchaseCommitte_ID((Integer) obj[1]);
				objDept.setPmssdeptName((String) obj[2]);
				objDept.setInstituteid((Integer) obj[3]);
				objDept.setSpfuid((Integer) obj[4]);
				// objDept.setInstitutionPurchaseCommitte_ID((Integer)obj[3]);
				objDept.setCommitteMemberName((String) obj[5]);
				objDept.setDesignation((String) obj[6]);
				objDept.setPCommitteRole((String) obj[7]);
				mylist.add(objDept);
			}

		}
		// map.put("data", mylist);
		return mylist;
	}

	public List<TeqipPmssInstitutionlogo> getInstituteLogoByInstId(Integer id) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap();
		Map<String, Object> deptMap = new LinkedHashMap<>();
		List<Object[]> objList = procurmentPlanDaoImp.getInstituteLogoByInstituteId(id);
		List<TeqipPmssInstitutionlogo> mylist = new ArrayList<>();
		if (objList.size() >= 1) {
			for (Object[] obj : objList) {
				TeqipPmssInstitutionlogo objDept = new TeqipPmssInstitutionlogo();
				objDept.setInstitutionLogoID((Integer) obj[0]);
				// objDept.setIsactive((Boolean) obj[4]);
				objDept.setOriginalFileName((String) obj[1]);
				objDept.setDescription((String) obj[2]);
				objDept.setCreatedOn((Timestamp) obj[3]);
				mylist.add(objDept);
			}

		}
		return mylist;

	}

	public List<TeqipPmssInstitutionlogo> getInstituteLogoByStateId(Integer id) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap();
		Map<String, Object> deptMap = new LinkedHashMap<>();
		List<Object[]> objList = procurmentPlanDaoImp.getAllInstituteLogoByStateId(id);
		List<TeqipPmssInstitutionlogo> mylist = new ArrayList<>();
		if (objList.size() >= 1) {
			for (Object[] obj : objList) {
				TeqipPmssInstitutionlogo objDept = new TeqipPmssInstitutionlogo();
				objDept.setInstitutionLogoID((Integer) obj[0]);
				// objDept.setIsactive((Boolean) obj[4]);
				objDept.setOriginalFileName((String) obj[1]);
				objDept.setDescription((String) obj[2]);
				objDept.setCreatedOn((Timestamp) obj[3]);
				mylist.add(objDept);
			}

		}
		return mylist;

	}

	public List<TeqipPmssInstitutionlogo> getAllLogoForNpc(Integer id) {
		return procurmentPlanDaoImp.getAllLogoForNpc(id).stream().map(obj -> {
			TeqipPmssInstitutionlogo objDept = new TeqipPmssInstitutionlogo();
			objDept.setInstitutionLogoID((Integer) obj[0]);
			// objDept.setIsactive((Boolean) obj[4]);
			objDept.setOriginalFileName((String) obj[1]);
			objDept.setDescription((String) obj[2]);
			objDept.setCreatedOn((Timestamp) obj[3]);
			return objDept;
		}).collect(Collectors.toList());

	}

	public List<TeqipUsersDetail> findAllUserData() {
		// TODO Auto-generated method stub
		return teqipUserDetailsRepository.findAll();
	}

	public List<TeqipRolemaster> findAll() {
		// TODO Auto-generated method stub
		return roleMasterRepository.findAll();
	}

	public Map<String, Object> findAllinstandSt() {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList institutelist = new ArrayList();
		ArrayList stateMasterList = new ArrayList();
		List<TeqipInstitution> institutelists = teqipInstitutionRepository.findAll();
		for (TeqipInstitution inst : institutelists) {
			TeqipInstitutionPojo model = new TeqipInstitutionPojo();
			model.setInstitutionId(inst.getInstitutionId());
			if (inst.getTeqipStatemaster() != null) {
				model.setSpfuid(inst.getTeqipStatemaster().getStateId());
				model.setState(inst.getTeqipStatemaster().getStateName());
			}
			model.setInstitutionName(inst.getInstitutionName());
			institutelist.add(model);
		}
		List<TeqipStatemaster> stateMasterLists = teqipStatemasteryRepository.findAll();
		for (TeqipStatemaster master : stateMasterLists) {
			TeqipStatemastermodel model = new TeqipStatemastermodel();
			model.setStateId(master.getStateId());
			model.setStateName(master.getStateName());
			stateMasterList.add(model);
		}
		// List<InstitutionType> instituteTypelist =
		// procurmentPlanService.getInstitutionType();

		if (institutelist != null) {
			map.put("institutelist", institutelist);
			map.put("stateMasterList", stateMasterList);
			// map.put("instituteTypelist", instituteTypelist);
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.OK);

		} else {
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.BAD_REQUEST);
		}
		return map;
	}

	public Map<String, Object> addinstpurchasecommittee(TeqipInstitutionsubcomponentmappingPojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new HashMap();
		LoginUser claim = LoggedUser.get();
		TeqipInstitutionsubcomponentmapping model = new TeqipInstitutionsubcomponentmapping();
		model.setAllocatedtype(pojo.getAllcatedtype());
		model.setAllocatedbudget(pojo.getAllocatedbudget());
		model.setBudgetforcw(pojo.getBudgetforcw());
		model.setBudgetforprocuremen(pojo.getBudgetforprocuremen());
		model.setBudgetforservices(pojo.getBudgetforservices());
		model.setIsactive(true);
		model.setCreatedBy(claim.getUserid());
		model.setModifyBy(claim.getUserid());
		if (pojo.getId() != null) {
			TeqipSubcomponentmaster subComponent = teqipSubcomponentmasterRepository.findOne(pojo.getId());
			TeqipSubcomponentmaster submodel = new TeqipSubcomponentmaster();
			submodel.setId(subComponent.getId());
			model.setTeqipSubcomponentmaster(submodel);
		}
		if (pojo.getInstituteid() != null) {
			TeqipInstitution teqipInstitution = teqipInstitutionRepository.findOne(pojo.getInstituteid());
			TeqipInstitution inst = new TeqipInstitution();
			inst.setInstitutionId(teqipInstitution.getInstitutionId());
			model.setTeqipInstitution(inst);
		}
		if (pojo.getSpfuid() != null) {
			TeqipStatemaster teqipStatemaster = teqipStatemasteryRepository.findOne(pojo.getSpfuid());
			TeqipStatemaster st = new TeqipStatemaster();
			st.setStateId(teqipStatemaster.getStateId());
			model.setTeqipStatemaster(st);
		}
		if (pojo.getSpfuid() == null && pojo.getInstituteid() == null) {
			model.setType(pojo.getTypeNpcEdcil());
		}
		TeqipInstitutionsubcomponentmapping insSub = teqipInstitutionsubcomponentmappingRepository.save(model);
		try {
			if (insSub != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", insSub);
			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;
	}

	public Map<String, Object> updateInstSubcomponenet(TeqipInstitutionsubcomponentmappingPojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new HashMap();
		LoginUser claim = LoggedUser.get();
		TeqipInstitutionsubcomponentmapping model = teqipInstitutionsubcomponentmappingRepository
				.findOne(pojo.getInstitutionSubcomponentMapping_ID());
		model.setAllocatedtype(pojo.getAllcatedtype());
		model.setAllocatedbudget(pojo.getAllocatedbudget());
		model.setBudgetforcw(pojo.getBudgetforcw());
		model.setBudgetforprocuremen(pojo.getBudgetforprocuremen());
		model.setBudgetforservices(pojo.getBudgetforservices());
		model.setIsactive(true);
		model.setModifyBy(claim.getUserid());
		// TeqipInstitution inst = new TeqipInstitution();
		// inst.setInstitutionId(model.getTeqipInstitution().getInstitutionId());
		// model.setTeqipInstitution(inst);
		// TeqipStatemaster master = new TeqipStatemaster();
		// master.setStateId(model.getTeqipStatemaster().getStateId());
		// model.setTeqipStatemaster(master);
		// TeqipSubcomponentmaster submaster = new TeqipSubcomponentmaster();
		// submaster.setId(model.getTeqipSubcomponentmaster().getId());
		// model.setTeqipSubcomponentmaster(submaster);
		TeqipInstitutionsubcomponentmapping insSubs = teqipInstitutionsubcomponentmappingRepository.save(model);
		TeqipInstitutionsubcomponentmapping insSub = new TeqipInstitutionsubcomponentmapping();
		insSub.setAllocatedbudget(insSubs.getAllocatedbudget());
		insSub.setBudgetforcw(insSubs.getBudgetforcw());
		insSub.setBudgetforprocuremen(insSubs.getBudgetforprocuremen());
		insSub.setBudgetforservices(insSubs.getBudgetforservices());
		insSub.setIsactive(true);

		try {
			if (insSub != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", insSub);
			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;
	}

	public ResponseEntity<TeqipInstitutionsubcomponentmapping> saveInsSubComponenet(
			Integer institutionSubcomponentMapping_ID) {
		// TODO Auto-generated method stub
		TeqipInstitutionsubcomponentmapping model = teqipInstitutionsubcomponentmappingRepository
				.findOne(institutionSubcomponentMapping_ID);
		if (model != null) {
			model.setIsactive(false);
			TeqipInstitutionsubcomponentmapping insSub = teqipInstitutionsubcomponentmappingRepository.save(model);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}

	public Map<String, Object> addnewinstdept(TeqipInstitutiondepartmentmappingModel pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new HashMap();
		LoginUser claim = LoggedUser.get();
		TeqipInstitutiondepartmentmapping model = new TeqipInstitutiondepartmentmapping();
		model.setDepartmentcode(pojo.getDepartmentcode());
		model.setDepartmentHead(pojo.getDepartmenthead());
		model.setIsactive(true);
		model.setCreatedBy(claim.getUserid());
		model.setModifyBy(claim.getUserid());
		if (pojo.getDepartmentmasterid() != null) {
			TeqipPmssDepartmentMaster teqipPmssdepartmentMaster = departmentMasterRepository
					.findOne(pojo.getDepartmentmasterid());
			TeqipPmssDepartmentMaster dept = new TeqipPmssDepartmentMaster();
			dept.setDepartmentId(teqipPmssdepartmentMaster.getDepartmentId());
			model.setTeqipPmssdepartmentmaster(dept);
		}
		if (pojo.getTeqipInstitutionId() != null && pojo.getType().equals("institute")) {
			TeqipInstitution teqipInstitution = teqipInstitutionRepository.findOne(pojo.getTeqipInstitutionId());
			TeqipInstitution inst = new TeqipInstitution();
			inst.setInstitutionId(teqipInstitution.getInstitutionId());
			model.setTeqipInstitution(inst);
		} else if (pojo.getSpfuid() != null && pojo.getType().equals("state")) {
			TeqipStatemaster state = teqipStatemasteryRepository.findOne(pojo.getSpfuid());
			TeqipStatemaster state1 = new TeqipStatemaster();
			state1.setStateId(state.getStateId());
			model.setTeqipStatemaster(state1);
			// model.setTeqipInstitution(null);
		} else if (pojo.getTeqipInstitutionId() == null && pojo.getSpfuid() == null) {
			model.setTypeNpcEdcil(pojo.getTypeNpcEdcil());
		}
		TeqipInstitutiondepartmentmapping insdept = teqipInstitutiondepartmentmappingRepository.save(model);
		try {
			if (insdept != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("sucess", insdept);
			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;

	}

	public Map<String, Object> updateInstDept(TeqipInstitutiondepartmentmappingModel pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new HashMap();
		LoginUser claim = LoggedUser.get();
		TeqipInstitutiondepartmentmapping obj = teqipInstitutiondepartmentmappingRepository
				.findOne(pojo.getInstitutionDepartmentMapping_ID());
		TeqipInstitutiondepartmentmapping mapping = new TeqipInstitutiondepartmentmapping();
		mapping.setDepartmentcode(pojo.getDepartmentcode());
		mapping.setDepartmentHead(pojo.getDepartmenthead());
		mapping.setIsactive(true);
		mapping.setInstitutionDepartmentMapping_ID(pojo.getInstitutionDepartmentMapping_ID());
		mapping.setModifyBy(claim.getUserid());
		if (pojo.getType().equalsIgnoreCase("institute")) {
			TeqipInstitution inst = new TeqipInstitution();
			inst.setInstitutionId(obj.getTeqipInstitution().getInstitutionId());
			mapping.setTeqipInstitution(inst);
		} else if (pojo.getType().equalsIgnoreCase("state")) {
			TeqipStatemaster state = teqipStatemasteryRepository.findOne(pojo.getSpfuid());
			TeqipStatemaster state1 = new TeqipStatemaster();
			state1.setStateId(state.getStateId());
			mapping.setTeqipStatemaster(state1);

		}
		TeqipPmssDepartmentMaster master = new TeqipPmssDepartmentMaster();
		master.setDepartmentId(obj.getTeqipPmssdepartmentmaster().getDepartmentId());
		mapping.setTeqipPmssdepartmentmaster(master);

		TeqipInstitutiondepartmentmapping dept = teqipInstitutiondepartmentmappingRepository.save(mapping);
		try {
			if (dept != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", dept);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;

	}

	public ResponseEntity<TeqipInstitutiondepartmentmapping> desabledept(Integer deptId) {
		// TODO Auto-generated method stub
		TeqipInstitutiondepartmentmapping deptmod = teqipInstitutiondepartmentmappingRepository.findOne(deptId);
		if (deptmod != null) {
			deptmod.setIsactive(false);
			TeqipInstitutiondepartmentmapping dept = teqipInstitutiondepartmentmappingRepository.save(deptmod);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}

	public Map<String, Object> addinstLogo(InstitutionlogoPojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new HashMap();
		TeqipPmssInstitutionlogo obj = new TeqipPmssInstitutionlogo();
		obj.setCreatedOn(pojo.getCreatedOn());
		obj.setCreatedBy(pojo.getCreatedBy());
		obj.setSystemFileName(pojo.getSystemFileName());
		obj.setOriginalFileName(pojo.getOriginalFileName());
		obj.setDescription(pojo.getDescription());
		obj.setIsactice(true);
		if (pojo.getInstituteid() != null) {
			TeqipInstitution teqipInstitution = teqipInstitutionRepository.findOne(pojo.getInstituteid());
			TeqipInstitution inst = new TeqipInstitution();
			inst.setInstitutionId(teqipInstitution.getInstitutionId());
			obj.setTeqipInstitution(inst);
		}
		TeqipPmssInstitutionlogo logo = teqipPmssInstitutionlogoRepository.save(obj);
		try {
			if (logo != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", logo);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;
	}

	public Map<String, Object> updateLogo(InstitutionlogoPojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new HashMap();
		TeqipPmssInstitutionlogo obj = teqipPmssInstitutionlogoRepository.findOne(pojo.getInstitutionLogoID());
		obj.setCreatedOn(pojo.getCreatedOn());
		obj.setSystemFileName(pojo.getOriginalFileName());
		obj.setOriginalFileName(pojo.getOriginalFileName());
		obj.setDescription(pojo.getDescription());
		obj.setIsactice(true);
		TeqipInstitution inst = new TeqipInstitution();
		inst.setInstitutionId(obj.getTeqipInstitution().getInstitutionId());
		obj.setTeqipInstitution(inst);
		TeqipPmssInstitutionlogo logo = teqipPmssInstitutionlogoRepository.save(obj);
		try {
			if (logo != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", logo);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;
	}

	public ResponseEntity<TeqipPmssInstitutionlogo> desableinsLogoById(Integer institutionLogoID) {
		// TODO Auto-generated method stub
		TeqipPmssInstitutionlogo logo = teqipPmssInstitutionlogoRepository.findOne(institutionLogoID);
		if (logo != null) {
			logo.setIsactice(false);
			TeqipPmssInstitutionlogo log = teqipPmssInstitutionlogoRepository.save(logo);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();

	}

	public Map<String, Object> addinstpurchasecommittee(TeqipPmssInstitutionpurchasecommittePojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new HashMap();
		LoginUser claim = LoggedUser.get();
		TeqipPmssInstitutionpurchasecommitte obj = new TeqipPmssInstitutionpurchasecommitte();
		obj.setCommitteMemberName(pojo.getCommitteMemberName());
		obj.setCommitteetype(pojo.getCommitteetype());
		obj.setIsactive(true);
		obj.setCreatedBy(claim.getUserid());
		obj.setModifiedBy(claim.getUserid());
		obj.setCommitteMemberName(pojo.getCommitteMemberName());
		obj.setDesignation(pojo.getDesignation());
		obj.setPCommitteRole(pojo.getPCommitteRole());
		if (pojo.getInstituteid() != null) {
			TeqipInstitution teqipInstitution = teqipInstitutionRepository.findOne(pojo.getInstituteid());
			TeqipInstitution inst = new TeqipInstitution();
			inst.setInstitutionId(teqipInstitution.getInstitutionId());
			obj.setTeqipInstitution(inst);
		}
		if (pojo.getDeptId() != null) {
			TeqipPmssDepartmentMaster teqipPmssdepartmentMaster = departmentMasterRepository.findOne(pojo.getDeptId());
			TeqipPmssDepartmentMaster pmss = new TeqipPmssDepartmentMaster();
			pmss.setDepartmentId(teqipPmssdepartmentMaster.getDepartmentId());
			obj.setTeqipPmssdepartmentmaster(pmss);
		}
		if (pojo.getSpfuid() != null) {
			TeqipStatemaster teqipStatemaster = teqipStatemasteryRepository.findOne(pojo.getSpfuid());
			TeqipStatemaster master = new TeqipStatemaster();
			master.setStateId(teqipStatemaster.getStateId());
			obj.setStatemaster(master);
		} else if (pojo.getSpfuid() == null && pojo.getInstituteid() == null) {
			obj.setType(pojo.getTypeNpcEdcil());
		}

		TeqipPmssInstitutionpurchasecommitte teqipPmssInstitutionpurchasecommitte = teqipPmssInstitutionpurchasecommitteRepository
				.save(obj);
		try {
			if (teqipPmssInstitutionpurchasecommitte != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", teqipPmssInstitutionpurchasecommitte);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;
	}

	public Map<String, Object> updatepurchasecommittee(TeqipPmssInstitutionpurchasecommittePojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new HashMap();
		LoginUser claim = LoggedUser.get();
		TeqipPmssInstitutionpurchasecommitte obj = teqipPmssInstitutionpurchasecommitteRepository
				.findOne(pojo.getInstitutionPurchaseCommitte_ID());
		obj.setCommitteMemberName(pojo.getCommitteMemberName());
		obj.setCommitteetype(pojo.getCommitteetype());
		obj.setIsactive(true);
		obj.setModifiedBy(claim.getUserid());
		obj.setCommitteMemberName(pojo.getCommitteMemberName());
		obj.setDesignation(pojo.getDesignation());
		obj.setPCommitteRole(pojo.getPCommitteRole());
		if (pojo.getInstituteid() != null) {
			TeqipInstitution teqipInstitution = teqipInstitutionRepository.findOne(pojo.getInstituteid());
			TeqipInstitution inst = new TeqipInstitution();
			inst.setInstitutionId(teqipInstitution.getInstitutionId());
			obj.setTeqipInstitution(inst);
		}
		if (pojo.getDeptId() != null) {
			TeqipPmssDepartmentMaster teqipPmssdepartmentMaster = departmentMasterRepository.findOne(pojo.getDeptId());
			TeqipPmssDepartmentMaster pmss = new TeqipPmssDepartmentMaster();
			pmss.setDepartmentId(teqipPmssdepartmentMaster.getDepartmentId());
			obj.setTeqipPmssdepartmentmaster(pmss);
		}
		if (pojo.getSpfuid() != null) {
			TeqipStatemaster teqipStatemaster = teqipStatemasteryRepository.findOne(pojo.getSpfuid());
			TeqipStatemaster master = new TeqipStatemaster();
			master.setStateId(teqipStatemaster.getStateId());
			obj.setStatemaster(master);
		}

		TeqipPmssInstitutionpurchasecommitte teqipPmssInstitutionpurchasecommitte = teqipPmssInstitutionpurchasecommitteRepository
				.save(obj);
		try {
			if (teqipPmssInstitutionpurchasecommitte != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", teqipPmssInstitutionpurchasecommitte);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;
	}

	public ResponseEntity<TeqipPmssInstitutionpurchasecommitte> desablepurchasecommitee(
			Integer institutionPurchaseCommitte_ID) {
		// TODO Auto-generated method stub
		TeqipPmssInstitutionpurchasecommitte pcommitte = teqipPmssInstitutionpurchasecommitteRepository
				.findOne(institutionPurchaseCommitte_ID);
		if (pcommitte != null) {
			pcommitte.setIsactive(false);
			TeqipPmssInstitutionpurchasecommitte teqipPmssInstitutionpurchasecommitte = teqipPmssInstitutionpurchasecommitteRepository
					.save(pcommitte);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}

	public List<TeqipSubcomponentmasterModel> findAllsubcomponent() {
		// TODO Auto-generated method stub
		ArrayList list = new ArrayList();
		List<TeqipSubcomponentmaster> master = teqipSubcomponentmasterRepository.findAll();
		for (TeqipSubcomponentmaster subcomponent : master) {
			TeqipSubcomponentmasterModel model = new TeqipSubcomponentmasterModel();
			model.setId(subcomponent.getId());
			model.setSubcomponentcode(subcomponent.getSubcomponentcode());
			model.setSubComponentName(subcomponent.getSubComponentName());
			list.add(model);
		}
		return list;
	}

	public List<NpiuDetailsMaster> findNpiu() {
		// TODO Auto-generated method stub
		return (List<NpiuDetailsMaster>) npiuDetailsMasterRepo.findAll();

	}

	public Map<String, Object> findNpiuById(Integer code) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList al = new ArrayList();
		List<Object[]> npiuDetailsMaster = npiuDetailsMasterRepo.findNpiuDetails(code);

		if (npiuDetailsMaster != null) {
			for (Object[] obj : npiuDetailsMaster) {
				NpiuDetailsMasterPojo Npiuobj = new NpiuDetailsMasterPojo();
				Npiuobj.setId((Integer) obj[0]);
				Npiuobj.setCode((String) obj[1]);
				Npiuobj.setName((String) obj[2]);
				Npiuobj.setProjectName((String) obj[3]);
				Npiuobj.setCreditNumber((String) obj[4]);
				Npiuobj.setEmailId((String) obj[5]);
				Npiuobj.setAddress((String) obj[6]);
				Npiuobj.setProjectCost((Double) obj[7]);
				Npiuobj.setFaxNumber((String) obj[8]);
				Npiuobj.setAllocatedBudget((Double) obj[9]);
				al.add(Npiuobj);
				// al.addAll(instPurchaseCommitte);

				map.put("sucess", al);
				// map.put("purchaseCommittee", instPurchaseCommitte);

			}
		} else {
			map.put("TOKEN", "ABC@123");
		}

		return map;

	}

	public Map<String, Object> getnpiupurchasecommittee(Integer id) {
		// TODO Auto-generated method stubthrows Exception {
		Map<String, Object> deptMap = new LinkedHashMap<>();
		String code = npiuDetailsMasterRepo.findNpiuCode(id);
		List<Object[]> objList = teqipPmssInstitutionpurchasecommitteRepository.getCommitteDetails(code);
		List<TeqipPmssInstitutionpurchasecommittePojo> mylist = new ArrayList<>();
		if (objList.size() >= 1) {
			for (Object[] obj : objList) {
				TeqipPmssInstitutionpurchasecommittePojo objDept = new TeqipPmssInstitutionpurchasecommittePojo();
				objDept.setIsactive((Byte) obj[0]);
				objDept.setInstitutionPurchaseCommitte_ID((Integer) obj[1]);
				objDept.setCommitteMemberName((String) obj[2]);
				objDept.setDesignation((String) obj[3]);
				objDept.setPCommitteRole((String) obj[4]);
				objDept.setCommitteetype((String) obj[5]);
				mylist.add(objDept);
			}
			deptMap.put("data", mylist);

		}
		return deptMap; // return mylist;
	}

	public List<TeqipRolemaster> findAllRoles() {
		// TODO Auto-generated method stub
		ArrayList<TeqipRolemaster> master = new ArrayList();
		List<TeqipRolemaster> role = roleMasterRepository.findAll();
		for (TeqipRolemaster rolemaster : role) {
			TeqipRolemaster roles = new TeqipRolemaster();
			roles.setRoleId(rolemaster.getRoleId());
			roles.setPmssRole(rolemaster.getPmssRole());
			roles.setRoleDescription(rolemaster.getRoleDescription());
			master.add(roles);
		}
		return master;

	}

	public List<TeqipRolemaster> findAllRolesmaster() {
		// TODO Auto-generated method stub
		return roleMasterRepository.findAll();
	}

	public Map<String, Object> getStateById(HttpServletRequest req) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap<>();
		List<TeqipStatemaster> obj = new ArrayList();
		LoginUser claim = (LoginUser) req.getAttribute("claim");
		String role = claim.getPmssrole();

		if (npiu.equals(role)) {
			List<TeqipStatemaster> stateMasterLists = teqipStatemasteryRepository.findAll();
			ArrayList stateMasterList = new ArrayList();
			for (TeqipStatemaster stateMaster : stateMasterLists) {
				TeqipStatemastermodel model = new TeqipStatemastermodel();
				model.setStateId(stateMaster.getStateId());
				model.setStateCode(stateMaster.getStateCode());
				model.setStateName(stateMaster.getStateName());
				model.setAddress(stateMaster.getAddress());
				model.setEmailID(stateMaster.getEmailID());
				model.setFaxNumber(stateMaster.getFaxNumber());
				model.setPhoneNumber(stateMaster.getPhoneNumber());
				model.setIsactive(stateMaster.getIsactive());
				model.setAllocatedbudget(stateMaster.getAllocatedBudget());
				stateMasterList.add(model);
			}
			if (stateMasterList != null) {
				map.put("Result", "Success");
				map.put("stateMasterList", stateMasterList);
				map.put("Status", HttpStatus.OK);

			} else {
				map.put("Status", HttpStatus.BAD_REQUEST);
			}
		}
		if (spfu.equals(role) || spa.equals(role)) {
			TeqipStatemaster stateMaster = teqipStatemasteryRepository.findOne(claim.getStateid());
			ArrayList stateMasterList = new ArrayList();
			TeqipStatemastermodel model = new TeqipStatemastermodel();
			model.setStateId(stateMaster.getStateId());
			model.setStateCode(stateMaster.getStateCode());
			model.setStateName(stateMaster.getStateName());
			model.setAddress(stateMaster.getAddress());
			model.setEmailID(stateMaster.getEmailID());
			model.setFaxNumber(stateMaster.getFaxNumber());
			model.setPhoneNumber(stateMaster.getPhoneNumber());
			model.setIsactive(stateMaster.getIsactive());
			model.setAllocatedbudget(stateMaster.getAllocatedBudget());
			stateMasterList.add(model);

			if (stateMasterList != null) {
				map.put("Result", "Success");
				map.put("stateMasterList", stateMasterList);
				map.put("Status", HttpStatus.OK);

			} else {

				map.put("Status", HttpStatus.BAD_REQUEST);
			}
		}
		return map;

	}

	public Map<String, Object> getpurchasecommitteeByid(Long id) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap();
		Map<String, Object> deptMap = new LinkedHashMap<>();

		List<Object[]> objList = procurmentPlanDaoImp.getInstitutionPurchaseCommitteBystId(id);
		ArrayList mylist = new ArrayList<>();
		if (objList.size() >= 1) {
			for (Object[] obj : objList) {
				TeqipPmssInstitutionpurchasecommittePojo objDept = new TeqipPmssInstitutionpurchasecommittePojo();
				objDept.setIsactive((Byte) obj[0]);
				objDept.setInstitutionPurchaseCommitte_ID((Integer) obj[1]);
				objDept.setInstituteid((Integer) obj[2]);
				objDept.setSpfuid((Integer) obj[3]);
				objDept.setCommitteMemberName((String) obj[4]);
				objDept.setDesignation((String) obj[5]);
				objDept.setPCommitteRole((String) obj[6]);
				mylist.add(objDept);
			}

		}
		map.put("Succes", mylist);
		return map;
	}

	public Map<String, Object> getAll() {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		ArrayList list = new ArrayList();
		Boolean status = true;
		List<TeqipProcurementmethod> lists = procurementmethodRepository.findProcurementmethod(status);
		for (TeqipProcurementmethod procmethod : lists) {
			TeqipProcurementmethodModel model = new TeqipProcurementmethodModel();
			TeqipCategorymasterModel catmodel = new TeqipCategorymasterModel();
			model.setProcurementmethodCode(procmethod.getProcurementmethodCode());
			model.setProcurementmethodId(procmethod.getProcurementmethodId());
			model.setProcurementmethodName(procmethod.getProcurementmethodName());
			catmodel.setCatId(procmethod.getTeqipCategorymaster().getCatId());
			catmodel.setCategoryName(procmethod.getTeqipCategorymaster().getCategoryName());
			model.setTeqipCategorymasterModel(catmodel);
			list.add(model);

		}

		mapObj.put("Status", "Success");
		mapObj.put("DATA", list);
		return mapObj;
	}

	public Map<String, Object> saveSatemaster(TeqipStatemastermodel statemaster) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap();
		if (statemaster == null) {
			return null;
		}
		TeqipStatemaster stmaster = new TeqipStatemaster();
		stmaster.setStateCode(statemaster.getStateCode());
		stmaster.setStateName(statemaster.getStateName());
		stmaster.setAddress(statemaster.getAddress());
		stmaster.setEmailID(statemaster.getEmailID());
		stmaster.setFaxNumber(statemaster.getFaxNumber());
		stmaster.setPhoneNumber(statemaster.getPhoneNumber());
		stmaster.setWebsiteURL(statemaster.getWebsiteURL());
		stmaster.setIsactive(true);
		TeqipStatemaster master = teqipStatemasteryRepository.save(stmaster);
		if (master != null) {
			map.put("Success", master);
		} else {
			map.put("Fail", "Failed");
		}
		return map;
	}

	public Map<String, Object> updateState(TeqipStatemastermodel statemaster) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap();
		if (statemaster == null) {
			return null;
		}
		TeqipStatemaster stmaster = teqipStatemasteryRepository.findOne(statemaster.getStateId());
		stmaster.setStateCode(statemaster.getStateCode());
		stmaster.setStateName(statemaster.getStateName());
		stmaster.setAddress(statemaster.getAddress());
		stmaster.setEmailID(statemaster.getEmailID());
		stmaster.setFaxNumber(statemaster.getFaxNumber());
		stmaster.setPhoneNumber(statemaster.getPhoneNumber());
		stmaster.setWebsiteURL(statemaster.getWebsiteURL());
		stmaster.setIsactive(true);
		TeqipStatemaster masters = teqipStatemasteryRepository.save(stmaster);
		TeqipStatemastermodel master = new TeqipStatemastermodel();
		master.setStateId(masters.getStateId());
		master.setStateCode(masters.getStateCode());
		master.setStateName(masters.getStateName());
		master.setAddress(masters.getAddress());
		master.setAllocatedbudget(masters.getAllocatedBudget());
		master.setEmailID(masters.getEmailID());
		master.setFaxNumber(masters.getFaxNumber());
		master.setPhoneNumber(masters.getPhoneNumber());
		master.setWebsiteURL(masters.getWebsiteURL());
		if (master != null) {
			map.put("Success", master);
		} else {
			map.put("Fail", "Failed");
		}
		return map;
	}

	public Map<String, Object> findprocurmentById(Integer procurementId) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		List<Object[]> list = teqipProcMethodTimelineRepository.findprocurementmethod(procurementId);
		List<PrecurementMethodTimeLineModel> mylist = new ArrayList<>();
		if (list.size() >= 1) {
			for (Object[] obj : list) {
				PrecurementMethodTimeLineModel objList = new PrecurementMethodTimeLineModel();
				objList.setBidDocumentationPreprationDatedays((Float) obj[0]);
				objList.setBankNOCForBiddingDocumentsdays((Float) obj[1]);
				objList.setBidInvitationDatedays((Float) obj[2]);
				objList.setBidOpeningDatedays((Float) obj[3]);
				objList.setContractAwardDatedays((Float) obj[4]);
				objList.setContractCompletionDatedays((Float) obj[5]);
				objList.settORFinalizationDatedays((Float) obj[6]);
				objList.setAdvertisementDatedays((Float) obj[7]);
				objList.setFinalDraftToBeForwardedToTheBankDatedays((Float) obj[8]);
				objList.setNoObjectionFromBankForRFPdays((Float) obj[9]);
				objList.setrFPIssuedDatedays((Float) obj[10]);
				objList.setLastDateToReceiveProposalsdays((Float) obj[11]);
				objList.setEvaluationDatedays((Float) obj[12]);
				objList.setNoObjectionFromBankForEvaluationdays((Float) obj[13]);
				// objList.setContractAwardDate_WithoutBankNOCdays((Date)obj[4]);
				// objList.setContractAwardDate_WithoutBankNOCdays((Date)obj[5]);
				objList.setProcurementmethodcode((String) obj[14]);
				objList.setProcurementmethodname((String) obj[15]);
				mylist.add(objList);
				mapObj.put("Status", "success");
				mapObj.put("data", mylist);
			}
		}

		return mapObj;
	}

	public Map<String, Object> findStateMasterById(Integer id) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap();
		TeqipStatemaster statemaster = teqipStatemasteryRepository.findOne(id);
		TeqipStatemastermodel stmaster = new TeqipStatemastermodel();
		stmaster.setStateCode(statemaster.getStateCode());
		stmaster.setStateName(statemaster.getStateName());
		stmaster.setAddress(statemaster.getAddress());
		stmaster.setEmailID(statemaster.getEmailID());
		stmaster.setFaxNumber(statemaster.getFaxNumber());
		stmaster.setPhoneNumber(statemaster.getPhoneNumber());
		stmaster.setWebsiteURL(statemaster.getWebsiteURL());
		stmaster.setIsactive(true);
		// TeqipStatemaster master= teqipStatemasteryRepository.save(stmaster);
		if (stmaster != null) {
			map.put("Success", stmaster);
		} else {
			map.put("Fail", "Failed");
		}
		return map;

	}

	public Map<String, Object> addNewprocurement(PrecurementMethodTimeLinePojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		try {
			// ProcurementMethodCategory obj=new ProcurementMethodCategory();
			// obj.setCategoryName(pojo.getCategoryName());

			// obj.setISACTIVE(pojo.getISACTIVE());
			// ProcurementMethodCategory
			// response=procurmentPlanService.save(obj);
			TeqipProcurementmethod procurementMethod = new TeqipProcurementmethod();
			if (pojo.getCategoryId() != null) {
				TeqipCategorymaster catmaster = categoryRepository.findOne(pojo.getCategoryId());
				procurementMethod.setTeqipCategorymaster(catmaster);
			}
			procurementMethod.setProcurementmethodCode(pojo.getProcurementmethodcode());
			procurementMethod.setProcurementmethodName(pojo.getProcurementmethodname());
			// procurementMethod.setProcurementMethodCategory(response);
			TeqipProcurementmethod procmethod = procurementmethodRepository.save(procurementMethod);
			TeqipProcurementmethodModel model = new TeqipProcurementmethodModel();
			TeqipCategorymasterModel catmodel = new TeqipCategorymasterModel();
			model.setProcurementmethodCode(procmethod.getProcurementmethodCode());
			model.setProcurementmethodId(procmethod.getProcurementmethodId());
			model.setProcurementmethodName(procmethod.getProcurementmethodName());
			catmodel.setCatId(procmethod.getTeqipCategorymaster().getCatId());
			catmodel.setCategoryName(procmethod.getTeqipCategorymaster().getCategoryName());
			model.setTeqipCategorymasterModel(catmodel);
			if (model != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", model);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;
	}

	public Map<String, Object> getprocurementwithPrior(Integer procurementId) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		List<Object[]> list = procurementmethodRepository.findById(procurementId);
		List<PrecurementMethodTimeLinePojo> mylist = new ArrayList<>();
		if (list.size() >= 1) {
			for (Object[] obj : list) {
				PrecurementMethodTimeLinePojo objList = new PrecurementMethodTimeLinePojo();
				objList.setBidDocumentationPreprationDatedays((Date) obj[0]);
				objList.setBankNOCForBiddingDocumentsdays((Date) obj[1]);
				objList.setBidInvitationDatedays((Date) obj[2]);
				objList.setBidOpeningDatedays((Date) obj[3]);
				objList.setContractAwardDatedays((Date) obj[4]);
				objList.setContractCompletionDatedays((Date) obj[5]);
				objList.settORFinalizationDatedays((Date) obj[6]);
				objList.setAdvertisementDatedays((Date) obj[7]);
				objList.setFinalDraftToBeForwardedToTheBankDatedays((Date) obj[8]);
				objList.setNoObjectionFromBankForRFPdays((Date) obj[9]);
				objList.setrFPIssuedDatedays((Date) obj[10]);
				objList.setLastDateToReceiveProposalsdays((Date) obj[11]);
				objList.setEvaluationDatedays((Date) obj[12]);
				objList.setNoObjectionFromBankForEvaluationdays((Date) obj[13]);
				// objList.setContractAwardDate_WithoutBankNOCdays((Date)obj[4]);
				// objList.setContractAwardDate_WithoutBankNOCdays((Date)obj[5]);
				objList.setProcurementmethodcode((String) obj[14]);
				objList.setProcurementmethodname((String) obj[15]);
				mylist.add(objList);
				mapObj.put("Status", "success");
				mapObj.put("data", mylist);
			}
		}

		return mapObj;
	}

	public Map<String, Object> getprocurementwithoughtPrior(Integer procurementId) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		List<Object[]> list = teqipProcMethodTimelineRepository.findprocurementmethod(procurementId);
		List<PrecurementMethodTimeLinePojo> mylist = new ArrayList<>();
		if (list.size() >= 1) {
			for (Object[] obj : list) {
				PrecurementMethodTimeLinePojo objList = new PrecurementMethodTimeLinePojo();
				objList.setBidDocumentationPreprationDatedays((Date) obj[0]);
				objList.setBankNOCForBiddingDocumentsdays((Date) obj[1]);
				objList.setBidInvitationDatedays((Date) obj[2]);
				objList.setBidOpeningDatedays((Date) obj[3]);
				objList.setContractAwardDatedays((Date) obj[4]);
				objList.setContractCompletionDatedays((Date) obj[5]);
				objList.settORFinalizationDatedays((Date) obj[6]);
				objList.setAdvertisementDatedays((Date) obj[7]);
				objList.setFinalDraftToBeForwardedToTheBankDatedays((Date) obj[8]);
				objList.setNoObjectionFromBankForRFPdays((Date) obj[9]);
				objList.setrFPIssuedDatedays((Date) obj[10]);
				objList.setLastDateToReceiveProposalsdays((Date) obj[11]);
				objList.setEvaluationDatedays((Date) obj[12]);
				objList.setNoObjectionFromBankForEvaluationdays((Date) obj[13]);
				// objList.setContractAwardDate_WithoutBankNOCdays((Date)obj[4]);
				// objList.setContractAwardDate_WithoutBankNOCdays((Float)obj[5]);
				objList.setProcurementmethodcode((String) obj[14]);
				objList.setProcurementmethodname((String) obj[15]);
				mylist.add(objList);
				mapObj.put("Status", "success");
				mapObj.put("data", mylist);
			}
		}

		return mapObj;

	}

	public Map<String, Object> findthreshold() {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList categorymasterList = new ArrayList();
		ArrayList threshholdmasterlist = new ArrayList();
		Boolean isActive = true;
		List<TeqipPmssThreshholdmaster> threshholdmasterlists = teqipPmssThreshholdmasterRepository
				.findThreshholdMasterByActiveStatus();

		for (TeqipPmssThreshholdmaster threshold : threshholdmasterlists) {
			TeqipPmssThreshholdmodel thres = new TeqipPmssThreshholdmodel();
			thres.setThresholdid(threshold.getThresholdid());
			thres.setThresholdMaxValue(threshold.getThresholdMaxValue());
			thres.setThresholdMinValue(threshold.getThresholdMinValue());
			thres.setIsproprietary(threshold.getIsproprietary());
			thres.setReview(threshold.getReview());
			thres.setIsactive(threshold.getIsactive());
			TeqipCategorymasterModel model = new TeqipCategorymasterModel();
			model.setCatId(threshold.getTeqipCategorymaster().getCatId());
			model.setCategoryName(threshold.getTeqipCategorymaster().getCategoryName());
			thres.setTeqipCategorymaster(model);
			TeqipProcurementmethodModel procure = new TeqipProcurementmethodModel();
			procure.setProcurementmethodCode(threshold.getTeqipProcurementmethod().getProcurementmethodCode());
			procure.setProcurementmethodId(threshold.getTeqipProcurementmethod().getProcurementmethodId());
			procure.setProcurementmethodName(threshold.getTeqipProcurementmethod().getProcurementmethodName());
			thres.setTeqipProcurementmethod(procure);
			threshholdmasterlist.add(thres);

		}
		List<TeqipCategorymaster> categorymasterLists = categoryRepository
				.getCategoryMasterDataByActiveStatus(isActive);
		for (TeqipCategorymaster master : categorymasterLists) {
			TeqipCategorymasterModel model = new TeqipCategorymasterModel();
			model.setCatId(master.getCatId());
			model.setCategoryName(master.getCategoryName());
			categorymasterList.add(model);

		}

		if (threshholdmasterlist != null) {
			map.put("threshholdmasterlist", threshholdmasterlist);
			// map.put("subCategoryList", list);
			map.put("categoryList", categorymasterList);
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.OK);

		} else {
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.BAD_REQUEST);
		}
		return map;

	}

	public Map<String, Object> findprocurementBycatId(Long catId) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new LinkedHashMap<>();
		ArrayList procurementmethodList = new ArrayList();
		List<TeqipProcurementmethod> procurementmethodLists = procurementmethodRepository
				.getProcurementMethodsByCatId(catId);
		for (TeqipProcurementmethod procmethod : procurementmethodLists) {
			TeqipProcurementmethodModel procure = new TeqipProcurementmethodModel();
			procure.setProcurementmethodCode(procmethod.getProcurementmethodCode());
			procure.setProcurementmethodId(procmethod.getProcurementmethodId());
			procure.setProcurementmethodName(procmethod.getProcurementmethodName());
			procurementmethodList.add(procure);

		}
		if (procurementmethodList != null) {
			map.put("procurementmethodList", procurementmethodList);
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.OK);

		} else {
			map.put("TOKEN", "ABC@123");
			map.put("Status", HttpStatus.BAD_REQUEST);
		}
		return map;
	}

	public Map<String, Object> addthreshold(TeqipPmssThreshholdmodel pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		String directContracting = "Direct Contracting";

		try {
			TeqipPmssThreshholdmaster obj = new TeqipPmssThreshholdmaster();
			obj.setThresholdMinValue(pojo.getThresholdMinValue());
			obj.setThresholdMaxValue(pojo.getThresholdMaxValue());
			obj.setReview(pojo.getReview());
			obj.setIsactive(true);
			if (pojo.getCatId() != null) {
				TeqipCategorymaster categoryMaster = categoryRepository.findCategoryMasterById(pojo.getCatId());
				TeqipCategorymaster category = new TeqipCategorymaster();
				category.setCatId(categoryMaster.getCatId());
				category.setCategoryName(categoryMaster.getCategoryName());
				obj.setTeqipCategorymaster(category);
			}
			if (pojo.getProcurementmethodId() != null) {
				TeqipProcurementmethod procurementmethod = procurementmethodRepository
						.findOne(pojo.getProcurementmethodId());
				TeqipProcurementmethod procure = new TeqipProcurementmethod();
				procure.setProcurementmethodCode(procurementmethod.getProcurementmethodCode());
				procure.setProcurementmethodId(procurementmethod.getProcurementmethodId());
				procure.setProcurementmethodName(procurementmethod.getProcurementmethodName());

				obj.setTeqipProcurementmethod(procure);

				if (directContracting.equals(procurementmethod.getProcurementmethodName())) {
					obj.setIsproprietary(1);
					// obj.setIsactive(true);
				}
			}
			TeqipPmssThreshholdmaster response = teqipPmssThreshholdmasterRepository.save(obj);

			if (response != null) {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "SUCESS");
				mapObj.put("DATA", response);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;

	}

	public ResponseEntity<SupplierMaster> desablesupplier(Integer supplierID) {
		// TODO Auto-generated method stub
		SupplierMaster supplierMaster = supplierMasterRepo.findOne(supplierID);
		if (supplierMaster != null) {
			supplierMaster.setISACTIVE((byte) 0);
			SupplierMaster log = supplierMasterRepo.save(supplierMaster);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();

	}

	public Map<String, Object> saveDepartment(HttpServletRequest req, TeqipDepartmentModel department) {
		// TODO Auto-generated method stub
		if (department == null) {
			return null;
		}
		Map<String, Object> map = new HashMap();
		LoginUser claim = (LoginUser) req.getAttribute("claim");
		DepartmentMaster dept = new DepartmentMaster();
		dept.setDepartmentname(department.getDepartmentname());
		dept.setDepartmentcode(department.getDepartmentcode());
		dept.setCreated_By(claim.getUserid());
		dept.setIsactive(1);
		DepartmentMaster response = departmentMasterRepo.save(dept);
		if (response != null) {
			map.put("success", response);
		}
		return map;
	}

	public Map<String, Object> updateDepartmentMaster(HttpServletRequest req, TeqipDepartmentModel department) {
		// TODO Auto-generated method stub
		if (department == null) {
			return null;
		}
		Map<String, Object> map = new HashMap();
		LoginUser claim = (LoginUser) req.getAttribute("claim");
		DepartmentMaster dept = departmentMasterRepo.findOne(department.getDepartmentId());
		dept.setDepartmentname(department.getDepartmentname());
		dept.setDepartmentcode(department.getDepartmentcode());
		dept.setModify_by(claim.getUserid());
		// dept.setIsactive(1);
		DepartmentMaster response = departmentMasterRepo.save(dept);
		if (response != null) {
			map.put("success", response);
		}
		return map;
	}

	public ResponseEntity<DepartmentMaster> desabledepartmentt(Integer deptId) {
		// TODO Auto-generated method stub
		DepartmentMaster deptmnt = departmentMasterRepo.findOne(deptId);
		if (deptmnt != null) {
			deptmnt.setIsactive(0);
			DepartmentMaster dep = departmentMasterRepo.save(deptmnt);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();

	}

	public Map<String, Object> updateThreshold(HttpServletRequest request, TeqipPmssThreshholdmodel pojo) {
		// TODO Auto-generated method stub
		String directContracting = "Direct Contracting";

		Map<String, Object> mapObj = new LinkedHashMap();

		try {
			TeqipPmssThreshholdmaster obj = teqipPmssThreshholdmasterRepository.findOne(pojo.getThresholdid());
			if (obj != null) {
				obj.setThresholdMinValue(pojo.getThresholdMinValue());
				obj.setThresholdMaxValue(pojo.getThresholdMaxValue());
				obj.setReview(pojo.getReview());
				// obj.setIsactive(true);
				if (pojo.getCatId() != null) {
					TeqipCategorymaster categoryMaster = categoryRepository.findCategoryMasterById(pojo.getCatId());
					TeqipCategorymaster category = new TeqipCategorymaster();
					category.setCatId(categoryMaster.getCatId());
					category.setCategoryName(categoryMaster.getCategoryName());
					obj.setTeqipCategorymaster(category);
				}
				if (pojo.getProcurementmethodId() != null) {
					TeqipProcurementmethod procurementmethod = procurementmethodRepository
							.findOne(pojo.getProcurementmethodId());
					TeqipProcurementmethod procure = new TeqipProcurementmethod();
					procure.setProcurementmethodCode(procurementmethod.getProcurementmethodCode());
					procure.setProcurementmethodId(procurementmethod.getProcurementmethodId());
					procure.setProcurementmethodName(procurementmethod.getProcurementmethodName());

					obj.setTeqipProcurementmethod(procure);

					/*
					 * if (directContracting.equals(procurementmethod.
					 * getProcurementmethodName())) { obj.setIsproprietary(1);
					 * obj.setIsactive(true); } else { obj.setIsproprietary(0);
					 * obj.setIsactive(true); }
					 */
				}
				TeqipPmssThreshholdmaster response = teqipPmssThreshholdmasterRepository.save(obj);
				if (response != null) {
					mapObj.put("TOKEN", "ABC@123");
					mapObj.put("RESULT", "SUCESS");
					mapObj.put("DATA", response);

				}
			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			// e.printStackTrace();
		}

		return mapObj;

	}

	public Map<String, Object> UserInfoByUsername(String username) {
		// TODO Auto-generated method stub
		List<Object[]> list = teqipUserDetailsRepository.findUserinfo(username);
		UserLoginDetails detail = new UserLoginDetails();
		Map<String, Object> userInfo = new HashMap();
		if (list.size() >= 1) {
			for (Object[] info : list) {
				detail.setFirstName((String) info[0]);
				detail.setMiddleName((String) info[1]);
				detail.setLanme((String) info[2]);
				detail.setUserName((String) info[3]);
				detail.setDob((java.sql.Date) info[4]);
				detail.setGender((String) info[5]);
				detail.setPhoneNumber((String) info[7]);
				detail.setEmail((String) info[6]);
				detail.setAddress((String) info[8]);
				detail.setPincode((Double) info[9]);
				detail.setState((String) info[10]);
				detail.setInstituteName((String) info[11]);
				detail.setRole((String) info[12]);

			}
			userInfo.put("Success", detail);

		}
		return userInfo;
	}

	public Map<String, Object> savelogoimg(MultipartFile file, Integer instituteid, String description, Integer spfuid,
			Integer typeNpcEdcil) throws IOException {
		Map map = new HashMap<String, Object>();
		// TODO Auto-generated method stub
		if (file != null) {
			String b = FileUtils.checkFileMimeType(file);
			if (b == "Not Valid") {
				map.put("Invalid File", "Choose the file with  valid name and extension and size less then 2mb ");
				return map;
			}

		}
		LoginUser claim = LoggedUser.get();
		TeqipPmssInstitutionlogo teqiplogo = new TeqipPmssInstitutionlogo();
		teqiplogo.setOriginalFileName(file.getOriginalFilename());
		teqiplogo.setSystemFileName(System.currentTimeMillis() + "_" + file.getOriginalFilename());

		// teqiplogo.setCreatedOn(AttachedDate);
		teqiplogo.setDescription(description);
		teqiplogo.setIsactice(true);
		teqiplogo.setCreatedBy(claim.getUserid());
		teqiplogo.setModifiedby(claim.getUserid());

		if (instituteid != null) {
			TeqipInstitution teqipInstitution = teqipInstitutionRepository.findOne(instituteid);
			TeqipInstitution inst = new TeqipInstitution();
			inst.setInstitutionId(teqipInstitution.getInstitutionId());
			teqiplogo.setTeqipInstitution(inst);
		}
		if (spfuid != null) {
			TeqipStatemaster state = teqipStatemasteryRepository.findOne(spfuid);
			TeqipStatemaster state1 = new TeqipStatemaster();
			state1.setStateId(state.getStateId());
			teqiplogo.setTeqipStatemaster(state1);

		}
		if (spfuid == null && instituteid == null) {
			teqiplogo.setTypeNpcEdcil(typeNpcEdcil);
		}
		TeqipPmssInstitutionlogo ob = teqipPmssInstitutionlogoRepository.save(teqiplogo);
		String filesavetime = "";
		String[] filetime = ob.getSystemFileName().split("_");
		filesavetime = filesavetime + filetime[0];
		File file1 = new File(MethodConstants.UPLOAD_LOCATION);
		String path = file1.getCanonicalPath();
		System.out.println("path :" + path);
		String dir = path + File.separator + "logo";
		String filename = filesavetime + "_" + file.getOriginalFilename();
		// String directory = MethodConstants.LOGO_LOCATION;
		File filepath = new File(dir);
		if (!filepath.exists()) {
			filepath.mkdirs();
		}
		File originalFile = new File(filepath + File.separator + filename);
		originalFile.createNewFile();
		OutputStream op = new FileOutputStream(originalFile);
		op.write(file.getBytes());
		op.close();

		map.put("sucess", ob);
		return map;
	}

	public Map<String, Object> updatelogo(MultipartFile file, Integer institutionLogoID, String description,
			Integer spfuid, Integer instituteId, Integer typeEdcilNpc) throws IOException {
		Map<String, Object> map = new HashMap();
		if (file != null) {
			String b = FileUtils.checkFileMimeType(file);
			if (b == "Not Valid") {
				map.put("Invalid File", "Choose the file with  valid name and extension and size less then 2mb ");
				return map;
			}
		}
		// TODO Auto-generated method stub
		if (institutionLogoID == null) {
			return null;
		}
		LoginUser claim = LoggedUser.get();
		TeqipPmssInstitutionlogo teqiplogo = teqipPmssInstitutionlogoRepository.findOne(institutionLogoID);
		teqiplogo.setIsactice(true);
		teqiplogo.setOriginalFileName(file.getOriginalFilename());
		teqiplogo.setInstitutionLogoID(institutionLogoID);
		if (instituteId != null) {
			TeqipInstitution teqipInstitution = teqipInstitutionRepository.findOne(instituteId);
			TeqipInstitution inst = new TeqipInstitution();
			inst.setInstitutionId(teqipInstitution.getInstitutionId());
			teqiplogo.setTeqipInstitution(inst);
		}
		if (spfuid != null) {
			TeqipStatemaster state = teqipStatemasteryRepository.findOne(spfuid);
			TeqipStatemaster state1 = new TeqipStatemaster();
			state1.setStateId(state.getStateId());
			teqiplogo.setTeqipStatemaster(state1);

		}
		if (typeEdcilNpc != null) {
			teqiplogo.setTypeNpcEdcil(typeEdcilNpc);
		}
		TeqipPmssInstitutionlogo instlogo = teqipPmssInstitutionlogoRepository.save(teqiplogo);
		String filesavetime = "";
		String[] filetime = instlogo.getSystemFileName().split("_");
		filesavetime = filesavetime + filetime[0];
		File file1 = new File(MethodConstants.UPLOAD_LOCATION);
		String path = file1.getCanonicalPath();
		System.out.println("path :" + path);
		String dir = path + File.separator + "logo";
		String filename = filesavetime + "_" + file.getOriginalFilename();
		// String directory = MethodConstants.LOGO_LOCATION;
		File filepath = new File(dir);
		if (!filepath.exists()) {
			filepath.mkdirs();
		}
		File originalFile = new File(filepath + File.separator + filename);
		originalFile.createNewFile();
		OutputStream op = new FileOutputStream(originalFile);
		op.write(file.getBytes());
		op.close();

		map.put("sucess", instlogo.getInstitutionLogoID());
		return map;

	}

	public Map<String, Object> findCityBystId(Integer stid) {
		// TODO Auto-generated method stub

		Map<String, Object> obj = new HashMap();
		ArrayList<CityMaster> citymaster = new ArrayList();
		List<TeqipCitymaster> cities = cityMasterRepository.findCity(stid);
		for (TeqipCitymaster city : cities) {
			CityMaster master = new CityMaster();
			master.setCityName(city.getCityName());
			citymaster.add(master);
		}
		obj.put("City", citymaster);
		return obj;
	}

	public Map<String, Object> saveNewUser(UserPojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		Boolean status = true;
		String statecode = null;
		String institutioncode = null;
		String lastname = null;
		Integer roleid = null;

		if (pojo == null) {
			return null;
		}

		try {
			TeqipUsersDetail obj = new TeqipUsersDetail();
			obj.setDob(pojo.getDob());
			obj.setGender(pojo.getGender());
			obj.setIsactive(true);
			obj.setLname(pojo.getLname());
			obj.setName(pojo.getName());
			obj.setIsBlocked(false);
			if (pojo.getPassword() != null) {
				// String pawordDecrypt =
				// Base64Encoder.decodePassword(pojo.getPassword());
				obj.setPassword(pojo.getPassword().trim());
			}
			obj.setSpouseName(pojo.getSpouseName());
			obj.setUserMobile(pojo.getUserMobile());
			if (pojo.getEmail() != null) {
				TeqipUsersDetail detail = teqipUserDetailsRepository.findByemail(pojo.getEmail());
				if (null != detail && detail.getEmail().equals(pojo.getEmail())) {
					// throw new RuntimeException("Email address is allready
					// exist");
					throw new PMSSException("EmailId is alleady Exist.",
							new ErrorCode("PMSS-500", "EmailId is alleady Exist."));
				} else {
					obj.setEmail(pojo.getEmail());
				}

			}
			UserCreater userCreater = new UserCreater();
			if (pojo.getStateId() != null) {
				TeqipStatemaster teqipStateMaster = teqipStatemasteryRepository.findOne(pojo.getStateId());
				if (teqipStateMaster == null) {
					statecode = null;
				} else {
					statecode = teqipStateMaster.getStateCode();
				}
			}
			if (pojo.getInstitutionid() != null) {
				TeqipInstitution institution = teqipInstitutionRepository.findOne(pojo.getInstitutionid());
				if (institution != null) {
					institutioncode = institution.getInstitutionCode();
				} else {
					institutioncode = null;
				}
			}
			if (pojo.getLname() != null) {
				lastname = pojo.getLname();
			}
			if (pojo.getRoleid() != null) {
				roleid = pojo.getRoleid();
			}
			TeqipRolemaster roledesc = roleMasterRepository.getRoleDescription(roleid);

			String userName = userCreater.createUser(roleid, institutioncode, lastname, statecode, roledesc);
			TeqipUsersDetail teqipUsersDetail = teqipUserDetailsRepository.find(userName, status);
			if (teqipUsersDetail == null) {
				obj.setUserName(userName);
				if (pojo.getRoleid() != null) {
					TeqipRolemaster roleObj = roleMasterRepository.findOne(pojo.getRoleid());
					obj.setTeqipRolemaster(roleObj);
				}
				TeqipUsersDetail users = teqipUserDetailsRepository.save(obj);
				if (users != null) {
					Mail details = new Mail();
					JavaMailSenderImpl mailsender = new JavaMailSenderImpl();
					Properties props = mailsender.getJavaMailProperties();
					props.put("mail.transport.protocol", "smtp");
					props.put("mail.smtp.auth", "true");
					props.put("mail.smtp.starttls.enable", "true");
					props.put("mail.debug", "true");
					props.put(" Djava.net.preferIPv4Stack", "true");
					props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

					details.setFrom("kashif1991nadim@gmail.com");
					details.setTo(new String[] { users.getEmail() });
					String pawordDecrypts = AesUtil.decrypt(users.getPassword());
					details.setBody("Your userName is" + ": " + users.getUserName() + "<br/>" + "Your Password is"
							+ ": " + pawordDecrypts);
					// String newline =
					details.setSubject("Your pmss login Details  ");
					sendEmail(details);
				}
				TeqipUsersDetail response = new TeqipUsersDetail();
				response.setUserid(users.getUserid());
				response.setUserName(users.getUserName());
				response.setUserMobile(users.getUserMobile());
				response.setEmail(users.getEmail());
				TeqipUsersMaster teqipUsersMaster = new TeqipUsersMaster();
				if (pojo.getInstitutionid() != null) {
					TeqipInstitution teqipInstitution = teqipInstitutionRepository.findOne(pojo.getInstitutionid());
					teqipUsersMaster.setTeqipInstitution(teqipInstitution);
				}
				if (pojo.getStateId() != null) {
					TeqipStatemaster state = teqipStatemasteryRepository.findOne(pojo.getStateId());
					teqipUsersMaster.setTeqipStatemaster(state);
				}
				teqipUsersMaster.setIsactive(true);
				if (pojo.getRoleid() != null) {
					TeqipRolemaster roleObj = roleMasterRepository.findOne(pojo.getRoleid());
					teqipUsersMaster.setTeqipRolemaster(roleObj);
				}
				teqipUsersMaster.setTeqipUsersDetail(response);
				TeqipUsersMaster resp = teqipUsersMasterRepo.save(teqipUsersMaster);
				if (resp != null) {
					mapObj.put("RESULT", "SUCESS");
					mapObj.put("DATA", response);

				} else {
					mapObj.put("RESULT", "FAIL");
				}
			} else {
				// mapObj.put("RESULT", "User is Already Exist");
				throw new PMSSException("User is Already Exist", new ErrorCode("PMSS-500", "User is Already Exist"));
			}
		} catch (Exception e) {
			mapObj.put("RESULT", "FAIL");
			mapObj.put("message", e.getMessage());
		}
		return mapObj;
	}

	private void sendEmail(Mail mail) throws Exception {
		MimeMessage message = sender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(message, true, CharEncoding.UTF_8);

		helper.setTo(mail.getTo());

		if (mail.getFrom() != null) {
			helper.setFrom(mail.getFrom());
		}

		if (mail.getCc() != null) {
			helper.setCc(mail.getCc());
		}
		if (mail.getBcc() != null) {

			helper.setBcc(mail.getBcc());
		}
		helper.setSubject(mail.getSubject());

		helper.setText(mail.getBody(), true); // set to html

		if (mail.getFileobj() != null) {
			for (FileObect fileObect : mail.getFileobj()) {
				helper.addAttachment(fileObect.getFileName(), fileObect.getFile());
			}
		}
		sender.send(message);
	}

	public Map<String, Object> saveInstitutionType(TeqipInstitutionTypeModel institutionType) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		if (institutionType == null) {
			return null;
		}
		try {
			TeqipInstitutiontype type = new TeqipInstitutiontype();
			type.setInstitutiontypeId(institutionType.getInstitutiontypeId());
			type.setInstitutiontypeName(institutionType.getInstitutiontypeName());
			type.setIsactive(true);
			TeqipInstitutiontype response = institutionTypeRepository.save(type);
			if (response != null) {
				mapObj.put("DATA", response);

			} else {
				mapObj.put("TOKEN", "ABC@123");
				mapObj.put("RESULT", "FAIL");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		}
		return mapObj;

	}

	public Map<String, Object> updateInstitutionType(TeqipInstitutionTypeModel institutionType) {
		// TODO Auto-generated method stub
		if (institutionType == null) {
			return null;
		}
		Map<String, Object> map = new HashMap();
		if (institutionType.getInstitutiontypeId() != null) {
			TeqipInstitutiontype type = institutionTypeRepository.findOne(institutionType.getInstitutiontypeId());

			type.setInstitutiontypeId(institutionType.getInstitutiontypeId());
			type.setInstitutiontypeName(institutionType.getInstitutiontypeName());
			TeqipInstitutiontype response = institutionTypeRepository.save(type);
			if (response != null) {
				map.put("Data", institutionType);
			}
		}
		return map;
	}

	public Map<String, Object> addnpiu(NpiuDetailsMasterPojo npiuDetails) {
		// TODO Auto-generated method stub
		if (npiuDetails != null) {
			return null;
		}
		Map<String, Object> resp = new HashMap();
		LoginUser claim = LoggedUser.get();
		NpiuDetailsMaster master = new NpiuDetailsMaster();
		master.setCode(npiuDetails.getCode());
		master.setName(npiuDetails.getName());
		master.setDescription(npiuDetails.getDescription());
		master.setAddress(npiuDetails.getAddress());
		master.setCreditNumber(npiuDetails.getCreditNumber());
		master.setAllocatedBudget(npiuDetails.getAllocatedBudget());
		master.setEmailId(npiuDetails.getEmailId());
		master.setFaxNumber(npiuDetails.getFaxNumber());
		master.setProjectCost(npiuDetails.getProjectCost());
		master.setProjectName(npiuDetails.getProjectName());
		master.setIsactive(1);
		master.setCreatedBy(claim.getUserid());
		NpiuDetailsMaster response = npiuDetailsMasterRepo.save(master);
		if (response != null) {
			resp.put("success", response);
		}
		return resp;
	}

	public Map<String, Object> editnpiu(NpiuDetailsMasterPojo npiuDetails) {
		// TODO Auto-generated method stub
		Map<String, Object> resp = new HashMap();
		if (npiuDetails.getId() != null) {
			NpiuDetailsMaster master = npiuDetailsMasterRepo.findOne(npiuDetails.getId());
			LoginUser claim = LoggedUser.get();
			master.setCode(npiuDetails.getCode());
			master.setName(npiuDetails.getName());
			master.setDescription(npiuDetails.getDescription());
			master.setAddress(npiuDetails.getAddress());
			master.setCreditNumber(npiuDetails.getCreditNumber());
			master.setAllocatedBudget(npiuDetails.getAllocatedBudget());
			master.setEmailId(npiuDetails.getEmailId());
			master.setFaxNumber(npiuDetails.getFaxNumber());
			master.setProjectCost(npiuDetails.getProjectCost());
			master.setProjectName(npiuDetails.getProjectName());
			master.setIsactive(1);
			master.setModifiedby(claim.getUserid());
			NpiuDetailsMaster response = npiuDetailsMasterRepo.save(master);
			if (response != null) {
				resp.put("success", response);
			}
		}
		return resp;

	}

	// helpdesk--->Start

	public List<HelpDeskTicketDetails> getHelpDeskTicketType() {
		return helpDeskTicketDetailsRepository.getTicketType();
	}

	public List<HelpDeskTicketDetails> getHelpDeskTicketPriority() {
		return helpDeskTicketDetailsRepository.getTicketPriority();
	}

	public Integer updateAllStatusAndComment(Helpdesk_tickethistory helpdesk_tickethistory) throws java.lang.Exception {
		Helpdesk_ticket mainticket = helpdesk_ticketRepository.getTicketObjet(helpdesk_tickethistory.getHdnTicketId());
		if (mainticket == null) {
			throw new Exception("ticket Not generated");
		} else if (mainticket != null) {

			boolean ab = helpdesk_tickethistory.getHdcComments().contains("'");

			if (ab == true) {
				System.out.println(ab);
				return 0;
			}
		}
		Date createdOn = new Date();
		helpdesk_tickethistory.setModifiedOn(createdOn);
		Helpdesk_tickethistory helpdesk_tickethistory1 = helpdesk_tickethistoryRepository.save(helpdesk_tickethistory);

		// mainticket.setHDnTicketId(helpdesk_tickethistory.getHdnTicketId());
		mainticket.setHDnTicketStatusId(helpdesk_tickethistory.getHdnTicketStatusId());
		helpdesk_ticketRepository.save(mainticket);

		return helpdesk_tickethistory1.getId();

	}

	public Resource downloadHelpDeskFile(Integer id, Integer locationId, String fileName)
			throws MalformedURLException, IOException {
		File file = new File("..");
		String path = file.getCanonicalPath() + File.separator + "helpdeskFile" + File.separator;
		if (fileName == null && fileName.isEmpty()) {
			Helpdesk_documentattachment doc = new Helpdesk_documentattachment();
			helpdesk_documentattachmentRepository.findHelpdeskDoc(id).equals(doc.getAttachedDocName());
		}
		// String path1=path.get;
		// File getFilePath = new File(path);
		// String finalPath = getFilePath.getParent();

		Path rootLocation = Paths.get(file.getCanonicalPath() + File.separator + "helpdeskFile" + File.separator
				+ locationId + File.separator + id + File.separator);

		Path file1 = rootLocation.resolve(fileName);
		org.springframework.core.io.Resource resource = new UrlResource(file1.toUri());
		if (!resource.exists()) {
			throw new PMSSException("File not exist", new ErrorCode("PMSS-500", "File not Exist"));
		}
		return resource;

	}

	public List<TeqipHelpdeskTicketModel> getAllStatusAndComment(Integer id) {
		ArrayList al = new ArrayList();
		List<Object[]> list = helpdesk_tickethistoryRepository.getAlStatusAndComment(id);
		for (Object[] ob : list) {
			TeqipHelpdeskTicketModel pojo = new TeqipHelpdeskTicketModel();
			// pojo.setId(ob[0]);
			pojo.setHDnTicketId((Integer) ob[1]);
			pojo.setHDcDetails((String) ob[2]);
			pojo.setHDnTicketStatusId((Integer) ob[3]);
			pojo.setCreatedBy((Integer) ob[4]);
			pojo.setCreatedOn((Date) ob[5]);
			pojo.setHdnTicketSeverityId((Integer) ob[6]);
			pojo.setStatus((String) ob[8]);
			pojo.setLocatonname((String) ob[7]);
			al.add(pojo);
		}

		return al;

	}

	public List<Helpdesk_ticket> getHelpDesk() {
		LoginUser claim = LoggedUser.get();
		List<Object[]> list = null;
		if (claim.getPmssrole().equalsIgnoreCase("SPFU")) {
			list = helpdesk_ticketRepository.getDataInCaseState(claim.getUsername());
		}

		else if (claim.getPmssrole().equalsIgnoreCase("INST") || claim.getPmssrole().equalsIgnoreCase("INST_DIR")) {
			list = helpdesk_ticketRepository.getDataInCaseOfInstitute(claim.getUsername());
		} else {
			list = helpdesk_ticketRepository.getDataForAdmin();

		}

		ArrayList al = new ArrayList();
		if (list != null && list.size() > 0) {
			for (Object[] ob : list) {
				TeqipHelpdeskTicketModel pojo = new TeqipHelpdeskTicketModel();
				pojo.setHDnTicketId((Integer) ob[0]);
				pojo.setHdnTicketnumber((String) ob[1]);
				pojo.setHDnTicketTypeId((Integer) ob[2]);
				pojo.setHdnTicketSeverityId((Integer) ob[3]);
				pojo.setHdnTicketPriorityId((Integer) ob[4]);
				pojo.setHDnInstituteId((Integer) ob[5]);
				pojo.setHDnTicketStatusId((Integer) ob[6]);
				pojo.setHDcTitle((String) ob[7]);
				pojo.setHDcDetails((String) ob[8]);
				pojo.setCreatedBy((Integer) ob[9]);
				pojo.setCreatedOn((Date) ob[10]);
				pojo.setUserName((String) ob[11]);
				pojo.setUserPhone((String) ob[12]);
				pojo.setUserMobile((String) ob[13]);
				pojo.setUserEmail((String) ob[14]);
				pojo.setType((String) ob[15]);
				pojo.setPriority((String) ob[16]);
				pojo.setStatus((String) ob[17]);
				pojo.setLocatonname((String) ob[19]);
				pojo.setDocName((String) ob[18]);
				al.add(pojo);
			}
		}
		return al;
	}

	public Integer addInHelpDesk(Integer locationid, Integer statusid, Integer typeid, Integer priorityid,
			String subject, String description, Integer ticketsevirtyid, String userphone, String name, String mobileno,
			String email, MultipartFile file) throws ParseException, IOException {
		LoginUser claim = LoggedUser.get();
		Random rand = new Random();
		DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Date dateobj = new Date();
		String tckt = "PTKT";
		int x = 0;
		// x=rand.nextInt(1000)*10;
		int count = 0;
		int i = (int) (new Date().getTime() / 1000);

		while (count < 6) {
			count++;

			if (count < 6) {
				x = i / 10;
			} else
				break;
		}

		String HdnTicketnumber = tckt + x;

		Helpdesk_ticket ticket = new Helpdesk_ticket();
		ticket.setHDnLocationId(locationid);
		ticket.setHDnTicketStatusId(statusid);
		ticket.setHDnTicketTypeId(typeid);
		ticket.setHdnTicketPriorityId(priorityid);
		ticket.setHDcTitle(subject);
		ticket.setHdnTicketSeverityId(ticketsevirtyid);
		ticket.setModifiedBy(claim.getUserid());
		ticket.setUserPhone(userphone);
		ticket.setUserName(name);
		ticket.setUserEmail(email);
		ticket.setHdnTicketnumber(HdnTicketnumber);
		ticket.setModifiedOn(new Date());
		ticket.setUserMobile(mobileno);
		ticket.setHDcDetails(description);
		Helpdesk_ticket ticket1 = helpdesk_ticketRepository.save(ticket);

		if (ticket1 != null) {
			Helpdesk_tickethistory tickethistory = new Helpdesk_tickethistory();
			tickethistory.setHdnTicketId(ticket.getHDnTicketId());
			tickethistory.setHdnTicketSeverityId(ticketsevirtyid);
			tickethistory.setHdcComments(description);
			tickethistory.setHdnTicketStatusId(statusid);
			tickethistory.setModifiedBy(ticket1.getModifiedBy());
			tickethistory.setModifiedOn(ticket1.getModifiedOn());
			helpdesk_tickethistoryRepository.save(tickethistory);
		} else {
			try {
				throw new Exception("data is not added");
			} catch (java.lang.Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String fileName = null;
		if (file != null) {
			fileName = file.getOriginalFilename();
			createHelpDeskFile(ticket1.getHDnTicketId(), locationid, file);
			String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
			Helpdesk_documentattachment helpdeskdoc = new Helpdesk_documentattachment();
			helpdeskdoc.setAttachedDocName(fileName);
			helpdeskdoc.setAttachedDocType(extension);
			helpdeskdoc.setAttchedDocSize((int) file.getSize());
			helpdeskdoc.setIsActive(true);
			helpdeskdoc.setTicketID(ticket1.getHDnTicketId());
			helpdeskdoc.setModifiedBy(ticket1.getModifiedBy());
			helpdeskdoc.setModifiedDate(ticket1.getModifiedOn());

			helpdesk_documentattachmentRepository.save(helpdeskdoc);
		}

		// try {
		// sendFeedback(ticket.getHDnTicketId());
		// } catch (Exception e) {
		// // .out.println("sending mail exception occur"+e.getMessage());
		// }

		return ticket.getHDnTicketId();
	}

	public List<HelpDeskTicketDetails> getAllStatus() {
		return helpDeskTicketDetailsRepository.getAllStatus();

	}

	public Integer deleteAllHelpdesk(Integer id) {

		helpdesk_tickethistoryRepository.deleteTicketHistory(id);
		helpdesk_ticketRepository.delete(id);

		// helpdesk_documentattachmentRepository.deleteHelpdeskDoc(id);
		return id;
	}

	public Integer editInHelpDesk(Integer hdntcktid, Integer locationid, Integer statusid, Integer typeid,
			Integer priorityid, String subject, String description, Integer ticketsevirtyid, Integer modifidby,
			String modifiedon, String userphone, String name, String mobileno, String email, MultipartFile file)
					throws java.lang.Exception {

		Helpdesk_ticket ticket = helpdesk_ticketRepository.getTicketObjet(hdntcktid);
		if (ticket != null) {
			ticket.setHDnTicketId(hdntcktid);
			ticket.setHDnLocationId(locationid);
			ticket.setHDnTicketStatusId(statusid);
			ticket.setHDnTicketTypeId(typeid);
			ticket.setHdnTicketPriorityId(priorityid);
			ticket.setHDcTitle(subject);
			ticket.setHdnTicketSeverityId(ticketsevirtyid);
			ticket.setModifiedBy(modifidby);
			ticket.setUserPhone(userphone);
			ticket.setUserName(name);
			ticket.setUserEmail(email);
			// ticket.setHdnTicketnumber(HdnTicketnumber);
			ticket.setModifiedOn(new Date());
			ticket.setUserMobile(mobileno);
			ticket.setHDcDetails(description);

			Helpdesk_ticket ticket1 = helpdesk_ticketRepository.save(ticket);

			if (ticket1 != null) {
				Helpdesk_tickethistory tickethistory = new Helpdesk_tickethistory();

				tickethistory.setHdnTicketId(hdntcktid);
				tickethistory.setHdnTicketSeverityId(ticketsevirtyid);
				tickethistory.setHdcComments(description);
				tickethistory.setHdnTicketStatusId(statusid);
				tickethistory.setModifiedBy(modifidby);
				tickethistory.setModifiedOn(ticket1.getModifiedOn());
				helpdesk_tickethistoryRepository.save(tickethistory);
			} else {
				throw new Exception("data is not added");
			}

			String fileName = null;
			if (file != null) {
				helpdesk_documentattachmentRepository.updateDocHelpdeskDoc(hdntcktid);
				fileName = file.getOriginalFilename();
				createHelpDeskFile(hdntcktid, locationid, file);
				String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
				Helpdesk_documentattachment helpdeskdoc = new Helpdesk_documentattachment();
				helpdeskdoc.setAttachedDocName(fileName);
				helpdeskdoc.setAttachedDocType(extension);
				helpdeskdoc.setAttchedDocSize((int) file.getSize());
				helpdeskdoc.setIsActive(true);
				helpdeskdoc.setTicketID(ticket1.getHDnTicketId());
				helpdeskdoc.setModifiedBy(modifidby);
				helpdeskdoc.setModifiedDate(ticket1.getModifiedOn());

				helpdesk_documentattachmentRepository.save(helpdeskdoc);
			}
		}

		// sendFeedback(ticket.getHDnTicketId());

		return ticket.getHDnTicketId();
	}

	public void createHelpDeskFile(Integer id, Integer locationId, MultipartFile file) throws IOException {
		// File dir=new File("..");

		String path = "helpdeskFile" + File.separator + locationId + File.separator + id + File.separator;
		File mainDir = new File("..", path);
		if (!mainDir.exists()) {
			mainDir.mkdirs();

		}
		File orginalFile = new File(mainDir.getAbsolutePath() + File.separator + file.getOriginalFilename());

		String filename = file.getOriginalFilename();
		orginalFile.createNewFile();
		OutputStream fout = new FileOutputStream(orginalFile);
		fout.write(file.getBytes());
		fout.close();

	}

}
