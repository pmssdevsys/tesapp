package com.qspear.procurement.service;

import static com.qspear.procurement.security.constant.SecurityConstants.HEADER_STRING;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;


import com.qspear.procurement.model.LoginModel;
import com.qspear.procurement.model.UserPojo;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipRolemaster;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.persistence.TeqipUsersDetail;
import com.qspear.procurement.persistence.TeqipUsersMaster;
import com.qspear.procurement.repositories.RoleMasterRepository;
import com.qspear.procurement.repositories.TeqipInstitutionRepository;
import com.qspear.procurement.repositories.TeqipStatemasteryRepository;
import com.qspear.procurement.repositories.TeqipUserDetailsRepository;
import com.qspear.procurement.repositories.TeqipUsersMasterRepo;
import com.qspear.procurement.security.util.TokenGenerator;
import com.qspear.procurement.util.UserCreater;
@Service
public class SignServiceImpl {
	@Autowired
	TeqipInstitutionRepository teqipInstitutionRepository;
	@Autowired
	TeqipStatemasteryRepository teqipStatemasteryRepository;
	@Autowired
	TeqipUsersMasterRepo teqipUsersMasterRepo;
	@Autowired
	RoleMasterRepository roleMasterRepository;
	@Autowired
	TeqipUserDetailsRepository teqipUserDetailsRepository;
	 @Autowired
	private JavaMailSender sender;
	public Map<String, Object> saveNewUser(UserPojo pojo) {
		// TODO Auto-generated method stub
		Map<String, Object> mapObj = new LinkedHashMap();
		Boolean status=true;
		String statecode=null;
		String institutioncode=null;
		String lastname=null;
		Integer roleid=null;
	
		if(pojo==null){
			return null;
		}

		try {
			TeqipUsersDetail obj = new TeqipUsersDetail();
			obj.setDob(pojo.getDob());
			obj.setGender(pojo.getGender());
			obj.setIsactive(true);
			obj.setLname(pojo.getLname());
			obj.setName(pojo.getName());
			obj.setPassword(pojo.getPassword().trim());
			obj.setSpouseName(pojo.getSpouseName());
			obj.setUserMobile(pojo.getUserMobile());
			if(pojo.getEmail()!=null){
			TeqipUsersDetail detail=teqipUserDetailsRepository.findByemail(pojo.getEmail());
			if(null!=detail && detail.getEmail().equals(pojo.getEmail())){
				throw new RuntimeException("Email address is allready exist");
			}
			else{
				obj.setEmail(pojo.getEmail());
			}
			
			}
			UserCreater userCreater = new UserCreater();
				if(pojo.getStateId()!=null){
			TeqipStatemaster teqipStateMaster = teqipStatemasteryRepository.findOne(pojo.getStateId());
			if(teqipStateMaster==null){
			 statecode=null;
			}
			else{
				statecode=teqipStateMaster.getStateCode();
			}
			}
			if(pojo.getInstitutionid()!=null){
				TeqipInstitution institution=teqipInstitutionRepository.findOne(pojo.getInstitutionid());
				if(institution!=null){
					institutioncode=institution.getInstitutionCode();
				}
				else{
					institutioncode=null;
				}
			}
			if(pojo.getLname()!=null){
				lastname=pojo.getLname();
			}
			if(pojo.getRoleid()!=null){
				roleid=pojo.getRoleid();
			}
				TeqipRolemaster roledesc=roleMasterRepository.getRoleDescription(roleid);

			String userName = userCreater.createUser(roleid, institutioncode, lastname,  statecode,roledesc);
			TeqipUsersDetail teqipUsersDetail = teqipUserDetailsRepository.find(userName,status);
			if (teqipUsersDetail == null) {
				obj.setUserName(userName);
				if(pojo.getRoleid()!=null){
				TeqipRolemaster roleObj = roleMasterRepository.findOne(pojo.getRoleid());
				obj.setTeqipRolemaster(roleObj);
				}
				TeqipUsersDetail users = teqipUserDetailsRepository.save(obj);
			/*	if(users!=null){
					String arr[]={users.getEmail()};
					Mail mail=new Mail(arr,null,null,"Longin information","Welcome to PMSS,Your Username is"+ users.getUserName()+ "Your Password is"+ users.getPassword());
					sendEmail(mail);
				}*/
				TeqipUsersDetail response =new TeqipUsersDetail();
				response.setUserid(users.getUserid());
				response.setUserName(users.getUserName());
				response.setUserMobile(users.getUserMobile());
				response.setEmail(users.getEmail());
				TeqipUsersMaster teqipUsersMaster = new TeqipUsersMaster();
				if(pojo.getInstitutionid()!=null){
				TeqipInstitution teqipInstitution=teqipInstitutionRepository.findOne(pojo.getInstitutionid());
				teqipUsersMaster.setTeqipInstitution(teqipInstitution);
				}
				if(pojo.getStateId()!=null){
					TeqipStatemaster state=teqipStatemasteryRepository.findOne(pojo.getStateId());
					teqipUsersMaster.setTeqipStatemaster(state);
				}
				teqipUsersMaster.setIsactive(true);
				if(pojo.getRoleid()!=null){
					TeqipRolemaster roleObj = roleMasterRepository.findOne(pojo.getRoleid());
				teqipUsersMaster.setTeqipRolemaster(roleObj);
				}
				teqipUsersMaster.setTeqipUsersDetail(response);
				TeqipUsersMaster resp = teqipUsersMasterRepo.save(teqipUsersMaster);
				if (resp != null) {
					mapObj.put("RESULT", "SUCESS");
					mapObj.put("DATA", response);

				} else {
					mapObj.put("RESULT", "FAIL");
				}
			} else {
				mapObj.put("RESULT", "User is Already Exist");
			}
		} catch (Exception e) {
			mapObj.put("TOKEN", "ABC@123");
			mapObj.put("RESULT", "FAIL");
			e.printStackTrace();
		} 
		return mapObj;
	}
	  
}
