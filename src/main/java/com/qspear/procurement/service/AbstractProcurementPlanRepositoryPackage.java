package com.qspear.procurement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.qspear.procurement.configuration.dao.ProcurmentPlanDaoImp;
import com.qspear.procurement.repositories.ActivityRepository;
import com.qspear.procurement.repositories.CategoryRepository;
import com.qspear.procurement.repositories.CityMasterRepository;
import com.qspear.procurement.repositories.ComponentMasterRepository;
import com.qspear.procurement.repositories.DepartmentMasterRepo;
import com.qspear.procurement.repositories.InstitutionTypeRepository;
import com.qspear.procurement.repositories.ItemRepository;
import com.qspear.procurement.repositories.NpiuDetailsMasterRepo;
import com.qspear.procurement.repositories.ProcurementmethodRepository;
import com.qspear.procurement.repositories.RoleMasterRepository;
import com.qspear.procurement.repositories.SupplierMasterRepo;
import com.qspear.procurement.repositories.TeqipInstitutionRepository;
import com.qspear.procurement.repositories.TeqipInstitutiondepartmentmappingRepository;
import com.qspear.procurement.repositories.TeqipInstitutionsubcomponentmappingRepository;
import com.qspear.procurement.repositories.TeqipPmssInstitutionlogoRepository;
import com.qspear.procurement.repositories.TeqipPmssInstitutionpurchasecommitteRepository;
import com.qspear.procurement.repositories.TeqipPmssThreshholdmasterRepository;
import com.qspear.procurement.repositories.TeqipProcMethodTimelineRepository;
import com.qspear.procurement.repositories.TeqipStatemasteryRepository;
import com.qspear.procurement.repositories.TeqipSubcategorymasterRepository;
import com.qspear.procurement.repositories.TeqipSubcomponentmasterRepository;
import com.qspear.procurement.repositories.TeqipUserDetailsRepository;
import com.qspear.procurement.repositories.TeqipUsersMasterRepo;
import com.qspear.procurement.repositories.UserRepository;
import com.qspear.procurement.repositories.method.DepartmentMasterRepository;

@Component
public abstract class AbstractProcurementPlanRepositoryPackage {
	@Autowired
	ActivityRepository activityRepository;
	@Autowired
	TeqipSubcategorymasterRepository teqipSubcategorymasterRepository;
	@Autowired
	CategoryRepository categoryRepository;
	@Autowired
	ComponentMasterRepository componentMasterRepository;
	@Autowired
	TeqipSubcomponentmasterRepository teqipSubcomponentmasterRepository;
	@Autowired
	InstitutionTypeRepository institutionTypeRepository;
	@Autowired
	SupplierMasterRepo supplierMasterRepo;
	@Autowired
	ItemRepository ItemRepository;
	@Autowired
	DepartmentMasterRepository departmentMasterRepository;
	@Autowired
	TeqipInstitutionRepository teqipInstitutionRepository;
	@Autowired
	TeqipStatemasteryRepository teqipStatemasteryRepository;
	@Autowired
	protected ProcurmentPlanDaoImp procurmentPlanDaoImp;
	@Autowired
	UserRepository userRepository;
	@Autowired
	DepartmentMasterRepo departmentMasterRepo;
	@Autowired
	RoleMasterRepository roleMasterRepository;
	@Autowired
	TeqipUserDetailsRepository teqipUserDetailsRepository;
	@Autowired
	TeqipUsersMasterRepo teqipUsersMasterRepo;
	@Autowired
	TeqipInstitutionsubcomponentmappingRepository teqipInstitutionsubcomponentmappingRepository;
	@Autowired
	TeqipInstitutiondepartmentmappingRepository teqipInstitutiondepartmentmappingRepository;
	@Autowired
	TeqipPmssInstitutionlogoRepository teqipPmssInstitutionlogoRepository;
	@Autowired
	TeqipPmssInstitutionpurchasecommitteRepository teqipPmssInstitutionpurchasecommitteRepository;
	@Autowired
	NpiuDetailsMasterRepo npiuDetailsMasterRepo;
	@Autowired
	ProcurementmethodRepository procurementmethodRepository;
	@Autowired
	TeqipProcMethodTimelineRepository teqipProcMethodTimelineRepository;
	@Autowired
	TeqipPmssThreshholdmasterRepository teqipPmssThreshholdmasterRepository;
	@Autowired
	CityMasterRepository cityMasterRepository;
	
	
	}
