package com.qspear.procurement.service;


import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSException;
import com.qspear.procurement.persistence.User;
import com.qspear.procurement.persistence.UserAttempts;
import com.qspear.procurement.repositories.UserAttemptRepository;
import com.qspear.procurement.repositories.UserRepository;

@Service
public class UserControllerService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    UserAttemptRepository userAttemptRepository;
    public User findByUserName(String username) {
        User user = userRepository.findByUsername(username);
        return user;
    }

    public User findByUserid(Integer createdBy) {
        
        return userRepository.findByUserid(createdBy);
    }

///////////////////////////?
public void attempt(User loginUser) {
UserAttempts userAttempts=userAttemptRepository.findByUsername(loginUser.getUserName());
if(userAttempts!=null){
if(userAttempts.getAttempts()>=5) {
Timestamp currentTime=new Timestamp(new Date().getTime());
if(loginUser.getIsBlocked()) {
/*if((currentTime.getTime()-loginUser.getBlockedAt().getTime())>=86400000){
userAttempts.setAttempts(0);;
loginUser.setIsBlocked(false);
loginUser.setBlockedAt(null);
userDetailRepository.save(loginUser);
}
else {*/
throw new PMSSException( "Maximum attempt reached, Please retry after 24 hours",new ErrorCode("PMSS-500", "Maximum attempt reached, Please retry after 24 hours"));
 
//}
}else {
if(userAttempts.getAttempts()>=5)
throw new PMSSException( "Maximum attempt reached, Please retry after 24 hours",new ErrorCode("PMSS-500", "Maximum attempt reached, Please retry after 24 hours"));
}
}else {
int count=userAttempts.getAttempts()+1;
userAttempts.setAttempts(count);
if(count>=5) {
loginUser.setBlockedAt(new Timestamp(new Date().getTime()));
loginUser.setIsBlocked(true);
userRepository.save(loginUser);
userAttemptRepository.save(userAttempts);
throw new PMSSException("Maximum attempt reached, Please retry after 24 hours", new ErrorCode("PMSS-500", "Maximum attempt reached, Please retry after 24 hours"));
}else {
userAttemptRepository.save(userAttempts);
throw new PMSSException("username and password did not match. Only "+(5 - count)+" more attempts left now.", new ErrorCode("PMSS-500", "username and password did not match. Only "+(5 - count)+" more attempts left now."));
}
}

}
else {
userAttempts=new UserAttempts();
userAttempts.setUsername(loginUser.getUserName());
userAttempts.setAttempts(1);
}
try{
userAttemptRepository.save(userAttempts);
}
catch(Exception e){
	System.out.println(e.getMessage());
}
}

public void isBlocked(User loginUser) {
	// TODO Auto-generated method stub
	if(loginUser.getIsBlocked()!=null && loginUser.getIsBlocked()) {
		Timestamp currentTime=new Timestamp(new Date().getTime());
		if((currentTime.getTime()-loginUser.getBlockedAt().getTime())>=86400000){
			loginUser.setIsBlocked(false);
			loginUser.setBlockedAt(null);
			userRepository.save(loginUser);
			UserAttempts userAttempts=userAttemptRepository.findByUsername(loginUser.getUserName());
			if(userAttempts!=null){
					userAttempts.setAttempts(0);
					userAttemptRepository.save(userAttempts);
				}
		}else {
			throw new PMSSException("Maximum attempt reached, Please retry after 24 hours", new ErrorCode("PMSS-500", "Maximum attempt reached, Please retry after 24 hours"));
			//throw new PMSSException(new ErrorCode("PMSS-500","Maximum attempt reached, Please retry after 24 hours"));

		}
	}

	
}

}
