package com.qspear.procurement.service;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.constants.ProcurementConstants;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSException;
import com.qspear.procurement.model.InstituteWiseApproval;
import com.qspear.procurement.model.StateWiseApproval;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipItemGoodsDetail;
import com.qspear.procurement.persistence.TeqipItemGoodsDetailHistory;
import com.qspear.procurement.persistence.TeqipItemWorkDetail;
import com.qspear.procurement.persistence.TeqipItemWorkDetailHistory;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPackageHistory;
import com.qspear.procurement.persistence.TeqipPlanApprovalstatus;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.repositories.ItemGoodHistoryRepository;
import com.qspear.procurement.repositories.ItemGoodRepository;
import com.qspear.procurement.repositories.ItemWorkHistoryRepository;
import com.qspear.procurement.repositories.ItemWorkRepository;
import com.qspear.procurement.repositories.TeqipPackageHistoryRepository;
import com.qspear.procurement.security.util.LoginUser;
import com.qspear.procurement.service.mapper.InvitationLetterMapper;
import com.qspear.procurement.service.mapper.ItemGoodsMapper;
import com.qspear.procurement.service.mapper.ItemMasterMapper;
import com.qspear.procurement.service.mapper.ProcurementMapper;
import java.util.ArrayList;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ApprovalService extends AbstractPackageService {

    @Autowired
    ProcurementMapper procurementMapper;

    @Autowired
    InvitationLetterMapper invitationLetterMapper;

    @Autowired
    ItemGoodsMapper itemGoodsMapper;

    @Autowired
    ItemMasterMapper itemMasterMapper;

    @Autowired
    TeqipPackageHistoryRepository teqipPackageHistoryRepository;

    @Autowired
    ItemGoodHistoryRepository itemGoodHistoryRepository;

    @Autowired
    ItemWorkHistoryRepository itemWorkHistoryRepository;

    @Autowired
    ItemGoodRepository itemGoodRepository;

    @Autowired
    ItemWorkRepository itemWorkRepository;

    public String changePackageStatus(
            Integer planId,
            Integer id,
            String packageType,
            String stage,
            String comment,
            HttpServletRequest httpServletRequest) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute("claim");

        String submitStatus = "";

        List<TeqipPackage> teqipPackages = packageService.getPackages(packageType, id, planId, "ALL");

        if (teqipPackages == null || teqipPackages.isEmpty()) {
            throw new PMSSException(
                    new ErrorCode(
                            "PMSS-NO-PACKAGE",
                            "Institute doesn't have any package"));

        }

        TeqipInstitution teqipInstitution = teqipPackages.get(0).getTeqipInstitution();
        TeqipStatemaster teqipStatemaster = teqipPackages.get(0).getTeqipStatemaster();

        int typeId = teqipPackages.get(0).getTypeofplanCreator();

        TeqipPlanApprovalstatus approvalstatus = teqipPlanApprovalStatusRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(
                teqipInstitution, teqipStatemaster, typeId);

        int currentRevisionId = approvalstatus.getReviseid();
        TeqipPmssProcurementstage nextPmssProcurementstage = approvalstatus.getTeqipPmssProcurementstage();
        int roleId = nextPmssProcurementstage.getRoleId();

        TeqipPlanApprovalstatus nextStage = approvalstatus.clone();

        switch (stage) {
            case MethodConstants.APPROVED:

                int approvalProcurementId = teqipPmssProcurementstageRepository
                        .findNextApprovedStage(approvalstatus.getTeqipPmssProcurementstage().getOrderNo(), typeId, roleId, currentRevisionId > 1 ? 1 : 0);
                System.out.println(approvalProcurementId);
                nextStage.setTeqipPmssProcurementstage(teqipPmssProcurementstageRepository.findOne(approvalProcurementId));
                submitStatus = "Approved Successfully";
                break;

            case MethodConstants.REJECTED:

                int rejectProcurementId = teqipPmssProcurementstageRepository
                        .findPreviousRejectedStage(approvalstatus.getTeqipPmssProcurementstage().getOrderNo(), typeId, roleId, currentRevisionId > 1 ? 1 : 0);
                System.out.println(rejectProcurementId);
                nextStage.setTeqipPmssProcurementstage(teqipPmssProcurementstageRepository.findOne(rejectProcurementId));
                submitStatus = "Rejected Successfully";
                break;

            case MethodConstants.SUBMIT:

                int submitProcurementId = teqipPmssProcurementstageRepository
                        .findFirstApprovedStage(typeId, roleId, currentRevisionId > 1 ? 1 : 0);
                System.out.println(submitProcurementId);
                nextStage.setTeqipPmssProcurementstage(teqipPmssProcurementstageRepository.findOne(submitProcurementId));
                submitStatus = "Package Submit Successfully";
                break;

            case MethodConstants.REVISION:
                this.setPackageRevision(teqipPackages, currentRevisionId);
                int revisionProcurementId = teqipPmssProcurementstageRepository
                        .findInitialRevisionStage(typeId, roleId);
                System.out.println(revisionProcurementId);
                nextStage.setTeqipPmssProcurementstage_prev(null);
                nextStage.setTeqipPmssProcurementstage(teqipPmssProcurementstageRepository.findOne(revisionProcurementId));
                currentRevisionId++;
                nextStage.setReviseid(currentRevisionId);

                submitStatus = "Package revison Successfully";

                break;

            default:
                throw new PMSSException(
                        new ErrorCode(
                                "PMSS-INVALIDA_SUBMISSION-TYPE",
                                "Please use type as APPROVED, REJECTED, SUBMIT, REVISION only"));
        }

        for (TeqipPackage teqipPackage : teqipPackages) {
            if (currentRevisionId == teqipPackage.getReviseId()) {
                teqipPackage.setTeqipPmssProcurementstage(nextStage.getTeqipPmssProcurementstage());
                teqipPackage = teqipPackageRepository.saveAndFlush(teqipPackage);
            }
        }

        if (nextStage.getTeqipInstitution() != null) {
            teqipPlanApprovalStatusRepository.updateInstCurrentStage(nextStage.getTeqipInstitution().getInstitutionId());

        } else if (nextStage.getTeqipStatemaster() != null) {
            teqipPlanApprovalStatusRepository.updateSPFUCurrentStage(nextStage.getTeqipStatemaster().getStateId());

        } else {
            teqipPlanApprovalStatusRepository.updateNpiuOrEdcilCurrentStage(nextStage.getType());

        }
        nextStage.setCurrentStage(1);
        nextStage.setStatusComment(comment);
        teqipPlanApprovalStatusRepository.saveAndFlush(nextStage);

        return submitStatus;
    }

    void setApprovalStage(String role, List<Integer> approvalStages) {

        if (ProcurementConstants.isStateRole(role)) {
            approvalStages.add(3);
            approvalStages.add(4);
        }

        if (ProcurementConstants.isNationalRole(role)) {
            approvalStages.add(5);
        }

        if (ProcurementConstants.USER_ROLE_CPA.equalsIgnoreCase(role)) {
            approvalStages.add(9);
        }

        if (ProcurementConstants.USER_ROLE_INST_DIR.equalsIgnoreCase(role)) {
            approvalStages.add(15);
            approvalStages.add(16);
        }
    }

    void setRevisionStage(String role, List<Integer> approvalStages) {

        if (ProcurementConstants.USER_ROLE_INST_DIR.equalsIgnoreCase(role)) {
            approvalStages.add(11);
            approvalStages.add(17);
        }

        if (ProcurementConstants.isNationalRole(role)) {
            approvalStages.add(13);
        }

        if (ProcurementConstants.isStateRole(role)) {
            approvalStages.add(18);
            approvalStages.add(19);
        }

        if (ProcurementConstants.USER_ROLE_CPA.equalsIgnoreCase(role)) {
            approvalStages.add(14);
        }
    }

    public List<StateWiseApproval> stateWiseApprovalOrRevisionPackage(HttpServletRequest httpServletRequest) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute("claim");
        String role = claim.getPmssrole().toUpperCase();

        List<Integer> approvalStages = new ArrayList<>();
        setApprovalStage(role, approvalStages);
        setRevisionStage(role, approvalStages);

        List<Object[]> stateWiseApprovalforInst = new ArrayList<>();

        if (ProcurementConstants.isInstitutionalRole(role)) {
            stateWiseApprovalforInst.addAll(teqipInstitutionRepository.stateWiseApprovalStateInstPackage(approvalStages, claim.getStateid(), claim.getInstid()));//loginstate

        }
        if (ProcurementConstants.isStateRole(role)) {
            stateWiseApprovalforInst.addAll(teqipInstitutionRepository.stateWiseApprovalStateInstPackage(approvalStages, claim.getStateid(), 0));//loginstate

        }
        if (ProcurementConstants.isNationalRole(role)) {
            stateWiseApprovalforInst.addAll(teqipInstitutionRepository.stateWiseApprovalNationalPackage(approvalStages));
            stateWiseApprovalforInst.addAll(teqipInstitutionRepository.stateWiseApprovalEdcilPackage(approvalStages));
            stateWiseApprovalforInst.addAll(teqipInstitutionRepository.stateWiseApprovalCFIPackage(approvalStages));
            stateWiseApprovalforInst.addAll(teqipInstitutionRepository.stateWiseApprovalStateInstPackage(approvalStages, 0, 0));

        }

        return procurementMapper.mapStateWiseApproval(stateWiseApprovalforInst);
    }

    public List<InstituteWiseApproval> instituteWiseApprovalOrRevisionPackage(int stateId, HttpServletRequest httpServletRequest) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute("claim");
        String role = claim.getPmssrole().toUpperCase();

        List<Integer> approvalStages = new ArrayList<>();
        setApprovalStage(role, approvalStages);
        setRevisionStage(role, approvalStages);

        List<Object[]> instituteWiseApproval = new ArrayList<>();

        if (ProcurementConstants.isInstitutionalRole(role)) {
            if (stateId == 0) {
                instituteWiseApproval.addAll(teqipInstitutionRepository.instituteCFIPackageApproval(approvalStages, claim.getInstid()));

            } else {
                instituteWiseApproval.addAll(teqipInstitutionRepository.institutePackageApproval(stateId, approvalStages, claim.getInstid()));//loginstate
            }
        }

        if (ProcurementConstants.isStateRole(role)) {
            instituteWiseApproval.addAll(teqipInstitutionRepository.statePackageApproval(stateId, approvalStages, claim.getStateid())); //loginstate
            instituteWiseApproval.addAll(teqipInstitutionRepository.institutePackageApproval(stateId, approvalStages, 0));

        }

        if (ProcurementConstants.isNationalRole(role)) {
            if (stateId == 0) {
                instituteWiseApproval.addAll(teqipInstitutionRepository.instituteCFIPackageApproval(approvalStages, 0));

            } else {
                instituteWiseApproval.addAll(teqipInstitutionRepository.statePackageApproval(stateId, approvalStages, 0));
                instituteWiseApproval.addAll(teqipInstitutionRepository.institutePackageApproval(stateId, approvalStages, 0));
            }
        }

        return procurementMapper.mapInstituteWiseApproval(instituteWiseApproval);
    }

    public List<InstituteWiseApproval> instituteWiseNotInApprovalOrRevisionPackage(int stateId, HttpServletRequest httpServletRequest) {

        LoginUser claim = (LoginUser) httpServletRequest.getAttribute("claim");
        String role = claim.getPmssrole().toUpperCase();

        List<Integer> approvalStages = new ArrayList<>();
        setApprovalStage(role, approvalStages);
        setRevisionStage(role, approvalStages);

        List<Object[]> instituteWiseApproval = new ArrayList<>();

        if (ProcurementConstants.isInstitutionalRole(role)) {
            instituteWiseApproval.addAll(teqipInstitutionRepository.institutePackageApprovalNotIn(stateId, claim.getInstid()));//loginstate

        }

        if (ProcurementConstants.isStateRole(role)) {
            instituteWiseApproval.addAll(teqipInstitutionRepository.statePackageApprovalNotIn(stateId, claim.getStateid())); //loginstate
            instituteWiseApproval.addAll(teqipInstitutionRepository.institutePackageApprovalNotIn(stateId, 0));

        }

        if (ProcurementConstants.isNationalRole(role)) {
            if (stateId == -1) {
                instituteWiseApproval.addAll(teqipInstitutionRepository.instituteCFIPackageApprovalNotIn());

            } else {
                instituteWiseApproval.addAll(teqipInstitutionRepository.statePackageApprovalNotIn(stateId, 0));
                instituteWiseApproval.addAll(teqipInstitutionRepository.institutePackageApprovalNotIn(stateId, 0));
            }
        }

        return procurementMapper.mapInstituteWiseApproval(instituteWiseApproval);
    }

    private void setPackageRevision(List<TeqipPackage> teqipPackages, int currentRevisionId) {

        for (TeqipPackage teqipPackage : teqipPackages) {

            TeqipPackageHistory teqipPackageHistory = new TeqipPackageHistory(teqipPackage);
            teqipPackageHistory.setReviseId(currentRevisionId);
            teqipPackageHistoryRepository.saveAndFlush(teqipPackageHistory);

            List<TeqipItemGoodsDetail> teqipItemGoodsDetails = teqipPackage.getTeqipItemGoodsDetails();

            if (teqipItemGoodsDetails != null) {

                for (TeqipItemGoodsDetail teqipItemGoodsDetail : teqipItemGoodsDetails) {

                    TeqipItemGoodsDetailHistory itemGoodsDetailHistory = new TeqipItemGoodsDetailHistory(teqipPackageHistory, teqipItemGoodsDetail);
                    itemGoodsDetailHistory.setReviseId(currentRevisionId);
                    itemGoodHistoryRepository.saveAndFlush(itemGoodsDetailHistory);

                }
            }
            List<TeqipItemWorkDetail> teqipItemWorkDetails = teqipPackage.getTeqipItemWorkDetails();

            if (teqipItemWorkDetails != null) {

                for (TeqipItemWorkDetail teqipItemWorkDetail : teqipItemWorkDetails) {

                    TeqipItemWorkDetailHistory itemWorkDetailHistory = new TeqipItemWorkDetailHistory(teqipPackageHistory, teqipItemWorkDetail);
                    itemWorkDetailHistory.setReviseId(currentRevisionId);
                    itemWorkHistoryRepository.saveAndFlush(itemWorkDetailHistory);

                }
            }
        }

    }

}
