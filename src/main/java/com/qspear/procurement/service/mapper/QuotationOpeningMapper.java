package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.QuotationForm;
import com.qspear.procurement.model.methods.QuotationOpening;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageQuotationDetailRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsSupplierInputRepository;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class QuotationOpeningMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackageQuotationsSupplierInputRepository packageQuotationsSupplierInputRepository;

    @Autowired
    PackageQuotationDetailRepository quotationDetailRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    QuestionMapper questionMapper;

    @Autowired
    UploadMapper uploadMapper;

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    @Override
    public void mapObjectToEntity(TeqipPackage entity, MethodPackageUpdateRequest mappingObject) {

        QuotationOpening quotationOpening = mappingObject.getQuotationOpening();

        if (quotationOpening == null) {
            return;
        }

        TeqipPackageQuotationDetail quotationDetail = quotationDetailRepository.findByTeqipPackage(entity);

        quotationDetail = quotationDetail != null ? quotationDetail : new TeqipPackageQuotationDetail();

        quotationDetail.setQuotationOpeningDate(quotationOpening.getQuotationOpeningDate());
        quotationDetail.setQuotationOpeningTime(quotationOpening.getQuotationOpeningTime());
        quotationDetail.setQuotationMom(quotationOpening.getQuotationMom());
        quotationDetail.setTeqipPackage(entity);

        quotationDetailRepository.saveAndFlush(quotationDetail);

        ArrayList<QuotationForm> quotationForm = quotationOpening.getQuotationForm();
        if (quotationForm == null) {
            return;
        }
        for (QuotationForm form : quotationForm) {

            TeqipPmssSupplierMaster supplierMaster = supplierRepository.findOne(form.getSupplierId());

            TeqipPackageSupplierDetail packageSupplierDetail = packageSupplierRepository.findByTeqipPackageAndTeqipPmssSupplierMaster(entity, supplierMaster);
            packageSupplierDetail.setIsFormResponsive(form.getIsFormResponsive());
            packageSupplierDetail.setIsFromResponsiveSource("OPENING");
            packageSupplierRepository.saveAndFlush(packageSupplierDetail);

            this.questionMapper.saveQuestionInputs(form.getQuestions(), supplierMaster, entity, "opening");

        }

    }

    @Override
    public void mapEntityToObject(TeqipPackage entity, MethodPackageResponse mappingObject) {
        QuotationOpening quotationOpening = new QuotationOpening();

        TeqipPackageQuotationDetail quotationDetail = quotationDetailRepository.findByTeqipPackage(entity);
        //Set Opening detail form

        quotationDetail = quotationDetail != null ? quotationDetail : new TeqipPackageQuotationDetail();
        quotationOpening.setPackQuotationDetailId(quotationDetail.getPackQuotationDetailId());
        quotationOpening.setQuotationOpeningDate(quotationDetail.getQuotationOpeningDate());
        quotationOpening.setQuotationOpeningTime(quotationDetail.getQuotationOpeningTime());
        quotationOpening.setQuotationMom(quotationDetail.getQuotationMom());

        mappingObject.setQuotationOpening(quotationOpening);

        //Set Opening fill form 
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = entity.getTeqipPackageSupplierDetails();
        List<TeqipPackageQuotationsSupplierInput> teqipPackageQuotationsEvaluations = packageQuotationsSupplierInputRepository.findByTeqipPackage(entity);

        if (teqipPackageSupplierDetails != null) {

            ArrayList<QuotationForm> quotationForm = new ArrayList<>();
            for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {
                TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
                if (teqipPmssSupplierMaster != null) {
                    QuotationForm form = new QuotationForm();
                    form.setSupplierId(teqipPmssSupplierMaster.getSupplierId());
                    form.setSupplierName(teqipPmssSupplierMaster.getSupplierName());
                    form.setIsFormResponsive(teqipPackageSupplierDetail.getIsFormResponsive());
                    form.setQuestions(this.questionMapper.setQuestionInput(teqipPackageQuotationsEvaluations, teqipPmssSupplierMaster, null, "opening"));

                    quotationForm.add(form);
                }
            }

            quotationOpening.setQuotationForm(quotationForm);
        }
        ArrayList<Uploads> uploads = uploadMapper.getUploads(entity, entity.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_QUOTATION_OPENING);
        uploads.addAll(uploadMapper.getUploads(entity, entity.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_NCB_BID_OPENING));

        quotationOpening.setQoGeneration(uploads);

        mappingObject.setQuotationOpening(quotationOpening);

    }

}
