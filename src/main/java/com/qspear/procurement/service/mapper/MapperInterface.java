package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;

/**
 * Created by e1002703 on 4/4/2018.
 */
public interface MapperInterface<T> {
    public void mapObjectToEntity(T entity, MethodPackageUpdateRequest mappingObject);
    public void mapEntityToObject(T entity, MethodPackageResponse mappingObject);

}
