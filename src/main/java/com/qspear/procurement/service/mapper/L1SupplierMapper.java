package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.L1Supplier;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageL1SupplierDetails;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.L1SupplierRepository;
import com.qspear.procurement.repositories.method.PackageAwardRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class L1SupplierMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    L1SupplierRepository l1SupplierRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PackageAwardRepository packageAwardRepository;

    @Autowired
    QuestionMapper questionMapper;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        L1Supplier model = mappingObject.getL1Supplier();

        if (model == null) {
            return;
        }

        TeqipPackageL1SupplierDetails entity = l1SupplierRepository.findByTeqipPackage(teqipPackage);
        entity =  entity != null ?entity : new TeqipPackageL1SupplierDetails();
        entity.setTeqipPackage(teqipPackage);
        entity.setTeqipPmssSupplierMaster(supplierRepository.findOne(model.getSupplierId()));
        entity.setQuotationNumber(model.getQuotationNumber());
        entity.setQuotationDate(model.getQuotationDate());
        entity.setQuotedPrice(model.getQuotedPrice());

        l1SupplierRepository.saveAndFlush(entity);
    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        TeqipPackageL1SupplierDetails entity = l1SupplierRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return;
        }
        L1Supplier model = new L1Supplier();

        TeqipPmssSupplierMaster supplierMaster = entity.getTeqipPmssSupplierMaster();
        if (supplierMaster != null) {
            model.setSupplierName(supplierMaster.getSupplierName());
            model.setSupplierId(supplierMaster.getSupplierId());
        }
        model.setQuotationNumber(entity.getQuotationNumber());
        model.setQuotationDate(entity.getQuotationDate());
        model.setQuotedPrice(entity.getQuotedPrice());

        mappingObject.setL1Supplier(model);

    }

}
