package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.AdvDetails;
import com.qspear.procurement.model.methods.AdvExpOfInterest;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageBidAdvertisement;
import com.qspear.procurement.repositories.method.PackageBidAdvertisementRepository;
import com.qspear.procurement.repositories.method.PackageBidMeetingDetailRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class AdvExpOfInterestMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PackageBidAdvertisementRepository packageBidAdvertisementRepository;

    @Autowired
    PackageSupplierMappper packageSupplierMappper;

    @Autowired
    PackageBidMeetingDetailRepository bidMeetingDetailRepository;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {

        AdvExpOfInterest advExpOfInterest = mappingObject.getAdvExpOfInterest();
        if (advExpOfInterest == null) {
            return;
        }

        AdvDetails advDetails = advExpOfInterest.getAdvDetails();
        if (advDetails == null) {
            return;
        }
        TeqipPackageBidAdvertisement entity = packageBidAdvertisementRepository.findByTeqipPackage(teqipPackage);

        entity = entity != null ? entity : new TeqipPackageBidAdvertisement();

        entity.setTeqipPackage(teqipPackage);

        entity.setLocalPaperName(advDetails.getLocalPaperName());
        entity.setNationalPaperName(advDetails.getNationalPaperName());
        entity.setAdvertisementDate(advDetails.getDateOfReleaseOfAdv());
        entity.setActualpublicationLocaleDate(advDetails.getActualPublicationLocalDate());
        entity.setActualpublicationNationalDate(advDetails.getActualPublicationNationalDate());
        entity.setEoiSubmissionDate(advDetails.getEoiSubmissionDate());
        packageBidAdvertisementRepository.saveAndFlush(entity);
    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        TeqipPackageBidAdvertisement entity = packageBidAdvertisementRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return;
        }
        AdvDetails advDetails = new AdvDetails();
        advDetails.setLocalPaperName(entity.getLocalPaperName());
        advDetails.setNationalPaperName(entity.getNationalPaperName());
        advDetails.setDateOfReleaseOfAdv(entity.getAdvertisementDate());
        advDetails.setActualPublicationLocalDate(entity.getActualpublicationLocaleDate());
        advDetails.setActualPublicationNationalDate(entity.getActualpublicationNationalDate());
        advDetails.setEoiSubmissionDate(entity.getEoiSubmissionDate());

        AdvExpOfInterest advExpOfInterest = new AdvExpOfInterest();
        advExpOfInterest.setAdvDetails(advDetails);

        mappingObject.setAdvExpOfInterest(advExpOfInterest);

    }

}
