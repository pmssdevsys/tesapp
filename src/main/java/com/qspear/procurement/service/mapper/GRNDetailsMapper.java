package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.*;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageGRNItemDistribution;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.ItemRepository;
import com.qspear.procurement.repositories.method.DepartmentMasterRepository;
import com.qspear.procurement.repositories.method.PackageCompleteChecklistRepository;
import com.qspear.procurement.repositories.method.PackageGRNDetailRepository;
import com.qspear.procurement.repositories.method.PackageGRNItemDetailRepository;
import com.qspear.procurement.repositories.method.PackageGRNItemDistributionRepository;
import com.qspear.procurement.repositories.method.PackagePaymentRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class GRNDetailsMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    UploadMapper uploadMapper;

    @Autowired
    PaymentMapper paymentMapper;

    @Autowired
    PackageGRNDetailRepository grnDetailRepository;

    @Autowired
    PackageGRNItemDetailRepository grnItemDetailRepository;

    @Autowired
    PackageGRNItemDistributionRepository grnItemDistributionRepository;

    @Autowired
    PackagePaymentRepository packagePaymentRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    DepartmentMasterRepository departmentMasterRepository;

    @Autowired
    PackageCompleteChecklistRepository completeChecklistRepository;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackageSpecific, MethodPackageUpdateRequest mappingObject) {
        GRN grn = mappingObject.getGrn();
        if (grn == null) {
            return;
        }
        this.savePackageGRN(teqipPackageSpecific, grn.getGrnDetail());

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackageSpecific, MethodPackageResponse mappingObject) {

        GRN grn = new GRN();

        this.setPackageGrn(teqipPackageSpecific, grn);

        mappingObject.setGrn(grn);

    }

    private void setGrnItems(GRNDetail grnDetail, List<TeqipPackageGRNItemDetail> teqipPackageGRNItemDetails) {
        if (teqipPackageGRNItemDetails == null) {
            return;
        }
        ArrayList<GRNItem> grnItems = new ArrayList<>();

        for (TeqipPackageGRNItemDetail entity : teqipPackageGRNItemDetails) {
            GRNItem item = new GRNItem();

            item.setPackageGrnItemDetailId(entity.getPackGrnItemDetailId());
            item.setItemComment(entity.getItemComment());
            item.setItemcode(entity.getItemCode());
            item.setPageno(entity.getPageNumber());
            item.setReceivedQty(entity.getReceivedQty());
            item.setSerialno(entity.getSerialNumber());
            item.setStockRegistorName(entity.getStockRegistorName());
            item.setGst(entity.getGst());

            TeqipItemMaster teqipItemMaster = entity.getTeqipItemMaster();
            if (teqipItemMaster != null) {
                item.setItemMasterid(teqipItemMaster.getItemId());
                item.setItemName(teqipItemMaster.getItemName());
            }

            this.setItemDistribution(item, entity.getTeqipPackageGRNItemDistributions());

            grnItems.add(item);
        }
        grnDetail.setGrnItems(grnItems);

    }

    private TeqipPackageDocument savePackageDocument(TeqipPackage teqipPackage,
            Uploads upload,
            String categoryLookup) {

        if (upload == null) {
            return null;
        }
        upload.setDocumentCategory(categoryLookup);
        return uploadMapper.saveUpload(teqipPackage, upload);
    }

    private Uploads getPackageDocument(TeqipPackageDocument teqipPackageDocuments,TeqipPackage teqipPackage) {

        if (teqipPackageDocuments == null) {
            return null;
        }
        
        List<TeqipPackageDocument> list = new ArrayList<>();
        
        list.add(teqipPackageDocuments);
        ArrayList<Uploads> uploadList = this.uploadMapper.getUploads(teqipPackage,
                list,
                teqipPackageDocuments.getDocumentCategory());
        
        return uploadList != null & uploadList.size() > 0 ? uploadList.get(0) : null;
    }
        

    private void setItemDistribution(GRNItem item, List<TeqipPackageGRNItemDistribution> teqipPackageGRNItemDistributions) {
        if (teqipPackageGRNItemDistributions == null) {
            return;
        }
        ArrayList<GRNItemDistribution> grnItemDistributions = new ArrayList<>();

        for (TeqipPackageGRNItemDistribution entity : teqipPackageGRNItemDistributions) {
            GRNItemDistribution distribution = new GRNItemDistribution();
            distribution.setPackageGRNItemDistributionId(entity.getPackGrnItemDistributionId());

            distribution.setQuantity(entity.getQuantity());

            TeqipPmssDepartmentMaster teqipPmssDepartmentMaster = entity.getTeqipPmssDepartmentMaster();
            if (teqipPmssDepartmentMaster != null) {
                distribution.setDepartmentId(teqipPmssDepartmentMaster.getDepartmentId());
                distribution.setDepartmentName(teqipPmssDepartmentMaster.getDepartment());
            }

            grnItemDistributions.add(distribution);
        }

        item.setGrnItemDistributions(grnItemDistributions);

    }

    private void saveGrnItem(TeqipPackage teqipPackageSpecific,
            TeqipPackageGRNDetail teqipPackageGRNDetail,
            ArrayList<GRNItem> grnItems) {

        if (grnItems == null) {
            return;
        }
        for (GRNItem item : grnItems) {
            TeqipPackageGRNItemDetail entity = item.getPackageGrnItemDetailId() != null
                    ? this.grnItemDetailRepository.findOne(item.getPackageGrnItemDetailId())
                    : new TeqipPackageGRNItemDetail();
            entity.setTeqipPackage(teqipPackageSpecific);
            entity.setTeqipPackageGRNDetails(teqipPackageGRNDetail);

            entity.setItemComment(item.getItemComment());
            entity.setItemCode(item.getItemcode());
            entity.setPageNumber(item.getPageno());
            entity.setReceivedQty(item.getReceivedQty());
            entity.setSerialNumber(item.getSerialno());
            entity.setStockRegistorName(item.getStockRegistorName());
            entity.setGst(item.getGst());
            if (item.getItemMasterid() != null) {
                TeqipItemMaster itemMaster = itemRepository.findOne(item.getItemMasterid());
                entity.setTeqipItemMaster(itemMaster);
            }

            grnItemDetailRepository.saveAndFlush(entity);

            this.saveItemDistribution(entity, item.getGrnItemDistributions());

        }

    }

    private void saveItemDistribution(TeqipPackageGRNItemDetail teqipPackageGRNItemDetail, ArrayList<GRNItemDistribution> grnItemDistributions) {
        if (grnItemDistributions == null) {
            return;
        }

        for (GRNItemDistribution itemDist : grnItemDistributions) {
            TeqipPackageGRNItemDistribution entity = itemDist.getPackageGRNItemDistributionId() != null
                    ? grnItemDistributionRepository.findOne(itemDist.getPackageGRNItemDistributionId())
                    : new TeqipPackageGRNItemDistribution();
            entity.setTeqipPackageGRNItemDetails(teqipPackageGRNItemDetail);
            entity.setQuantity(itemDist.getQuantity());
            if (itemDist.getDepartmentId() != null) {
                TeqipPmssDepartmentMaster teqipPmssDepartmentMaster = departmentMasterRepository.findOne(itemDist.getDepartmentId());
                entity.setTeqipPmssDepartmentMaster(teqipPmssDepartmentMaster);
            }

            grnItemDistributionRepository.saveAndFlush(entity);
        }
    }

    private void savePackageGRN(TeqipPackage teqipPackageSpecific, ArrayList<GRNDetail> grnDetails) {

        if (grnDetails == null) {
            return;
        }
        for (GRNDetail grnDetail : grnDetails) {
            TeqipPackageGRNDetail entity = grnDetail.getPackageGRNDetailId() != null
                    ? grnDetailRepository.findOne(grnDetail.getPackageGRNDetailId())
                    : new TeqipPackageGRNDetail();

            entity.setGrnno(grnDetail.getGrnNumber());
            entity.setSupplyDate(grnDetail.getSupplyDate());

            if (grnDetail.getSupplierId() != null) {
                TeqipPmssSupplierMaster supplierMaster = supplierRepository.findOne(grnDetail.getSupplierId());
                entity.setTeqipPmssSupplierMaster(supplierMaster);
            }

            entity.setGrnDocument(this.savePackageDocument(teqipPackageSpecific,
                    grnDetail.getGrnUpload(),
                    "GRN_DETAIL"));

            entity.setAssetDocument(this.savePackageDocument(teqipPackageSpecific,
                    grnDetail.getAssetUpload(),
                    "ASSET_DETAIL"));

            entity.setTeqipPackage(teqipPackageSpecific);
            grnDetailRepository.saveAndFlush(entity);

            this.saveGrnItem(teqipPackageSpecific, entity, grnDetail.getGrnItems());

        }
    }

    private void setPackageGrn(TeqipPackage teqipPackageSpecific, GRN grn) {
        List<TeqipPackageGRNDetail> teqipPackageGRNDetails = teqipPackageSpecific.getTeqipPackageGRNDetails();
        if (teqipPackageGRNDetails == null) {
            return;
        }

        ArrayList<GRNDetail> grnDetails = new ArrayList<>();

        for (TeqipPackageGRNDetail entity : teqipPackageGRNDetails) {

            GRNDetail grnDetail = new GRNDetail();

            grnDetail.setPackageGRNDetailId(entity.getPackGrnDetailId());
            grnDetail.setGrnNumber(entity.getGrnno());
            grnDetail.setSupplyDate(entity.getSupplyDate());

            TeqipPmssSupplierMaster supplierMaster = entity.getTeqipPmssSupplierMaster();
            if (supplierMaster != null) {
                grnDetail.setSupplierId(supplierMaster.getSupplierId());
                grnDetail.setSupplierName(supplierMaster.getSupplierName());
            }

            grnDetail.setGrnUpload(this.getPackageDocument(entity.getGrnDocument(), teqipPackageSpecific));
            grnDetail.setAssetUpload(this.getPackageDocument(entity.getAssetDocument(),teqipPackageSpecific));

            Uploads packageDocument = this.getPackageDocument(entity.getGrnLetterDocument(), teqipPackageSpecific);
            if(packageDocument != null){
                ArrayList list  =  new ArrayList();
                list.add(packageDocument);
                grnDetail.setGrnlGeneration(list);
            }
            Uploads assetLetter = this.getPackageDocument(entity.getAssetLetterDocument(), teqipPackageSpecific);
            if(assetLetter != null){
                ArrayList list  =  new ArrayList();
                list.add(assetLetter);
                grnDetail.setAlGeneration(list);
            }

            this.setGrnItems(grnDetail, entity.getTeqipPackageGRNItemDetails());

            grnDetails.add(grnDetail);
        }
        grn.setGrnDetail(grnDetails);
    }

}
