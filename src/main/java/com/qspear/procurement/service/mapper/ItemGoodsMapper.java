/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.persistence.TeqipItemGoodsDetail;
import com.qspear.procurement.model.TeqipItemGoodsDepartmentBreakupModel;
import com.qspear.procurement.model.TeqipItemGoodsDetailModel;
import com.qspear.procurement.persistence.*;
import com.qspear.procurement.repositories.ItemRepository;
import com.qspear.procurement.repositories.method.DepartmentMasterRepository;
import com.qspear.procurement.repositories.ItemGoodDepartmentBreakupRepository;
import com.qspear.procurement.repositories.ItemGoodRepository;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jaspreet
 */
@Service
public class ItemGoodsMapper {

    @Autowired
    ItemGoodRepository itemGoodRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    ItemGoodDepartmentBreakupRepository itemGoodDepartmentBreakupRepository;

    @Autowired
    DepartmentMasterRepository departmentMasterRepository;

    public List<TeqipItemGoodsDetailModel> mapEntityToObject(List<TeqipItemGoodsDetail> teqipItemGoodsDetails) {
        if (teqipItemGoodsDetails == null) {
            return null;
        }

        List<TeqipItemGoodsDetailModel> list = new ArrayList<>();

        for (TeqipItemGoodsDetail entity : teqipItemGoodsDetails) {
            TeqipItemGoodsDetailModel model = new TeqipItemGoodsDetailModel();
            model.setId(entity.getId());
            model.setItemCostUnit(entity.getItemCostUnit());
            model.setItemSpecification(entity.getItemSpecification());
            model.setItemQnt(entity.getItemQnt());
            model.setReviseId(entity.getReviseId());
            model.setStatus(entity.getStatus());
            if (entity.getTeqipItemMaster() != null) {
                model.setTeqipItemMasterId(entity.getTeqipItemMaster().getItemId());
                model.setTeqipItemMaster(entity.getTeqipItemMaster().getItemName());
            }
            if (entity.getTeqipPmssDepartmentMaster() != null) {
                model.setTeqipPmssDepartmentMasterId(entity.getTeqipPmssDepartmentMaster().getDepartmentId());
                model.setTeqipPmssDepartmentMaster(entity.getTeqipPmssDepartmentMaster().getDepartment());

            }

            model.setTeqipPackageId(entity.getTeqipPackage().getPackageId());

            List<TeqipItemGoodsDepartmentBreakupModel> teqipItemGoodsDepartmentBreakups
                    = mapteqipDepartmentBreakups(entity.getTeqipItemGoodsDepartmentBreakups());

            model.setTeqipItemGoodsDepartmentBreakups(teqipItemGoodsDepartmentBreakups);

            list.add(model);

        }
        return list;

    }

    public List<TeqipItemGoodsDepartmentBreakupModel> mapteqipDepartmentBreakups(
            List<TeqipItemGoodsDepartmentBreakup> teqipItemGoodsDepartmentBreakups) {

        if (teqipItemGoodsDepartmentBreakups == null) {
            return null;
        }

        List<TeqipItemGoodsDepartmentBreakupModel> list = new ArrayList<>();

        for (TeqipItemGoodsDepartmentBreakup entity : teqipItemGoodsDepartmentBreakups) {
            TeqipItemGoodsDepartmentBreakupModel model = new TeqipItemGoodsDepartmentBreakupModel();
            model.setId(entity.getDepartmentBreakUpId());
            model.setQuantity(entity.getQuantity());
            if (entity.getTeqipPmssDepartmentMaster() != null) {
                model.setTeqipPmssDepartmentMasterId(entity.getTeqipPmssDepartmentMaster().getDepartmentId());
                model.setTeqipPmssDepartmentMaster(entity.getTeqipPmssDepartmentMaster().getDepartment());

            }
            if (entity.getTeqipItemGoodsDetail() != null) {
                model.setTeqipItemGoodsDetailId(entity.getTeqipItemGoodsDetail().getId());
            }

            list.add(model);

        }
        return list;

    }
    public List<TeqipItemGoodsDetailModel> mapItemGoodsHistory(List<TeqipItemGoodsDetailHistory> teqipItemGoodsDetails) {
        if (teqipItemGoodsDetails == null) {
            return null;
        }

        List<TeqipItemGoodsDetailModel> list = new ArrayList<>();

        for (TeqipItemGoodsDetailHistory entity : teqipItemGoodsDetails) {
            TeqipItemGoodsDetailModel model = new TeqipItemGoodsDetailModel();
            model.setId(entity.getId());
            model.setItemCostUnit(entity.getItemCostUnit());
            model.setItemSpecification(entity.getItemSpecification());
            model.setItemQnt(entity.getItemQnt());
            model.setReviseId(entity.getReviseId());
            model.setStatus(entity.getStatus());
            if (entity.getTeqipItemMaster() != null) {
                model.setTeqipItemMasterId(entity.getTeqipItemMaster().getItemId());
                model.setTeqipItemMaster(entity.getTeqipItemMaster().getItemName());
            }
            if (entity.getTeqipPmssDepartmentMaster() != null) {
                model.setTeqipPmssDepartmentMasterId(entity.getTeqipPmssDepartmentMaster().getDepartmentId());
                model.setTeqipPmssDepartmentMaster(entity.getTeqipPmssDepartmentMaster().getDepartment());

            }

            model.setTeqipPackageId(entity.getTeqipPackageHistory().getPackageHistoryId());

            list.add(model);

        }
        return list;

    }
    
   

    public void mapObjectToEntity(List<TeqipItemGoodsDetailModel> itemModel, TeqipPackage teqipPackage) {
        if (itemModel == null) {
            return;
        }

        List<Integer> newId = new ArrayList<>();

        List<TeqipItemGoodsDetail> olderItemGood = teqipPackage.getTeqipItemGoodsDetails();
        for (TeqipItemGoodsDetailModel model : itemModel) {
            TeqipItemGoodsDetail entity = model.getId() != null
                    ? itemGoodRepository.findOne(model.getId())
                    : new TeqipItemGoodsDetail();

            entity.setItemCostUnit(model.getItemCostUnit());
            entity.setItemQnt(model.getItemQnt());
            entity.setItemSpecification(model.getItemSpecification());
            entity.setStatus(model.getStatus());
            entity.setTeqipPackage(teqipPackage);
            entity.setTeqipCategorymaster(teqipPackage.getTeqipCategorymaster());

            if (entity.getReviseId() == null) {
                entity.setReviseId(1);
            }

            if (model.getTeqipItemMasterId() != null) {
                entity.setTeqipItemMaster(itemRepository.findOne(model.getTeqipItemMasterId()));
            }

            if (model.getTeqipPmssDepartmentMasterId() != null) {
                entity.setTeqipPmssDepartmentMaster(departmentMasterRepository.findOne(model.getTeqipPmssDepartmentMasterId()));

            }
            itemGoodRepository.saveAndFlush(entity);
            newId.add(entity.getId());

            this.saveItemGoodsBreakup(model.getTeqipItemGoodsDepartmentBreakups(), entity);

        }
        this.deleteItemGoods(olderItemGood, newId);
    }

    public void saveItemGoodsBreakup(List<TeqipItemGoodsDepartmentBreakupModel> teqipItemGoodsDepartmentBreakups,
            TeqipItemGoodsDetail itemGoodsDetail) {
        if (teqipItemGoodsDepartmentBreakups == null) {
            return;
        }

        List<Integer> newId = new ArrayList<>();
        List<TeqipItemGoodsDepartmentBreakup> breakupList = itemGoodsDetail.getTeqipItemGoodsDepartmentBreakups();
        for (TeqipItemGoodsDepartmentBreakupModel model : teqipItemGoodsDepartmentBreakups) {

            TeqipItemGoodsDepartmentBreakup entity = model.getId() != null
                    ? itemGoodDepartmentBreakupRepository.findOne(model.getId())
                    : new TeqipItemGoodsDepartmentBreakup();

            entity.setQuantity(model.getQuantity());
            entity.setTeqipItemGoodsDetail(itemGoodsDetail);
            if (model.getTeqipPmssDepartmentMasterId() != null) {
                entity.setTeqipPmssDepartmentMaster(departmentMasterRepository.findOne(model.getTeqipPmssDepartmentMasterId()));
            }
            if (entity.getReviseId() == null) {
                entity.setReviseId(1);
            }

            itemGoodDepartmentBreakupRepository.saveAndFlush(entity);
            newId.add(entity.getDepartmentBreakUpId());
        }
        this.deleteItemGoodsDepartmentBreakup(breakupList, newId);
    }

    @Transactional
    public void deleteItemGoods(List<TeqipItemGoodsDetail> olderItemGood, List<Integer> newId) {
        if (olderItemGood == null) {
            return;
        }

        for (TeqipItemGoodsDetail entity : olderItemGood) {
            if (!newId.contains(entity.getId())) {
                this.deleteItemGoodsDepartmentBreakup(entity.getTeqipItemGoodsDepartmentBreakups(), new ArrayList<Integer>());
                System.out.println("TeqipItemGoodsDetail" + entity.getId());
                itemGoodRepository.delete(entity.getId());
                itemGoodRepository.flush();

            }
        }

    }

    @Transactional
    public void deleteItemGoodsDepartmentBreakup(List<TeqipItemGoodsDepartmentBreakup> breakupList, List<Integer> newId) {
        if (breakupList == null) {
            return;
        }

        for (TeqipItemGoodsDepartmentBreakup entity : breakupList) {
            if (!newId.contains(entity.getDepartmentBreakUpId())) {
                System.out.println("TeqipItemGoodsDepartmentBreakup" + entity.getDepartmentBreakUpId());

                itemGoodDepartmentBreakupRepository.delete(entity.getDepartmentBreakUpId());
                itemGoodDepartmentBreakupRepository.flush();

            }
        }

    }

}
