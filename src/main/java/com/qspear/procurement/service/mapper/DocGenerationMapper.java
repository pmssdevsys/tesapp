/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.User;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.repositories.method.PackageDocumentRepository;
import com.qspear.procurement.service.UserControllerService;
import com.qspear.procurement.util.MethodUtility;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class DocGenerationMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackageDocumentRepository documentRepository;

    @Autowired
    UserControllerService userControllerService;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage entity, MethodPackageUpdateRequest mappingObject) {

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {
        Map<String, ArrayList<Uploads>> map = new HashMap<>();
        List<TeqipPackageDocument> teqipPackageDocuments = teqipPackage.getTeqipPackageDocuments();
        for (TeqipPackageDocument entity : teqipPackageDocuments) {

            if (MethodConstants.isAppDocCategory(entity.getDocumentCategory())) {
                Uploads uploadObject = uploadMapper.getUploadObject(teqipPackage, entity);
                
                ArrayList<Uploads> get = map.get(entity.getDocumentCategory());
                get = get != null ?get: new ArrayList<>();
                get.add(uploadObject);
                
                map.put(entity.getDocumentCategory(), get);
                
            }
        }
        mappingObject.setDocGeneration(map);
    }
    
}
