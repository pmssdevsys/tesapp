package com.qspear.procurement.service.mapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qspear.procurement.constants.ProcurementConstants;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.PackageDetails;
import com.qspear.procurement.model.methods.PriorReviewMetaInfo;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPmssCurrencymaster;
import com.qspear.procurement.persistence.TeqipPmssThreshholdmaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;
import com.qspear.procurement.persistence.User;
import org.springframework.stereotype.Service;
import com.qspear.procurement.service.AbstractPackageService;
import com.qspear.procurement.service.PMSSReferenceService;
import com.qspear.procurement.service.UserControllerService;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class PackageDetailsMapper extends AbstractPackageService implements MapperInterface<TeqipPackage> {

    @Autowired
    UserControllerService userControllerService;

    @Autowired
    PMSSReferenceService pmssrs;

    @Override
    public void mapObjectToEntity(TeqipPackage entity, MethodPackageUpdateRequest mappingObject) {
        PackageDetails pkgDetails = mappingObject.getPkgDetails();
        PriorReviewMetaInfo priorReviewMetaInfo = mappingObject.getPriorReviewMetaInfo();

        JsonNode packageMetaInfo = mappingObject.getPackageMetaInfo();

        if (pkgDetails == null && packageMetaInfo == null && priorReviewMetaInfo == null) {
            return;
        }

        if (pkgDetails != null) {
            entity.setPackageCode(pkgDetails.getPackageCode());
            entity.setPackageName(pkgDetails.getPackageName());
            entity.setPackageInitiationDate(pkgDetails.getInitiationDate());
            entity.setEstimatedCost(pkgDetails.getEstimatedCost());
            entity.setJustification(pkgDetails.getDescription());

            TeqipCategorymaster category = categoryRepository.findOne(pkgDetails.getCategoryId());
            entity.setTeqipCategorymaster(category);

            TeqipSubcategorymaster subCategory = teqipSubcategorymasterRepository.findOne(pkgDetails.getSubCategoryId());
            entity.setTeqipSubcategorymaster(subCategory);

            TeqipProcurementmethod procurementmethod = procurementmethodRepository.findOne(pkgDetails.getProcMethodId());
            entity.setTeqipProcurementmethod(procurementmethod);

            TeqipSubcomponentmaster subComponent = teqipSubcomponentmasterRepository.findOne(pkgDetails.getSubComponentId());
            entity.setTeqipSubcomponentmaster(subComponent);
            entity.setPurchaseType(pkgDetails.getPurchaseType());
        }

        if (priorReviewMetaInfo != null) {
            entity.setIsCurrentlyInPriorReview(priorReviewMetaInfo.getIsCurrentlyInPriorReview());
            entity.setRoleOfNextActor(priorReviewMetaInfo.getRoleOfNextActor());
        }

        if (packageMetaInfo != null) {
            entity.setIsPackageCompleted(packageMetaInfo.get("isCompleted") != null ? packageMetaInfo.get("isCompleted").intValue() : 0);
            entity.setIsPackageCancelled(packageMetaInfo.get("isCanceled") != null ? packageMetaInfo.get("isCanceled").intValue() : 0);
            entity.setCurrentStageIndex(packageMetaInfo.get("currentStageIndex") != null ? packageMetaInfo.get("currentStageIndex").intValue() : 0);
            entity.setCurrentStage(packageMetaInfo.get("currentStage") != null ? packageMetaInfo.get("currentStage").asText() : null);

            entity.setPackageMetaData(packageMetaInfo.toString());
        }

        teqipPackageRepository.saveAndFlush(entity);

    }

    @Override
    public void mapEntityToObject(TeqipPackage entity, MethodPackageResponse mappingObject) {

        PackageDetails packageDetails = new PackageDetails();

        packageDetails.setPackageId(entity.getPackageId());
        packageDetails.setPackageCode(entity.getPackageCode());

        packageDetails.setPackageName(entity.getPackageName());
        packageDetails.setInitiationDate(entity.getPackageInitiationDate());
        packageDetails.setServiceProviderId(entity.getServiceProviderId());
        packageDetails.setPurchaseType(entity.getPurchaseType());
        packageDetails.setIsOldPackage(entity.getIsOldPackage());

        TeqipCategorymaster teqipCategorymaster = entity.getTeqipCategorymaster();
        if (teqipCategorymaster != null) {
            packageDetails.setCategoryId(teqipCategorymaster.getCatId());
            packageDetails.setCategory(teqipCategorymaster.getCategoryName());
        }

        TeqipSubcategorymaster teqipSubcategorymaster = entity.getTeqipSubcategorymaster();
        if (teqipSubcategorymaster != null) {
            packageDetails.setSubCategoryId(teqipSubcategorymaster.getSubcatId());
            packageDetails.setSubCategory(teqipSubcategorymaster.getSubCategoryName());
        }

        TeqipSubcomponentmaster teqipSubcomponentmaster = entity.getTeqipSubcomponentmaster();
        if (teqipSubcomponentmaster != null) {
            packageDetails.setSubComponentId(teqipSubcomponentmaster.getId());
            packageDetails.setSubComponent(teqipSubcomponentmaster.getSubComponentName());
        }
        packageDetails.setEstimatedCost(entity.getEstimatedCost());

        packageDetails.setDescription(entity.getJustification());

        User user = entity.getCreatedBy() != null ? userControllerService.findByUserid(entity.getCreatedBy()) : null;
        if (user != null) {
            packageDetails.setCreatedBy(user.getUserName());
            packageDetails.setCreatedByRole(user.getTeqipRolemaster().getPmssRole());

            if (entity.getTeqipInstitution() != null
                    && entity.getTeqipInstitution().getTeqipInstitutiontype() != null
                    && "Centrally Funded Institution".equalsIgnoreCase(entity.getTeqipInstitution().getTeqipInstitutiontype().getInstitutiontypeName())) {
                packageDetails.setCreatedByRole("CFI");

            }
        }

        TeqipProcurementmethod teqipProcurementmethod = entity.getTeqipProcurementmethod();
        if (teqipProcurementmethod != null) {
            packageDetails.setProcMethodId(teqipProcurementmethod.getProcurementmethodId());
            packageDetails.setProcMethod(teqipProcurementmethod.getProcurementmethodName());

            if (teqipCategorymaster != null) {
                TeqipPmssCurrencymaster teqipPmssCurrencymaster = teqipPmssCurrencymasterRepository.findByCurrencyname(ProcurementConstants.USD_CURRENCY_CODE);

                List<TeqipPmssThreshholdmaster> retrieveThreshholdmasterList = pmssrs.retrieveThreshholdmasterList(
                        teqipCategorymaster.getCatId(), entity.getIsprop(), entity.getIsgem(), entity.getEstimatedCost());

                for (TeqipPmssThreshholdmaster teqipPmssThreshholdmaster : retrieveThreshholdmasterList) {
                    if (teqipPmssThreshholdmaster.getTeqipProcurementmethod() == null) {
                        continue;
                    }
                    if (Objects.equals(teqipPmssThreshholdmaster.getTeqipProcurementmethod().getProcurementmethodId(),
                            teqipProcurementmethod.getProcurementmethodId())) {
                        packageDetails.setMethodThresholdValue(teqipPmssThreshholdmaster.getThresholdMaxValue() * teqipPmssCurrencymaster.getCurrencyvalue());
                    }

                }
            }
        }
        mappingObject.setPkgDetails(packageDetails);

        if (entity.getIsCurrentlyInPriorReview() != null) {

            PriorReviewMetaInfo priorReviewMetaInfo = new PriorReviewMetaInfo();
            priorReviewMetaInfo.setIsCurrentlyInPriorReview(entity.getIsCurrentlyInPriorReview());
            priorReviewMetaInfo.setRoleOfNextActor(entity.getRoleOfNextActor());

            mappingObject.setPriorReviewMetaInfo(priorReviewMetaInfo);
        }

        String packageMetaData = entity.getPackageMetaData();
        if (packageMetaData != null) {
            try {
                JsonNode actualObj = new ObjectMapper().readTree(packageMetaData);
                mappingObject.setPackageMetaInfo(actualObj);

            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
