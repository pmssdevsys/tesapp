package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import static com.qspear.procurement.constants.MethodConstants.DOC_CAT_INVITATION_GOODS_DC;
import com.qspear.procurement.model.methods.InvitationLetter;
import com.qspear.procurement.model.methods.InvitationLetterDetail;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageInvitationLetter;
import com.qspear.procurement.repositories.method.InviationLetterRepository;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class InvitationLetterMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    InviationLetterRepository inviationLetterRepository;

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    QuestionMapper questionMapper;

    @Autowired
    ProcuringDepartmentMappper procuringDepartmentMappper;

    @Autowired
    PaymentMapper paymentMapper;

    @Autowired
    PackageSupplierMappper packageSupplierMappper;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage entity, MethodPackageUpdateRequest mappingObject) {

        InvitationLetter invitationLetter = mappingObject.getInvitationLetter();
        if (invitationLetter == null) {
            return;
        }

        this.saveInvitationLetter(entity, invitationLetter);

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        InvitationLetter invitationLetter = new InvitationLetter();

        this.setInvitationLetter(teqipPackage, invitationLetter);

        ArrayList<Uploads> uploads = uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_INVITATION_GOODS_BLANK);
        uploads.addAll(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_INVITATION_GOODS_DC));
        uploads.addAll(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_INVITATION_WORKS_BLANK));
        uploads.addAll(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_INVITATION_QCBS));
        invitationLetter.setIlGeneration(uploads);

        mappingObject.setInvitationLetter(invitationLetter);

    }

    private void setInvitationLetter(TeqipPackage teqipPackage, InvitationLetter object) {

        TeqipPackageInvitationLetter entity = inviationLetterRepository.findByTeqipPackage(teqipPackage);

        if (entity == null) {
            return;
        }
        InvitationLetterDetail invitationLetter = new InvitationLetterDetail();
        invitationLetter.setInvitationLetterId(entity.getInvitationLetterId());
        invitationLetter.setQuotationValidityRequired(entity.getQuotationValidityRequired());
        invitationLetter.setTrainingClause(entity.getTrainingClause());
        invitationLetter.setWarranty(entity.getWarranty());
        invitationLetter.setWorkCompletionPeriod(entity.getWorkCompletionPeriod());
        invitationLetter.setLiquidatedDamages(entity.getMinliquidatedDamages());
        invitationLetter.setLastSubmissionDate(entity.getLastSubmissionDate());
        invitationLetter.setLastSubmissionTime(entity.getLastSubmissionTime());
        invitationLetter.setInstallationClause(entity.getInstallationClause());
        invitationLetter.setAmc(entity.getAmc());
        invitationLetter.setPlannedBidOpeningDate(entity.getPlannedBidOpeningDate());
        invitationLetter.setPlannedBidOpeningTime(entity.getPlannedBidOpeningTime());
        invitationLetter.setMaxLiquidatedDamages(entity.getMaxliquidatedDamages());
        invitationLetter.setQuotedPrice(entity.getQuotedPrice());
        invitationLetter.setPerformanceSecurityInPercentage(entity.getPerformanceSecurityInPercentage());
        invitationLetter.setNumberOfBids(entity.getNumberOfBids());
        object.setDetails(invitationLetter);

    }

    private void saveInvitationLetter(TeqipPackage teqipPackage, InvitationLetter object) {
        if (object == null || object.getDetails() == null) {
            return;
        }

        InvitationLetterDetail invitationLetter = object.getDetails();
        TeqipPackageInvitationLetter entity = inviationLetterRepository.findByTeqipPackage(teqipPackage);

        entity = entity != null ? entity : new TeqipPackageInvitationLetter();

        entity.setTeqipPackage(teqipPackage);
        entity.setQuotationValidityRequired(invitationLetter.getQuotationValidityRequired());
        entity.setTrainingClause(invitationLetter.getTrainingClause());
        entity.setWarranty(invitationLetter.getWarranty());
        entity.setWorkCompletionPeriod(invitationLetter.getWorkCompletionPeriod());
        entity.setMinliquidatedDamages(invitationLetter.getLiquidatedDamages());
        entity.setLastSubmissionDate(invitationLetter.getLastSubmissionDate());
        entity.setLastSubmissionTime(invitationLetter.getLastSubmissionTime());
        entity.setInstallationClause(invitationLetter.getInstallationClause());
        entity.setAmc(invitationLetter.getAmc());
        entity.setPlannedBidOpeningDate(invitationLetter.getPlannedBidOpeningDate());
        entity.setPlannedBidOpeningTime(invitationLetter.getPlannedBidOpeningTime());
        entity.setMaxliquidatedDamages(invitationLetter.getMaxLiquidatedDamages());
        entity.setQuotedPrice(invitationLetter.getQuotedPrice());
        entity.setPerformanceSecurityInPercentage(invitationLetter.getPerformanceSecurityInPercentage());
         entity.setNumberOfBids(invitationLetter.getNumberOfBids());
        inviationLetterRepository.saveAndFlush(entity);

    }

}
