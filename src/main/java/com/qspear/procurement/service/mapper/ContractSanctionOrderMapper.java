package com.qspear.procurement.service.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qspear.procurement.model.methods.ContractSanctionOrder;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageContractSanctionOrder;
import com.qspear.procurement.repositories.method.ContractSanctionOrderRepository;

@Service
public class ContractSanctionOrderMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    ContractSanctionOrderRepository contractSanctionOrderRepository;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        ContractSanctionOrder model = mappingObject.getContractSanctionOrder();
        if (model == null) {
            return;
        }

        TeqipPackageContractSanctionOrder entity = contractSanctionOrderRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackageContractSanctionOrder();
        entity.setTeqipPackage(teqipPackage);
        entity.setContractNumber(model.getContractNumber());
        entity.setContractDate(model.getContractDate());
        entity.setSanctionDate(model.getSanctionDate());
        entity.setSanctionNumber(model.getSanctionNumber());
        contractSanctionOrderRepository.saveAndFlush(entity);

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {
        // TODO Auto-generated method stub

        TeqipPackageContractSanctionOrder entity = contractSanctionOrderRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return;
        }

        ContractSanctionOrder sactiondetails = new ContractSanctionOrder();
        sactiondetails.setContractSanctionId(entity.getContractSanctionId());
        sactiondetails.setContractNumber(entity.getContractNumber());
        sactiondetails.setContractDate(entity.getContractDate());
        sactiondetails.setSanctionDate(entity.getSanctionDate());
        sactiondetails.setSanctionNumber(entity.getSanctionNumber());

        mappingObject.setContractSanctionOrder(sactiondetails);

    }
}
