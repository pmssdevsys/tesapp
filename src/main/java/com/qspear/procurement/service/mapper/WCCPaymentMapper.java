package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.AmendmentDetail;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.WCCPayments;
import com.qspear.procurement.model.methods.WorkCompletionCertificates;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAmendmentdetail;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderCertificate;
import com.qspear.procurement.repositories.method.AmendmentdetailRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import com.qspear.procurement.repositories.method.WorkOrderCertificateRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class WCCPaymentMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    AmendmentdetailRepository amendmentdetailRepository;

    @Autowired
    WorkOrderCertificateRepository workOrderCertificateRepository;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        WCCPayments wccPayments = mappingObject.getWccPayments();
        if (wccPayments == null) {
            return;
        }

        this.saveAmendmentDetail(teqipPackage, wccPayments.getAmmendmentDetails());
        this.saveWorkCompletionCertificate(teqipPackage, wccPayments.getWorkCompletionCertificates());

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        WCCPayments wccPayments = new WCCPayments();
        wccPayments.setAmmendmentDetails(this.setAmendmentDetail(teqipPackage));
        wccPayments.setWorkCompletionCertificates(this.setWorkCompletionCertificate(teqipPackage));
        mappingObject.setWccPayments(wccPayments);
    }

    private void saveAmendmentDetail(TeqipPackage teqipPackage, AmendmentDetail model) {
        if (model == null) {
            return;
        }

        TeqipPackageAmendmentdetail entity = amendmentdetailRepository.findByTeqipPackage(teqipPackage);

        entity = entity != null ? entity : new TeqipPackageAmendmentdetail();

        entity.setTeqipPackage(teqipPackage);
        entity.setIsAmendmentRequired(model.getIsAmmendmentRequired());
        entity.setWorkCompletionDate(model.getWorkCompletionDate());
        entity.setPerfSecurityExpiryDate(model.getPerformanceSecurityExpiryDate());
        entity.setPerfSecurityAmount(model.getPerformanceSecurityAmount());
        entity.setPeriodExpiryDate(model.getMaintainencePeriodExpiryDate());
        entity.setIsAmmendmentComplete(model.getIsAmmendmentComplete());

        amendmentdetailRepository.saveAndFlush(entity);

    }

    private AmendmentDetail setAmendmentDetail(TeqipPackage teqipPackage) {

        TeqipPackageAmendmentdetail entity = amendmentdetailRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return null;
        }
        AmendmentDetail model = new AmendmentDetail();

        model.setMaintainencePeriodExpiryDate(entity.getPeriodExpiryDate());
        model.setPerformanceSecurityAmount(entity.getPerfSecurityAmount());
        model.setPerformanceSecurityExpiryDate(entity.getPerfSecurityExpiryDate());
        model.setWorkCompletionDate(entity.getWorkCompletionDate());
        model.setIsAmmendmentRequired(entity.getIsAmendmentRequired());
        model.setIsAmmendmentComplete(entity.getIsAmmendmentComplete());
        return model;
    }

    private void saveWorkCompletionCertificate(TeqipPackage teqipPackage, List<WorkCompletionCertificates> workCompletionCertificates) {
        if (workCompletionCertificates == null) {
            return;
        }

        for (WorkCompletionCertificates model : workCompletionCertificates) {

            TeqipPackageWorkOrderCertificate entity = null;
            if (model.getWorkCompletionCertificateId() != null) {
                entity = workOrderCertificateRepository.findOne(model.getWorkCompletionCertificateId());
            }
            entity = entity != null ? entity : new TeqipPackageWorkOrderCertificate();
            entity.setTeqipPackage(teqipPackage);

            entity.setActualCompletionDate(model.getActualDateOfCompletion());
            entity.setBalanceWork(model.getBalanceWork());
            entity.setComment(model.getComments());
            entity.setWoNumber(model.getWoNumber());
            entity.setWorkDone(model.getWorkDone());

            workOrderCertificateRepository.saveAndFlush(entity);
        }

    }

    private List<WorkCompletionCertificates> setWorkCompletionCertificate(TeqipPackage teqipPackage) {

        List<TeqipPackageWorkOrderCertificate> certificates = workOrderCertificateRepository.findByTeqipPackage(teqipPackage);
        if (certificates == null) {
            return null;
        }
        List<WorkCompletionCertificates> list = new ArrayList<>();

        for (TeqipPackageWorkOrderCertificate entity : certificates) {
            WorkCompletionCertificates model = new WorkCompletionCertificates();

            model.setWorkCompletionCertificateId(entity.getPackageWoWccId());
            model.setActualDateOfCompletion(entity.getActualCompletionDate());
            model.setBalanceWork(entity.getBalanceWork());
            model.setComments(entity.getComment());
            model.setWoNumber(entity.getWoNumber());
            model.setWorkDone(entity.getWorkDone());

            list.add(model);
        }

        return list;
    }

}
