package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.TeqipItemWorkDetailModel;
import com.qspear.procurement.model.methods.Items;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.ProcuringDepartmentDetails;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipItemGoodsDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssDepartmentMaster;
import com.qspear.procurement.repositories.method.DepartmentMasterRepository;
import com.qspear.procurement.repositories.ItemGoodDepartmentBreakupRepository;
import com.qspear.procurement.repositories.ItemGoodRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class ProcuringDepartmentMappper implements MapperInterface<TeqipPackage> {

    @Autowired
    ItemGoodsMapper itemGoodsMapper;

    @Autowired
    ItemWorkMapper itemWorkMapper;

    @Autowired
    ItemGoodRepository itemGoodRepository;

    @Autowired
    ItemGoodDepartmentBreakupRepository itemGoodDepartmentBreakupRepository;

    @Autowired
    DepartmentMasterRepository departmentMasterRepository;

    ArrayList<Items> getItems(TeqipPackage teqipPackage) {
        List<TeqipItemGoodsDetail> teqipItemGoodsDetails = teqipPackage.getTeqipItemGoodsDetails();

        if (teqipItemGoodsDetails == null) {
            return null;
        }

        ArrayList<Items> items = new ArrayList<>();

        //Load Item According with Breakups
        for (TeqipItemGoodsDetail entity : teqipItemGoodsDetails) {

            Items item = new Items();

            item.setItemId(entity.getId());
            item.setItemDescription(entity.getItemSpecification());
            item.setEstimatedCost(entity.getItemCostUnit());
            item.setDeliveyPeriod(entity.getDeliveyPeriod());
            item.setTrainingRequired(entity.getTrainingRequired());
            item.setInstallationRequired(entity.getInstallationRequired());
            item.setItemmainspecification(entity.getItemMainSpecification());
            item.setPlaceofdelivery(entity.getPlaceOfDelivery());
            item.setInstllationrequirement(entity.getInstllationRequirement());

            //Item Master
            TeqipItemMaster teqipItemMaster = entity.getTeqipItemMaster();
            if (teqipItemMaster != null) {
                item.setItemMasterId(teqipItemMaster.getItemId());
                item.setItemName(teqipItemMaster.getItemName());
            }

            //Set Breakup Details
            item.setQuantity(entity.getItemQnt());
            TeqipPmssDepartmentMaster teqipPmssDepartmentMaster = entity.getTeqipPmssDepartmentMaster();
            if (teqipPmssDepartmentMaster != null) {
                item.setDepartmentId(teqipPmssDepartmentMaster.getDepartmentId());
                item.setDepartment(teqipPmssDepartmentMaster.getDepartment());

            }

            //Set Item breakup
            item.setItemBreakUp(itemGoodsMapper.mapteqipDepartmentBreakups(entity.getTeqipItemGoodsDepartmentBreakups()));

            items.add(item);
        }

        return items;
    }

    private void saveItems(ArrayList<Items> items) {
        if (items == null) {
            return;
        }
        for (Items item : items) {
            TeqipItemGoodsDetail entity = item.getItemId() != null
                    ? itemGoodRepository.findOne(item.getItemId())
                    : new TeqipItemGoodsDetail();

            entity.setItemSpecification(item.getItemDescription());
            entity.setDeliveyPeriod(item.getDeliveyPeriod());
            entity.setTrainingRequired(item.getTrainingRequired());
            entity.setInstallationRequired(item.getInstallationRequired());
            entity.setItemMainSpecification(item.getItemmainspecification());
            entity.setPlaceOfDelivery(item.getPlaceofdelivery());
            entity.setInstllationRequirement(item.getInstllationrequirement());

            itemGoodRepository.saveAndFlush(entity);

            //Set Breakup Details
            itemGoodsMapper.saveItemGoodsBreakup(item.getItemBreakUp(), entity);

        }
    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {
        ArrayList<Items> items = getItems(teqipPackage);

        //Filter Items
        Map<Integer, ProcuringDepartmentDetails> map = new HashMap<>();

        if (items != null) {
            for (Items item : items) {

                Integer departmentId = item.getDepartmentId();

                ProcuringDepartmentDetails department = map.containsKey(departmentId) ? map.get(departmentId) : new ProcuringDepartmentDetails();
                department.setDepartmentName(item.getDepartment());

                ArrayList<Items> subItems = department.getItems() != null ? department.getItems() : new ArrayList<>();

                subItems.add(item);

                department.setItems(subItems);

                map.put(departmentId, department);

            }
        }

        List<TeqipItemWorkDetailModel> works = itemWorkMapper.mapEntityToObject(teqipPackage.getTeqipItemWorkDetails());

        if (works != null) {
            for (TeqipItemWorkDetailModel item : works) {

                Integer departmentId = item.getTeqipPmssDepartmentMasterId();

                ProcuringDepartmentDetails department = map.containsKey(departmentId) ? map.get(departmentId) : new ProcuringDepartmentDetails();
                department.setDepartmentName(item.getTeqipPmssDepartmentMaster());

                ArrayList<TeqipItemWorkDetailModel> subItems = department.getWorks() != null ? department.getWorks() : new ArrayList<>();

                subItems.add(item);

                department.setWorks(subItems);

                map.put(departmentId, department);

            }
        }

        response.setProcuringDepartmentDetails(new ArrayList(map.values()));

    }

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {
        ArrayList<ProcuringDepartmentDetails> procuringDepartmentDetails = methodPackageUpdateRequest.getProcuringDepartmentDetails();

        if (procuringDepartmentDetails == null) {
            return;
        }

        for (ProcuringDepartmentDetails procuringDepartmentDetail : procuringDepartmentDetails) {

            saveItems(procuringDepartmentDetail.getItems());

            itemWorkMapper.mapObjectToEntity(procuringDepartmentDetail.getWorks(), teqipPackage, false);
        }

    }

}
