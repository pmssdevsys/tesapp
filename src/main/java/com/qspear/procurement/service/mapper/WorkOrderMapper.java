package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.model.methods.WorkOrder;
import com.qspear.procurement.model.methods.WorkOrderWorks;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrder;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrderDetail;
import com.qspear.procurement.repositories.method.SupplierRepository;
import com.qspear.procurement.repositories.method.WorkOrderDetailRepository;
import com.qspear.procurement.repositories.method.WorkOrderRepository;
import com.qspear.procurement.util.ProcurementUtility;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class WorkOrderMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    WorkOrderRepository workOrderRepository;

    @Autowired
    WorkOrderDetailRepository workOrderDetailRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    ProcurementUtility procurementUtility;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {

        WorkOrder model = mappingObject.getWorkOrder();
        if (model == null) {
            return;
        }

        TeqipPackageWorkOrder entity = workOrderRepository.findByTeqipPackage(teqipPackage);

        entity = entity != null ? entity : new TeqipPackageWorkOrder();

        entity.setTeqipPackage(teqipPackage);
        entity.setBasicvalue(model.getBasicValue());
        entity.setContractstartdate(model.getContractStartDate());
        entity.setContractvalue(model.getContractValue());
        entity.setPerfsecurityexpdate(model.getPerformanceSecurityExpiryDate());
        entity.setPerfsecurityrcvd(model.getPerformanceSecurityReceivedDate());
        entity.setSumapplicable(model.getSumOfApplicableTaxes());
        entity.setWorkcompletiondate(model.getWorkCompletionDate());
        entity.setWorkstartdate(model.getWorkStartDate());
        entity.setWorkGeneratedDate(model.getWorkGeneratedDate());
        entity.setPerformanceSecurityAmount(model.getPerformanceSecurityAmount());

        if (entity.getWorkOrderNo() == null) {
            entity.setWorkOrderNo(teqipPackage.getPackageCode());
        }
        workOrderRepository.saveAndFlush(entity);
//        if (entity.getWorkOrderNo() == null) {
//
//            String poNumber = procurementUtility.getPackageCode();
//            poNumber = poNumber
//                    + teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode()
//                    + "/" + entity.getPackageWorkorderId();
//
//            entity.setWorkOrderNo(poNumber);
//            workOrderRepository.saveAndFlush(entity);
//
//        }

        this.saveWorkOrderWork(model.getWorks(), teqipPackage);

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        TeqipPackageWorkOrder entity = workOrderRepository.findByTeqipPackage(teqipPackage);

        if (entity == null) {
            return;
        }

        WorkOrder model = new WorkOrder();
        model.setPackageWorkOrderId(entity.getPackageWorkorderId());
        model.setBasicValue(entity.getBasicvalue());
        model.setContractStartDate(entity.getContractstartdate());
        model.setContractValue(entity.getContractvalue());
        model.setPerformanceSecurityExpiryDate(entity.getPerfsecurityexpdate());
        model.setPerformanceSecurityReceivedDate(entity.getPerfsecurityrcvd());
        model.setSumOfApplicableTaxes(entity.getSumapplicable());
        model.setWorkCompletionDate(entity.getWorkcompletiondate());
        model.setWorkStartDate(entity.getWorkstartdate());
        model.setWorkOrderNo(entity.getWorkOrderNo());
        model.setWorkGeneratedDate(entity.getWorkGeneratedDate());
        model.setPerformanceSecurityAmount(entity.getPerformanceSecurityAmount());
        model.setWorks(setWorkOrderWork(teqipPackage));

        ArrayList<Uploads> uploads = uploadMapper.getUploads(teqipPackage,
                teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_WORK_ORDER);
        
        model.setWoGeneration(uploads);
        mappingObject.setWorkOrder(model);
    }

    public void saveWorkOrderWork(ArrayList<WorkOrderWorks> works, TeqipPackage teqipPackage) {

        if (works == null) {
            return;
        }

        for (WorkOrderWorks work : works) {

            TeqipPackageWorkOrderDetail entity = work.getWoWorkId() != null
                    ? workOrderDetailRepository.findOne(work.getWoWorkId())
                    : new TeqipPackageWorkOrderDetail();
            entity.setTeqipPackage(teqipPackage);
            entity.setTeqipPmssSupplierMaster(supplierRepository.findOne(work.getSupplierId()));

            entity.setWorkCost(work.getWorkCost());
            entity.setWorkName(work.getWorkName());
            entity.setWorkEstimatedCost(work.getWorkEstimatedCost());
            entity.setComment(work.getComments());
            entity.setWorkSpecification(work.getWorkSpecification());

            workOrderDetailRepository.saveAndFlush(entity);

        }
    }

    public ArrayList<WorkOrderWorks> setWorkOrderWork(TeqipPackage teqipPackage) {

        List<TeqipPackageWorkOrderDetail> works = workOrderDetailRepository.findByTeqipPackage(teqipPackage);
        if (works == null) {
            return null;
        }

        ArrayList<WorkOrderWorks> workList = new ArrayList<>();

        for (TeqipPackageWorkOrderDetail evaluationDetail : works) {

            WorkOrderWorks work = new WorkOrderWorks();

            work.setWoWorkId(evaluationDetail.getWorkOrderDetailId());
            if (evaluationDetail.getTeqipPmssSupplierMaster() != null) {
                work.setSupplierId(evaluationDetail.getTeqipPmssSupplierMaster().getSupplierId());
                work.setSupplierName(evaluationDetail.getTeqipPmssSupplierMaster().getSupplierName());
            }
            work.setWorkCost(evaluationDetail.getWorkCost());
            work.setWorkName(evaluationDetail.getWorkName());
            work.setWorkEstimatedCost(evaluationDetail.getWorkEstimatedCost());
            work.setComments(evaluationDetail.getComment());
            work.setWorkSpecification(evaluationDetail.getWorkSpecification());

            workList.add(work);

        }

        return workList;
    }

}
