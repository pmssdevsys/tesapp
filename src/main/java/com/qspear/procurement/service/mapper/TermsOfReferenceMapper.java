/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.BankNOCDetails;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.TermsOfReference;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageTermReference;
import com.qspear.procurement.repositories.method.TermReferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class TermsOfReferenceMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    TermReferenceRepository termReferenceRepository;

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {

        TeqipPackageTermReference entity = termReferenceRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return;
        }
        TermsOfReference termsOfReference = new TermsOfReference();
        BankNOCDetails model = new BankNOCDetails();
        model.setId(entity.getTermOfReferenceId());
        model.setIsWordWorkBankNOC(entity.getIsworldBankNoc());
        model.setWordBankNOCDate(entity.getWorldBankNocDate());

        termsOfReference.setBankNOCDetails(model);
        response.setTermsOfReference(termsOfReference);

    }

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {

        TermsOfReference termsOfReference = methodPackageUpdateRequest.getTermsOfReference();
        if (termsOfReference == null) {
            return;
        }

        BankNOCDetails model = termsOfReference.getBankNOCDetails();
        if (model == null) {
            return;
        }
        TeqipPackageTermReference entity = termReferenceRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackageTermReference();
        entity.setTeqipPackage(teqipPackage);

        entity.setIsworldBankNoc(model.getIsWordWorkBankNOC());
        entity.setWorldBankNocDate(model.getWordBankNOCDate());

        termReferenceRepository.saveAndFlush(entity);

    }

}
