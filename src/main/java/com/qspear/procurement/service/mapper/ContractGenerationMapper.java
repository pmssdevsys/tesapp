/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.AwardGenerationChecklists;
import com.qspear.procurement.model.methods.ContractDetails;
import com.qspear.procurement.model.methods.ContractGeneration;
import com.qspear.procurement.model.methods.FinancialProposalDetails;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.NegitiationDetails;
import com.qspear.procurement.model.methods.RecommendationDetails;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQCBSContractGeneration;
import com.qspear.procurement.repositories.method.QCBSContractDetailRepository;
import com.qspear.procurement.repositories.method.QCBSContractGenerationRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import com.qspear.procurement.service.mapper.MapperInterface;
import com.qspear.procurement.util.ProcurementUtility;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class ContractGenerationMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    QCBSContractDetailRepository qcbsContractDetailRepository;

    @Autowired
    QCBSContractGenerationRepository qcbsContractGenerationRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    UploadMapper uploadMapper;
    @Autowired
    ProcurementUtility procurementUtility;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        ContractGeneration model = mappingObject.getContractGeneration();

        if (model == null) {
            return;
        }

        ContractDetails contractDetails = model.getContractDetails();
        TeqipPackageQCBSContractDetail entityDetail = null;
        if (contractDetails != null) {
            entityDetail = qcbsContractDetailRepository.findByTeqipPackage(teqipPackage);
            entityDetail = entityDetail != null ? entityDetail : new TeqipPackageQCBSContractDetail();
            entityDetail.setTeqipPackage(teqipPackage);

            entityDetail.setMemberInCharge(contractDetails.getMemberInCharge());
            entityDetail.setContractStartDate(contractDetails.getContractStartDate());
            entityDetail.setAuthorizedRepresentativeOfClient(contractDetails.getAuthorizedRepresentativeOfClient());
            entityDetail.setCommencementServiceDate(contractDetails.getCommencementServiceDate());
            entityDetail.setDeliveryPeriodInMonths(contractDetails.getDeliveryPeriodInMonths());
            entityDetail.setArbitrationProceedingHeld(contractDetails.getArbitrationProceedingHeldAt());
            entityDetail.setInterestPercentageOnDelayedPayments(contractDetails.getInterestPercentageOnDelayedPayments());
            entityDetail.setBasicValue(contractDetails.getBasicValue());
            entityDetail.setContractValue(contractDetails.getContractValue());
            entityDetail.setApplicableTax(contractDetails.getApplicableTax());

        }
        AwardGenerationChecklists awardGenerationChecklists = model.getAwardGenerationChecklists();
        if (awardGenerationChecklists != null) {
            if (entityDetail == null) {
                entityDetail = qcbsContractDetailRepository.findByTeqipPackage(teqipPackage);
                entityDetail = entityDetail != null ? entityDetail : new TeqipPackageQCBSContractDetail();
                entityDetail.setTeqipPackage(teqipPackage);
            }
            entityDetail.setIsAdvancePaymentRequired(awardGenerationChecklists.getIsAdvancePaymentRequired());
            entityDetail.setPaymentMilestonesDefined(awardGenerationChecklists.getPaymentMilestonesDefined());
            entityDetail.setIsBankGuaranteeSubmitted(awardGenerationChecklists.getIsBankGuaranteeSubmitted());
            entityDetail.setIsArbitratorAgreed(awardGenerationChecklists.getIsArbitratorAgreed());

        }
        FinancialProposalDetails financialProposalDetails = model.getFinancialProposalDetails();
        if (financialProposalDetails != null) {
            if (entityDetail == null) {
                entityDetail = qcbsContractDetailRepository.findByTeqipPackage(teqipPackage);
                entityDetail = entityDetail != null ? entityDetail : new TeqipPackageQCBSContractDetail();
                entityDetail.setTeqipPackage(teqipPackage);
            }
            entityDetail.setIsWordWorkBankNOC(financialProposalDetails.getIsWordWorkBankNOC());
            entityDetail.setWordBankNOCDate(financialProposalDetails.getWordBankNOCDate());
            entityDetail.setProposalAmount(financialProposalDetails.getProposalAmount());

        }
        if (entityDetail != null) {
            if (entityDetail.getContractNumber() == null) {
                  entityDetail.setContractNumber(teqipPackage.getPackageCode());
             }
        
            qcbsContractDetailRepository.saveAndFlush(entityDetail);

//            if (entityDetail.getContractNumber() == null) {
//                String poNumber = procurementUtility.getPackageCode();
//                poNumber = poNumber
//                        + teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode()
//                        + "/" + entityDetail.getContractDetailId();
//                entityDetail.setContractNumber(poNumber);
//                qcbsContractDetailRepository.saveAndFlush(entityDetail);
//            }

        }

        RecommendationDetails recommendationDetails = model.getRecommendationDetails();
        TeqipPackageQCBSContractGeneration entityGeneration = null;
        if (recommendationDetails != null) {

            if (entityGeneration == null) {
                entityGeneration = qcbsContractGenerationRepository.findByTeqipPackage(teqipPackage);
                entityGeneration = entityGeneration != null ? entityGeneration : new TeqipPackageQCBSContractGeneration();
                entityGeneration.setTeqipPackage(teqipPackage);
            }
            if (recommendationDetails.getL1SuplierId() != null) {
                entityGeneration.setTeqipPmssSupplierMaster(supplierRepository.findOne(recommendationDetails.getL1SuplierId()));
            }
            entityGeneration.setRecommendationComments(recommendationDetails.getRecommendationComments());
            if (recommendationDetails.getContractValue() != null) {
                entityGeneration.setContractValue(recommendationDetails.getContractValue());
            }

        }
        NegitiationDetails negitiationDetails = model.getNegitiationDetails();
        if (negitiationDetails != null) {

            if (entityGeneration == null) {
                entityGeneration = qcbsContractGenerationRepository.findByTeqipPackage(teqipPackage);
                entityGeneration = entityGeneration != null ? entityGeneration : new TeqipPackageQCBSContractGeneration();
                entityGeneration.setTeqipPackage(teqipPackage);
            }

            if (negitiationDetails.getL1SuplierId() != null) {
                entityGeneration.setTeqipPmssSupplierMaster(supplierRepository.findOne(negitiationDetails.getL1SuplierId()));
            }
            entityGeneration.setRecommendationComments(negitiationDetails.getRecommendationComments());

            entityGeneration.setNegotiationComments(negitiationDetails.getNegotiationComments());
            entityGeneration.setIsNegotiationSuccessful(negitiationDetails.getIsNegotiationSuccessful());
            entityGeneration.setDateOfNegotiation(negitiationDetails.getDateOfNegotiation());

            if (negitiationDetails.getContractValue() != null) {
                entityGeneration.setContractValue(negitiationDetails.getContractValue());
            }
        }

        if (entityGeneration != null) {
            qcbsContractGenerationRepository.saveAndFlush(entityGeneration);
        }

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        TeqipPackageQCBSContractDetail contractDetailEntity = qcbsContractDetailRepository.findByTeqipPackage(teqipPackage);
        TeqipPackageQCBSContractGeneration entityGeneration = qcbsContractGenerationRepository.findByTeqipPackage(teqipPackage);
        if (contractDetailEntity == null && entityGeneration == null) {
            return;
        }
        ContractGeneration contractGeneration = new ContractGeneration();
        if (contractDetailEntity != null) {
            ContractDetails contractDetailModel = new ContractDetails();

            contractDetailModel.setContractNumber(contractDetailEntity.getContractNumber());
            contractDetailModel.setMemberInCharge(contractDetailEntity.getMemberInCharge());
            contractDetailModel.setContractStartDate(contractDetailEntity.getContractStartDate());
            contractDetailModel.setAuthorizedRepresentativeOfClient(contractDetailEntity.getAuthorizedRepresentativeOfClient());
            contractDetailModel.setCommencementServiceDate(contractDetailEntity.getCommencementServiceDate());
            contractDetailModel.setDeliveryPeriodInMonths(contractDetailEntity.getDeliveryPeriodInMonths());
            contractDetailModel.setArbitrationProceedingHeldAt(contractDetailEntity.getArbitrationProceedingHeld());
            contractDetailModel.setInterestPercentageOnDelayedPayments(contractDetailEntity.getInterestPercentageOnDelayedPayments());
            contractDetailModel.setBasicValue(contractDetailEntity.getBasicValue());
            contractDetailModel.setContractValue(contractDetailEntity.getContractValue());
            contractDetailModel.setApplicableTax(contractDetailEntity.getApplicableTax());

            contractGeneration.setContractDetails(contractDetailModel);

            AwardGenerationChecklists awardGenerationChecklists = new AwardGenerationChecklists();

            awardGenerationChecklists.setIsAdvancePaymentRequired(contractDetailEntity.getIsAdvancePaymentRequired());
            awardGenerationChecklists.setPaymentMilestonesDefined(contractDetailEntity.getPaymentMilestonesDefined());
            awardGenerationChecklists.setIsBankGuaranteeSubmitted(contractDetailEntity.getIsBankGuaranteeSubmitted());
            awardGenerationChecklists.setIsArbitratorAgreed(contractDetailEntity.getIsArbitratorAgreed());

            contractGeneration.setAwardGenerationChecklists(awardGenerationChecklists);

            FinancialProposalDetails financialProposalDetails = new FinancialProposalDetails();

            financialProposalDetails.setIsWordWorkBankNOC(contractDetailEntity.getIsWordWorkBankNOC());
            financialProposalDetails.setWordBankNOCDate(contractDetailEntity.getWordBankNOCDate());
            financialProposalDetails.setProposalAmount(contractDetailEntity.getProposalAmount());

            contractGeneration.setFinancialProposalDetails(financialProposalDetails);

        }

        if (entityGeneration != null) {
            RecommendationDetails recommendationDetails = new RecommendationDetails();
            if (entityGeneration.getTeqipPmssSupplierMaster() != null) {
                recommendationDetails.setL1SuplierId(entityGeneration.getTeqipPmssSupplierMaster().getSupplierId());
                recommendationDetails.setL1SupplierName(entityGeneration.getTeqipPmssSupplierMaster().getSupplierName());
            }
            recommendationDetails.setRecommendationComments(entityGeneration.getRecommendationComments());
            recommendationDetails.setContractValue(entityGeneration.getContractValue());

            contractGeneration.setRecommendationDetails(recommendationDetails);

            NegitiationDetails negitiationDetails = new NegitiationDetails();

            if (entityGeneration.getTeqipPmssSupplierMaster() != null) {
                negitiationDetails.setL1SuplierId(entityGeneration.getTeqipPmssSupplierMaster().getSupplierId());
                negitiationDetails.setL1SupplierName(entityGeneration.getTeqipPmssSupplierMaster().getSupplierName());
            }

            negitiationDetails.setRecommendationComments(entityGeneration.getRecommendationComments());
            negitiationDetails.setContractValue(entityGeneration.getContractValue());
            negitiationDetails.setNegotiationComments(entityGeneration.getNegotiationComments());
            negitiationDetails.setIsNegotiationSuccessful(entityGeneration.getIsNegotiationSuccessful());
            negitiationDetails.setDateOfNegotiation(entityGeneration.getDateOfNegotiation());

            contractGeneration.setNegitiationDetails(negitiationDetails);
        }

        ArrayList<Uploads> uploads = uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_QCBS_CONTRACT);
        contractGeneration.setcGeneration(uploads);

        mappingObject.setContractGeneration(contractGeneration);

    }

}
