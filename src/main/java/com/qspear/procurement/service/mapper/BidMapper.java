package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.Bid;
import com.qspear.procurement.model.methods.BidDetail;
import com.qspear.procurement.model.methods.BidINSTAndSCC;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.PriorReview;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageBidDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageBidINSTAndSCC;
import com.qspear.procurement.repositories.method.PackageBidDetailRepository;
import com.qspear.procurement.repositories.method.PackageBidINSTAndSCCRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jaspreets on 4/4/2018.
 */
@Service
public class BidMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PaymentMapper paymentMapper;

    @Autowired
    PackageBidDetailRepository packageBidDetailRepository;

    @Autowired
    PackageBidINSTAndSCCRepository packageBidINSTAndSCCRepository;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        Bid genofBid = mappingObject.getGenofBid();

        if (genofBid == null) {
            return;
        }
        this.saveBidDetail(teqipPackage, genofBid.getBidDetail());
        this.saveBidINSTAndSCC(teqipPackage, genofBid.getSccInformation());

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {
        Bid genofBid = new Bid();

        genofBid.setBidDetail(this.setBidDetail(teqipPackage));
        genofBid.setSccInformation(this.setBidINSTAndSCC(teqipPackage));
       
        ArrayList<Uploads> uploads = uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_NCB_SBD);
        uploads.addAll(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_NCB_SBD_WORK));
        genofBid.setBidGeneration(uploads);

        mappingObject.setGenofBid(genofBid);

    }

    private void saveBidDetail(TeqipPackage teqipPackage, BidDetail model) {

        if (model == null) {
            return;
        }
        TeqipPackageBidDetail entity = packageBidDetailRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackageBidDetail();

        entity.setTeqipPackage(teqipPackage);

        entity.setSalebidDocument(model.getSbdDate());
        entity.setLastreceiptBidDate(model.getLastDateReceiptsofBids());
        entity.setLastSbdDate(model.getLastDateSBD());
        entity.setOpeningDate(model.getDateOpeningBid());
        entity.setValidityQuotation(model.getValidityQuotation());
        entity.setBidsecurityValidity(model.getBidSecurityValidity());
        entity.setCostbidDocument(model.getCostOfBidDocument());
        packageBidDetailRepository.saveAndFlush(entity);

    }

    private void saveBidINSTAndSCC(TeqipPackage teqipPackage, BidINSTAndSCC model) {

        if (model == null) {
            return;
        }
        TeqipPackageBidINSTAndSCC entity = packageBidINSTAndSCCRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackageBidINSTAndSCC();

        entity.setTeqipPackage(teqipPackage);

        entity.setChequeFavour(model.getDraftInFavour());
        entity.setPayableAt(model.getPayableAt());
        entity.setBidsellStartTime(model.getBdsStartTime());
        entity.setBidsellEndTime(model.getBdsEndTime());
        entity.setPostalCharge(model.getPostalCharges());
        entity.setBidSecurity(model.getBidSecurity());
        entity.setBidOfficer(model.getInvitingOfficer());
        entity.setPrebidMeetingDate(model.getPrebidMeetingDate());
        entity.setPrebidmeetingDateTime(model.getPrebidMeetingTime());
        entity.setWarranty(model.getWarranty());
        entity.setArbitrationPlace(model.getArbitrationProceeding());
        entity.setAddressofNotice(model.getAddressPurchaserNotice());
        entity.setMinLiquidatedDamage(model.getLiquidatedDamages());
        entity.setMaxLiquidatedDamage(model.getLiquidatedDamagesMaximum());
        entity.setPlaceOfOpeningOfBids(model.getPlaceOfOpeningOfBids());
        entity.setPerformanceSecurity(model.getPerformanceSecurity());
        packageBidINSTAndSCCRepository.saveAndFlush(entity);

    }

    private BidDetail setBidDetail(TeqipPackage teqipPackage) {

        TeqipPackageBidDetail entity = packageBidDetailRepository.findByTeqipPackage(teqipPackage);

        if (entity == null) {
            return null;
        }

        BidDetail model = new BidDetail();

        model.setBiddetailId(entity.getBiddetailId());
        model.setSbdDate(entity.getSalebidDocument());
        model.setLastDateReceiptsofBids(entity.getLastreceiptBidDate());
        model.setLastDateSBD(entity.getLastSbdDate());
        model.setDateOpeningBid(entity.getOpeningDate());
        model.setValidityQuotation(entity.getValidityQuotation());
        model.setBidSecurityValidity(entity.getBidsecurityValidity());
        model.setCostOfBidDocument(entity.getCostbidDocument());

        return model;

    }

    private BidINSTAndSCC setBidINSTAndSCC(TeqipPackage teqipPackage) {
        TeqipPackageBidINSTAndSCC entity = packageBidINSTAndSCCRepository.findByTeqipPackage(teqipPackage);

        if (entity == null) {
            return null;
        }

        BidINSTAndSCC model = new BidINSTAndSCC();

        model.setBidinstructionId(entity.getBidinstructionId());
        model.setPackageName(teqipPackage.getPackageName());
        model.setPackageCode(teqipPackage.getPackageCode());

        model.setDraftInFavour(entity.getChequeFavour());
        model.setPayableAt(entity.getPayableAt());
        model.setBdsStartTime(entity.getBidsellStartTime());
        model.setBdsEndTime(entity.getBidsellEndTime());
        model.setPostalCharges(entity.getPostalCharge());
        model.setBidSecurity(entity.getBidSecurity());
        model.setInvitingOfficer(entity.getBidOfficer());
        model.setPrebidMeetingDate(entity.getPrebidMeetingDate());
        model.setPrebidMeetingTime(entity.getPrebidmeetingDateTime());
        model.setWarranty(entity.getWarranty());
        model.setArbitrationProceeding(entity.getArbitrationPlace());
        model.setAddressPurchaserNotice(entity.getAddressofNotice());
        model.setLiquidatedDamages(entity.getMinLiquidatedDamage());
        model.setLiquidatedDamagesMaximum(entity.getMaxLiquidatedDamage());
        model.setPlaceOfOpeningOfBids(entity.getPlaceOfOpeningOfBids());
        model.setPerformanceSecurity(entity.getPerformanceSecurity());
        return model;
    }

}
