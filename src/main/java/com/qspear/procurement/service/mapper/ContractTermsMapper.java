/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.ContractTerms;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageContractTerms;
import com.qspear.procurement.repositories.method.ContractTermsRepository;
import com.qspear.procurement.service.mapper.MapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class ContractTermsMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    ContractTermsRepository contractTermsRepository;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        ContractTerms model = mappingObject.getContractTerms();
        if (model == null) {
            return;
        }

        TeqipPackageContractTerms entity = contractTermsRepository.findByTeqipPackage(teqipPackage);

        entity = entity != null ? entity : new TeqipPackageContractTerms();
        entity.setTeqipPackage(teqipPackage);

        entity.setTypeOfContract(model.getTypeOfContract());
        entity.setBillingCycle(model.getBillingCycle());
        entity.setContractValue(model.getContractValue());
        entity.setAmount(model.getAmount());
        entity.setFrequency(model.getFrequency());
        entity.setContractPeriod(model.getContractPeriod());
        entity.setFees(model.getFees());
        entity.setContractStartDate(model.getContractStartDate());

        contractTermsRepository.saveAndFlush(entity);

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        TeqipPackageContractTerms entity = contractTermsRepository.findByTeqipPackage(teqipPackage);

        if (entity == null) {
            return;
        }
        ContractTerms model = new ContractTerms();

        model.setTypeOfContract(entity.getTypeOfContract());
        model.setBillingCycle(entity.getBillingCycle());
        model.setContractValue(entity.getContractValue());
        model.setAmount(entity.getAmount());
        model.setFrequency(entity.getFrequency());
        model.setContractPeriod(entity.getContractPeriod());
        model.setFees(entity.getFees());
        model.setContractStartDate(entity.getContractStartDate());

        mappingObject.setContractTerms(model);
    }

}
