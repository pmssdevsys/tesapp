package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.CompleteCheckList;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageCompleteCheckList;
import com.qspear.procurement.repositories.method.PackageBidAdvertisementRepository;
import com.qspear.procurement.repositories.method.PackageCompleteChecklistRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class CompleteCheckListMapper implements MapperInterface<TeqipPackage> {
    
    @Autowired
    SupplierRepository supplierRepository;
    
    @Autowired
    PackageBidAdvertisementRepository packageBidAdvertisementRepository;
    
    @Autowired
    PackageSupplierMappper packageSupplierMappper;
    
    @Autowired
    PackageCompleteChecklistRepository completeChecklistRepository;
    
    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        CompleteCheckList completeCheckList = mappingObject.getCompleteCheckLists();
        
        if (completeCheckList == null) {
            return;
        }
        
        TeqipPackageCompleteCheckList entity = completeChecklistRepository.findByTeqipPackage(teqipPackage);
                
        entity =  entity != null ?entity : new TeqipPackageCompleteCheckList();
       
        entity.setEnvchecklistUpdated(completeCheckList.getEnvchecklistUpdated());
        entity.setGoodReceived(completeCheckList.getGoodReceived());
        entity.setInstallationDone(completeCheckList.getInstallationDone());
        entity.setPaymentDone(completeCheckList.getPaymentDone());
        entity.setProcurementDate(completeCheckList.getProcurementDate());
        entity.setTrainingDone(completeCheckList.getTrainingDone());
        entity.setWorkCompleted(completeCheckList.getWorkCompleted());
        if (completeCheckList.getSupplierId() != null) {
            entity.setTeqipPmssSupplierMaster(supplierRepository.findOne(completeCheckList.getSupplierId()));
        }
        
        entity.setTeqipPackage(teqipPackage);
        completeChecklistRepository.saveAndFlush(entity);
        
    }
    
    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {
        
        TeqipPackageCompleteCheckList entity  = completeChecklistRepository.findByTeqipPackage(teqipPackage);
        
        if (entity == null) {
            return;
        }
        
        CompleteCheckList completeCheckList = new CompleteCheckList();
        
        completeCheckList.setPackageCompleteCheckListId(entity.getPackageCompleteCheckListId());
        completeCheckList.setEnvchecklistUpdated(entity.getEnvchecklistUpdated());
        completeCheckList.setGoodReceived(entity.getGoodReceived());
        completeCheckList.setInstallationDone(entity.getInstallationDone());
        completeCheckList.setPaymentDone(entity.getPaymentDone());
        completeCheckList.setProcurementDate(entity.getProcurementDate());
        completeCheckList.setTrainingDone(entity.getTrainingDone());
        completeCheckList.setWorkCompleted(entity.getWorkCompleted());
        if (entity.getTeqipPmssSupplierMaster() != null) {
            completeCheckList.setSupplierId(entity.getTeqipPmssSupplierMaster().getSupplierId());
            completeCheckList.setSupplierName(entity.getTeqipPmssSupplierMaster().getSupplierName());
        }
        
        mappingObject.setCompleteCheckLists(completeCheckList);
    }
    
}
