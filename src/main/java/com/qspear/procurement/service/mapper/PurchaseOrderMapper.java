package com.qspear.procurement.service.mapper;

import com.qspear.procurement.repositories.ItemGoodRepository;
import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.*;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.*;
import com.qspear.procurement.repositories.ItemRepository;
import com.qspear.procurement.repositories.method.*;
import com.qspear.procurement.util.ProcurementUtility;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class PurchaseOrderMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackagePurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PackagePurchaseOrderDetailRepository purchaseOrderDetailRepository;

    @Autowired
    ItemGoodRepository itemGoodRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PaymentMapper paymentMapper;

    @Autowired
    ProcurementUtility procurementUtility;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage entity, MethodPackageUpdateRequest mappingObject) {

        PurchaseOrder purchaseOrder = mappingObject.getPurchaseOrder();
        if (purchaseOrder == null) {
            return;
        }

        this.savePurchaseOrder(entity, purchaseOrder.getGeneratePo());

        this.savePurchaseOrderItem(purchaseOrder.getItems(), entity);

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackageSpecific, MethodPackageResponse mappingObject) {
        PurchaseOrder purchaseOrder = new PurchaseOrder();

        TeqipPackagePurchaseOrder entity = purchaseOrderRepository.findByTeqipPackage(teqipPackageSpecific);
        if (entity != null) {
            GeneratePO generatePo = new GeneratePO();

            generatePo.setPackagePurchaseOrderId(entity.getPackagePurchaseOrderId());
            TeqipPmssSupplierMaster supplierMaster = entity.getTeqipPmssSupplierMaster();
            if (supplierMaster != null) {
                generatePo.setSupplierId(supplierMaster.getSupplierId());
                generatePo.setSupplierName(supplierMaster.getSupplierName());

            }
            generatePo.setPoNumber(entity.getPurchaseOrderNo());
            generatePo.setPoDate(entity.getPoDate());
//            generatePo.setGst(entity.getGstPercentage());
            generatePo.setOctroi(entity.getOctroiOther());
            generatePo.setEvaluatedCost(entity.getEvaluatedCost());
            generatePo.setPerformanceSecurityAmount(entity.getPerformanceSecurityAmount());
            generatePo.setTotalBaseCost(entity.getTotalBaseCost());
            generatePo.setTotalContractValue(entity.getTotalContractValue());
            generatePo.setIsPoFinalised(entity.getIsPoFinalised());

            purchaseOrder.setGeneratePo(generatePo);

            purchaseOrder.setItems(this.setPurchaseOrderItem(teqipPackageSpecific));

        }
        ArrayList<Uploads> uploads = uploadMapper.getUploads(teqipPackageSpecific,
                teqipPackageSpecific.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_PURCHASE_ORDER);
        uploads.addAll(uploadMapper.getUploads(teqipPackageSpecific,
                teqipPackageSpecific.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_PURCHASE_ORDER_DC));
        uploads.addAll(uploadMapper.getUploads(teqipPackageSpecific,
                teqipPackageSpecific.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_PURCHASE_ORDER_NCB));

        purchaseOrder.setPoGeneration(uploads);
        mappingObject.setPurchaseOrder(purchaseOrder);
    }

    private void savePurchaseOrder(TeqipPackage teqipPackage, GeneratePO generatePo) {

        if (generatePo == null) {
            return;
        }

        TeqipPackagePurchaseOrder entity = purchaseOrderRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackagePurchaseOrder();

//        entity.setGstPercentage(generatePo.getGst());
        entity.setOctroiOther(generatePo.getOctroi());
        entity.setEvaluatedCost(generatePo.getEvaluatedCost());
        entity.setPerformanceSecurityAmount(generatePo.getPerformanceSecurityAmount());
        entity.setPoDate(generatePo.getPoDate());
        entity.setTotalBaseCost(generatePo.getTotalBaseCost());
        entity.setTotalContractValue(generatePo.getTotalContractValue());
        entity.setIsPoFinalised(generatePo.getIsPoFinalised());
        entity.setTeqipPackage(teqipPackage);

        if (generatePo.getSupplierId() != null) {
            TeqipPmssSupplierMaster supplierMaster = supplierRepository.findOne(generatePo.getSupplierId());
            entity.setTeqipPmssSupplierMaster(supplierMaster);
        }
       
        if (entity.getPurchaseOrderNo() == null) {
            entity.setPurchaseOrderNo(teqipPackage.getPackageCode());
        }

        purchaseOrderRepository.saveAndFlush(entity);
//        if (entity.getPurchaseOrderNo() == null) {
//            String poNumber = procurementUtility.getPackageCode();
//            poNumber = poNumber
//                    + teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode()
//                    + "/" + entity.getPackagePurchaseOrderId();
//            entity.setPurchaseOrderNo(poNumber);
//            purchaseOrderRepository.saveAndFlush(entity);
//        }

    }

    public void savePurchaseOrderItem(ArrayList<PurchaseOrderItems> items, TeqipPackage teqipPackage) {
        if (items == null) {
            return;
        }
        for (PurchaseOrderItems item : items) {

            TeqipPackagePurchaseOrderDetail entity = item.getPoItemId() != null
                    ? purchaseOrderDetailRepository.findOne(item.getPoItemId())
                    : new TeqipPackagePurchaseOrderDetail();

            entity.setTeqipPackage(teqipPackage);
            entity.setTeqipPmssSupplierMaster(supplierRepository.findOne(item.getSupplierId()));
            entity.setTeqipItemMaster(itemRepository.findOne(item.getItemMasterId()));
            entity.setItemQty(item.getItemQty());
            entity.setBasicCost(item.getBasicCost());
            entity.setTotalPrice(item.getTotalPrice());
            entity.setComment(item.getComments());
            entity.setGst(item.getGst());
            entity.setHsn(item.getHsn());

            purchaseOrderDetailRepository.saveAndFlush(entity);

        }
    }

    public ArrayList<PurchaseOrderItems> setPurchaseOrderItem(TeqipPackage teqipPackage) {

        List<TeqipPackagePurchaseOrderDetail> details = purchaseOrderDetailRepository.findByTeqipPackage(teqipPackage);

        if (details == null) {
            return null;
        }
        ArrayList<PurchaseOrderItems> items = new ArrayList<>();

        for (TeqipPackagePurchaseOrderDetail entity : details) {

            PurchaseOrderItems item = new PurchaseOrderItems();

            item.setPoItemId(entity.getPurchaseOrderDetailId());
            if (entity.getTeqipItemMaster() != null) {
                item.setItemMasterId(entity.getTeqipItemMaster().getItemId());
                item.setItemName(entity.getTeqipItemMaster().getItemName());
            }

            if (entity.getTeqipPmssSupplierMaster() != null) {
                item.setSupplierId(entity.getTeqipPmssSupplierMaster().getSupplierId());
                item.setSupplierName(entity.getTeqipPmssSupplierMaster().getSupplierName());
            }
            item.setItemQty(entity.getItemQty());
            item.setBasicCost(entity.getBasicCost());
            item.setTotalPrice(entity.getTotalPrice());
            item.setComments(entity.getComment());
            item.setGst(entity.getGst());
            item.setHsn(entity.getHsn());

            items.add(item);

        }

        return items;
    }

}
