package com.qspear.procurement.service.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qspear.procurement.model.methods.EOIExtension;
import com.qspear.procurement.model.methods.Extension;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageEOIExtension;
import com.qspear.procurement.repositories.method.EOIExtensionRepository;
@Service
public class EOIExtensionMapper implements MapperInterface<TeqipPackage>{

	 @Autowired
	 EOIExtensionRepository eoiextensionRepository;
         
         @Override
	    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {

	        ArrayList<EOIExtension> eoiextensions = methodPackageUpdateRequest.getEoiextensions();

	        if (eoiextensions == null) {
	            return;
	        }

	        for (EOIExtension details : eoiextensions) {

	        	TeqipPackageEOIExtension entity = details.getEoiextensionId() != null
	                    ? eoiextensionRepository.findOne(details.getEoiextensionId())
	                    : new TeqipPackageEOIExtension();
	            entity.setTeqipPackage(teqipPackage);
	            entity.setExtendedDate(details.getExtendedDate());
	            entity.setReasonforExtension(details.getReasonforExtension());
	            entity.setSubmissionDate(details.getSubmissionDate());

	            eoiextensionRepository.saveAndFlush(entity);

	        }
	    }
         
	    @Override
	    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {

	        List<TeqipPackageEOIExtension> list = eoiextensionRepository.findByTeqipPackage(teqipPackage);
	        if (list == null) {
	            return;
	        }

	        ArrayList<EOIExtension> eoiextensions = new ArrayList<>();

	        for (TeqipPackageEOIExtension entity : list) {

	         EOIExtension details = new EOIExtension();
	            details.setEoiextensionId(entity.getEoiextensionId());
	            details.setExtendedDate(entity.getExtendedDate());
	            details.setReasonforExtension(entity.getReasonforExtension());
	            details.setSubmissionDate(entity.getSubmissionDate());

	            eoiextensions.add(details);

	        }
	        response.setEoiextensions(eoiextensions);
	    }

	    

}
