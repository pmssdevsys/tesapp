package com.qspear.procurement.service.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.EMDDetails;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageEMDDetails;
import com.qspear.procurement.repositories.method.EMDDetailsRepository;
@Service
public class EMDDetailsMapper implements MapperInterface<TeqipPackage> {
    
	@Autowired
	EMDDetailsRepository eMDDetailsRepository;

	@Override
	public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
		// TODO Auto-generated method stub
                
                 EMDDetails model = mappingObject.getEmdDetails();

                    if (model == null) {
                        return;
                    }
		 TeqipPackageEMDDetails entity = eMDDetailsRepository.findByTeqipPackage(teqipPackage);
		 
                    entity=entity!=null?entity:new TeqipPackageEMDDetails();
                    
                    entity.setTeqipPackage(teqipPackage);
                    entity.setBankName(model.getBankName());
                    entity.setBranchName(model.getBranchName());
                    entity.setEmdPercentage(model.getEmdPercentage());
                    entity.setEmdAmount(model.getEmdAmount());
                    entity.setBankAccountHolder(model.getBankAccountHolder());
                    entity.setAccountNumber(model.getAccountNumber());
                    entity.setIfscCode(model.getIfscCode());
                    entity.setRemarks(model.getRemarks());
                    entity.setIsEMDRequired(model.getIsEMDRequired());
		 
                     eMDDetailsRepository.saveAndFlush(entity);
	}

	@Override
	public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {
		// TODO Auto-generated method stub
                TeqipPackageEMDDetails entity = eMDDetailsRepository.findByTeqipPackage(teqipPackage);
		 if (entity == null) {
	            return;
                 }
	        
	        EMDDetails emdetails=new EMDDetails();
	       
                    emdetails.setBankName(entity.getBankName());
                    emdetails.setBranchName(entity.getBranchName());
                    emdetails.setEmdPercentage(entity.getEmdPercentage());
                    emdetails.setEmdAmount(entity.getEmdAmount());
                    emdetails.setBankAccountHolder(entity.getBankAccountHolder());
                    emdetails.setAccountNumber(entity.getAccountNumber());
                    emdetails.setIfscCode(entity.getIfscCode());
                    emdetails.setRemarks(entity.getRemarks());
                    emdetails.setIsEMDRequired(entity.getIsEMDRequired());
	      
                mappingObject.setEmdDetails(emdetails);
	        

	}

}
