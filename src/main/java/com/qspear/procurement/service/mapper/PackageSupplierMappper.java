package com.qspear.procurement.service.mapper;

import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSException;
import com.qspear.procurement.exception.SupplierAlreadyExists;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.SupplierDetails;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class PackageSupplierMappper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    UploadMapper uploadMapper;

    public List<SupplierDetails> mapSupplierMaster(List<TeqipPmssSupplierMaster> findByPmssType) {
        if (findByPmssType == null) {
            return null;
        }

        ArrayList<SupplierDetails> supplierDetails = new ArrayList<>();

        for (TeqipPmssSupplierMaster entity : findByPmssType) {

            SupplierDetails details = mapSupplier(entity);

            supplierDetails.add(details);

        }
        return supplierDetails;
    }

    public ArrayList<SupplierDetails> mapPackageSupplier(List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails) {

        ArrayList<SupplierDetails> supplierDetails = new ArrayList<>();

        for (TeqipPackageSupplierDetail supplierDetail : teqipPackageSupplierDetails) {
            TeqipPmssSupplierMaster entity = supplierDetail.getTeqipPmssSupplierMaster();

            if (entity == null) {
                continue;
            }

            SupplierDetails details = mapSupplier(entity);
            details.setPackageSupplierId(supplierDetail.getPackageSupplierDetailId());
            details.setIsShortlisted(supplierDetail.getIsShortListed());
            details.setComments(supplierDetail.getComments());
            details.setSupplierRegistrationNumber(supplierDetail.getSupplierRegistrationNumber());
            details.setIsQuotationSent(supplierDetail.getIsQuotationSent());
            details.setIsEmailSent(supplierDetail.getIsEmailSent());
            details.setQuotationNumber(supplierDetail.getQuotationNumber());
            details.setQuotationDate(supplierDetail.getQuotationDate());
            details.setQuotedPrice(supplierDetail.getQuotedPrice());
            details.setQuotationFileLink(getLink(supplierDetail.getQuotationDocument(), supplierDetail.getTeqipPackage()));

            details.setInvitationLetterLink(getLink(supplierDetail.getInvitationDocument(), supplierDetail.getTeqipPackage()));           
            if (supplierDetail.getInvitationDocument() != null) {
                details.setInvitationLetterFileName(supplierDetail.getInvitationDocument().getOriginalFileName());
            }

            details.setInvitationLetterFinancialLink(getLink(supplierDetail.getInvitationFinancialDocument(), supplierDetail.getTeqipPackage()));           
            if (supplierDetail.getInvitationFinancialDocument() != null) {
                details.setInvitationLetterFinancialFileName(supplierDetail.getInvitationFinancialDocument().getOriginalFileName());
            }

            supplierDetails.add(details);

        }
        return supplierDetails;
    }

    private String getLink(TeqipPackageDocument teqipPackageDocuments, TeqipPackage teqipPackage) {

        if (teqipPackageDocuments == null) {
            return null;
        }

        List<TeqipPackageDocument> list = new ArrayList<>();

        list.add(teqipPackageDocuments);
        ArrayList<Uploads> uploadList = this.uploadMapper.getUploads(teqipPackage,
                list,
                teqipPackageDocuments.getDocumentCategory());

        return uploadList != null & uploadList.size() > 0 ? uploadList.get(0).getFileNameLink() : null;
    }

    public SupplierDetails mapSupplier(TeqipPmssSupplierMaster entity) {

        SupplierDetails details = new SupplierDetails();
        details.setSupplierId(entity.getSupplierId());
        details.setSupplierName(entity.getSupplierName());
        details.setAddress(entity.getSupllierAddress());
        details.setPhoneNo(entity.getSupplierPhoneno());
        details.setEmailId(entity.getSuplierEmailId());
        details.setPanNo(entity.getSuplierPanNumber());
        details.setTanNo(entity.getSuplierTanNumber());
        details.setTaxNo(entity.getSuplierTaxNumber());
        details.setFaxNo(entity.getSuplierFaxNumber());
        details.setCountry(entity.getSupplierCountry());
        details.setNationality(entity.getSupplierNationality());
        details.setCity(entity.getSupplierCity());
        details.setSupplierSource(entity.getSupplierSource());
        details.setSpecificSource(entity.getSupplierSpecificSource());
        details.setPinCode(entity.getSupplierPincode());
        details.setState(entity.getSupplierState());
        details.setReprensentative(entity.getSupplierRepresentativeName());
        details.setLetter(entity.getSupliereLetter());
        details.setPmssType(entity.getPmssType());
        details.setRemarks(entity.getRemarks());
        details.setGstNo(entity.getGstNo());
        details.setEpfNo(entity.getEpfNo());
        details.setEsiNo(entity.getEsiNo());
        details.setContractLabourLicenseNo(entity.getContractLabourLicenseNo());
        return details;

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {

        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();
        if (teqipPackageSupplierDetails == null) {
            return;
        }
        response.setSupplierDetails(mapPackageSupplier(teqipPackageSupplierDetails));
    }

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {
        ArrayList<SupplierDetails> supplierDetails = methodPackageUpdateRequest.getSupplierDetails();

        if (supplierDetails == null) {
            return;
        }

        for (SupplierDetails details : supplierDetails) {

            if(  (details.getPackageSupplierId() == null ||  details.getPackageSupplierId() <=0 )
                    && details.getSupplierId() != null && details.getSupplierId()>0){
                TeqipPmssSupplierMaster supplier = supplierRepository.findOne(details.getSupplierId());
                TeqipPackageSupplierDetail packageSupplier = packageSupplierRepository.findByTeqipPackageAndTeqipPmssSupplierMaster(teqipPackage, supplier);
                if(packageSupplier != null){
                    throw new SupplierAlreadyExists( "Supplier Already Exists");
                }
            }
            
            TeqipPmssSupplierMaster entity = details.getSupplierId() != null
                    ? supplierRepository.findOne(details.getSupplierId())
                    : new TeqipPmssSupplierMaster();

            entity.setSupplierName(details.getSupplierName());
            entity.setSupllierAddress(details.getAddress());
            entity.setSupplierPhoneno(details.getPhoneNo());
            entity.setSuplierEmailId(details.getEmailId());
            entity.setSuplierPanNumber(details.getPanNo());
            entity.setSuplierTanNumber(details.getTanNo());
            entity.setSuplierTaxNumber(details.getTaxNo());
            entity.setSuplierFaxNumber(details.getFaxNo());
            entity.setSupplierCountry(details.getCountry());
            entity.setSupplierNationality(details.getNationality());
            entity.setSupplierCity(details.getCity());
            entity.setSupplierSource(details.getSupplierSource());
            entity.setSupplierSpecificSource(details.getSpecificSource());
            entity.setSupplierPincode(details.getPinCode());
            entity.setSupplierState(details.getState());
            entity.setSupplierRepresentativeName(details.getReprensentative());
            entity.setSupliereLetter(details.getLetter());
            entity.setPmssType(details.getPmssType());
            entity.setRemarks(details.getRemarks());
            entity.setGstNo(details.getGstNo());
            entity.setEpfNo(details.getEpfNo());
            entity.setEsiNo(details.getEsiNo());
            entity.setContractLabourLicenseNo(details.getContractLabourLicenseNo());

            supplierRepository.saveAndFlush(entity);

            TeqipPackageSupplierDetail packageSupplier = details.getPackageSupplierId() != null
                    ? packageSupplierRepository.findOne(details.getPackageSupplierId())
                    : new TeqipPackageSupplierDetail();

            packageSupplier.setTeqipPackage(teqipPackage);
            packageSupplier.setTeqipPmssSupplierMaster(entity);
            packageSupplier.setIsShortListed(details.getIsShortlisted());
            packageSupplier.setComments(details.getComments());
            packageSupplier.setSupplierRegistrationNumber(details.getSupplierRegistrationNumber());
            packageSupplier.setIsQuotationSent(details.getIsQuotationSent());
            packageSupplier.setIsEmailSent(details.getIsEmailSent());
            packageSupplier.setQuotationNumber(details.getQuotationNumber());
            packageSupplier.setQuotationDate(details.getQuotationDate());
            packageSupplier.setQuotedPrice(details.getQuotedPrice());

            packageSupplierRepository.saveAndFlush(packageSupplier);

        }
    }

}
