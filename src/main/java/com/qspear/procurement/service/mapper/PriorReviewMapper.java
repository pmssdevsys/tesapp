package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.PriorReview;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipRolemaster;
import com.qspear.procurement.persistence.User;
import com.qspear.procurement.persistence.methods.TeqipPackagePriorReview;
import com.qspear.procurement.repositories.method.PriorReviewRepository;
import com.qspear.procurement.security.util.LoggedUser;
import com.qspear.procurement.service.UserControllerService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class PriorReviewMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    UserControllerService userControllerService;

    @Autowired
    PriorReviewRepository priorReviewRepository;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {

        List<PriorReview> priorReview = mappingObject.getPriorReview();
        if (priorReview == null || priorReview.isEmpty()) {
            return;
        }

        for (PriorReview model : priorReview) {

            TeqipPackagePriorReview entity = priorReviewRepository.findByTeqipPackageAndStageName(teqipPackage, model.getStageName());
            entity = entity != null ? entity : new TeqipPackagePriorReview();
            entity.setIsPriorReviewRequired(model.getIsPriorReviewRequired());
            entity.setStageIndex(model.getStageIndex());
            entity.setStageName(model.getStageName());
            entity.setPriorReviewStageText(model.getPriorReviewStageText());
            entity.setReviewStatus(model.getReviewStatus());
            entity.setComments(model.getComments());
            entity.setUpdatedTime(model.getUpdatedTime());
            entity.setSubmittedTime(model.getSubmittedTime());
            entity.setRequestorName(model.getRequestorName());
            entity.setRoleOfNextActor(model.getRoleOfNextActor());
            Integer reviewedByUserId = model.getReviewedByUserId();
            if (reviewedByUserId != null && reviewedByUserId > 0) {
                entity.setUser(userControllerService.findByUserid(reviewedByUserId));
            }
            entity.setTeqipPackage(teqipPackage);
            priorReviewRepository.saveAndFlush(entity);

        }

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        List<TeqipPackagePriorReview> list = priorReviewRepository.findByTeqipPackage(teqipPackage);
        if (list == null || list.isEmpty()) {
            return;
        }

        List<PriorReview> priorReview = new ArrayList<>();

        for (TeqipPackagePriorReview entity : list) {
            PriorReview model = new PriorReview();
            model.setIsPriorReviewRequired(entity.getIsPriorReviewRequired());
            model.setStageIndex(entity.getStageIndex());
            model.setStageName(entity.getStageName());
            model.setPriorReviewStageText(entity.getPriorReviewStageText());
            model.setReviewStatus(entity.getReviewStatus());
            model.setComments(entity.getComments());
            model.setUpdatedTime(entity.getUpdatedTime());
            model.setSubmittedTime(entity.getSubmittedTime());
            model.setRequestorName(entity.getRequestorName());
            model.setRoleOfNextActor(entity.getRoleOfNextActor());
            User user = entity.getUser();
            if (user != null) {
                model.setReviewedByUserId(user.getUserid());
                model.setReviewedByUserName(user.getUserName());
                TeqipRolemaster teqipRolemaster = user.getTeqipRolemaster();
                if (teqipRolemaster != null) {
                    model.setReviewedByUserRoleType(teqipRolemaster.getPmssRole());
                }
            }

            priorReview.add(model);
        }

        mappingObject.setPriorReview(priorReview);

    }

}
