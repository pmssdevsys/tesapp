package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.ProcurementConstants;
import com.qspear.procurement.model.InstituteWiseApproval;
import com.qspear.procurement.model.PackageHistoryList;
import com.qspear.procurement.model.StateWiseApproval;
import com.qspear.procurement.model.TeqipDepartmentModel;
import com.qspear.procurement.model.TeqipPackageModel;
import com.qspear.procurement.model.TeqipProcurementmethodModel;
import com.qspear.procurement.model.TeqipSubcomponentModel;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPackageHistory;
import com.qspear.procurement.persistence.TeqipPlanApprovalstatus;
import com.qspear.procurement.persistence.TeqipPmssCurrencymaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;
import com.qspear.procurement.persistence.User;
import com.qspear.procurement.persistence.methods.TeqipPackagePaymentDetail;
import com.qspear.procurement.persistence.methods.TeqipPackagePoContractGenereration;
import com.qspear.procurement.persistence.methods.TeqipPackagePriorReview;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrder;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrder;
import com.qspear.procurement.repositories.method.PackagePurchaseOrderRepository;
import com.qspear.procurement.repositories.method.PoContractGenererationRepository;
import com.qspear.procurement.repositories.method.PriorReviewRepository;
import com.qspear.procurement.repositories.method.WorkOrderRepository;
import com.qspear.procurement.service.AbstractPackageService;
import com.qspear.procurement.service.UserControllerService;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcurementMapper extends AbstractPackageService {

    @Autowired
    ItemGoodsMapper itemGoodsMapper;

    @Autowired
    ItemWorkMapper itemWorkMapper;

    @Autowired
    UserControllerService userControllerService;

    @Autowired
    PackagePurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    WorkOrderRepository workOrderRepository;

    @Autowired
    PoContractGenererationRepository poContractGenererationRepository;

    @Autowired
    PriorReviewRepository priorReviewRepository;

    public List<TeqipPackageModel> mapTeqipPackages(List<TeqipPackage> teqipPackage, boolean detail) {
        if (teqipPackage == null) {
            return null;
        }
        List<TeqipPackageModel> list = new ArrayList<>();

        for (TeqipPackage entity : teqipPackage) {
            list.add(mapTeqipPackage(entity, detail));
        }
        return list;

    }

    public List<TeqipPackageModel> mapTeqipPackagesHistory(List<TeqipPackageHistory> teqipPackage) {
        if (teqipPackage == null) {
            return null;
        }
        List<TeqipPackageModel> list = new ArrayList<>();

        for (TeqipPackageHistory entity : teqipPackage) {

            TeqipPackageModel model = new TeqipPackageModel();
            model.setPackageId(entity.getPackageHistoryId());
            model.setTypeofplanCreator(entity.getTypeofplanCreator());
            model.setEstimatedCost(entity.getEstimatedCost());
            model.setFinancialSanctionDate(entity.getFinancialSanctionDate());
            model.setIscoe(entity.getIscoe());
            model.setIsgem(entity.getIsgem());
            model.setIsprop(entity.getIsprop());
            model.setJustification(entity.getJustification());
            model.setPackageCode(entity.getPackageCode());
            model.setPackageName(entity.getPackageName());
            model.setRevisionComments(entity.getRevisionComments());
            model.setServiceProviderId(entity.getServiceProviderId());
            model.setReviseId(entity.getReviseId());
            model.setPackageInitiatedate(entity.getInitiateDate());

            model.setModifyOn(entity.getModifyOn());
            model.setModifyBy(entity.getModifyBy());
            model.setCreatedOn(entity.getCreatedOn());
            User user = entity.getCreatedBy() != null ? userControllerService.findByUserid(entity.getCreatedBy()) : null;
            if (user != null) {
                model.setCreatedBy(user.getUserName());
            }

            if (entity.getTeqipStatemaster() != null) {
                model.setTeqipStatemasterId(entity.getTeqipStatemaster().getStateId());
                model.setTeqipStatemasterName(entity.getTeqipStatemaster().getStateName());
            }
            if (entity.getTeqipSubcomponentmaster() != null) {
                model.setTeqipSubcomponentmasterId(entity.getTeqipSubcomponentmaster().getId());
                model.setTeqipSubcomponentmasterName(entity.getTeqipSubcomponentmaster().getSubcomponentcode());
            }
            if (entity.getTeqipCategorymaster() != null) {
                model.setTeqipCategorymasterId(entity.getTeqipCategorymaster().getCatId());
                model.setTeqipCategorymasterName(entity.getTeqipCategorymaster().getCategoryName());
            }
            if (entity.getTeqipSubcategorymaster() != null) {
                model.setTeqipSubcategorymasterId(entity.getTeqipSubcategorymaster().getSubcatId());
                model.setTeqipSubcategorymasterName(entity.getTeqipSubcategorymaster().getSubCategoryName());
            }
            if (entity.getTeqipActivitiymaster() != null) {
                model.setTeqipActivitiymasterId(entity.getTeqipActivitiymaster().getActivityId());
                model.setTeqipActivitiymasterName(entity.getTeqipActivitiymaster().getActivityName());
            }

            if (entity.getTeqipProcurementmethod() != null) {
                model.setTeqipProcurementmethodId(entity.getTeqipProcurementmethod().getProcurementmethodId());
                model.setTeqipProcurementmethodName(entity.getTeqipProcurementmethod().getProcurementmethodName());
                model.setTeqipProcurementmethodCode(entity.getTeqipProcurementmethod().getProcurementmethodCode());

            }
            if (entity.getTeqipProcurementmaster() != null) {
                model.setTeqipProcurementmasterId(entity.getTeqipProcurementmaster().getProcurementmasterId());
                model.setTeqipProcurementmasterName(entity.getTeqipProcurementmaster().getPlanName());
            }

            if (entity.getTeqipInstitution() != null) {
                model.setTeqipInstitutionId(entity.getTeqipInstitution().getInstitutionId());
                model.setTeqipInstitutionName(entity.getTeqipInstitution().getInstitutionName());
            }

            if (entity.getTeqipPmssProcurementstage() != null) {
                model.setTeqipPmssProcurementstageId(entity.getTeqipPmssProcurementstage().getStageid());
                model.setTeqipPmssProcurementstageName(entity.getTeqipPmssProcurementstage().getProcurementstages());
                model.setStageId(entity.getTeqipPmssProcurementstage().getProcurementstageid());
            }

            model.setTeqipItemGoodsDetails(itemGoodsMapper.mapItemGoodsHistory(entity.getTeqipItemGoodsDetails()));
            model.setTeqipItemWorkDetails(itemWorkMapper.mapWorkHistory(entity.getTeqipItemWorkDetails()));

            list.add(model);
        }
        return list;

    }

    public List<TeqipProcurementmethodModel> mapTeqipProcurementmethodModel(List<TeqipProcurementmethod> teqipProcurementmethodList) {

        if (teqipProcurementmethodList == null) {
            return null;
        }

        List<TeqipProcurementmethodModel> list = new ArrayList<>();

        for (TeqipProcurementmethod entity : teqipProcurementmethodList) {
            TeqipProcurementmethodModel model = new TeqipProcurementmethodModel();
            model.setProcurementmethodCode(entity.getProcurementmethodCode());
            model.setProcurementmethodId(entity.getProcurementmethodId());
            model.setProcurementmethodName(entity.getProcurementmethodName());
            model.setCategoryName(entity.getTeqipCategorymaster() != null ? entity.getTeqipCategorymaster().getCategoryName() : null);
            list.add(model);
        }
        return list;
    }

    public List<TeqipSubcomponentModel> mapListOfTeqipSubcomponentModel(List<TeqipSubcomponentmaster> listOfTeqipSubcomponentmaster) {
        if (listOfTeqipSubcomponentmaster == null) {
            return null;
        }

        List<TeqipSubcomponentModel> list = new ArrayList<>();

        for (TeqipSubcomponentmaster entity : listOfTeqipSubcomponentmaster) {
            TeqipSubcomponentModel model = new TeqipSubcomponentModel();

            model.setComponentId(entity.getId());
            model.setSubComponentCode(entity.getSubcomponentcode());
            model.setSubComponentName(entity.getSubComponentName());

            list.add(model);
        }
        return list;
    }

    public List<TeqipDepartmentModel> mapDepartments(List<TeqipInstitutiondepartmentmapping> listOfDepartment) {
        if (listOfDepartment == null) {
            return null;
        }

        List<TeqipDepartmentModel> list = new ArrayList<>();

        for (TeqipInstitutiondepartmentmapping entity : listOfDepartment) {
            TeqipDepartmentModel model = new TeqipDepartmentModel();
            model.setInstitutionDepartmentMapping_ID(entity.getInstitutionDepartmentMapping_ID());
            model.setDepartmenthead(entity.getDepartmentHead());
            model.setDepartmentname(entity.getTeqipPmssdepartmentmaster().getDepartment());
            model.setTeqipPmssDepartmentMasterId(entity.getTeqipPmssdepartmentmaster().getDepartmentId());

            list.add(model);
        }
        return list;

    }

    public TeqipPackageModel mapTeqipPackage(TeqipPackage entity, boolean detail) {

        if (entity == null) {
            return null;
        }

        TeqipPackageModel model = new TeqipPackageModel();
        model.setPackageId(entity.getPackageId());
        model.setTypeofplanCreator(entity.getTypeofplanCreator());
        model.setEstimatedCost(entity.getEstimatedCost());
        model.setFinancialSanctionDate(entity.getFinancialSanctionDate());
        model.setIscoe(entity.getIscoe());
        model.setIsgem(entity.getIsgem());
        model.setIsprop(entity.getIsprop());
        model.setJustification(entity.getJustification());
        model.setPackageCode(entity.getPackageCode());
        model.setPackageName(entity.getPackageName());
        model.setRevisionComments(entity.getRevisionComments());
        model.setServiceProviderId(entity.getServiceProviderId());
        model.setReviseId(entity.getReviseId());
        model.setPackageInitiatedate(entity.getInitiateDate());
        model.setCurrentStage(entity.getCurrentStage());
        model.setCurrentStageIndex(entity.getCurrentStageIndex());
        model.setCreatedOn(entity.getCreatedOn());
        model.setModifyOn(entity.getModifyOn());
        model.setModifyBy(entity.getModifyBy());
        model.setPackageInitiationDate(entity.getPackageInitiationDate());
        model.setIsgemdcbid(entity.getIsgemdcbid());
        model.setPurchaseType(entity.getPurchaseType());
        model.setPriorReviewNeeded(this.checkPriorReviewNeeded(entity));
        User user = entity.getCreatedBy() != null ? userControllerService.findByUserid(entity.getCreatedBy()) : null;
        if (user != null) {
            model.setCreatedBy(user.getUserName());
        }
        if (entity.getTeqipStatemaster() != null) {
            model.setTeqipStatemasterId(entity.getTeqipStatemaster().getStateId());
            model.setTeqipStatemasterName(entity.getTeqipStatemaster().getStateName());
        }
        if (entity.getTeqipSubcomponentmaster() != null) {
            model.setTeqipSubcomponentmasterId(entity.getTeqipSubcomponentmaster().getId());
            model.setTeqipSubcomponentmasterName(entity.getTeqipSubcomponentmaster().getSubcomponentcode());
        }
        if (entity.getTeqipCategorymaster() != null) {
            model.setTeqipCategorymasterId(entity.getTeqipCategorymaster().getCatId());
            model.setTeqipCategorymasterName(entity.getTeqipCategorymaster().getCategoryName());
        }
        if (entity.getTeqipSubcategorymaster() != null) {
            model.setTeqipSubcategorymasterId(entity.getTeqipSubcategorymaster().getSubcatId());
            model.setTeqipSubcategorymasterName(entity.getTeqipSubcategorymaster().getSubCategoryName());
        }
        if (entity.getTeqipActivitiymaster() != null) {
            model.setTeqipActivitiymasterId(entity.getTeqipActivitiymaster().getActivityId());
            model.setTeqipActivitiymasterName(entity.getTeqipActivitiymaster().getActivityName());
        }

        if (entity.getTeqipProcurementmethod() != null) {
            model.setTeqipProcurementmethodId(entity.getTeqipProcurementmethod().getProcurementmethodId());
            model.setTeqipProcurementmethodName(entity.getTeqipProcurementmethod().getProcurementmethodName());
            model.setTeqipProcurementmethodCode(entity.getTeqipProcurementmethod().getProcurementmethodCode());

        }
        if (entity.getTeqipProcurementmaster() != null) {
            model.setTeqipProcurementmasterId(entity.getTeqipProcurementmaster().getProcurementmasterId());
            model.setTeqipProcurementmasterName(entity.getTeqipProcurementmaster().getPlanName());
        }

        if (entity.getTeqipInstitution() != null) {
            model.setTeqipInstitutionId(entity.getTeqipInstitution().getInstitutionId());
            model.setTeqipInstitutionName(entity.getTeqipInstitution().getInstitutionName());
        }

        if (entity.getTeqipPmssProcurementstage() != null) {
            model.setTeqipPmssProcurementstageId(entity.getTeqipPmssProcurementstage().getStageid());
            model.setTeqipPmssProcurementstageName(entity.getTeqipPmssProcurementstage().getProcurementstages());
            model.setStageId(entity.getTeqipPmssProcurementstage().getProcurementstageid());
        }

        List<TeqipPackagePaymentDetail> teqipPackagePaymentDetails = entity.getTeqipPackagePaymentDetails();
        if (teqipPackagePaymentDetails != null) {
            Double actualAmount = 0.0;
            for (TeqipPackagePaymentDetail teqipPackagePaymentDetail : teqipPackagePaymentDetails) {
                actualAmount += teqipPackagePaymentDetail.getActualPaymentAmount() != null ? teqipPackagePaymentDetail.getActualPaymentAmount() : 0.0;
            }
            model.setActualCost(actualAmount > 0 ? actualAmount : null);
        }

        TeqipPlanApprovalstatus approvalstatus = teqipPlanApprovalStatusRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(
                entity.getTeqipInstitution(), entity.getTeqipStatemaster(),entity.getTypeofplanCreator());

        if (approvalstatus != null && approvalstatus.getReviseid() > 1
                && entity.getReviseId().equals(approvalstatus.getReviseid())
                && !(approvalstatus.getTeqipPmssProcurementstage().getProcurementstageid().equals(6)
                || approvalstatus.getTeqipPmssProcurementstage().getProcurementstageid().equals(12))) {
            model.setIsRevisedPackage(1);
        }

        List<TeqipPackagePriorReview> list = priorReviewRepository.findByTeqipPackage(entity);
        if (list != null && !list.isEmpty() && "REJECTED".equals(list.get(list.size() - 1).getReviewStatus())) {
            model.setPriorRejected(true);
        }
        if (detail) {
            model.setTeqipItemGoodsDetails(itemGoodsMapper.mapEntityToObject(entity.getTeqipItemGoodsDetails()));
            model.setTeqipItemWorkDetails(itemWorkMapper.mapEntityToObject(entity.getTeqipItemWorkDetails()));
        }
        return model;
    }

    public TeqipPackage savePackageModel(TeqipPackageModel model) {

        if (model == null) {
            return null;
        }

        TeqipPackage entity = model.getPackageId() != null
                ? teqipPackageRepository.findOne(model.getPackageId())
                : new TeqipPackage();

//        entity.setTypeofplanCreator(model.getTypeofplanCreator());
        entity.setEstimatedCost(model.getEstimatedCost());
        entity.setFinancialSanctionDate(model.getFinancialSanctionDate());
        entity.setIscoe(model.getIscoe());
        entity.setIsgem(model.getIsgem());
        entity.setIsprop(model.getIsprop());
        entity.setJustification(model.getJustification());
        entity.setPackageCode(model.getPackageCode());
        entity.setPackageName(model.getPackageName());
        entity.setRevisionComments(model.getRevisionComments());
        entity.setServiceProviderId(model.getServiceProviderId());
        entity.setConsumeFromWorks(model.getConsumeFromWorks() != null ? model.getConsumeFromWorks() : 0);
        entity.setIsgemdcbid(model.getIsgemdcbid());
        entity.setPurchaseType(model.getPurchaseType());
        if (model.getTeqipSubcomponentmasterId() != null) {
            entity.setTeqipSubcomponentmaster(teqipSubcomponentmasterRepository.findOne(model.getTeqipSubcomponentmasterId()));
        }
        if (model.getTeqipCategorymasterId() != null) {
            entity.setTeqipCategorymaster(categoryRepository.findOne(model.getTeqipCategorymasterId()));
        }
        if (model.getTeqipSubcategorymasterId() != null) {
            entity.setTeqipSubcategorymaster(teqipSubcategorymasterRepository.findOne(model.getTeqipSubcategorymasterId()));
        }

        if (model.getTeqipActivitiymasterId() != null) {
            entity.setTeqipActivitiymaster(activityRepository.findOne(model.getTeqipActivitiymasterId()));
        }

        if (model.getTeqipProcurementmethodId() != null) {
            entity.setTeqipProcurementmethod(procurementmethodRepository.findOne(model.getTeqipProcurementmethodId()));
        }

        if (model.getTeqipProcurementmasterId() != null) {
            entity.setTeqipProcurementmaster(teqipProcurementmasterRepository.findOne(model.getTeqipProcurementmasterId()));
        }

        return entity;

    }

    public List<StateWiseApproval> mapStateWiseApproval(List<Object[]> stateWiseApproval) {
        if (stateWiseApproval == null) {
            return null;
        }

        List<StateWiseApproval> list = new ArrayList<>();

        for (Object[] objects : stateWiseApproval) {
            Object object = objects[0];

//            BigInteger count = (BigInteger) objects[3];
//            if (countcount.intValue() > 0) {
            Integer stateId = object instanceof BigInteger
                    ? ((BigInteger) object).intValue()
                    : (Integer) object;
            StateWiseApproval approval = new StateWiseApproval(
                    stateId,
                    (String) objects[1],
                    (String) objects[2],
                    (BigInteger) objects[3],
                    (Double) objects[4],
                    (Double) objects[5],
                    (Double) objects[6],
                    (Double) objects[7],
                    (String) objects[8]
            );

            list.add(approval);
//            }

        }
        return list;

    }

    public List<InstituteWiseApproval> mapInstituteWiseApproval(List<Object[]> instituteWiseApproval) {
        if (instituteWiseApproval == null) {
            return null;
        }
        List<InstituteWiseApproval> list = new ArrayList<>();

        for (Object[] objects : instituteWiseApproval) {
            BigInteger count = (BigInteger) objects[6];

            if (count.intValue() > 0) {
                InstituteWiseApproval approval = new InstituteWiseApproval(
                        (Integer) objects[0],
                        (String) objects[1],
                        (String) objects[2],
                        (String) objects[3],
                        (Date) objects[4],
                        (Integer) objects[5],
                        (BigInteger) objects[6],
                        (Double) objects[7],
                        (Double) objects[8],
                        (Double) objects[9],
                        (Double) objects[10],
                        (Integer) objects[11],
                        (String) objects[12]);

                list.add(approval);
            }

        }

        return list;

    }

    public List<PackageHistoryList> mapPackageHistory(List<Object[]> packageHistory) {
        if (packageHistory == null) {
            return null;
        }
        List<PackageHistoryList> list = new ArrayList<>();

        for (Object[] objects : packageHistory) {

            PackageHistoryList approval = new PackageHistoryList(
                    (Integer) objects[0],
                    (String) objects[1],
                    (Integer) objects[2],
                    (String) objects[3],
                    (Date) objects[4],
                    (Integer) objects[5],
                    (String) objects[6],
                    (String) objects[7],
                    (Integer) objects[8]);

            list.add(approval);

        }

        return list;
    }

    public List<TeqipPackageModel> filterPackageModel(List<TeqipPackageModel> mapTeqipPackages,
            String method, Integer categoryId, Integer subCategoryId) {
        
        if(mapTeqipPackages == null){
            return null;
        }
        
        if (method != null ) {
            mapTeqipPackages = mapTeqipPackages.stream().filter((obj) -> {
                return Objects.equals(obj.getTeqipProcurementmethodCode(), method);
            }).collect(Collectors.toList());
        }
        if (categoryId != null && categoryId > 0) {
            mapTeqipPackages = mapTeqipPackages.stream().filter((obj) -> {
                return Objects.equals(obj.getTeqipCategorymasterId(), categoryId);
            }).collect(Collectors.toList());
        }
        if (subCategoryId != null && subCategoryId > 0) {
            mapTeqipPackages = mapTeqipPackages.stream().filter((obj) -> {
                return Objects.equals(obj.getTeqipSubcategorymasterId(), subCategoryId);
            }).collect(Collectors.toList());
        }
        return mapTeqipPackages;
    }

    private Boolean checkPriorReviewNeeded(TeqipPackage entity) {
        TeqipProcurementmethod method = entity.getTeqipProcurementmethod();
        TeqipCategorymaster category = entity.getTeqipCategorymaster();
        Double estimatedCost = entity.getEstimatedCost();
        if (method != null && category != null && estimatedCost != null) {
            TeqipPmssCurrencymaster teqipPmssCurrencymaster = teqipPmssCurrencymasterRepository.findByCurrencyname(ProcurementConstants.USD_CURRENCY_CODE);

            estimatedCost = estimatedCost / teqipPmssCurrencymaster.getCurrencyvalue();

            if ("Direct Contract".equalsIgnoreCase(method.getProcurementmethodCode())
                    && "Goods".equalsIgnoreCase(category.getCategoryName())) {
                if (estimatedCost >= 1000000) {
                    return true;
                }
            }
            if ("NCB".equalsIgnoreCase(method.getProcurementmethodCode())
                    && "Goods".equalsIgnoreCase(category.getCategoryName())) {
                if (estimatedCost < 2000000) {
                    return true;
                }
                if (estimatedCost >= 2000000) {
                    return true;
                }
            }
            if ("NCB".equalsIgnoreCase(method.getProcurementmethodCode())
                    && "Civil Works".equalsIgnoreCase(category.getCategoryName())) {
                if (estimatedCost < 10000000) {
                    return true;
                }
                if (estimatedCost >= 10000000) {
                    return true;
                }
            }
            if ("QCBS".equalsIgnoreCase(method.getProcurementmethodCode())
                    || "FBS".equalsIgnoreCase(method.getProcurementmethodCode())
                    || "CQS".equalsIgnoreCase(method.getProcurementmethodCode())
                    || "LCS".equalsIgnoreCase(method.getProcurementmethodCode())) {
                if (estimatedCost >= 1000000) {
                    return true;
                }
            }
            if ("SSS".equalsIgnoreCase(method.getProcurementmethodCode())
                    || "Indv Consultant SSS".equalsIgnoreCase(method.getProcurementmethodCode())
                    || "Indv Consultant Comp.".equalsIgnoreCase(method.getProcurementmethodCode())) {
                if (estimatedCost >= 300000) {
                    return true;
                }
            }

        }

        return false;
    }

}
