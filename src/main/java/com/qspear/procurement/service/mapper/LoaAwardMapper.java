package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.GenerateContract;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.LoaAward;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackagePoContractGenereration;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageBidDetailRepository;
import com.qspear.procurement.repositories.method.PackageBidINSTAndSCCRepository;
import com.qspear.procurement.repositories.method.PoContractGenererationRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import com.qspear.procurement.util.ProcurementUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jaspreets on 4/4/2018.
 */
@Service
public class LoaAwardMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PaymentMapper paymentMapper;

    @Autowired
    PackageBidDetailRepository packageBidDetailRepository;

    @Autowired
    PackageBidINSTAndSCCRepository packageBidINSTAndSCCRepository;

    @Autowired
    PoContractGenererationRepository poContractGenererationRepository;

    @Autowired
    PurchaseOrderMapper purchaseOrderMapper;

    @Autowired
    WorkOrderMapper workOrderMapper;
    
    @Autowired
    UploadMapper uploadMapper;
    
     @Autowired
    ProcurementUtility procurementUtility;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {

        LoaAward model = mappingObject.getLoaAward();

        if (model == null) {
            return;
        }
        TeqipPackagePoContractGenereration entity = poContractGenererationRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackagePoContractGenereration();

        entity.setTeqipPackage(teqipPackage);
        
        
        entity.setLoageneratedDate(model.getLoaGeneratedDate());
        entity.setMaintainencePeriodExpiryDate(model.getMaintainencePeriodExpiryDate());
        GenerateContract generateContract = model.getGenerateContract();
        if (generateContract != null) {
            if (generateContract.getSupplierId() != null) {
                entity.setTeqipPmssSupplierMaster(supplierRepository.findOne(generateContract.getSupplierId()));
            }
            entity.setContractStartDate(generateContract.getContractStartDate());
            entity.setExpectedDeliveryDate(generateContract.getExpectedDeliveryDate());
            entity.setContractDocsignedDate(generateContract.getContractDocumentSigned());
            entity.setPerformanceSecurityDate(generateContract.getSecurityRvcdDate());
            entity.setPerfSecurityAmount(generateContract.getSecurityAmount());
            entity.setArbitratorAgreed(generateContract.getIsArbitratorAgreed());
            entity.setPerfSecurityInstrument(generateContract.getSecurityInstrument());
            entity.setVatPercentage(generateContract.getVatPercent());
            entity.setPerfSecurityExpiryDate(generateContract.getSecurityExpDate());
            entity.setOtherOctroi(generateContract.getOctroi());
            entity.setEvaluatedPrice(generateContract.getEvaluatedPrice());
            entity.setIsMobilizationAdvancePaid(generateContract.getIsMobilizationAdvancePaid());
            entity.setIsBankSecurityGiven(generateContract.getIsBankSecurityGiven());
            entity.setMobilizationAmount(generateContract.getMobilizationAmount());
            entity.setSumOfAllApplicableTaxes(generateContract.getSumOfAllApplicableTaxes());
            entity.setTotalContractValue(generateContract.getTotalContractValue());
            entity.setWorkStartDate(generateContract.getWorkStartDate());
            entity.setWorkCompletionDate(generateContract.getWorkCompletionDate());
            entity.setTotalBaseCost(generateContract.getBaseCost());
            

        }
        poContractGenererationRepository.saveAndFlush(entity);
        
        if (entity.getPoNumber() == null) {
            String poNumber = procurementUtility.getPackageCode();
                poNumber = poNumber
                        + teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode()
                        + "/" + entity.getContractGenerationId();

            entity.setPoNumber(poNumber);
        }
         poContractGenererationRepository.saveAndFlush(entity);
        
        purchaseOrderMapper.savePurchaseOrderItem(model.getItems(), teqipPackage);
        workOrderMapper.saveWorkOrderWork(model.getWorks(), teqipPackage);

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        TeqipPackagePoContractGenereration entity = poContractGenererationRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return;
        }
        LoaAward model = new LoaAward();

        model.setPoNumber(entity.getPoNumber());
        model.setLoaGeneratedDate(entity.getLoageneratedDate());
        model.setMaintainencePeriodExpiryDate(entity.getMaintainencePeriodExpiryDate());
        GenerateContract generateContract = new GenerateContract();

        generateContract.setPo(entity.getPoNumber());
        generateContract.setContractStartDate(entity.getContractStartDate());
        generateContract.setExpectedDeliveryDate(entity.getExpectedDeliveryDate());
        generateContract.setContractDocumentSigned(entity.getContractDocsignedDate());
        generateContract.setSecurityRvcdDate(entity.getPerformanceSecurityDate());
        generateContract.setSecurityAmount(entity.getPerfSecurityAmount());
        generateContract.setIsArbitratorAgreed(entity.getArbitratorAgreed());
        generateContract.setSecurityInstrument(entity.getPerfSecurityInstrument());
        generateContract.setVatPercent(entity.getVatPercentage());
        generateContract.setSecurityExpDate(entity.getPerfSecurityExpiryDate());
        generateContract.setOctroi(entity.getOtherOctroi());
        generateContract.setEvaluatedPrice(entity.getEvaluatedPrice());
        generateContract.setBaseCost(entity.getTotalBaseCost());
        generateContract.setIsMobilizationAdvancePaid(entity.getIsMobilizationAdvancePaid());
        generateContract.setIsBankSecurityGiven(entity.getIsBankSecurityGiven());
        generateContract.setMobilizationAmount(entity.getMobilizationAmount());
        generateContract.setSumOfAllApplicableTaxes(entity.getSumOfAllApplicableTaxes());
        generateContract.setTotalContractValue(entity.getTotalContractValue());
        generateContract.setWorkStartDate(entity.getWorkStartDate());
        generateContract.setWorkCompletionDate(entity.getWorkCompletionDate());
        

        TeqipPmssSupplierMaster teqipPmssSupplierMaster = entity.getTeqipPmssSupplierMaster();

        if (teqipPmssSupplierMaster != null) {
            generateContract.setSupplierId(teqipPmssSupplierMaster.getSupplierId());
            generateContract.setSupplierName(teqipPmssSupplierMaster.getSupplierName());
        }

        model.setGenerateContract(generateContract);

        model.setItems(purchaseOrderMapper.setPurchaseOrderItem(teqipPackage));
        model.setWorks(workOrderMapper.setWorkOrderWork(teqipPackage));

        model.setContractGeneration(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_NCB_CONTRACT));
        model.setLoaGeneration(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE_NCB));
        model.setWoGeneration(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_WORK_ORDER_NCB));

        mappingObject.setLoaAward(model);

    }

}
