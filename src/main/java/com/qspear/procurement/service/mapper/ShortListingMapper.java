/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.ShortListing;
import com.qspear.procurement.model.methods.ShortlistedConsultants;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageTermReference;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import com.qspear.procurement.repositories.method.TermReferenceRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class ShortListingMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    TermReferenceRepository termReferenceRepository;

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    @Autowired
    SupplierRepository supplierRepository;
    
    @Autowired
    UploadMapper uploadMapper;
      

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {

        TeqipPackageTermReference entity = termReferenceRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return;
        }
        ShortListing shortList = new ShortListing();
        shortList.setId(entity.getTermOfReferenceId());
        shortList.setEoiOpeningDate(entity.getEoiOpeningDate());
        shortList.setRemarks(entity.getRemarks());
        shortList.setDateOfShortlistingFirms(entity.getDateOfShortlistingFirms());
        ArrayList<ShortlistedConsultants> shortlistedConsultants = new ArrayList<>();

        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();

        if (teqipPackageSupplierDetails != null) {

            for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {

                TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
                if (teqipPmssSupplierMaster != null) {

                    ShortlistedConsultants model = new ShortlistedConsultants();
                    model.setSupplierId(teqipPmssSupplierMaster.getSupplierId());
                    model.setSupplierName(teqipPmssSupplierMaster.getSupplierName());
                    model.setIsShortListed(teqipPackageSupplierDetail.getIsShortListed());
                    model.setComments(teqipPackageSupplierDetail.getComments());

                    shortlistedConsultants.add(model);

                }
            }
        }

        ArrayList<Uploads> uploads = new ArrayList<>() ;
        uploads.addAll(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_QCBS_EOI_OPENING));
        shortList.setEoiGeneration(uploads);
        
        shortList.setShortlistedConsultants(shortlistedConsultants);
        response.setShortListing(shortList);
    }

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {
        ShortListing shortListing = methodPackageUpdateRequest.getShortListing();
        if (shortListing == null) {
            return;
        }

        TeqipPackageTermReference entity = termReferenceRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackageTermReference();
        entity.setTeqipPackage(teqipPackage);

        entity.setEoiOpeningDate(shortListing.getEoiOpeningDate());
        entity.setRemarks(shortListing.getRemarks());
        entity.setDateOfShortlistingFirms(shortListing.getDateOfShortlistingFirms());
        termReferenceRepository.saveAndFlush(entity);

        ArrayList<ShortlistedConsultants> shortlistedConsultants = shortListing.getShortlistedConsultants();
        if (shortlistedConsultants == null) {
            return;
        }

        for (ShortlistedConsultants shortlistedConsultant : shortlistedConsultants) {

            if (shortlistedConsultant.getSupplierId() != null) {

                TeqipPmssSupplierMaster supplierMaster = supplierRepository.findOne(shortlistedConsultant.getSupplierId());
                if (supplierMaster != null) {

                    TeqipPackageSupplierDetail packageSupplier = packageSupplierRepository.findByTeqipPackageAndTeqipPmssSupplierMaster(teqipPackage, supplierMaster);

                    packageSupplier.setIsShortListed(shortlistedConsultant.getIsShortListed());
                    packageSupplier.setComments(shortlistedConsultant.getComments());

                    packageSupplierRepository.saveAndFlush(packageSupplier);
                }

            }

        }
    }

}
