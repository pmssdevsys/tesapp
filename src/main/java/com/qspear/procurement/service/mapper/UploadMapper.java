/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSBudgetException;
import com.qspear.procurement.exception.PMSSException;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.User;
import com.qspear.procurement.persistence.methods.TeqipPackageDocument;
import com.qspear.procurement.repositories.method.PackageDocumentRepository;
import com.qspear.procurement.service.UserControllerService;
import com.qspear.procurement.util.FileUtils;
import com.qspear.procurement.util.MethodUtility;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class UploadMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackageDocumentRepository documentRepository;

    @Autowired
    UserControllerService userControllerService;

    @Override
    public void mapObjectToEntity(TeqipPackage entity, MethodPackageUpdateRequest mappingObject) {

        ArrayList<Uploads> uploadList = mappingObject.getUploads();
        if (uploadList == null) {
            return;
        }

        for (Uploads uploads : uploadList) {
            if (uploads.getPackageDocumentId() == null) {
                this.saveUpload(entity, uploads);
            }
        }
    }

    @Override
    public void mapEntityToObject(TeqipPackage entity, MethodPackageResponse mappingObject) {

        mappingObject.setUploads(getUploads(entity, entity.getTeqipPackageDocuments(), "ALL"));
    }

    public TeqipPackageDocument saveUpload(TeqipPackage teqipPackage,
            Uploads uploads) {
    	 TeqipPackageDocument entity =null;
    	if(uploads.getFileName()!=null){
   String ab = FileUtils.checkDocumentFile(uploads.getFileName());
   if(ab=="Not Valid")
   {    	System.out.println("File-->"+uploads.getFileName());

	   throw new PMSSBudgetException("Invalid FIle.",
               new ErrorCode("Enter a valid file name with valid extension", "Enter a valid file name with valid extension"));
	   
    	}
    	
    	  
    	else{
    entity = uploads.getPackageDocumentId() != null
                ? documentRepository.findOne(uploads.getPackageDocumentId())
                : new TeqipPackageDocument();

        entity.setOriginalFileName(uploads.getFileName());
        uploads.setFileName(new Date().getTime() + "_" + uploads.getFileName());
        entity.setDocumentCategory(uploads.getDocumentCategory());
        entity.setDocumentfilename(uploads.getDocumentCategory() + File.separator + uploads.getFileName());
        entity.setDescription(uploads.getDescription());
        entity.setTeqipPackage(teqipPackage);

        MethodUtility.saveDocument(teqipPackage.getPackageId(), uploads.getDocumentCategory(), uploads.getFileName(), uploads.getFile());

        documentRepository.saveAndFlush(entity);
    	}
    	}
       
    	 return entity;
    }

    public ArrayList<Uploads> getUploads(TeqipPackage teqipPackage,
            List<TeqipPackageDocument> teqipPackageDocuments,
            String documentCategoryLookup) {

        if (teqipPackageDocuments == null) {
            return null;
        }

        ArrayList<Uploads> uploadList = new ArrayList<>();
        for (TeqipPackageDocument entity : teqipPackageDocuments) {

            if ((documentCategoryLookup.equals("ALL") && !MethodConstants.isAppDocCategory(entity.getDocumentCategory()))
                    || documentCategoryLookup.equals(entity.getDocumentCategory())) {

                uploadList.add(getUploadObject(teqipPackage, entity));
            }
        }
        return uploadList;
    }

    public Uploads getUploadObject(TeqipPackage teqipPackage, TeqipPackageDocument entity) {
        Uploads uploads = new Uploads();
        uploads.setPackageDocumentId(entity.getPackageDocumentId());
        uploads.setDocumentCategory(entity.getDocumentCategory());
        uploads.setFileName(entity.getOriginalFileName());
        uploads.setDescription(entity.getDescription());
        uploads.setUploadedDate(entity.getCreatedOn());
        uploads.setFileNameLink("/pmss/methods/download/" + teqipPackage.getPackageId() + "/" + entity.getPackageDocumentId());

        User user = entity.getCreatedBy() != null ? userControllerService.findByUserid(entity.getCreatedBy()) : null;
        if (user != null) {
            uploads.setUploadedBy(user.getUserName());
        }
        return uploads;

    }

}
