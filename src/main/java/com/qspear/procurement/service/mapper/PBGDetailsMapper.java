package com.qspear.procurement.service.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.PBDGDetails;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackagePBGDDetails;
import com.qspear.procurement.repositories.method.PBDGDetailsRepository;

@Service
public class PBGDetailsMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PBDGDetailsRepository pbgdDetailsRepository;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        // TODO Auto-generated method stub

        PBDGDetails model = mappingObject.getPbdgDetails();

        if (model == null) {
            return;
        }
        TeqipPackagePBGDDetails entity = pbgdDetailsRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackagePBGDDetails();
        entity.setTeqipPackage(teqipPackage);

        entity.setPbgDetails(model.getPbgDetails());
        entity.setBankName(model.getBankName());
        entity.setBranchName(model.getBranchName());
        entity.setIsPBGRequired(model.getIsPBGRequired());
        entity.setPbgPercentage(model.getPbgPercentage());
        entity.setPbgAmount(model.getPbgAmount());
        entity.setRemarks(model.getRemarks());

        pbgdDetailsRepository.saveAndFlush(entity);
    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {
        // TODO Auto-generated method stub
        TeqipPackagePBGDDetails entity = pbgdDetailsRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return;
        }

        PBDGDetails pdetails = new PBDGDetails();
        pdetails.setPbgDetails(entity.getPbgDetails());
        pdetails.setBankName(entity.getBankName());
        pdetails.setBranchName(entity.getBranchName());
        pdetails.setIsPBGRequired(entity.getIsPBGRequired());
        pdetails.setPbgPercentage(entity.getPbgPercentage());
        pdetails.setPbgAmount(entity.getPbgAmount());
        pdetails.setRemarks(entity.getRemarks());

        mappingObject.setPbdgDetails(pdetails);

    }

}
