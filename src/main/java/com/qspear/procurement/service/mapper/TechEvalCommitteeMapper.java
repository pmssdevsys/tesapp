/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.TechEvalCommittee;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageTechEvalCommitee;
import com.qspear.procurement.repositories.method.TechEvalCommiteeRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class TechEvalCommitteeMapper implements MapperInterface<TeqipPackage> {
    
    @Autowired
    TechEvalCommiteeRepository techEvalCommiteeRepository;
    
    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {
        ArrayList<TeqipPackageTechEvalCommitee> techEvalCommittee = techEvalCommiteeRepository.findByTeqipPackage(teqipPackage);
        if (techEvalCommittee == null) {
            return;
        }
        ArrayList<TechEvalCommittee> techEvalCommitteeList = new ArrayList<>();
        for (TeqipPackageTechEvalCommitee entity : techEvalCommittee) {
            TechEvalCommittee model = new TechEvalCommittee();
            
            model.setMemberId(entity.getTechEvalCommiteeId());
            model.setMemberName(entity.getMemberName());
            
            techEvalCommitteeList.add(model);
        }
        response.setTechEvalCommittee(techEvalCommitteeList);
        
    }
    
    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {
        
        ArrayList<TechEvalCommittee> techEvalCommittee = methodPackageUpdateRequest.getTechEvalCommittee();
        if (techEvalCommittee == null) {
            return;
        }
        
        for (TechEvalCommittee model : techEvalCommittee) {
            TeqipPackageTechEvalCommitee entity = model.getMemberId() != null
                    ? techEvalCommiteeRepository.findOne(model.getMemberId())
                    : new TeqipPackageTechEvalCommitee();
            
            entity.setTeqipPackage(teqipPackage);
            entity.setMemberName(model.getMemberName());
            
            techEvalCommiteeRepository.saveAndFlush(entity);
            
        }
        
    }
    
}
