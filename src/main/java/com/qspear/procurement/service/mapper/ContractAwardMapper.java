/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.ContractAward;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageIssueRFP;
import com.qspear.procurement.repositories.method.IssueRFPRepository;
import com.qspear.procurement.service.mapper.MapperInterface;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class ContractAwardMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    IssueRFPRepository issueRFPRepository;
    
    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        ContractAward contractAward = mappingObject.getContractAward();
        if (contractAward == null) {
            return;
        }

        TeqipPackageIssueRFP entity = issueRFPRepository.findByTeqipPackage(teqipPackage);

        entity = entity != null ? entity : new TeqipPackageIssueRFP();
        entity.setTeqipPackage(teqipPackage);

        entity.setContractSignDate(contractAward.getContractSignDate());

        issueRFPRepository.saveAndFlush(entity);

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        TeqipPackageIssueRFP entity = issueRFPRepository.findByTeqipPackage(teqipPackage);

        if (entity == null) {
            return;
        }
        ContractAward contractAward = new ContractAward();

        contractAward.setContractSignDate(entity.getContractSignDate());

        ArrayList<Uploads> uploads = uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_QCBS_PRIOR_REVIEW);
        contractAward.setpGeneration(uploads);
        mappingObject.setContractAward(contractAward);
    }

}
