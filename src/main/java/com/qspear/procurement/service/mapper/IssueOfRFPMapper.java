/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.IssueOfRFP;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.PreProposalMeetingDetails;
import com.qspear.procurement.model.methods.ProposalExtension;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageIssueRFP;
import com.qspear.procurement.repositories.method.IssueRFPRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class IssueOfRFPMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    IssueRFPRepository issueRFPRepository;

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {

        TeqipPackageIssueRFP entity = issueRFPRepository.findByTeqipPackage(teqipPackage);

        if (entity == null) {
            return;
        }

        IssueOfRFP issueOfREP = new IssueOfRFP();
        PreProposalMeetingDetails preProposalMeetingDetails = new PreProposalMeetingDetails();

        preProposalMeetingDetails.setId(entity.getIssueOfRfpId());
        preProposalMeetingDetails.setActualPreProposalDate(entity.getActualPreProposalDate());
        preProposalMeetingDetails.setClarificationDetails(entity.getClarificationDetails());
        preProposalMeetingDetails.setClarificationIssuedDate(entity.getClarificationIssuedDate());
        preProposalMeetingDetails.setClarificationIssuedToConsultants(entity.getClarificationIssuedToConsultants());
        preProposalMeetingDetails.setPreProposalMinutes(entity.getPreProposalMinutes());

        ProposalExtension proposalExtension = new ProposalExtension();

        proposalExtension.setCurrentSubmissionDate(entity.getCurrentSubmissionDate());
        proposalExtension.setExtendedDate(entity.getExtendedDate());
        proposalExtension.setReasonOfExtension(entity.getReasonOfExtension());

        issueOfREP.setPreProposalMeetingDetails(preProposalMeetingDetails);
        issueOfREP.setProposalExtension(proposalExtension);
        response.setIssueOfRFP(issueOfREP);

    }

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {

        IssueOfRFP issueOfRFP = methodPackageUpdateRequest.getIssueOfRFP();
        if (issueOfRFP == null) {
            return;
        }

        TeqipPackageIssueRFP entity = issueRFPRepository.findByTeqipPackage(teqipPackage);

        entity = entity != null ? entity : new TeqipPackageIssueRFP();
        entity.setTeqipPackage(teqipPackage);

        PreProposalMeetingDetails preProposalMeetingDetails = issueOfRFP.getPreProposalMeetingDetails();
        if (preProposalMeetingDetails != null) {
            entity.setActualPreProposalDate(preProposalMeetingDetails.getActualPreProposalDate());
            entity.setClarificationDetails(preProposalMeetingDetails.getClarificationDetails());
            entity.setClarificationIssuedDate(preProposalMeetingDetails.getClarificationIssuedDate());
            entity.setClarificationIssuedToConsultants(preProposalMeetingDetails.getClarificationIssuedToConsultants());
            entity.setPreProposalMinutes(preProposalMeetingDetails.getPreProposalMinutes());

        }

        ProposalExtension proposalExtension = issueOfRFP.getProposalExtension();
        if (proposalExtension != null) {

            entity.setCurrentSubmissionDate(proposalExtension.getCurrentSubmissionDate());
            entity.setExtendedDate(proposalExtension.getExtendedDate());
            entity.setReasonOfExtension(proposalExtension.getReasonOfExtension());

        }

        issueRFPRepository.saveAndFlush(entity);

    }

}
