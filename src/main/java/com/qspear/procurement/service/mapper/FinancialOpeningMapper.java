/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.FinancialOpening;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.OpeningData;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageFinancialOpeningData;
import com.qspear.procurement.persistence.methods.TeqipPackageIssueRFP;
import com.qspear.procurement.repositories.method.FinancialOpeningDataRepository;
import com.qspear.procurement.repositories.method.IssueRFPRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class FinancialOpeningMapper implements MapperInterface<TeqipPackage> {
    
    @Autowired
    IssueRFPRepository issueRFPRepository;
    
    @Autowired
    FinancialOpeningDataRepository financialOpeningDataRepository;
    
    @Autowired
    SupplierRepository supplierRepository;
    
    @Autowired
    UploadMapper uploadMapper;
      
    
    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        FinancialOpening financialOpening = mappingObject.getFinancialOpening();
        if (financialOpening == null) {
            return;
        }
        TeqipPackageIssueRFP entity = issueRFPRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackageIssueRFP();
        entity.setTeqipPackage(teqipPackage);
        
        entity.setFinancialOpeningDate(financialOpening.getFinancialOpeningDate());
        entity.setFinancialOpeningDateTime(financialOpening.getFinancialOpeningTime());
        
        issueRFPRepository.saveAndFlush(entity);
        
        ArrayList<OpeningData> openingData = financialOpening.getOpeningData();
        if (openingData != null) {
            
            for (OpeningData data : openingData) {
                
                TeqipPackageFinancialOpeningData openingDataEnitity = data.getId() != null
                        ? financialOpeningDataRepository.findOne(data.getId())
                        : new TeqipPackageFinancialOpeningData();
                openingDataEnitity.setTeqipPackage(teqipPackage);
                
                if (data.getSupplierId() != null) {
                    openingDataEnitity.setTeqipPmssSupplierMaster(supplierRepository.findOne(data.getSupplierId()));
                }
                
                openingDataEnitity.setPrice(data.getPrice());
                openingDataEnitity.setTotalScore(data.getTotalScore());
                openingDataEnitity.setRank(data.getRank());
                openingDataEnitity.setTechnicalScore(data.getTechnicalScore());
                openingDataEnitity.setFinancialScore(data.getFinancialScore());
                openingDataEnitity.setCombinedScore(data.getCombinedScore());
                openingDataEnitity.setIsTechnicalResponsive(data.getIsTechnicalResponsive());
                
                financialOpeningDataRepository.saveAndFlush(openingDataEnitity);
                
            }
        }
        
    }
    
    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {
        TeqipPackageIssueRFP entity = issueRFPRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return;
        }
        FinancialOpening financialOpening = new FinancialOpening();
        financialOpening.setId(entity.getIssueOfRfpId());
        financialOpening.setFinancialOpeningDate(entity.getFinancialOpeningDate());
        financialOpening.setFinancialOpeningTime(entity.getFinancialOpeningDateTime());
        
        ArrayList<TeqipPackageFinancialOpeningData> financialOpeningDatas = financialOpeningDataRepository.findByTeqipPackage(teqipPackage);
        if (financialOpeningDatas != null) {
            
            ArrayList<OpeningData> openingData = new ArrayList<>();
            
            for (TeqipPackageFinancialOpeningData openingDataEnitity : financialOpeningDatas) {
                OpeningData data = new OpeningData();
                
                data.setId(openingDataEnitity.getFinanceOpenigdataId());
                if (openingDataEnitity.getTeqipPmssSupplierMaster() != null) {
                    data.setSupplierId(openingDataEnitity.getTeqipPmssSupplierMaster().getSupplierId());
                    data.setSupplierName(openingDataEnitity.getTeqipPmssSupplierMaster().getSupplierName());
                }
                
                data.setPrice(openingDataEnitity.getPrice());
                data.setTotalScore(openingDataEnitity.getTotalScore());
                data.setRank(openingDataEnitity.getRank());
                data.setTechnicalScore(openingDataEnitity.getTechnicalScore());
                data.setFinancialScore(openingDataEnitity.getFinancialScore());
                data.setCombinedScore(openingDataEnitity.getCombinedScore());
                data.setIsTechnicalResponsive(openingDataEnitity.getIsTechnicalResponsive());
                openingData.add(data);
            }
            financialOpening.setOpeningData(openingData);
        }
        
         ArrayList<Uploads> uploads = new ArrayList<>() ;
        uploads.addAll(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_QCBS_FINANCIAL_OPENING));
        financialOpening.setFoiGeneration(uploads);
        
        
        mappingObject.setFinancialOpening(financialOpening);
        
    }
    
}
