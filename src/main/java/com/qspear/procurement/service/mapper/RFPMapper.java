/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.RFP;
import com.qspear.procurement.model.methods.RFPDetail;
import com.qspear.procurement.model.methods.RfpDocument;
import com.qspear.procurement.model.methods.SubCriteria;
import com.qspear.procurement.model.methods.TechnicalCriteriaParameters;
import com.qspear.procurement.model.methods.TechnicalCriterias;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageRFPDocument;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.persistence.methods.TeqipPackageTechSubCriteria;
import com.qspear.procurement.repositories.method.RFPDetailRepository;
import com.qspear.procurement.repositories.method.RFPDocumentRepository;
import com.qspear.procurement.repositories.method.TechCriteriaRepository;
import com.qspear.procurement.repositories.method.TechSubCriteriaRepository;
import com.qspear.procurement.util.ProcurementUtility;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class RFPMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    RFPDetailRepository rFPDetailRepository;

    @Autowired
    RFPDocumentRepository rFPDocumentRepository;

    @Autowired
    TechCriteriaRepository techCriteriaRepository;

    @Autowired
    TechSubCriteriaRepository techSubCriteriaRepository;

    @Autowired
    ProcurementUtility procurementUtility;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {

        TeqipPackageRFPDetail entity = rFPDetailRepository.findByTeqipPackage(teqipPackage);
        RFP rfp = new RFP();

        if (entity != null) {
            RFPDetail details = new RFPDetail();
            details.setId(entity.getRfpdetailId());
            details.setPreProposalMeetingDate(entity.getPreProposalMeetingDate());
            details.setProposalSubmissionDate(entity.getProposalSubmissionDate());
            details.setTypeOfContract(entity.getTypeOfContract());
            details.setPreProposalVenue(entity.getPreProposalVenue());
            details.setIsWordWorkBankNOC(entity.getIsWordWorkBankNOC());
            details.setWordBankNOCDate(entity.getWordBankNOCDate());
            rfp.setDetails(details);

        }

        TeqipPackageRFPDocument entityDocument = rFPDocumentRepository.findByTeqipPackage(teqipPackage);

        if (entityDocument != null) {
            RfpDocument rfpDocument = new RfpDocument();
            rfpDocument.setPkgName(teqipPackage.getPackageName());
            rfpDocument.setPkgCode(teqipPackage.getPackageCode());
            rfpDocument.setReferenceNo(entityDocument.getReferenceNo());

            TechnicalCriteriaParameters criteriaParams = new TechnicalCriteriaParameters();

            criteriaParams.setMinTechnicalPassingScore(entityDocument.getMinTechnicalPassingScore());
            criteriaParams.setWeightageForFinancialProposal(entityDocument.getWeightageForFinancialProposal());
            criteriaParams.setWeightageForTechnicalProposal(entityDocument.getWeightageForTechnicalProposal());
            criteriaParams.setParagraph1_2(entityDocument.getParagraph1_2());
            criteriaParams.setParagraph1_3(entityDocument.getParagraph1_3());
            criteriaParams.setParagraph1_4(entityDocument.getParagraph1_4());
            criteriaParams.setParagraph1_14(entityDocument.getParagraph1_14());
            criteriaParams.setParagraph1_14Date(entityDocument.getParagraph1_14Date());
            criteriaParams.setParagraph2_1(entityDocument.getParagraph2_1());
            criteriaParams.setParagraph2_1Address(entityDocument.getParagraph2_1Address());
            criteriaParams.setParagraph3_3(entityDocument.getParagraph3_3());
            criteriaParams.setParagraph3_4(entityDocument.getParagraph3_4());
            criteriaParams.setParagraph3_4IsTraining(entityDocument.getParagraph3_4IsTraining());
            criteriaParams.setParagraph3_4Information(entityDocument.getParagraph3_4Information());
            criteriaParams.setParagraph3_6(entityDocument.getParagraph3_6());
            criteriaParams.setParagraph4_3(entityDocument.getParagraph4_3());
            criteriaParams.setParagraph4_5(entityDocument.getParagraph4_5());
            criteriaParams.setParagraph4_5Date(entityDocument.getParagraph4_5Date());
            criteriaParams.setParagraph4_5DateTime(entityDocument.getParagraph4_5DateTime());
            criteriaParams.setParagraph6_1Date(entityDocument.getParagraph6_1Date());
            criteriaParams.setParagraph6_1Address(entityDocument.getParagraph6_1Address());
            criteriaParams.setParagraph7_1(entityDocument.getParagraph7_1());
            criteriaParams.setParagraph7_1Address(entityDocument.getParagraph7_1Address());

            rfpDocument.setTechnicalCriteriaParameters(criteriaParams);

            ArrayList<TeqipPackageTechCriteria> technicalCriteriasList = techCriteriaRepository.findByTeqipPackage(teqipPackage);
            if (technicalCriteriasList != null) {
                ArrayList<TechnicalCriterias> technicalCriterias = new ArrayList<>();

                for (TeqipPackageTechCriteria entityCriteria : technicalCriteriasList) {
                    TechnicalCriterias criterias = new TechnicalCriterias();
                    criterias.setId(entityCriteria.getTechCriteriaId());
                    criterias.setDescription(entityCriteria.getTechCriteriaDesc());
                    criterias.setMarks(entityCriteria.getMarks());

                    ArrayList<TeqipPackageTechSubCriteria> techSubCriteriaList = techSubCriteriaRepository
                            .findByTeqipPackageAndTeqipPackageTechCriteria(teqipPackage, entityCriteria);

                    if (techSubCriteriaList != null) {
                        ArrayList<SubCriteria> subCriteria = new ArrayList<>();

                        for (TeqipPackageTechSubCriteria entitySubCriteria : techSubCriteriaList) {

                            SubCriteria subCritera = new SubCriteria();
                            subCritera.setId(entitySubCriteria.getTechsubcriteriaId());
                            subCritera.setSubCriteriaQuestionText(entitySubCriteria.getTechCriteriaDesc());
                            subCritera.setMarks(entitySubCriteria.getMarks());

                            subCriteria.add(subCritera);
                        }

                        criterias.setSubCriteria(subCriteria);

                    }
                    technicalCriterias.add(criterias);

                }

                rfpDocument.setTechnicalCriterias(technicalCriterias);

            }

            rfp.setRfpDocument(rfpDocument);
        }

        ArrayList<Uploads> uploads = uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_QCBS_RFP);
        rfp.setRfpGeneration(uploads);

        response.setRfp(rfp);
    }

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {
        RFP rfp = methodPackageUpdateRequest.getRfp();
        if (rfp == null) {
            return;
        }
        RFPDetail details = rfp.getDetails();
        if (details != null) {
            TeqipPackageRFPDetail entity = rFPDetailRepository.findByTeqipPackage(teqipPackage);
            entity = entity != null ? entity : new TeqipPackageRFPDetail();
            entity.setTeqipPackage(teqipPackage);

            entity.setPreProposalMeetingDate(details.getPreProposalMeetingDate());
            entity.setProposalSubmissionDate(details.getProposalSubmissionDate());
            entity.setTypeOfContract(details.getTypeOfContract());
            entity.setPreProposalVenue(details.getPreProposalVenue());
            entity.setIsWordWorkBankNOC(details.getIsWordWorkBankNOC());
            entity.setWordBankNOCDate(details.getWordBankNOCDate());
            rFPDetailRepository.saveAndFlush(entity);

        }

        RfpDocument rfpDocument = rfp.getRfpDocument();
        if (rfpDocument != null) {
            TeqipPackageRFPDocument entityDocument = rFPDocumentRepository.findByTeqipPackage(teqipPackage);
            entityDocument = entityDocument != null ? entityDocument : new TeqipPackageRFPDocument();
            entityDocument.setTeqipPackage(teqipPackage);

            TechnicalCriteriaParameters criteriaParams = rfpDocument.getTechnicalCriteriaParameters();
            if (criteriaParams != null) {

                entityDocument.setMinTechnicalPassingScore(criteriaParams.getMinTechnicalPassingScore());
                entityDocument.setWeightageForFinancialProposal(criteriaParams.getWeightageForFinancialProposal());
                entityDocument.setWeightageForTechnicalProposal(criteriaParams.getWeightageForTechnicalProposal());
                entityDocument.setParagraph1_2(criteriaParams.getParagraph1_2());
                entityDocument.setParagraph1_3(criteriaParams.getParagraph1_3());
                entityDocument.setParagraph1_4(criteriaParams.getParagraph1_4());
                entityDocument.setParagraph1_14(criteriaParams.getParagraph1_14());
                entityDocument.setParagraph1_14Date(criteriaParams.getParagraph1_14Date());
                entityDocument.setParagraph2_1(criteriaParams.getParagraph2_1());
                entityDocument.setParagraph2_1Address(criteriaParams.getParagraph2_1Address());
                entityDocument.setParagraph3_3(criteriaParams.getParagraph3_3());
                entityDocument.setParagraph3_4(criteriaParams.getParagraph3_4());
                entityDocument.setParagraph3_4IsTraining(criteriaParams.getParagraph3_4IsTraining());
                entityDocument.setParagraph3_4Information(criteriaParams.getParagraph3_4Information());
                entityDocument.setParagraph3_6(criteriaParams.getParagraph3_6());
                entityDocument.setParagraph4_3(criteriaParams.getParagraph4_3());
                entityDocument.setParagraph4_5(criteriaParams.getParagraph4_5());
                entityDocument.setParagraph4_5Date(criteriaParams.getParagraph4_5Date());
                entityDocument.setParagraph4_5DateTime(criteriaParams.getParagraph4_5DateTime());
                entityDocument.setParagraph6_1Date(criteriaParams.getParagraph6_1Date());
                entityDocument.setParagraph6_1Address(criteriaParams.getParagraph6_1Address());
                entityDocument.setParagraph7_1(criteriaParams.getParagraph7_1());
                entityDocument.setParagraph7_1Address(criteriaParams.getParagraph7_1Address());
            }

            rFPDocumentRepository.saveAndFlush(entityDocument);

            if (entityDocument.getReferenceNo() == null) {

            }

            if (entityDocument.getReferenceNo() == null) {

                String poNumber = procurementUtility.getPackageCode();
                poNumber = poNumber
                        + teqipPackage.getTeqipProcurementmethod().getProcurementmethodCode()
                        + "/" + entityDocument.getRfpDocumentId();

                entityDocument.setReferenceNo(poNumber);
                rFPDocumentRepository.saveAndFlush(entityDocument);

            }

            ArrayList<TechnicalCriterias> technicalCriterias = rfpDocument.getTechnicalCriterias();
            if (technicalCriterias != null) {
                for (TechnicalCriterias technicalCriteria : technicalCriterias) {

                    TeqipPackageTechCriteria entityCriteria = technicalCriteria.getId() != null
                            ? techCriteriaRepository.findOne(technicalCriteria.getId())
                            : new TeqipPackageTechCriteria();
                    entityCriteria.setTeqipPackage(teqipPackage);
                    entityCriteria.setTechCriteriaDesc(technicalCriteria.getDescription());
                    entityCriteria.setMarks(technicalCriteria.getMarks());

                    techCriteriaRepository.saveAndFlush(entityCriteria);

                    ArrayList<SubCriteria> subCriteria = technicalCriteria.getSubCriteria();
                    if (subCriteria != null) {

                        for (SubCriteria subCriteriaModel : subCriteria) {
                            TeqipPackageTechSubCriteria entitySubCriteria = subCriteriaModel.getId() != null
                                    ? techSubCriteriaRepository.findOne(subCriteriaModel.getId())
                                    : new TeqipPackageTechSubCriteria();
                            entitySubCriteria.setTeqipPackage(teqipPackage);
                            entitySubCriteria.setTeqipPackageTechCriteria(entityCriteria);

                            entitySubCriteria.setTechCriteriaDesc(subCriteriaModel.getSubCriteriaQuestionText());
                            entitySubCriteria.setMarks(subCriteriaModel.getMarks());

                            techSubCriteriaRepository.saveAndFlush(entitySubCriteria);

                        }

                    }

                }

            }

        }

    }

}
