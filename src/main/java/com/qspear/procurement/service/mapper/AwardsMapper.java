package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.Award;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.PriorReview;
import com.qspear.procurement.model.methods.QuotationForm;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAward;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageAwardRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsSupplierInputRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class AwardsMapper implements MapperInterface<TeqipPackage> {
    
    @Autowired
    PackageQuotationsSupplierInputRepository packageQuotationsSupplierInputRepository;
    
    @Autowired
    SupplierRepository supplierRepository;
    
    @Autowired
    PackageAwardRepository packageAwardRepository;
    
    @Autowired
    QuestionMapper questionMapper;
    
    @Autowired
    UploadMapper uploadMapper;
    
  
    @Override
    public void mapObjectToEntity(TeqipPackage entity, MethodPackageUpdateRequest mappingObject) {
        Award award = mappingObject.getAward();
        
        if (award == null) {
            return;
        }
        
        TeqipPackageAward teqipPackageAward = packageAwardRepository.findByTeqipPackage(entity);
        
        teqipPackageAward = teqipPackageAward != null ? teqipPackageAward : new TeqipPackageAward();
        
        if (award.getL1SupplierId() != null) {
            TeqipPmssSupplierMaster supplierMaster = supplierRepository.findOne(award.getL1SupplierId());
            teqipPackageAward.setTeqipPmssSupplierMaster(supplierMaster);
        }
        
        teqipPackageAward.setTeqipPackage(entity);
        teqipPackageAward.setAwardComments(award.getRecommandationComments());
        
        packageAwardRepository.saveAndFlush(teqipPackageAward);
        
        ArrayList<QuotationForm> quotationForm = award.getExtendValidity();
        if (quotationForm != null) {
            
            for (QuotationForm form : quotationForm) {
                
                TeqipPmssSupplierMaster supplierMaster = supplierRepository.findOne(form.getSupplierId());
                
                this.questionMapper.saveQuestionInputs(form.getQuestions(), supplierMaster, entity, "extendValidity");
                
            }
        }
                
    }
    
    @Override
    public void mapEntityToObject(TeqipPackage entity, MethodPackageResponse mappingObject) {
        
        TeqipPackageAward teqipPackageAward = packageAwardRepository.findByTeqipPackage(entity);
        if (teqipPackageAward == null) {
            return;
        }
        
        Award award = new Award();
        award.setPackageAwardDetailId(teqipPackageAward.getPackageAwardDetailId());
        award.setRecommandationComments(teqipPackageAward.getAwardComments());
        
        TeqipPmssSupplierMaster supplierMaster = teqipPackageAward.getTeqipPmssSupplierMaster();
        if (supplierMaster != null) {
            award.setL1Supplier(supplierMaster.getSupplierName());
            award.setL1SupplierId(supplierMaster.getSupplierId());
        }

        //Set Opening fill form 
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = entity.getTeqipPackageSupplierDetails();
        List<TeqipPackageQuotationsSupplierInput> teqipPackageQuotationsEvaluations = packageQuotationsSupplierInputRepository.findByTeqipPackage(entity);
        
        ArrayList<QuotationForm> extendValidity = new ArrayList<>();
        
        if (teqipPackageSupplierDetails != null) {
            
            for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {
                TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
                
                if (teqipPmssSupplierMaster != null) {
                    QuotationForm form = new QuotationForm();
                    form.setSupplierId(teqipPmssSupplierMaster.getSupplierId());
                    form.setSupplierName(teqipPmssSupplierMaster.getSupplierName());
                    form.setQuestions(this.questionMapper.setQuestionInput(teqipPackageQuotationsEvaluations, teqipPmssSupplierMaster, null, "extendValidity"));
                    
                    extendValidity.add(form);
                }
            }
        }
        
        award.setExtendValidity(extendValidity);
      
        ArrayList<Uploads> uploads = uploadMapper.getUploads(entity, entity.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_NCB_BID_AWARD);
        award.setAwardGeneration(uploads);
        
        mappingObject.setAward(award);
        
    }
    
}
