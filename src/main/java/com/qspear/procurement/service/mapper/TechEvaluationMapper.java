/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.TechEvaluation;
import com.qspear.procurement.model.methods.TechEvaluationDetails;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageIssueRFP;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.repositories.method.IssueRFPRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsSupplierInputRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class TechEvaluationMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackageQuotationsSupplierInputRepository packageQuotationsSupplierInputRepository;

    @Autowired
    IssueRFPRepository issueRFPRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    QuestionMapper questionMapper;

    @Autowired
    QuotationEvaluationMapper quotationEvaluationMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {

        TechEvaluation techEvaluation = mappingObject.getTechEvaluation();
        if (techEvaluation == null) {
            return;
        }
        TechEvaluationDetails details = techEvaluation.getDetails();
        if (details != null) {
            TeqipPackageIssueRFP entity = issueRFPRepository.findByTeqipPackage(teqipPackage);
            entity = entity != null ? entity : new TeqipPackageIssueRFP();
            entity.setTeqipPackage(teqipPackage);

            entity.setTechProposalOpeningDate(details.getTechProposalOpeningDate());
            entity.setPlannedFinancialOpeningDate(details.getPlannedFinancialOpeningDate());
            entity.setPlannedFinancialOpeningDateTime(details.getPlannedFinancialOpeningTime());

            issueRFPRepository.saveAndFlush(entity);
        }

        quotationEvaluationMapper.saveQuationForm(techEvaluation.getEvaluationForms(), teqipPackage,"instructionToConsultants");

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {
        TechEvaluation techEvaluation = new TechEvaluation();

        TeqipPackageIssueRFP entity = issueRFPRepository.findByTeqipPackage(teqipPackage);

        if (entity != null) {

            TechEvaluationDetails details = new TechEvaluationDetails();

            details.setId(entity.getIssueOfRfpId());
            details.setTechProposalOpeningDate(entity.getTechProposalOpeningDate());
            details.setPlannedFinancialOpeningDate(entity.getPlannedFinancialOpeningDate());
            details.setPlannedFinancialOpeningTime(entity.getPlannedFinancialOpeningDateTime());

            techEvaluation.setDetails(details);
        }
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = teqipPackage.getTeqipPackageSupplierDetails();

        List<TeqipPackageQuotationsSupplierInput> teqipPackageQuotationsEvaluations = packageQuotationsSupplierInputRepository.findByTeqipPackage(teqipPackage);

        techEvaluation.setEvaluationForms(quotationEvaluationMapper.setQuationForm(teqipPackage, teqipPackageSupplierDetails, teqipPackageQuotationsEvaluations,"instructionToConsultants"));

        mappingObject.setTechEvaluation(techEvaluation);
    }

}
