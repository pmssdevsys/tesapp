/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.TeqipItemWorkDetailModel;
import com.qspear.procurement.persistence.TeqipItemWorkDetail;
import com.qspear.procurement.persistence.TeqipItemWorkDetailHistory;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.repositories.method.DepartmentMasterRepository;
import com.qspear.procurement.repositories.ItemWorkRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jaspreet
 */
@Service
public class ItemWorkMapper {

    @Autowired
    ItemWorkRepository itemWorkRepository;

    @Autowired
    DepartmentMasterRepository departmentMasterRepository;

    public List<TeqipItemWorkDetailModel> mapEntityToObject(List<TeqipItemWorkDetail> teqipItemWorkDetails) {
        if (teqipItemWorkDetails == null) {
            return null;
        }

        List<TeqipItemWorkDetailModel> list = new ArrayList<>();

        for (TeqipItemWorkDetail entity : teqipItemWorkDetails) {
            TeqipItemWorkDetailModel model = new TeqipItemWorkDetailModel();

            model.setId(entity.getId());
            model.setCreatedBy(entity.getCreatedBy());
            model.setModifyBy(entity.getModifyBy());
            model.setModifyOn(entity.getModifyOn());
            model.setWorkCost(entity.getWorkCost());
            model.setWorkName(entity.getWorkName());
            model.setWorkSpecification(entity.getWorkSpecification());
            model.setReviseId(entity.getReviseId());
            model.setStatus(entity.getStatus());
            model.setDeliveyPeriod(entity.getDeliveyPeriod());
            model.setTrainingRequired(entity.getTrainingRequired());
            model.setInstallationRequired(entity.getInstallationRequired());
            model.setPlaceofdelivery(entity.getPlaceOfDelivery());
            model.setInstllationrequirement(entity.getInstllationRequirement());

            if (entity.getTeqipPmssDepartmentMaster() != null) {
                model.setTeqipPmssDepartmentMasterId(entity.getTeqipPmssDepartmentMaster().getDepartmentId());
                model.setTeqipPmssDepartmentMaster(entity.getTeqipPmssDepartmentMaster().getDepartment());
            }
            if (entity.getTeqipPackage() != null) {
                model.setTeqipPackageId(entity.getTeqipPackage().getPackageId());
            }

            list.add(model);

        }
        return list;
    }

    public List<TeqipItemWorkDetailModel> mapWorkHistory(List<TeqipItemWorkDetailHistory> teqipItemWorkDetails) {
        if (teqipItemWorkDetails == null) {
            return null;
        }

        List<TeqipItemWorkDetailModel> list = new ArrayList<>();

        for (TeqipItemWorkDetailHistory entity : teqipItemWorkDetails) {
            TeqipItemWorkDetailModel model = new TeqipItemWorkDetailModel();

            model.setId(entity.getId());
            model.setCreatedBy(entity.getCreatedBy());
            model.setModifyBy(entity.getModifyBy());
            model.setModifyOn(entity.getModifyOn());
            model.setWorkCost(entity.getWorkCost());
            model.setWorkName(entity.getWorkName());
            model.setWorkSpecification(entity.getWorkSpecification());
            model.setReviseId(entity.getReviseId());
            model.setStatus(entity.getStatus());
            model.setDeliveyPeriod(entity.getDeliveyPeriod());
            model.setTrainingRequired(entity.getTrainingRequired());
            model.setInstallationRequired(entity.getInstallationRequired());
            model.setPlaceofdelivery(entity.getPlaceOfDelivery());
            model.setInstllationrequirement(entity.getInstllationRequirement());

            if (entity.getTeqipPmssDepartmentMaster() != null) {
                model.setTeqipPmssDepartmentMasterId(entity.getTeqipPmssDepartmentMaster().getDepartmentId());
                model.setTeqipPmssDepartmentMaster(entity.getTeqipPmssDepartmentMaster().getDepartment());
            }
            if (entity.getTeqipPackage() != null) {
                model.setTeqipPackageId(entity.getTeqipPackageHistory().getPackageHistoryId());
            }

            list.add(model);

        }
        return list;
    }

    public void mapObjectToEntity(List<TeqipItemWorkDetailModel> itemModel, TeqipPackage teqipPackage) {

        this.mapObjectToEntity(itemModel, teqipPackage, true);
    }

    public void mapObjectToEntity(List<TeqipItemWorkDetailModel> itemModel, TeqipPackage teqipPackage, boolean deleteOther) {

        if (itemModel == null) {
            return;
        }

        List<Integer> newId = new ArrayList<>();
        List<TeqipItemWorkDetail> oldItemWork = teqipPackage.getTeqipItemWorkDetails();
        for (TeqipItemWorkDetailModel model : itemModel) {

            TeqipItemWorkDetail entity = model.getId() != null
                    ? itemWorkRepository.findOne(model.getId())
                    : new TeqipItemWorkDetail();

            entity.setCreatedBy(model.getCreatedBy());
            entity.setModifyBy(model.getModifyBy());
            entity.setModifyOn(model.getModifyOn());
            entity.setWorkCost(model.getWorkCost());
            entity.setWorkName(model.getWorkName());
            entity.setWorkSpecification(model.getWorkSpecification());
            entity.setStatus(model.getStatus());
            entity.setTeqipPackage(teqipPackage);
            entity.setTeqipCategorymaster(teqipPackage.getTeqipCategorymaster());
            entity.setDeliveyPeriod(model.getDeliveyPeriod());
            entity.setTrainingRequired(model.getTrainingRequired());
            entity.setInstallationRequired(model.getInstallationRequired());
            entity.setPlaceOfDelivery(model.getPlaceofdelivery());
            entity.setInstllationRequirement(model.getInstllationrequirement());

            if (entity.getReviseId() == null) {
                entity.setReviseId(1);
            }

            if (model.getTeqipPmssDepartmentMasterId() != null) {
                entity.setTeqipPmssDepartmentMaster(departmentMasterRepository.findOne(model.getTeqipPmssDepartmentMasterId()));

            }
            itemWorkRepository.saveAndFlush(entity);

            newId.add(entity.getId());

        }
        if (deleteOther) {
            this.deletePackageWorks(oldItemWork, newId);
        }

    }

    @Transactional
    public void deletePackageWorks(List<TeqipItemWorkDetail> oldList, List<Integer> newId) {
        if (oldList == null) {
            return;
        }

        for (TeqipItemWorkDetail entity : oldList) {
            if (!newId.contains(entity.getId())) {
                System.out.println("TeqipItemWorkDetail" + entity.getId());

                itemWorkRepository.delete(entity.getId());
                itemWorkRepository.flush();

            }
        }

    }
}
