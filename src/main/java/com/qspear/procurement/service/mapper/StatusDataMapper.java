/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.ActionModel;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.StageModel;
import com.qspear.procurement.model.methods.StatusData;
import com.qspear.procurement.model.methods.StatusInformation;
import com.qspear.procurement.model.methods.StatusSummary;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPlanApprovalstatus;
import com.qspear.procurement.persistence.User;
import com.qspear.procurement.persistence.methods.TeqipPackageL1SupplierDetails;
import com.qspear.procurement.persistence.methods.TeqipPackageStage;
import com.qspear.procurement.persistence.methods.TeqipPackageStageIndex;
import com.qspear.procurement.persistence.methods.TeqipPackageStatusInfo;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.TeqipPlanApprovalStatusRepository;
import com.qspear.procurement.repositories.method.PackageStageIndexRepository;
import com.qspear.procurement.repositories.method.PackageStageRepository;
import com.qspear.procurement.repositories.method.PackageStatusInfoRepository;
import com.qspear.procurement.security.util.LoggedUser;
import com.qspear.procurement.security.util.LoginUser;
import com.qspear.procurement.service.UserControllerService;
import com.qspear.procurement.service.mapper.MapperInterface;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class StatusDataMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackageStageRepository packageStageRepository;

    @Autowired
    PackageStageIndexRepository packageStageIndexRepository;

    @Autowired
    PackageStatusInfoRepository packageStatusInfoRepository;

    @Autowired
    UploadMapper uploadMapper;

    @Autowired
    UserControllerService userControllerService;

    @Autowired
    protected TeqipPlanApprovalStatusRepository teqipPlanApprovalStatusRepository;

    @Autowired
    protected TeqipPackageRepository teqipPackageRepository;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {

        StatusData statusData = mappingObject.getStatusData();
        if (statusData == null) {
            return;
        }
        StatusInformation information = statusData.getInformation();
        if (information != null) {
            TeqipPackageStatusInfo teqipPackageStatusInfo = packageStatusInfoRepository.findByTeqipPackage(teqipPackage);
            teqipPackageStatusInfo = teqipPackageStatusInfo != null ? teqipPackageStatusInfo : new TeqipPackageStatusInfo();

            teqipPackageStatusInfo.setTeqipPackage(teqipPackage);
            teqipPackageStatusInfo.setStartedDate(information.getStartedDate());
            teqipPackageStatusInfo.setLastRunDate(information.getLastRunDate());
            teqipPackageStatusInfo.setOriginatorName(information.getOriginatorName());
            teqipPackageStatusInfo.setLastActorName(information.getLastActorName());
            teqipPackageStatusInfo.setCurrentStage(information.getCurrentStage());
            teqipPackageStatusInfo.setApprovers(information.getApprovers());

            packageStatusInfoRepository.saveAndFlush(teqipPackageStatusInfo);

        }

        StatusSummary summary = statusData.getSummary();
        if (summary != null) {

            StageModel nextStage = summary.getNextStage();
            if (nextStage != null) {

                TeqipPackageStageIndex packageStageIndex = packageStageIndexRepository.findTopByTeqipPackageAndCategoryOrderByPackageStageIndexIdDesc(teqipPackage, "next");
                packageStageIndex = packageStageIndex != null ? packageStageIndex : new TeqipPackageStageIndex();

                packageStageIndex.setStageIndex(nextStage.getStageIndex());
                packageStageIndex.setStageName(nextStage.getStageName());
                packageStageIndex.setCategory("next");
                packageStageIndex.setTeqipPackage(teqipPackage);
                packageStageIndexRepository.saveAndFlush(packageStageIndex);

                List<ActionModel> actionsList = nextStage.getActionsList();
                if (actionsList != null) {

                    for (ActionModel actionModel : actionsList) {

                        TeqipPackageStage entity = actionModel.getId() != null
                                ? packageStageRepository.findOne(actionModel.getId())
                                : new TeqipPackageStage();
                        entity.setTeqipPackage(teqipPackage);
                        entity.setTeqipPackageStageIndex(packageStageIndex);
                        entity.setActionTime(actionModel.getActionTime());
                        entity.setActorName(actionModel.getActorName());
                        entity.setActionString(actionModel.getActionString());
                        entity.setActionType(actionModel.getActionType());
                        entity.setComments(actionModel.getComments());

                        packageStageRepository.saveAndFlush(entity);

                    }
                }
            }

            List<StageModel> stages = summary.getStages();
            if (stages != null) {
                for (StageModel stage : stages) {
                    TeqipPackageStageIndex packageStageIndex = stage.getPackageStageIndexId() != null
                            ? packageStageIndexRepository.findOne(stage.getPackageStageIndexId()) : new TeqipPackageStageIndex();
                packageStageIndex = packageStageIndex != null ? packageStageIndex : new TeqipPackageStageIndex();

                    packageStageIndex.setStageIndex(stage.getStageIndex());
                    packageStageIndex.setStageName(stage.getStageName());
                    packageStageIndex.setCategory("current");
                    packageStageIndex.setTeqipPackage(teqipPackage);
                    packageStageIndexRepository.saveAndFlush(packageStageIndex);

                    List<ActionModel> actionsList = stage.getActionsList();
                    if (actionsList != null) {
                        for (ActionModel actionModel : actionsList) {
                            TeqipPackageStage entity = actionModel.getId() != null
                                    ? packageStageRepository.findOne(actionModel.getId())
                                    : new TeqipPackageStage();
                            entity.setTeqipPackage(teqipPackage);
                            entity.setTeqipPackageStageIndex(packageStageIndex);
                            entity.setActionTime(actionModel.getActionTime());
                            entity.setActorName(actionModel.getActorName());
                            entity.setActionString(actionModel.getActionString());
                            entity.setActionType(actionModel.getActionType());
                            entity.setComments(actionModel.getComments());
                            packageStageRepository.saveAndFlush(entity);

                        }
                    }

                }
            }

        }

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        StatusData statusData = new StatusData();

        StatusInformation information = new StatusInformation();

        TeqipPackageStatusInfo teqipPackageStatusInfo = packageStatusInfoRepository
                .findByTeqipPackage(teqipPackage);
        if (teqipPackageStatusInfo != null) {

            information.setStartedDate(teqipPackageStatusInfo.getStartedDate());
            information.setLastRunDate(teqipPackageStatusInfo.getLastRunDate());
            information.setOriginatorName(teqipPackageStatusInfo.getOriginatorName());
            information.setLastActorName(teqipPackageStatusInfo.getLastActorName());
            information.setCurrentStage(teqipPackageStatusInfo.getCurrentStage());
            information.setApprovers(teqipPackageStatusInfo.getApprovers());
        } else {

            information.setStartedDate(teqipPackage.getCreatedOn());
            information.setLastRunDate(teqipPackage.getModifyOn());

            User user = teqipPackage.getCreatedBy() != null ? userControllerService.findByUserid(teqipPackage.getCreatedBy()) : null;
            if (user != null) {
                information.setOriginatorName(user.getUserName());
            }
            User user1 = teqipPackage.getModifyBy() != null ? userControllerService.findByUserid(teqipPackage.getModifyBy()) : null;
            if (user1 != null) {
                information.setLastActorName(user1.getUserName());
            }

            TeqipPlanApprovalstatus approvalStatus = teqipPlanApprovalStatusRepository.findFirstByTeqipInstitutionAndTeqipStatemasterAndTypeOrderByIdDesc(
                    teqipPackage.getTeqipInstitution(), teqipPackage.getTeqipStatemaster(),teqipPackage.getTypeofplanCreator());
            if (approvalStatus != null) {
                User user2 = approvalStatus.getCreatedBy() != null ? userControllerService.findByUserid(approvalStatus.getCreatedBy()) : null;
                if (user2 != null) {
                    information.setApprovers(user2.getUserName());
                }
            }

            TeqipPackageStageIndex currentStage = packageStageIndexRepository
                    .findTopByTeqipPackageAndCategoryOrderByPackageStageIndexIdDesc(teqipPackage, "current");
            if (currentStage != null) {
                information.setCurrentStage(currentStage.getStageName());
            }

        }
        statusData.setInformation(information);

        StatusSummary summary = new StatusSummary();

        TeqipPackageStageIndex stageIndex = packageStageIndexRepository
                .findTopByTeqipPackageAndCategoryOrderByPackageStageIndexIdDesc(teqipPackage, "next");
        if (stageIndex != null) {
            StageModel model = new StageModel();
            model.setPackageStageIndexId(stageIndex.getPackageStageIndexId());
            model.setStageIndex(stageIndex.getStageIndex());
            model.setStageName(stageIndex.getStageName());
            List<ActionModel> actionsList = new ArrayList<>();

            List<TeqipPackageStage> list = packageStageRepository
                    .findByTeqipPackageAndTeqipPackageStageIndex(teqipPackage, stageIndex);
            if (list != null) {
                for (TeqipPackageStage entity : list) {

                    ActionModel actionModel = new ActionModel();

                    actionModel.setActionTime(entity.getActionTime());
                    actionModel.setActorName(entity.getActorName());
                    actionModel.setActionString(entity.getActionString());
                    actionModel.setActionType(entity.getActionType());
                    actionModel.setComments(entity.getComments());
                    actionModel.setId(entity.getPackageStageId());

                    actionsList.add(actionModel);
                }
            }

            model.setActionsList(actionsList);
            summary.setNextStage(model);
        }

        List<TeqipPackageStageIndex> currentStages = packageStageIndexRepository
                .findByTeqipPackageAndCategory(teqipPackage, "current");
        if (currentStages != null) {
            List<StageModel> stageModels = new ArrayList<>();
            for (TeqipPackageStageIndex stage : currentStages) {
                StageModel model = new StageModel();
                model.setPackageStageIndexId(stage.getPackageStageIndexId());
                model.setStageIndex(stage.getStageIndex());
                model.setStageName(stage.getStageName());
                List<ActionModel> actionsList = new ArrayList<>();

                List<TeqipPackageStage> list = packageStageRepository
                        .findByTeqipPackageAndTeqipPackageStageIndex(teqipPackage, stage);
                if (list != null) {
                    for (TeqipPackageStage entity : list) {

                        ActionModel actionModel = new ActionModel();

                        actionModel.setActionTime(entity.getActionTime());
                        actionModel.setActorName(entity.getActorName());
                        actionModel.setActionString(entity.getActionString());
                        actionModel.setActionType(entity.getActionType());
                        actionModel.setComments(entity.getComments());
                        actionModel.setId(entity.getPackageStageId());

                        actionsList.add(actionModel);
                    }
                }

                model.setActionsList(actionsList);
                stageModels.add(model);
            }
            summary.setStages(stageModels);

        }

        statusData.setSummary(summary);
        mappingObject.setStatusData(statusData);
    }

}
