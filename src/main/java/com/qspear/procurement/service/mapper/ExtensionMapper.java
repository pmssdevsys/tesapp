/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.Extension;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageExtension;
import com.qspear.procurement.repositories.method.ExtensionRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class ExtensionMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    ExtensionRepository extensionRepository;

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {

        List<TeqipPackageExtension> list = extensionRepository.findByTeqipPackage(teqipPackage);
        if (list == null) {
            return;
        }

        ArrayList<Extension> extensions = new ArrayList<>();

        for (TeqipPackageExtension entity : list) {

            Extension details = new Extension();
            details.setExtensionId(entity.getExtensionId());
            details.setExtendedDate(entity.getExtendedDate());
            details.setReasonforExtension(entity.getReasonforExtension());
            details.setSubmissionDate(entity.getSubmissionDate());

            extensions.add(details);

        }
        response.setExtension(extensions);
    }

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {

        ArrayList<Extension> extensions = methodPackageUpdateRequest.getExtension();

        if (extensions == null) {
            return;
        }

        for (Extension details : extensions) {

            TeqipPackageExtension entity = details.getExtensionId() != null
                    ? extensionRepository.findOne(details.getExtensionId())
                    : new TeqipPackageExtension();
            entity.setTeqipPackage(teqipPackage);
            entity.setExtensionId(details.getExtensionId());
            entity.setExtendedDate(details.getExtendedDate());
            entity.setReasonforExtension(details.getReasonforExtension());
            entity.setSubmissionDate(details.getSubmissionDate());

            extensionRepository.saveAndFlush(entity);

        }
    }
}
