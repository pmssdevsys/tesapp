package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.AdvIssueOfBid;
import com.qspear.procurement.model.methods.BidExtension;
import com.qspear.procurement.model.methods.GenerateAdv;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.PreBidMeetingInformation;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.model.methods.ViewAdvDetails;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageBidAdvertisement;
import com.qspear.procurement.persistence.methods.TeqipPackageBidMeetingDetail;
import com.qspear.procurement.repositories.method.PackageBidAdvertisementRepository;
import com.qspear.procurement.repositories.method.PackageBidMeetingDetailRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class AdvIssueOfBidMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PackageBidAdvertisementRepository packageBidAdvertisementRepository;

    @Autowired
    PackageSupplierMappper packageSupplierMappper;

    @Autowired
    PackageBidMeetingDetailRepository bidMeetingDetailRepository;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {

        AdvIssueOfBid advIssueOfBid = mappingObject.getAdvIssueOfBid();
        if (advIssueOfBid == null) {
            return;
        }

        this.saveAdvDetail(teqipPackage, advIssueOfBid.getGenerateAdv(), advIssueOfBid.getViewAdvDetails());
        this.saveBidMeetingDetail(teqipPackage, advIssueOfBid.getPrebidMeetingInformation(), advIssueOfBid.getBidExtension());

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        AdvIssueOfBid advIssueOfBid = new AdvIssueOfBid();

        ViewAdvDetails viewAdvDetails = this.setAdvDetails(teqipPackage);
        advIssueOfBid.setGenerateAdv(viewAdvDetails);
        advIssueOfBid.setViewAdvDetails(viewAdvDetails);

        this.setBidInformationandEstenstion(advIssueOfBid, teqipPackage);
        ArrayList<Uploads> uploads = uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_NCB_ADVERTISEMENT);
        uploads.addAll(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_QCBS_ADVERTISEMENT));

        advIssueOfBid.setAdvGeneration(uploads);
        mappingObject.setAdvIssueOfBid(advIssueOfBid);

    }

    private ViewAdvDetails setAdvDetails(TeqipPackage teqipPackage) {

        TeqipPackageBidAdvertisement entity = packageBidAdvertisementRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return null;
        }
        ViewAdvDetails viewAdvDetails = new ViewAdvDetails();
        viewAdvDetails.setLocalPaperName(entity.getLocalPaperName());
        viewAdvDetails.setNationalPaperName(entity.getNationalPaperName());
        viewAdvDetails.setDateReleaseAdv(entity.getAdvertisementDate());
        viewAdvDetails.setPackageName(teqipPackage.getPackageName());
        viewAdvDetails.setPackageId(teqipPackage.getPackageId());
        viewAdvDetails.setPublicationDateLocal(entity.getActualpublicationLocaleDate());
        viewAdvDetails.setPublicationDateNational(entity.getActualpublicationNationalDate());

        return viewAdvDetails;

    }

    private void saveAdvDetail(TeqipPackage teqipPackage, GenerateAdv generateAdv, ViewAdvDetails viewAdvDetails) {

        if (generateAdv == null && viewAdvDetails == null) {
            return;
        }

        TeqipPackageBidAdvertisement entity = packageBidAdvertisementRepository.findByTeqipPackage(teqipPackage);

        entity = entity != null ? entity : new TeqipPackageBidAdvertisement();

        entity.setTeqipPackage(teqipPackage);

        if (generateAdv != null) {
            entity.setLocalPaperName(generateAdv.getLocalPaperName());
            entity.setNationalPaperName(generateAdv.getNationalPaperName());
            entity.setAdvertisementDate(generateAdv.getDateReleaseAdv());
        }
        if (viewAdvDetails != null) {
            entity.setActualpublicationLocaleDate(viewAdvDetails.getPublicationDateLocal());
            entity.setActualpublicationNationalDate(viewAdvDetails.getPublicationDateNational());

        }

        packageBidAdvertisementRepository.saveAndFlush(entity);

    }

    private void saveBidMeetingDetail(TeqipPackage teqipPackage, PreBidMeetingInformation prebidMeetingInformation, BidExtension bidExtension) {

        if (prebidMeetingInformation == null && bidExtension == null) {
            return;
        }

        TeqipPackageBidMeetingDetail entity = bidMeetingDetailRepository.findByTeqipPackage(teqipPackage);
        entity = entity != null ? entity : new TeqipPackageBidMeetingDetail();

        entity.setTeqipPackage(teqipPackage);

        if (prebidMeetingInformation != null) {
            entity.setPrebidMinutes(prebidMeetingInformation.getPrebidMeetingTime());
            entity.setActualprebidMeetingDate(prebidMeetingInformation.getActualMeetingDate());
            entity.setIsclarificationIssues(prebidMeetingInformation.getIsClarifiationIssued());
            entity.setAnyCorrigendum(prebidMeetingInformation.getIsCorregendum());
            entity.setPrebidMom(prebidMeetingInformation.getPrebidMOM());
            entity.setMeetingClarificationDate(prebidMeetingInformation.getMeetingClarificationDate());
        }
        if (bidExtension != null) {
            entity.setCurrentSubmissionDate(bidExtension.getCurrentDate());
            entity.setExtendedDate(bidExtension.getExtendedDate());
            entity.setReasonforExtension(bidExtension.getReasonForExtension());

        }
        bidMeetingDetailRepository.saveAndFlush(entity);

    }

    private void setBidInformationandEstenstion(AdvIssueOfBid advIssueOfBid, TeqipPackage teqipPackage) {
        TeqipPackageBidMeetingDetail entity = bidMeetingDetailRepository.findByTeqipPackage(teqipPackage);
        if (entity == null) {
            return;
        }
        BidExtension bidExtension = new BidExtension();
        PreBidMeetingInformation prebidMeetingInformation = new PreBidMeetingInformation();

        prebidMeetingInformation.setPackageCode(teqipPackage.getPackageCode());
        prebidMeetingInformation.setPackageName(teqipPackage.getPackageName());
        prebidMeetingInformation.setPrebidMeetingTime(entity.getPrebidMinutes());
        prebidMeetingInformation.setActualMeetingDate(entity.getActualprebidMeetingDate());
        prebidMeetingInformation.setIsClarifiationIssued(entity.getIsclarificationIssues());
        prebidMeetingInformation.setIsCorregendum(entity.getAnyCorrigendum());
        prebidMeetingInformation.setPrebidMOM(entity.getPrebidMom());
        prebidMeetingInformation.setMeetingClarificationDate(entity.getMeetingClarificationDate());

        bidExtension.setCurrentDate(entity.getCurrentSubmissionDate());
        bidExtension.setExtendedDate(entity.getExtendedDate());
        bidExtension.setReasonForExtension(entity.getReasonforExtension());

        advIssueOfBid.setPrebidMeetingInformation(prebidMeetingInformation);
        advIssueOfBid.setBidExtension(bidExtension);

    }

}
