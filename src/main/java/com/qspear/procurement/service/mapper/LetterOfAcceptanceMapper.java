package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.LetterOfAcceptance;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageLetterAcceptance;
import com.qspear.procurement.repositories.method.LetterOfAcceptanceRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class LetterOfAcceptanceMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    LetterOfAcceptanceRepository letterOfAcceptanceRepository;

    @Autowired
    UploadMapper uploadMapper;

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest mappingObject) {
        LetterOfAcceptance model = mappingObject.getLetterOfAcceptance();

        if (model == null) {
            return;
        }

        TeqipPackageLetterAcceptance entity = letterOfAcceptanceRepository.findByTeqipPackage(teqipPackage);

        entity = entity != null ? entity : new TeqipPackageLetterAcceptance();

        entity.setTeqipPackage(teqipPackage);
        entity.setMaintancePeriodExpiryDate(model.getMaintenancePeriod());
        entity.setLoaGeneratedDate(model.getLoaGeneratedDate());
        entity.setNewBidValidity(model.getNewBidValidity());
        entity.setSecurityAmount(model.getPerformanceSecurity());

        letterOfAcceptanceRepository.saveAndFlush(entity);

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse mappingObject) {

        TeqipPackageLetterAcceptance entity = letterOfAcceptanceRepository.findByTeqipPackage(teqipPackage);

        if (entity == null) {
            return;
        }

        LetterOfAcceptance model = new LetterOfAcceptance();

        model.setLetterofAcceptanceId(entity.getPackageLetterAcceptanceId());
        model.setMaintenancePeriod(entity.getMaintancePeriodExpiryDate());
        model.setLoaGeneratedDate(entity.getLoaGeneratedDate());
        model.setNewBidValidity(entity.getNewBidValidity());
        model.setPerformanceSecurity(entity.getSecurityAmount());

        ArrayList<Uploads> uploads = uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE);
        uploads.addAll(uploadMapper.getUploads(teqipPackage, teqipPackage.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_LETTER_OF_ACCEPTANCE_DC));
        

        model.setLoaGeneration(uploads);

        mappingObject.setLetterOfAcceptance(model);
    }

}
