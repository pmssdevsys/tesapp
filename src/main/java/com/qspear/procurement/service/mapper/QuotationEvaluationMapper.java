package com.qspear.procurement.service.mapper;

import com.qspear.procurement.constants.MethodConstants;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.QuotationDetail;
import com.qspear.procurement.model.methods.QuotationEvaluation;
import com.qspear.procurement.model.methods.QuotationEvaluationForm;
import com.qspear.procurement.model.methods.QuotationForm;
import com.qspear.procurement.model.methods.QuotationItems;
import com.qspear.procurement.model.methods.QuotationWork;
import com.qspear.procurement.model.methods.SubCriteria;
import com.qspear.procurement.model.methods.TechEvaluationDetail;
import com.qspear.procurement.model.methods.TechnicalCriterias;
import com.qspear.procurement.model.methods.Uploads;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageAward;
import com.qspear.procurement.persistence.methods.TeqipPackageConsultantEvaluation;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsEvaluationItemDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsEvaluationWorkDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageSupplierDetail;
import com.qspear.procurement.persistence.methods.TeqipPackageTechCriteria;
import com.qspear.procurement.persistence.methods.TeqipPackageTechSubCriteria;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.ItemRepository;
import com.qspear.procurement.repositories.method.ConsultantEvaluationRepository;
import com.qspear.procurement.repositories.method.PackageAwardRepository;
import com.qspear.procurement.repositories.method.PackageQuestionRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.qspear.procurement.repositories.method.PackageQuotationsSupplierInputRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsEvaluationItemDetailRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsEvaluationWorkDetailRepository;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import com.qspear.procurement.repositories.method.TechSubCriteriaRepository;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by e1002703 on 4/4/2018.
 */
@Service
public class QuotationEvaluationMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackageQuotationsSupplierInputRepository packageQuotationsSupplierInputRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PackageQuestionRepository questionRepository;

    @Autowired
    QuestionMapper questionMapper;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    PackageQuotationsEvaluationItemDetailRepository quotationsEvaluationDetailRepository;

    @Autowired
    PackageQuotationsEvaluationWorkDetailRepository quotationsEvaluationWorkDetailRepository;

    @Autowired
    PackageAwardRepository packageAwardRepository;

    @Autowired
    ConsultantEvaluationRepository consultantEvaluationRepository;

    @Autowired
    TechSubCriteriaRepository techSubCriteriaRepository;

    @Autowired
    UploadMapper uploadMapper;

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    @Override
    public void mapObjectToEntity(TeqipPackage entity, MethodPackageUpdateRequest mappingObject) {

        QuotationEvaluation quotationEvaluation = mappingObject.getQuotationEvaluation();

        if (quotationEvaluation == null) {
            return;
        }

        if (quotationEvaluation.getL1Bidder() != null) {
            TeqipPackageAward teqipPackageAward = packageAwardRepository.findByTeqipPackage(entity);
            teqipPackageAward = teqipPackageAward != null ? teqipPackageAward : new TeqipPackageAward();

            teqipPackageAward.setTeqipPmssSupplierMaster(supplierRepository.findOne(quotationEvaluation.getL1Bidder()));
            packageAwardRepository.saveAndFlush(teqipPackageAward);

        }

        // Fill Evaluation Form
        this.saveQuationForm(quotationEvaluation.getEvaluationForms(), entity, "evaluation");

        ArrayList<QuotationForm> quotationForm = quotationEvaluation.getPostQualificationForms();
        if (quotationForm == null) {
            return;
        }
        for (QuotationForm form : quotationForm) {

            TeqipPmssSupplierMaster supplierMaster = supplierRepository.findOne(form.getSupplierId());

            TeqipPackageSupplierDetail packageSupplierDetail = packageSupplierRepository.findByTeqipPackageAndTeqipPmssSupplierMaster(entity, supplierMaster);
            packageSupplierDetail.setIsFormResponsive(form.getIsFormResponsive());
            packageSupplierDetail.setIsFromResponsiveSource("EVALUATION");
            packageSupplierRepository.saveAndFlush(packageSupplierDetail);

            this.questionMapper.saveQuestionInputs(form.getQuestions(), supplierMaster, entity, "post_evaluation");

        }

    }

    @Override
    public void mapEntityToObject(TeqipPackage entity, MethodPackageResponse mappingObject) {
        QuotationEvaluation quotationEvaluation = new QuotationEvaluation();

        TeqipPackageAward teqipPackageAward = packageAwardRepository.findByTeqipPackage(entity);

        if (teqipPackageAward != null && teqipPackageAward.getTeqipPmssSupplierMaster() != null) {
            quotationEvaluation.setL1Bidder(teqipPackageAward.getTeqipPmssSupplierMaster().getSupplierId());
        }
        List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails = entity.getTeqipPackageSupplierDetails();

        List<TeqipPackageQuotationsSupplierInput> teqipPackageQuotationsEvaluations = packageQuotationsSupplierInputRepository.findByTeqipPackage(entity);

        // Fill Evaluation Form
        quotationEvaluation.setEvaluationForms(this.setQuationForm(entity, teqipPackageSupplierDetails, teqipPackageQuotationsEvaluations, "evaluation"));

        ArrayList<QuotationForm> quotationForm = new ArrayList<>();
        for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {
            TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
            if (teqipPmssSupplierMaster != null) {
                QuotationForm form = new QuotationForm();
                form.setSupplierId(teqipPmssSupplierMaster.getSupplierId());
                form.setSupplierName(teqipPmssSupplierMaster.getSupplierName());
                form.setIsFormResponsive(teqipPackageSupplierDetail.getIsFormResponsive());
                form.setQuestions(this.questionMapper.setQuestionInput(teqipPackageQuotationsEvaluations, teqipPmssSupplierMaster, null, "post_evaluation"));

                quotationForm.add(form);
            }
        }

        quotationEvaluation.setPostQualificationForms(quotationForm);

        ArrayList<Uploads> uploads = uploadMapper.getUploads(entity, entity.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_QUOTATION_EVALUATION);
        uploads.addAll(uploadMapper.getUploads(entity, entity.getTeqipPackageDocuments(), MethodConstants.DOC_CAT_NCB_BID_EVALUATION));
        quotationEvaluation.setQeGeneration(uploads);

        mappingObject.setQuotationEvaluation(quotationEvaluation);
    }

    public void saveQuationForm(ArrayList<QuotationEvaluationForm> evaluationForms, TeqipPackage entity, String questionCategory) {
        if (evaluationForms == null) {
            return;
        }

        for (QuotationEvaluationForm evaluationForm : evaluationForms) {
            TeqipPmssSupplierMaster supplierMaster = supplierRepository.findOne(evaluationForm.getSupplierId());

            this.questionMapper.saveQuestionInputs(evaluationForm.getQuestions(), supplierMaster, entity, questionCategory);
            this.saveQuationDetail(evaluationForm.getQuotationDetail(), supplierMaster, entity);
            this.saveTechEvaluationDetail(evaluationForm.getTechEvaluationDetail(), supplierMaster, entity);
        }
    }

    public ArrayList<QuotationEvaluationForm> setQuationForm(TeqipPackage entity,
            List<TeqipPackageSupplierDetail> teqipPackageSupplierDetails,
            List<TeqipPackageQuotationsSupplierInput> teqipPackageQuotationsEvaluations,
            String questionCategory) {

        List<TeqipPackageQuotationsEvaluationItemDetail> teqipPackageQuotationwItemEvaluationDetails = quotationsEvaluationDetailRepository.findByTeqipPackage(entity);
        List<TeqipPackageQuotationsEvaluationWorkDetail> teqipPackageQuotationsWorkEvaluationDetails = quotationsEvaluationWorkDetailRepository.findByTeqipPackage(entity);
        List<TeqipPackageConsultantEvaluation> teqipPackageConsultantEvaluations = consultantEvaluationRepository.findByTeqipPackage(entity);

        if (teqipPackageSupplierDetails == null) {
            return null;
        }

        ArrayList<QuotationEvaluationForm> evaluationForms = new ArrayList<>();

        for (TeqipPackageSupplierDetail teqipPackageSupplierDetail : teqipPackageSupplierDetails) {

            TeqipPmssSupplierMaster teqipPmssSupplierMaster = teqipPackageSupplierDetail.getTeqipPmssSupplierMaster();
            if (teqipPmssSupplierMaster != null) {

                QuotationEvaluationForm evaluationForm = new QuotationEvaluationForm();

                evaluationForm.setSupplierId(teqipPmssSupplierMaster.getSupplierId());
                evaluationForm.setSupplierName(teqipPmssSupplierMaster.getSupplierName());

                QuotationDetail quotationDetail = new QuotationDetail();
                quotationDetail.setPackageReadOutPrice(0.0);

                evaluationForm.setQuestions(this.questionMapper.setQuestionInput(teqipPackageQuotationsEvaluations, teqipPmssSupplierMaster, quotationDetail, questionCategory));
                this.setQuationDetailItem(teqipPackageQuotationwItemEvaluationDetails, teqipPmssSupplierMaster, quotationDetail);
                this.setQuationDetailWork(teqipPackageQuotationsWorkEvaluationDetails, teqipPmssSupplierMaster, quotationDetail);

                evaluationForm.setQuotationDetail(quotationDetail);
                evaluationForm.setTechEvaluationDetail(this.setTechEvaluationDetail(teqipPackageConsultantEvaluations, teqipPmssSupplierMaster, entity));

                evaluationForms.add(evaluationForm);
            }

        }
        return evaluationForms;

    }

    private void saveQuationDetail(QuotationDetail quotationDetail, TeqipPmssSupplierMaster supplierMaster, TeqipPackage teqipPackage) {
        if (quotationDetail == null) {
            return;
        }
        saveQuotationDetailItem(quotationDetail.getItems(), supplierMaster, teqipPackage);
        saveQuotationDetailWork(quotationDetail.getWorks(), supplierMaster, teqipPackage);

    }

    private void setQuationDetailItem(
            List<TeqipPackageQuotationsEvaluationItemDetail> teqipPackageQuotationsEvaluationDetails,
            TeqipPmssSupplierMaster teqipPmssSupplierMaster,
            QuotationDetail quotationDetail) {

        if (teqipPackageQuotationsEvaluationDetails == null) {
            return;
        }
        ArrayList<QuotationItems> items = new ArrayList<>();

        Double totalPriceOfItems = 0.0;

        for (TeqipPackageQuotationsEvaluationItemDetail evaluationDetail : teqipPackageQuotationsEvaluationDetails) {

            if (!evaluationDetail.getTeqipPmssSupplierMaster().getSupplierId().equals(teqipPmssSupplierMaster.getSupplierId())) {
                continue;
            }
            QuotationItems item = new QuotationItems();
            item.setQuotationDetailId(evaluationDetail.getQuotationItemDetailId());
            if (evaluationDetail.getTeqipItemMaster() != null) {
                item.setItemMasterId(evaluationDetail.getTeqipItemMaster().getItemId());
                item.setItemName(evaluationDetail.getTeqipItemMaster().getItemName());
            }

            item.setItemQty(evaluationDetail.getItemQty());
            item.setBasicCost(evaluationDetail.getBasicCost());
            item.setTotalPrice(evaluationDetail.getTotalPrice());
            item.setComments(evaluationDetail.getComment());
            item.setDescription(evaluationDetail.getDescription());
            totalPriceOfItems += evaluationDetail.getTotalPrice() != null ? evaluationDetail.getTotalPrice() : 0.0f;

            items.add(item);

        }

        quotationDetail.setItems(items);
        quotationDetail.setTotalPriceOfItems(totalPriceOfItems);
    }

    private void saveQuotationDetailItem(ArrayList<QuotationItems> items, TeqipPmssSupplierMaster supplierMaster, TeqipPackage teqipPackage) {
        if (items == null) {
            return;
        }
        for (QuotationItems item : items) {

            TeqipPackageQuotationsEvaluationItemDetail evaluationDetail = item.getQuotationDetailId() != null
                    ? quotationsEvaluationDetailRepository.findOne(item.getQuotationDetailId())
                    : new TeqipPackageQuotationsEvaluationItemDetail();
            evaluationDetail.setTeqipPackage(teqipPackage);
            evaluationDetail.setTeqipPmssSupplierMaster(supplierMaster);
            evaluationDetail.setTeqipItemMaster(itemRepository.findOne(item.getItemMasterId()));
            evaluationDetail.setItemQty(item.getItemQty());
            evaluationDetail.setBasicCost(item.getBasicCost());
            evaluationDetail.setTotalPrice(item.getTotalPrice());
            evaluationDetail.setComment(item.getComments());
            evaluationDetail.setDescription(item.getDescription());
            quotationsEvaluationDetailRepository.saveAndFlush(evaluationDetail);

        }
    }

    private void saveQuotationDetailWork(ArrayList<QuotationWork> works, TeqipPmssSupplierMaster supplierMaster, TeqipPackage teqipPackage) {

        if (works == null) {
            return;
        }

        for (QuotationWork work : works) {

            TeqipPackageQuotationsEvaluationWorkDetail evaluationDetail = work.getQuotationDetailId() != null
                    ? quotationsEvaluationWorkDetailRepository.findOne(work.getQuotationDetailId())
                    : new TeqipPackageQuotationsEvaluationWorkDetail();
            evaluationDetail.setTeqipPackage(teqipPackage);
            evaluationDetail.setTeqipPmssSupplierMaster(supplierMaster);
            evaluationDetail.setWorkCost(work.getWorkCost());
            evaluationDetail.setWorkName(work.getWorkName());
            evaluationDetail.setWorkEstimatedCost(work.getWorkEstimatedCost());
            evaluationDetail.setComment(work.getComments());
            evaluationDetail.setWorkSpecification(work.getWorkSpecification());
            evaluationDetail.setDescription(work.getDescription());
            quotationsEvaluationWorkDetailRepository.saveAndFlush(evaluationDetail);

        }
    }

    private void setQuationDetailWork(List<TeqipPackageQuotationsEvaluationWorkDetail> teqipPackageQuotationsWorkEvaluationDetails, TeqipPmssSupplierMaster teqipPmssSupplierMaster, QuotationDetail quotationDetail) {

        if (teqipPackageQuotationsWorkEvaluationDetails == null) {
            return;
        }

        ArrayList<QuotationWork> works = new ArrayList<>();

        Double totalPriceOfItems = 0.0;

        for (TeqipPackageQuotationsEvaluationWorkDetail evaluationDetail : teqipPackageQuotationsWorkEvaluationDetails) {

            if (!Objects.equals(evaluationDetail.getTeqipPmssSupplierMaster().getSupplierId(), teqipPmssSupplierMaster.getSupplierId())) {
                continue;
            }

            QuotationWork work = new QuotationWork();
            work.setQuotationDetailId(evaluationDetail.getQuotationWorkDetailId());
            work.setWorkCost(evaluationDetail.getWorkCost());
            work.setWorkName(evaluationDetail.getWorkName());
            work.setWorkEstimatedCost(evaluationDetail.getWorkEstimatedCost());
            work.setComments(evaluationDetail.getComment());
            work.setWorkSpecification(evaluationDetail.getWorkSpecification());
            work.setDescription(evaluationDetail.getDescription());
            totalPriceOfItems += evaluationDetail.getWorkEstimatedCost() != null ? evaluationDetail.getWorkEstimatedCost() : 0.0f;

            works.add(work);

        }

        quotationDetail.setWorks(works);
        quotationDetail.setTotalPriceOfItems(totalPriceOfItems);
    }

    private void saveTechEvaluationDetail(TechEvaluationDetail techEvaluationDetail, TeqipPmssSupplierMaster supplierMaster, TeqipPackage teqipPackage) {
        if (techEvaluationDetail == null) {
            return;
        }

        ArrayList<TechnicalCriterias> technicalCriterias = techEvaluationDetail.getTechnicalCriterias();
        if (technicalCriterias == null) {
            return;
        }

        for (TechnicalCriterias technicalCriteria : technicalCriterias) {
            ArrayList<SubCriteria> subCriteriaList = technicalCriteria.getSubCriteria();
            if (subCriteriaList == null) {
                continue;
            }

            for (SubCriteria subCriteria : subCriteriaList) {

                TeqipPackageConsultantEvaluation entity = subCriteria.getConsultantEvaluationId() != null
                        ? consultantEvaluationRepository.findOne(subCriteria.getConsultantEvaluationId())
                        : new TeqipPackageConsultantEvaluation();
                entity.setTeqipPackage(teqipPackage);
                entity.setTeqipPmssSupplierMaster(supplierMaster);
                entity.setTeqipPackageTechSubCriteria(techSubCriteriaRepository.findOne(subCriteria.getId()));

                entity.setMarks(subCriteria.getConsultantMarks());

                consultantEvaluationRepository.saveAndFlush(entity);

            }

        }
    }

    private TechEvaluationDetail setTechEvaluationDetail(List<TeqipPackageConsultantEvaluation> teqipPackageConsultantEvaluations,
            TeqipPmssSupplierMaster teqipPmssSupplierMaster, TeqipPackage teqipPackage) {

        if (teqipPackageConsultantEvaluations == null) {
            return null;
        }

        TechEvaluationDetail techEvaluationDetail = new TechEvaluationDetail();

        Map<Integer, TechnicalCriterias> map = new HashMap<>();

        for (TeqipPackageConsultantEvaluation entity : teqipPackageConsultantEvaluations) {

            if (entity.getTeqipPmssSupplierMaster() != null
                    && Objects.equals(entity.getTeqipPmssSupplierMaster().getSupplierId(), teqipPmssSupplierMaster.getSupplierId())) {

                TeqipPackageTechSubCriteria entitySubCriteria = entity.getTeqipPackageTechSubCriteria();

                TeqipPackageTechCriteria entityCriteria = entitySubCriteria.getTeqipPackageTechCriteria();

                TechnicalCriterias criterias = map.get(entityCriteria.getTechCriteriaId());
                criterias = criterias != null ? criterias : new TechnicalCriterias();
                criterias.setId(entityCriteria.getTechCriteriaId());
                criterias.setDescription(entityCriteria.getTechCriteriaDesc());
                criterias.setMarks(entityCriteria.getMarks());

                ArrayList<SubCriteria> subCriteriaList = criterias.getSubCriteria();
                subCriteriaList = subCriteriaList != null ? subCriteriaList : new ArrayList<>();

                SubCriteria subCritera = new SubCriteria();
                subCritera.setConsultantEvaluationId(entity.getConsultantEvaluationId());
                subCritera.setId(entitySubCriteria.getTechsubcriteriaId());
                subCritera.setSubCriteriaQuestionText(entitySubCriteria.getTechCriteriaDesc());
                subCritera.setMarks(entitySubCriteria.getMarks());

                subCritera.setConsultantMarks(entity.getMarks() != null ? entity.getMarks() : 0.0f);

                subCriteriaList.add(subCritera);

                Double consultantMarks = criterias.getConsultantMarks() != null ? criterias.getConsultantMarks() : 0.0f;
                criterias.setConsultantMarks(consultantMarks + subCritera.getConsultantMarks());
                criterias.setSubCriteria(subCriteriaList);

                map.put(criterias.getId(), criterias);

            }
        }
        techEvaluationDetail.setTechnicalCriterias(new ArrayList<>(map.values()));
        return techEvaluationDetail;
    }

}
