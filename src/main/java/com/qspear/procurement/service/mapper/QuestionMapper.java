/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.Questions;
import com.qspear.procurement.model.methods.QuestionsDetails;
import com.qspear.procurement.model.methods.QuotationDetail;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackageQuestionMaster;
import com.qspear.procurement.persistence.methods.TeqipPackageQuotationsSupplierInput;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.method.PackageQuestionRepository;
import com.qspear.procurement.repositories.method.PackageQuotationsSupplierInputRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service

public class QuestionMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackageQuestionRepository packageQuestionRepository;

    @Autowired
    PackageQuotationsSupplierInputRepository packageQuotationsSupplierInputRepository;

    public TeqipPackageQuestionMaster saveQuestion(Questions question, String category, TeqipPackage teqipPackage) {

        TeqipPackageQuestionMaster questionDetails = question.getPackageQuestionId() != null
                ? packageQuestionRepository.findOne(question.getPackageQuestionId())
                : new TeqipPackageQuestionMaster();

        questionDetails.setTeqipPackage(teqipPackage);

        questionDetails.setQuestionCategory(category);
        questionDetails.setDecision(question.getDecision());
        questionDetails.setQuestionDesc(question.getQuestion());
        questionDetails.setIsStandardQuestion(question.getIsStandardQuestion());
        questionDetails.setType(question.getType());
        questionDetails.setQuestionNonResponsiveValue(question.getQuestionNonResponsiveValue());
        questionDetails.setIsevalatedPrice(question.getIsEvaluatedPrice());
        questionDetails.setIsreadOut(question.getIsReadOutPrice());
        questionDetails.setOptions(question.getOptions());
        questionDetails.setMinValue(question.getMinValue());
        questionDetails.setMaxValue(question.getMaxValue());

        packageQuestionRepository.saveAndFlush(questionDetails);
        return questionDetails;
    }

    public Questions setQuestion(TeqipPackageQuestionMaster entity) {

        Questions question = new Questions();
        question.setPackageQuestionId(entity.getQuestionId());
        question.setQuestion(entity.getQuestionDesc());
        question.setDecision(entity.getDecision());
        question.setIsStandardQuestion(entity.getIsStandardQuestion());
        question.setType(entity.getType());
        question.setQuestionCategory(entity.getQuestionCategory());
        question.setQuestionNonResponsiveValue(entity.getQuestionNonResponsiveValue());
        question.setIsEvaluatedPrice(entity.getIsevalatedPrice());
        question.setIsReadOutPrice(entity.getIsreadOut());
        question.setOptions(entity.getOptions());
        question.setMinValue(entity.getMinValue());
        question.setMaxValue(entity.getMaxValue());

        return question;

    }

    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {

        List<TeqipPackageQuestionMaster> teqipPackageQuestionDetails = teqipPackage.getTeqipPackageQuestionMasters();
        if (teqipPackageQuestionDetails == null) {
            return;
        }

        Map<String, QuestionsDetails> map = new HashMap<>();
        for (TeqipPackageQuestionMaster entity : teqipPackageQuestionDetails) {

            String questionCategory = entity.getQuestionCategory();
            QuestionsDetails questionsDetail = map.containsKey(questionCategory) ? map.get(questionCategory) : new QuestionsDetails();
            questionsDetail.setQuestionCategory(questionCategory);

            ArrayList<Questions> questions = questionsDetail.getQuestions() != null ? questionsDetail.getQuestions() : new ArrayList<>();

            questions.add(this.setQuestion(entity));

            questionsDetail.setQuestions(questions);

            map.put(questionCategory, questionsDetail);

        }

        response.setQuestionsDetails(new ArrayList<>(map.values()));

    }

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {
        ArrayList<QuestionsDetails> questionsDetails = methodPackageUpdateRequest.getQuestionsDetails();
        if (questionsDetails == null) {
            return;
        }

        for (QuestionsDetails entity : questionsDetails) {

            if (entity.getQuestions() == null) {
                continue;
            }
            for (Questions question : entity.getQuestions()) {
                this.saveQuestion(question, entity.getQuestionCategory(), teqipPackage);
            }

        }
    }

    public ArrayList<Questions> setQuestionInput(
            List<TeqipPackageQuotationsSupplierInput> teqipPackageQuotationsEvaluations,
            TeqipPmssSupplierMaster teqipPmssSupplierMaster,
            QuotationDetail quotationDetail, String questCategory) {

        if (teqipPackageQuotationsEvaluations == null) {
            return null;
        }
        ArrayList<Questions> questions = new ArrayList<>();

        for (TeqipPackageQuotationsSupplierInput entity : teqipPackageQuotationsEvaluations) {

            TeqipPackageQuestionMaster teqipPackageQuestionMaster = entity.getTeqipPackageQuestionMaster();

            if (teqipPackageQuestionMaster != null
                    && teqipPackageQuestionMaster.getQuestionCategory().equals(questCategory)
                    && entity.getTeqipPmssSupplierMaster() != null
                    && Objects.equals(entity.getTeqipPmssSupplierMaster().getSupplierId(), teqipPmssSupplierMaster.getSupplierId())) {

                Questions question = this.setQuestion(entity.getTeqipPackageQuestionMaster());
                question.setPackageQuotationEvaluationId(entity.getPackageQuotationSupplierInputId());
                question.setUserResponse(entity.getUserResponse());
                question.setReasonForNonResponsiveNess(entity.getReasonForNonResponsiveNess());

                if (quotationDetail != null && question.getIsReadOutPrice() != null && question.getIsReadOutPrice() == 1) {
                    Double packageReadOutPrice = quotationDetail.getPackageReadOutPrice();
                    packageReadOutPrice += question.getUserResponse() != null ? Double.valueOf(question.getUserResponse()) : 0.0f;
                    quotationDetail.setPackageReadOutPrice(packageReadOutPrice);
                }

                questions.add(question);
            }
        }
        return questions;
    }

    public void saveQuestionInputs(ArrayList<Questions> questions,
            TeqipPmssSupplierMaster supplierMaster,
            TeqipPackage entity, String questCategory) {

        if (questions == null) {
            return;
        }
        for (Questions question : questions) {

            TeqipPackageQuotationsSupplierInput evaluation = question.getPackageQuotationEvaluationId() != null
                    ? packageQuotationsSupplierInputRepository.findOne(question.getPackageQuotationEvaluationId())
                    : new TeqipPackageQuotationsSupplierInput();

            evaluation.setTeqipPackage(entity);
            evaluation.setTeqipPmssSupplierMaster(supplierMaster);

            if (question.getPackageQuestionId() == null) {
                TeqipPackageQuestionMaster master = packageQuestionRepository.findByTeqipPackageAndQuestionDescAndQuestionCategory(entity, question, questCategory);
                if (master == null) {
                    evaluation.setTeqipPackageQuestionMaster(this.saveQuestion(question, questCategory, entity));
                } else {
                    evaluation.setTeqipPackageQuestionMaster(master);

                }
            } else {
                evaluation.setTeqipPackageQuestionMaster(packageQuestionRepository.findOne(question.getPackageQuestionId()));
            }

            evaluation.setUserResponse(question.getUserResponse());
            evaluation.setReasonForNonResponsiveNess(question.getReasonForNonResponsiveNess());

            packageQuotationsSupplierInputRepository.saveAndFlush(evaluation);

        }

    }
}
