/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.methods.ContractManagementCriteria;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.PaymentDetails;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPackagePaymentDetail;
import com.qspear.procurement.persistence.methods.TeqipPackagePurchaseOrder;
import com.qspear.procurement.persistence.methods.TeqipPackageWorkOrder;
import com.qspear.procurement.repositories.method.PackagePaymentRepository;
import com.qspear.procurement.repositories.method.PackagePurchaseOrderRepository;
import com.qspear.procurement.repositories.method.WorkOrderRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class PaymentMapper implements MapperInterface<TeqipPackage> {

    @Autowired
    PackagePaymentRepository packagePaymentRepository;

    @Autowired
    PackagePurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    WorkOrderRepository workOrderRepository;


    @Override
    public void mapEntityToObject(TeqipPackage teqipPackage, MethodPackageResponse response) {

        List<TeqipPackagePaymentDetail> teqipPackagePaymentDetails = teqipPackage.getTeqipPackagePaymentDetails();
        if (teqipPackagePaymentDetails == null) {
            return;
        }

        TeqipPackagePurchaseOrder teqipPackagePurchaseOrder = purchaseOrderRepository.findByTeqipPackage(teqipPackage);
        TeqipPackageWorkOrder teqipPackageWorkOrder = workOrderRepository.findByTeqipPackage(teqipPackage);

        ArrayList<PaymentDetails> paymentDetails = new ArrayList<>();

        for (TeqipPackagePaymentDetail entity : teqipPackagePaymentDetails) {

            PaymentDetails details = new PaymentDetails();
            details.setPackagePaymentId(entity.getPackagePaymentId());
            details.setPaymentDescription(entity.getPaymentDescription());
            details.setMilestonePercentage(entity.getMilestonePercentage());
            details.setExpectedDeliveryDate(entity.getExpectedDeliveryDate());
            details.setExpectedDeliveryPeriod(entity.getExpectedDeliveryPeriod());
            details.setExpectedPaymentDate(entity.getExpectedPaymentdate());
            details.setExpectedPaymentAmount(entity.getExpectedPaymentAmount());
            if (teqipPackagePurchaseOrder != null) {
                details.setPoNumber(teqipPackagePurchaseOrder.getPurchaseOrderNo());
            }
            details.setIsWCCGenerated(entity.getIsWCCGenerated());
            details.setMilestoneActualCompletionPeriod(entity.getMilestoneActualCompletionPeriod());
            details.setPaymentDate(entity.getPaymentDate());
            details.setChequeDraftNumber(entity.getChequeDraftNumber());
            details.setIsWorkCompleted(entity.getIsWorkCompleted());
            details.setActualCompletionDate(entity.getActualCompletionDate());
            details.setActualPaymentAmount(entity.getActualPaymentAmount());
            details.setTotalPaymentTillNow(entity.getTotalPaymentTillNow());
            details.setComments(entity.getComments());
            details.setUploadCompletionCertificate(entity.getUploadCompletionCertificate());

            ContractManagementCriteria criteria = new ContractManagementCriteria();

            criteria.setIsWccGenerated(entity.getIsWCCGenerated());
            criteria.setIsWorkCompleted(entity.getIsWorkCompleted());
            criteria.setCheckDraftNumber(entity.getChequeDraftNumber());
            criteria.setActualCompletionDate(entity.getActualCompletionDate());
            criteria.setIsLiquidityDamagesWaived(entity.getIsliquidateDamageWaived());
            criteria.setLiquidatedDamages(entity.getLiquidatedDamages());
            criteria.setExpectedPaymentAmountWithLD(entity.getExpectedPaymentAmountLD());
            criteria.setTotalPaymentTillDate(entity.getTotalPaymentTillNow());
            criteria.setActualCompletionPeriod(entity.getActualCompletionPeriod());
            criteria.setActualCompletionPeriodComment(entity.getComment1());
            criteria.setActualPaymentDate(entity.getActualPaymentDate());
            criteria.setActualPaymentDateComment(entity.getComment2());
            criteria.setActualPaymentAmount(entity.getActualPaymentAmount());
            criteria.setActualPaymentAmountComment(entity.getComment3());
            criteria.setRetentionMoney(entity.getRetentionMoney());
            criteria.setComments(entity.getContractManagmentComment());
            criteria.setDeductionForMobilization(entity.getDeductionForMobilization());
            criteria.setIsQualityCheckCarriedOut(entity.getIsQualityCheckCarriedOut());
            criteria.setPaymentMode(entity.getPaymentMode());
            criteria.setPaymentModeDate(entity.getPaymentModeDate());
            criteria.setBankName(entity.getBankName());
            criteria.setBankDetails(entity.getBankDetails());
            if (teqipPackageWorkOrder != null) {
                criteria.setWorkOrderNumber(teqipPackageWorkOrder.getWorkOrderNo());
            }
            if (teqipPackagePurchaseOrder != null) {
                criteria.setPoNumber(teqipPackagePurchaseOrder.getPurchaseOrderNo());
            }

            details.setContractManagementCriteria(criteria);
            paymentDetails.add(details);

        }
        response.setPaymentDetails(paymentDetails);
    }

    @Override
    public void mapObjectToEntity(TeqipPackage teqipPackage, MethodPackageUpdateRequest methodPackageUpdateRequest) {

        ArrayList<PaymentDetails> paymentDetails = methodPackageUpdateRequest.getPaymentDetails();

        if (paymentDetails == null) {
            return;
        }

        for (PaymentDetails details : paymentDetails) {

            TeqipPackagePaymentDetail entity = details.getPackagePaymentId() != null
                    ? packagePaymentRepository.findOne(details.getPackagePaymentId())
                    : new TeqipPackagePaymentDetail();

            entity.setPackagePaymentId(entity.getPackagePaymentId());
            entity.setTeqipPackage(teqipPackage);
            entity.setPaymentDescription(details.getPaymentDescription());
            entity.setMilestonePercentage(details.getMilestonePercentage());
            entity.setExpectedDeliveryDate(details.getExpectedDeliveryDate());
            entity.setExpectedDeliveryPeriod(details.getExpectedDeliveryPeriod());
            entity.setExpectedPaymentdate(details.getExpectedPaymentDate());
            entity.setExpectedPaymentAmount(details.getExpectedPaymentAmount());
            entity.setIsWCCGenerated(details.getIsWCCGenerated());
            entity.setMilestoneActualCompletionPeriod(details.getMilestoneActualCompletionPeriod());
            entity.setPaymentDate(details.getPaymentDate());
            entity.setChequeDraftNumber(details.getChequeDraftNumber());
            entity.setIsWorkCompleted(details.getIsWorkCompleted());
            entity.setActualCompletionDate(details.getActualCompletionDate());
            entity.setActualPaymentAmount(details.getActualPaymentAmount());
            entity.setTotalPaymentTillNow(details.getTotalPaymentTillNow());
            entity.setComments(details.getComments());
            entity.setUploadCompletionCertificate(details.getUploadCompletionCertificate());

            ContractManagementCriteria criteria = details.getContractManagementCriteria();
            if (criteria != null) {

                entity.setIsWCCGenerated(criteria.getIsWccGenerated());
                entity.setIsWorkCompleted(criteria.getIsWorkCompleted());
                entity.setChequeDraftNumber(criteria.getCheckDraftNumber());
                entity.setActualCompletionDate(criteria.getActualCompletionDate());
                entity.setIsliquidateDamageWaived(criteria.getIsLiquidityDamagesWaived());
                entity.setLiquidatedDamages(criteria.getLiquidatedDamages());
                entity.setExpectedPaymentAmountLD(criteria.getExpectedPaymentAmountWithLD());
                entity.setTotalPaymentTillNow(criteria.getTotalPaymentTillDate());
                entity.setActualCompletionPeriod(criteria.getActualCompletionPeriod());
                entity.setComment1(criteria.getActualCompletionPeriodComment());
                entity.setActualPaymentDate(criteria.getActualPaymentDate());
                entity.setComment2(criteria.getActualPaymentDateComment());
                entity.setActualPaymentAmount(criteria.getActualPaymentAmount());
                entity.setComment3(criteria.getActualPaymentAmountComment());
                entity.setRetentionMoney(criteria.getRetentionMoney());
                entity.setContractManagmentComment(criteria.getComments());
                entity.setDeductionForMobilization(criteria.getDeductionForMobilization());
                entity.setIsQualityCheckCarriedOut(criteria.getIsQualityCheckCarriedOut());
                entity.setPaymentMode(criteria.getPaymentMode());
                entity.setPaymentModeDate(criteria.getPaymentModeDate()); 
                entity.setBankName(criteria.getBankName());
                entity.setBankDetails(criteria.getBankDetails());
            }

            packagePaymentRepository.saveAndFlush(entity);

        }
    }
}
