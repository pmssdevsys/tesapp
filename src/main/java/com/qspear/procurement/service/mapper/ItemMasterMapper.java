/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qspear.procurement.service.mapper;

import com.qspear.procurement.model.TeqipItemMasterModel;
import com.qspear.procurement.persistence.TeqipItemMaster;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaspreet
 */
@Service
public class ItemMasterMapper {
    
    
    
        public List<TeqipItemMasterModel> mapEntityToObject(List<TeqipItemMaster> itemMasterList) {
        if (itemMasterList == null) {
            return null;
        }

        List<TeqipItemMasterModel> list = new ArrayList<>();

        for (TeqipItemMaster entity : itemMasterList) {
            TeqipItemMasterModel model = new TeqipItemMasterModel();

            model.setItemId(entity.getItemId());
            model.setItemName(entity.getItemName());
            model.setDescription(entity.getDescription());
            model.setStatus(entity.getStatus());
            model.setTeqipCategorymasterId(entity.getTeqipCategorymaster().getCatId());
            model.setTeqipSubcategorymasterId(entity.getTeqipSubcategorymaster().getSubcatId());

            list.add(model);
        }
        return list;

    }
    
}
