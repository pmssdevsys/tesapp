package com.qspear.procurement.service;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qspear.procurement.constants.ProcurementConstants;
import com.qspear.procurement.model.CityMaster;
import com.qspear.procurement.model.ItemSpecificationModel;
import com.qspear.procurement.model.PlanInfo;
import com.qspear.procurement.model.PlanSatus;
import com.qspear.procurement.model.PrecurementMethodTimeLinePojo;
import com.qspear.procurement.model.StateMaster;
import com.qspear.procurement.model.StatusSummary;
import com.qspear.procurement.model.StatusTimeLine;
import com.qspear.procurement.model.TeqipActivitiymasterModel;
import com.qspear.procurement.model.TeqipCategorymasterModel;
import com.qspear.procurement.model.TeqipDepartmentModel;
import com.qspear.procurement.model.TeqipItemMasterModel;
import com.qspear.procurement.model.TeqipProcurementmasterModel;
import com.qspear.procurement.model.TeqipProcurementmethodModel;
import com.qspear.procurement.model.TeqipSubcategorymasterModel;
import com.qspear.procurement.model.TeqipSubcomponentModel;
import com.qspear.procurement.model.TeqipTimelineModel;
import com.qspear.procurement.model.methods.SupplierDetails;
import com.qspear.procurement.persistence.TeqipCategorymaster;
import com.qspear.procurement.persistence.TeqipCitymaster;
import com.qspear.procurement.persistence.TeqipInstitution;
import com.qspear.procurement.persistence.TeqipInstitutiondepartmentmapping;
import com.qspear.procurement.persistence.TeqipItemMaster;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.TeqipPlanApprovalstatus;
import com.qspear.procurement.persistence.TeqipPmssCurrencymaster;
import com.qspear.procurement.persistence.TeqipPmssProcurementstage;
import com.qspear.procurement.persistence.TeqipPmssThreshholdmaster;
import com.qspear.procurement.persistence.TeqipProcurementmaster;
import com.qspear.procurement.persistence.TeqipProcurementmethod;
import com.qspear.procurement.persistence.TeqipProcurementmethodTimeline;
import com.qspear.procurement.persistence.TeqipStatemaster;
import com.qspear.procurement.persistence.TeqipSubcategorymaster;
import com.qspear.procurement.persistence.TeqipSubcomponentmaster;
import com.qspear.procurement.persistence.Teqip_procurement_revisedtimeline;
import com.qspear.procurement.persistence.Teqip_procurementmethod_Usertimeline;
import com.qspear.procurement.persistence.User;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.repositories.CityMasterRepository;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.Teqip_procurementmethod_UsertimelineRepo;
import com.qspear.procurement.repositories.Teqip_procurementmethod_revisedtimelineRepository;
import com.qspear.procurement.service.mapper.ItemGoodsMapper;
import com.qspear.procurement.service.mapper.ItemMasterMapper;
import com.qspear.procurement.service.mapper.PackageSupplierMappper;
import com.qspear.procurement.service.mapper.ProcurementMapper;
import java.util.LinkedHashMap;
import java.util.Map;
import com.qspear.procurement.repositories.StateMasterRepository;
import java.time.LocalDate;
import java.time.Month;
import org.springframework.util.StringUtils;

@Service
@Transactional
public class PMSSReferenceService extends AbstractPackageService {

    @Autowired
    ProcurementMapper procurementMapper;

    @Autowired
    PackageSupplierMappper packageSupplierMappper;

    @Autowired
    ItemGoodsMapper itemGoodsMapper;

    @Autowired
    ItemMasterMapper itemMasterMapper;

    @Autowired
    Teqip_procurementmethod_UsertimelineRepo Teqip_procurementmethod_UsertimelineRepo;

    @Autowired
    Teqip_procurementmethod_revisedtimelineRepository teqip_procurement_revisedtimelineRepo;

    @Autowired
    TeqipPackageRepository packageRepository;

    @Autowired
    StateMasterRepository stateMasterRepository;

    @Autowired
    CityMasterRepository cityMasterRepository;

    @Autowired
    UserControllerService userControllerService;

    public List<TeqipCategorymasterModel> retrieveCategory(HttpServletRequest httpServletRequest, String categoryName) {

        Iterable<Object[]> teqipCategorymasterList = categoryRepository.findByIsactive(Boolean.TRUE);

        List<TeqipCategorymasterModel> listOfFinalCategories = new ArrayList<>();
        for (Object[] teqipCategorymaster : teqipCategorymasterList) {
            String categoryName1 = (String) teqipCategorymaster[0];
            Integer categoryId = (Integer) teqipCategorymaster[1];
            TeqipCategorymasterModel teqipCategorymasterModel = new TeqipCategorymasterModel();
            teqipCategorymasterModel.setCategoryName(categoryName1);
            teqipCategorymasterModel.setCatId(categoryId);

            if (categoryName.trim().equals(categoryName1) && categoryName.equals("Services")) {
                listOfFinalCategories.add(teqipCategorymasterModel);
                break;
            } else if (!categoryName1.equals("Services")) {
                listOfFinalCategories.add(teqipCategorymasterModel);
            }
        }

        return listOfFinalCategories;
    }

    public List<TeqipSubcategorymasterModel> retrieveSubCategory(String categoryId, HttpServletRequest httpServletRequest) {
        TeqipCategorymaster teqipCategorymaster = new TeqipCategorymaster();
        teqipCategorymaster.setCatId(Integer.parseInt(categoryId));

        List<Object[]> teqipSubcategorymaster = teqipSubcategorymasterRepository.findByTeqipCategorymaster(teqipCategorymaster);

        List<TeqipSubcategorymasterModel> listOfTeqipSubcategorymasterModel = new ArrayList<>();

        for (Object[] temp : teqipSubcategorymaster) {
            TeqipSubcategorymasterModel teqipSubcategorymasterModel = new TeqipSubcategorymasterModel();
            teqipSubcategorymasterModel.setSubCategoryName((String) temp[1]);
            teqipSubcategorymasterModel.setSubcatId((Integer) temp[0]);
            listOfTeqipSubcategorymasterModel.add(teqipSubcategorymasterModel);
        }

        return listOfTeqipSubcategorymasterModel;
    }

    public List<TeqipActivitiymasterModel> retrieveActivity(String categoryId, HttpServletRequest httpServletRequest) {

        TeqipCategorymaster teqipCategorymaster = new TeqipCategorymaster();
        teqipCategorymaster.setCatId(Integer.parseInt(categoryId));

        List<Object[]> teqipSubcategorymaster = activityRepository
                .findByTeqipCategorymaster(/*teqipCategorymaster*/);

        List<TeqipActivitiymasterModel> listOfTeqipActivitiymasterModel = new ArrayList<>();

        for (Object[] temp : teqipSubcategorymaster) {
            TeqipActivitiymasterModel activitiymasterModel = new TeqipActivitiymasterModel();
            activitiymasterModel.setActivityId((Integer) temp[0]);
            activitiymasterModel.setActivityName((String) temp[1]);

            listOfTeqipActivitiymasterModel.add(activitiymasterModel);
        }

        return listOfTeqipActivitiymasterModel;
    }

    public List<TeqipPmssThreshholdmaster> retrieveThreshholdmasterList(
            Integer categoryId, Boolean isproprietary, Boolean isgem, Double thresholdValue) {
        TeqipPmssCurrencymaster teqipPmssCurrencymaster = teqipPmssCurrencymasterRepository.findByCurrencyname(ProcurementConstants.USD_CURRENCY_CODE);

        Double thresholdValueAfterConversion = thresholdValue / teqipPmssCurrencymaster.getCurrencyvalue();

        TeqipCategorymaster findOne = categoryRepository.findOne(categoryId);
        List<TeqipPmssThreshholdmaster> teqipPmssThreshholdmasterList;
        if (isgem == null) {
            teqipPmssThreshholdmasterList = teqipPmssThreshholdmasterRepository
                    .findByIsactiveAndIsproprietaryAndTeqipCategorymaster(Boolean.TRUE,
                            isproprietary ? new Integer("1") : new Integer("0"),
                            findOne);
        } else {
            teqipPmssThreshholdmasterList = teqipPmssThreshholdmasterRepository
                    .findByIsactiveAndIsproprietaryAndIsgemAndTeqipCategorymaster(Boolean.TRUE,
                            isproprietary ? new Integer("1") : new Integer("0"),
                            isgem ? new Integer("1") : new Integer("0"),
                            findOne);
        }

        List<TeqipPmssThreshholdmaster> filteredteqipPmssThreshholdmasterList = new ArrayList<>();

        for (TeqipPmssThreshholdmaster teqipPmssThreshholdmaster : teqipPmssThreshholdmasterList) {
            if (thresholdValueAfterConversion >= teqipPmssThreshholdmaster.getThresholdMinValue()
                    && thresholdValueAfterConversion <= teqipPmssThreshholdmaster.getThresholdMaxValue()) {
                filteredteqipPmssThreshholdmasterList.add(teqipPmssThreshholdmaster);
            }
        }
        return filteredteqipPmssThreshholdmasterList;
    }

    public List<TeqipProcurementmethodModel> retrieveProcurementMethods(HttpServletRequest httpServletRequest,
            Integer categoryId, Boolean isproprietary, Boolean isgem, Double thresholdValue) {

        List<TeqipPmssThreshholdmaster> filteredteqipPmssThreshholdmasterList
                = retrieveThreshholdmasterList(categoryId, isproprietary, isgem, thresholdValue);

        List<TeqipProcurementmethod> teqipProcurementmethodList = new ArrayList<>();
        filteredteqipPmssThreshholdmasterList.stream().forEach(tqipMethod -> {
            teqipProcurementmethodList.add(tqipMethod.getTeqipProcurementmethod());
        });

        List<TeqipProcurementmethodModel> mapTeqipProcurementmethodModel = procurementMapper.mapTeqipProcurementmethodModel(teqipProcurementmethodList);
        Collections.sort(mapTeqipProcurementmethodModel);
        return mapTeqipProcurementmethodModel;
    }

    public List<TeqipItemMasterModel> retrieveItems(String category, String subCategory) {
        TeqipSubcategorymaster teqipSubcategoryMaster = new TeqipSubcategorymaster();
        teqipSubcategoryMaster.setSubcatId(Integer.parseInt(subCategory));

        TeqipCategorymaster teqipCategoryMaster = new TeqipCategorymaster();
        teqipCategoryMaster.setCatId(Integer.parseInt(category));

        List<TeqipItemMaster> itemMasterList = itemRepository
                .findDistinctItemNameByTeqipCategorymasterAndTeqipSubcategorymaster(teqipCategoryMaster, teqipSubcategoryMaster);

        return itemMasterMapper.mapEntityToObject(itemMasterList);
    }

    public List<TeqipProcurementmasterModel> retrievePlans(HttpServletRequest httpServletRequest) {

        List<TeqipProcurementmaster> teqipProcurementmasterList = procurementPlanRepository
                .findByIsactive(Boolean.TRUE);

        List<TeqipProcurementmasterModel> listOfTeqipProcurementmasterModel = new ArrayList<>();

        for (TeqipProcurementmaster procurementmaster : teqipProcurementmasterList) {
            TeqipProcurementmasterModel teqipProcurementmasterModel = new TeqipProcurementmasterModel();
            teqipProcurementmasterModel.setLastDateSubmission(getLastDayOfQuarter());
            teqipProcurementmasterModel.setEndDate(procurementmaster.getEndDate());
            teqipProcurementmasterModel.setStartDate(procurementmaster.getStartDate());
            teqipProcurementmasterModel.setPlanAmount(procurementmaster.getPlanAmount());
            teqipProcurementmasterModel.setPlanName(procurementmaster.getPlanName());
            teqipProcurementmasterModel.setProcurementmasterId(procurementmaster.getProcurementmasterId());
            teqipProcurementmasterModel.setShortName(procurementmaster.getShortName());
            procurementUtility.setSubcomponentDetails(teqipProcurementmasterModel,
                    teqipInstitutionsubcomponentmappingRepository, httpServletRequest);

            listOfTeqipProcurementmasterModel.add(teqipProcurementmasterModel);

            break;
        }

        return listOfTeqipProcurementmasterModel;
    }

    private static Date getLastDayOfQuarter() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        Integer B1 = cal.get(Calendar.MONTH);
        Integer B2 = B1 + 1;
        Integer MOD = B2 % 3;
        Integer Diff = MOD != 0 ? (3 - MOD) : 0;
        Integer nextMonth = (B2 + (Diff - 1));

        cal.set(Calendar.MONTH, nextMonth);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public List<TeqipSubcomponentModel> getSubComponentMaster(Integer categoryId, Integer subCategoryId,
            HttpServletRequest httpServletRequest) {
        TeqipCategorymaster teqipCategorymaster = new TeqipCategorymaster();
        teqipCategorymaster.setCatId(categoryId);
        TeqipSubcategorymaster teqipSubcategorymaster = new TeqipSubcategorymaster();
        teqipSubcategorymaster.setSubcatId(subCategoryId);

        List<TeqipSubcomponentmaster> listOfTeqipSubcomponentmaster = teqipSubcomponentmasterRepository
                .findByTeqipCategorymasterAndIsactive(teqipCategorymaster, Boolean.TRUE);
        return procurementMapper.mapListOfTeqipSubcomponentModel(listOfTeqipSubcomponentmaster);
    }

    public List<TeqipDepartmentModel> getDepartments(String type,
            Integer id, HttpServletRequest httpServletRequest) {

        TeqipInstitution teqipInstitution = null;
        TeqipStatemaster teqipStatemaster = null;
        Integer packageType = 0;

        if ("instituteId".equalsIgnoreCase(type)) {
            teqipInstitution = teqipInstitutionRepository.findOne(id);
        
        }

        if ("state".equalsIgnoreCase(type)) {
            teqipStatemaster = teqipStatemasteryRepository.findOne(id);
        }

        if ("edcil".equalsIgnoreCase(type)) {
            packageType = 1;
        }

        List<TeqipInstitutiondepartmentmapping> listOfDepartment = teqipInstitutiondepartmentmappingRepository
                .findByTeqipInstitutionAndTeqipStatemasterAndType(teqipInstitution,teqipStatemaster, packageType);
        
        return procurementMapper.mapDepartments(listOfDepartment);
    }

    public List<SupplierDetails> retrieveSupplier(int type, String state, String city) {
        List<TeqipPmssSupplierMaster> listSupplier = null;

        if (state != null && !"".equals(state.trim()) && city != null && !"".equals(city.trim())) {

            listSupplier = supplierRepository.findByPmssTypeAndSupplierStateAndSupplierCity(type, state, city);
        } else if (state != null && !"".equals(state.trim())) {

            listSupplier = supplierRepository.findByPmssTypeAndSupplierState(type, state);

        } else if (city != null && !"".equals(city.trim())) {

            listSupplier = supplierRepository.findByPmssTypeAndSupplierCity(type, city);

        } else {

            listSupplier = supplierRepository.findByPmssType(type);
        }

        return packageSupplierMappper.mapSupplierMaster(listSupplier);
    }

    public List<ItemSpecificationModel> retrieveItems(String itemName) {
        return itemRepository.findByItemNameContaining(itemName);
    }

    public Teqip_procurementmethod_Usertimeline getProcUserTimeLine(Integer procurementmethodtimelineId) {
        // TODO Auto-generated method stub
        return Teqip_procurementmethod_UsertimelineRepo.findOne(procurementmethodtimelineId);
    }

    public Teqip_procurementmethod_Usertimeline saveUserTimeLine(Teqip_procurementmethod_Usertimeline UserTimeLine) {
        // TODO Auto-generated method stub
        return Teqip_procurementmethod_UsertimelineRepo.save(UserTimeLine);
    }

    public PrecurementMethodTimeLinePojo findProcureUserTime(Integer packageid) {
        // TODO Auto-generated method stub
        Teqip_procurementmethod_Usertimeline procUserTimeLine = Teqip_procurementmethod_UsertimelineRepo.findPackOne(packageid);

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageid);
        Date iniD = teqipPackage.getInitiateDate();
        TeqipTimelineModel teqipTimeline = null;
        if (iniD != null) {
            TeqipProcurementmethodTimeline teqipPackageTimeline = timelineRepository
                    .findByTeqipProcurementmethodAndTeqipCategorymaster(teqipPackage.getTeqipProcurementmethod(), teqipPackage.getTeqipCategorymaster());

//            LocalDate date = LocalDate.of(iniD.getYear(), iniD.getMonth(), iniD.getDate());
            teqipTimeline = procurementUtility.getTeqipTimeline(teqipPackageTimeline, iniD);
        }

        PrecurementMethodTimeLinePojo Usertimeline = new PrecurementMethodTimeLinePojo(null,
                procUserTimeLine, teqipTimeline, teqipPackage);

        Usertimeline.setProcurementmethodtimeline(procUserTimeLine != null ? procUserTimeLine.getTeqip_procurementmethod_UsertimelineId() : 0);

        return Usertimeline;
    }

    public PrecurementMethodTimeLinePojo findProcurerevisedTime(Integer packageid) {
        // TODO Auto-generated method stub
        Teqip_procurement_revisedtimeline reviseTimeLine = teqip_procurement_revisedtimelineRepo.findPackOne(packageid);
        Teqip_procurementmethod_Usertimeline procUserTimeLine = Teqip_procurementmethod_UsertimelineRepo.findPackOne(packageid);

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageid);
        Date iniD = teqipPackage.getInitiateDate();
        TeqipTimelineModel teqipTimeline = null;
        if (iniD != null) {
            TeqipProcurementmethodTimeline teqipPackageTimeline = timelineRepository
                    .findByTeqipProcurementmethodAndTeqipCategorymaster(teqipPackage.getTeqipProcurementmethod(), teqipPackage.getTeqipCategorymaster());
//            LocalDate date = LocalDate.of(iniD.getYear(), iniD.getMonth(), iniD.getDay());
            teqipTimeline = procurementUtility.getTeqipTimeline(teqipPackageTimeline, iniD);
        }

        PrecurementMethodTimeLinePojo Usertimeline = new PrecurementMethodTimeLinePojo(
                reviseTimeLine, procUserTimeLine, teqipTimeline, teqipPackage);

        return Usertimeline;
    }

    public Map<String, Object> saverevisedPlan(PrecurementMethodTimeLinePojo pojo) {
        // TODO Auto-generated method stub
        Map<String, Object> mapObj = new LinkedHashMap();
        try {
            Teqip_procurement_revisedtimeline Usertimelines = teqip_procurement_revisedtimelineRepo.findrevisedtimeline(pojo.getPackageId());
            if (Usertimelines == null) {
                Teqip_procurement_revisedtimeline Usertimeline = new Teqip_procurement_revisedtimeline();
                Usertimeline.setAdvertisementDate_WithoutBankNOC_days(pojo.getAdvertisementDate_WithoutBankNOC_days());
                Usertimeline.setAdvertisementDatedays(pojo.getAdvertisementDatedays());
                Usertimeline.setBankNOCForBiddingDocumentsdays(pojo.getBankNOCForBiddingDocumentsdays());
                Usertimeline.setBidDocumentationPreprationDatedays(pojo.getBidDocumentationPreprationDatedays());
                Usertimeline.setBidInvitationDatedays(pojo.getBidInvitationDatedays());
                Usertimeline.setBidOpeningDatedays(pojo.getBidOpeningDatedays());
                Usertimeline.setContractAwardDate_WithoutBankNOCdays(pojo.getContractAwardDate_WithoutBankNOCdays());
                Usertimeline.setContractAwardDatedays(pojo.getContractAwardDatedays());
                Usertimeline.setContractCompletionDate_WithoutBankNOC_days(pojo.getContractCompletionDate_WithoutBankNOC_days());
                Usertimeline.setContractCompletionDatedays(pojo.getContractCompletionDatedays());
                Usertimeline.setCreatedBy(pojo.getCreatedBy());
                Usertimeline.setCreatedOn(pojo.getCreatedOn());
                Usertimeline.setEvaluationDate_WithoutBankNOCdays(pojo.getEvaluationDate_WithoutBankNOCdays());
                Usertimeline.setEvaluationDatedays(pojo.getEvaluationDatedays());
                Usertimeline.setFinalDraftToBeForwardedToTheBankDatedays(pojo.getFinalDraftToBeForwardedToTheBankDatedays());
                Usertimeline.setLastDateToReceiveProposalsdays(pojo.getLastDateToReceiveProposalsdays());
                Usertimeline.setModifiedBy(pojo.getModifiedBy());
                Usertimeline.setModifiedOn(pojo.getModifiedOn());
                Usertimeline.setNoObjectionFromBankForEvaluationdays(pojo.getNoObjectionFromBankForEvaluationdays());
                Usertimeline.setNoObjectionFromBankForRFPdays(pojo.getNoObjectionFromBankForRFPdays());
                Usertimeline.setrFPIssuedDate_WithoutBankNOCdays(pojo.getrFPIssuedDate_WithoutBankNOCdays());
                Usertimeline.setrFPIssuedDatedays(pojo.getrFPIssuedDatedays());
                Usertimeline.settORFinalizationDate_WithoutBankNOCdays(pojo.gettORFinalizationDate_WithoutBankNOCdays());
                Usertimeline.settORFinalizationDatedays(pojo.gettORFinalizationDatedays());
                Usertimeline.setLastdateofsubmissionofEOI(pojo.getLastdateofsubmissionofEOI());
                Usertimeline.setShortlistingOfEOI(pojo.getShortlistingOfEOI());
                Usertimeline.setFinancialEvaluationDate(pojo.getFinancialEvaluationDate());
                Usertimeline.setrFPApprovalDate(pojo.getrFPApprovalDate());
                Usertimeline.setProposalInvitationDate(pojo.getProposalInvitationDate());
                Usertimeline.setProposalSubmissionDate(pojo.getProposalSubmissionDate());

                if (pojo.getCategoryId() != null) {
                    TeqipCategorymaster teqipCategorymaster = categoryRepository.findOne(pojo.getCategoryId());;
                    Usertimeline.setTeqipCategorymaster(teqipCategorymaster);
                }
                if (pojo.getProcurementmethodId() != null) {
                    TeqipProcurementmethod procurementmethod = procurementmethodRepository.findOne(pojo.getProcurementmethodId());
                    Usertimeline.setProcurementmethod(procurementmethod);
                }
                if (pojo.getPackageId() != null) {
                    TeqipPackage pack = packageRepository.findOne(pojo.getPackageId());
                    Usertimeline.setTeqipPackage(pack);
                }

                Teqip_procurement_revisedtimeline userstimline = teqip_procurement_revisedtimelineRepo.save(Usertimeline);

                if (userstimline != null) {
                    mapObj.put("RESULT", "SUCESS");
                    mapObj.put("DATA", userstimline);

                } else {
                    mapObj.put("RESULT", "FAIL");
                }
            } else {
                throw new RuntimeException("Package Id cant't be duplicate");
            }
        } catch (Exception e) {
            mapObj.put("TOKEN", "ABC@123");
            mapObj.put("RESULT", "FAIL");
            e.printStackTrace();
        }
        return mapObj;

    }

    public Teqip_procurementmethod_Usertimeline findbyPackageId(Integer packageId) {
        // TODO Auto-generated method stub
        return Teqip_procurementmethod_UsertimelineRepo.findPackOne(packageId);
    }

    @Transactional
    public Map<String, Object> updaterevisedPlan(PrecurementMethodTimeLinePojo pojo) {
        // TODO Auto-generated method stub
        Map<String, Object> mapObj = new LinkedHashMap();
        try {
            Teqip_procurement_revisedtimeline revisetime = teqip_procurement_revisedtimelineRepo.findrevisedtime(pojo.getProcurementmethodrevisedId());
            if (revisetime != null) {
                revisetime.setAdvertisementDate_WithoutBankNOC_days(pojo.getAdvertisementDate_WithoutBankNOC_days());
                revisetime.setAdvertisementDatedays(pojo.getAdvertisementDatedays());
                revisetime.setBankNOCForBiddingDocumentsdays(pojo.getBankNOCForBiddingDocumentsdays());
                revisetime.setBidDocumentationPreprationDatedays(pojo.getBidDocumentationPreprationDatedays());
                revisetime.setBidInvitationDatedays(pojo.getBidInvitationDatedays());
                revisetime.setBidOpeningDatedays(pojo.getBidOpeningDatedays());
                revisetime.setContractAwardDate_WithoutBankNOCdays(pojo.getContractAwardDate_WithoutBankNOCdays());
                revisetime.setContractAwardDatedays(pojo.getContractAwardDatedays());
                revisetime.setContractCompletionDate_WithoutBankNOC_days(pojo.getContractCompletionDate_WithoutBankNOC_days());
                revisetime.setContractCompletionDatedays(pojo.getContractCompletionDatedays());
                revisetime.setCreatedBy(pojo.getCreatedBy());
                revisetime.setCreatedOn(pojo.getCreatedOn());
                revisetime.setEvaluationDate_WithoutBankNOCdays(pojo.getEvaluationDate_WithoutBankNOCdays());
                revisetime.setEvaluationDatedays(pojo.getEvaluationDatedays());
                revisetime.setFinalDraftToBeForwardedToTheBankDatedays(pojo.getFinalDraftToBeForwardedToTheBankDatedays());
                revisetime.setLastDateToReceiveProposalsdays(pojo.getLastDateToReceiveProposalsdays());
                revisetime.setModifiedBy(pojo.getModifiedBy());
                revisetime.setModifiedOn(pojo.getModifiedOn());
                revisetime.setNoObjectionFromBankForEvaluationdays(pojo.getNoObjectionFromBankForEvaluationdays());
                revisetime.setNoObjectionFromBankForRFPdays(pojo.getNoObjectionFromBankForRFPdays());
                revisetime.setrFPIssuedDate_WithoutBankNOCdays(pojo.getrFPIssuedDate_WithoutBankNOCdays());
                revisetime.setrFPIssuedDatedays(pojo.getrFPIssuedDatedays());
                revisetime.settORFinalizationDate_WithoutBankNOCdays(pojo.gettORFinalizationDate_WithoutBankNOCdays());
                revisetime.settORFinalizationDatedays(pojo.gettORFinalizationDatedays());
                revisetime.setLastdateofsubmissionofEOI(pojo.getLastdateofsubmissionofEOI());
                revisetime.setShortlistingOfEOI(pojo.getShortlistingOfEOI());
                revisetime.setFinancialEvaluationDate(pojo.getFinancialEvaluationDate());
                revisetime.setrFPApprovalDate(pojo.getrFPApprovalDate());
                revisetime.setProposalInvitationDate(pojo.getProposalInvitationDate());
                revisetime.setProposalSubmissionDate(pojo.getProposalSubmissionDate());

            }
            Teqip_procurement_revisedtimeline revisetimline = teqip_procurement_revisedtimelineRepo.save(revisetime);

            if (revisetimline != null) {
                mapObj.put("RESULT", "SUCESS");
                mapObj.put("DATA", revisetimline);

            } else {
                mapObj.put("RESULT", "FAIL");
            }
        } catch (Exception e) {
            mapObj.put("TOKEN", "ABC@123");
            mapObj.put("RESULT", "FAIL");
            e.printStackTrace();
        }
        return mapObj;

    }

    public List<StateMaster> findAllState() {

        List<TeqipStatemaster> findAll = stateMasterRepository.findAll();
        if (findAll != null) {
            List<StateMaster> sms = new ArrayList<>();
            for (TeqipStatemaster teqipStatemaster : findAll) {
                sms.add(new StateMaster(teqipStatemaster));
            }
            return sms;
        }
        return null;
    }

    public List<CityMaster> findAllCities(String stateName) {

        TeqipStatemaster statemaster = stateMasterRepository.findFirstByStateName(stateName);
        List<TeqipCitymaster> findAll = cityMasterRepository.findByStatemaster(statemaster);
        if (findAll != null) {
            List<CityMaster> sms = new ArrayList<>();
            for (TeqipCitymaster citymaster : findAll) {
                sms.add(new CityMaster(citymaster));
            }
            return sms;
        }
        return null;
    }

    public PlanSatus retrievePlanStatus(String type, Integer id) {

        TeqipInstitution institute = null;
        TeqipStatemaster state = null;
        int typeId = 0;

        if (ProcurementConstants.isInstitutionalRole(type)) {
            institute = teqipInstitutionRepository.findOne(id);
            if (institute.getTeqipInstitutiontype() != null) {
                typeId = institute.getTeqipInstitutiontype().getInstitutiontypeId();
            }

        } else if (ProcurementConstants.isStateRole(type)) {
            state = teqipStatemasteryRepository.findOne(id);

        } else if (ProcurementConstants.isEdcilRole(type)) {
            typeId = 1;

        }

        PlanSatus planSatus = new PlanSatus();

        List<TeqipPlanApprovalstatus> approvalstatusList = teqipPlanApprovalStatusRepository.findAllByTeqipInstitutionAndTeqipStatemasterAndTypeOrderById(
                institute, state, typeId);
        Set<String> approvers = new HashSet<>();

        if (approvalstatusList != null && !approvalstatusList.isEmpty()) {

            List<StatusSummary> statusSummary = new ArrayList<>();

            Integer revision = 1;
            for (TeqipPlanApprovalstatus approvalstatus : approvalstatusList) {
                Integer procurementstageid = approvalstatus.getTeqipPmssProcurementstage().getProcurementstageid();
                String status = approvalstatus.getTeqipPmssProcurementstage().getStatus();

                // skip rejected object
                if (!"rejected".equals(status)) {

                    StatusSummary summary = new StatusSummary();
                    summary.setProcurementstageid(procurementstageid);
                    summary.setStatusName(approvalstatus.getTeqipPmssProcurementstage().getProcurementstages());
                    statusSummary.add(summary);

                }

                StatusSummary lastStatuSummary = statusSummary.get(statusSummary.size() - 1);

                TeqipPmssProcurementstage prevStage = approvalstatus.getTeqipPmssProcurementstage_prev();

                if (lastStatuSummary.getProcurementstageid() != 1
                        && prevStage != null && prevStage.getProcurementstageid() != 1) {

                    boolean revisionMode = false;
                    if (!Objects.equals(approvalstatus.getReviseid(), revision)) {
                        revisionMode = true;
                        revision = approvalstatus.getReviseid();
                    }
                    StatusTimeLine statusTimeLineObj = new StatusTimeLine();

                    statusTimeLineObj.setTime(approvalstatus.getCreatedOn());
                    statusTimeLineObj.setComment(approvalstatus.getStatusComment());
                    statusTimeLineObj.setAction(revisionMode ? "Submit Revision for Approval"
                            : "rejected".equals(status) ? "Revise and Re-Submit" : "No Objection");
                    User actor = approvalstatus.getCreatedBy() != null ? userControllerService.findByUserid(approvalstatus.getCreatedBy()) : null;
                    if (actor != null) {
                        statusTimeLineObj.setActor(actor.getUserName());
                        approvers.add(actor.getUserName());
                    }

                    if (!"rejected".equals(status) && statusSummary.size() >= 2) {
                        statusSummary.get(statusSummary.size() - 2).getStatusTimeLine().add(statusTimeLineObj);
                    }
                    if ("rejected".equals(status) && statusSummary.size() >= 1) {
                        statusSummary.get(statusSummary.size() - 1).getStatusTimeLine().add(statusTimeLineObj);
                    }
                }

            }

            TeqipPlanApprovalstatus lastObject = approvalstatusList.get(approvalstatusList.size() - 1);
            if ("approved".equals(lastObject.getTeqipPmssProcurementstage().getStatus())
                    && lastObject.getTeqipPmssProcurementstage().getProcurementstageid() != 10
                    && lastObject.getTeqipPmssProcurementstage().getProcurementstageid() != 12) {

                StatusSummary summary = new StatusSummary();
                summary.setStatusName("Waiting for " + lastObject.getTeqipPmssProcurementstage().getProcurementstages().replaceAll("Approval", "") + " to take Action !");
                summary.setIsWaitingObject(1);
                statusSummary.add(summary);

            }

            planSatus.setStatusSummary(statusSummary);

            PlanInfo planInfo = new PlanInfo();
            TeqipPlanApprovalstatus first = approvalstatusList.get(0);
            TeqipPlanApprovalstatus last = approvalstatusList.get(approvalstatusList.size() - 1);

            planInfo.setStarted(first.getCreatedOn());
            planInfo.setLastRun(last.getCreatedOn());
            planInfo.setCurrent(last.getTeqipPmssProcurementstage().getProcurementstages());

            User userfirst = first.getCreatedBy() != null ? userControllerService.findByUserid(first.getCreatedBy()) : null;
            if (userfirst != null) {
                planInfo.setOriginator(userfirst.getUserName());
            }
            User userlast = first.getCreatedBy() != null ? userControllerService.findByUserid(first.getCreatedBy()) : null;
            if (userlast != null) {
                planInfo.setLastActor(userlast.getUserName());
            }

            planInfo.setApprovers(StringUtils.collectionToCommaDelimitedString(approvers));

            planSatus.setPlanInfo(planInfo);

        }
        return planSatus;
    }

}
