package com.qspear.procurement.service;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qspear.procurement.exception.ErrorCode;
import com.qspear.procurement.exception.PMSSBudgetException;
import com.qspear.procurement.model.PrecurementMethodTimeLinePojo;
import com.qspear.procurement.model.TeqipSuccess;
import com.qspear.procurement.model.TeqipSupplierVerify;
import com.qspear.procurement.model.methods.MethodPackageResponse;
import com.qspear.procurement.model.methods.MethodPackageUpdateRequest;
import com.qspear.procurement.model.methods.SupplierVerify;
import com.qspear.procurement.persistence.TeqipPackage;
import com.qspear.procurement.persistence.methods.TeqipPmssSupplierMaster;
import com.qspear.procurement.service.mapper.AdvExpOfInterestMapper;
import com.qspear.procurement.service.mapper.AdvIssueOfBidMapper;
import com.qspear.procurement.service.mapper.AwardsMapper;
import com.qspear.procurement.service.mapper.BidMapper;
import com.qspear.procurement.service.mapper.CompleteCheckListMapper;
import com.qspear.procurement.service.mapper.ContractAwardMapper;
import com.qspear.procurement.service.mapper.ContractGenerationMapper;
import com.qspear.procurement.service.mapper.ContractSanctionOrderMapper;
import com.qspear.procurement.service.mapper.ContractTermsMapper;
import com.qspear.procurement.service.mapper.DocGenerationMapper;
import com.qspear.procurement.service.mapper.EMDDetailsMapper;
import com.qspear.procurement.service.mapper.EOIExtensionMapper;
import com.qspear.procurement.service.mapper.ExtensionMapper;
import com.qspear.procurement.service.mapper.FinancialOpeningMapper;
import com.qspear.procurement.service.mapper.GRNDetailsMapper;
import com.qspear.procurement.service.mapper.InvitationLetterMapper;
import com.qspear.procurement.service.mapper.IssueOfRFPMapper;
import com.qspear.procurement.service.mapper.L1SupplierMapper;
import com.qspear.procurement.service.mapper.LetterOfAcceptanceMapper;
import com.qspear.procurement.service.mapper.PBGDetailsMapper;
import com.qspear.procurement.service.mapper.PackageDetailsMapper;
import com.qspear.procurement.service.mapper.PackageSupplierMappper;
import com.qspear.procurement.service.mapper.PaymentMapper;
import com.qspear.procurement.service.mapper.LoaAwardMapper;
import com.qspear.procurement.service.mapper.PriorReviewMapper;
import com.qspear.procurement.service.mapper.ProcuringDepartmentMappper;
import com.qspear.procurement.service.mapper.PurchaseOrderMapper;
import com.qspear.procurement.service.mapper.QuestionMapper;
import com.qspear.procurement.service.mapper.QuotationEvaluationMapper;
import com.qspear.procurement.service.mapper.QuotationOpeningMapper;
import com.qspear.procurement.service.mapper.RFPMapper;
import com.qspear.procurement.service.mapper.ShortListingMapper;
import com.qspear.procurement.service.mapper.StatusDataMapper;
import com.qspear.procurement.service.mapper.TechEvalCommitteeMapper;
import com.qspear.procurement.service.mapper.TechEvaluationMapper;
import com.qspear.procurement.service.mapper.TermsOfReferenceMapper;
import com.qspear.procurement.service.mapper.UploadMapper;
import com.qspear.procurement.service.mapper.WCCPaymentMapper;
import com.qspear.procurement.service.mapper.WorkOrderMapper;
import java.util.List;
import java.util.Objects;

/**
 * Created by e1002703 on 4/3/2018.
 */
@Service
@Transactional
public class MethodService extends AbstractPackageService {

    @Autowired
    AwardsMapper awardsMapper;

    @Autowired
    InvitationLetterMapper invitationLetterMapper;

    @Autowired
    PackageDetailsMapper packageDetailsMapper;

    @Autowired
    GRNDetailsMapper paymentDetailsMapper;

    @Autowired
    PurchaseOrderMapper purchaseOrderMapper;

    @Autowired
    QuotationEvaluationMapper quotationEvaluationMapper;

    @Autowired
    QuotationOpeningMapper quotationOpeningMapper;

    @Autowired
    GRNDetailsMapper grnDetailsMapper;

    @Autowired
    UploadMapper uploadMapper;

    @Autowired
    LetterOfAcceptanceMapper letterOfAcceptanceMapper;

    @Autowired
    WorkOrderMapper workOrderMapper;

    @Autowired
    BidMapper bidMapper;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    LoaAwardMapper poContractGenerationMapper;

    @Autowired
    AdvIssueOfBidMapper advIssueOfBidMapper;

    @Autowired
    WCCPaymentMapper wccPaymentMapper;

    @Autowired
    PaymentMapper paymentMapper;

    @Autowired
    PackageSupplierMappper packageSupplierMappper;

    @Autowired
    QuestionMapper questionMapper;

    @Autowired
    ProcuringDepartmentMappper procuringDepartmentMappper;

    @Autowired
    CompleteCheckListMapper completeCheckListMapper;

    @Autowired
    TermsOfReferenceMapper termsOfReferenceMapper;

    @Autowired
    TechEvalCommitteeMapper techEvalCommitteeMapper;

    @Autowired
    ShortListingMapper shortListingMapper;

    @Autowired
    RFPMapper rfpMapper;

    @Autowired
    IssueOfRFPMapper issueOfRFPMapper;

    @Autowired
    ContractAwardMapper contractAwardMapper;

    @Autowired
    ContractGenerationMapper contractGenerationMapper;

    @Autowired
    FinancialOpeningMapper financialOpeningMapper;

    @Autowired
    TechEvaluationMapper techEvaluationMapper;

    @Autowired
    AdvExpOfInterestMapper advExpOfInterestMapper;

    @Autowired
    ContractTermsMapper contractTermsMapper;

    @Autowired
    StatusDataMapper statusDataMapper;

    @Autowired
    PMSSReferenceService referenceService;

    @Autowired
    PriorReviewMapper priorReviewMapper;

    @Autowired
    ExtensionMapper extensionMapper;

    @Autowired
    EMDDetailsMapper eMDDetailsMapper;

    @Autowired
    PBGDetailsMapper pBDGDetailsMapper;
    @Autowired
    EOIExtensionMapper eoiExtensionMapper;

    @Autowired
    ContractSanctionOrderMapper contractSanctionOrderMapper;

    @Autowired
    L1SupplierMapper l1SupplierMapper;

     @Autowired
    DocGenerationMapper docGenerationMapper;
    
    public ResponseEntity<MethodPackageResponse> getPackageDetails(int packageId) {
        MethodPackageResponse response = new MethodPackageResponse();

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new PMSSBudgetException("Package is not available.",
                    new ErrorCode("PACKAGE NOT FOUND", "Package issue"));
        }

        packageDetailsMapper.mapEntityToObject(teqipPackage, response);
        invitationLetterMapper.mapEntityToObject(teqipPackage, response);
        quotationOpeningMapper.mapEntityToObject(teqipPackage, response);
        quotationEvaluationMapper.mapEntityToObject(teqipPackage, response);
        awardsMapper.mapEntityToObject(teqipPackage, response);
        purchaseOrderMapper.mapEntityToObject(teqipPackage, response);
        grnDetailsMapper.mapEntityToObject(teqipPackage, response);
        uploadMapper.mapEntityToObject(teqipPackage, response);
        letterOfAcceptanceMapper.mapEntityToObject(teqipPackage, response);
        workOrderMapper.mapEntityToObject(teqipPackage, response);
        bidMapper.mapEntityToObject(teqipPackage, response);
        poContractGenerationMapper.mapEntityToObject(teqipPackage, response);
        advIssueOfBidMapper.mapEntityToObject(teqipPackage, response);
        wccPaymentMapper.mapEntityToObject(teqipPackage, response);
        paymentMapper.mapEntityToObject(teqipPackage, response);
        packageSupplierMappper.mapEntityToObject(teqipPackage, response);
        questionMapper.mapEntityToObject(teqipPackage, response);
        procuringDepartmentMappper.mapEntityToObject(teqipPackage, response);
        termsOfReferenceMapper.mapEntityToObject(teqipPackage, response);
        techEvalCommitteeMapper.mapEntityToObject(teqipPackage, response);
        shortListingMapper.mapEntityToObject(teqipPackage, response);
        rfpMapper.mapEntityToObject(teqipPackage, response);
        issueOfRFPMapper.mapEntityToObject(teqipPackage, response);
        contractAwardMapper.mapEntityToObject(teqipPackage, response);
        contractGenerationMapper.mapEntityToObject(teqipPackage, response);
        financialOpeningMapper.mapEntityToObject(teqipPackage, response);
        techEvaluationMapper.mapEntityToObject(teqipPackage, response);
        advExpOfInterestMapper.mapEntityToObject(teqipPackage, response);
        completeCheckListMapper.mapEntityToObject(teqipPackage, response);
        contractTermsMapper.mapEntityToObject(teqipPackage, response);
        statusDataMapper.mapEntityToObject(teqipPackage, response);
        priorReviewMapper.mapEntityToObject(teqipPackage, response);
        extensionMapper.mapEntityToObject(teqipPackage, response);
        eMDDetailsMapper.mapEntityToObject(teqipPackage, response);
        pBDGDetailsMapper.mapEntityToObject(teqipPackage, response);
        eoiExtensionMapper.mapEntityToObject(teqipPackage, response);
        contractSanctionOrderMapper.mapEntityToObject(teqipPackage, response);
        l1SupplierMapper.mapEntityToObject(teqipPackage, response);
        docGenerationMapper.mapEntityToObject(teqipPackage, response);

        PrecurementMethodTimeLinePojo findProcureUserTime = referenceService.findProcurerevisedTime(packageId);

        if (findProcureUserTime != null) {
            findProcureUserTime.setPackageInitatedate(teqipPackage.getInitiateDate());
        }

        response.setTimeLine(findProcureUserTime);

        return ResponseEntity.ok().body(response);

    }

    @Transactional
    public ResponseEntity<MethodPackageResponse> updatePackageComponents(int packageId, MethodPackageUpdateRequest methodPackageUpdateRequest) {

        TeqipPackage teqipPackage = teqipPackageRepository.findOne(packageId);

        if (teqipPackage == null) {
            throw new PMSSBudgetException("Package is not available.",
                    new ErrorCode("PACKAGE NOT FOUND", "Package issue"));
        }

        packageDetailsMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        invitationLetterMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        quotationOpeningMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        quotationEvaluationMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        awardsMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        purchaseOrderMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        grnDetailsMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        uploadMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        letterOfAcceptanceMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        workOrderMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        bidMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        poContractGenerationMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        advIssueOfBidMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        wccPaymentMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        paymentMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        packageSupplierMappper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        questionMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        procuringDepartmentMappper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        completeCheckListMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        termsOfReferenceMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        techEvalCommitteeMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        shortListingMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        rfpMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        issueOfRFPMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        contractAwardMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        contractGenerationMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        financialOpeningMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        techEvaluationMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        advExpOfInterestMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        contractTermsMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        statusDataMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        priorReviewMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        extensionMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        eMDDetailsMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        pBDGDetailsMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        eoiExtensionMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        contractSanctionOrderMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        l1SupplierMapper.mapObjectToEntity(teqipPackage, methodPackageUpdateRequest);
        this.entityManager.clear();
        return this.getPackageDetails(packageId);
    }

    public TeqipSupplierVerify verifySupplier(SupplierVerify verify) {

        TeqipSupplierVerify verifyResponse = new TeqipSupplierVerify(true);

        if (verify.getEmailId() != null &&  ! verify.getEmailId().trim().isEmpty()) {

            List<TeqipPmssSupplierMaster> supplierMasters = null;
            if (verify.getSupplierId() != null && verify.getSupplierId() > 0) {
                supplierMasters = supplierRepository.findSupplierByEmailAndNotSupplierId(verify.getEmailId(), verify.getSupplierId());
            } else {

                supplierMasters = supplierRepository.findSupplierByEmail(verify.getEmailId());
            }
            if (supplierMasters != null && !supplierMasters.isEmpty()) {
                verifyResponse.setStatus(false);
                verifyResponse.getMessage().add("emailId");

            }

        }
        if (verify.getPhoneNo() != null &&  ! verify.getPhoneNo().trim().isEmpty()) {
            List<TeqipPmssSupplierMaster> supplierMasters = null;
            if (verify.getSupplierId() != null && verify.getSupplierId() > 0) {
                supplierMasters = supplierRepository.findSupplierByPhoneAndNotSupplierId(verify.getPhoneNo(), verify.getSupplierId());
            } else {

                supplierMasters = supplierRepository.findSupplierByPhone(verify.getPhoneNo());
            }
            if (supplierMasters != null && !supplierMasters.isEmpty()) {
                verifyResponse.setStatus(false);
                verifyResponse.getMessage().add("phoneNo");
            }

        }
        if (verify.getPanNo() != null &&  ! verify.getPanNo().trim().isEmpty()) {
            List<TeqipPmssSupplierMaster> supplierMasters = null;
            if (verify.getSupplierId() != null && verify.getSupplierId() > 0) {
                supplierMasters = supplierRepository.findSupplierByPanAndNotSupplierId(verify.getPanNo(), verify.getSupplierId());
            } else {

                supplierMasters = supplierRepository.findSupplierByPan(verify.getPanNo());
            }
            if (supplierMasters != null && !supplierMasters.isEmpty()) {
                verifyResponse.setStatus(false);
                verifyResponse.getMessage().add("panNo");
            }

        }
        if (verify.getGstNo() != null &&  ! verify.getGstNo().trim().isEmpty()) {
            List<TeqipPmssSupplierMaster> supplierMasters = null;
            if (verify.getSupplierId() != null && verify.getSupplierId() > 0) {
                supplierMasters = supplierRepository.findSupplierByGstNoAndNotSupplierId(verify.getGstNo(), verify.getSupplierId());
            } else {

                supplierMasters = supplierRepository.findSupplierByGstNo(verify.getGstNo());
            }
            if (supplierMasters != null && !supplierMasters.isEmpty()) {
                verifyResponse.setStatus(false);
                verifyResponse.getMessage().add("gstNo");
            }

        }
        return verifyResponse;
    }

}
