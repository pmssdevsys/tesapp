package com.qspear.procurement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.qspear.procurement.repositories.ActivityRepository;
import com.qspear.procurement.repositories.CategoryRepository;
import com.qspear.procurement.repositories.ItemRepository;
import com.qspear.procurement.repositories.ProcurementPlanRepository;
import com.qspear.procurement.repositories.ProcurementmethodRepository;
import com.qspear.procurement.repositories.RoleMasterRepository;
import com.qspear.procurement.repositories.TeqipInstitutionRepository;
import com.qspear.procurement.repositories.TeqipInstitutiondepartmentmappingRepository;
import com.qspear.procurement.repositories.TeqipInstitutionsubcomponentmappingRepository;
import com.qspear.procurement.repositories.TeqipPackageHistoryRepository;
import com.qspear.procurement.repositories.TeqipPackageRepository;
import com.qspear.procurement.repositories.TeqipPlanApprovalStatusRepository;
import com.qspear.procurement.repositories.TeqipPmssCurrencymasterRepository;
import com.qspear.procurement.repositories.TeqipPmssProcurementstageRepository;
import com.qspear.procurement.repositories.TeqipPmssThreshholdmasterRepository;
import com.qspear.procurement.repositories.TeqipProcMethodTimelineRepository;
import com.qspear.procurement.repositories.TeqipProcurementmasterRepository;
import com.qspear.procurement.repositories.TeqipSubcategorymasterRepository;
import com.qspear.procurement.repositories.TeqipSubcomponentmasterRepository;
import com.qspear.procurement.repositories.method.PackageDocumentRepository;
import com.qspear.procurement.repositories.method.PackagePaymentRepository;
import com.qspear.procurement.repositories.method.PackageQuestionRepository;
import com.qspear.procurement.repositories.method.PackageSupplierRepository;
import com.qspear.procurement.repositories.method.SupplierRepository;
import com.qspear.procurement.util.ProcurementUtility;
import com.qspear.procurement.repositories.StateMasterRepository;

@Component
public abstract class AbstractPackageService {

    @Autowired
    TeqipPackageHistoryRepository packageHistoryRepository;
    
    @Autowired
    protected TeqipPackageRepository teqipPackageRepository;

    @Autowired
    protected ProcurementUtility procurementUtility;

    @Autowired
    protected TeqipPmssCurrencymasterRepository teqipPmssCurrencymasterRepository;

    @Autowired
    protected TeqipPmssThreshholdmasterRepository teqipPmssThreshholdmasterRepository;

    @Autowired
    protected ItemRepository itemRepository;

    @Autowired
    protected ProcurementPlanRepository procurementPlanRepository;

    @Autowired
    protected TeqipProcMethodTimelineRepository timelineRepository;

    @Autowired
    protected TeqipProcurementmasterRepository teqipProcurementmasterRepository;

    @Autowired
    protected ProcurementmethodRepository procurementmethodRepository;

    @Autowired
    protected ActivityRepository activityRepository;

    @Autowired
    protected TeqipSubcategorymasterRepository teqipSubcategorymasterRepository;

    @Autowired
    protected CategoryRepository categoryRepository;

    @Autowired
    protected TeqipInstitutionRepository teqipInstitutionRepository;

    @Autowired
    protected TeqipPmssProcurementstageRepository teqipPmssProcurementstageRepository;

    @Autowired
    protected TeqipSubcomponentmasterRepository teqipSubcomponentmasterRepository;

    @Autowired
    protected TeqipInstitutiondepartmentmappingRepository teqipInstitutiondepartmentmappingRepository;

    @Autowired
    protected TeqipInstitutionsubcomponentmappingRepository teqipInstitutionsubcomponentmappingRepository;

    @Autowired
    protected TeqipPlanApprovalStatusRepository teqipPlanApprovalStatusRepository;

    @Autowired
    protected StateMasterRepository teqipStatemasteryRepository;

    @Autowired
    PackageSupplierRepository packageSupplierRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PackageQuestionRepository packageQuestionRepository;

    @Autowired
    PackagePaymentRepository packagePaymentRepository;

    @Autowired
    PackageDocumentRepository documentRepository;

   

    @Autowired
    PackageService packageService;
    
    @Autowired
    RoleMasterRepository roleMasterRepository;

}
