package com.qspear.procurement.configuration.dao;

import java.util.List;

import com.qspear.procurement.model.SupplierMasterModel;

public interface ProcurmentPlanDao {


List<Object[]> getInstitutedpartByInstituteId(Integer id) throws Exception;

List<Object[]> getInstituteLogoByInstituteId(Integer id) throws Exception;

List<Object[]> getInstitutionsubcomponentByInstId(Integer id)throws Exception;

List<Object[]> getInstitutionPurchaseCommitteByInstId(Integer id) throws Exception;
List<Object[]> getByString(String string,String username,Integer roleid,String institutionName,String state,String name)throws Exception;

List<Object[]> getInstitutionPurchaseCommitteBystId(Long id);
List<Object[]> searchSupplierName(String word);
 List<Object[]> getStateForSupplier();
 List<Object[]> getAllSupplierName();
 List<Object[]> getSupplierForEdit(Integer id);
 List<Object[]> getAllSubComponentByState(Integer id);
 List<Object[]>  getAllDepartmentByState(Integer id);
 List<Object[]> getAllPurchaseByStateId(Integer id);
 List<Object[]>  getAllInstituteLogoByStateId(Integer id);
 public List<Object[]> getAllItemName();
 List<Object[]> getAllNpcBudget();
 List<Object[]> getPurchaseDataForNpc();
 public List<Object[]> getAllEdcilBudget();
}
