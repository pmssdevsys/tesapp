package com.qspear.procurement.configuration.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.qspear.procurement.model.SupplierMasterModel;
import com.qspear.procurement.persistence.SupplierMaster;

@Repository
public class ProcurmentPlanDaoImp implements ProcurmentPlanDao{
	 @PersistenceContext
	  private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getInstitutedpartByInstituteId(Integer id) throws Exception {
		// TODO Auto-generated method stub
		//String query = "SELECT  DEPARTMENT_CODE, departmenthead, departmentname from teqip_institutiondepartmentmapping  where INSTITUTION_ID=:id";
		String query="SELECT i.isactive,i.InstitutionDepartmentMapping_ID,i.DEPARTMENTHEAD,i.departmentcode,d.Department,d.DepartmentID from teqip_institutiondepartmentmapping i, teqip_pmssdepartmentmaster d where i.DEPARTMENT_ID=d.DepartmentID and i.INSTITUTION_ID=:id and i.isactive=1";
		Query q= entityManager.createNativeQuery(query);
		q.setParameter("id", id);
		
		List<Object[]> objList = q.getResultList();
		if(objList.isEmpty()){
			return objList;
		}
		return objList;
	}

//	@Override
//	public List<Object[]> getInstituteLogoByInstituteId(Long id) throws Exception {
//		// TODO Auto-generated method stub
//		String query = "SELECT l.InstitutionLogoID,l.OriginalFileName,l.Description,l.CREATED_ON,l.ISACTIVE from teqip_pmss_institutionlogo l  where INSTITUTION_ID=:id and l.ISACTIVE=1";
//		Query q= entityManager.createNativeQuery(query);
//		q.setParameter("id", id);
//		List<Object[]> objList = q.getResultList();
//		return objList;
//	}

	@Override
	public List<Object[]> getInstitutionsubcomponentByInstId(Integer id) throws Exception {
		// TODO Auto-generated method stub
		
		String query = "select m.ID,i.isactive,m.SUB_COMPONENT_NAME,i.InstitutionSubcomponentMapping_ID,i.INSTITUTION_ID,i.SPFUID,i.allocatedbudget,i.budgetforcw,i.budgetforservices,i.budgetforprocuremen,m.SUBCOMPONENTCODE from TEQIP_InstitutionSubcomponentMapping  i, TEQIP_SUBCOMPONENTMASTER m  where i.SUBCOMPONENT_ID = m.ID AND i.INSTITUTION_ID= :id and i.isactive=1";
		Query q= entityManager.createNativeQuery(query);
		q.setParameter("id", id);
		List<Object[]> objList = q.getResultList();
		return objList;
	}

	@Override
	public List<Object[]> getInstitutionPurchaseCommitteByInstId(Integer id) throws Exception {
		// TODO Auto-generated method stub
		
		//String query = "select d.Department ,c.CommitteMemberName,c.Designation,c.PCommitteRole from TEQIP_PMSS_InstitutionPurchaseCommitte c, TEQIP_PMSSDepartmentMaster d where c.DEPARTMENT_ID=d.DepartmentID AND c.INSTITUTION_ID = :id";
		String query="select c.isactive,c.institutionPurchaseCommitte_ID, d.Department ,c.INSTITUTION_ID,c.SPFUID,c.CommitteMemberName,c.DesignationName,c.PcCommitteName from TEQIP_PMSS_InstitutionPurchaseCommitte c, TEQIP_PMSSDepartmentMaster d where c.DEPARTMENT_ID=d.DepartmentID AND c.INSTITUTION_ID =:id and c.ISACTIVE=1";
		Query q= entityManager.createNativeQuery(query);
		q.setParameter("id", id);
		List<Object[]> objList = q.getResultList();
		return objList;
	}
	
	@Override
	public List<Object[]> getInstitutionPurchaseCommitteBystId(Long id)  {
		// TODO Auto-generated method stub
		
		//String query = "select d.Department ,c.CommitteMemberName,c.Designation,c.PCommitteRole from TEQIP_PMSS_InstitutionPurchaseCommitte c, TEQIP_PMSSDepartmentMaster d where c.DEPARTMENT_ID=d.DepartmentID AND c.INSTITUTION_ID = :id";
		String query="select c.isactive,c.institutionPurchaseCommitte_ID,c.INSTITUTION_ID,c.SPFUID,c.CommitteMemberName,c.DesignationName,c.PcCommitteName from TEQIP_PMSS_InstitutionPurchaseCommitte c where c.SPFUID =:id and c.ISACTIVE=1";
		Query q= entityManager.createNativeQuery(query);
		q.setParameter("id", id);
		List<Object[]> objList = q.getResultList();
		return objList;
	}
	@Override
	public List<Object[]> getByString(String string,String userName,Integer roleid,String institutionName,String state,String name) {
		// TODO Auto-generated method stub
		Query q=entityManager.createNativeQuery(string);
		if(null!=userName && !userName.isEmpty()){
			q.setParameter("userName", userName);
		}
		if(null!=roleid){
			q.setParameter("roleid", roleid);
		}
		if(null!=institutionName  && !institutionName.isEmpty()){
			q.setParameter("institutionName", institutionName);
		}
		if(null!=state && !state.isEmpty()){
			q.setParameter("state", state);
		}
		if(null!=name && !name.isEmpty()){
			q.setParameter("name", name);
		}
		List<Object[]> objList = q.getResultList();
		return objList;
	}

	public List<Object[]> getInstituteLogoByInstituteId(Integer id) {
		// TODO Auto-generated method stub
		String query = "SELECT l.InstitutionLogoID,l.OriginalFileName,l.Description,l.CREATED_ON,l.ISACTIVE from teqip_pmss_institutionlogo l  where l.INSTITUTION_ID =:id and l.ISACTIVE=1";
		Query q= entityManager.createNativeQuery(query);
		q.setParameter("id", id);
		List<Object[]> objList = q.getResultList();
		return objList;
	}

	public List<Object[]> searchSupplierName(String word){
		String w=word+"%";
		
		String query="Select * from TEQIP_PMSS_suppliermaster where ISACTIVE=1 and  suppliername like '"+w+"'";
		Query q=entityManager.createNativeQuery(query);
		List<Object[]>  ob=q.getResultList();
		return ob;
	}
	public List<Object[]> getStateForSupplier()
	{
		String query="SELECT STATE_NAME FROM teqip_statemaster where ISACTIVE=1";
		List<Object[]> q=entityManager.createNativeQuery(query).getResultList();	
            return q;
	}
	public List<Object[]> getAllSupplierName(){
		String query="Select * from TEQIP_PMSS_suppliermaster where isactive=1";
		List <Object[]> q=entityManager.createNativeQuery(query).getResultList();
		return q;
		}
	
	public List<Object[]> getAllItemName(){
		String query="SELECT * FROM teqip_item_master";
		List<Object[]> q=entityManager.createNativeQuery(query).getResultList();
		return q;
	}

public List<Object[]> getSupplierForEdit(Integer id){
	String query="SELECT s.*,state.STATE_ID FROM steqip_pmss_suppliermaster s,teqip_statemaster state  where s.supplierID=:id group by s.supplierID";
	List <Object []> q=entityManager.createNativeQuery(query).setParameter("id",id).getResultList();
	return q;
	}

	public List<Object[]> getAllSubComponentByState(Integer id) {
		String query = 
				  "select i.isactive,m.SUB_COMPONENT_NAME,i.InstitutionSubcomponentMapping_ID,"
				+ " i.INSTITUTION_ID,i.SPFUID,i.allocatedbudget,i.budgetforcw,"
				+ " i.budgetforservices,i.budgetforprocuremen,m.SUBCOMPONENTCODE,i.SUBCOMPONENT_ID  "
				+ " from TEQIP_InstitutionSubcomponentMapping  i, TEQIP_SUBCOMPONENTMASTER m  where "
				+ " i.SUBCOMPONENT_ID = m.ID AND i.spfuid='"+id+"' and i.isactive=1 ";
		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();
		return q;

	}

	public List<Object[]> getAllDepartmentByState(Integer id) {

		String query = "SELECT i.isactive,i.InstitutionDepartmentMapping_ID,i.DEPARTMENTHEAD,i.departmentcode,"
				+"d.Department,d.DepartmentID,i.spfuid from teqip_institutiondepartmentmapping i, "
				+" teqip_pmssdepartmentmaster d where i.DEPARTMENT_ID=d.DepartmentID"
			     +"	 and i.spfuid='"+id+"'  and i.isactive=1";
		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();
		return q;
	}

	public List<Object[]> getAllDepartmentForNpc() {
		String query = "SELECT i.isactive,i.InstitutionDepartmentMapping_ID,i.DEPARTMENTHEAD,i.departmentcode,"
				+ "d.Department,d.DepartmentID,i.spfuid from teqip_institutiondepartmentmapping i, "
				+ "teqip_pmssdepartmentmaster d where i.DEPARTMENT_ID=d.DepartmentID"
				+ " and  i.INSTITUTION_ID is null and i.SPFUID is null and i.type =0   and i.isactive=1";
		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();
		return q;
	}
	
	public List<Object[]> getAllDepartmentForEdcil() {
		String query = "SELECT i.isactive,i.InstitutionDepartmentMapping_ID,i.DEPARTMENTHEAD,i.departmentcode,"
				+ "d.Department,d.DepartmentID,i.spfuid from teqip_institutiondepartmentmapping i, "
				+ "teqip_pmssdepartmentmaster d where i.DEPARTMENT_ID=d.DepartmentID"
				+ " and  i.INSTITUTION_ID is null and i.SPFUID is null and i.type =1   and i.isactive=1";
		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();
		return q;
	}
	public List<Object[]> getAllPurchaseByStateId(Integer id) {
		String query =  "select c.isactive,c.institutionPurchaseCommitte_ID, d.Department ,"
				+"c.INSTITUTION_ID,c.SPFUID,c.CommitteMemberName,c.DesignationName,"
				+" c.PcCommitteName ,c.DEPARTMENT_ID from TEQIP_PMSS_InstitutionPurchaseCommitte c, "
				+"TEQIP_PMSSDepartmentMaster d where c.DEPARTMENT_ID=d.DepartmentID"
				+"  AND c.spfuid ='"+id+"' and c.ISACTIVE=1";
		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();
		return q;

	}

	public List<Object[]> getPurchaseDataForNpc() {
		String query = "select c.isactive,c.institutionPurchaseCommitte_ID, d.Department ,"
				+ "c.INSTITUTION_ID,c.SPFUID,c.CommitteMemberName,c.DesignationName,"
				+ "c.PcCommitteName,c.DEPARTMENT_ID from TEQIP_PMSS_InstitutionPurchaseCommitte c, "
				+ " TEQIP_PMSSDepartmentMaster d where c.DEPARTMENT_ID=d.DepartmentID"
				+ "  AND c.INSTITUTION_ID is null and c.SPFUID is null and c.type =0 and c.ISACTIVE=1";
		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();
		return q;

	}
	
	public List<Object[]> getPurchaseDataForEdcil() {
		String query = "select c.isactive,c.institutionPurchaseCommitte_ID, d.Department ,"
				+ "c.INSTITUTION_ID,c.SPFUID,c.CommitteMemberName,c.DesignationName,"
				+ "c.PcCommitteName,c.DEPARTMENT_ID from TEQIP_PMSS_InstitutionPurchaseCommitte c, "
				+ " TEQIP_PMSSDepartmentMaster d where c.DEPARTMENT_ID=d.DepartmentID"
				+ "  AND c.INSTITUTION_ID is null and c.SPFUID is null and c.type =1 and c.ISACTIVE=1";
		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();
		return q;

	}


	public List<Object[]> getAllInstituteLogoByStateId(Integer id) {
		String query = "SELECT l.InstitutionLogoID,l.OriginalFileName,l.Description,l.CREATED_ON,l.ISACTIVE "
				+ " from teqip_pmss_institutionlogo l  where l.spfuid ='"+id+"'  and l.ISACTIVE=1";

		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();

		return q;
	}

	
	public List<Object[]> getAllLogoForNpc(Integer id) {
		StringBuilder s=new StringBuilder("SELECT l.InstitutionLogoID,l.OriginalFileName,l.Description,l.CREATED_ON,l.ISACTIVE from teqip_pmss_institutionlogo l  where   l.ISACTIVE=1 and  l.INSTITUTION_ID is null and l.SPFUID is null ");
if(id==2)
		s.append("and"+ " l.type =0");
else if(id==3)
	s.append("and"+ " l.type =1");
System.out.println("--->"+s.toString());
		List<Object[]> q = entityManager.createNativeQuery(s.toString()).getResultList();

		
		return q;
	}
	public List<Object[]> getAllNpcBudget() {
		String query = "select i.isactive,m.SUB_COMPONENT_NAME,i.InstitutionSubcomponentMapping_ID,"
				+ "i.INSTITUTION_ID,i.SPFUID,i.allocatedbudget,i.budgetforcw,"
				+ "i.budgetforservices,i.budgetforprocuremen,m.SUBCOMPONENTCODE,i.SUBCOMPONENT_ID "
				+ "from TEQIP_InstitutionSubcomponentMapping  i, TEQIP_SUBCOMPONENTMASTER m  where i.SUBCOMPONENT_ID =m.id and "
				+ " i.INSTITUTION_ID is null and i.SPFUID is null and  i.type =0 and i.isactive=1";
		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();

		return q;
	}
	
	public List<Object[]> getAllEdcilBudget() {
		String query = "select i.isactive,m.SUB_COMPONENT_NAME,i.InstitutionSubcomponentMapping_ID,"
				+ "i.INSTITUTION_ID,i.SPFUID,i.allocatedbudget,i.budgetforcw,"
				+ "i.budgetforservices,i.budgetforprocuremen,m.SUBCOMPONENTCODE,i.SUBCOMPONENT_ID "
				+ "from TEQIP_InstitutionSubcomponentMapping  i, TEQIP_SUBCOMPONENTMASTER m  where i.SUBCOMPONENT_ID =m.id and "
				+ " i.INSTITUTION_ID is null and i.SPFUID is null and  i.type =1 and i.isactive=1";
		List<Object[]> q = entityManager.createNativeQuery(query).getResultList();

		return q;
	}
}



