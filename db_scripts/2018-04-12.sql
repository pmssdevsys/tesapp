-- MySQL dump 10.13  Distrib 5.7.21, for macos10.13 (x86_64)
--
-- Host: localhost    Database: edcil
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `teqip_activitiymaster`
--

DROP TABLE IF EXISTS `teqip_activitiymaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_activitiymaster` (
  `ACTIVITY_ID` int(4) NOT NULL AUTO_INCREMENT,
  `ACTIVITY_NAME` varchar(200) DEFAULT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ACTIVITY_ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`),
  CONSTRAINT `teqip_activitiymaster_ibfk_1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_activitiymaster`
--

LOCK TABLES `teqip_activitiymaster` WRITE;
/*!40000 ALTER TABLE `teqip_activitiymaster` DISABLE KEYS */;
INSERT INTO `teqip_activitiymaster` VALUES (1,'Modernization of classroom and Smart Classroom',1,1,1,'2018-03-07 12:30:02'),(2,'Modernization of classroom Modernization of classroom & Smart Classroom Classroom',1,1,1,'2018-03-07 12:30:24'),(3,'Updation of learning resources',1,1,1,'2018-03-07 12:30:24'),(4,'Establishment/Upgradation of Central Establishment/Upgradation of Central & Departmental computer centres computer centres',1,1,1,'2018-03-07 12:30:33'),(5,'Modernization and Strengthening of Libraries and increasing access to knowledge resources',1,1,1,'2018-03-07 12:30:33'),(6,'Procurement of furniture',1,1,1,'2018-03-07 12:30:33'),(7,'Minor Civil Works',1,1,1,'2018-03-07 12:30:33'),(8,'Networking',1,1,1,'2018-03-07 12:30:33');
/*!40000 ALTER TABLE `teqip_activitiymaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_categorymaster`
--

DROP TABLE IF EXISTS `teqip_categorymaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_categorymaster` (
  `CAT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORY_NAME` varchar(255) NOT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`CAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_categorymaster`
--

LOCK TABLES `teqip_categorymaster` WRITE;
/*!40000 ALTER TABLE `teqip_categorymaster` DISABLE KEYS */;
INSERT INTO `teqip_categorymaster` VALUES (1,'Civil Works',1),(2,'Goods',1),(3,'Services',1);
/*!40000 ALTER TABLE `teqip_categorymaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_componentmaster`
--

DROP TABLE IF EXISTS `teqip_componentmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_componentmaster` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPONENT_CODE` varchar(50) NOT NULL,
  `COMPONENT_NAME` varchar(200) NOT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_componentmaster`
--

LOCK TABLES `teqip_componentmaster` WRITE;
/*!40000 ALTER TABLE `teqip_componentmaster` DISABLE KEYS */;
INSERT INTO `teqip_componentmaster` VALUES (1,'Component 1','Improving Quality of Education in Selected Institutions',1),(2,'Component 2','Improving System Management',1);
/*!40000 ALTER TABLE `teqip_componentmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_institutiondepartmentmapping`
--

DROP TABLE IF EXISTS `teqip_institutiondepartmentmapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_institutiondepartmentmapping` (
  `InstitutionDepartmentMapping_ID` int(11) NOT NULL AUTO_INCREMENT,
  `INSTITUTION_ID` int(11) NOT NULL,
  `DEPARTMENT_ID` int(11) NOT NULL,
  `DEPARTMENTHEAD` varchar(200) NOT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`InstitutionDepartmentMapping_ID`),
  KEY `INSTITUTION_ID` (`INSTITUTION_ID`),
  KEY `DEPARTMENT_ID` (`DEPARTMENT_ID`),
  CONSTRAINT `teqip_institutiondepartmentmapping_ibfk_1` FOREIGN KEY (`INSTITUTION_ID`) REFERENCES `teqip_institutions` (`INSTITUTION_ID`),
  CONSTRAINT `teqip_institutiondepartmentmapping_ibfk_2` FOREIGN KEY (`DEPARTMENT_ID`) REFERENCES `teqip_pmssdepartmentmaster` (`DepartmentID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_institutiondepartmentmapping`
--

LOCK TABLES `teqip_institutiondepartmentmapping` WRITE;
/*!40000 ALTER TABLE `teqip_institutiondepartmentmapping` DISABLE KEYS */;
INSERT INTO `teqip_institutiondepartmentmapping` VALUES (1,1,1,'Prof. Tankeshwar Kumar',1,'2018-04-03 14:43:13',NULL,NULL,NULL),(2,1,2,'Mr. S. U. Ambekar',1,'2018-04-03 14:43:14',NULL,NULL,NULL),(3,2,1,'Prof. Tankeshwar Kumar',1,'2018-04-03 14:43:14',NULL,NULL,NULL),(4,2,2,'Mr. S. U. Ambekar',1,'2018-04-03 14:43:15',NULL,NULL,NULL);
/*!40000 ALTER TABLE `teqip_institutiondepartmentmapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_institutions`
--

DROP TABLE IF EXISTS `teqip_institutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_institutions` (
  `INSTITUTION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `INSTITUTION_CODE` varchar(50) NOT NULL,
  `INSTITUTION_NAME` varchar(255) NOT NULL,
  `STATE` varchar(50) NOT NULL,
  `ADDRESS` varchar(200) NOT NULL,
  `EMAILID` varchar(50) NOT NULL,
  `TELEPHONENUMBER` varchar(50) NOT NULL,
  `FAXNUMBER` varchar(50) NOT NULL,
  `INSTITUTIONTYPE_ID` int(11) NOT NULL,
  `SPFUID` int(11) NOT NULL,
  `WEBSITEURL` varchar(200) NOT NULL,
  `ALLOCATEDBUGET` float DEFAULT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`INSTITUTION_ID`),
  KEY `INSTITUTIONTYPE_ID` (`INSTITUTIONTYPE_ID`),
  KEY `SPFUID` (`SPFUID`),
  CONSTRAINT `teqip_institutions_ibfk_1` FOREIGN KEY (`INSTITUTIONTYPE_ID`) REFERENCES `teqip_institutiontype` (`INSTITUTIONTYPE_ID`),
  CONSTRAINT `teqip_institutions_ibfk_2` FOREIGN KEY (`SPFUID`) REFERENCES `teqip_statemaster` (`STATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_institutions`
--

LOCK TABLES `teqip_institutions` WRITE;
/*!40000 ALTER TABLE `teqip_institutions` DISABLE KEYS */;
INSERT INTO `teqip_institutions` VALUES (1,'aeck','Assam Engineering College, Kamrup','Assam','Test','test@test.com','1234567890','1234567890',1,1,'test',10000000,1,'2018-03-13 15:21:01'),(2,'guis','Guwahati University Institute of Science Technology, Guwahati','Assam','Test','test@test.com','1234567890','1234567890',1,1,'test',10000000,1,'2018-03-13 15:22:24'),(3,'auce','A U College of EngineeringAndhraUniversity,Visakhapatnam','Kerala','Test','test@test.com','1234567890','1234567890',1,3,'test',10000000,1,'2018-03-13 15:24:13');
/*!40000 ALTER TABLE `teqip_institutions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_institutionsubcomponentmapping`
--

DROP TABLE IF EXISTS `teqip_institutionsubcomponentmapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_institutionsubcomponentmapping` (
  `InstitutionSubcomponentMapping_ID` int(11) NOT NULL AUTO_INCREMENT,
  `INSTITUTION_ID` int(11) DEFAULT NULL,
  `SPFUID` int(11) DEFAULT NULL,
  `SUBCOMPONENT_ID` int(11) DEFAULT NULL,
  `ALLOCATEDBUDGET` float DEFAULT NULL,
  `BUDGETFORPROCUREMEN` float DEFAULT NULL,
  `BUDGETFORCW` float DEFAULT NULL,
  `BUDGETFORSERVICES` float DEFAULT NULL,
  `ALLOCATEDTYPE` varchar(50) DEFAULT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`InstitutionSubcomponentMapping_ID`),
  KEY `INSTITUTION_ID` (`INSTITUTION_ID`),
  KEY `SUBCOMPONENT_ID` (`SUBCOMPONENT_ID`),
  KEY `SPFUID` (`SPFUID`),
  CONSTRAINT `teqip_institutionsubcomponentmapping_ibfk_1` FOREIGN KEY (`INSTITUTION_ID`) REFERENCES `teqip_institutions` (`INSTITUTION_ID`),
  CONSTRAINT `teqip_institutionsubcomponentmapping_ibfk_2` FOREIGN KEY (`SUBCOMPONENT_ID`) REFERENCES `teqip_subcomponentmaster` (`ID`),
  CONSTRAINT `teqip_institutionsubcomponentmapping_ibfk_3` FOREIGN KEY (`SPFUID`) REFERENCES `teqip_statemaster` (`STATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_institutionsubcomponentmapping`
--

LOCK TABLES `teqip_institutionsubcomponentmapping` WRITE;
/*!40000 ALTER TABLE `teqip_institutionsubcomponentmapping` DISABLE KEYS */;
INSERT INTO `teqip_institutionsubcomponentmapping` VALUES (1,1,NULL,1,10000000,5500000,3500000,1000000,NULL,1,'2018-04-03 14:42:38',NULL,NULL,NULL);
/*!40000 ALTER TABLE `teqip_institutionsubcomponentmapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_institutiontype`
--

DROP TABLE IF EXISTS `teqip_institutiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_institutiontype` (
  `INSTITUTIONTYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `INSTITUTIONTYPE_NAME` varchar(255) NOT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`INSTITUTIONTYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_institutiontype`
--

LOCK TABLES `teqip_institutiontype` WRITE;
/*!40000 ALTER TABLE `teqip_institutiontype` DISABLE KEYS */;
INSERT INTO `teqip_institutiontype` VALUES (1,'Centrally Funded Institution',1),(2,'Private unaided Institution',1),(3,'Govt./ Govt. aided Institution',1),(4,'Affiliated Technical University',1);
/*!40000 ALTER TABLE `teqip_institutiontype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_item_goods_department_breakup`
--

DROP TABLE IF EXISTS `teqip_item_goods_department_breakup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_item_goods_department_breakup` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ITEM_DETAIL_ID` int(11) DEFAULT NULL,
  `ITEM_DEPARTMENT_ID` int(11) DEFAULT NULL,
  `QUANTITY` float DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ITEM_DETAIL_ID` (`ITEM_DETAIL_ID`),
  KEY `ITEM_DEPARTMENT_ID` (`ITEM_DEPARTMENT_ID`),
  CONSTRAINT `teqip_item_goods_department_breakup_ibfk_1` FOREIGN KEY (`ITEM_DETAIL_ID`) REFERENCES `teqip_item_goods_detail` (`ID`),
  CONSTRAINT `teqip_item_goods_department_breakup_ibfk_2` FOREIGN KEY (`ITEM_DEPARTMENT_ID`) REFERENCES `teqip_pmssdepartmentmaster` (`DepartmentID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_item_goods_department_breakup`
--

LOCK TABLES `teqip_item_goods_department_breakup` WRITE;
/*!40000 ALTER TABLE `teqip_item_goods_department_breakup` DISABLE KEYS */;
INSERT INTO `teqip_item_goods_department_breakup` VALUES (3,1,7,1,'2018-04-11 16:16:59',1,'2018-04-11',1);
/*!40000 ALTER TABLE `teqip_item_goods_department_breakup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_item_goods_department_breakup_history`
--

DROP TABLE IF EXISTS `teqip_item_goods_department_breakup_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_item_goods_department_breakup_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ITEM_DETAIL_ID` int(11) DEFAULT NULL,
  `ITEM_DEPARTMENT_ID` int(11) DEFAULT NULL,
  `QUANTITY` float DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ITEM_DETAIL_ID` (`ITEM_DETAIL_ID`),
  KEY `ITEM_DEPARTMENT_ID` (`ITEM_DEPARTMENT_ID`),
  CONSTRAINT `teqip_item_goods_department_breakup_history_ibfk_1` FOREIGN KEY (`ITEM_DETAIL_ID`) REFERENCES `teqip_item_goods_detail` (`ID`),
  CONSTRAINT `teqip_item_goods_department_breakup_history_ibfk_2` FOREIGN KEY (`ITEM_DEPARTMENT_ID`) REFERENCES `teqip_pmssdepartmentmaster` (`DepartmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_item_goods_department_breakup_history`
--

LOCK TABLES `teqip_item_goods_department_breakup_history` WRITE;
/*!40000 ALTER TABLE `teqip_item_goods_department_breakup_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_item_goods_department_breakup_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_item_goods_detail`
--

DROP TABLE IF EXISTS `teqip_item_goods_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_item_goods_detail` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ITEM_ID` int(5) DEFAULT NULL,
  `ITEM_SPECIFICATION` varchar(500) DEFAULT NULL,
  `ITEM_CATEGORY_ID` int(11) DEFAULT NULL,
  `ITEM_QNT` float DEFAULT NULL,
  `ITEM_COST_UNIT` float DEFAULT NULL,
  `deliveyperiod` int(11) DEFAULT NULL,
  `itemmainspecification` varchar(500) DEFAULT NULL,
  `trainingrequired` int(11) DEFAULT NULL,
  `installationrequired` int(11) DEFAULT NULL,
  `placeofdelivery` varchar(500) DEFAULT NULL,
  `instllationrequirement` varchar(500) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  `PACKAGE_ID` int(11) NOT NULL,
  `REVISEID` int(11) DEFAULT NULL,
  `STATUS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ITEM_CATEGORY_ID` (`ITEM_CATEGORY_ID`),
  KEY `ITEM_ID` (`ITEM_ID`),
  KEY `PACKAGE_ID` (`PACKAGE_ID`),
  CONSTRAINT `teqip_item_goods_detail_ibfk_1` FOREIGN KEY (`ITEM_CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`),
  CONSTRAINT `teqip_item_goods_detail_ibfk_2` FOREIGN KEY (`ITEM_ID`) REFERENCES `teqip_item_master` (`ITEM_ID`),
  CONSTRAINT `teqip_item_goods_detail_ibfk_3` FOREIGN KEY (`PACKAGE_ID`) REFERENCES `teqip_package` (`PACKAGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_item_goods_detail`
--

LOCK TABLES `teqip_item_goods_detail` WRITE;
/*!40000 ALTER TABLE `teqip_item_goods_detail` DISABLE KEYS */;
INSERT INTO `teqip_item_goods_detail` VALUES (1,NULL,'itemDescription',NULL,NULL,100,1,'itemMainSpecification',1,1,'placeofdelivery','instllationrequirement','2018-04-11 16:16:59',NULL,NULL,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `teqip_item_goods_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_item_goods_detail_history`
--

DROP TABLE IF EXISTS `teqip_item_goods_detail_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_item_goods_detail_history` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `ITEM_ID` int(5) DEFAULT NULL,
  `ITEM_SPECIFICATION` varchar(500) DEFAULT NULL,
  `ITEM_CATEGORY_ID` int(11) NOT NULL,
  `ITEM_QNT` float DEFAULT NULL,
  `ITEM_COST_UNIT` float DEFAULT NULL,
  `deliveyperiod` int(11) DEFAULT NULL,
  `itemmainspecification` varchar(500) DEFAULT NULL,
  `trainingrequired` int(11) DEFAULT NULL,
  `installationrequired` int(11) DEFAULT NULL,
  `placeofdelivery` varchar(500) DEFAULT NULL,
  `instllationrequirement` varchar(500) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  `PACKAGE_ID` int(11) NOT NULL,
  `REVISEID` int(11) NOT NULL,
  `STATUS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ITEM_CATEGORY_ID` (`ITEM_CATEGORY_ID`),
  KEY `ITEM_ID` (`ITEM_ID`),
  KEY `PACKAGE_ID` (`PACKAGE_ID`),
  CONSTRAINT `teqip_item_goods_detail_history_ibfk_1` FOREIGN KEY (`ITEM_CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`),
  CONSTRAINT `teqip_item_goods_detail_history_ibfk_2` FOREIGN KEY (`ITEM_ID`) REFERENCES `teqip_item_master` (`ITEM_ID`),
  CONSTRAINT `teqip_item_goods_detail_history_ibfk_3` FOREIGN KEY (`PACKAGE_ID`) REFERENCES `teqip_package` (`PACKAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_item_goods_detail_history`
--

LOCK TABLES `teqip_item_goods_detail_history` WRITE;
/*!40000 ALTER TABLE `teqip_item_goods_detail_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_item_goods_detail_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_item_master`
--

DROP TABLE IF EXISTS `teqip_item_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_item_master` (
  `ITEM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ITEM_NAME` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `ITEM_CATEGORY_ID` int(11) DEFAULT NULL,
  `SUB_CATEGORY_ID` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  `STATUS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ITEM_ID`),
  KEY `ITEM_CATEGORY_ID` (`ITEM_CATEGORY_ID`),
  KEY `SUB_CATEGORY_ID` (`SUB_CATEGORY_ID`),
  CONSTRAINT `teqip_item_master_ibfk_1` FOREIGN KEY (`ITEM_CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`),
  CONSTRAINT `teqip_item_master_ibfk_2` FOREIGN KEY (`SUB_CATEGORY_ID`) REFERENCES `teqip_subcategorymaster` (`SUBCAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_item_master`
--

LOCK TABLES `teqip_item_master` WRITE;
/*!40000 ALTER TABLE `teqip_item_master` DISABLE KEYS */;
INSERT INTO `teqip_item_master` VALUES (1,'Chair',NULL,1,1,'2018-03-09 10:27:28',NULL,NULL,NULL,1),(2,'Book',NULL,1,1,'2018-03-09 10:27:28',NULL,NULL,NULL,1),(3,'Table',NULL,1,1,'2018-03-09 10:27:28',NULL,NULL,NULL,1),(4,'Almirah',NULL,1,1,'2018-03-09 10:27:28',NULL,NULL,NULL,1),(5,'Goods',NULL,1,1,'2018-03-09 10:27:28',NULL,NULL,NULL,1),(6,'Almirah',NULL,2,2,'2018-03-09 10:27:28',NULL,NULL,NULL,1),(7,'Table',NULL,2,2,'2018-03-09 10:27:28',NULL,NULL,NULL,1),(8,'Book',NULL,2,2,'2018-03-09 10:27:28',NULL,NULL,NULL,1),(9,'Chair',NULL,2,2,'2018-03-09 10:27:28',NULL,NULL,NULL,1),(13,'itemName',NULL,NULL,NULL,'2018-04-11 16:16:59',NULL,NULL,NULL,NULL),(14,NULL,NULL,NULL,NULL,'2018-04-11 16:34:56',NULL,NULL,NULL,NULL),(15,NULL,NULL,NULL,NULL,'2018-04-11 16:39:36',NULL,NULL,NULL,NULL),(16,NULL,NULL,NULL,NULL,'2018-04-11 16:45:51',NULL,NULL,NULL,NULL),(18,NULL,NULL,NULL,NULL,'2018-04-11 16:50:42',NULL,NULL,NULL,NULL),(19,NULL,NULL,NULL,NULL,'2018-04-11 16:53:33',NULL,NULL,NULL,NULL),(20,NULL,NULL,NULL,NULL,'2018-04-11 16:55:01',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `teqip_item_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_item_work_detail`
--

DROP TABLE IF EXISTS `teqip_item_work_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_item_work_detail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `WORK_NAME` varchar(500) DEFAULT NULL,
  `WORK_SPECIFICATION` varchar(500) DEFAULT NULL,
  `WORK_CATEGORY_ID` int(11) NOT NULL,
  `ITEM_DEPARTMENT_ID` int(11) NOT NULL,
  `WORK_COST` float DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  `PACKAGE_ID` int(11) DEFAULT NULL,
  `REVISEID` int(11) NOT NULL,
  `STATUS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `WORK_CATEGORY_ID` (`WORK_CATEGORY_ID`),
  KEY `PACKAGE_ID` (`PACKAGE_ID`),
  KEY `ITEM_DEPARTMENT_ID` (`ITEM_DEPARTMENT_ID`),
  CONSTRAINT `teqip_item_work_detail_ibfk_1` FOREIGN KEY (`WORK_CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`),
  CONSTRAINT `teqip_item_work_detail_ibfk_2` FOREIGN KEY (`PACKAGE_ID`) REFERENCES `teqip_package` (`PACKAGE_ID`),
  CONSTRAINT `teqip_item_work_detail_ibfk_3` FOREIGN KEY (`ITEM_DEPARTMENT_ID`) REFERENCES `teqip_institutiondepartmentmapping` (`InstitutionDepartmentMapping_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_item_work_detail`
--

LOCK TABLES `teqip_item_work_detail` WRITE;
/*!40000 ALTER TABLE `teqip_item_work_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_item_work_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_item_work_detail_history`
--

DROP TABLE IF EXISTS `teqip_item_work_detail_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_item_work_detail_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `WORK_NAME` varchar(500) DEFAULT NULL,
  `WORK_SPECIFICATION` varchar(500) DEFAULT NULL,
  `WORK_CATEGORY_ID` int(11) NOT NULL,
  `ITEM_DEPARTMENT_ID` int(11) NOT NULL,
  `WORK_COST` float DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  `PACKAGE_ID` int(11) DEFAULT NULL,
  `REVISEID` int(11) NOT NULL,
  `STATUS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `WORK_CATEGORY_ID` (`WORK_CATEGORY_ID`),
  KEY `PACKAGE_ID` (`PACKAGE_ID`),
  KEY `ITEM_DEPARTMENT_ID` (`ITEM_DEPARTMENT_ID`),
  CONSTRAINT `teqip_item_work_detail_history_ibfk_1` FOREIGN KEY (`WORK_CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`),
  CONSTRAINT `teqip_item_work_detail_history_ibfk_2` FOREIGN KEY (`PACKAGE_ID`) REFERENCES `teqip_package` (`PACKAGE_ID`),
  CONSTRAINT `teqip_item_work_detail_history_ibfk_3` FOREIGN KEY (`ITEM_DEPARTMENT_ID`) REFERENCES `teqip_institutiondepartmentmapping` (`InstitutionDepartmentMapping_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_item_work_detail_history`
--

LOCK TABLES `teqip_item_work_detail_history` WRITE;
/*!40000 ALTER TABLE `teqip_item_work_detail_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_item_work_detail_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package`
--

DROP TABLE IF EXISTS `teqip_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package` (
  `PACKAGE_ID` int(5) NOT NULL AUTO_INCREMENT,
  `PROCUREMENT_PLAN_ID` int(11) NOT NULL,
  `PACKAGE_CODE` varchar(50) DEFAULT NULL,
  `PACKAGE_NAME` varchar(50) DEFAULT NULL,
  `JUSTIFICATION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `SUB_CATEGORY_ID` int(11) DEFAULT NULL,
  `SUB_COMPONENT_ID` int(11) DEFAULT NULL,
  `ACTIVITY_ID` int(11) DEFAULT NULL,
  `FINANCIAL_SANCTION_DATE` date DEFAULT NULL,
  `ISPROP` tinyint(4) DEFAULT NULL,
  `ISGEM` tinyint(4) DEFAULT NULL,
  `ISCOE` tinyint(4) DEFAULT NULL,
  `PROCUREMENT_METHOD_ID` int(11) NOT NULL,
  `SERVICE_PROVIDER_ID` int(11) DEFAULT NULL,
  `ESTIMATED_COST` float DEFAULT NULL,
  `REVISION_COMMENTS` varchar(30) DEFAULT NULL,
  `INSTITUTION_ID` int(11) DEFAULT NULL,
  `SPFUID` int(11) DEFAULT NULL,
  `TYPEOFPLAN_CREATOR` int(11) NOT NULL,
  `PROCUREMENTSTAGE_ID` int(11) NOT NULL,
  `REVISEID` int(11) NOT NULL,
  `PACKAGE_INITIATEDATE` date DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`PACKAGE_ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`),
  KEY `SUB_CATEGORY_ID` (`SUB_CATEGORY_ID`),
  KEY `SUB_COMPONENT_ID` (`SUB_COMPONENT_ID`),
  KEY `ACTIVITY_ID` (`ACTIVITY_ID`),
  KEY `PROCUREMENT_METHOD_ID` (`PROCUREMENT_METHOD_ID`),
  KEY `PROCUREMENT_PLAN_ID` (`PROCUREMENT_PLAN_ID`),
  KEY `INSTITUTION_ID` (`INSTITUTION_ID`),
  KEY `SPFUID` (`SPFUID`),
  KEY `PROCUREMENTSTAGE_ID` (`PROCUREMENTSTAGE_ID`),
  CONSTRAINT `teqip_package_ibfk_1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`),
  CONSTRAINT `teqip_package_ibfk_2` FOREIGN KEY (`SUB_CATEGORY_ID`) REFERENCES `teqip_subcategorymaster` (`SUBCAT_ID`),
  CONSTRAINT `teqip_package_ibfk_3` FOREIGN KEY (`SUB_COMPONENT_ID`) REFERENCES `teqip_subcomponentmaster` (`ID`),
  CONSTRAINT `teqip_package_ibfk_4` FOREIGN KEY (`ACTIVITY_ID`) REFERENCES `teqip_activitiymaster` (`ACTIVITY_ID`),
  CONSTRAINT `teqip_package_ibfk_5` FOREIGN KEY (`PROCUREMENT_METHOD_ID`) REFERENCES `teqip_procurementmethod` (`PROCUREMENTMETHOD_ID`),
  CONSTRAINT `teqip_package_ibfk_6` FOREIGN KEY (`PROCUREMENT_PLAN_ID`) REFERENCES `teqip_procurementmaster` (`PROCUREMENTMASTER_ID`),
  CONSTRAINT `teqip_package_ibfk_7` FOREIGN KEY (`INSTITUTION_ID`) REFERENCES `teqip_institutions` (`INSTITUTION_ID`),
  CONSTRAINT `teqip_package_ibfk_8` FOREIGN KEY (`SPFUID`) REFERENCES `teqip_statemaster` (`STATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package`
--

LOCK TABLES `teqip_package` WRITE;
/*!40000 ALTER TABLE `teqip_package` DISABLE KEYS */;
INSERT INTO `teqip_package` VALUES (1,1,'packageCode','packageName','description',1,1,2,1,'2018-04-11',1,1,1,3,1,100,'test',1,1,1,1,1,'2018-04-11','2018-04-11 16:52:22',123,'2018-04-11',123);
/*!40000 ALTER TABLE `teqip_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_award`
--

DROP TABLE IF EXISTS `teqip_package_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_award` (
  `pack_quotationdetailid` int(11) NOT NULL AUTO_INCREMENT,
  `packageid` int(11) NOT NULL,
  `supplierid` int(11) NOT NULL,
  `ispofinalize` int(11) DEFAULT NULL,
  `userresponse` int(11) DEFAULT NULL,
  `awardcomments` varchar(500) DEFAULT NULL,
  `lowerevaluatedsupplier` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`pack_quotationdetailid`),
  KEY `packageid` (`packageid`),
  KEY `supplierid` (`supplierid`),
  CONSTRAINT `teqip_package_award_ibfk_1` FOREIGN KEY (`packageid`) REFERENCES `teqip_package` (`PACKAGE_ID`),
  CONSTRAINT `teqip_package_award_ibfk_2` FOREIGN KEY (`supplierid`) REFERENCES `teqip_pmss_suppliermaster` (`supplierID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_award`
--

LOCK TABLES `teqip_package_award` WRITE;
/*!40000 ALTER TABLE `teqip_package_award` DISABLE KEYS */;
INSERT INTO `teqip_package_award` VALUES (1,1,1,NULL,NULL,'recommandationComments',NULL,'2018-04-11 16:55:01',1,'2018-04-11',1);
/*!40000 ALTER TABLE `teqip_package_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_history`
--

DROP TABLE IF EXISTS `teqip_package_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_history` (
  `PACKAGE_ID` int(5) NOT NULL AUTO_INCREMENT,
  `PROCUREMENT_PLAN_ID` int(11) NOT NULL,
  `PACKAGE_CODE` varchar(50) DEFAULT NULL,
  `PACKAGE_NAME` varchar(50) DEFAULT NULL,
  `JUSTIFICATION` varchar(1000) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `SUB_CATEGORY_ID` int(11) DEFAULT NULL,
  `SUB_COMPONENT_ID` int(11) DEFAULT NULL,
  `ACTIVITY_ID` int(11) DEFAULT NULL,
  `FINANCIAL_SANCTION_DATE` date DEFAULT NULL,
  `ISPROP` tinyint(4) DEFAULT NULL,
  `ISGEM` tinyint(4) DEFAULT NULL,
  `ISCOE` tinyint(4) DEFAULT NULL,
  `PROCUREMENT_METHOD_ID` int(11) NOT NULL,
  `SERVICE_PROVIDER_ID` int(11) DEFAULT NULL,
  `ESTIMATED_COST` float DEFAULT NULL,
  `REVISION_COMMENTS` varchar(30) DEFAULT NULL,
  `INSTITUTION_ID` int(11) NOT NULL,
  `SPFUID` int(11) DEFAULT NULL,
  `TYPEOFPLAN_CREATOR` int(11) NOT NULL,
  `PROCUREMENTSTAGEID` int(11) NOT NULL,
  `REVISEID` int(11) NOT NULL,
  `PACKAGE_INITIATEDATE` date DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`PACKAGE_ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`),
  KEY `SUB_CATEGORY_ID` (`SUB_CATEGORY_ID`),
  KEY `SUB_COMPONENT_ID` (`SUB_COMPONENT_ID`),
  KEY `ACTIVITY_ID` (`ACTIVITY_ID`),
  KEY `PROCUREMENT_METHOD_ID` (`PROCUREMENT_METHOD_ID`),
  KEY `PROCUREMENT_PLAN_ID` (`PROCUREMENT_PLAN_ID`),
  KEY `INSTITUTION_ID` (`INSTITUTION_ID`),
  KEY `SPFUID` (`SPFUID`),
  KEY `PROCUREMENTSTAGEID` (`PROCUREMENTSTAGEID`),
  CONSTRAINT `teqip_package_history_ibfk_1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`),
  CONSTRAINT `teqip_package_history_ibfk_2` FOREIGN KEY (`SUB_CATEGORY_ID`) REFERENCES `teqip_subcategorymaster` (`SUBCAT_ID`),
  CONSTRAINT `teqip_package_history_ibfk_3` FOREIGN KEY (`SUB_COMPONENT_ID`) REFERENCES `teqip_subcomponentmaster` (`ID`),
  CONSTRAINT `teqip_package_history_ibfk_4` FOREIGN KEY (`ACTIVITY_ID`) REFERENCES `teqip_activitiymaster` (`ACTIVITY_ID`),
  CONSTRAINT `teqip_package_history_ibfk_5` FOREIGN KEY (`PROCUREMENT_METHOD_ID`) REFERENCES `teqip_procurementmethod` (`PROCUREMENTMETHOD_ID`),
  CONSTRAINT `teqip_package_history_ibfk_6` FOREIGN KEY (`PROCUREMENT_PLAN_ID`) REFERENCES `teqip_procurementmaster` (`PROCUREMENTMASTER_ID`),
  CONSTRAINT `teqip_package_history_ibfk_7` FOREIGN KEY (`INSTITUTION_ID`) REFERENCES `teqip_institutions` (`INSTITUTION_ID`),
  CONSTRAINT `teqip_package_history_ibfk_8` FOREIGN KEY (`SPFUID`) REFERENCES `teqip_statemaster` (`STATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_history`
--

LOCK TABLES `teqip_package_history` WRITE;
/*!40000 ALTER TABLE `teqip_package_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_package_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_invitationletter`
--

DROP TABLE IF EXISTS `teqip_package_invitationletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_invitationletter` (
  `invitationLetterId` int(11) NOT NULL AUTO_INCREMENT,
  `packageId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `quotationValidityRequired` int(11) DEFAULT NULL,
  `trainingClause` varchar(500) DEFAULT NULL,
  `warranty` varchar(100) DEFAULT NULL,
  `workCompletionPeriod` int(11) DEFAULT NULL,
  `minliquidatedDamages` float DEFAULT NULL,
  `lastSubmissionDate` date DEFAULT NULL,
  `lastSubmissionTime` datetime DEFAULT NULL,
  `installationClause` varchar(500) DEFAULT NULL,
  `amc` varchar(100) DEFAULT NULL,
  `plannedBidOpeningDate` date DEFAULT NULL,
  `plannedBidOpeningTime` datetime DEFAULT NULL,
  `maxliquidatedDamages` float DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`invitationLetterId`),
  KEY `packageId` (`packageId`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `teqip_package_invitationletter_ibfk_1` FOREIGN KEY (`packageId`) REFERENCES `teqip_package` (`PACKAGE_ID`),
  CONSTRAINT `teqip_package_invitationletter_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `teqip_categorymaster` (`CAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_invitationletter`
--

LOCK TABLES `teqip_package_invitationletter` WRITE;
/*!40000 ALTER TABLE `teqip_package_invitationletter` DISABLE KEYS */;
INSERT INTO `teqip_package_invitationletter` VALUES (1,1,NULL,1,'trainingClause','warranty',1,1.1,'2018-04-11','2018-04-11 05:30:00','installationClause','amc','2018-04-11','2018-04-11 05:30:00',1.1,'2018-04-11 15:52:12',NULL,'2018-04-11',NULL);
/*!40000 ALTER TABLE `teqip_package_invitationletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_paymentdetail`
--

DROP TABLE IF EXISTS `teqip_package_paymentdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_paymentdetail` (
  `pack_paymentid` int(11) NOT NULL AUTO_INCREMENT,
  `packageid` int(11) NOT NULL,
  `paymentDescription` varchar(200) DEFAULT NULL,
  `milestonePercentage` float DEFAULT NULL,
  `expectedDeliveryDate` date DEFAULT NULL,
  `expectedDeliveryPeriod` int(11) DEFAULT NULL,
  `expectedPaymentAmount` float DEFAULT NULL,
  `poNumber` varchar(50) DEFAULT NULL,
  `poDate` date DEFAULT NULL,
  `isWCCGenerated` int(11) DEFAULT NULL,
  `milestoneActualCompletionPeriod` int(11) DEFAULT NULL,
  `paymentDate` varchar(50) DEFAULT NULL,
  `chequeDraftNumber` varchar(50) DEFAULT NULL,
  `expectedPaymentdate` date DEFAULT NULL,
  `isWorkCompleted` int(11) DEFAULT NULL,
  `actualCompletionDate` date DEFAULT NULL,
  `actualPaymentAmount` float DEFAULT NULL,
  `totalPaymentTillNow` float DEFAULT NULL,
  `comments` varchar(500) DEFAULT NULL,
  `gst` float DEFAULT NULL,
  `octroi` float DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`pack_paymentid`),
  KEY `packageid` (`packageid`),
  CONSTRAINT `teqip_package_paymentdetail_ibfk_1` FOREIGN KEY (`packageid`) REFERENCES `teqip_package` (`PACKAGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_paymentdetail`
--

LOCK TABLES `teqip_package_paymentdetail` WRITE;
/*!40000 ALTER TABLE `teqip_package_paymentdetail` DISABLE KEYS */;
INSERT INTO `teqip_package_paymentdetail` VALUES (1,1,'paymentDescription',1.1,'2018-04-11',1,1.1,'poNumber',NULL,1,1,'2018-04-11','chequeDraftNumber','2018-04-11',1,'2018-04-11',1.1,1.1,'comments',NULL,NULL,'2018-04-11 16:45:51',1,'2018-04-11',1);
/*!40000 ALTER TABLE `teqip_package_paymentdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_question_detail`
--

DROP TABLE IF EXISTS `teqip_package_question_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_question_detail` (
  `questionid` int(5) NOT NULL AUTO_INCREMENT,
  `question_category` varchar(50) NOT NULL,
  `question_desc` varchar(500) DEFAULT NULL,
  `question_decision` int(11) DEFAULT NULL,
  `question_nonresponsive_value` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  `packageid` int(11) NOT NULL,
  `STATUS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`questionid`),
  KEY `packageid` (`packageid`),
  CONSTRAINT `teqip_package_question_detail_ibfk_1` FOREIGN KEY (`packageid`) REFERENCES `teqip_package` (`PACKAGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_question_detail`
--

LOCK TABLES `teqip_package_question_detail` WRITE;
/*!40000 ALTER TABLE `teqip_package_question_detail` DISABLE KEYS */;
INSERT INTO `teqip_package_question_detail` VALUES (1,'questionCategory','question',1,1,'2018-04-11 16:34:56',1,'2018-04-11',1,1,NULL);
/*!40000 ALTER TABLE `teqip_package_question_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_questionmaster`
--

DROP TABLE IF EXISTS `teqip_package_questionmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_questionmaster` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `question_category` varchar(50) NOT NULL,
  `question_desc` varchar(500) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  `STATUS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_questionmaster`
--

LOCK TABLES `teqip_package_questionmaster` WRITE;
/*!40000 ALTER TABLE `teqip_package_questionmaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_package_questionmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_quotationdetail`
--

DROP TABLE IF EXISTS `teqip_package_quotationdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_quotationdetail` (
  `pack_quotationdetailid` int(11) NOT NULL AUTO_INCREMENT,
  `packageid` int(11) NOT NULL,
  `supplierid` int(11) DEFAULT NULL,
  `quotationopeningdate` date DEFAULT NULL,
  `quotationopeningtime` datetime DEFAULT NULL,
  `quotationmom` varchar(500) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`pack_quotationdetailid`),
  KEY `packageid` (`packageid`),
  KEY `supplierid` (`supplierid`),
  CONSTRAINT `teqip_package_quotationdetail_ibfk_1` FOREIGN KEY (`packageid`) REFERENCES `teqip_package` (`PACKAGE_ID`),
  CONSTRAINT `teqip_package_quotationdetail_ibfk_2` FOREIGN KEY (`supplierid`) REFERENCES `teqip_pmss_suppliermaster` (`supplierID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_quotationdetail`
--

LOCK TABLES `teqip_package_quotationdetail` WRITE;
/*!40000 ALTER TABLE `teqip_package_quotationdetail` DISABLE KEYS */;
INSERT INTO `teqip_package_quotationdetail` VALUES (1,1,NULL,'2018-04-11','2018-04-11 05:30:00','quotationMom','2018-04-11 16:50:42',1,'2018-04-11',1);
/*!40000 ALTER TABLE `teqip_package_quotationdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_quotationevaluation`
--

DROP TABLE IF EXISTS `teqip_package_quotationevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_quotationevaluation` (
  `package_QuotationEvaluationID` int(11) NOT NULL AUTO_INCREMENT,
  `supplierid` int(11) NOT NULL,
  `packageid` int(11) NOT NULL,
  `isquotationdeliveryvalue` int(11) DEFAULT NULL,
  `isquotationdeliverycomment` varchar(200) DEFAULT NULL,
  `isquotationpaymentvalue` int(11) DEFAULT NULL,
  `isquotationpaymentcomment` varchar(200) DEFAULT NULL,
  `Istrainingvalue` int(11) DEFAULT NULL,
  `Istrainingcomment` varchar(200) DEFAULT NULL,
  `Isinstalltionvalue` int(11) DEFAULT NULL,
  `Isinstalltioncomment` varchar(200) DEFAULT NULL,
  `Isquantityvalue` int(11) DEFAULT NULL,
  `Isquantitycomment` varchar(200) DEFAULT NULL,
  `Isplacevalue` int(11) DEFAULT NULL,
  `Isplacecomment` varchar(200) DEFAULT NULL,
  `Isqotationtechvalue` int(11) DEFAULT NULL,
  `Isqotationtechcomment` varchar(200) DEFAULT NULL,
  `Ispricecorrectionvalue` int(11) DEFAULT NULL,
  `Ispricecorrectioncomment` varchar(200) DEFAULT NULL,
  `pricecorrection` varchar(100) DEFAULT NULL,
  `evaluatedprice` float DEFAULT NULL,
  `taxes` float DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  `istotalmonetaryvalue` int(11) DEFAULT NULL,
  `istotalmonetarycomment` varchar(200) DEFAULT NULL,
  `isincometaxcertvalue` int(11) DEFAULT NULL,
  `isincometaxcertcooment` varchar(200) DEFAULT NULL,
  `isfinancereportvalue` int(11) DEFAULT NULL,
  `isfinancereportcomment` varchar(200) DEFAULT NULL,
  `islitigationreportvalue` int(11) DEFAULT NULL,
  `islitigationreportcomment` varchar(200) DEFAULT NULL,
  `hascontractorsatisfiedvalue` int(11) DEFAULT NULL,
  `hascontractorsatisfiedcomment` varchar(200) DEFAULT NULL,
  `contractorelectricalvalue` int(11) DEFAULT NULL,
  `contractorelectricalcomment` varchar(200) DEFAULT NULL,
  `contractorsanitaryvalue` int(11) DEFAULT NULL,
  `contractorsanitarycomment` varchar(200) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`package_QuotationEvaluationID`),
  KEY `packageid` (`packageid`),
  KEY `supplierid` (`supplierid`),
  CONSTRAINT `teqip_package_quotationevaluation_ibfk_1` FOREIGN KEY (`packageid`) REFERENCES `teqip_package` (`PACKAGE_ID`),
  CONSTRAINT `teqip_package_quotationevaluation_ibfk_2` FOREIGN KEY (`supplierid`) REFERENCES `teqip_pmss_suppliermaster` (`supplierID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_quotationevaluation`
--

LOCK TABLES `teqip_package_quotationevaluation` WRITE;
/*!40000 ALTER TABLE `teqip_package_quotationevaluation` DISABLE KEYS */;
INSERT INTO `teqip_package_quotationevaluation` VALUES (1,1,1,1,NULL,1,NULL,1,NULL,1,NULL,1,NULL,NULL,NULL,1,NULL,1,NULL,'reasonForPriceCorrection',NULL,NULL,'comments',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-11 16:53:33',1,'2018-04-11',1),(2,1,1,1,NULL,1,NULL,1,NULL,1,NULL,1,NULL,NULL,NULL,1,NULL,1,NULL,'reasonForPriceCorrection',NULL,NULL,'comments',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-11 16:55:01',1,'2018-04-11',1);
/*!40000 ALTER TABLE `teqip_package_quotationevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_quotationsopening`
--

DROP TABLE IF EXISTS `teqip_package_quotationsopening`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_quotationsopening` (
  `package_QuotationID` int(5) NOT NULL AUTO_INCREMENT,
  `supplierid` int(11) NOT NULL,
  `packageid` int(11) NOT NULL,
  `isquotationreceivedvalue` int(11) DEFAULT NULL,
  `isquotationreceivedcomment` varchar(200) DEFAULT NULL,
  `IsQuotationsignedvalue` int(11) DEFAULT NULL,
  `IsQuotationsignedcomment` varchar(200) DEFAULT NULL,
  `quotationnumbervalue` varchar(100) DEFAULT NULL,
  `quotationreceiveddatevalue` date DEFAULT NULL,
  `quotationvalidityvalue` int(11) DEFAULT NULL,
  `quotationdiscountvalue` float DEFAULT NULL,
  `quotationtaxesvalue` float DEFAULT NULL,
  `quotationnonreason` varchar(100) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`package_QuotationID`),
  KEY `packageid` (`packageid`),
  KEY `supplierid` (`supplierid`),
  CONSTRAINT `teqip_package_quotationsopening_ibfk_1` FOREIGN KEY (`packageid`) REFERENCES `teqip_package` (`PACKAGE_ID`),
  CONSTRAINT `teqip_package_quotationsopening_ibfk_2` FOREIGN KEY (`supplierid`) REFERENCES `teqip_pmss_suppliermaster` (`supplierID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_quotationsopening`
--

LOCK TABLES `teqip_package_quotationsopening` WRITE;
/*!40000 ALTER TABLE `teqip_package_quotationsopening` DISABLE KEYS */;
INSERT INTO `teqip_package_quotationsopening` VALUES (1,1,1,1,NULL,1,NULL,'quotationNumber','2018-04-11',1,1.1,NULL,NULL,'2018-04-11 16:50:42',1,'2018-04-11',1);
/*!40000 ALTER TABLE `teqip_package_quotationsopening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_package_supplierdetail`
--

DROP TABLE IF EXISTS `teqip_package_supplierdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_package_supplierdetail` (
  `pack_supplierdetailid` int(11) NOT NULL AUTO_INCREMENT,
  `packageid` int(11) NOT NULL,
  `supplierid` int(11) NOT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`pack_supplierdetailid`),
  KEY `packageid` (`packageid`),
  KEY `supplierid` (`supplierid`),
  CONSTRAINT `teqip_package_supplierdetail_ibfk_1` FOREIGN KEY (`packageid`) REFERENCES `teqip_package` (`PACKAGE_ID`),
  CONSTRAINT `teqip_package_supplierdetail_ibfk_2` FOREIGN KEY (`supplierid`) REFERENCES `teqip_pmss_suppliermaster` (`supplierID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_package_supplierdetail`
--

LOCK TABLES `teqip_package_supplierdetail` WRITE;
/*!40000 ALTER TABLE `teqip_package_supplierdetail` DISABLE KEYS */;
INSERT INTO `teqip_package_supplierdetail` VALUES (1,1,1,'2018-04-11 16:39:36',1,'2018-04-11',1);
/*!40000 ALTER TABLE `teqip_package_supplierdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_plan_approvalstatus`
--

DROP TABLE IF EXISTS `teqip_plan_approvalstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_plan_approvalstatus` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEQIP_PLANAPPROVAL_ID` int(11) NOT NULL,
  `PROCUREMENT_PLAN_ID` int(11) NOT NULL,
  `PACKAGE_ID` int(11) NOT NULL,
  `INSTITUTION_ID` int(11) NOT NULL,
  `SPFUID` int(11) NOT NULL,
  `CREATED_TYPE` int(11) NOT NULL,
  `PROCUREMENTSTAGEID` int(11) NOT NULL,
  `InstituteApprovalComment` varchar(500) DEFAULT NULL,
  `InstituteRejectionComment` varchar(500) DEFAULT NULL,
  `InstituteRevesionComment` varchar(500) DEFAULT NULL,
  `SpfuApprovalComment` varchar(500) DEFAULT NULL,
  `SpfuRejectionComment` varchar(500) DEFAULT NULL,
  `NpiuApprovalComment` varchar(500) DEFAULT NULL,
  `NpiuRejectionComment` varchar(500) DEFAULT NULL,
  `REVISEID` int(11) NOT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PROCUREMENT_PLAN_ID` (`PROCUREMENT_PLAN_ID`),
  KEY `PACKAGE_ID` (`PACKAGE_ID`),
  KEY `INSTITUTION_ID` (`INSTITUTION_ID`),
  KEY `SPFUID` (`SPFUID`),
  KEY `PROCUREMENTSTAGEID` (`PROCUREMENTSTAGEID`),
  CONSTRAINT `teqip_plan_approvalstatus_ibfk_1` FOREIGN KEY (`PROCUREMENT_PLAN_ID`) REFERENCES `teqip_procurementmaster` (`PROCUREMENTMASTER_ID`),
  CONSTRAINT `teqip_plan_approvalstatus_ibfk_2` FOREIGN KEY (`PACKAGE_ID`) REFERENCES `teqip_package` (`PACKAGE_ID`),
  CONSTRAINT `teqip_plan_approvalstatus_ibfk_3` FOREIGN KEY (`INSTITUTION_ID`) REFERENCES `teqip_institutions` (`INSTITUTION_ID`),
  CONSTRAINT `teqip_plan_approvalstatus_ibfk_4` FOREIGN KEY (`SPFUID`) REFERENCES `teqip_statemaster` (`STATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_plan_approvalstatus`
--

LOCK TABLES `teqip_plan_approvalstatus` WRITE;
/*!40000 ALTER TABLE `teqip_plan_approvalstatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_plan_approvalstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_pmss_currencymaster`
--

DROP TABLE IF EXISTS `teqip_pmss_currencymaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_pmss_currencymaster` (
  `CURRENCYID` int(11) NOT NULL AUTO_INCREMENT,
  `CURRENCYNAME` varchar(50) NOT NULL,
  `CURRENCYVALUE` float NOT NULL,
  `CURRENCYCOUNTRY` varchar(255) NOT NULL,
  PRIMARY KEY (`CURRENCYID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_pmss_currencymaster`
--

LOCK TABLES `teqip_pmss_currencymaster` WRITE;
/*!40000 ALTER TABLE `teqip_pmss_currencymaster` DISABLE KEYS */;
INSERT INTO `teqip_pmss_currencymaster` VALUES (1,'USD',64,'USA');
/*!40000 ALTER TABLE `teqip_pmss_currencymaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_pmss_institutionlogo`
--

DROP TABLE IF EXISTS `teqip_pmss_institutionlogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_pmss_institutionlogo` (
  `InstitutionLogoID` int(11) NOT NULL AUTO_INCREMENT,
  `INSTITUTION_ID` int(11) NOT NULL,
  `OriginalFileName` varchar(500) NOT NULL,
  `SystemFileName` varchar(500) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Description` varchar(500) NOT NULL,
  PRIMARY KEY (`InstitutionLogoID`),
  KEY `INSTITUTION_ID` (`INSTITUTION_ID`),
  CONSTRAINT `teqip_pmss_institutionlogo_ibfk_1` FOREIGN KEY (`INSTITUTION_ID`) REFERENCES `teqip_institutions` (`INSTITUTION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_pmss_institutionlogo`
--

LOCK TABLES `teqip_pmss_institutionlogo` WRITE;
/*!40000 ALTER TABLE `teqip_pmss_institutionlogo` DISABLE KEYS */;
INSERT INTO `teqip_pmss_institutionlogo` VALUES (1,1,'abc.jpg','abc.jpg',1,'2018-03-13 15:35:52','Test');
/*!40000 ALTER TABLE `teqip_pmss_institutionlogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_pmss_institutionpurchasecommitte`
--

DROP TABLE IF EXISTS `teqip_pmss_institutionpurchasecommitte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_pmss_institutionpurchasecommitte` (
  `InstitutionPurchaseCommitte_ID` int(11) NOT NULL AUTO_INCREMENT,
  `INSTITUTION_ID` int(11) DEFAULT NULL,
  `SPFUID` int(11) DEFAULT NULL,
  `DEPARTMENT_ID` int(11) DEFAULT NULL,
  `CommitteMemberName` varchar(200) NOT NULL,
  `DesignationID` int(11) DEFAULT NULL,
  `PCommitteRoleID` int(11) DEFAULT NULL,
  `committeetype` varchar(50) DEFAULT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`InstitutionPurchaseCommitte_ID`),
  KEY `INSTITUTION_ID` (`INSTITUTION_ID`),
  KEY `DEPARTMENT_ID` (`DEPARTMENT_ID`),
  KEY `SPFUID` (`SPFUID`),
  KEY `DesignationID` (`DesignationID`),
  KEY `PCommitteRoleID` (`PCommitteRoleID`),
  CONSTRAINT `teqip_pmss_institutionpurchasecommitte_ibfk_1` FOREIGN KEY (`INSTITUTION_ID`) REFERENCES `teqip_institutions` (`INSTITUTION_ID`),
  CONSTRAINT `teqip_pmss_institutionpurchasecommitte_ibfk_2` FOREIGN KEY (`DEPARTMENT_ID`) REFERENCES `teqip_pmssdepartmentmaster` (`DepartmentID`),
  CONSTRAINT `teqip_pmss_institutionpurchasecommitte_ibfk_3` FOREIGN KEY (`SPFUID`) REFERENCES `teqip_statemaster` (`STATE_ID`),
  CONSTRAINT `teqip_pmss_institutionpurchasecommitte_ibfk_4` FOREIGN KEY (`DesignationID`) REFERENCES `teqip_pmssdesignationmaster` (`DesignationID`),
  CONSTRAINT `teqip_pmss_institutionpurchasecommitte_ibfk_5` FOREIGN KEY (`PCommitteRoleID`) REFERENCES `teqip_pmssrolepurchasecommittee` (`PCRoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_pmss_institutionpurchasecommitte`
--

LOCK TABLES `teqip_pmss_institutionpurchasecommitte` WRITE;
/*!40000 ALTER TABLE `teqip_pmss_institutionpurchasecommitte` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_pmss_institutionpurchasecommitte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_pmss_procurementstages`
--

DROP TABLE IF EXISTS `teqip_pmss_procurementstages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_pmss_procurementstages` (
  `STAGEID` int(11) NOT NULL AUTO_INCREMENT,
  `PROCUREMENTSTAGEID` int(11) NOT NULL,
  `PROCUREMENTSTAGES` varchar(255) NOT NULL,
  `STAGE_DESCRIPTION` varchar(255) NOT NULL,
  `ROLE_ID` int(11) DEFAULT NULL,
  `ORDERNO` int(11) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `STATUS` varchar(50) DEFAULT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`STAGEID`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_pmss_procurementstages`
--

LOCK TABLES `teqip_pmss_procurementstages` WRITE;
/*!40000 ALTER TABLE `teqip_pmss_procurementstages` DISABLE KEYS */;
INSERT INTO `teqip_pmss_procurementstages` VALUES (28,1,'Under Preparation','PP - Prepare Procurement Plan by Institute or SPFU/NPIU/CFI.',5,3,1,'',1),(29,2,'Under Clarification','PP - Clarification Procurement Plan by Institute or SPFU/NPIU/CFI.',5,0,1,'rejected',1),(30,3,'SPFU Approval','PP - SPFU Approval.',7,5,1,'approved',1),(31,4,'Under Clarification','PP - Clarification Procurement Plan by Institute or SPFU/NPIU/CFI.',5,0,1,'rejected',1),(32,5,'NPIU Approval','PP - NPIU Approval.',3,3,1,'approved',1),(33,6,'NPIU Rejected','PP - NPIU Rejected.',3,2,1,'rejected',1),(34,7,'Plan Approved','PP - Plan Approved.',3,0,1,'',1);
/*!40000 ALTER TABLE `teqip_pmss_procurementstages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_pmss_suppliermaster`
--

DROP TABLE IF EXISTS `teqip_pmss_suppliermaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_pmss_suppliermaster` (
  `supplierID` int(11) NOT NULL AUTO_INCREMENT,
  `suppliername` varchar(255) NOT NULL,
  `supllieraddress` varchar(500) NOT NULL,
  `suppliercity` varchar(100) NOT NULL,
  `supplierstate` varchar(100) NOT NULL,
  `suppliercountry` varchar(100) NOT NULL,
  `suppliernationality` varchar(100) NOT NULL,
  `suppliersource` varchar(100) DEFAULT NULL,
  `supplierspecificsource` varchar(100) DEFAULT NULL,
  `supplierrepresentativename` varchar(100) DEFAULT NULL,
  `supplierpincode` int(11) NOT NULL,
  `supplierphoneno` int(11) NOT NULL,
  `pmss_type` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  `supliertannumber` varchar(100) DEFAULT NULL,
  `suplierfaxnumber` varchar(100) DEFAULT NULL,
  `suplierpannumber` varchar(100) DEFAULT NULL,
  `supliergstnumber` varchar(100) DEFAULT NULL,
  `supliertaxnumber` varchar(100) DEFAULT NULL,
  `suplieremailid` varchar(100) DEFAULT NULL,
  `supliereletter` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`supplierID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_pmss_suppliermaster`
--

LOCK TABLES `teqip_pmss_suppliermaster` WRITE;
/*!40000 ALTER TABLE `teqip_pmss_suppliermaster` DISABLE KEYS */;
INSERT INTO `teqip_pmss_suppliermaster` VALUES (1,'supplierName','address','city','state','country','nationality','supplierSource',NULL,'reprensentative',11,99999999,NULL,'2018-04-11 16:39:36',1,'2018-04-11',1,1,'tanNo','faxNo','panNo',NULL,'taxNo','emailId','letter');
/*!40000 ALTER TABLE `teqip_pmss_suppliermaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_pmss_threshholdmaster`
--

DROP TABLE IF EXISTS `teqip_pmss_threshholdmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_pmss_threshholdmaster` (
  `THRESHOLDID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORYID` int(11) NOT NULL,
  `threshold_min_value` float NOT NULL,
  `threshold_max_value` float NOT NULL,
  `procurementmethodid` int(11) DEFAULT NULL,
  `isproprietary` int(11) NOT NULL,
  `iscoe` int(11) NOT NULL,
  `isgem` int(11) NOT NULL,
  `review` varchar(50) DEFAULT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`THRESHOLDID`),
  KEY `CATEGORYID` (`CATEGORYID`),
  KEY `procurementmethodid` (`procurementmethodid`),
  CONSTRAINT `teqip_pmss_threshholdmaster_ibfk_1` FOREIGN KEY (`CATEGORYID`) REFERENCES `teqip_categorymaster` (`CAT_ID`),
  CONSTRAINT `teqip_pmss_threshholdmaster_ibfk_2` FOREIGN KEY (`procurementmethodid`) REFERENCES `teqip_procurementmethod` (`PROCUREMENTMETHOD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_pmss_threshholdmaster`
--

LOCK TABLES `teqip_pmss_threshholdmaster` WRITE;
/*!40000 ALTER TABLE `teqip_pmss_threshholdmaster` DISABLE KEYS */;
INSERT INTO `teqip_pmss_threshholdmaster` VALUES (1,1,2000000,3000000,2,0,0,0,'Prior',1),(2,1,100000,2000000,2,0,0,0,'Post',1),(3,1,1,100000,3,0,0,0,'Post',1),(4,1,1000000,100000000,4,1,0,0,'Prior',1),(5,1,1,1000000,4,1,0,0,'Post',1),(7,1,1,1000,9,0,0,0,'Post',1);
/*!40000 ALTER TABLE `teqip_pmss_threshholdmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_pmssdepartmentmaster`
--

DROP TABLE IF EXISTS `teqip_pmssdepartmentmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_pmssdepartmentmaster` (
  `DepartmentID` int(11) NOT NULL AUTO_INCREMENT,
  `Department` varchar(100) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`DepartmentID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_pmssdepartmentmaster`
--

LOCK TABLES `teqip_pmssdepartmentmaster` WRITE;
/*!40000 ALTER TABLE `teqip_pmssdepartmentmaster` DISABLE KEYS */;
INSERT INTO `teqip_pmssdepartmentmaster` VALUES (1,'SEPG',0,'2018-04-03 14:38:26',1,NULL,NULL),(2,'Finance',0,'2018-04-03 14:38:26',1,NULL,NULL),(3,'Marketing',0,'2018-04-03 14:38:27',1,NULL,NULL),(4,'Human Resource',0,'2018-04-03 14:38:27',1,NULL,NULL),(7,'department',1,'2018-04-11 16:16:59',1,'2018-04-11',1);
/*!40000 ALTER TABLE `teqip_pmssdepartmentmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_pmssdesignationmaster`
--

DROP TABLE IF EXISTS `teqip_pmssdesignationmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_pmssdesignationmaster` (
  `DesignationID` int(11) NOT NULL AUTO_INCREMENT,
  `Designation` varchar(100) NOT NULL,
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`DesignationID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_pmssdesignationmaster`
--

LOCK TABLES `teqip_pmssdesignationmaster` WRITE;
/*!40000 ALTER TABLE `teqip_pmssdesignationmaster` DISABLE KEYS */;
INSERT INTO `teqip_pmssdesignationmaster` VALUES (1,'SEPG',0,'2018-04-03 14:40:02',1,NULL,NULL),(2,'Finance',0,'2018-04-03 14:40:03',1,NULL,NULL),(3,'Marketing',0,'2018-04-03 14:40:03',1,NULL,NULL),(4,'Human Resource',0,'2018-04-03 14:40:04',1,NULL,NULL);
/*!40000 ALTER TABLE `teqip_pmssdesignationmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_pmssrolepurchasecommittee`
--

DROP TABLE IF EXISTS `teqip_pmssrolepurchasecommittee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_pmssrolepurchasecommittee` (
  `PCRoleID` int(11) NOT NULL AUTO_INCREMENT,
  `PCRoleName` varchar(100) NOT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`PCRoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_pmssrolepurchasecommittee`
--

LOCK TABLES `teqip_pmssrolepurchasecommittee` WRITE;
/*!40000 ALTER TABLE `teqip_pmssrolepurchasecommittee` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_pmssrolepurchasecommittee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_procurementallocation`
--

DROP TABLE IF EXISTS `teqip_procurementallocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_procurementallocation` (
  `PROCUREMENTALLOCATION_ID` int(4) NOT NULL AUTO_INCREMENT,
  `PROCUREMENT_PLAN_ID` int(11) NOT NULL,
  `BUDGET_ID` int(11) DEFAULT NULL,
  `GOODS_PERCENTAGE` float DEFAULT NULL,
  `WORKS_PERCENTAGE` float DEFAULT NULL,
  `SERVICE_PERCENTAGE` float DEFAULT NULL,
  `OTHER_PERCENTAGE` float DEFAULT NULL,
  `PLAN_AMOUNT` float DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PROCUREMENTALLOCATION_ID`),
  KEY `PROCUREMENT_PLAN_ID` (`PROCUREMENT_PLAN_ID`),
  CONSTRAINT `teqip_procurementallocation_ibfk_1` FOREIGN KEY (`PROCUREMENT_PLAN_ID`) REFERENCES `teqip_procurementmaster` (`PROCUREMENTMASTER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_procurementallocation`
--

LOCK TABLES `teqip_procurementallocation` WRITE;
/*!40000 ALTER TABLE `teqip_procurementallocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_procurementallocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_procurementmaster`
--

DROP TABLE IF EXISTS `teqip_procurementmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_procurementmaster` (
  `PROCUREMENTMASTER_ID` int(4) NOT NULL AUTO_INCREMENT,
  `SHORT_NAME` varchar(100) DEFAULT NULL,
  `PLAN_NAME` varchar(300) DEFAULT NULL,
  `ISACTIVE` tinyint(4) DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `PLAN_AMOUNT` float DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PROCUREMENTMASTER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_procurementmaster`
--

LOCK TABLES `teqip_procurementmaster` WRITE;
/*!40000 ALTER TABLE `teqip_procurementmaster` DISABLE KEYS */;
INSERT INTO `teqip_procurementmaster` VALUES (1,'PMSS','PMSS Procurement Plan',1,'2018-03-01','2020-03-31',1000000,'2018-03-07 12:34:45');
/*!40000 ALTER TABLE `teqip_procurementmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_procurementmethod`
--

DROP TABLE IF EXISTS `teqip_procurementmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_procurementmethod` (
  `PROCUREMENTMETHOD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROCUREMENTMETHOD_CODE` varchar(100) NOT NULL,
  `PROCUREMENTMETHOD_NAME` varchar(200) NOT NULL,
  `PROCUREMENTMETHOD_CAT_ID` int(11) NOT NULL,
  PRIMARY KEY (`PROCUREMENTMETHOD_ID`),
  KEY `PROCUREMENTMETHOD_CAT_ID` (`PROCUREMENTMETHOD_CAT_ID`),
  CONSTRAINT `teqip_procurementmethod_ibfk_1` FOREIGN KEY (`PROCUREMENTMETHOD_CAT_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_procurementmethod`
--

LOCK TABLES `teqip_procurementmethod` WRITE;
/*!40000 ALTER TABLE `teqip_procurementmethod` DISABLE KEYS */;
INSERT INTO `teqip_procurementmethod` VALUES (1,'ICB','International Competitive Bidding (ICB)',1),(2,'NCB','National Competitive Bidding',1),(3,'Shopping','Shopping',1),(4,'Direct Contract','Direct Contracting',1),(5,'LIB','Limited International Bidding',2),(6,'QCBS','Quality and Cost Based Selection',2),(7,'FBS','Fixed Budget Selection',2),(8,'LCS','Least Cost Selection',2),(9,'GSS','Single Supplier',1);
/*!40000 ALTER TABLE `teqip_procurementmethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_procurementmethod_timeline`
--

DROP TABLE IF EXISTS `teqip_procurementmethod_timeline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_procurementmethod_timeline` (
  `PROCUREMENTMETHOD_TIMELINEID` int(11) NOT NULL AUTO_INCREMENT,
  `PROCUREMENTMETHOD_ID` int(11) NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `BidDocumentPreparationDate_days` float DEFAULT NULL,
  `BankNOCForBiddingDocuments_days` float DEFAULT NULL,
  `BidInvitationDate_days` float DEFAULT NULL,
  `BidOpeningDate_days` float DEFAULT NULL,
  `TORFinalizationDate_days` float DEFAULT NULL,
  `AdvertisementDate_days` float DEFAULT NULL,
  `FinalDraftToBeForwardedToTheBankDate_days` float DEFAULT NULL,
  `NoObjectionFromBankForRFP_days` float DEFAULT NULL,
  `RFPIssuedDate_days` float DEFAULT NULL,
  `LastDateToReceiveProposals_days` float DEFAULT NULL,
  `EvaluationDate_days` float DEFAULT NULL,
  `NoObjectionFromBankForEvaluation_days` float DEFAULT NULL,
  `ContractCompletionDate_days` float DEFAULT NULL,
  `ContractAwardDate_days` float DEFAULT NULL,
  `TORFinalizationDate_WithoutBankNOC_days` float DEFAULT NULL,
  `AdvertisementDate_WithoutBankNOC_days` float DEFAULT NULL,
  `RFPIssuedDate_WithoutBankNOC_days` float DEFAULT NULL,
  `LastDateToReceiveProposals_WithoutBankNOC_days` float DEFAULT NULL,
  `EvaluationDate_WithoutBankNOC_days` float DEFAULT NULL,
  `ContractAwardDate_WithoutBankNOC_days` float DEFAULT NULL,
  `ContractCompletionDate_WithoutBankNOC_days` float DEFAULT NULL,
  PRIMARY KEY (`PROCUREMENTMETHOD_TIMELINEID`),
  KEY `PROCUREMENTMETHOD_ID` (`PROCUREMENTMETHOD_ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`),
  CONSTRAINT `teqip_procurementmethod_timeline_ibfk_1` FOREIGN KEY (`PROCUREMENTMETHOD_ID`) REFERENCES `teqip_procurementmethod` (`PROCUREMENTMETHOD_ID`),
  CONSTRAINT `teqip_procurementmethod_timeline_ibfk_2` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_procurementmethod_timeline`
--

LOCK TABLES `teqip_procurementmethod_timeline` WRITE;
/*!40000 ALTER TABLE `teqip_procurementmethod_timeline` DISABLE KEYS */;
INSERT INTO `teqip_procurementmethod_timeline` VALUES (1,1,1,15,20,24,30,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,115,60,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `teqip_procurementmethod_timeline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_quotationevaluation`
--

DROP TABLE IF EXISTS `teqip_quotationevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_quotationevaluation` (
  `QuotationEvaluationID` int(5) NOT NULL AUTO_INCREMENT,
  `QuotationEvaluationquestion` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`QuotationEvaluationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_quotationevaluation`
--

LOCK TABLES `teqip_quotationevaluation` WRITE;
/*!40000 ALTER TABLE `teqip_quotationevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_quotationevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_quotationopening`
--

DROP TABLE IF EXISTS `teqip_quotationopening`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_quotationopening` (
  `QuotationID` int(5) NOT NULL AUTO_INCREMENT,
  `quotationquestions` varchar(100) DEFAULT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFY_ON` date DEFAULT NULL,
  `MODIFY_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`QuotationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_quotationopening`
--

LOCK TABLES `teqip_quotationopening` WRITE;
/*!40000 ALTER TABLE `teqip_quotationopening` DISABLE KEYS */;
/*!40000 ALTER TABLE `teqip_quotationopening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_rolemaster`
--

DROP TABLE IF EXISTS `teqip_rolemaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_rolemaster` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_DESCRIPTION` varchar(255) NOT NULL,
  `PMSS_ROLE` varchar(50) NOT NULL,
  `LEVEL_ID` int(11) DEFAULT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_rolemaster`
--

LOCK TABLES `teqip_rolemaster` WRITE;
/*!40000 ALTER TABLE `teqip_rolemaster` DISABLE KEYS */;
INSERT INTO `teqip_rolemaster` VALUES (1,'Admin','NPIU',2,1,1,'2018-03-07 12:32:40'),(2,'Central Project Advisor','NPIU',2,1,1,'2018-03-07 12:32:40'),(3,'Procurement Coordinator','NPIU',2,1,1,'2018-03-07 12:32:40'),(4,'State Procurement Coordinator','SPFU',2,1,1,'2018-03-07 12:32:40'),(5,'Institution Procurement Coordinator','INST',2,1,1,'2018-03-07 12:32:40'),(6,'Institution Head/ Director','INST',2,1,1,'2018-03-07 12:32:40'),(7,'State Project Advisor (SPA)','SPFU',2,1,1,'2018-03-07 12:32:40'),(8,'State Project Coordinator','SPFU',2,1,1,'2018-03-07 12:32:40'),(9,'Institution TEQIP Coordinator','INST',2,1,1,'2018-03-07 12:32:40');
/*!40000 ALTER TABLE `teqip_rolemaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_statemaster`
--

DROP TABLE IF EXISTS `teqip_statemaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_statemaster` (
  `STATE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `STATE_CODE` varchar(255) NOT NULL,
  `STATE_NAME` varchar(255) NOT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`STATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_statemaster`
--

LOCK TABLES `teqip_statemaster` WRITE;
/*!40000 ALTER TABLE `teqip_statemaster` DISABLE KEYS */;
INSERT INTO `teqip_statemaster` VALUES (1,'AS','Assam',1),(2,'AP','Andhra Pradesh',1),(3,'KL','Kerala',1),(4,'TL','Telangana',1),(5,'HR','Haryana',1),(6,'KA','Karnataka',1);
/*!40000 ALTER TABLE `teqip_statemaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_subcategorymaster`
--

DROP TABLE IF EXISTS `teqip_subcategorymaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_subcategorymaster` (
  `SUBCAT_ID` int(4) NOT NULL AUTO_INCREMENT,
  `CATEGORY_ID` int(11) NOT NULL,
  `SUB_CATEGORY_NAME` varchar(100) NOT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`SUBCAT_ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`),
  CONSTRAINT `teqip_subcategorymaster_ibfk_1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_subcategorymaster`
--

LOCK TABLES `teqip_subcategorymaster` WRITE;
/*!40000 ALTER TABLE `teqip_subcategorymaster` DISABLE KEYS */;
INSERT INTO `teqip_subcategorymaster` VALUES (1,1,'Repair works',1),(2,1,'Refurbishment works',1),(3,1,'Extension of Buildings',1),(4,2,'Equipment',1),(5,2,'Furniture',1),(6,2,'Minor Items',1),(7,2,'Books Books & Learning Resources Resources',1),(8,3,'Research/ Consultancy Contracts',1),(9,3,'Professional Services',1),(10,3,'Training',1),(11,3,'Workshops',1);
/*!40000 ALTER TABLE `teqip_subcategorymaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_subcomponentmaster`
--

DROP TABLE IF EXISTS `teqip_subcomponentmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_subcomponentmaster` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPONENT_ID` int(11) NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `SUB_CATEGORY_ID` int(11) DEFAULT NULL,
  `SUBCOMPONENTCODE` varchar(50) NOT NULL,
  `SUB_COMPONENT_NAME` varchar(200) NOT NULL,
  `ISACTIVE` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `COMPONENT_ID` (`COMPONENT_ID`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`),
  CONSTRAINT `teqip_subcomponentmaster_ibfk_1` FOREIGN KEY (`COMPONENT_ID`) REFERENCES `teqip_componentmaster` (`ID`),
  CONSTRAINT `teqip_subcomponentmaster_ibfk_2` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `teqip_categorymaster` (`CAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_subcomponentmaster`
--

LOCK TABLES `teqip_subcomponentmaster` WRITE;
/*!40000 ALTER TABLE `teqip_subcomponentmaster` DISABLE KEYS */;
INSERT INTO `teqip_subcomponentmaster` VALUES (1,1,1,1,'1.1','Institutional Development Grants to Participating institutes',1),(2,1,1,1,'1.2','Widening Impact through ATUs',1),(3,1,1,1,'1.2.1a','Establishing Centers of Excellence (CoE1)',1),(4,2,2,2,'2.1','Capacity Building to Strengthen Management',1),(5,2,2,2,'2.2','Project Management, Monitoring and Evaluation',1),(6,1,1,1,'1.2.1b','Establishing Centers of Excellence (CoE2)',1),(7,2,1,1,'1.3','Twinning Arrangements to Build Capacity and Improve Performance of Participating Institutes and ATUs',1);
/*!40000 ALTER TABLE `teqip_subcomponentmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_users_detail`
--

DROP TABLE IF EXISTS `teqip_users_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_users_detail` (
  `USERID` int(11) NOT NULL AUTO_INCREMENT,
  `EMPLOYEE_CODE` int(11) DEFAULT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `LNAME` varchar(50) DEFAULT NULL,
  `SPOUSE_NAME` varchar(100) DEFAULT NULL,
  `USER_MOBILE` bigint(20) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `JOINING_DATE` date DEFAULT NULL,
  `DEPT_ID` int(11) DEFAULT NULL,
  `ROLE_ID` int(11) DEFAULT NULL,
  `CTC` float DEFAULT NULL,
  `USER_NAME` varchar(100) NOT NULL,
  `ISACTIVE` tinyint(1) NOT NULL DEFAULT '1',
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_ON` date DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`USERID`),
  KEY `ROLE_ID` (`ROLE_ID`),
  CONSTRAINT `teqip_users_detail_ibfk_1` FOREIGN KEY (`ROLE_ID`) REFERENCES `teqip_rolemaster` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_users_detail`
--

LOCK TABLES `teqip_users_detail` WRITE;
/*!40000 ALTER TABLE `teqip_users_detail` DISABLE KEYS */;
INSERT INTO `teqip_users_detail` VALUES (1,NULL,'Test',NULL,NULL,NULL,NULL,NULL,'test@test.com','edcil',NULL,NULL,5,NULL,'edcil',1,'2018-03-18 06:07:59',NULL,NULL,NULL),(2,NULL,'Test',NULL,NULL,NULL,NULL,NULL,'test@test.com','spfu',NULL,NULL,7,NULL,'spfu',1,'2018-03-18 06:08:34',NULL,NULL,NULL),(3,NULL,'Test',NULL,NULL,NULL,NULL,NULL,'test@test.com','npiu',NULL,NULL,1,NULL,'npiu',1,'2018-03-18 06:09:03',NULL,NULL,NULL);
/*!40000 ALTER TABLE `teqip_users_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teqip_users_master`
--

DROP TABLE IF EXISTS `teqip_users_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teqip_users_master` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMPLOYEE_CODE` int(11) DEFAULT NULL,
  `USER_ID` int(11) NOT NULL,
  `ISACTIVE` tinyint(1) NOT NULL DEFAULT '1',
  `INSTITUTION_ID` int(11) DEFAULT NULL,
  `SPFUID` int(11) DEFAULT NULL,
  `ROLE_ID` int(11) NOT NULL,
  `CREATED_ON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_ON` date DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `INSTITUTION_ID` (`INSTITUTION_ID`),
  KEY `ROLE_ID` (`ROLE_ID`),
  KEY `SPFUID` (`SPFUID`),
  CONSTRAINT `teqip_users_master_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `teqip_users_detail` (`USERID`),
  CONSTRAINT `teqip_users_master_ibfk_2` FOREIGN KEY (`INSTITUTION_ID`) REFERENCES `teqip_institutions` (`INSTITUTION_ID`),
  CONSTRAINT `teqip_users_master_ibfk_3` FOREIGN KEY (`ROLE_ID`) REFERENCES `teqip_rolemaster` (`ROLE_ID`),
  CONSTRAINT `teqip_users_master_ibfk_4` FOREIGN KEY (`SPFUID`) REFERENCES `teqip_statemaster` (`STATE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teqip_users_master`
--

LOCK TABLES `teqip_users_master` WRITE;
/*!40000 ALTER TABLE `teqip_users_master` DISABLE KEYS */;
INSERT INTO `teqip_users_master` VALUES (1,NULL,1,1,1,1,5,'2018-03-18 06:22:07',NULL,NULL,NULL),(2,NULL,2,1,NULL,1,7,'2018-03-18 06:23:28',NULL,NULL,NULL),(3,NULL,3,1,NULL,NULL,1,'2018-03-18 06:23:48',NULL,NULL,NULL);
/*!40000 ALTER TABLE `teqip_users_master` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-12 12:46:58
